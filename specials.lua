stairs = {}
stairs.__index = stairs
function stairs:new(x,y,width,height,sx,sy)
	local object = {
		x = x,
		y = y,
		fixtures = {},
		width = width,
		height = height,
		time = 0,
		rx = 0,
		ry = 0,
		dir = 1
	}
	object.points = {}
	table.insert(object.points,0)
	table.insert(object.points,0)
	object.width = width
	object.height = height
	local width = object.width
	local height = object.height
	table.insert(object.points,width)
	table.insert(object.points,-height)
	table.insert(object.points,width)
	table.insert(object.points,0)
	table.insert(object.points,0)
	table.insert(object.points,0)
	if sx and  sx < 0 then
		self.dir = -1
		object.points[1] = width
		object.points[3] = width
	end
	if sy and sy < 0 then
		self.diry = -1
		object.points[2] = height
		object.points[4] = -height
	end
	object.points2 = object.points
	object.points = love.math.triangulate(unpack(object.points))
	local table = object.points
	for i in ipairs(table) do
		if not love.math.isConvex(unpack(table[i])) then
			error("not convex: "..i)
		end
	end
	return setmetatable(object,stairs)
end
function stairs:addPhyBody(phy,hc)
	local width = self.width
	local height = self.height
	local table = self.points
	local fixture = nil
	local steep = math.sin(width/height)
	local shape = love.physics.newPolygonShape(self.points2)
	self.shape = shape

		self.body = love.physics.newBody(phyWorld,self.x,-self.y,"static")
		fixture = love.physics.newFixture(self.body,shape)
		fixture:setGroupIndex(-1)
		self.fixtures = fixture
		fixture:setUserData("stairs "..tostring(self))

	if not hc then
		self.colision = Collider:addPolygon(shape:getPoints())
		local x1,y1,x2,y2 = self.colision:bbox()
		local width = x2 - x1
		local height = y2 - y1
		self.colision.width = -(width/100)*16
		self.colision.height = -(height/100)*17
		self.colision.userData = "stairs"
		self.colision.source = self
		self.colision.fixture = fixture
		self.colision.body = self.body
		self.colision.steep = steep
		self.colision.clamp1 = 0
	end

end
function stairs:draw(col)
	if col then
		love.graphics.setColor(255,0,110)
		self.colision:draw("fill")
		if self.body then
			love.graphics.push()
			love.graphics.setColor(0,255,150)
			love.graphics.translate(self.body:getPosition())
			love.graphics.polygon("line",self.shape:getPoints())
			love.graphics.pop()
		end
		love.graphics.setColor(255,255,255)
	end
end
function stairs:setCScale(x,y)
end
function stairs:remove()
	if self.body then
		self.body:destroy()
	end
	if self.colision then
		Collider:remove(self.colision)
	end
end
function stairs:moveTo(x,y)
	if self.body then
		self.body:setPosition(x - self.width/2,y + self.height/2)
	end
	if self.colision then
		self.colision:moveTo(x - (self.colision.width*self.dir),y - self.colision.height)
	end
end
function stairs:onContact(a,b,coll)
	local index = tostring(self)
	local other
	if string.find(a:getUserData(),index) then
		other = b
	elseif string.find(b:getUserData(),index) then
		other = a
	end
	if other ~= nil then
		local text = other:getUserData()
		if string.find(text,"Player") then
			if not love.keyboard.isDown("s") then
				local text = other:getBody()
				local player = text:getUserData()
				player:stairs(true,math.abs(self.colision.steep),math.abs(self.width),math.abs(self.height))
			else
				coll:isEnabled(false)
			end
		end
	end
end
function stairs:endContact(a,b,coll)
	local index = tostring(self)
	local other
	if string.find(a:getUserData(),index) then
		other = b
	elseif string.find(b:getUserData(),index) then
		other = a
	end
	if other ~= nil then
		local text = other:getUserData()
		if string.find(text,"Player") then
			local text = other:getBody()
			local player = text:getUserData()
			player:stairs(false,0)
		end
	end
end
function stairs:HCol(dt,shape_a,shape_b)
	local other
	if shape_a == self.colision then
		other = shape_b
	elseif shape_b == self.colision then
		other = shape_a
	end
	if other ~= nil and other.userData ~= nil then 
		if string.find(other.userData,"player") then
			local _
			local _a
			local text = other.userData
			local player = other.source
			if self.colision.clamp1 == 0 and love.keyboard.isDown("s") then
				_ = true 
			end
			self.colision.clamp1 = self.colision.clamp1 + 1
			local x,y1 = self.body:getPosition()
			local x2,y2 = player.point_1:center()
			local dif = y1  - y2
			if self.dir == 1 then
				if x + self.width - 5 < x2 then
					_ = true 
				end				
			else
				if x + self.width - 5  > x2 then
					_ = true
				end
			end
			if dif < 0 or  _ then
				self.colision.fixture:setMask(16)
			elseif not love.keyboard.isDown("s") then
			end
		end
	end
end
function stairs:Hstop(dt,shape_a,shape_b)
	local other
	if shape_a == self.colision then
		other = shape_b
	elseif shape_b == self.colision then
		other = shape_a
	end
	if other ~= nil and other.userData and string.find(other.userData,"player") then
		local player = other.source
		self.colision.fixture:setMask(1)
		player:stairs(false,0)
		self.colision.clamp1 = 0
	end
end
function stairs:setDirection(dir)
	if dir > 0 then
		self.dir = 1
	elseif dir  < 0 then
		self.dir = -1
	elseif dir == "right" then
		self.dir = 1
	elseif dir == "left" then
		self.dir = -1
	end
end
platform = {}
platform.__index = platform

function platform:new(x,y,width,height,scalex,scaley)
	local object = {
		x = x  or 0,
		y = y  or 0,
		width = width or 100,
		height = height or 10,
		speed = 5,
		floor = {},
		ox = x or 0,
		oy = y or 0,
		fx = fx or 0,
		fy = fy or 0,
		fixture = {},
		des = 1,
		body = nil,
		cur = 0,
		shape = nil,
		current = 1,
		smooth = smooth or false,
		drawPath = true,
		image = image,
		ui = false,
		hasUi = false,
		keyp = false,
		keyr = false,
		onCol = false,
		contact = false,
		shapes = {},
		pause = 0,
		timer = 0,
		active = false,
		items = {},
		scalex = scalex or 1,
		scaley = scaley or 1,
		colisions = {},
		renderRange = nil,
		cycle2 = false,
		rail = false,
		railEnd = false,
		spritebatch = false,
		floors = {},
		status = "moving",
		xvl  =  0,
		yvl = 0
	}
	return setmetatable(object,platform)
end
function platform:addFrame(shape)
	if self.body then 
		local index = tostring(self)
		local shape,x,y,types = shape(self)
		if not shape:getPoints() then
			error("invalid shape")
		end
		local body = self.body
		local fixture = love.physics.newFixture(self.body,shape)
		fixture:setUserData("platform:"..index)
		if types == "dynamic" then
			local colision = Collider:addPolygon(shape:getPoints())
			colision:moveTo(body:getX(),body:getY())
			colision.body = self.body
			colision.fparent = self
			colision.userData = "platform"
			colision.modx = x
			colision.mody = y
			table.insert(self.colisions,colision)
		end
		table.insert(self.shapes,shape)
	end
end
function platform:setImage(image,type,width,height)
	local mag = {}
	self.scalex = sx or self.scalex
	self.scaley = sy or self.scaley
	mag.x = 0 - (image:getWidth()*self.scalex)/2
	mag.y = 0 - (image:getHeight()*self.scaley)
	if width then
		self.width = image:getWidth()*self.scalex
	end
	if height then 
		self.height = image:getHeight()*self.scaley
	end
	mag.image = image
	if type == nil then
		type = "image"
	end
	mag.type = type
	table.insert(self.items,mag)
end
function platform:attachImage(x,y,image,type)
	local mag = {}
	mag.x = x
	mag.y = y
	table.insert(self.items,mag)
end
function platform:attachItem(item)
	table.insert(self.items,item)
end
function platform:setOrigin(ox,oy)
	self.ox = ox
	self.oy = oy
end
function platform:setPos(x,y)
	self.x = x
	self.y = y
	self:cUpdate()
end
function platform:getOrigin()
	return self.ox,self.oy
end
function platform:setUI(func)
	self.ui = func
end
function platform:setKeyp(func)
	self.keyp = func
end
function platform:setKeyr(func)
	self.keyr = func
end
function platform:hasUI(hasUI)
	self.hasUi = hasUI
end
function platform:setDrawPath(path)
	self.drawPath = path
end
function platform:setSmooth(smooth)
	self.smooth = smooth
end
function platform:adjustSpeed(horiz,vert,destination)
	local body = self.body
	local cx = body:getX()
	local cy = body:getY()
	local nx = nil
	local ny = nil
	local dx = self.floor[destination].x
	local dy = self.floor[destination].y
	dx = math.floor(dx)
	dy = math.floor(dy)
	cx = math.floor(cx)
	cy = math.floor(cy)
	local difx = math.max(dx,cx) - math.min(dx,cx)
	local dify = math.max(dy,cy) - math.min(dy,cy)
	if horiz then
		self.speedx = math.floor(math.abs(difx*100))
	end
	if vert then
		self.speedy = math.floor(math.abs(dify*100))
	end
end
function platform:getSmooth()
	return smooth
end
function platform:setSpeed(speed)
	self.speed = speed
end
function platform:getSpeedVec()
	return self.speedx,self.speedy
end
function platform:newFloor(x,y,no)
	if y == nil then
		y = self.body:getY()
	end
	if x == nil then
		x = self.body:getX() 
	end
	if no == nil then
		no = tableLenght(self.floor) + 1
	end

	local floor = {}
	floor.x = x 
	floor.y = y
	if self.floor[no] ~= nil then
		table.insert(self.floor,no,floor)
	else
		self.floor[no] = {}
		self.floor[no].x = x
		self.floor[no].y = y
	end
	if self.renderRange then
		Collider:remove(self.renderRange)
	end
	local tablex = {}
	local tabley = {}
	for i in ipairs(self.floor) do
		local x = self.floor[i].x
		local y = self.floor[i].y
		table.insert(tablex,x)
		table.insert(tabley,y)
	end
	local Xmax = math.max(unpack(tablex))
	local Xmin = math.min(unpack(tablex))
	local Ymax = math.max(unpack(tabley))
	local Ymin = math.min(unpack(tabley))
	local width = Xmax - Xmin
	local height = Ymin - Ymax
	if Xmin < 0 then
		width = Xmax - Xmin
	end
	if Ymin < 0 then
		height = Ymax - Ymin
	end
	local tablew = {}
	local tableh = {}
	for i in ipairs(self.items) do
		if self.items[i].image then
			table.insert(tablew,self.items[i].image:getWidth())
			table.insert(tableh,self.items[i].image:getHeight())
		end
	end
	
	local maxw = math.max(unpack(tablew))*self.scalex
	local maxh = math.max(unpack(tableh))*self.scaley
	
	local width2 = width
	width = width 
	local x = Xmin
	local y = Ymin
	self.renderRange = Collider:addRectangle(x - maxw/2,y - maxh,width + maxw ,height + maxh)
	self.renderRange.source = self
	self.renderRange.userData = "elevator"
end
function platform:makeBody(hc,phy) 
	if hc == nil then
		hc = true
	end
	if phy == nil then
		phy = true
	end
	local shape = self.shape
	local body = self.body
	
	
	if phy then
		shape = self.shape or love.physics.newRectangleShape(self.width*self.scalex,self.height*self.scaley)
		body = self.body or love.physics.newBody(phyWorld,self.x,self.y,"kinematic")
		local fixture = love.physics.newFixture(body,shape)
		fixture:setUserData("platform")
		body:setPosition(self.x,self.y)
	end
	if hc and current_gamestate ~= "editor" then
		if shape then
			self.colision = Collider:addPolygon(shape:getPoints())
			self.colision:move(body:getX(),body:getY())
		else
			self.colision = Collider:addRectangle(self.x,self.y,self.width*self.scalex,self.height*self.scaley)
			self.colision:moveTo(self.x,self.y)
		end
		self.colision.body = body
		self.colision.fparent = self
		self.colision.userData = "platform"
		self.colision.modx = 0
		self.colision.mody = 0
		table.insert(self.colisions,self.colision)
	end
	self.shape = shape
	self.body = body
	local width = {}
	local height = {}
	for i in ipairs(self.items) do
		table.insert(height,self.items[i].image:getHeight())
		table.insert(width,self.items[i].image:getWidth())
	end
	local width = math.max(unpack(width)) * self.scalex
	local height = math.max(unpack(height)) * self.scaley
	self._width = width
	self._height = height
	self.renderRange = Collider:addRectangle(self.x - width/2,self.y - height,width,height)
	self.renderRange.source = self
	self.renderRange.userData = "elevator"
end
function platform:returnColision()
	return self.renderRange
end
function platform:cUpdate()
	local width = self._width
	local height = self._height
	self.renderRange:moveTo(self.x,self.y - height/2,width,height)
end
function platform:colFunc(func)
	self.onCol = func
end
function platform:draw(drawcol)
	local x = math.random(1,10)
	if self.drawPath and drawcol then
		for i in ipairs(self.floor) do
			local x = self.floor[i].x
			local y = self.floor[i].y
			local w = self.width/2
			local h = self.height/2
			love.graphics.rectangle("fill",x-w/2,y-h/2,self.width/2,self.height/2)
			if self.floor[i + 1] ~= nil then
				local x1 = self.floor[i + 1].x
				local y1 = self.floor[i + 1].y
				love.graphics.line(x,y,x1,y1)
			end
		end
	end
	for i in ipairs(self.floors) do
		local x = self.floors[i].x
		local y = self.floors[i].y
		self.floors[i]:draw("fill")
		if self.floors[i + 1] then
			local x1 = self.floors[i+1].x
			local y1 = self.floors[i+1].y
			love.graphics.line(x,y,x1,y1)
		end
	end
	if self.rail then
		for i in ipairs(self.floor) do
			local x = self.floor[i].x - self.floor[i].ofx
			local y = self.floor[i].y - self.floor[i].ofy
			local r = self.floor[i].r
			local x2 = self.rail.image:getWidth()
			local y2 = self.rail.image:getHeight()
			if self.floor[i].quad then
				love.graphics.draw(self.rail.image,self.floor[i].quad,x,y,r,self.scalex,self.scaley,0,y2/2)
			end
		end
	end
	if self.body then
		if self.railEnd or self.railConnector then
			for i in ipairs(self.floor) do
				local x = self.floor[i].x
				local y = self.floor[i].y
				local r = self.floor[i].r
				if self.railEnd and self.floor[i+1] == nil or i == 1 then
					local ox = self.railEnd.ox + self.railEnd.image:getWidth()/2
					local oy = self.railEnd.oy + self.railEnd.image:getHeight()/2
					if self.floor[i+1] == nil then
						r = r + math.rad(180)
					else
						r = r + math.rad(180)
					end
					love.graphics.draw(self.railEnd.image,x,y,r,self.scalex,self.scaley,ox,oy)
				elseif self.railConnector then
					local ox = 0
					local oy = 0
					if self.floor[i].cdata then
						ox = self.floor[i].cdata.ox
						oy = self.floor[i].cdata.oy
					end
					if self.floor[i].connect then
						love.graphics.draw(self.floor[i].connect,x,y,0,self.scalex,self.scaley,ox,oy)
					else
						local img = self.railConnector.image
						love.graphics.draw(img,x,y,0,self.scalex,self.scaley,img:getWidth()/2,img:getHeight()/2)
					end
				end
			end
		end
	end
	for i in ipairs(self.items) do
		local x = self.items[i].x
		local y = self.items[i].y
		local x2 = self.x
		local y2 = self.y
		if self.body then
			x2 = self.body:getX()
			y2 = self.body:getY()
		end
		if self.items[i].type == "image" then
		local w = (self.items[i].image:getWidth())*self.scalex
		local h =  (self.items[i].image:getHeight())*self.scaley
			love.graphics.draw(self.items[i].image,x2,y2 ,0,self.scalex,self.scaley,w/2,h)
		elseif self.items[i].type == "animation" then
			self.items[i]:draw(x + x2,y + y2,self.scalex,self.scaley)
		end
	end
	if drawcol then
		if self.renderRange then
			self.renderRange:draw("line")
		end
		if self.body then
			love.graphics.push()
			love.graphics.translate(self.body:getX(),self.body:getY())
			love.graphics.setColor(250,50,50)
			for i in ipairs(self.shapes) do
				love.graphics.polygon("fill",self.shapes[i]:getPoints())
			end
			love.graphics.polygon("fill",self.shape:getPoints())
			love.graphics.pop()
		end
		for i in ipairs(self.colisions) do
			love.graphics.setColor(10*i,0,0)
			self.colisions[i]:draw("line")
		end
		love.graphics.setColor(255,255,255)
	end
	if self.onDraw then
		self.onDraw(self)
	end
end
function platform:keypressed(key)
	if self.keyp then
		self.keyp(key,self)
	end
end
function platform:keyreleased(key)
	if self.keyr then
		self.keyr(key,self)
	end
end
function platform:setPause(time)
	self.pause = time
end
function platform:drawUI(contact)
	if self.hasUi then
		if self.contact or not contact then
			self.ui(self)
		end
	end
end
function platform:update(dt,condition)
	local des = self.des
	local body = self.body
	local current = self.current
	local fulse
	if des ~= self.current and self.floor[des] ~= nil then
		if not self.cycle2 then
			local direction = 1
			if des - current < 0 then
				direction = -1
			end
			local tables = {}
			for i = current,des,direction do
				if self.floor[i].x == self.floor[current].x and self.floor[i].y ~= self.floor[current].y then
					des = i
					table.insert(tables,i)
					fulse = true
				elseif self.floor[i].y == self.floor[current].y and self.floor[i].x ~= self.floor[current].x then
					des = i
					fulse = true
					table.insert(tables,i)
				end
			end
			if direction < 0 then
				des = math.ceil(math.min(unpack(tables)))
			else
				des = math.floor(math.max(unpack(tables)))
			end
		end
		
		if not fulse or self.cycle2 then
			local val = self.des - self.current
			if val == 1 then
				des = self.des
			else
				if val < 0 then
					des = self.current - 1 
				elseif val > 0 then
					des = self.current + 1
				end
			end
		end
		local cx = body:getX()
		local cy = body:getY()
		local nx = nil
		local ny = nil
		local dx = self.floor[des].x
		local dy = self.floor[des].y
		dx = math.floor(dx)
		dy = math.floor(dy)
		cx = math.floor(cx)
		cy = math.floor(cy)
		if not self.active then
			if dy ~= cy or dx ~= cx then
				self.timer = self.timer + 0.1
			end
		end
		if self.timer >= self.pause then
			self.active = true
			self.timer = 0
		end
		if self.smooth and self.active and not self.clamp then
			local difx = 0
			local dify = 0
			if dy ~= cy or dx ~= cx then 
				difx = dx - cx
				dify = dy - cy
				nx = difx*(self.speed/100)
				ny = dify*(self.speed/100)
				if nx or ny then
					local xvl = nx or 0
					local yvl = ny or 0
					self.body:setLinearVelocity(xvl,yvl)
				end
			end
			if dy == cy and dx == cx then
				self.current = des
				self.active = false
				self.body:setLinearVelocity(0,0)
			end
		end
			

	if not self.smooth and self.active then
			local body = self.body
			body:setLinearVelocity(0,0)
			local x,y = body:getPosition()
			local dx = self.floor[des].x
			local dy = self.floor[des].y
			local cx = self.floor[self.current].x
			local cy = self.floor[self.current].y
			local difx = dx - cx
			local dify = dy - cy
			local ratx,raty
			if difx == 0 or dify == 0 then
				ratx = 1
				raty = 1
			else
				ratx = 1
				raty = tonumber(dify/difx)
			end
			local speed = self.speed
			local a = speed/(math.abs(raty)+math.abs(ratx))
			local speedy = (a*math.abs(raty))
			local speedx = (a*math.abs(ratx))
			local dx = self.floor[des].x
			local dy = self.floor[des].y
				if x ~= dx then
					if x - dx > 0 then
						speedx = -speedx 
					elseif x - dx < 0 then
						speedx = speedx
					else
						speedx = 0
					end
				end
				if y ~= dy then
					if y - dy > 0 then
						speedy = -speedy
					elseif y - dy < 0 then
						speedy = speedy
					else
						speedy = 0
					end
				end
			body:setLinearVelocity(speedx,speedy)
			dy,dx = body:getPosition()
			if dy == cy and dx == cx then
				self.current = des
				self.active = false
				self.body:setLinearVelocity(0,0)
			end
		end
		if dy == cy and dx == cx then
			self.current = des
			self.active = false
			self.body:setLinearVelocity(0,0)
		end
	end
	for i in ipairs(self.colisions) do
		self.colisions[i]:moveTo(body:getX() - self.colisions[i].modx,body:getY() - self.colisions[i].mody)
	end
	if self.active then
		self.status = "moving"
	else
		self.status = "still"
	end
	if self.onUpdate then
		self.onUpdate(dt,self)
	end
end
function platform:goTo(floor)
	self.des = floor
	assert(self.floor[floor],"The floor: "..floor.." is nil")
end
function platform:cycle(floor1,floor2)
	self.cycle2 = true
	if self.current == floor1 then
		self.des = floor2
	elseif self.current == floor2 then
		self.des = floor1
	end
end
function platform:destroy(phyWorld)
	Collider:remove(self.colision)
	self.body:remove()
	self = nil
end
function platform:Hstop(dt,a,b)
	for i in ipairs(self.colisions) do 
		local other
		if a == self.colisions[i] then
			other = b
		elseif b == self.colisions[i] then
			other = a
		end
		if other ~= nil and other.userData ~= nil then 
			if string.find(other.userData,"p_underline") then
			end
		end
	end
end
function platform:HCol(dt,a,b)
	for i in ipairs(self.colisions) do
		local other
		if a == self.colisions[i] then
			other = b
		elseif b == self.colisions[i] then
			other = a
		end
		if other ~= nil and other.userData ~= nil then
			local x1,y1,x2,y2 = self.colisions[i]:bbox()
			if string.find(other.userData,"p_underline") then
				local xvel,yvel = other.body:getLinearVelocity()
				local xvl,yvl = self.body:getLinearVelocity()
				-- check if player is moving or jumping
				if love.keyboard.isDown("a") or  love.keyboard.isDown("d") then
					xvl = xvel
				end
				if love.keyboard.isDown(" ") then
					yvl = yvel
				end
				other.body:setLinearVelocity(xvl,yvl)
			end
		end
	end
end
function platform:setRail(image,sx,sy)
	self.rail = {}
	self.rail.image = image
	self.rail.image:setWrap("repeat")
	self.rail.sx = sx or self.scalex
	self.rail.sy = sy or self.scaley
end
function platform:setRailConnector(image,ox,oy)
	self.railConnector = {}
	self.railConnector.image = image
	self.railConnector.ox = ox or image:getWidth()/2
	self.railConnector.oy = oy or image:getHeight()/2
end
function platform:rebuildRails()
		local w = self.rail.image:getWidth()
		local h = self.rail.image:getHeight()
		if self.rail then
			for i in ipairs(self.floor) do
				local x = self.floor[i].x
				local y = self.floor[i].y
				local no = i + 1 
				if self.floor[no] ~= nil then
					local x1 = self.floor[i + 1].x
					local y1 = self.floor[i + 1].y
					local w2 = x - x1
					local h2 = y - y1
					if w2 ~= 0 or h2 ~= 0 then
						self.floor[i].r = math.atan(h2/w2)
						self.floor[i].ofx = w2
						self.floor[i].ofy = h2
						if w2 < 0 then
							self.floor[i].ofx = 0
							self.floor[i].ofy = 0
						end
						w2 = math.sqrt(w2^2 + h2^2)
						self.floor[i].quad = love.graphics.newQuad(0,0,w2/self.scalex,h,w,h)
					else
						if h2 == 0 then
							self.floor[i].ofx = 0
							self.floor[i].r = 0
						end
						if w2 == 0 then
							self.floor[i].ofx = 0
							w2 = (w*self.scalex)
							self.floor[i].r = math.rad(90)
						end
						
						local mx = math.sqrt(w2^2 + h2^2)
						if not self.floor[i].ofx then
							self.floor[i].ofx = w2
						end
						if not self.floor[i].ofy then
							self.floor[i].ofy = h2
						end
						self.floor[i].quad = love.graphics.newQuad(0,0,mx/self.scalex,h,w,h)
					end
				else
					self.floor[i].quad = false
					self.floor[i].ofx = 0
					self.floor[i].ofy = 0
					self.floor[i].r = 0
				end
			end
		end
		if self.railConnector and self.doAngles then
			for i in ipairs(self.floor) do
				local no = i + 1
				if self.floor[no] ~= nil then
					local primR = math.max(self.floor[i].r,self.floor[no].r)
					local w = (self.railConnector.image:getWidth())
					local h = (self.railConnector.image:getHeight())
					local w = w*math.sin(primR) + h*math.cos(primR)
					local h = h*math.sin(primR) + w*math.cos(primR)
					if tableLenght(self.floor) > 1 then
						w = self.width*self.scalex
						h = self.height*self.scaley
					end
					local can = love.graphics.newCanvas(w,h)
					love.graphics.setCanvas(can)
						can:clear()
						local ox = self.railConnector.ox
						local oy = self.railConnector.oy
						love.graphics.draw(self.railConnector.image,w/2,h/2,self.floor[i].r,1,1,ox,oy)
						love.graphics.draw(self.railConnector.image,w/2,h/2,self.floor[no].r,1,1,ox,oy)
					love.graphics.setCanvas()
					self.floor[i].connect = love.graphics.newImage(can:getImageData())
					self.floor[i].cdata = {}
					self.floor[i].cdata.ox = self.floor[i].connect:getWidth()/2
					self.floor[i].cdata.oy = self.floor[i].connect:getHeight()/2
				end
			end
		end
end
function platform:setRailEnd(image,ofx,ofy,sx,sy)
	self.railEnd = {}
	self.railEnd.image = image
	self.railEnd.ox = ofx or 0
	self.railEnd.oy = ofy or 0
	self.railEnd.sx = sx or self.scalex
	self.railEnd.sy = sy or self.scaley
end
function platform:updateFloor(floor,x,y)
	self.floors[floor].x = x
	self.floors[floor].y = y
	self.floors[floor].src:moveTo(x,y)
end
function platform:addFloor(x,y)
	local col = Collider:addRectangle(x - self.width/2,y-self.height/2,self.width,self.height)
	col.userData = "elevator_floor"
	col.src = col
	col.x = x
	col.y = y
	local id = tableLenght(self.floors) + 1
	if id == 0 then
		id = 1 
	end
	col.src2 = id
	col.source = self
	table.insert(self.floors,col)
end
function platform:returnFloors()
	return self.floors
end 
function platform:remove()
	if self.onRemove then
		self.onRemove(self)
	end
	if self.body then
		self.body:destroy()
	end
	if self.colisions then
		for i in ipairs(self.colisions) do
			Collider:remove(self.colisions[i])
		end
	end
	if self.renderRange then
		Collider:remove(self.renderRange)
	end
	if self.colision then
		Collider:remove(self.colision)
	end
	for i in ipairs(self.floors) do
		Collider:remove(self.floors[i])
	end
end
function platform:setPosition(x,y)
	self.x = x 
	self.y = y
end
door = {}
door.__index = door
function door:new(x,y,zmap,width,height,scalex,scaley)
	local object = {
		x = x or 0,
		y = y or 0,
		width = width or 20,
		height = height or 150,
		body = false,
		range = false,
		door = false,
		image = false,
		image2 = false,
		scalex = 1,
		scaley = 1,
		shape = false,
		free = false,
		status = "closed",
		fixture = nil,
		zmap = zmap or 1,
		itype = "none",
		flip = false,
		animation = false,
		nRange = true,
		userData = "door"
	}
	return setmetatable(object,door)
end
function door:setFlip(bool)
	self.flip = bool
end
function door:setImage(image,image2,fw,fh,sx,sy)
	self.itype = "image"
	self.image_o = image
	self.image_c = image2
	self.scalex = sx or 1
	self.scaley = sy or 1
	local w = fw or image:getWidth()
	local h = fh or image:getHeight()
	self.width = w*self.scalex
	self.height = h*self.scaley
end
function door:setAnimation(animation,closeFrame,openFrame,fw,fh,sx,sy)
	local sx = sx or self.scalex
	local sy = sy or self.scaley
	self.itype = "animation"
	self.scalex = sx
	self.width = fw or animation:getWidth()
	self.height = fh or animation:getHeight()
	self.image_c = closeFrame or 1
	self.image_o = openFrame or animation:getSize()
	animation:setMode("once")
	animation:stop()
	animation:reset()
	self.animation = animation
end
function door:update(dt)
	local anim = self.animation
	local mask = self.fixture:getMask()
	if anim then
		if anim:getCurrentFrame() == self.image_c and mask ~= 1 then
			self.fixture:setMask(1)
		end
		if anim:getCurrentFrame() == self.image_o and mask ~= 16 then
			self.fixture:setMask(16)
		end
		anim:update(dt)
	end
	if self.onUpdate then 
		self.onUpdate(dt,self)
	end
end
function door:makeBody(phy,width,height)
	local x = self.x
	local y = self.y
	local world = phyWorld
	local nwidth = width or self.width*4
	local nheight = height or self.height
	self.range = Collider:addRectangle(x - nwidth/2,y - nheight/2,nwidth,nheight)
	self.door = Collider:addRectangle(x - self.width/2,y- self.height/2,self.width,self.height)
	if phy and world then
		self.body = love.physics.newBody(world,x,y)
		local shape = love.physics.newRectangleShape(self.width,self.height)
		self.shape = shape
		self.fixture = love.physics.newFixture(self.body,shape)
		local salf = tostring(self)
		self.fixture:setUserData("door")
	end
	self.range.userData = "door_range"
	self.door.userData = "door"
	self.door.source = self
end
function door:draw(wireframe,mirror,reflect)
	local scalex = self.scalex*(mirror or 1)
	local scaley = self.scaley*(reflect or 1)
	if wireframe then
		self.range:draw("line")
		if self.status == "closed" then
			love.graphics.setColor(5,200,100)
		else
			love.graphics.setColor(0,0,255)
		end
		self.door:draw("fill")
		if self.shape then
			love.graphics.push()
			love.graphics.translate(self.x,self.y)
			love.graphics.setColor(255,0,0)
			love.graphics.polygon("line",self.shape:getPoints())
			love.graphics.pop()
		end
		love.graphics.setColor(255,255,255)
	end
	local image
	if self.status == "closed" and self.image_c then
		image = self.image_c
	elseif self.status == "open" and self.image_o then
		image = self.image_o
	end
	if self.itype == "image" then
		love.graphics.draw(image,self.x,self.y,0,scalex,scaley,(self.width/2)*math.abs(self.scalex),(self.height/2)*math.abs(self.scaley))
	elseif self.itype == "animation" then
		self.animation:draw(self.x,self.y,0,scalex,scaley,(self.width/2)*math.abs(self.scalex),(self.height/2)*math.abs(self.scaley))
	end
	if self.onDraw then
		self.onDraw(self,dt)
	end
end
function door:open(force)	
	local x = 0
	local y = 0
	local clamp
	self.free = true 
	local range = false
	for other in pairs(self.door:neighbors()) do
		if other ~= self.range and self.door:collidesWith(other) then
			if other.zmap ~= nil and self.zmap == other.zmap then
				self.free = false
			end
		end
	end
	if self.free then
		for other in pairs(self.range:neighbors()) do
			if self.range:collidesWith(other) then
				if other.userData and string.find(other.userData,"player") and other.zmap == self.zmap then
					x,y = other.source:getPos()
					if self.x < x then
						if self.scalex < 0 then
							self.scalex = -self.scalex
						end
					end
					if self.x > x then
						if self.scalex > 0 then
							self.scalex = -self.scalex
						end
					end
					if self.itype == "animation" then
						clamp = true
					end
					range = true
				end			
			end
		end
	end
	if self.itype == "animation" and force then
		clamp = true
	end
	if range or not self.nRange or force then
		if self.free or force then
			if self.status == "closed" then
				self.status = "open"
			end
			if clamp then
				self.animation:setMode("once")
				self.animation:play()
			else
				self.fixture:setMask(16)
			end
		end
	end
end
function door:close(force)
	local clamp
	self.free = true 
	local range = false
	for other in pairs(self.door:neighbors()) do
		if other ~= self.range and self.door:collidesWith(other) then
			if other.zmap ~= nil and self.zmap == other.zmap then
				self.free = false
			end
		end
	end
	if self.free then
		for other in pairs(self.range:neighbors()) do
		if self.range:collidesWith(other) then
			if other and other.userData and string.find(other.userData,"player") and other.zmap == self.zmap  then
				range = true
				if self.itype == "animation" then
					clamp = true
				end
			end
		end
	end
	end
	if self.itype == "animation" and force then
		clamp = true
	end
	if range or not self.nRange or force then
		if self.free or force then
			if self.status == "open" then
				self.status = "closed"
			end
			if clamp then
				self.animation:setMode("reverse2")
				self.animation:play()
			else
				self.fixture:setMask(1)
			end
		end
	end
end
function door:moveTo(x,y)
	if self.body then
		self.body:setPosition(x,y)
		local body = self.body
	end
	self.x = x
	self.y = y
	if self.range then
		self.range:moveTo(self.x,self.y)
	end
	if self.door then
		self.door:moveTo(self.x,self.y)
	end
end
function door:needsRange(bool)
	self.nRange = bool
end
function door:keypressed(key,condition)
	if condition == nil then
		condition = true 
	end
end
function door:remove()
	if self.body and not self.body:isDestroyed() then
		self.body:destroy()
	end
	if self.door then
		Collider:remove(self.door)
	end
	if self.range then
		Collider:remove(self.range)
	end
end
function door:setPos(x,y)
	self:moveTo(x,y)
end
lamp = {}
lamp.__index = lamp
function lamp:new(lamp,x,y)
	local object = {
		x = x or 0,
		y = y or 0,
		width = 0,
		height = 0,
		light = false,
		image = false,
		colision = false
	}	
	return setmetatable(object,platform)
end
function lamp:draw(wireframe)
end
function lamp:addShape(shape)
	local shape,x,y = shape(self)
	if not shape:getPoints() then
		error("invalid shape")
	end
	local body = self.body
	local fixture = love.physics.newFixture(self.body,shape)
	fixture:setUserData("lamp")
	local colision = Collider:addPolygon(shape:getPoints())
	colision:moveTo(body:getX(),body:getY())
	colision.body = self.body
	colision.fparent = self
	colision.userData = "lamp"
	colision.modx = x
	colision.mody = y
	table.insert(self.colisions,colision)
	table.insert(self.shapes,shape)
	
end
function lamp:setImage(image)	
	self.image = image
	self.width = lamp:getWidth()
	self.height = lamp:getHeight()
end
function lamp:setLight(light,x,y)
	local light,x,y = light(self)
	self.light = {}
	self.light.body = light
	self.light.ox = ox or 0
	self.light.oy = oy or 0
end
function lamp:makeBody()
	if self.shape then
		self.colision = Collider:newPolygon(self.shapes[1]:unpack())
	else
		self.colision = Collider:newRectangle(self.x,self.y,self.width,self.height)
		self.colision.source = self
	end
end
function lamp:update(dt)
end

zdoor = {}
zdoor.__index = zdoor
function zdoor:new(x,y,w,h,mode,zmap,loc)
	local object = {
		x = x,
		y = y,
		w = w,
		h = h,
		loc = loc or "maps",
		mode = mode,
		name = nil,
		links = {},
		zmap = zmap,
		type = "zdoor"
	}
	setmetatable(object,zdoor)
	object:makeColision()
	return object
end
function zdoor:setGame(game)
	self.game = game
end
function zdoor:makeColision()
	local x,y,w,h = self.x,self.y,self.w,self.h
	self.colision = Collider:addRectangle(x,y,w,h)
	self.colision.userData = "zdoor"
	self.colision.source = self
	self:moveTo(x,y)
end
function zdoor:moveTo(x,y)
	self.colision:moveTo(x,y)
end
function zdoor:update(dt)
	if self.onUpdate then
		self.onUpdate(dt)
	end
end
function zdoor:setLoc(loc)
	self.loc = loc
end
function zdoor:keypressed(key)
	if self:inRange() then
		if self.child and key == "w" then
			self:transfer(self.child,self.loc.."/children","child")
		end
		if self.parent and key == "s" then
			self:transfer(self.parent,nil,"parent")
		end
		
	end
end
function zdoor:transfer(map,loc,mode)
	if loc then 
		current_map = loc.."/"..map
	else
		current_map = map
	end
	transferProtocol = {
		active = current_map,
		from = self.name,
		to = self.links[mode]
	}
	if current_gamestate == "editor" then
		Gamestate.switch(editor)
	else
		-- if self.game then
			-- self.game:save()
		-- end
		Gamestate.switch(testRun)
	end
end
function zdoor:ui(menu,object)
	local menu2 = loveframes.Create("menu")
	menu:AddSubMenu("Set..",false,menu2)
	if self.child then
		menu:AddOption("MoveThrough[child]",false,function() 
			self.loc = object.map
			object.save_button:click()
			self:transfer(self.child,self.loc.."/children","child")
		end)
	end
	if self.parent then
		menu:AddOption("MoveThrough[parent]",false,function() 
			self.loc = object.map
			object.save_button:click()
			self:transfer(self.parent,nil,"parent")
		end)
	end
	menu2:AddOption("Size",false,function() 
		local frame = loveframes.Create("frame")
		frame:SetSize(200,130)
		frame:Center()
		local set = loveframes.Create("button",frame)
		local text = loveframes.Create("text",frame)
		text:SetPos(5,30)
		text:SetText("Width:")
		local text = loveframes.Create("text",frame)
		text:SetPos(5,60)
		text:SetText("Height:")
		local numberbox = loveframes.Create("numberbox",frame)
		numberbox:SetValue(self.w)
		numberbox:SetPos(70,30)
		local numberbox2 = loveframes.Create("numberbox",frame)
		numberbox2:SetValue(self.h)
		numberbox2:SetPos(70,60)
		set:SetText("Set")
		set.OnClick =function()
			local w = numberbox:GetValue()
			local h = numberbox2:GetValue()
			self:setSize(w,h)
			frame:Remove()
		end 
		set:SetPos(frame.width/2 - set.width/2,frame.height - set.height - 5)
	end)
	menu2:AddOption("Name",false,function()
		local frame = loveframes.Create("frame")
		frame:SetSize(150,100)
		frame:Center()
		frame:SetName("Change name")
		local textinput = loveframes.Create("textinput",frame)
		textinput:SetPos(5,30)
		textinput:SetText(self.name)
		textinput:SetSize(140,20)
		local set = loveframes.Create("button",frame)
		set:SetText("set")
		set:SetPos(frame.width/2 -set.width/2,frame.height - 10 - set.height)
		set.OnClick = function()
			self:setName(textinput:GetText())
			frame:Remove()
		end
	end)
	menu2:AddOption("Child",false,function()
		local child = {}
		local frame = loveframes.Create("frame")
		frame:SetSize(210,220)
		frame:Center()
		local list = loveframes.Create("list",frame)
		list:SetPos(5,30)
		list:SetSize(200,150)
		local lfs = love.filesystem
		local mc = loveframes.Create("multichoice",frame)
		mc:SetPos(5,190)
		mc:AddChoice("maps")
		if lfs.exists(object.map.."/children") then
			mc:AddChoice(object.map.."/children")
		end
		local dir = love.filesystem.getSaveDirectory( )
		mc.OnChoiceSelected = function(object, choice)
			list:Clear()
			local files = lfs.getDirectoryItems(choice)
			local savedLoc = object:GetChoice()
			print(savedLoc)
			for i in ipairs(files) do
				local button = loveframes.Create("button")
				button:SetText(tostring(files[i]))
				button.OnClick = function(object)
					child = object:GetText()
						print(savedLoc.."/"..child.."/entities")
						if love.filesystem.exists(savedLoc.."/"..child.."/entities") then
						print("Exists!")
						local entity = Tserial.unpack(love.filesystem.read(savedLoc.."/"..child.."/entities"))
						if entity["zdoor"] then
							local frame = loveframes.Create("frame","Set child door")
							frame:SetSize(210,220)
							frame:Center()
							local list = loveframes.Create("list",frame)
							list:SetPos(5,30)
							list:SetSize(200,150)
							for i in ipairs(entity["zdoor"]) do
								local var = entity["zdoor"][i]
								local button = loveframes.Create("button")
								local name = var.name 
								print(name)
								if name then
									button:SetText(var.name)
								else
									button:SetText("Not Named")
									button._fNamed = true
								end
								button.OnClick = function(object)
									if not object._fNamed then
										self.links["child"] = button:GetText()
									end
									self:setChild(child)
									frame:Remove()
								end
								list:AddItem(button)
							end
							local button = loveframes.Create("button")
							button:SetText("Nil")
							list:AddItem(button)
							button.OnClick = function(object)
								if not object._fNamed then
									self.links["child"] = button:GetText()
								end
								self:setChild(child)
								frame:Remove()
							end
						end
					end
					frame:Remove()
				end 
				list:AddItem(button)
			end
		end
		mc:SelectChoice("maps")
	end)
	if string.find(object.map,"children") then
		local map = string.reverse(object.map)
		local a,b = string.find(map,"/nerdlihc/")
		local leng = string.len(map)
		local h2 = string.sub(map,b + 1,leng)
		map = string.reverse(h2)
		parent = map
		menu2:AddOption("Parent",false,function()
			local frame = loveframes.Create("frame")
			frame:SetSize(210,220)
			frame:Center()
			local list = loveframes.Create("list",frame)
			list:SetPos(5,30)
			list:SetSize(200,150)
			local lfs = love.filesystem
			local button = loveframes.Create("button")
			button:SetText(parent)
			button.OnClick = function(object)
				self:setParent(parent)
							print(parent.."/entities")
							if love.filesystem.exists(parent.."/entities") then
								local entity = Tserial.unpack(love.filesystem.read(parent.."/entities"))
								if entity["zdoor"] then
									local frame = loveframes.Create("frame","Set parent door")
									frame:SetSize(210,220)
									frame:Center()
									local list = loveframes.Create("list",frame)
									list:SetPos(5,30)
									list:SetSize(200,150)
									for i in ipairs(entity["zdoor"]) do
										local var = entity["zdoor"][i]
										local button = loveframes.Create("button")
										local name = var.name 
										if name then
											button:SetText(var.name)
										else
											button:SetText("Not Named")
											button._fNamed = true
										end
										button.OnClick = function(object)
											if not object._fNamed then
												self.links["parent"] = button:GetText()
											end
											self:setParent(parent)
											frame:Remove()
										end
										list:AddItem(button)
									end
									local button = loveframes.Create("button")
									button:SetText("Nil")
									list:AddItem(button)
									button.OnClick = function(object)
										if not object._fNamed then
											self.links["parent"] = button:GetText()
										end
										self:setParent(parent)
										frame:Remove()
									end
								end
							end
				frame:Remove()
			end 
			list:AddItem(button)
		end)
	end
	menu2:AddOption("Image",false,function() 
		local frame = loveframes.Create("frame")
		frame:SetResizable(true)
		frame:SetName("Select Image")
		frame:SetMaxWidth(object.width - 100)
		frame:SetMaxHeight(object.height - 20)
		frame:SetMinHeight(100)
		local source_choice = loveframes.Create("multichoice",frame)
		source_choice:SetPos(5,30)
		source_choice:SetWidth(150)
		frame:SetMinWidth(source_choice.width + 10)
		local sprite_list = loveframes.Create("list",frame)
		sprite_list:SetPos(5,60)
		sprite_list:SetSize(frame.width - 10,frame.height - 70)
		sprite_list:SetPadding(5)
		sprite_list:SetSpacing(25)
		sprite_list:SetSize(frame:GetWidth() - 10,frame.height - 70)
		
		
		frame.OnMouseEnter = function(object, width, height)
			sprite_list:SetSize(frame:GetWidth() - 10,frame.height - 70)
		end
		
		
		local tables2 = {}
		local src = "None"
		for i in ipairs(object.tableru) do
			local x = object.tableru[i].imagex
			local y = object.tableru[i].imagey
			local sx = object.tableru[i].scalex
			local sy = object.tableru[i].scaley
			local w = object.tableru[i].fw
			local h = object.tableru[i].fh
			local img = object.tableru[i].image
			local norm = object.tableru[i].nimage
			local glow = object.tableru[i].gimage
			local text = object.tableru[i].text
			src = img
			
			
			local fuxc = {}
			local ts = tileset:new(img,norm,glow)
			ts:setScale(fuxc,sx,sy)
			ts:setImage(fuxc,x,y,w,h)
			local imagebutton = loveframes.Create("imagebutton")
			imagebutton:SetImage(fuxc.image)
			local thisx = sprite_list:GetWidth()/fuxc.image:getWidth()
			local thisy = 70/fuxc.image:getHeight()
			if thisx ~= 1 or thisy ~= 1 then
				local cock = scalez(imagebutton:GetImage(),thisx,thisy)
				imagebutton:SetImage(cock)
				imagebutton:SetText(text)
			end
			imagebutton.OnClick = function(object)
				local image = crop(img,x,y,w,h,sx,sy)
				local normal = crop(norm,x,y,w,h,sx,sy)
				local glaw = crop(glow,x,y,w,h,sx,sy)
				local stack = 
				{
					img = img,
					norm = norm,
					glow = glow,
					x = x,
					y = y,
					w = w,
					h = h,
					sx = sx,
					sy,sy,
					
				}
				self:setImage(image,stack)
				if normal then 
					self:setNormal(normal)
				end
				if glaw then
					self:setGlow(glaw)
				end
				frame:Remove()
			end
			local x = {
				imagebutton = imagebutton,
				image = img
			}
			table.insert(tables2,x)
			sprite_list:AddItem(imagebutton)
		end
		local objectcategory = {}
		for i in ipairs(tables2) do
			for v in ipairs(objectcategory) do
				local a
				if tables2[i].img == objectcategory[v] then
					a = true
				end
				if not a then
					source_choice:AddChoice(objectcategory[v])
					local o = {
						name = self.tableru[v].image
						}
					table.insert(objectcategory,o)
				end
			end
		end
		source_choice:SelectChoice(src)
		
		source_choice.OnChoiceSelected = function(object, choice)
			sprite_list:Clear()
			for i in ipairs(objectcategory) do
				for v in ipairs(tables2) do
					if choice == tables2[v].image then
						sprite_list:AddItem(tables2[v].imagebutton)
					end
				end
			end
		end
		
	end)
	return menu2
end
function zdoor:setName(name)
	self.name = name
end
function zdoor:inRange()
	local x = self.x
	local y = self.y
	local x1 = x + self.w/2 
	local y1 = y + self.h/2
	local tables = Collider:shapesInRange(x,y,x1,y1)
	for shape in pairs(tables) do
		if shape.userData == "player" then
			return true
		end
	end
end
function zdoor:setChild(child)
	self.child = child
end
function zdoor:draw()
	love.graphics.setColor(0,0,0)
	self.colision:draw("fill")
	love.graphics.setColor(255,255,255)
	if self.image then 
		love.graphics.draw(self.image,self.x - self.w/2,self.y - self.h/2,0,1,1)
	end
end
function zdoor:remove()
	Collider:remove(self.colision)
end
function zdoor:setParent(parent)
	self.parent = parent
end
function zdoor:setImage(image,stack)
	self.w = image:getWidth()
	self.h = image:getHeight()
	self.image = image
	self:makeColision()
	if stack then
		self.stack = stack
	end
end
function zdoor:save()
	self.normal = nil
	self.glow = nil
	self.image = nil
end
function zdoor:setPos(x,y)
	self.x = x
	self.y = y
	self:moveTo(x,y)
end
function zdoor:setNormal(image)
	self.normal = image
end
function zdoor:setGlow(glow)
	self.glow = glow
end
function zdoor:setSize(w,h)
	self.w = w
	self.h = h 
	self:makeColision()
end









