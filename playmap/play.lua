local _PACKAGE = "editor"
local sa = require (_PACKAGE.. "/shapes")
local mapAdmin = require (_PACKAGE.. "/mapAdmin")
local layers = require (_PACKAGE.. "/layer")
local t = require(_PACKAGE.."/textures")

local imgMan = require(_PACKAGE.."/images")
local moduleMan = require(_PACKAGE.."/moduleAdmin")
local soundMan = require(_PACKAGE.."/soundAdmin")
local triggers = require(_PACKAGE.."/triggerAdmin")
local rand = require(_PACKAGE.."/random")
local textmanager = require(_PACKAGE.."/textmanager")
local cardManager = require("playmap/cardp")
local mobCards = require "ghostcards"

local ui = require "menu.ui"

local cam = require "hump.camera"
local menu = require("menu.init")

Radio = require("playmap/radio") 
local play = {}
function play:enter(prev,stack)
	
	LOCKCONTROLS = true 
	SHOWUI = true 
	print("ENTERING PLAY MODE")
	local ed 
	_currentGamestate = self
	LockKeypress = false
	self.OnLoaded = {}
	self.SaveStack = {}
	local MoveTarget 
	if stack then 
		map = stack["map"]
		ed = stack.Editor
		if stack["load_one"] then
			local s = stack["load_one"] 
			if type(s) == "string" then  
				if love.filesystem.exists("save_data/"..s) then 
					s = {Tserial.unpack(love.filesystem.read("save_data/"..s)).gameData,string.find(s,".troll")}
				else 
					s = nil 
				end 
			end 
			if s then 
				self:load_one(unpack(s))
				current_map = self.map 
			end 
		elseif stack["gameData"] then 
			self:load_one(stack["gameData"])
		end 
		MoveTarget = stack.target
	else 
		map = current_map or "maps/new"
		print("NO STACK PROVIDED!!!")
	end 
	self.map = stack["map"] or self.map
	local text,func = {},{}
	if ed then 
		text = {
			"Return to Editor"
		} 
		func = {
			function()
				Gamestate.switch(Editor,{map = self.map,x = stack.Editor.x,y = stack.Editor.y})
			end 
		}
	end 
	self.random = rand 
	self.ui = ui:init()
	local w,h = 95,128
	self.radio = Radio:new(self,love.graphics.getWidth()-(w/2),love.graphics.getHeight()-(h/2),w,h)
	Radio = self.radio
	self.menu = menu:init(text,func)
	self.gameMode = "play"
	self.old_gameMode = self.gameMode
	self.menu.OnRelease = function()
		self.gameMode = self.old_gameMode
	end 
	
	if phyWorld and not phyWorld:isDestroyed() then 
		phyWorld:destroy()
	end
	
	eCurrentMap = self.map or map
	local phyWorld = love.physics.newWorld(0,0)
	phyWorld:setCallbacks(function(...) self:beginContact(...) end,function(...) self:endContact(...) end,function(...) self:preSolve(...) end,function(...) self:postSolve(...) end)
	phyWorld:setSleepingAllowed(false)
	self.phyWorld = phyWorld
	love.physics.setMeter(3)
	
	local textures,textureManager,makeTexture = t[1],t[2],t[3]
	self.width,self.height = love.graphics.getDimensions()
	
	local shapeCol = function(...)
		self:HCol(...)
	end 
	local shapeStop = function(...)
		self:HStop(...)
	end 
	self._G = {}
	
	self.realView = true
	
	self.moduleMan = moduleMan
	self.shapeCollider = HC(200,shapeCol,shapeStop)
	self.mainCanvas = love.graphics.newCanvas(width,height)
	
	self.timer = Timer
	self.ui_keyControler = keyControl:new("ui") 
	self.play_keyControler = keyControl:new("game")
	
	self.textAdmin = textmanager:new(self,self.shapeCollider)
	
	self.textureMapper = textures
	
	self.triggerAdmin = triggers:new(self)
	
	self.imageAdmin = imgMan:new(self,self.shapeCollider,self.phyWorld)

	self.moduleAdmin = moduleMan:new(self,nil,self.shapeCollider,self.phyWorld)
	self.soundAdmin = soundMan:new(self,self.shapeCollider)
	
	
	self.shapeAdmin = sa:new(self,self.shapeCollider,nil,nil,self.phyWorld)
	
	self.textureManager = textureManager:new(self) 
	self.textureMapper = textures
	
	self.layers = layers:new(self)
	
	
	self.all_active = {}
	self.ActiveShapes = {}
	self.active_modules = {}
	self.priority = {}
	self.updateStack = {} 
	self.SaveData = {} 
	self.afterLoad = {} 
	
	self.cam = cam(self.width/2,self.height/2)
	self.scale = 1
		
	self.tweenGroup = Flux.group()
	if self.preLoad then 
		self.preLoad()
	end 
	mapAdmin.load(self,self.map)
	if self.OnLoad then 
		self.OnLoad()
	end 
	self.x = 0 
	self.y = 0
	
	self.gameMode = "play"
	self.CameraBox = self.shapeCollider:addRectangle(0,0,self.width + 200,self.height + 200)
	self.shapeCollider:update(0.02)
	local rt = self._G[MoveTarget]
	Timer.add(1,function()
		for i,v in ipairs(mapAdmin.parse(self.shapeCollider._active_shapes,self.shapeCollider._passive_shapes),self) do 
			if v.update then v:update(love.timer.getDelta()) end
		end 
	end) 
	for i,v in ipairs(mapAdmin.parse(self.shapeCollider._active_shapes,self.shapeCollider._passive_shapes),self) do 
		if v.update then v:update(love.timer.getDelta()) end
		mapAdmin.parseSingle(self,v:getColision(),0.02,self.active_modules)
	end 
	for i,v in ipairs(self.active_modules) do 
		if v then 
		if not self.upc then
			self.upc = v
		else
			if v.CameraPriority then 
				if self.upc.cameraStolen then self.upc:cameraStolen(dt) end 
				self.upc = v
			end 
		end 
		end 
	end
	if MoveTarget and rt then 
		local x,y = rt:getColision():bbox()
		if self.upc then self.upc:moveTo(x,y) end 
	end 
	self:intro()
		love.graphics.setDefaultFilter("nearest","nearest")
end 
function play:update(dt)
	if self.gameMode == "play" and self.phyWorld and not self.phyWorld:isDestroyed() then 	
		if not SHOWUI and self.hud then  
			for i,v in pairs(self.hud) do 
				v:remove()
			end 
			self.hud = nil 
		elseif SHOWUI and not self.hud then 
			self:makeUi()
		end 
		local x,y = self.x,self.y 
		if self.upc then 
			self.upc:update(dt) 
			x,y = self.upc:updateCamera(dt,misc.cameraSlide) 
		else 
			self.static_x = 0 
			self.static_y = 0 
		end 
		if self.cameraClamp then 
			x,y = self.cameraClamp:updateCamera(dt,misc.cameraSlide)
		end
		self.lastCords = {x,y}
		self.x = -((self.static_x or x) or 0) + self.width/2
		self.y = -((self.static_y  or y) or 0) + self.height/2
		self.CameraBox:moveTo(-self.x + self.width/2,-self.y + self.height/2)
	
		if Channel.MaxCapture > 0 then 
			Channel.MaxCapture = Channel.MaxCapture - 5*dt  
			self:tweenCapture()
		else 
			Channel.MaxCapture = 0 
			self:tweenReset()
		end 
		local c 
		self.radio:update(dt)
		if ST and ST:isRunning() then
			sav_tween.active = true
		else 
			c = true 
		end 
		sav_tween:update(self:getTween(),c,dt)
		self.cmx = x 
		self.cmy = y
		self.cam:zoomTo(self.scale)
		
		self.ui_keyControler:attach(dt)
		self.play_keyControler:attach(dt)
		
		self:navi(self.ui_keyControler,self.play_keyControler)
	
		self.soundAdmin:update(dt)
		
		self.layers:update()
		self.ui:update(dt)
		Collider:update(dt)
		if not self.shapeCollider then return false end 
		self.shapeCollider:update(dt)
		if self.skybox then self.skybox.update(dt) end 
		
		local accum = dt
		while accum > 0 do
			local dt = math.min(1/120,accum)
			accum = accum - dt
			if self.phyWorld and not self.phyWorld:isDestroyed() then 
				self.phyWorld:update(dt)
			end  
		end
		
		if not self.ActiveShapes then 
			return
		end 
		self.all_active = {} 
		self.ActiveShapes = {}
		self.active_modules = {} 
		mapAdmin.parseHead(self)
		if self.phyWorld and self.phyWorld:isDestroyed() then return end 
		for shape in pairs(self.CameraBox:neighbors()) do
			if shape:collidesWith(self.CameraBox) then 
				mapAdmin.parseSingle(self,shape,dt,self.active_modules,function(p)
					if p == self.upc then 
						return true
					end 
				end)
			end
		end
	
	
		Timer.update(dt)
		if not self.tweenGroup then 
			return 
		end 
		self.tweenGroup:update(dt)
		
		self.play_keyControler:detach(dt)
		self.play_keyControler:detach(dt)
		for i,v in pairs(self.updateStack) do 
			if v.update then v:update(dt) end
		end 
		
	else 
		Channel.MaxCapture = 0 
	end 
	if self.gameMode == "menu" then 
		self.menu:update(dt)
	else 
		self.old_gameMode = self.gameMode
	end 
end 
function play:render(d)
	local lg = love.graphics
	self.mainCanvas:clear()
	self.mainCanvas:renderTo(function()
		setColor()
		lg.push()
		lg.translate(self.x,self.y)
		lg.scale(self.scale)
		
		setColor(self.ambient_color or {20,50,90,255})
		if self.skybox then 
			self.skybox.draw(-self.x/self.scale,-self.y/self.scale)
		else 
			love.graphics.rectangle("fill",-self.x/self.scale,-self.y/self.scale,love.graphics.getWidth()/self.scale,love.graphics.getHeight()/self.scale)
			setColor()
		end 
		
		mapAdmin.draw_visible(self)
		self.CameraBox:draw("line")
		
		if d then 
			for i,v in pairs(self.all_active) do 
				v:getColision():draw("line")
			end 
		end 
		lg.pop()
		setColor()
	end)
end 
function play:draw()
	if self.gameMode == "play" then 
		self:render()
		self:drawIntro()
		love.graphics.draw(self.mainCanvas)
		self.ui:draw()
		if sav_tween.active then 
			sav_tween:draw()
		end 
		self:tweenDraw()
		love.graphics.setStencil()
	elseif self.gameMode == "menu" then 
		self.menu:draw()
	end 
	if misc.showFPS then 
		showFPS()
	end
	if DEBUG or STATS then 
		printStats(5,205)
	end 
	drawRelease()
end
function play:leave()
	print("LEAVING PLAY MODE")
	mapAdmin.clear(self.shapeCollider,self)
	if not self.phyWorld:isDestroyed() then self.phyWorld:destroy() end
	self.phyWorld = nil
	self.shapeCollider:clear()
	self.ui:clear()
	Timer.clear()
	if Collider then Collider:clear() end 
	for i,v in pairs(self) do 
		if type(v) ~= "function" then 
			self[i] = nil 
		end
	end 
	self.tweenGroup = nil
	Channel.MaxCapture = 0
end 
function play:getPlayer()
	return self.upc
end
function play:getScenes(map,intial)
	local map = map or self.map 
	local scenes = initial or {}
	local dir = map.."/children"
	for i,v in ipairs(love.filesystem.getDirectoryItems(dir)) do 
		local mod = Tserial.unpack(love.filesystem.read(dir.."/"..v.."/misc"))
		if mod.mode == "battle" then 
			table.insert(scenes,dir.."/"..v)
		end 
	end 
	return scenes 
end
function play:OnBattleMode(team,player,ownTeam,cards)
	self.gameMode = "Transition"
	self:save("one_use.troll",team)
		local s = self.map.."/scenes/"
		local map = "inbuilt_maps/scene"
		local _ = self:getScenes()
		if #_ >= 1 then 
			local found 
			local parents = getParents(self.map)
			for i,v in ipairs(parents) do  
				self:getScenes(v,_)
			end 
			if #_ >= 1 then  
				map = _[math.random(1,#_)]
			end 
		end
		Gamestate.switch(battle,{team,
		player,
		map = map,
		team1 = ownTeam,
		pickedCards = cards,
		cards = cards,
		OnFinish = function()
			Gamestate.switch(PlayMap,{load_one = {"one_use.troll"}})
		end
		})
end 
function play:save(name,team,all,ret)
	local t = {}
	t.map = self.map 
	t.SD = self.SaveData
	local modules = {} 
	mapAdmin.scanModules(self,self.shapeCollider,modules)
	local retMod  = {}
	local no = 0 
	print("STARTING MODULE RUN ")
	for i,v in ipairs(self.moduleAdmin.registrat) do 
		local btype = (v.type == "npc" or v.type == "player" or v.act.QuickSaveEnabled)
		local t 
		if btype then
			t = {} 
			local m = v.act
			local dead = m.isDead or m.dead 
			if team then 
				for i,v in pairs(team) do 
					print(m,v._host)
					if m == v._host then 
						dead = true 
					end 
				end 
			end 
			
			t = m:save()
			if t.y and t.x then 
				if t.y < 0 then 
					t.y = t.y + 10
				else									
					t.y = t.y - 10
				end 
			end 
			local inst 
			if m.LParent then 
				inst = m.LParent
			end
			t.REF = {module = v.module,own = v.own,inst = inst,insta = i}
			print(m.name,dead)
			t.dead = dead
		end 
		print("Save RangE:",v.act.save_game)
		if v.act.save_game then 
			if not t then t = {
				module = v.module,
				own = v.own,
				insta = i,
			} end 
			data = v.act:save_game()
		end
		if t then table.insert(retMod,t) end 
	end 
	if self.OnInterimSave then 
		for i,v in ipairs(self.OnInterimSave) do 
			v(retMod)
		end 
	end 		
	print("--END--")
	t.modSaves = retMod
	if name then 
		local d = Tserial.pack(t)
		if not love.filesystem.exists("save_data") then 
			love.filesystem.createDirectory("save_data")
		end 
		love.filesystem.write("save_data/"..name,d)
	else
		return t
	end 
end
function play:save_alt()
	local ret = {} 
	local t = {} 
	t.map = self.map 
	t.SaveData = self.SaveData
	assert(t.map,"Corrupted save!!")
	local modules = {} 
	mapAdmin.scanModules(self,self.shapeCollider,modules)
	for i,v in ipairs(modules) do
		local btype = (v.type == "npc" or v.type == "player" or v.QuickSaveEnabled)
		local t 
		if btype then
			t = {}  
			if v.save then 
				t = v:save()
			end 
			if btype then  t.y = v.y - 10 else t.y = v.y end 
			t.dead = t.dead 	
		end 
		if v.save_game then 
			t = {} 
			t.data = v:save_game()
		end 
		if t then 
			t.REF = v.___Reference 
			table.insert(ret,t)
		end 
	end 
	t.modSaves = ret
	return t
end 
function play:getUi()
	return self.ui
end 
function play:load_one(savefile,rem) -- save file is file name 
	self.SecondaryRun = true 
	self.OnInterimSave = {}
	local function load(t)
		self.map = t.map
		assert(t.map,"Corrupted save")
		print("LOADING MAP: "..self.map.." WITH SAVE: ")
		self.preLoad = function()
			self.SaveData = t.SD or {} 
		end
		self.OnLoad = function()
			--PRESISTENT DATA--
			for i,v in pairs(self.SaveData) do 
				print(i,v)
			end
			-- END -- 
			local modules = {}
			mapAdmin.scanModules(self,self.shapeCollider,modules)
			for i,v in pairs(t.modSaves) do 
				if v.REF and  v.REF.inst then 
					local m = self.moduleAdmin:seekInstance(v.REF.module,v.REF.own)
					if not v.dead then 
						m.actual:load(v)
					else 
						error("DEAD")
						print("ISDEAD:",v.REF.module,v.REF.own)
						local r = v.REF
						local ref = {module = r.module,own = r.own,inst = r.inst}
						table.insert(self.OnInterimSave,function(tables)
							assert(ref)
							local t = m:save() 
							t.dead = true
							t.REF = ref
							table.insert(tables,t)
						end)
						self.instances[v.REF.inst]:remove()
					end 
				end 
				for m,k in ipairs(modules) do 
					if k.___Reference then 
						if v.REF then 
							if true then 
								local owr = k.___Reference
								print("LODAING:",v.REF.module,v.REF.own,v.REF.insta,v.dead)
								if owr.module == v.REF.module and owr.own == v.REF.own and owr.insta == v.REF.insta then  
									k:load(v,v.dead)
									if k.load_game then 
										print(k.load_game)
										k:load_game(v.data)
									end		 	
									if v.dead then 
										k.isDead = v.dead
										table.insert(self.OnInterimSave,function(tables)
											local t = k:save()
											t.dead = true
											t.REF = v.REF
											table.insert(tables,t)
										end)
									end 
								end
							end 							
						end 
					end 
				end 
			end 
			self.OnLoad = nil 
		end 
		print("LOADED MAP: "..self.map.." WITH SAVE: "..tostring(savefile))
	end 
	if type(savefile) == "table" then 
		load(savefile)
	else
		if love.filesystem.exists("save_data/"..savefile) then 
			local t = Tserial.unpack(love.filesystem.read("save_data/"..savefile))
			load(t)
		end
		if rem or string.find(savefile,".troll") then 
			love.filesystem.remove("save_data/"..savefile)
		end 		
	end 
end 
function play:saveGame()
	return self:save()
end 
function play:loadGame(t)
	
end 
function play:navi(keyss,hKeys)
	LockKeypress = self.ui.textInputObject
	self.ui:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
	self.ui:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
end 
function play:makeUi()
	self.hud = {}
	local ui = self.ui
	local w,h = 250,50 
	local f = ui:addFrame(nil,w/2,h/2,w,h)
	table.insert(self.hud,f)
	f.color = basicFrameColor
	f.dontDraw = true 
	f.DrawChildren = true 
	local gap = 2 
	local iw,ih = (w - (2*5))/5,h
	local mobs = playersMobs
	for i=1,5 do 
		local b = ui:addButton(f,5+(iw+gap)*(i-1),0,iw,ih)
		b.dontDraw = true
		b.OnClick = function()
			mobCards(ui,mobs[i])
		end 
		if mobs[i] then 
			local w,h = b.width,b.height
			local nw,nh = mobs[i].width,mobs[i].height
			local sx,sy  = w/nw,h/nh
			b.OnDraw = function()
				local x,y = b.colision:bbox()
				mobs[i]:draw(x,y,0,sx,sy)
				local hp,mhp = mobs[i].info.hp,mobs[i].info.maxHp
				local pr = (w-5)/mhp
				local rw = pr*hp
				setColor(colors.red)
					love.graphics.rectangle("fill",x,y+b.height+2,rw,2)
				setColor()
				if hp <= 0 then 
					local image = icons["dead"]
					local x,y = b.colision:center()
					local w,h = image:getDimensions()
					love.graphics.draw(image,x,y,0,1,1,w/2,h/2)
				end 
			end 
			b.OnUpdate = function(dt)
				mobs[i]:update(dt)
			end 
		end 
	end 
	local b = ui:addButton(nil,40,90,80,60)
	b.dontDraw = true
	table.insert(self.hud,b)
	local img = love.graphics.newImage("Ghosts/cards/icon.png")
	b.OnClick = function()
		cardManager(ui)
	end 
	b.OnDraw = function()
		local cards = #playersCards
		local x,y = b.colision:bbox()
		local h = ui:getFont():getWidth("x "..cards) 
		local h2 = ui:getFont():getHeight()
		love.graphics.print("x "..cards,x + (b.width - h),b.y-h2/2)
		love.graphics.draw(img,x,y) 
	end 
	local img = icons["essence_small"]
	local w,h = img:getDimensions()
	local b = ui:addButton(nil,love.graphics.getDimensions() -w/2 - 5,h/2 +5,w,h)
	table.insert(self.hud,b)
	b.OnDraw = function()
		local x,y = b.colision:bbox()
		love.graphics.draw(img,x,y)
		local w = ui:getFont():getWidth(playerData.essence)
		love.graphics.print(playerData.essence,x-(w+4),y)
	end 
	b.dontDraw = true 
	
end 
function play:mousepressed(...)
	if self.gameMode == "play" then 
		self.ui:mousepressedList(...)
	end 
end
function play:keypressed(key)
	self.ui:keypressed(key)
	if not self.disableMenu and key == "escape"  and not self.menu.warning then 
		local menu  = self.menu
		if self.gameMode == "menu" then
			self.gameMode = self.old_gameMode
			menu:ends()
			self.menu.options = nil 
			self.menu.volutary = false 
		else
			self.gameMode = "menu"
			menu:start()
			self.menu.volutary = true 
		end
	end 
end 
function play:getType()
	return "play"
end 
function play:beginContact(a,b,coll)
	coll:setFriction(0)
	coll:setRestitution(0)
	for i,v in pairs(self.all_active) do 
		if v.beginContact then 
			v:beginContact(a,b,coll)
		end 
		if v.children then 
			for k,m in pairs(v.children) do 
				if m.beginContact then m:beginContact(a,b,coll) end 
			end 
		end 
		if v.__triggers then 
			for k,m in ipairs(v.__triggers) do 
				if m.beginContact then m:beginContact(a,b,coll) end
			end 			
		end
	end 
end 
function play:endContact(a,b,coll)
	coll:setFriction(0)
	coll:setRestitution(0)
	for i,v in pairs(self.all_active) do 
		if v.endContact then 
			v:endContact(a,b,coll)
		end 
		if v.children then 
			for k,m in pairs(v.children) do 
				if m.endContact then m:endContact(a,b,coll) end 
			end 
		end 
		if v.__triggers then 
			for k,m in ipairs(v.__triggers) do 
				if m.endContact then m:endContact(a,b,coll) end
			end 			
		end
	end  
end 
function play:preSolve(...)
	for i,v in pairs(self.all_active) do 
		if v.preSolve then 
			v:preSolve(a,b,coll)
		end 
		if v.children then 
			for k,m in pairs(v.children) do 
				if m.preSolve then m:preSolve(a,b,coll) end 
			end 
		end 
		if v.__triggers then 
			for k,m in ipairs(v.__triggers) do 
				if m.preSolve then m:preSolve(a,b,coll) end
			end 			
		end
	end  
end 
function play:postSolve(...)
	for i,v in pairs(self.all_active) do 
		if v.postSolve then 
			v:postSolve(a,b,coll)
		end 
		if v.children then 
			for k,m in pairs(v.children) do 
				if m.postSolve then m:postSolve(a,b,coll) end 
			end 
		end 
		if v.__triggers then 
			for k,m in ipairs(v.__triggers) do 
				if m.postSolve then m:postSolve(a,b,coll) end
			end 			
		end
	end 
end 
local function test(col)
	if col.getUserData then 
		return  true
	elseif col.editorShape and col.editorShape.__triggers then 
		return true 
	end 
end 
function play:HCol(dt,a,b,vx,vy)
	if test(a) and test(b) and self.all_active then 
		for i,v in pairs(self.all_active) do 
			if v.HCol then 
				v:HCol(dt,a,b,vx,vy)
			end 
			if v.children then 
				for k,m in ipairs(v.children) do 
					if m.HCol then m:HCol(dt,a,b,vx,vy) end 
				end 
			end 
			if v.__triggers then 
				for k,m in ipairs(v.__triggers) do 
					if m.HCol then m:HCol(dt,a,b,vx,vy) end
				end 			
			end 
		end
	end
end 
function play:HStop(dt,a,b,vx,vy)
	local shape 
	if a == self.CameraBox then 
		shape = b
	elseif b == self.CameraBox then 
		shape = a 
	end 
	if shape and shape.getUserData then 
		local d = shape.getUserData()
		local source = d.src or d.source
		if source and source.OnLoseFocus then source:OnLoseFocus() end 
	end 
	
	if test(a) and test(b) then 
		if self.all_active then 
			for i,v in pairs(self.all_active) do 
				if v.HStop then 
					v:HStop(dt,a,b,vx,vy)
				end
				if v.children then 
					for k,m in ipairs(v.children) do 
						if m.HStop then m:HStop(dt,a,b,vx,vy) end 
					end 
				end 
				if v.__triggers then 
					for k,m in ipairs(v.__triggers) do 
						if m.HStop then m:HStop(dt,a,b,vx,vy) end
					end 			
				end 
			end
		end
	end 
end 
function play:Hcol(...)
	if not self.ui then return end 
	self.ui:Hcol(...)
end
function play:Hstop(...)
	if not self.ui then return end 
	self.ui:Hstop(...)
end 
function play:worldPos(x,y)
	local x = (x-self.x)/self.scale
	local y = (y-self.y)/self.scale
	return x,y
end
function play:camPos(x,y)
	local x = x*self.scale + self.x
	local y = y*self.scale + self.y
	return x,y 
end 
function play:mousepressed(x,y,b)
	self.ui:mousepressedList(x,y,b)
end 
function play:textinput(t)
	self.ui:textinput(t)
end 
function play:parse()
	local a = mapAdmin.parse(self.shapeCollider._active_shapes,self.shapeCollider._passive_shapes)
	return a
end 
function play:getTween()
	return self.tweenGroup
end
function play:remove()
	mapAdmin.clear(self,self.shapeColider)
	Collider:clear()
	self.phyWorld:destroy()
	for i,v in ipairs(self) do 
		self[i] = nil 
	end 
	Channel.MaxCapture = 0
end 
function play:tweenCapture()
	local prc = math.floor(math.abs(Channel.MaxCapture*1000))
	local tween = self:getTween()
	--[[  Pos :
		{        [4]         }
		{[2]              [3]}
		{        [1]         }
 	]]--
	if not self._trectangles then 
		self._trectangles = {} 
	elseif findDistance(self.OldCapture or 0,prc) > 10 then 
		if (self.OldCapture or 0) < prc then 

			local w,h = love.graphics.getDimensions()
			local nw = (((w+h)/4) -(self._trectangles.width or 0))*prc
			local w = math.random(5,nw/100) 
			local x,y = 0,0
			local pos = math.random(1,4)
			if pos == 1 then 
				x = math.random(10,love.graphics.getWidth())
				y = math.random(10,love.graphics.getHeight()/3)
			elseif pos == 2 then 
				x = math.random(10,love.graphics.getWidth()/3)
				y = math.random(10,love.graphics.getHeight())
			elseif pos == 3 then 
				x = math.random(love.graphics.getWidth()/3,love.graphics.getWidth() -10)
				y = math.random(10,love.graphics.getHeight())
			elseif pos == 4 then 
				x = math.random(10,love.graphics.getWidth())
				y = math.random(love.graphics.getHeight()/3, love.graphics.getHeight()-10)
			end 
			local rectangle = {
				x = x,
				y = y,
				w = w,
				h = w,
				transp = 255,
			}
			table.insert(self._trectangles,rectangle)
			self._trectangles.width = (self._trectangles.width or 0) + w
			self.OldCapture = prc 
		else 
			local no = math.random(1,#self._trectangles)
			local rect = self._trectangles[no]
			if rect then 
				local w = rect.w
				if not rect.dead then 
					rect.dead = true 
					tween:to(rect,1,{transp = 0}):oncomplete(function()
						rect.confirm = true
						table.remove(self._trectangles,no)
						self._trectangles.width = (self._trectangles.width or 0) - w
					end)
				end 
			end
		end 
	end 
end 
function play:intro()
	self.intro_data = {
		x = love.graphics.getWidth()/2,
		y = love.graphics.getHeight()/2,
		r = 0,
	}
	self.tweenGroup:to(self.intro_data,3,{
		r = math.max(love.graphics.getHeight()/1.3,love.graphics.getWidth()/1.3),
	}):oncomplete(function()
		print("[- After Load")
		for i,v in ipairs(self.afterLoad) do 
			local t,a = pcall(v)
			print(t,a)
		end 
		print("-]")
		LOCKCONTROLS = false  
	end)
end 
function play:drawIntro()
	if LOCKCONTROLS then 
		setColor(colors.black)
		local v = self.intro_data
		love.graphics.setStencil(function()
			love.graphics.circle("fill",v.x,v.y,v.r)
		end)
	end 
	setColor()
end 
function play:tweenDraw()
	for i=1,#(self._trectangles or EmptyTable) do 
		local v = self._trectangles[i]
		local transp = v.transp
		local r,g,b = unpack(colors.black)
		setColor(r,g,b,transp)
		love.graphics.rectangle("fill",v.x,v.y,v.w,v.h)
		setColor()
	end 
end 
function play:clampCamera(lock)
	LOCKCONTROLS = lock 
	self.cameraClamp = self:getCamera()
	return self.cameraClamp 
end
local Cam = {}  
function play:getCamera()
	local s = {
		x = self.x,
		y = self.y,
	}
	if self.lastCords then 
		s.x = self.lastCords[1]
		s.y = self.lastCords[2]
	end 
	setmetatable(s,{__index = Cam})
	return s 
end
function Cam:updateCamera(dt,misc)
	return self.x,self.y
end
function play:releaseCamera()
	LOCKCONTROLS = false 
	self.cameraClamp = false 
end
function play:clearSelf()
	for i,v in pairs(self) do 
	 	if not type(v) == "function" then 
			self[i] = nil 
		end 
 	end 
end 
function play:tweenReset()
	local tween = self:getTween()
	if self._trectangles and self._trectangles[1] then 
		local found  
		for i,v in ipairs(self._trectangles) do 
			if not v.dead then
				v.dead = true 
				tween:to(v,0.2,{transp = 0}):oncomplete(function()
					v.confirm = true
				end)
				found = true 
			elseif not v.dead or not v.confirm then 
				found = true
			end 
		end 
		if not found then 
			self._trectangles = {}
			self.OldCapture = nil
		end 
	end 
end
function play:switchMap(dir)
	local gs = Gamestate.current()
	Gamestate.switch(gs,{map = dir})
end 
return play 