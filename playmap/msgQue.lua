local que = {} 
local mt = {__index = que}

function que:new(x,y,w,h)
	local self = {}
	local ui = Gamestate.current():getUi()
	local b = ui:addButton(nil,x,y,w,h)
	b.dontDraw = true 
	b.OnDraw = function() if self.Active and b.tooltip then b.tooltip:draw() end end 
	b:setInactive(true) 
	local c = ui:addTooltip(b,"")
	c.manual = true
	c.appearDelay = 0.5 
	c.disappearDelay = 0.5
	self.button = b 
	self.tooltip = c
	self.On = false 
	self.pos = 1
	self.que = {} 
	return setmetatable(self,mt)
end 
function que:setPosition(x,y)
	self.button:moveTo(x,y)
end 
function que:setText(t)
	print("Setting text: "..t)
	self.tooltip:setText(t) 
	self.TextDelay = math.max((string.len(t)/10)*(misc.textSpeed or 1),5)
end 
function que:update(dt)
	if self.pos ~= self.OldPos then 
		if self.que[self.pos] then 
			self.On = true
			self:setText(self.que[self.pos].txt)
			self.tooltip.TurnedOn = true
			Timer.add(self.TextDelay,function()
				self.tooltip.TurnedOn = false
				if self.que[self.pos].fn then self.que[self.pos].fn() end
				Timer.add(1,function() 
					self.pos = self.pos +1 
				end)
			end)
			self.OldPos = self.pos
		end 
	end 
	if self.OnUpdate then 
		self.OnUpdate(dt)
	end
end 
function que:cypher()
	local font = Glyhps
	self.tooltip.font = font 
end
function que:remove()
	self.button:remove()
end 
function que:add(msg,fn,t)
	if self.Active or t then
		print("Adding to new text que: "..msg)	
		table.insert(self.que,{txt = msg,fn = fn})
	end 
end 

return que