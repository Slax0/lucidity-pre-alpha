local radio = {} 
function radio:new(parent,x,y,w,h)
	self.image = love.graphics.newImage("playmap/radio.png")
	local availW = love.graphics.getWidth() -w - 20
	local ui = parent.ui
	local self = {} 
	self.ui = ui
	self.parent = parent
	setmetatable(self,{__index = radio})
	
	local b = ui:addButton(nil,x,y,w,h)
	self.button = b 
	b.OnDraw = function()
		if self.Active then 
			local x,y = self.button.colision:bbox()
			for i,v in ipairs(self.alertLines) do 
				setColor(colors.yellow)
				DrawRectangle(x + v.x,y + v.y,20,100,v.rot,10,50)
				setColor()
			end 
			local x,y = b.colision:bbox()
			love.graphics.draw(self.image,x,y)
			self.tooltip:draw()
		end 
	end 
	b.dontDraw = true
	b.OldX = b.x 
	b.OldY = b.y
	b.y = b.y +h
	
	local c = ui:addTooltip(b,"","left")
	self.tooltip = c
	
	c.manual = true
	c.appearDelay = 0.5 
	c.disappearDelay = 0.5
	
	self.On = false 
	self.Go = false 
	self.pos = 1
	self.que = {}
	
	local w,h = love.graphics.getDimensions()
	local alertLines = {
		{},{},{}
	} 
	for i,v in ipairs(alertLines) do 
		alertLines[i].x = self.button.width/2  
		alertLines[i].y = self.button.height
		v.rot = 0 
	end 
	self.alertLines = alertLines
	return self 
end 
function radio:cypher()
	local font = Glyhps
	self.tooltip.font = font 
end
function radio:decypher()
	self.tooltip.font = nil 
end 
function radio:clearQue()
	self.pos = 1
	self.OldPos = 0
	self.que = {}
	
	self.Go = true 
	self.queFunc = nil 
	self.Tweened = nil
	self.TweenedDone  = nil 
	self.TextDelay = nil 
	local active = self.Active
	if self.activeT then 
		Timer.cancel(self.activeT)
	end 
end 
function radio:Tween(on)
	local tween = self.parent:getTween()
	local alertLines = self.alertLines
	
	if not on and self.Tweened then 
		local no = 0 
		for i,v in ipairs(alertLines) do 
			local w,h = love.graphics.getDimensions()
				self.TweenedDone = false 
			tween:to(v,(0.2*i),{x =  self.button.width/2,y = self.button.height,rot = 0}):oncomplete(function() 
				no = no +1 
				if no == 3 then 
					self.Tweened = false
				end 
			end)
		end 
	elseif on and not self.Tweened then 
		local function pickColor(case)
			return colors.yellow
		end 
		for i,v in ipairs(alertLines) do 
			alertLines[i].color = pickColor(i)
		end
		local w,h = self.button.width,self.button.height
		local x,y = self.button.colision:bbox()
		local gap = 30
		for i,v in ipairs(alertLines) do 
			local rot = 0 
			local oy = 0 
			local ox = 0 
			if i == 1 then 
				rot = -60
				oy = -40 
				ox = -10 
			elseif i == 2 then 
				rot = -25
				oy = -10
			else 
				rot = 20
				oy = -20
				ox = 0 
			end 
			tween:to(v,(0.2*i),{x = -90 + ((20 +gap)*i) - ox,y = -h/2 + 90 -oy,rot = math.rad(rot)})
			:oncomplete(function() 
				self.TweenedDone = true
			end)
		end 
		self.Tweened = true
	end
end 
function radio:tweenUD()
	if self.TweenedDone then 
		for i,v in ipairs(self.alertLines) do 
			if i == 1 then 
				upDown(v,20,v.x,"x",1)
			elseif i == 2 then 
				upDown(v,20,v.y,"y",1)
			elseif i == 3 then 
				upDown(v,-20,v.y,"y",1)
			end 
		end 
	end 
end 
function radio:update(dt)
	local x,y = self.button.colision:center()
	if self.On ~= self.OldOn then 
		if self.On then  
			self.parent:getTween():to(self.button,0.5,{y = self.button.OldY}):oncomplete(function()
				self.Go = true 
				if self.queFunc then self.queFunc() end 
				self.queFunc = nil
			end)
		else
			self.parent:getTween():to(self.button,1.5,{y = love.graphics.getHeight()+128}):oncomplete(function()
				self.Go = false 
			end)
		end 
		self.OldOn = self.On
	end 
	if self.pos ~= self.OldPos then 

		if self.que[self.pos] then
			self.On = true
			self.queFunc = function() 
				self:setText(self.que[self.pos].txt)
				self.tooltip.TurnedOn = true
				self:Tween(true)
				print(self.TextDelay)
				self.activeT = Timer.add(self.TextDelay,function()
					self.tooltip.TurnedOn = false
					if self.que[self.pos].fn then self.que[self.pos].fn() end
					self:Tween()
					self.activeT = Timer.add(1,function() 
						self.pos = self.pos +1 
					end)
				end)
			end 
			if self.Go then 
				self.queFunc()
				self.queFunc = nil
			end 
			self.OldPos = self.pos
		else 
			self.On = false
		end 
	end 
	self:tweenUD()
	if self.OnUpdate then 
		self.OnUpdate(dt)
	end
end 
function radio:setText(t)
	local availW = love.graphics.getWidth()-200
	local font = self.ui:getFont()
	local w = font:getWidth(t)
	local no = availW/w 
	local actual_text = ""
	local text = t
		local tex = "" 
		text:gsub(".", function(c)
			tex = tex..c
			if font:getWidth(tex) > availW then
				actual_text = tex.."\n"
				tex = "" 
			end 
		end)
	self.tooltip:setText(actual_text..tex) 
	self.TextDelay = math.max((string.len(actual_text..tex)/10)*(misc.textSpeed or 1),5)
end 
function radio:add(msg,fn,t)
	if t then self.Active = t end 
	if self.Active or t then
		print("Adding to Radio: ",msg)
		if not self.que then self.que = {} end 
		table.insert(self.que,{txt = msg,fn = fn})
	end 
end 
function radio:push(msg)
	
end 
function radio:remove()
	self.tooltip:remove()
	self.button:remove()
end 

return radio