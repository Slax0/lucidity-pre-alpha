local GrayCard = love.graphics.newImage("Ghosts/cards/gray.png")
local function makePanel(ui)
	local w,h = love.graphics.getDimensions()
	local cw,ch = 180,267
	local linew = math.floor(w/(cw + 100))
	local lineh = math.floor(h/(ch+200))
	local neno = (linew+1)*(lineh+1)
	local frame = ui:addFrame("Cards",w/2,h/2,(cw + 10)*(linew+1),ch*(lineh+1) + 30)
	frame.dragEnabled = true 
	local grid = {} 
	frame.OnDrawTop = function()
		for i,v in ipairs(grid) do 
			if v.button.tooltip then
				v.button.tooltip:draw()
			end 
		end 
	end 
	
	local ox,oy = 5,25
	local page = 1 
	for x = 0,linew  do 
		for y=0,lineh do 
			local card = {x = ox + (cw + 5)*(x),y = oy + (ch + 5)*(y)}
			local b = ui:addButton(frame,card.x,card.y,cw,ch)
			card.button = b 
			b.dontDrawB = true 
			b.dontDrawC = true
			b.activeColoring = true 
			function b.OnUpdate()
				card.card:update()
			end
			b.OnDraw = function()
				local x,y = b.colision:bbox()
				if card.card then 
					b.inactive = false
					love.graphics.draw(card.card:retCanvas(),x,y)
				else 
					b.inactive = true 
					love.graphics.draw(GrayCard,x,y)
				end 
			end 
			card.b = b 
			table.insert(grid,card)
		end 
	end 
	frame:addCloseButton()
	local function swapPage(no)
		page = no 
		for i,v in ipairs(grid) do 
			grid[i].card = nil 
			if grid[i].tooltip then grid[i].tooltip:remove() end 
			grid[i].button.OnClick = false 
			grid[i].button.OnRightClick = false 
		end 
		local start = neno*(no-1)  
		local finish = start + neno
		local n = 0 
		for i,v in ipairs(playersCards) do 
			if i > start  and i <= finish then 
				n = n + 1
				grid[n].card = v 
				local b = grid[n].button
				local t = b:addTooltip("Cost: "..v.cost.." left click to use(as applicable),right click to discard")
				t.dontDrawB = true 
				b.OnClick = function()
					if v.externalUse then 
					warning:new(ui,"Do you wish to use the card:\n\n "..v.name,
						function() 
							if not v.Used then 
								v.externalUse(playersMobs)
								v.Used = true 
								removeFromTable(playersCards,v)
								swapPage(page)
							end 
						end)
					end 
				end
				b.OnRightClick = function()
					warning:new(ui,"This will discard the card:\n\n "..v.name.."\n\n Are you sure?",
					function() 
						removeFromTable(playersCards,v)
						swapPage(page)
					end)
				end 
			end 
		end 
	end 
	for i=1,math.ceil(#playersCards/neno) do
		local b = ui:addButton(frame,frame.width,(30 + 5)*(i),30,30)
		b:setText(i)
		b.color = basicFrameColor
		b.dontDrawB = true 
		b.OnClick = function()
			swapPage(i)
		end 
		b.OnUpdate = function()
			if b.text == page then 
				b.inactive = true 
			else 
				b.inactive = false 
			end 
		end 
	end 
	swapPage(page)
end
return makePanel