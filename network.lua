-- Used exceprts of Luasocket and added to it. 
-- LuaSocket 3.0 license
-- Copyright © 20015-2017 Diego Nehab and Janis Sabanskis 

-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.





local network = {}
function network:new()
	local s = {
		acc = acc, 
		pass = "startedmakingtroubleintheneighbourhood",
		net = net,
		channel = love.thread.getChannel("get_update"),
		out_ch = love.thread.getChannel("out_prc"),
		out = love.thread.getChannel("out_update"),
		status = "",
	}
	local function fn()
		require("love.filesystem")
		require("love.timer")
		require("love.image")
		local socket = require("socket")
		local ftp = require("socket.ftp")
		local ltn12 = require("ltn12")
		local url = require("socket.url")
		local ch = love.thread.getChannel("get_update")
		local out = love.thread.getChannel("out_update")
		local chOut = love.thread.getChannel("out_prc")
		local acc    = "a6366482"
		local pass   = "12adbaba"
		local net    = "templucidity.site90.com"
		local prefix = "public_html" 
		local dir 	 = ch:pop()
		-- formats a number of seconds into human readable form
		function nicetime(s)
			local l = "s"
			if s > 60 then
				s = s / 60
				l = "m"
				if s > 60 then
					s = s / 60
					l = "h"
					if s > 24 then
						s = s / 24
						l = "d" -- hmmm
					end
				end
			end
			if l == "s" then return string.format("%5.0f%s", s, l)
			else return string.format("%5.2f%s", s, l) end
		end

		-- formats a number of bytes into human readable form
		function nicesize(b)
			local l = "B"
			if b > 1024 then
				b = b / 1024
				l = "KB"
				if b > 1024 then
					b = b / 1024
					l = "MB"
					if b > 1024 then
						b = b / 1024
						l = "GB" -- hmmm
					end
				end
			end
			return string.format("%7.2f%2s", b, l)
		end
		
		local remaining_s = "%s received, %s/s throughput, %2.0f%% done, %s remaining"
		local elapsed_s =   "%s received, %s/s throughput, %s elapsed" 
		
		function gauge(got, delta, size)
			local rate = got / delta
			if size and size >= 1 then
				return string.format(remaining_s, nicesize(got),  nicesize(rate),
					100*got/size, nicetime((size-got)/rate))
			else
				return string.format(elapsed_s, nicesize(got),
					nicesize(rate), nicetime(delta))
			end
		end 
		function stats(size)
			local start = socket.gettime()
			local last = start
			local got = 0
			return function(chunk)
				-- elapsed time since start
				local current = socket.gettime()
					-- total bytes received
					got = got + string.len(chunk or "")   
					-- not enough time for estimate
					chOut:push(gauge(got, current - start, size))
					last = current
				return chunk
			end
		end
		local function getDirectoryItems(u,cmnd)
			local t = {}
			local p = url.parse(u)
			p.command = "nlst"
			p.sink = ltn12.sink.table(t)
			local r,e = ftp.get(p)
			return r and table.concat(t),e,u,p 
		end
			local function getItems(last,str,p,files,mode,fn)
				local no = 0 
				for line in str:gmatch("[^\r\n]+") do
					no = no +1 
					if no >= 3 then 
						local cont 
						if not mode or mode == "list" then 
							cont = true 
							if files then 
								table.insert(files,p.path.."/"..line)
							end 
						elseif mode == "get" then 
							local f,e = ftp.get(last.."/"..line..";type=i")
							if not e then 
								if files then 
									files[p.path.."/"..line] = f
								end 
								if fn then 
									fn(p.path.."/"..line,f,"file")
								end 
							else 
								cont = true 
							end 				 
						end 
						if cont then 
							if fn then fn(p.path.."/"..line,nil,"dir") end 
							local str,e,last,p = getDirectoryItems(last.."/"..line)
							if str then
								getItems(last,str,p,files,mode,fn)
							else 
								out:push("ERROR: "..e)
							end 
						else 
						end 
					end 
				end 
			end 
			local function downDirectoryItems(acc,pass,net,prefix,dir)
				local files = {}
				local str,e,last,p = getDirectoryItems("ftp://"..acc..":"..pass.."@"..net.."/"..prefix.."/"..dir)
				getItems(last,str,p,files,"get",function(path,data,mode)
					local path = string.gsub(path,prefix.."/","")
					path = string.gsub(path,"//","/")
					out:push(path)
					local fs = love.filesystem
					if mode == "dir" then
						fs.createDirectory(path)
					elseif mode == "file" then  
						fs.write(path,data)
					end
				end)
			end 
			downDirectoryItems(acc,pass,net,prefix,dir)
		end
	s.thread = love.thread.newThread(string.dump(fn))
	
	return setmetatable(s,{__index = network})
end 
function network:getCurrentPrecent()
end 
function network:getUpdate()
	self:copyDir("inbuilt_maps")
end 
function network:getProgress()
	local t = "DONE"
	if self.thread:isRunning() then 
		local ch = self.out_ch 
		local file = self.current_file or "none"
		local prc = ch:pop()
		t = file.." "..(self.status) 
		if not prc then
		elseif string.find(prc,"DONE") then 
			self.status = "Done"
		else 
			self.status = prc.." %"
		end 
	end 
	return t
end  
function network:setSourceDir(dir)
	self.prefix = dir 
end 
function network:update(dt)
	if self.thread:isRunning() then 
		local out = self.out 
		local count = out:getCount()
		if count > (self.out_count or 0) then
			self.current_file = tostring(out:pop())
			self.out_count = count
		end 
	end 
end 
function network:copyDir(dir)
	if not self.thread:isRunning() then 
	self.channel:push("inbuilt_maps")
	self.thread:start()
	end
end 
function network:getVersion()
end 
return network 