local options = {}
local Collider = Collider 
function options:enter(menu,_,_,Collider)
		local SaveName = "settings"
		local function setColorBool(bool,button)
			local color
			local text
			if bool then
				text = "ON"
				color = {0,255,0}
			else 
				text = "OFF"
				color = {255,0,0}
			end
			button:setText(text)
			button.color = color
		end
		local activeColor = {25,5,5,165}
		
		keys,alt_keys,misc = loadKeyOptions(love.filesystem.read(SaveName),false)
		local unibodyFont = love.graphics.newFont(uniBody,8*px)
		local unibodyFont_size2 = love.graphics.newFont(uniBody,12*px)
		local ui = menu.ui
		self.parent = menu
		local w,h = 700*px,500*py
		local fw,fh = w,h
		
		local frame = ui:addFrame(nil,menu.width/2,menu.height/2,w,h)
		
		self.centerFrame = frame
		frame.color = {255,255,255,55}
		local aw,ah = 70*px,20*py
		
		self.row = {}
		
		local close = ui:addButton(frame,w - (aw + 10) ,h - (ah + 10),aw,ah)
		close:setText("Done")
		close.textColor = {255,255,255}
		close.textFont = unibodyFont
		 
		local cancel = ui:addButton(frame,w - (aw + 10)*2 ,h - (ah + 10),aw,ah)
		cancel:setText("Cancel")
		cancel.OnClick = function()
			self:leave()
		end 
		cancel.textFont = unibodyFont
		self.removeTable = {}

		
		local function clear(item,active)
			if active then active.ActivatedColoring = nil end 
			for i,v in pairs(self.removeTable) do	
				v:remove()
			end	
			self.removeTable = {}
			if item then item.ActivatedColoring = activeColor end
	end
	
	local wah,hah = 83*px,20*py
	local ox = 10
	local title = ui:addButton(frame,(ox*px),ox*py,wah,hah) -- title is the Control thing
	local keys2 = ui:addButton(frame,(ox*px) + (wah + 15),ox*py,wah,hah)
	ui:addTooltip(keys2,"Set the alternate key map")
	local gameplay = ui:addButton(frame,(ox*px) + (wah + 15)*2,ox*py,wah,hah)
	local videos = ui:addButton(frame,(ox*px) + (wah + 15)*3,ox*py,wah,hah)
	local sound = ui:addButton(frame,(ox*px) + (wah + 15)*4,ox*py,wah,hah)
	sound.text = "Sound"
	
	sound.OnClick = function()
		if self.activeSelection ~= sound then
			clear(sound, self.activeSelection)
			
			local w,h = frame.width - 20*px,frame.height - 80*py
			local centerFrame = ui:addFrame(nil,10*px,40*py,w,h,frame)
			centerFrame.color = basicFrameColor
			local gap = 80 
			local oy = -40
			
			local master = ui:addSlider(centerFrame, frame.width/2,  oy + gap, 400, 5)
			master:setRange(0,100)
			master:setValue(misc.mastervol or 50)
			master.OnDraw = function()
				local w,h = master.width,master.height
				love.graphics.rectangle("line",master.x ,master.y,w,h)
				love.graphics.print("Master Volume",master.x,master.y)
			end 
			local sfxSlider = ui:addSlider(centerFrame, frame.width/2,  oy + gap*2, 400, 5)
			sfxSlider:setRange(0,100)
			sfxSlider.OnDraw = function()
				love.graphics.print("Sound Effects",sfxSlider.x,sfxSlider.y)
			end 
			sfxSlider:setValue(misc.sfvol or 50)
			
			local bgmSlider = ui:addSlider(centerFrame, frame.width/2, oy + gap*3, 400, 5)
			bgmSlider:setRange(0,100)
			bgmSlider.OnDraw = function()
				love.graphics.print("Music",bgmSlider.x,bgmSlider.y)
			end 
			bgmSlider:setValue(misc.bgmvol or 50 )
			
			local Voice = ui:addSlider(centerFrame, frame.width/2, oy + gap*4, 400, 5)
			Voice:setRange(0,100)
			Voice:setValue(misc.voicevol or 50)
			
			Voice.OnUpdate = function(dt)
				misc.sfvol    = sfxSlider:getValue()
				misc.bgmvol   = bgmSlider:getValue()
				misc.voicevol = Voice:getValue()
				misc.mastervol = master:getValue()
			end 
			Voice.OnDraw = function()
				love.graphics.print("Voice",Voice.x,Voice.y)
			end 
			table.insert(self.removeTable,centerFrame)
			self.activeSelection = sound
		end
	end
		self.clear = clear
		
		ui:addTooltip(keys2,"Set the alternate key map")
		table.insert(self.row,title)
		table.insert(self.row,keys2)
		table.insert(self.row,gameplay)
		table.insert(self.row,videos)
		table.insert(self.row,sound)
		videos:setText("Video")
		gameplay:setText("Gameplay")
		gameplay._OnClick = function() 
			if self.activeSelection ~= gameplay then  
				clear(gameplay,self.activeSelection)
				
				local w,h = frame.width - 20*px,frame.height - 80*py
				local centerFrame = ui:addFrame(nil,10*px,40*py,w,h,frame)
				centerFrame.color = {125,125,125,195}
				
				local txt = "Show FPS counter:"
				
				local oy = 5
				
				local a = self.ui:addText(centerFrame,5*px,oy*py,txt,unibodyFont_size2)
				local width2 = unibodyFont_size2:getWidth(txt)*2
				local width3 = unibodyFont_size2:getHeight(tostring(requiredMobs))
				local gap = 10*py
				local fontH = unibodyFont_size2:getHeight()
				local cw,ch = 20*px,20*py
				
				local no = 1 
				local function addButton(text,table,var,fn,ygap)
					local m2 = self.ui:addText(centerFrame,5*px,((fontH + gap)*(no)) + oy*py + (ygap or 0),text,unibodyFont_size2)
					local _ = self.ui:addButton(centerFrame,width2 + 25*px,(ygap or 0) + oy*py+((fontH + gap)*(no)),cw + 20*px,ch)
						if not fn then 
							setColorBool(table[var],_)
							_.OnClick = function()
								if table[var] then 
									table[var] = false
								else
									table[var] = true
								end
								setColorBool(table[var],_)
							end
						else 
							setColorBool(fn(),_)
							_.OnClick = function()
								if fn() then 
									fn(false)
								else
									fn(true)
								end
								setColorBool(fn(),_)
							end
						end 
					no = no +1
				end 
				local _ = self.ui:addButton(centerFrame,width2 + 25*px,oy*py,cw + 20*px,ch)
				local _oldVal = misc.showFPS
				setColorBool(misc.showFPS,_)
				_.OnUpdate = function()				
					if _oldVal ~= misc.showFPS then 
						setColorBool(misc.showFPS,_)
						_oldVal = misc.showFPS
					end 
				end
				_.OnClick = function()
					if misc.showFPS then 
						misc.showFPS = false
					else
						misc.showFPS = true
					end
				end  


				
				addButton("Camera slides in game: ",misc,"cameraSlide")
				addButton("Camera slides in battle: ",misc,"cameraSlideB")
				if current_gamestate == "menu" then
					addButton("Lock mouse to window: ",video,"trapMouse")
				end 
				addButton("Mouse is visible: ",video,"mouseVisible")
				addButton("Mouse is Locked: ",misc,"mouseLock")
				addButton("Console: ",misc,"console",nil,20)			
				addButton("Debug: ",nil,nil,function(var)
					if var ~= nil then 
						DEBUG = var
					end
					return DEBUG  					
				end,20)		
				addButton("Statistics: ",nil,nil,function(var)
					if var ~= nil then 
						STATS = var
					end
					return STATS  
				end,20)	
				addButton("Profiler: ",nil,nil,function(var)
					if var ~= nil then 
						PROFILER = var
					end
					return PROFILER  
				end,20)					
				
				table.insert(self.removeTable,centerFrame)
			self.activeSelection = gameplay
			end	
		end
		
		
		local oldVideos = deepCopy2(video)
		
		
		videos._OnClick = function()
			if self.activeSelection ~= videos then
				clear(videos,self.activeSelection)
				local w,h = frame.width - 20*px,frame.height - 80*py
				local centerFrame = ui:addFrame(nil,10*px,40*py,w,h,frame)
				centerFrame.color = {125,125,125,195}
				local ubf = unibodyFont
				local textH = ubf:getHeight()
				local gap = 5
				local text = tostring(video.width)..":"..tostring(video.height)
				local ow = video.width
				local oh = video.height
				local vars = ui:addText(centerFrame,w - 255*px,25*py,text,ubf)
				vars.OnUpdate = function() 
					if video.width ~= ow or video.height ~= oh then 
						ow = video.width
						oh = video.height
						vars:setText(tostring(video.width)..":"..tostring(video.height))
					end 
				end 
				local gapx = 5*px
				local gapy = 5*py
				local sw = (w - 255*px)
				local sh = 25*py
				local aw = (ubf:getWidth(text))*2
				local h1 = 15*px
				local up = ui:addButton(centerFrame,sw - aw/4,sh - h1*py,aw,h1*py)
				up:setText("UP")
				local down = ui:addButton(centerFrame,sw - aw/4, sh + h1,aw,h1*py)
				down:setText("DOWN")
				local sc_index = 1
				local presets = {
					{800,600},
					{1024,720},
					{1024,768},
					{1152,864},
					{1260,720},
					{1280,768},
					{1280,800},
					{1280,960},
					{1280,1024},
					{1366,768},
					{1440,900},
					{1600,900},
					{1680,1050},
					{1920,1080},
				}
				local mIndex = 0 
				for i,v in ipairs(presets) do 
					if v[1] == video.width and v[2] == video.height then 
						sc_index = i
					end
					if v[1] >= screen_w or v[2] >= screen_h  then 
						mIndex = i
					end 
				end 
				local function setPreset(i)
					print(i,mIndex)
					if i < 1 then
						i = 1
					end
					if i >= (mIndex) then 
						i = (mIndex -1)
					end
					sc_index = i				
					video.width = presets[i][1]
					video.height = presets[i][2]

					vars:setText(tostring(video.width)..":"..tostring(video.height))
				end 
				up.OnClick = function()
					sc_index = sc_index + 1
					setPreset(sc_index)
				end 
				
				down.OnClick = function() 
					sc_index = sc_index - 1
					setPreset(sc_index)
				end 
				
				local _ = ui:addText(centerFrame,5*px,25*py,"Screen Resolution:",ubf)
				
				local gapx = 15*py
				local _ = {
					"fullscreen",
					"fullscreentype",
					"display",
					"borderless",
					"vsync",
					"highdpi",
					"srgb",
				}
				for i,v in ipairs(_) do 
					local b = ui:addText(centerFrame,5*px,25*py + ((textH + gapx)*(i + 1)),v,ubf)
				end
				local no = 0
				local _2 = {}
				for i,v in pairs(video["flags"]) do 
					if no <= tablelenght(_) then 
						for b,n in ipairs(_) do 
							if n == i then 
								_2[b] = {i,v}
								no = no + 1
							end
						end 
					end
				end  
				for i,v in ipairs(_2) do 
					local a = v[2]
					local n = v[1]
					if type(a) == "boolean" then 
						local _ = self.ui:addButton(centerFrame,sw,25*py + ((textH + gapx)*(i + 1)),40*px,25*py)
						setColorBool(video["flags"][n],_)
						_.OnClick = function()
							if video["flags"][n] then 
								video["flags"][n] = false
							else
								video["flags"][n] = true
							end
							setColorBool(video["flags"][n],_)
						end  
					elseif type(a) == "string" then 
						local w = ubf:getWidth("desktop") + 10*px
						local h = ubf:getHeight() + 5*py
						local _ = self.ui:addButton(centerFrame,sw,25*py + ((textH + gapx)*(i + 1)),w,h)
						_:setText(a)
						_.textColor = {255,255,255}
						_.textFont = unibodyFont
						_.OnClick = function()
							if video["flags"][n] == "normal" then 
								video["flags"][n]  = "desktop"
							elseif video["flags"][n] == "desktop" then 
								video["flags"][n] = "normal"
							end 
							_:setText(video["flags"][n])
						end 
					elseif type(a) == "number" then 
						local w = ubf:getWidth(20) + 10*px
						local h = ubf:getHeight() + 5*py
						local _ = self.ui:addButton(centerFrame,sw,25*py + ((textH + gapx)*(i + 1)),w,h)
						_:setText(a)
						_.textColor = {255,255,255}
						_.textFont = unibodyFont
						local no = a
						_.OnClick = function()
							no = no + 1
							video["flags"][n] = no
							_:setText(video["flags"][n])
						end
						_.OnRightClick = function()
							no = no - 1
							video["flags"][n] = no
							_:setText(video["flags"][n])
						end 
					end
				end
		
				table.insert(self.removeTable,centerFrame)
			
			
			self.activeSelection = videos
			end
		end
		title:setText("Controls")
		keys2:setText("Alt Controls")
		
		title.textFont = unibodyFont
		keys2.textFont = unibodyFont
		
		local lenght = tablelenght(keys)
		local w,h = 200*px,400*py
		local BoxW = ((w + 10)*lenght)
		local intX = (fw - BoxW)/2
		title.textColor = {255,255,255}
		keys2.textColor = {255,255,255}
		
		
		local function makeKeys(keys)
			local i = 0
			for n,v in pairs(keys) do
				local frame = ui:addFrame(nil,intX + ((w + 10)*i),40*py,w,h,frame)
				table.insert(self.removeTable,frame)
				frame.header = {
					w = 200*px,
					h = 5*py,
				}
				frame.color = {125,125,125,195}
				frame.textFont = unibodyFont_size2
				frame.textColor = {255,255,255}
				frame:setText(n)
				local lenght = tablelenght(v)
				for c,b in ipairs(v) do
					local ow = w
					local w,h = (w/2),(20)*py
					local x,y = ow/2  - w/2 + 35*px,20 + ((h + 5)*c)
					local button = ui:addButton(frame,x,y,w,h)
					table.insert(self.removeTable,button)
					button.OnClick = function() 

						button:setText("Please Input a key")
						local nw 
						button.OnUpdate = function()				
								if not nw then 
									self.activeKey = nil
									self.source = nil
									nw = true 
								end
							if self.activeKey and self.source then
								button:setText(self.source.." "..self.activeKey)
								if self.activeKey == " " then
									button:setText(self.source.." space")
								end
								local text = tostring(self.activeKey)
								if self.source == "mouse" then
									v[c].f = function() return mousepress(text) end
									button.OnUpdate = nil
									self.activeKey = nil
									self.source = nil
								end
								if self.source == "keyboard" then
									v[c].f = function() return keyboardpress(text) end
									button.OnUpdate = nil
									self.activeKey = nil
									self.source = nil
								end
								if self.source == "gamepad" then
									v[c].f = function() return gamepadpress(self.gamepad,text) end
									button.OnUpdate = nil
									self.activeKey = nil
									self.source = nil
								end
							end
						end
					end
					button.textFont = unibodyFont
					button.OnRightClick = function()
						button:setText("None")
						v[c].f = function() return nokey()  end 
						print(c,v[c].f)
					end
					local _,a = b.f()
					button.textColor = {255,255,255}
					if a == "keyboard  " or a == "keyboard " then 
						a = "keyboard space"
					end
					button:setText(a)
					local texN = string.gsub(b.name,"_"," ")
					local text = ui:addText(frame,5*px,y + 3,texN,unibodyFont)
					text.color = {255,255,255}
				end
				i = i + 1
			end
		end 
		
		
		
		self.onRow = 1
		self.onColumn = 1
		--self.row[column][row]
		
		for i,v in ipairs(self.row) do 
			v.textColor = {255,255,255}
			v.textFont = unibodyFont
		end
		
		self.activeSelection = {}
		title._OnClick = function()
			if self.activeSelection ~= title then 
			clear(title,self.activeSelection)
			self.removeTable = {}
				makeKeys(keys)
				self.activeSelection = title
			end
		end
		title._OnClick()
		
		keys2._OnClick = function()
		if self.activeSelection ~= keys2 then 
				clear(keys2,self.activeSelection)
				makeKeys(alt_keys)
				self.activeSelection = keys2
				keys2.ActivatedColoring = activeColor
			end
		end
		
		self.ui =  ui
		close.OnClick = function()
			local restart
			local c = false 
			if not compareTable(video,oldVideos,true) then
				restart = true 
				loadVideoOptions(video)
			end
			saveKeyOptions(SaveName,keys,alt_keys,misc,video) 
			keys,alt_keys,misc = loadKeyOptions(love.filesystem.read(SaveName),false)
			if restart then
				self.OnRemove = nil
				Gamestate.switch(mainMenu)
			end 
			love.mouse.setGrabbed(misc.mouseLock)
			self:leave()
		end
		if Gamestate.current() ~= mainMenu then 
			videos.OnClick = nil
			videos:setInactive(true)
			videos.textColor = {255,255,255,155}
		end 
		return self
end
function options:leave()
	self.clear()
	self.centerFrame:remove()
	self.centerFrame = nil 
	if self.OnRemove then self.OnRemove() end
	for i,v in ipairs(self) do 
		self[i] = nil
	end 
end
function options:mousepressed(x,y,button)
	self.activeKey = button
	self.source = "mouse"
end
function options:mousereleased(x,y,button)
end
function options:keypressed(key,unicode)
	self.activeKey = key
	self.source = "keyboard"
end
function options:gamepadpressed(joy,key)
	self.gamepad = joy
	self.activeKey = key
	self.source = "gamepad" 
end
function options:joystickpressed(joy,key)
	self.gamepad = joy
	self.activeKey = key
	self.source = "gamepad" 
end
function options:draw(menu)
	
end


function options:OnResize(w,h)
end

return options