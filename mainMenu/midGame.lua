local sa = {}
local cardShop = require("Ghosts/cards/cardshop")
function sa:enter(_,uic,nvl)
	_currentGamestate = sa
	local sw,sh = love.graphics.getDimensions()
	local px,py = sw/800,sh/600
	local width,height = love.graphics.getDimensions()
	local w,h = sw - 20,90*py
	local ui = uic:init()
	self.ui = ui
	cardShop:new(ui,5,5,600*px,500*py,playersCards)
	
	local redPurge = loadCards:new("redPurge","set1")
	
	self.nvl = nvl:new(sw/2,sh - (h/2),w,h,ui,px,py)
	local nvl = self.nvl
	local sylvie_normal    = love.graphics.newImage("temporaryAssets/sylvie_normal.png")
	local sylvie_surprised = love.graphics.newImage("temporaryAssets/sylvie_surprised.png")
	local sylvie = nvl:addCharacter("Sylvie",{104,37,15},sylvie,-100,-50)
	sylvie:addMood("default",sylvie_normal,{104,37,15},nvl.default)
	sylvie:addMood("surprised",sylvie_surprised,{104,37,15},nvl.default)
	sylvie:setMood("default")
	sylvie.dontDraw = true 
	local a = loadMob:new("slime",{rarity = 10})
	a:setParams({weak = {"Hentai"}},"add")
	

	sylvie:say("Hello")
	sylvie:say("next que item")
	sylvie:say("Please pick one!")
	local w,h = 150,40
	local mf = nvl.mainFrame
	local topx,topy = mf.x + mf.width/2,mf.y - mf.height
	print(topx,topy)
	nvl:setButtonSize(w,h)
	-- nvl:addChoice(choice,onClick,onEnter,onLeave,x2,y2,w2,h2)
	nvl:addChoice("Card shop",function()
		
	end,
	
	nil,nil,topx - w/2,topy - (h/2*2))
	nvl:addChoice("Talk")
	nvl:addChoice("Challenge")
	nvl:addChoice("Leave")
	
end 

function sa:update(dt)
	self.nvl:update(dt)
	self.ui:update(dt)
	Collider:update(dt)
end

function sa:draw()
	love.graphics.setColor(120,167,147)
	love.graphics.rectangle("fill",0,0,800,600)
	love.graphics.setColor(255,255,255)
	self.nvl:draw()
	self.ui:draw()
end
function sa:Hcol(...)
	self.ui:Hcol(...)
end 
function sa:Hstop(...)
	self.ui:Hstop(...)
end 
function sa:keypressed(...)
	self.ui:keypressed(...)
end
function sa:mousepressed(...)
	self.ui:mousepressed(...)
end 
function sa:mousereleased(...)
	self.ui:mousereleased(...)
end 
return sa