local mainMenu = {} 
local ui = require "menu.ui"
local cam = require "hump.camera"
local Timer =  require "hump.timer"

local _PACKAGE = getPackage(...)
local nvl = require(_PACKAGE.."/nvl")
local midGame = require(_PACKAGE.."/midGame")
local uniBody = _PACKAGE.."/fonts/Unibody8Pro-Regular.otf"
local uniBody_italic = _PACKAGE.."/fonts/Unibody8Pro-RegularItalic.otf"
local uniBody_caps = _PACKAGE.."/fonts/Unibody8Pro-RegularSmallCaps.otf"
local uniBody_bold = _PACKAGE.."/fonts/Unibody8Pro-Bold.otf"
local uniBody_black = _PACKAGE.."/fonts/Unibody8Pro-Black.otf"
local text = "Lucidity"
local pFont = uniBody
local fontsmall = love.graphics.newFont(pFont,5)
local fontS = love.graphics.newFont(pFont,120)
local function randomWithinRange(upb,lwb,size)
	local var = love.math.random(upb,lwb)
	if math.abs(var) < size then 
		randomWithinRange(upb,lwb,size)
	end
	return var	
end
local function draw(...)
	love.graphics.draw(...)
end
local function charCount(str)
	local i = 0
	for c in str:gmatch"." do
		i = i + 1
	end
	return i
end
function mainMenu:enter()
love.graphics.setDefaultFilter("nearest","nearest")
	self.ui = ui:init()
	self.time = 0
	self.width,self.height = love.graphics.getDimensions()
	local propx = self.width/800 
	local propy = self.height/600
	local prop = (propx + propy)/2 
	fontS = love.graphics.newFont(pFont,60*prop)
	self.cam = cam(self.width/2,self.height/2)
	self.title = {}
	local gap = 15*propx
	
	centerPosx,centerPosy = self.width/2,self.height/4
	
	titleW = fontS:getWidth(text) + (gap * (charCount(text) - 1)) 
	titleH = fontS:getHeight(text)
	
	startX = centerPosx - titleW/2
	startY = centerPosy - titleH/2
	
	
	self.sortText = function(tables,gap,startX,startY)
		local i = 0
		local currentSize = startX
		for c in text:gmatch"." do
			local w = fontS:getWidth(c)
			local h = fontS:getHeight(c)
			local a = {char = c,ix = currentSize,iy = startY}
			currentSize = currentSize + w + gap
			table.insert(tables,a)
			i = i + 1
		end
		return tables
	end
	self.sortText(self.title,gap,startX,startY)
	
	local no = #self.title
	for i,v in ipairs(self.title) do 
		v.x = v.ix
		v.y = v.iy
		v.finished = 0
		if i % 2 == 0 then 
			v.y = v.iy + self.width
			v.tween = Timer.tween(3,v,{ y = v.iy - titleH/15 },"out-sine",function() v.finished = 1 end)
		else
			v.y = v.iy - self.height
			v.tween = Timer.tween(3,v,{ y = v.iy + titleH/15},"out-sine",function() v.finished = 1 end)
		end
	end
	self.propx = propx
	self.propy = propy
	
	self.animations = {}
	local playerX,playerY
	local a = {}
	local img = love.graphics.newImage("assets/float.png")
	local row = 2
	local column = 8 
	local w = img:getWidth()/column
	local h = img:getHeight()/row
	local s = 1
	local speed = 0.20
	a.paralaxFactor = 0.3
	a.anim = newAnimation(img,w,h,speed,1,11)
	a.x = self.width/2 - (w/2*s)
	a.y = startY + titleH + self.height/17
	a.ix = a.x
	a.iy = a.y
	playerX = a.ix + w/2
	playerY = a.iy + h*s
	a.scale = s
	if love.filesystem.exists("save_data/Autosave") then 
		local contB = self.ui:addButton(nil,playerX,playerY-10,70,90)
		contB:addTooltip("Continue game")
		contB.dontDrawB = true 
		contB.dontDrawC = true 
		contB.OnClick = function()
			loadGame("Autosave")
		end 
	end 
	table.insert(self.animations,a)
	
	
	local w = crystals["basic"]:getWidth()/15
	local i = 0 
	local gap = 25
	local leng = ((w/2 + gap)*7)
	local tabl = {}
	tabl[1] = crystals["basic"]
	tabl[2] = crystals["pink"]
	tabl[3] = crystals["blue"]
	tabl[4] = crystals["Inverted"]
	tabl[5] = crystals["cyan"]
	tabl[6] = crystals["silver"]
	tabl[7] = crystals["green"]
	for c,v in ipairs(tabl) do 
		local img = v
		local row = 1
		local column = 15 
		local w = img:getWidth()/column
		local h = img:getHeight()/row
		local a = {}
		a.scale = 1
		a.paralaxFactor = 0.3 + (0.1 - (1*(i - 3)^2)/100)
		a.x = (playerX - leng/2 - 20) + (((w*a.scale)+ gap)*i) 
		a.y = (playerY + 30) + -6*(i - 3)^2
		a.ix = a.x 
		a.iy = a.y 
		a.anim = newAnimation(img,w,h,0.07)
		table.insert(self.animations,a)
		i = i + 1
	end
	

	self.buttons = {}
	local w = 87*propx
	local h = 35*propy
	local gap = 10
	local startx = self.width/2 
	local starty = self.height/2 + startY + gap*4
	local text = {
		{name = "Credits", func = function() 
			Gamestate.switch(Credits)
		end},
		{name = "Editor", func = function() 
			current_gamestate = "editor"
			love.focus()
			Gamestate.switch(Editor)
		end}, 
		{name = "Options",func = function()
			local options = require(_PACKAGE.."/options")
			self.options = options:enter(self,propx,propy,Collider)
			self.options.OnRemove = function() 
				for i,v in ipairs(self.buttons) do 
					v:setInactive(false)
				end
				self.options = nil
			end
			for i,v in ipairs(self.buttons) do 
				v:setInactive(true)
				v.OnFocusLost()
			end
		end},
		{name = "Quit",func = function()
			love.event.push("quit")
		end},
		{name = "Load",func = function()
			local w,h = love.graphics.getDimensions()
			local activeSelection 
			local function done()
				for i,v in ipairs(self.buttons) do 
					v:setInactive(false)
				end 
			end 
			for i,v in ipairs(self.buttons) do 
				v:setInactive(true)
				v.OnFocusLost()
			end
			local ui = self.ui
			local frame = ui:addFrame("Load",w/2,h/2,w-200,h-200) 
			frame:setHeader()
			frame.color = basicFrameColor
			local w,h = w-200,h-200
			local list = ui:addList(frame,2,22,w-4,h-50)
			list.color = basicFrameColor
			
			local function refreshSaves()
				local saves = getSaves()
				list:clear()
				local SaveButtons = {} 
				local offset = 0 
				for i,v in ipairs(saves) do 
					if string.find(v.name,".troll") then 
						offset = offset + 35 
					end 
				end 
				for i,v in ipairs(saves) do 
					local b 
					if string.find(v.name,".troll") then 
						b = ui:addButton(nil,0,0,w-4,30)
						list:addItem(b,1)
						b.color = colors.red
						b:setText("One Use")
					else 
						b = ui:addFrame(nil,0,0,w-4,155)
						local frame = b 
						local t = (v.name or "CORUPTED SAVE")
						b:setHeader(t)
						b.headerColor = colors.paleBlue 
						list:addItem(b)

						local color = colors.white
						if v.name == "Autosave" then 
							list:removeItem(b,true)
							list:addItem(b,1)
							color = colors.green 
						end
						local lg = love.graphics
						local c = lg.newCanvas(b.width,b.height)
						local mobs 	= {}
						local cards = {} 
						local Cards = v.cards 
						local Mobs 	= v.mobs
						for i,v in ipairs(Cards) do 
							local a = loadCards:new(v)
							table.insert(cards,a)
						end 
						for i,v in ipairs(Mobs) do
							local a = loadGhost:new(v.name)
							a.image:setFilter("nearest","nearest")
							table.insert(mobs,a)
						end 
						local bw,bh = 60,60
						local ox,oy = b.width/2 - (2 + bw*#mobs)/2,b.height - (bw + 2)
						local time = v.playTime 
						local font = love.graphics.newFont(uniBody,14)
						
						local date = "XX/XX/XX"
						local time = "XX:XX:XX"
						if v.date then 
							date = v.date[1] 
							time = v.date[2]
						end 
						local h = font:getHeight()
						local t = time.." "..date
						local w = font:getWidth(t)
						local t2 = "Map: "..v.map
						local w2 = font:getWidth(t2)
						lg.setCanvas(c)
							setColor(colors.lightGray)
							lg.rectangle("fill",ox,oy,((2 + bw)*(5)),bh+4)
							setColor()
							for i,v in ipairs(mobs) do 
								local sx,sy = bw/v.width,bh/v.height
								v:draw(ox + ((2 + bw)*(i-1)),oy,0,sx,sy)
							end 

							
							local f = love.graphics.getFont()
							lg.setFont(font)
							setColor(colors.green)
							lg.print(t,b.width/2 -(w/2),h*2 + 5)
							setColor()
							lg.print(t2,b.width/2 -(w2/2),25)
							love.graphics.setFont(f)
						lg.setCanvas()
						b.dontDrawB = true
						
						local bw,bh = 60,90
						local cardList = ui:addList(frame,2,20,bw + 10,bh+25)
						cardList:setHeader("Cards")
						cardList.headerColor = colors.blue
						cardList.color = colors.paleBlue
						for i,v in ipairs(cards) do 
							local b = ui:addButton(nil,0,0,bw,bh)
							local ox,oy = b.x,b.y						
							local sx,sy = bw/v.width,bh/v.height 
							b.OnDraw = function()
								v:draw(b.x,b.y,0,sx,sy)
							end 
							b.dontDrawB = true 
							b.dontDrawC = true 
							cardList:addItem(b)
						end 
						b.OnDrawTop = function()
							setColor()
							lg.draw(c,b.x - b.width/2,b.y - b.height/2)
							if b.Selected then 
								setColor({22,22,22,50})
								b.colision:draw("fill")
								setColor()
							end 
						end 
					end 
					table.insert(SaveButtons,b)
					b.OnClick = function()
						activeSelection = v
						for i,v in ipairs(SaveButtons) do 
							v:setInactive(false)
							v.Selected = false 
						end 
						b:setInactive(true)
						b.Selected = true 
					end 
				end 
			end 
			refreshSaves()
			local x = ui:addButton(frame,frame.width - 20,0,20,20)
			x:setText("X")
			x.OnClick = function() done() frame:remove() end
			local bw,bh = 200,20
			local Load = ui:addButton(frame,2,frame.height - (bh + 2),bw,bh)
			Load:setText("Load")
			Load.OnClick = function()
				local name = activeSelection.name 
				loadGame(name)
			end 
			Load.OnUpdate = function()
				Load:setInactive(not activeSelection)
			end 
			local Delete = ui:addButton(frame,frame.width - (bw + 2),frame.height - (bh + 2),bw,bh)
			Delete:setText("Delete")
			Delete.OnClick = function()
				warning:new(ui,"This will permanently remove the save!",function() 
					local name = activeSelection.name 
					love.filesystem.remove("save_data/"..name) 
					refreshSaves()
				end)
			end 
			Delete.OnUpdate = function()
				Delete:setInactive(not activeSelection)
			end 
		end},
		{name = "Start",func = function()
			current_gamestate = "midGame"
			Gamestate.switch(PlayMap,{map = "inbuilt_maps/default"})
		end},
	}
	local yDif = 200
	for i = 1,3 do
		local x =  self.width + yDif
		local y = starty - (h + gap)*i
		local a = self.ui:addButton(nil,x,y,w,h)
		a.ix = startx +(w)*i
		a.iy = starty - (h + gap)*i
		table.insert(self.buttons,a)
	end
	for i = 1,3 do
		local x = -yDif
		local y = starty - (h + gap)*i
		local a = self.ui:addButton(nil,x ,y,w,h)
		a.ix = startx  - (w)*i
		a.iy =  starty - (h + gap)*i
		table.insert(self.buttons,a)
	end
	local function getFontSize(sizes)
		local size = math.floor(sizes)
		if size %2 == 0 then
		else
			size = size + 1
			size = getFontSize(size)
		end
		return size
	end
	for i,v in ipairs(self.buttons) do 
		v:setText(text[i].name)
		v.OnClick = text[i].func
		local a = v
		v:setInactive(true)
		v.tween2 = Timer.tween(3,v,{x = v.ix},"out-sine",function() 
			v:setInactive(false)
		end)
		a.dontDrawB = true
		a.dontDrawC = true 
		a.dontDrawActive = true  
		a.textColor = {255,255,255}
		local fontSize = getFontSize(14*propx)
		a.textFont = love.graphics.newFont(uniBody,fontSize)
		a.OnFocus = function()
			a.posInd = {
				x = a.x + a.width/2 + 10,
				y = a.y,
				ix = a.x + a.width/2 + 10,
				finished = 1,
				transp = 0,
			}
			a.posInd2 = {
				x = a.x - a.width/2 - 10,
				y = a.y,
				ix = a.x - a.width/2 - 10,
				finished = 1,
			} 
			a.OnDraw = function()
				local w,h = icons.indicator_red:getDimensions()
				love.graphics.setColor(255,255,255,a.posInd.transp)
				love.graphics.draw(icons.indicator_red,a.posInd.x,a.posInd.y,0,-1,1,w/2,h/2)
				love.graphics.draw(icons.indicator_red,a.posInd2.x,a.posInd2.y,0,1,1,w/2,h/2)
				love.graphics.setColor(255,255,255)
			end
			a.OnUpdate = function()
				local v = a.posInd
				local variation = 10
				local mode = nil
				if not v.visible then 
					Timer.tween(0.6,v,{transp = 255},"in-quad")
					v.visible = true
				end
				if v.finished == 1 then
					v.tween = Timer.tween(0.5,v,{x = v.x + variation},mode,function() v.finished = 2 end)
					v.finished = 0
				elseif v.finished == 2 then 
					v.tween = Timer.tween(0.5,v,{x = v.ix},mode,function() v.finished = 1 end)
					v.finished = 0
				end
				local v = a.posInd2
				if v.finished == 1 then
					v.tween = Timer.tween(0.5,v,{x = v.x - variation},mode,function() v.finished = 2 end)
					v.finished = 0
				elseif v.finished == 2 then 
					v.tween = Timer.tween(0.5,v,{x = v.ix},mode,function() v.finished = 1 end)
					v.finished = 0
				end
			end
		end
		v.OnFocusLost = function()
			v.OnDraw = nil
			v.OnUpdate = nil
			v.posInd = nil
			v.posInd2 = nil
			collectgarbage()
		end
	end
	
	self.ui_keyControler = keyControl:new("ui") 
	
end
function mainMenu:update(dt)
	globalKeystrokes(dt)
	self.ui_keyControler:attach()
	self.ui:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
	self.ui:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
	
	limitLag(dt,function()
		local mx,my = love.mouse.getPosition()
		local rangex = 10.003*self.propx
		local rangey = 10.003*self.propy
		local x,y = self.cam.x,self.cam.y
		local speed = 3
		if x < mx then 
			x = x + ((mx - x)/speed)*(dt)
		elseif x > mx then 
			x =  x - ((x - mx)/speed)*(dt)
		end
		
		if y < my then 
			y = y + ((my - y)/speed)*(dt)
		elseif y > my then 
			y = y - ((y - my)/speed)*(dt)
		end
		
		if x < self.width/2 - rangex then 
			x = self.width/2 - rangex
		elseif x > self.width/2 + rangex then 
			x = self.width/2 + rangex
		end
		if y > self.height/2 + rangey then 
			y = self.height/2 + rangey
		elseif y < self.height/2 - rangey  then
			y = self.height/2 - rangey
		end
		self.cam:lookAt(x,y)
	end)
	

	for i,v in ipairs(self.title) do 
		local mode = "out-sine"
		if v.finished == 1 then
			local variation = randomWithinRange(-titleH/7,titleH/7,(titleH/7)/2)
			v.tween = Timer.tween(2,v,{y = v.y + variation},mode,function() v.finished = 2 end)
			v.finished = 0
		elseif v.finished == 2 then 
			v.tween = Timer.tween(2,v,{y = v.iy},mode,function() v.finished = 1 end)
			v.finished = 0
		end
	end
	self.width,self.height = love.graphics.getDimensions()
	self.dt = dt
	self.time = (self.time + dt)
	Timer.update(dt)
	self.ui:update(dt)
	for i,v in ipairs(self.animations) do 
		v.anim:update(dt)
		local fin = v.finished
		local variation = 5
		if i == 1 then 
			variation = 10
		end
		if not fin then
			v.finished = 1
		elseif fin == 1 then 
			v.tween = Timer.tween(2,v,{y = v.y + variation},mode,function() v.finished  = 2 end)
			v.finished  = 0
		elseif fin == 2 then 
			v.tween = Timer.tween(2,v,{y = v.y - variation},mode,function() v.finished  = 1 end)
			v.finished  = 0
		end
	end
	self.ui.mxb:moveTo(self.cam:worldCoords(self.ui.mxb:center()))
	Collider:update(dt)
	if self.options then 
		if self.options._rem then 
			self.options = nil
		end 
	end
	globalKeystrokes()
	self.ui_keyControler:detach()
end
function mainMenu:pralaxPos(factor)
	local x = ((self.cam.x) - love.graphics.getWidth()/2)*factor
	local y = ((self.cam.y) - love.graphics.getHeight()/2)*factor
	return x,y
end
function mainMenu:draw()
	self.cam:attach()
	local oldFont = love.graphics.getFont()
	setColor(55,55,55)
	love.graphics.rectangle("fill",-10*self.propx,-10*self.propy,self.width+ (20*self.propx),self.height + (20*self.propy))
	setColor(255,255,255)
	local factor = 0.2
	local modx,mody = self:pralaxPos(factor)
	love.graphics.setFont(fontS)
	for i,v in ipairs(self.title) do
		love.graphics.print(v.char,v.x + modx,v.y + mody)
	end
	for i,v in ipairs(self.animations) do 
		local modx,mody = 0,0
		if v.paralaxFactor then
			modx,mody = self:pralaxPos(v.paralaxFactor)
		end
		v.anim:draw(v.x + modx,v.y + mody,0,v.scale or 1,v.scale or 1)
	end
	love.graphics.setFont(oldFont)
	self.ui:draw()
	if self.options then 
		self.options:draw()
	end
	self.cam:detach()
	if misc.showFPS then 
		showFPS()
	end 
	drawRelease()
end
function mainMenu:keypressed(key,unicode)
	if self.options then 
		self.options:keypressed(key,unicode)
	end
	self.ui:keypressed(key,unicode)
	-- DEBUG
	if key == "escape" then 
		for i,v in ipairs(self.buttons) do 
			v.x = v.ix
			v:setInactive(false)
			Timer.cancel(v.tween2)
		end 
	end 
end
function mainMenu:keyreleased(key,unicode)
	self.ui:keyreleased(key,unicode)
end
function mainMenu:mousepressed(x,y,button)
		self.ui:mousepressed(x,y,button)
		self.ui:mousepressedList(x,y,button)
	if self.options then 
		self.options:mousepressed(x,y,button)
	end
end
function mainMenu:mousereleased(x,y,button)
	self.ui:mousereleased(x,y,button)
	if self.options then 
		self.options:mousereleased(x,y,button)
	end
end
function mainMenu:gamepadpressed(joy,key)
	if self.options then
		self.options:gamepadpressed(joy,key)
	end
end
function mainMenu:joystickpressed(joy,key)
	if self.options then 
		self.options:joystickpressed(joy,key)
	end
end
function mainMenu:textinput(t)
end
function mainMenu:leave()
	self.ui:clear()
	Collider:clear()
end 
function mainMenu:Hcol(...)
	self.ui:Hcol(...)
end
function mainMenu:Hstop(...)
	self.ui:Hstop(...)
end
return mainMenu























