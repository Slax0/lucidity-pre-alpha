local nvl = {} 
local Timer =  require "hump.timer"
local _PACKAGE = getPackage(...)

function nvl:new(x,y,w,h,ui,px,py)
	self = {}
	self.w = w
	self.h = h 
	self.ui = ui
	self.queIndex = 0
	self.que = {}
	self.keyClamp = {}
	self.pressedKeys = {}
	local fSize = 16*(px/py)
	self.mainFrame = ui:addFrame(nil,x,y,w,h)
	local mainFrame = self.mainFrame
	mainFrame.color = {100,100,100,100}
	local wz,hz = 40*px,20*py 
	local gap = 5
	local h = bold:getHeight("A")
	self.default = default
	local starty = h + 5
	local startx = -wz - 10
	self.openQue = ui:addButton(mainFrame,startx + w,starty,wz,hz)
	self.openQue:setText("Queue")
	self.skip = ui:addButton(mainFrame,startx + w - (wz + gap),starty,wz,hz)
	self.skip:setText("Skip")
	self.last = ui:addButton(mainFrame,startx  + w - (wz + gap)*2,starty,wz,hz)
	self.last:setText("Last")
	
	self.openQue.OnClick = function()
	end 
	self.last.OnClick = function()
		self.queIndex = self.queIndex - 1
		local leng = tablelenght(self.que)
		if self.queIndex < 1 then 
			self.queIndex = 1
		elseif self.queIndex > leng then 
			self.queIndex = leng
		end 
	end 
	
	self.activeButtons = {
		self.openQue,
		self.skip,
		self.last,
	}
	
	
	local frame = ui:addFrame(nil,3,3,w - 6,h -3,mainFrame)
	frame.color = {55,55,55,155}
	
	self.charName = ui:addText(mainFrame,5*px,0," ",bold)
	self.charName:setZmap(10)
	
	
	self.speech = ui:addText(mainFrame,7*px,h + 2," ",default)
	self.speech:setZmap(10)
	self.characters = {}
	self.flags = {
		typeit = true,
	}
	self.choices = {}
	self.temporaryText = {}
	return setmetatable(self, { __index = nvl })
end
function nvl:setFlags(flags)
	self.flags = flags
end 
function nvl:setFonts(default,bold,italic)
	
end
function nvl:update(dt)
	for i,v in ipairs(keys.ui) do 
		local a = v.f()
		if not self.keyClamp[i] and a then
			self.pressedKeys[i] = true
			self.keyClamp[i] = true
		end
		if not a then 
			self.keyClamp[i] = false
		end
	end
	
	self:typeL(dt,100)
	
	
	
	if not self.choice then 
		if self.pressedKeys[4] then
			self.queIndex = self.queIndex + 1
		elseif self.pressedKeys[5] then
			self.queIndex = self.queIndex - 1
		end
	end
	local leng = tablelenght(self.que)
	if self.queIndex < 1 then 
		self.queIndex = 1
	elseif self.queIndex > leng then 
		self.queIndex = leng
	end 
	for i,v in pairs(self.pressedKeys) do 
		self.pressedKeys[i] = false
	end
end
function nvl:typeOut(text)
	self.typing = true
	self.accum = 0
	self.activeChar = 1
	self.activeText = text
	
end 
function nvl:typeL(dt,speed)
	if self.typing then 
		self.accum = self.accum + (dt*speed)
		if self.accum > 5 then
			local text = self.activeText
			local len = #text
			local ntext = ""
			if len >= self.activeChar then 
				for i = 1, self.activeChar do
					local c = text:sub(i,i)
					ntext = ntext..c
				end
				self.activeChar = self.activeChar + 1
				self.speech:setText(ntext)
			else 
				if self.OnTypeEnd then self.OnTypeEnd() end 
				self.typing = false
				self.activeText = nil
			end 
			self.accum = 0
		end
	end 
end 
function nvl:draw(mode) 
	if self.queIndex ~= self.oldQueIndex then 
		local font
		local v = self.que[self.queIndex]
		if v then 
			v.charac.active = true 
			local char = v.charac
			self.charName:setText(char.name)
			
			local image,color,font,preffixImage,suffixImage = char:_getMood()
			local text = v.text or ""
			
			if v.font then 
				font = v.font
			end 
			if v.mood then 
				char:setMood(v.mood)
			else
				char:setMood("default")
			end 
			if v.color then 
				color = v.color
			end
			if not color then 
				color = {255,255,255}
			end 
			if not font then 
				font = self.default
			end
			if self.flags.typeit then 
				self:typeOut(text)
			else 
				self.speech:setText(text)
			end 
			if v.prefixImage then 
				local w,h = v.prefixImage:getDimensions()
				self.speech.DrawPreffix = function(x,y)
					love.graphics.draw(x,y,0,1,1,w/2,h/2)
				end		
			else
				self.speech.DrawPreffix = nil 
			end
			if v.suffixImage then 
				local w,h = v.suffixImage:getDimensions()
				self.speech.DrawSuffix = function(x,y)
					love.graphics.draw(x,y,0,1,1,w/2,h/2)
				end		
			else
				self.speech.DrawSuffix = nil 
			end
			local function chose()
				if self.choices[self.queIndex] then 
					self.choice = true 
					local val = self.choices[self.queIndex]
					for k,b in ipairs(val) do 
						b._onEnter(love.graphics.getWidth()/2,love.graphics.getHeight()/2)
						b.OnEnter()
					end 
					self.oldChoice = val
				elseif self.oldChoice then 
					for k,b in ipairs(self.oldChoice) do 
						b._onLeave()
						b.OnLeave()
					end
					self.choice = false 
					self.oldChoice = nil
				end
			end 
			if not self.typing then 
					chose()
			else
				if self.choices[self.queIndex] then 
					self.choice = true
				elseif self.oldChoice then
					for k,b in ipairs(self.oldChoice) do 
						b._onLeave()
						b.OnLeave()
					end
					self.choice = false 
					self.oldChoice = nil
				end
				self.OnTypeEnd = function()	chose() self.OnTypeEnd = nil end
			end 
			self.oldQueIndex = self.queIndex
		end
	end
	local n = 0 
	for i,v in ipairs(self.characters) do 
		if v.active and not v.dontDraw then
			if n == 0 then 
				self.stackBotom = v
			end 
			if n >= 3 then 
				self.stackBotom.active = false
			else
				local img = v:_getMood()
				local sx = -v.sx
				local ox,oy = v.x,v.y
				local w,h = img:getDimensions() 
				local x,y = w/2 +5,h/2
				if n == 0 then 
					x,y = self.w - w/2,h/2
					sx = v.sx
				end 
				n = n + 1
				love.graphics.draw(img,x - ox,y - oy,v.r,sx,v.sy,w/2,h/2)
			end
		end 
	end
	
end
local charac = {}
function nvl:addCharacter(name,color,default_image,x,y,sx,sy,r)
	local sal = {
		name = name,
		color = color or {255,255,255},
		image = default_image,
		states = {},
		x = x or 0,
		y = y or 0,
		sx = sx or 1,
		sy = sy or 1,
		r = r or 0,
		parent = self,
		active = false,
	}
	if type(default_image) == "string" then 
		default_image = love.graphics.newImage(default_image)
		sal.image = default_image
	end
	table.insert(self.characters,sal)
	return setmetatable(sal,{__index = charac})
end
function charac:leave()
	self.active = false
end
function charac:addMood(name,image,color,font,preffixImage,suffixImage)
	local a = {name = name,image = image,color = color,font = font,preffixImage = preffixImage,suffixImage = suffixImage}
	table.insert(self.states,a)
end
function charac:setMood(mood)
	self.mood = mood
end
function charac:getMood()
	return self.mood 
end 
function charac:_getMood()
	for i,v in ipairs(self.states) do
		if self.mood == v.name then
			return v.image,v.color,v.font,v.preffixImage,v.suffixImage
		end
	end	
end
function charac:setStates(tables)
	self.states = tables
end
function charac:say(text,mood,color,font,preffixImage,suffixImage)
	local a = {
	charac = self,
	text = text,
	color = color,
	font = font, 
	preffixImage = preffixImage, 
	suffixImage = suffixImage,
	mood = mood,
	}
	table.insert(self.parent.que,a)
end
function nvl:setButtonSize(w,h)
	self._bw = w 
	self._bh = h
end 
function nvl:addChoice(choice,onClick,onEnter,onLeave,x2,y2,w2,h2)
	local i = 0
	local no = #self.que
	if not self.choices[no] then 
		self.choices[no] = {}
	else 
		i = tablelenght(self.choices[no])
	end 
	local func = function() end 
	
	local a
	local starty = 5
	local choice = {
		choice  = choice,
		_onEnter = function(x,y)
			local x,y = x2 or x,y2 or y
			local w,h = self._bw or 300, self._bh or 40
			local gap = 5
			if not self.activeFrame then 
				self.activeFrame = self.ui:addFrame(nil,x,y,w,h)
			else 
				self.activeFrame:setSize(w,(h)*(i + 1) - gap*(i))
			end
			local w,h = w - gap*2, h - gap*2			
			a = self.ui:addButton(self.activeFrame,gap,gap + (h + gap)*i,w,h)
			a:setText(choice)
			a.color = {55,55,55,155}
			a.OnClick = function()
				if onClick then onClick() end 
			end 
		end,
		_onLeave = function()
			if self.activeFrame then self.activeFrame:remove() end 
			self.activeFrame = nil 
		end,
		OnEnter = onEnter or func,
		OnLeave = onLeave or func,
	}
	table.insert(self.choices[no],choice)
end 
return nvl 