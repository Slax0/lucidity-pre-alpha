local textmanager = {}
function textmanager:new(self,colider,ui,global)
	local s = {
		parent = self,
		colider = colider, 
		ui = ui,
		global = global,
	}
	return setmetatable(s,{__index = textmanager})
end 
local text = {}
function textmanager:addText(x,y,z,font)
	local font = font or love.graphics.getFont()
	local s = {
		parent = self, 
		x = x,
		y = y,
		zmap = z,
		size = 12,
		width = font:getWidth("Text"),
		height = font:getHeight(),
		font = font,
		text = "Text",
		type = "text",
		color = {255,255,255},
	}
	setmetatable(s,{__index = text})
	s:makeColision()
	return s
end 
	function text:getColision()
		return self.colision
	end 
	function text:makeColision()
		if self.colision then self.parent.colider:remove(self.colision) end 
		self.width = math.max(self.font:getWidth(self.text),self.width,10)
		self.height = math.max(self.height,self.font:getHeight())
		self.colision = self.parent.colider:addRectangle(self.x,self.y,self.width,self.height)
		self.colision.editorShape = self
		self.colision.source = self
		self.colision.zmap = self.zmap
		self.colision.userData = self.type 
	end 
	function text:draw()
		local olf = love.graphics.getFont()
		love.graphics.setFont(self.font or olf)
		setColor(self.color)
		love.graphics.print(self.text,self.dx - self.width/2,self.dy-self.height/2)
		love.graphics.setFont(olf)
		setColor()
	end 
	function text:update(dt)
		self.text = tostring(self.text)
		local width,height = findDimensions(self.colision)
		local w,h = (width)/2,(height)/2
		self.topX,self.topY = incrementPos(self.x,self.y,w,h)
		self.dx = self.topX + (self.modx or 0) 
		self.dy = self.topY + (self.mody or 0)
		self.colision:moveTo(self.topX + (self.modx or 0),self.topY + (self.mody or 0))
		if self.OnUpdate then self.OnUpdate(dt) end 
	end 
	function text:setText(t,font)
		local font = font or self.font or love.graphics.getFont() 
		self.font = font 
		

		local lines = 1
		self.text = tostring(t)
		local w = font:getWidth(t)
		local h = font:getHeight(t)
		for word in tostring(t):gmatch("\n") do 
			lines = lines + 1
		end 
		self.width = w 
		self.height = h*lines
		self:makeColision()
	end 
	function text:remove()
		self.parent.colider:remove(self.colision)
	end 
	local function parsecolor(t)
		local c = {}
		for k in string.gmatch(t, "-?[0-9]+[.]?[0-9]*") do
			if #c < 3 then 
				table.insert(c, tonumber(k))
			end 
		end
		if not c[1] then 
			c[1] = 0 
		end 
		if not c[2] then 
			c[2] = 0 
		end 
		if not c[3] then 
			c[3] = 0 
		end 
		return c
	end 
	function text:OnMenu(menu,ui)
		menu:addChoice("Set Text",function()
			local w,h = love.graphics.getDimensions()
			local f = ui:addFrame("Input text",w/2,h/2,200,300)
			f.dragEnabled = true 
			local fh = ui:getFont():getHeight()
			local colorinp = ui:addTextinput(f,5,25,190,fh)
			local font = ui:addTextinput(f,5,25 + fh +5,190,fh)
			font:setText("default")
			font.OnChange = function(t)
				if Fonts[t] then 
					local s,font = pcall(love.graphics.newFont,Fonts[font.text].dir,self.size)
					self.fontDir = Fonts[t].dir
					self.fontName = font.text
					print(s,font,t)
					if not s then 
						self.font = love.graphics.getFont()
					else 
						print(font)
						self.font = font
					end 
				end 
			end 
			local color = self.color[1]..","..self.color[2]..","..self.color[3]..","
			colorinp:setText(color)
			colorinp.OnChange = function(t)
				self.color = parsecolor(t)
			end 
			local allowed = {"1","2","3","4","5","6","7","8","9","0","."}
			local size = ui:addTextinput(f,5,80,190,fh)
			size:setText(self.size)
			size.OnChange = function(t)
				self.size = tonumber(t)
				if self.fontDir then 
					self.font = love.graphics.newFont(self.fontDir,self.size)
				else 
					self.font = love.graphics.newFont(self.size)
				end 
				self:setText(self.text)
			end 
			local textinput = ui:addTextinput(f,5,75 + (fh +5),f.width-10,f.height- (75 +fh + 5))
			textinput:setText(self.text)
			textinput:setMultiline(true) 
			textinput.disallowed = {} 
			textinput.OnChange = function(t)
				self:setText(t)
			end 
			f:setHeader()
			f:addCloseButton()
		end)
	end 
	function text:OnCopy()
		
	end 
	function text:setMod(x,y)
		self.modx  = x 
		self.mody  = y
	end 
	function text:save()
		local v = self 
		local t = {
			text = v.text,
			size = v.size,
			color = v.color,
			fontName = v.fontName,
			x = self.dx or v.x,
			y = self.dy or v.y,
			z = self.zmap,
		}
		t.__NAME = v.__NAME
		local n = {} 
		for i,v in ipairs(v.__triggers or n) do 
			local _ = v:save()
			table.insert(n,_)
		end 
		t.__triggers = n
		return t 
	end 
	
function textmanager:load(data)
	for i,v in ipairs(data) do 
		local t = self:addText(v.x,v.y,v.z)
		t.fontName = v.fontName
		if t.fontName and Fonts[t.fontName] then 
			t.font = love.graphics.newFont(Fonts[t.fontName].fontDir,v.size)
			t.fontDir = Fonts[t.fontName].fontDir
		else 
			t.font = love.graphics.newFont(v.size)
		end 
		t.color = v.color 
		t.size = v.size 
		t:setText(v.text)
		if v.__NAME then 
			t.__NAME = v.__NAME 
			self.parent._G[v.__NAME] = v
		end 
		if v.__triggers then 
			for k,m in ipairs(v.__triggers) do 
				local a = self.parent.triggerAdmin:addTrigger(t)
				a:load(m)
				if not t.__triggers then t.__triggers = {} end 
				table.insert(t.__triggers,a)
			end 
		end 
	end 
end 
return textmanager