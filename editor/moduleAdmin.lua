local function scanInstances(mod,src,alt_src,dir,instance,parent)
	local parent = getParents(eCurrentMap)
	print("SCANING INSTANCES")
	print("PARENT:",parent)
	local modN = mod.name
	if not love.filesystem.exists(dir) then love.filesystem.createDirectory(dir) end 
	local instances  = mod:getInstances()
	local function scanInstance(instance)
		local found = false
		if love.filesystem.exists(dir.."/"..instance) and love.filesystem.exists(dir.."/"..instance.."/src.lua") then 
			found = true
		elseif parent and type(parent) == "string" then 
			local lf = love.filesystem
			if love.filesystem.exists(parent.."/"..instance) and love.filesystem.exists(parent.."/"..instance.."/src.lua") then 
				found = true 
			end 
		elseif parent then 
			local lf = love.filesystem
			for i,v in ipairs(parent) do 
				local dir = "/modules/"..modN.."/derivatives/" 
				dir = string.gsub(v..dir.."/"..instance,"//","/")
				print("DIR:",dir)
				if lf.exists(dir) and lf.exists(dir.."/src.lua") then 
					found = true
				end 
			end 
		end 	
	print("-------------------")
	print("FOUND: ",found)
		if not found then  
			if not love.filesystem.exists(string.gsub(dir,"/derivatives","").."/src.lua") then 
				local dir = string.gsub(dir,"/derivatives","")
				local src = string.gsub(src,"/derivatives","")
				print(dir,src)
				for i,v in ipairs(love.filesystem.getDirectoryItems(src)) do 
					print(src.."/"..v,dir.."/"..v)
					if not love.filesystem.isDirectory(src.."/"..v) then 
						copyFile(src.."/"..v,dir.."/"..v)
					end 
				end 
			end 
			if love.filesystem.exists(src.."/"..instance) then 
				local d = dir.."/"..instance
				if not string.find(d,"derivatives") then 
					d =  dir.."/derivatives/"..instance
				end 
				print("COPYING A ",src.."/"..instance,d)
				copyFiles(src.."/"..instance,d)
			elseif love.filesystem.exists(alt_src.."/"..instance) then 
				print("COPYING C ",alt_src.."/"..instance,dir.."/"..instance)
				copyFiles(alt_src.."/"..instance,dir.."/"..instance)
			else 
				print("Cant find derivative!",mod.name,instance)
			end 
		end	
	end 
	if instance then 
		scanInstance(instance)
	else 
		for i,v in pairs(instances) do 
			scanInstance(v.name)
		end 
	end 
	return src_dir
end 
local function mergeDerivatives(name)
	local parents = getParents(eCurrentMap)
	local derivatives = {} 
	local fs = love.filesystem
	if parents then 
		for i,v in ipairs(parents) do 
			local dir = "/modules/"..name
			dir = string.gsub(v..dir,"//","/")
			if fs.exists(dir) and fs.exists(dir.."/src.lua") then 
				print("LOOKING AT, FOR DERIV:",dir)
				local ndir = dir.."/derivatives/"
				local files = fs.getDirectoryItems(ndir)
				for i,v in ipairs(files) do 
					print("ADDING DERIVATIVES TO POOL:",ndir)
					table.insert(derivatives,ndir.."/"..v)
				end 
			else 
				print("IGNORING: ",dir)
			end  
		end 
	end 
	return derivatives
end 
	-- local p = dir.."/derivatives/"..v
local function installDerviative(dir,derivatives,env,i)
	local _,_1 = string.find(dir,".*/")
	local v = string.sub(dir,_1)
	local v = string.gsub(v,"/","")
	local found = false 
	for _,k in ipairs(derivatives) do 
		if k.name == v then 
			found = true 
		end 		
	end 
	if not found then 
		print("INSTALING DERIVATIVE: "..v)
		local p = dir
		local p = string.gsub(p,"//","/")
		if love.filesystem.exists(dir.."/src.lua") then 
			local b,err = pcall(love.filesystem.load(p.."/src.lua"))
			if not b then 
				print("LOADING FROM: ",p.."/src.lua")
				print("Module: "..v.." failed to load \n Returned error: \n"..err)
			else 
			
				local fn = love.filesystem.load(p.."/src.lua")
				setfenv(fn,env)
				local a = fn()
				print("Loading: "..p.."/src.lua","Got "..tostring(a))
				if type(a) == "table" then 
					local a = {name = v,src = a,dir = p,index = i}
					table.insert(derivatives,a)
				else 
					print("Invalid Module: Got:"..type(a).." Expected: Table")
				end 
			end 
		else 
			print("NO SOURCE: "..v.." AT: "..dir.."/src.lua")
		end 
	end 
end 

local moduleAdmin = {}
local module = {}
function moduleAdmin:new(self,ui,colider,phyWorld)
	local s = {parent = self,ui = ui,colider = colider,activeModules = {},modules = {},phyWorld = phyWorld,registrat = {}}
	s.env = getEnv()
	setmetatable(s,{__index = moduleAdmin})
	return s
end
	function moduleAdmin:seekInstance(m,own)
		for i,v in pairs(self.registrat) do 
			if v.module == m and v.own == own then 
				return v.act
			end 			
		end 
	end
	function moduleAdmin:seek(name)
		local mod 
		for i,v in pairs(self.modules) do
			if string.lower(name) == string.lower(v.name) then 
				mod = v 
			end 
		end 
		return mod 
	end
	function moduleAdmin:newModule(dir,name)
		local s = {parent = self,name = name,dir = dir}
		local env = self.env
		local found
		local mod 
		print("dir:name",dir,name)
		for i,v in pairs(self.modules) do 
			if name == v.name then 
				found = true 
				mod = v 
			end 
		end 
		if not found or not mod then 
			local derivatives = {}
			if not love.filesystem.exists(dir.."/src.lua") then 
				local parents = getParents(eCurrentMap)
				if parents then 
					for i,v in ipairs(parents) do 
						local ndir = v.."/modules/"..name
						ndir = string.gsub(ndir,"//","/")
						print("LOOKING AT:",ndir)
						if love.filesystem.exists(ndir) and love.filesystem.exists(ndir.."/src.lua") then 
							print("FOUND MODULE AT: ",ndir)
							dir = ndir
						end 
					end 
				else 
					error("Corrupted map: cannot find source for: "..name.." in "..dir.." has no children.")
				end 
			end 
			s.actual = love.filesystem.load(dir.."/src.lua")
			setfenv(s.actual,self.env)
			print("---------0--------0---------")
			print("LOADING MODULE:"..dir)
			if type(s.actual) == "function" then 
				s.actual = s.actual(dir)
			end 
			local files = love.filesystem.getDirectoryItems(dir.."/derivatives")
			if not love.filesystem.exists(dir.."/derivatives") then 
				print("No derivatives for: "..tostring(dir).." use the folder: "..(dir or "<module>").."/derivatives")
			end 
			for i,v in ipairs(files) do 
				local p = dir.."/derivatives/"..v
				installDerviative(p,derivatives,env,i)
				
			end
			for i,v in ipairs(mergeDerivatives(name)) do 
				installDerviative(v,derivatives,env,i)
			end 
			s.derivatives = derivatives
			s.instances = {}
			table.insert(self.modules,s)
			setmetatable(s,{__index = module})
			return s 
		else
			return mod
		end
	end
	function module:remove()
		for i,v in pairs(self.parent.modules) do 
			if v == self then 
				table.remove(self.parent.modules,i)
			end 
		end 
		for i,v in pairs(self.instances) do 
			if v.remove then v:remove() end 
			self.instances[i] = nil 
		end 
	end 
	function module:seek(deriv)
		local d 
		for i,v in ipairs(self.derivatives) do 
			if v.name == deriv then 
				d = v 
			end 			
		end
		return d 		
	end 
	function module:getInstances()
		return self.instances
	end 
		function module:addDeriv(dir,name)
			local b,err = pcall(love.filesystem.load(dir.."/src.lua"))
			local v = name
			if not b then 
				print("Module: "..v.." failed to load \n Returned error: \n"..err)
			else 
				local a = love.filesystem.load(dir.."/src.lua")()
				print("Loading: "..dir.."/src.lua")
				if type(a) == "table" then 
					local a = {name = v,src = a,dir = dir}
					table.insert(self.derivatives,a)
				else 
					print("Invalid Module: Got:"..type(a).." Expected: Table")
				end 
			end 
		end 
		function module:addInstance(name,x,y,zmap,loading,parent)
			print("Name:",name)
			local s = {
				module = self.name,
				parent = self,
			}
			table.insert(self.instances,s)
			if not self.actual then 
				self.actual = love.filesystem.load(self.dir.."/src.lua")
				setfenv(self.actual,self.parent.env)
				if type(self.actual) == "function" then 
					self.actual = self.actual(self.dir)
				end 
			end 
			local module_src = self.actual
			local src 
			local dir
			local index 
			for i,v in pairs(self.derivatives) do
				if v.name == name then 
					name = v.name 
					src = v.src
					dir = v.dir
					index = v.index 
				end 
			end
			s.name = name 
			assert(src,"Error loading module"..tostring(name).." No source returned/found, make sure you return the table at the end of the src.lua")
			if src.getDependencies then
				local d = {} 
				local dep = src:getDependencies()
				for i,v in pairs(dep) do 
					local mod = v.module 
					local deriv = v.derivative
					local mod_dir
					local der_dir 
					local module 
					local derivative
						
						for b,k in pairs(self.parent.modules) do 
							print("Names:",k.name,mod)
							if k.name == mod then 
								module = mod.actual
								mod_dir = mod.dir
								for n,a in pairs(k.derivatives) do 
									if a.name == deriv then 
										derivative = a.src
										der_dir = a.dir
									end 								
								end 
							end 
						end 
					if not module then 
						local src 
						local mod = tostring(mod)
						local found 
						local loc 
						if love.filesystem.exists(eCurrentMap.."/modules/"..mod) then 
							src = eCurrentMap.."/modules/"..mod
							loc = eCurrentMap.."/modules/"..mod
						elseif parent then 
							print("Has Parents")
							for i,v in ipairs(parent) do 
								print(v.name.."/modules/"..mod)
								if love.filesystem.exists(v.name.."/modules/"..mod) then 
									src = v.name.."/modules/"..mod
									loc = v.name.."/modules/"..mod
								end 
							end 
						end 
						if not src and love.filesystem.exists("resources/modules/"..mod) then
							src = "resources/modules/"..mod						
						end 			
						if src then 
							module = self.parent:newModule(src,mod)
							if not loc then 
								if parents then 
									loc = parents[1].."/modules/"..mod 
								else 
									loc = eCurrentMap.."/modules/"..mod
								end 
							else 
								loc = loc.."/derivatives"
							end 
							scanInstances(module,src.."/derivatives","resources/modules/"..mod.."/derivatives",loc,deriv,parent)
							module:remove()
							module = self.parent:newModule(src,mod)
							mod_dir = mod 
							if #module.derivatives < 1 then 
								print("MISSING DEPENDENCY [Derivative]: "..tostring(mod)..": "..tostring(deriv))
							end 
							for m,n in pairs(module.derivatives) do 
								if n.name ~= deriv then 
									print("MISSING DEPENDENCY [Derivative]: "..tostring(mod)..": "..tostring(deriv))
								else 
									derivative = n.src 
									der_dir = n.dir 
								end 
							end 
						else
							print("MISSING DEPENDENCY [Module]: "..tostring(mod)..": "..tostring(deriv))
						end 
					elseif not derivative then 
						local deriv = tostring(deriv)
						print("MISSING DEPENDENCY [Derivative]: "..tostring(mod)..": "..tostring(deriv))
					end 
					assert(module,"cannot find module by name of:",mod)
					module.dependency = true
					local g = {derivative = derivative,module = module.actual,mod_dir = mod_dir ,der_dir = der_dir}
					table.insert(d,g)
				end 
				src:setDependencies(d)
			end 
			print("##########",self.actual,module_src)
			if src.new then 
				s.actual = src:new(module_src,dir,self.parent.parent,self.parent.parent.shapeCollider,self.parent.parent.phyWorld,x,y)
				local act = s.actual
				act.___Reference = {module = self.index,own = index,insta = #self.parent.registrat+1}	
				local ref = {module = self.index,own = index,type = act.type,act = act}	
				table.insert(self.parent.registrat,ref)
			else 
				print("No NEW function found for "..tostring( name ).." please use <module>:new(src(the source module metatable),dir,editor,colider,phyWorld,x,y)")
			end 
			local var = s.actual
			if not var.zmap then var.zmap = self.parent.parent.current_zmap end
			if not var.getColision then 
				print("No getColision function detected for "..tostring( name ).." please set <module>:getColision to return a HardonCollider shape.")
			else 
				local function setCol(c)
					c._module = s.actual
					c._source = s.actual
					c._OnRemove = function()
						for i,v in pairs(self.instances) do 
							if v == s then 
								table.remove(self.instances,i)
							end 
						end 
					end
				end 
				local c = var:getColision()
				assert(c,"No colision provided by <module>:getColision()")
				setCol(c)
				var.OnMakeColision = function(c) 
					assert(c,"OnMakeColision needs a HardonCollider shape as parameter")
					setCol(c)
				end 
			end 
			if not var.remove then 
				print("No REMOVE function found for "..tostring(name).." please use <module>:remove()\n")
			end 
			if not var.setZmap then 
				print("No setZmap function found for "..tostring(name).." please use <module>:setZmap(z)\n")
			else 
				if not loading then 
					var:setZmap(zmap)
				end 
			end 
			if not var.moveTo then 
				print("No moveTo function found for "..tostring(name).." please use <module>:moveTo(x,y)\n")
			else 
				if not loading then 
					var:moveTo(x,y)
				end 
			end 
			if not var.draw then 
				print("No draw function found for "..tostring(name).." please use <module>:draw(debug)\n")
			end 
			if not var.update then 
				print("No update function found for "..tostring(name).." please use <module>:update(dt)\n")
			end 
			if not var.load then 
				print("No Load function found for "..tostring(name).." please use <module>:load(table) with return value of self, \n The table is the table made when saving!\n")
			end 
			if not var.save then 
				print("No Save function found for "..tostring(name).." please use <module>:save() with return value of a table. \n Make sure that no images or functions are returned even within tables!\n")
			end 
			if not var.setMod then 
				print("No setMod function found for "..tostring(name).." please use <module>:setMod(mx,my) \n This is for parallax, ignore this if you don't want parallax\n")
			end 
			return s
		end
function moduleAdmin:getModules()
	return self.modules
end 
function moduleAdmin:getModulesFrom(dir,make)
	local ndir = dir.."/modules" 
	local m = {} 
	if not love.filesystem.exists(ndir) then
		print("No directory: "..ndir)
	else 
		local files = love.filesystem.getDirectoryItems(ndir)
		for i,v in ipairs(files) do 
			local deriv = love.filesystem.exists(ndir.."/"..v.."/derivatives")
			local derivs
			if deriv then 
				derivs = love.filesystem.getDirectoryItems(ndir.."/"..v.."/derivatives")
				for i,v in ipairs(derivs) do 
					derivs[i] = {name = v}
				end 
			else 
				print("No derivatives: "..v)
				derivs = {}
			end 
			local v = {dir = ndir.."/"..v,name = v,derivatives = derivs}
			table.insert(m,v)
		end 
	end 
	if make then 
		m = self:make(m)
	end 
	return m 
end
function moduleAdmin:make(tables)
	for i,v in pairs(tables) do 
		tables[i] = self:newModule(v.dir,v.name)
	end 
end 
function moduleAdmin:save(map,source,parents)
	local source = source 
	local filter 
	if type(source) == "function" then 
		source = nil 
		filter = source 
	end 
	local fsys = love.filesystem 
	local g = {}
	if #self.modules > 0 then 
		if not love.filesystem.exists(map.."/modules/") then 
			fsys.createDirectory(map.."/modules/")
		end 
	end 
	for i,v in ipairs(self.modules) do 
		local s = true 
		if filter then 
			s = filter(v)
		end 
		if s then 
			if #v.instances > 0 or v.dependency then
				local b = self:saveModule(v,map,source,parents)
				table.insert(g,b)
			end 
		end 
	end 
	return g 
end
function moduleAdmin:saveModule(module,map,source,parents)
	local fsys = love.filesystem 
	local v = module
	local instances = {}
	local b = {name = v.name}
	local name = v.name
	local path = map.."/modules/"
	local found 
	if love.filesystem.exists(path..v.name) then
		found = true 
	elseif parents then 
		for k,m in ipairs(parents) do 
			print("cheking: ",m.."/modules/"..v.name)
			if love.filesystem.exists(m.."/modules/"..v.name) then 
				found = true 
			end 
		end 
	end 
	print("F",name,found)
	if not found then 
		self:copyModule(module,map,source,name)
	end 
	for k,m in ipairs(v.instances) do 
		local c 
		if m.actual.save then c = m.actual:save() else print ("No <module>:save() function for: "..m.name) end 
		if not c then 
			print("No return table!")
			c = {}
		end 
		c.x = c.x or 0
		c.y = c.y or 0 
		c.zmap = c.zmap or 1 
		c.REF = m.actual.___Reference
		c.__NAME = m.actual.__NAME
		local n = {} 
		for i,v in ipairs(m.actual.__triggers or n) do 
			local _ = v:save()
			table.insert(n,_)
		end 
		c.__triggers = n
		local a = {name = m.name,data = c}
		table.insert(instances,a)
	end 
	b.instances = instances
	return b 
end  
function moduleAdmin:copyModule(module,map,source,name)
	local source = source or "/resources"
	local name = name or module.name
	local srcP =  source.."/modules"
	local src_modPath = srcP.."/"..name
	local src_modPath_d = src_modPath.."/derivatives"
	
	local srcA = "resources/modules"
	local alt_src_mod = srcA.."/"..name
	local alt_src_mod_d = alt_src_mod.."/derivatives"
	
	
	local dest = map.."/modules"
	local dest_modPath = dest.."/"..name 
	local dest_modPath_d = dest_modPath.."/derivatives"
	

	local function copyMod(dir,dest)
		local fil = love.filesystem.getDirectoryItems(dir) 
		if not love.filesystem.exists(dest) then love.filesystem.createDirectory(dest) end
		for i,v in ipairs(fil) do 
			local f = dir.."/"..v
			if love.filesystem.isFile(f) then 
				local d = love.filesystem.newFileData(f)
				love.filesystem.write(dest.."/"..v,d)
			end 
		end 
	end 
	if love.filesystem.exists(dest_modPath) then 
		print(dest_modPath.."Exists")
		scanInstances(module,src_modPath_d,alt_src_mod_d,dest_modPath_d)
	else 
		if love.filesystem.exists(src_modPath) then 
			print(src_modPath.."Exists")
			copyMod(src_modPath,dest_modPath)
			scanInstances(module,src_modPath_d,alt_src_mod_d,dest_modPath_d)
		elseif love.filesystem.exists(alt_src_mod) then 
			print(alt_src_mod.."Exists")
			copyMod(alt_src_mod,dest_modPath)
			scanInstances(module,src_modPath_d,alt_src_mod_d,dest_modPath_d)
		else 
			print("Cant find Module: ",name)
		end 
	end 
end 
function moduleAdmin:checkModule(src,name,instances,parents)
	local src 
	local module 
	local mod = tostring(name)
	local sucess 
	if love.filesystem.exists(eCurrentMap.."/modules/"..mod) and love.filesystem.exists(eCurrentMap.."/modules/"..mod.."/src.lua") then 
		src = eCurrentMap.."/modules/"..mod
	elseif parents then 
		for i,v in ipairs(parents) do 
			if love.filesystem.exists(v.."/modules/"..mod) and love.filesystem.exists(v.."/modules/"..mod.."/src.lua") then 
				src = v.."/modules/"..mod
			end 
		end 
	end 
	if not src then 
		if love.filesystem.exists("resources/modules/"..mod) and love.filesystem.exists("resources/modules/"..mod.."/src.lua") then
			src = "resources/modules/"..mod
		end 		
	end 
	if src then 
		module = self:newModule(src,mod)
		sucess = true 
		for m,k in pairs(instances) do 
			local v =  k.data.name
			scanInstances(module,eCurrentMap.."/modules/"..mod.."/derivatives","resources/modules/"..mod.."/derivatives",eCurrentMap.."/modules/"..mod.."/derivatives",v,src.."/modules/"..mod.."/derivatives",parents)
		end 
		if #module.derivatives < 1 then 
			print("MISSING DEPENDENCY [Derivative]: "..tostring(mod)..": "..tostring(deriv))
		end 
	else
		print("MISSING DEPENDENCY [Module]: "..tostring(mod)..": "..tostring(deriv))
	end 
	if sucess then 
		module:remove() 
		module = self:newModule(src,mod)
		if not love.filesystem.exists(eCurrentMap.."/modules/"..mod.."/src.lua") then 
			local data = love.filesystem.newFileData(src.."/src.lua")
			print(src.."/src.lua")
			love.filesystem.write(eCurrentMap.."/modules/"..mod.."/src.lua",data)
		end 
	end 
	return module 
end 
function moduleAdmin:load(map,tables,parents)
	print("--LOADING MODULES---")
	print("From Maps: ",map)
	print("Parents: "..Tserial.pack(parents or {}))
	for i,v in ipairs(tables) do 
		local found 
		local lf = love.filesystem
		local _dir = map.."/modules/"..v.name
		local source = map 
		if lf.exists(_dir) and lf.exists(_dir.."/src.lua") then 
			found = true 
		elseif parents then 
			for m,k in ipairs(parents) do 
				local dir = k.."/modules/"..v.name
				if lf.exists(dir) and lf.exists(dir.."/src.lua") then 
					found = true
					source = k
				end 
			end 
		end 
		if not found then 
			self:checkModule("resources/modules/",v.name,v.instances,parents)
		end 
		local mod = self:newModule(source.."/modules/"..v.name,v.name)
		mod.index = i 
		local mod_name = v.name 
		local path = source.."/modules/"..v.name
		for k,b in ipairs(v.instances) do 
			local p = b.data
			local dir 
			if not love.filesystem.exists(path.."/derivatives/"..b.name) then 
				if parents then 
					for m,k in ipairs(parents) do 
						if love.filesystem.exists(k.."/modules/"..v.name.."/derivatives/"..b.name) then 
							found = true
						end 
					end 
				end 
				if not found then 
					scanInstances(mod,eCurrentMap.."/modules/"..mod_name.."/derivatives","resources/modules/"..mod_name.."/derivatives",eCurrentMap.."/modules/"..mod_name.."/derivatives",b.name,parents)
				end 
			end 
			local c = mod:addInstance(b.name,nil,nil,p.zmap,true,parents)
			local act = c.actual
			if act.load then act:load(p) else print("No <module>:load(table) function for "..b.name) end 	
			if p.__NAME then 
				act.__NAME = p.__NAME 
				self.parent._G[act.__NAME] = act
			end 
			if p.__triggers then 
				for i,v in ipairs(p.__triggers) do 
					local a = self.parent.triggerAdmin:addTrigger(act)
					a:load(v)
					if not act.__triggers then act.__triggers = {} end 
					table.insert(act.__triggers,a)
				end 
			end 
		end 
	end 
end 
function moduleAdmin:getInstances()
	local instances = {}
	for i,v in ipairs(self.modules) do 
		for k,m in ipairs(v.instances) do 
			table.insert(instances,m.actual)
		end 
	end 
	return instances
end 
function moduleAdmin:clear()
	for i,v in pairs(self.modules) do 
		v:remove()
		self.modules[i] = nil 
	end 
end 


return moduleAdmin