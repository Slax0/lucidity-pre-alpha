local dock = {}
local function findMap(str)
	local _,_1 = string.find(str,".*/")
	str = string.sub(str,_1+1)
	return str 
end
local mt = {__index = dock}
function dock:new(salf,ui,collider,icons)
	local self = {}
	setmetatable(self,mt)
	local names = {
		"Image",
		"Rectangle",
		"Circle",
		"Polygon",
		"Sound",
	}
	local buttons = {
		{name = "M",func  = function()
			
			if self.moduleFrame then 
				self.moduleFrame:remove()
				self.moduleFrame = nil 
			else 
				local modAdmin = salf.moduleAdmin
				local frame = ui:addFrame("Modules:",salf.width/2,salf.height/2,200,400)
				frame:setHeader()
				frame.isWindow = true 
				self.moduleFrame = frame
				frame.dragEnabled = true
				local close = ui:addButton(frame,frame.width - 20,0,20,20)
				close.text = "X"
				close.OnClick = function()
					self.moduleFrame:remove()
					self.moduleFrame = nil
				end 
				local list = ui:addList(frame,2,20,frame.width - 4,frame.height - 22 - 50)
				local back = ui:addButton(frame,2,frame.height - (2 + 25),frame.width -4,25)
				back:setText("Back")
				local src = ui:addButton(nil,0,0,frame.width - 4,20)
				src:setText("resources/")
				local w = frame.width - 4
				local function listModules(mods,dir,copyblock)
					list:clear()
					local mod
					local function listDerivatives(deriv,mod,copyblock)
						list:clear()
						for i,v in pairs(deriv) do 
							local b = ui:addButton(nil,0,0,w,20)
							list:addItem(b)
							b:setText(v.name)
							b.OnClick = function()
								fileStructure(salf.map)  
								print("DIRECTORY:","resources/modules/"..mod)
								local files = love.filesystem.getDirectoryItems("resources/modules/"..mod)
								if not love.filesystem.exists(salf.map.."/modules/"..mod) then 
									love.filesystem.createDirectory(salf.map.."/modules/"..mod)
								end 
								for i,v in ipairs(files) do 
									print("FILE: "..v)
									if v ~= "derivatives" then 
										print("Full Path: ",salf.map.."/modules/"..mod.."/"..v)
										if not copyblock then 
											if not love.filesystem.exists(salf.map.."/modules/"..mod.."/"..v) then 
												copyFile("resources/modules/"..mod.."/"..v,salf.map.."/modules/"..mod.."/"..v)
											end
										end 
									end 
								end 
								local dest = salf.map.."/modules/"..mod.."/derivatives/"..v.name								
								local dirx = dir.."/"..mod.."/derivatives/"..v.name
								if not love.filesystem.exists(dest) and not copyblock then 
									copyFiles(dirx,dest)
								end 
								
								local mod = modAdmin:newModule(dir.."/"..mod,mod)
								mod:addDeriv(dest,v.name)
								print("@@@@@",mod)
								local inst = mod:addInstance(v.name,salf.mx,salf.my,salf.current_zmap)
								salf.creatingItem = inst.actual
								table.insert(salf.all_active,inst.actual)
							end 
						end 
						back.OnClick = function()
							listModules(mods,dir)
						end 
					end 
					for i,v in pairs(mods) do 
						local b = ui:addButton(nil,0,0,w,20)
						list:addItem(b)
						b:setText(v.name)
							b.OnClick = function()
								mod = v.name 
								local deriv = v.derivatives
								listDerivatives(deriv,tostring(v.name),copyblock)
							end 
						end
					back.OnClick = function()
							list:clear()
							local src = ui:addButton(nil,0,0,frame.width - 4,20)
							src:setText("resources/")
							src.OnClick = function()
								local modules = modAdmin:getModulesFrom("resources/")
								listModules(modules,"resources/modules")
							end 
							list:addItem(src)
							
							src = ui:addButton(nil,0,0,frame.width - 4,20)
							if string.find(salf.map,"/children/") then 
								str = findMap(salf.map)
								local src = ui:addButton(nil,0,0,frame.width - 4,20)
								local parent = getParents(salf.map)
								src:setText(parent[1])
								src.OnClick = function()
									local modules = modAdmin:getModulesFrom(parent[1])
									listModules(modules,parent[1],true)
								end 
							list:addItem(src) 
							else 
								str = salf.map
							end 
							src:setText(str)
							src.OnClick = function()
								local modules = modAdmin:getModulesFrom(salf.map)
								listModules(modules,salf.map.."/modules/")
							end 
							list:addItem(src) 
						end 
					end 
				src.OnClick = function()
					local modules = modAdmin:getModulesFrom("resources/")
					listModules(modules,"resources/modules")
				end 
				list:addItem(src)
				src = ui:addButton(nil,0,0,frame.width - 4,20)
				if string.find(salf.map,"/children/") then 
					str = findMap(salf.map)
					local src = ui:addButton(nil,0,0,frame.width - 4,20)
					local parent = getParents(salf.map)
					src:setText(parent[1])
					src.OnClick = function()
						local modules = modAdmin:getModulesFrom(parent[1])
						listModules(modules,parent[1],true)
					end 
				list:addItem(src) 
				else 
					str = salf.map
				end 
				src:setText(str)
				src.OnClick = function()
					local modules = modAdmin:getModulesFrom(salf.map)
					listModules(modules,salf.map.."/modules/")
				end 
				list:addItem(src) 
			end
		end},
		{name = "Se",func    = function() 	
			salf.tool = "random"
		end},
		{name = "Re",func    = function() 	
			salf.tool = "rect"
		
		end},
		{name = "C",func    = function()	
			salf.tool = "circle"
		
		end},
		{name = "P",func    = function() 	
			salf.tool = "poly"
		
		end},
		{name = "Hats",func    = function() 
			local x,y = salf.x,salf.y
			local item = salf.textAdmin:addText(x,y,salf.zmap)
			salf.creatingItem = item
			table.insert(salf.all_active,item)
		end},
		{name = "Sc",func    = function() 	
			local scale = salf.scale + math.floor((0.15*salf.scale)*100)/100
			if scale > 0 and scale < 20 then
				salf.scale = scale
			else
				self.scale = 20
			end
		end,
		funcr = function()
			local scale = salf.scale - math.floor((0.15*salf.scale)*100)/100
			if scale > 0 and scale > 0.1 then
				salf.scale = scale
			else
				salf.scale = 0.1
			end
		end},
	}
	local width,height = love.graphics.getDimensions()
	local w = 40
	local gapx = w*4 
	local pw,ph = width - (gapx*2),23
	local mainFrame = ui:addFrame(nil,gapx + pw/2,height - ph/2,pw,ph)
	mainFrame.color = basicFrameColor
	mainFrame.zmap = 20
	mainFrame.isWindow = true 
	local ox = (pw - ((w)*#buttons))/#buttons
	for i,v in ipairs(buttons) do 
		local x,y = (w + ox)*(i - 1) + ox/2,ph/2 - w/2 + 5
		local shape = collider:addCircle(x - w/2,y,math.floor(w/2))
		local a = ui:addButton(mainFrame,x,y,w,nil,nil,shape)
		a.OnClick = function() v.func() salf.cursor = a end
		a.OnRightClick = v.funcr
		a.dontDrawB = true
		a.OnUpdate = function()
			salf.iconBatch:add(salf.icons[1][i],a.x,a.y,0,1,1,24/2,19/2)
			if salf.cursor == a then 
				local x,y = love.mouse.getPosition()
				salf.iconBatch:add(salf.icons[1][i],x,y)
				a:setInactive(true)
			else 
				a:setInactive(false)
			end 
		end 
		a.color = colors.lightGray
		buttons[i].child = a
	end
	
	local w = 15
	local x,y = width - w - 5,w + 5
	local shape = collider:addCircle(x,y,w)
	
	local ruler = ui:addButton(nil,x,y,w,nil,"Ru",shape)
	ruler.OnClick = function()
		if salf.drawTools.ruler[1] then 
			salf.drawTools.ruler[1] = false
		else 
			salf.drawTools.ruler[1] = true
		end 
		salf.ruler_on = not salf.ruler_on 
	end 
	ruler.OnUpdate = function()
		local q = salf.icons[2][2]
		local x,y = ruler.colision:bbox()
		salf.iconBatch:add(q,x,y)
	end
	ruler.dontDraw = true
	ruler.color = colors.gray
	
	salf.toggleRuler = function()
		ruler.OnClick()
	end 
	x,y = width - w - 5,w*3 + 10
	local shape = collider:addCircle(x,y,w)
	
	local lock = ui:addButton(nil,x,y,w,nil,nil,shape)
	lock.OnClick = function() 
		if salf.Lock then 
			salf.Lock = false
		else 
			salf.Lock = true
		end
	end 
	lock.OnUpdate = function()
		local x,y = lock.colision:bbox()
		if salf.Lock then 
			local q = salf.icons[2][3]
			salf.iconBatch:add(q,x,y)
		else
			local q = salf.icons[2][4]
			salf.iconBatch:add(q,x,y)
		end 	
	end 
	
	lock.dontDraw = true
	lock.color = colors.gray
	
	salf.toggleLock = function() lock.OnClick() end 
	
	x,y = width - w*3 - 10, w + 5
		local shape = collider:addCircle(x,y,w)
	local grid = ui:addButton(nil,x,y,w,nil,"G",shape)
	grid.dontDraw = true 
	grid.OnUpdate = function()
		local x,y = grid.colision:bbox()
		local q = salf.icons[2][1]
		salf.iconBatch:add(q,x,y)
	end
	grid.OnClick = function()
		if salf.drawTools.grid[1] then 
			salf.drawTools.grid[1] = false
		else 
			salf.drawTools.grid[1] = true
		end  
	end 
	grid.dontDrawB = true
	grid.color = colors.gray
	
	salf.toggleGrid = function() 
		grid.OnClick()
	end  
	
	
	
	x,y = width - w - 5,salf.height - (w*3 + 10)
	local shape = collider:addCircle(x,y,w)
	local layers = ui:addButton(nil,x,y,w,nil,"LA",shape)
	layers.dontDrawB = true 
	layers.color = colors.gray
	layers.OnUpdate = function()
		local x,y = layers.colision:bbox()
		local q = salf.icons[2][6]
		salf.iconBatch:add(q,x,y)
		
	end
	
	local frame 
	local Props
	layers.OnClick = function()
		if not frame then 
			frame = ui:addFrame("Layers",salf.width/2,salf.height/2,200,185)
			frame.isWindow = true 
			frame:addCloseButton(function()
				frame = nil 
			end)
			frame.color = basicFrameColor
			frame:setHeader()
			frame.dragEnabled = true
			local up = ui:addButton(frame,2,160,20,20)
			up:setText("UP")
			up.OnClick = function()
				local mi,ma = salf.layers:getRange()
				local layer = salf.layers:seek(salf.current_zmap)
				if inRangeHigher(mi,salf.current_zmap,ma) then 
					salf.layers:moveLayers(salf.current_zmap,salf.current_zmap -1)
					salf.refreshLayers()
				end
			end 
			
			local down = ui:addButton(frame,frame.width - 22,160,20,20)
			down:setText("Down")
			down.OnClick = function()
				local mi,ma = salf.layers:getRange()
				local layer = salf.layers:seek(salf.current_zmap)
				if inRangeLower(mi,salf.current_zmap,ma) then 
					salf.layers:moveLayers(salf.current_zmap,salf.current_zmap +1)
					salf.refreshLayers()
				end 
			end 
			
			
			local gap = 15
			local ox = 24
			local new = ui:addButton(frame,gap + ox ,160,20,20)
			new.text = "N"
			new.OnClick = function()
				-- local layers = {} 
				-- for i,v in ipairs(salf.LayerList.list) do 
					
				-- end 
				local mi,ma = salf.layers:getRange()
				local zmap =  salf.current_zmap
				local hzmap = ma + 1
				salf.layers:addLayer("New Layer: "..hzmap,hzmap,0,0,true,false)
				if zmap ~= ma-1 and salf.layers:seek(zmap) then 
					local index = -1 
					for i=hzmap,zmap+2,index do 
						if salf.layers:seek(i) and salf.layers:seek(i+index) then 
							print("Moving layers:",i,i+index)
							salf.layers:moveLayers(i,i+index)
							if salf.LayerList then salf.LayerList.OnRefresh() end
						end 
					end 
					salf.current_zmap =  salf.current_zmap 
				end 
				salf.current_zmap = zmap + 1
				if salf.LayerList then salf.LayerList.OnRefresh() end
			end 
			local merge = ui:addButton(frame,20 + (gap*2) + ox,160,20,20) 
			merge.text = "M"
			local delete = ui:addButton(frame,40 + (gap*3) + ox,160,20,20)
			delete.text = "D"
			delete.OnClick = function()
				warning:new(ui,"This will remove the layer \n Confirm? ",
				function()
					salf.layers:removeLayer(salf.current_zmap)
					local min,max = salf.layers:getRange()
					salf.current_zmap = max
					if salf.LayerList then salf.LayerList.OnRefresh() end
				end)
			end 
			local frame2
			local prop = ui:addButton(frame,60 + (gap*4) + ox,160,20,20)
			prop.text = "P" 
			prop.OnClick = function()
				if frame2 then frame2:remove() frame2 = nil end
				if not frame2 then 
					local x = frame.x - frame.width
					local w = -frame.width 
					if x < 0 then 
						w = frame.width
					end 
					frame2 = ui:addFrame("Properties",frame.x,frame.y,frame.width,frame.height)
					frame2.tempDMonitor = true
					Props = frame2
					frame2:setHeader()
					frame2.OnUpdate = function()
						if frame then 
							frame2.x = frame.x + w 
							frame2.y = frame.y
						else
							frame2:remove()
						end 
					end
					frame2.color = basicFrameColor
					local text = {
						"Name:",
						"Mod X:",
						"Mod Y:",
						"Colision:",
						"Sound:",
						"Transp:",
					}
					local layer = salf.layers:seek(salf.current_zmap)
					local name,modx,mody,colision,sound,transp = layer:getData()
					local ta = {name,modx,mody,colision,sound,transp} 
					local function colorBool(item,b)
						if item == true then 
							b.color = colors.green
						else
							b.color = colors.red
						end 
					end 
					
					local txt = {}
					for i,v in ipairs(text) do 
						text[i] = v.."\n"
						local h = ui:getFont():getHeight()
						if type(ta[i]) == "boolean" then 
							local b = ui:addButton(frame2,frame2.width - 22,20 +  h*(i-1),20,h)
							local function func() end 
							if string.find(v,"Colision") then 
								func = function()
									if ta[4] then ta[4] = false else ta[4] = true end  
									if ta[5] then 
										ta[5] = false 
									end 
								end 
							elseif string.find(v,"Sound") then 
								func = function()
									if ta[5] then ta[5] = false else ta[5] = true end 
									if ta[4] then 
										ta[4] = false
									end 
								end 
							end 
							function b.OnClick() 
								func()
							end 
							function b.OnUpdate() 	
								colorBool(ta[i],b)
							end 
						else 
							local text = ui:addTextinput(frame2,frame2.width/2,20 +  h*(i-1),frame2.width/2,h,tostring(ta[i]))
							txt[v] = text
							if type(ta[i]) == "number" and not string.find(v,"Mod") then 
								text:setAllowed({"1","2","3","4","5","6","7","8","9","0","."})
							elseif type(ta[i]) == "number" and string.find(v,"Mod") then 
								text:setAllowed({"1","2","3","4","5","6","7","8","9","0",".","n"})
							end 
						end 
					end 
					local w,h = 100,20
					local done = ui:addButton(frame2,frame2.width/2 - w/2,frame2.height - (h + 5),w,h)
					if layer.locked then 
						done:setInactive(true)
					end 
					done:setText("Apply")
					done.OnClick = function()
						local name,modx,mody,colision,sound,transp
						name = txt["Name:"]:getText()
						modx = tostring(txt["Mod X:"]:getText())
						mody = tostring(txt["Mod Y:"]:getText())
						colision = ta[4]
						sound = ta[5]
						transp = txt["Transp:"]:getText()
						layer:setData(name,modx,mody,colision,sound,transp)
					end 
					frame2.OnDraw = function()
						love.graphics.print(table.concat(text),frame2.x - frame2.width/2,frame2.y - frame2.height/2 + 20)
					end 
				end 
			end 
			local items = {}
			local list
			function layers.OnRefresh()
				if Props then Props:remove() end
				local mi,ma = salf.layers:getRange()
				local font = ui:getFont()
				local tw = 0 
				for i = mi,ma do 
					local layer = salf.layers:seek(i)
					if layer then 
						local t = font:getWidth(layer.actual_name or "")
						if tw < t then tw = t end 
					end 
				end 
				local w,h =  (tw or font:getWidth("Layer: ".."200".." (current)")) + 40,font:getHeight() + 4
				for i,v in pairs(items) do 
					items[i] = nil
				end 
				if frame then 
					if list then 
						list:remove()
					end 
					
					for i = mi,ma do 
						local layer = salf.layers:seek(i)
						if layer then 
							local a = {}
							local w,h = 200-5,font:getHeight() 
							a.w = w
							a.h = h
							a.func = function(b)
								b.Layer = layer
								b.OnClick = function()
									salf.current_zmap = b.Layer.zmap

									if Props then Props:remove() end
								end
								b.OnDraw = function()
									if b.Layer.sound then 
										b.color = {0,255,0}
									elseif b.Layer.colision then 
										local r,g,bx = unpack(colors.blue)
										b.color = {r,g,bx}
									else 	
										b.color = {255,255,255}
									end 
									if layer.active then 
										for i,v in ipairs(b.color) do 
											b.color[i] = v/2 
										end 
									end 
								end 
								b.OnUpdate = function()
									b.text = b.Layer.actual_name
								end 
							end 
							table.insert(items,a)
						end 
					end 
					frame:setSize(200,185)
					list = salf.ui:addList(frame,2,25,200-4,130,items)
					if salf.LayerList.list then 
						list.pos = salf.LayerList.list.pos 
					end 
					list.goMin = true 
					salf.LayerList.list = list 
				end 
			end 
		end 
		layers.OnRefresh()
	end 
	salf.LayerList = layers
	salf.refreshLayers = function()
		if salf.LayerList and salf.LayerList.OnRefresh then salf.LayerList.OnRefresh() end
	end 
	x,y = width - w - 45,salf.height - (w*3 + 10)
	local shape = collider:addCircle(x,y,w)
	local images = ui:addButton(nil,x,y,w,nil,"IM",shape)
	images.dontDrawB = true 
	images.OnUpdate = function()
		local x,y = images.colision:bbox()
		local q = salf.icons[2][5]
		salf.iconBatch:add(q,x,y)
	end
	images.color = colors.gray
	images.OnClick = function()
		salf:pickImage(function(sheet,quad) 
			local imgman = salf.imageAdmin
			local i = imgman:addImage(sheet,quad,1,1)
			salf.creatingItem = i
			i:update(love.timer.getDelta())
			salf.mapAdmin.parseSingle(salf,i:getColision(),love.timer.getDelta())
			salf.activeClamp = i 
		end)
	end 	
	return self
end 
return dock
