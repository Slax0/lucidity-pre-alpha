local edit = {}
local defaultFont = love.graphics.newFont(14)
local ui = require "menu.ui"
local menu = require("menu.init")
local cam = require "hump.camera"
local _PACKAGE = getPackage(...)

local Dock = require (_PACKAGE.."/Edock")
local dropDown = require (_PACKAGE.. "/dropDown")
local sa = require (_PACKAGE.. "/shapes")
local mapAdmin = require (_PACKAGE.. "/mapAdmin")
local layers = require (_PACKAGE.. "/layer")
local t = require(_PACKAGE.."/textures")
local imgMan = require(_PACKAGE.."/images")
local moduleMan = require(_PACKAGE.."/moduleAdmin")
local soundMan = require(_PACKAGE.."/soundAdmin")
local triggers = require(_PACKAGE.."/triggerAdmin")
local rand = require(_PACKAGE.."/random")
local scene_edtior = require(_PACKAGE.."/scene_editor")
local textmanager = require(_PACKAGE.."/textmanager")
local selection_manager = require(_PACKAGE.."/slection")

local image_ext = {
	"png",
	"jpg",
	"bmp",
	"tga",
}
function edit:enter(prev,stack) -- stack: {map}
	self.quickSave = 1
	self._G = {}
	self.OnLoaded = {}
	basicFrameColor = {125,125,125,195}
	basicButtonColor = {55,55,55,155}
	_currentGamestate = self
	local textures,textureManager,makeTexture = t[1],t[2],t[3]
	local stack = stack or {}
	self.map = stack.map or current_map
	self.mode = "game"
	self.random = rand 
	self._textureMapper = textures
	self.tweenGroup = Flux.group()
	self.width,self.height = love.graphics.getDimensions()
	
	local text,func = {},{}
	self.menu = menu:init(text,func)
	self.old_gameMode = self.gameMode
	self.menu.OnRelease = function()
		self.gameMode = nil
	end 
	
	
	local shapeCol = function(dt,a,b)
		self:shapeCol(dt,a,b)
		mapAdmin.shapeCol(self,self.UiShapes:getmxb(),dt,a,b)
	end 
	local shapeStop = function(dt,a,b)
		self:shapeStop(dt,a,b)
		mapAdmin.shapeStop(self,self.UiShapes:getmxb(),dt,a,b)
	end 
	
	self.shapeCollider = HC(200,shapeCol,shapeStop)
	self.iconAtlas = love.graphics.newImage(_PACKAGE.."/dockIcons.png")
	self.iconBatch = love.graphics.newSpriteBatch(self.iconAtlas,10000,"stream") 
	self.icons = {{},{}} 
	local ic = self.iconAtlas
	for i=1,8 do
		local fw,fh = 24,19
		local f = love.graphics.newQuad(fw*(i-1),0,fw,fh,ic:getWidth(),ic:getHeight())
		table.insert(self.icons[1],f)
	end 
	for i=1,6 do 
		local fw,fh = 30,30
		local n = 0 
		local f = love.graphics.newQuad(i + (fw*(i-1)),19,fw,fh,ic:getWidth(),ic:getHeight())
		table.insert(self.icons[2],f)
	end 
	self.ui = ui:init()
	self.UiShapes = ui:init(self.shapeCollider)
	self.UiShapes:setTileIncrement(64)
	
	self.realView = false
	
	self.xRange = 0
	self.yRange = 0 
	self.x = 0
	self.y = 0 
	self.zmap = self.UiShapes.zmap 
	
	self.layers = layers:new(self)
	self.layers:addLayer("Layer 1",1,0,0,true,false)
	
	self.current_zmap = self.zmap
	
	self.cam = cam(self.width/2,self.height/2)
	self.scale = 1

	self.tweens = {lmClick = {radius = 0},rmClick = {radius = 0}}
	
	self.triggerAdmin = triggers:new(self,self.UiShapes)
	
	self.textureManager = textureManager:new(self) 
	self.shapeAdmin = sa:new(self,self.shapeCollider,self.UiShapes,self.ui)
	self.moduleAdmin = moduleMan:new(self,self.shapeCollider,self.UiShapes)
	self.textAdmin = textmanager:new(self,self.shapeCollider,self.ui)
	
	local a = self.moduleAdmin:getModulesFrom("resources/")
		
		
	self.tool = "none"
	self.makingT = nil
	

	self.compiledStats = {}
	self.drawTools = {
		ruler = {true, function()
				local cx,cy = -self.x,-self.y
				local offsetY = self.OffsetX
				local scake = 0
					if math.floor(self.scale) == 0 then
						scake = math.floor((1/self.scale)*100)
					else
						scake = math.floor((1/self.scale)*100)
					end
					local scule = 1
					if scake % 10 == 0 then
						valuesz = scake
						if valuesz <= 100 then
							valuesz = valuesz/2
						end
					end
				love.graphics.setNewFont(9)
				love.graphics.setColor(65,65,65,100)
				love.graphics.rectangle("fill",0,offsetY,20,love.graphics.getHeight())
				love.graphics.rectangle("fill",0,offsetY,love.graphics.getWidth(),15)
				love.graphics.setColor(255,255,255)
				
				for v = math.floor(cy/self.scale),(math.floor(cy/self.scale)) + math.floor(love.graphics.getHeight()/self.scale) do
					if v % (valuesz or 100) == 0 then
						if v == 0 then
							love.graphics.setColor(255,0,0)
							love.graphics.line(0,self.y + v*self.scale,self.width,self.y + v*self.scale)
						end
						love.graphics.setColor(255,255,255)
						love.graphics.rectangle("fill",0,self.y + v*self.scale,22,1)
						-- love.graphics.line(0,self.y + v*self.scale,20,self.y + v*self.scale)
						love.graphics.print(""..v,10,(self.y + v*self.scale) + 6,1.57)				
					end
				end
				
				for v = math.floor(cx/self.scale),math.floor(cx/self.scale) + math.floor(love.graphics.getWidth()/self.scale) do
					if v % (valuesz or 100) == 0 then
						if v == 0 then
							love.graphics.setColor(0,255,0)
							love.graphics.line(self.x + v*self.scale,offsetY,self.x + v*self.scale,self.height)
						end
						love.graphics.setColor(255,255,255)
						love.graphics.line(self.x + v*self.scale,offsetY,self.x + v*self.scale,offsetY + 20)
						love.graphics.print(""..v,(self.x) + v*self.scale,offsetY,1.57)
					end
				end
				love.graphics.setColor(170,170,170)
				love.graphics.rectangle("fill",0,offsetY,20,20)
				love.graphics.setColor(20,20,20)
				love.graphics.print("Px",0,offsetY)
				love.graphics.setNewFont(12)
			
				return offsetY
		end},
		grid = {true,
			function()
				if self.scale > 0.3 then 
				local cx,cy = -self.x,-self.y
				local offsetY = 0
					local ox = math.floor(cx/self.scale)
					local oy = math.floor(cy/self.scale)
					for x = ox,ox + math.floor(self.width/self.scale) do 
						if x % 64 == 0 then 
							love.graphics.rectangle("fill",self.x + x*self.scale,1,1,self.height)
						end
					end 
					
					for y = oy,oy + math.floor(self.height/self.scale) do
						if y % 64 == 0 then 
							love.graphics.rectangle("fill",1,self.y + y*self.scale,self.width,1)
						end 
					end 
				end 
			end 
			},
		stats = {true,
			function()
				local total = 0
				for i,v in pairs(self.ActiveShapes) do
					if v.subUserData then 
						local item = v.subUserData
						if item == "rectangle" then 
							total = total + 1
							table.insert(self.stats.rectangles,v.source)
						elseif item == "circle" then 
							total = total + 1
							table.insert(self.stats.circles,v.source)
						elseif item == "polygon" then 
							total = total + 1
							table.insert(self.stats.polygons,v.source)
						elseif item == "point" then 
							total = total + 1 
							table.insert(self.stats.points,v.source)
						end 
					end 
				end
				for i,v in ipairs(self.compiledStats) do 
					self.compiledStats[i] = v.." \n"
				end 
			end,
			function(x,y)
				love.graphics.print(table.concat(self.compiledStats),20,50)
			end 
		}
	}
	self.ActiveShapes = {}
	
	self.dock = Dock:new(self,self.ui,Collider)
	self.OffsetX = 10
	self.dropDown = dropDown:new(self,self.ui,function(on)
		if not on then 
			self.OffsetX = 40
		else 
			self.OffsetX = 10
		end 
	end)
	self.mainCanvas = love.graphics.newCanvas(width,height)
	
	
	self.timer = Timer
	self.ui_keyControler = keyControl:new("ui") 
	self.H_keyControler = keyControl:newHidden(hiden_keys)
	
	self.textureMapper = textures
	self.imageAdmin = imgMan:new(self,self.shapeCollider,self.phyWorld)
	self.soundAdmin = soundMan:new(self)
	
	-- local bool,err = pcall(self.load,self,current_map)
	-- if bool then self:load(current_map) else print("Failed to load:  "..tostring(current_map).."\n returned Error:\n "..err) end 
	
	self.HasFocus = true
	local w,h = love.graphics.getDimensions()
	self.CameraBox = self.shapeCollider:addRectangle(self.x -w/2,self.y - h/2,w,h)
	eCurrentMap = self.map 
	if love.filesystem.exists(self.map) then 
		self:load(self.map)
	else 
		self.layers:addLayer("Layer 1",1,0,0,true)
	end 
	self.mapAdmin = mapAdmin
	self.rulerCanvas = love.graphics.newCanvas(love.graphics.getDimensions())
	self.x = stack.x or w/2 
	self.y = stack.y or h/2
	self.ruler_on = true 
	love.graphics.setDefaultFilter("nearest","nearest")
end 
function edit:renderRuler()
	if self.x ~= self.oldx or self.y ~= self.oldy or self.ruler_on ~= self.old_ruler_on then 
	self.rulerCanvas:clear()
		self.old_ruler_on = self.ruler_on
		self.oldy = self.y 
		self.oldx = self.x 
		self.rulerCanvas:renderTo(function()
			local r = self.drawTools.ruler
			if r[1] then  
				local of = r[2]()
				setColor(225,255,255)
				love.graphics.rectangle("fill",0,0,self.width,of) 
			end
		end)
	end 
end
function edit:worldPos(x,y)
	local x = (x-self.x)/self.scale
	local y = (y-self.y)/self.scale
	return x,y
end
function edit:camPos(x,y)
	local x = x*self.scale + self.x
	local y = y*self.scale + self.y
	return x,y 
end 
function edit:getMousePos()
	local x,y = love.mouse.getPosition()
	return self:worldPos(x,y)
end
function edit:parse()
	local a = mapAdmin.parse(self.shapeCollider._active_shapes,self.shapeCollider._passive_shapes)
	return a
end 
function edit:updateSpritesheets()
	if self.update_spritesheet then 
		self.update_spritesheet()
	end 
end 
function edit:update(dt)
	local c
	love.graphics.setFont(defaultFont)
	if self.quickSave == 0 then 
		sav_tween.active = true
	else 
		c = true 
	end 
	if self.quickSave >= 0 then 
		self.quickSave = self.quickSave  + 2*dt 
	end 
	if self.quickSave > 200 then 
		self:save()
		self.quickSave = 0 
	end 
	sav_tween:update(self:getTween(),c,dt)
	eCurrentMap = self.map 
	Timer.update(dt)
	self.iconBatch:clear()	

	local cx,cy = self.x,-self.y
	local x,y = love.mouse.getPosition()
	self.mx,self.my = self:worldPos(x,y)
	self.cmx = x 
	self.cmy = y
	
	self.cam:lookAt(self.x,self.y)
	self.cam:zoomTo(self.scale)
	if self.skybox then self.skybox.update(dt) end 
	
	self.ui_keyControler:attach(dt)
	self.H_keyControler:attach(dt)
	
	if self.rotatingShape then self.rotatingShape() end
	if self.HasFocus then 
		local ctrlDown = love.keyboard.isDown("lctrl") or love.keyboard.isDown("rctrl")
		self.ctrlDown = ctrlDown
		if not ctrlDown and not self.ui.textInputObject and not self.BlockCamera then 
			self:navigation(self.ui_keyControler,self.H_keyControler,dt)
		elseif self.ui.textInputObject then 
			self.ui:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
			self.ui:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
		end 
	end 
	self.x,self.y = self.cam.x,self.cam.y
	self.zmap = self.UiShapes.zmap
	self.all_active = {}
	
	self.layers:update()
	self.ui:update(dt)
	self.UiShapes:setMousePos(self.mx,self.my)
	self.UiShapes:update(dt)
	
	Collider:update(dt)
	self.tweenGroup:update(dt)
	
	
	local width = self.width/self.scale
	local height = self.height/self.scale
	if self.scale ~= self.OldScale or not self.CameraBox then 
		self:remakeCamera()
		self.OldScale  = self.scale 
	end
	self.CameraBox:moveTo(-(self.x - self.width/2)/self.scale,-(self.y - self.height/2)/self.scale)	
	self.all_active = {} 
	self.ActiveShapes = {}
	if self.phyWorld and self.phyWorld:isDestroyed() then return end 
	mapAdmin.parseHead(self)
	for shape in pairs(self.CameraBox:neighbors()) do
		if shape:collidesWith(self.CameraBox) then 
			mapAdmin.parseSingle(self,shape,dt)
		end
	end
	mapAdmin.parseTail(self,dt)
	self:render()
	
	self.shapeCollider:update(dt)
	
	
	if self.drawTools.stats[1] then self.drawTools.stats[2]() end
	
	
	self.H_keyControler:detach(dt)
	self.ui_keyControler:detach(dt)
	
	self.soundAdmin:update(dt)
	
	if not self.Lock and self.HasFocus then 
		local range = 20
		local factor = 0.9
		local x,y = love.mouse.getPosition()
		if x < range then
			if self.xRange < 0 then 
				self.xRange = 0 
			else 
				self.xRange = self.xRange + factor*dt 
			end 
			self.x = self.x + self.xRange
		elseif x > range and x > self.width - range then 
			if self.xRange > 0 then 
				self.xRange = 0 
			else 
				self.xRange = self.xRange - factor*dt 
			end 
		else 
			self.xRange = 0 
		end 
		if y < range then
			if self.yRange < 0 then 
				self.yRange = 0 
			else 
				self.yRange = self.yRange + factor*dt 
			end 
			self.y = self.y + self.yRange 
		elseif y > range and y > self.height - range then 
			if self.yRange > 0 then 
				self.yRange = 0 
			else 
				self.yRange = self.yRange - factor*dt 
			end
		else 
			self.yRange = 0 
		end 
		self.x = self.x + self.xRange
		self.y = self.y + self.yRange 
	end 
	if self.ui.activeObject then 
		self.aTarget = nil
	end 
	mapAdmin.updateCol(self,self.UiShapes:getmxb())
	if self.gameMode == "menu" then 
		self.menu:update(dt)
	else 
		self.old_gameMode = self.gameMode
	end 
end
function edit:render()
	local lg = love.graphics 
	self.mainCanvas:clear()
	self:renderRuler()
	self.mainCanvas:renderTo(function()

		
		lg.push()
		love.graphics.translate(self.x,self.y)
		love.graphics.scale(self.scale)
		local c = self.ambient_color or {20,50,90,255}
		setColor(c)
		if self.skybox then self.skybox.draw(-self.x/self.scale,-self.y/self.scale) end 
		setColor()
		if keys.ui[3].f() or alt_keys.ui[3].f() then
			local x = 0
			local y = 0
			if self.mousepressedX ~= nil and self.mousepressedY ~= nil then
				x,y = self.mousepressedAX,self.mousepressedAY
			end
			local value = 0
			local cx,cy = self.mx,self.my
			local valuex = math.pow(math.abs(x - cx) ,2)
			local valuey = math.pow(math.abs(y - cy) ,2)
			value = math.sqrt(valuex + valuey) 
			love.graphics.setColor(255,255,255)
			self.changeInPos = value
		end
		
		mapAdmin.draw_visible(self,self.zmap,self.current_zmap)
		
		if self.makingT and self.makingT == "line" then 
			self.target:draw()
		end 
		
		if not self.ui.activeObject then 
			local radi = self.tweens.lmClick.radius
			local radi2 = self.tweens.rmClick.radius
			if math.floor(radi) ~= 0 then 
				local modx,mody = self:getMod()
				local x,y = self.mousepressedAX,self.mousepressedAY
				setColor(colors.green)
				love.graphics.circle("line",x,y,radi)
				setColor(colors.white)
			end
			if math.floor(radi2) ~= 0 then 
				local modx,mody = self:getMod()
				local x,y = self.mousepressedAX,self.mousepressedAY
				
				setColor(colors.red)
				local w = radi2*2
				love.graphics.rectangle("line",x - w/2,y - w/2,w,w)
				setColor(colors.white)
			end 
		end 
		if self.aTarget then
			local oldw = love.graphics.getLineWidth()
			love.graphics.setLineWidth(2/self.scale)
			setColor(colors.red)
			self.aTarget:getColision():draw("line")
			setColor(colors.white)
			love.graphics.setLineWidth(oldw)
		end 
		lg.pop()
	end)
end 
function edit:getMod()
	return self.layers:seek(self.current_zmap):getMod()	
end 
function edit:draw()
	if self.gameMode ~= "menu" then 
		local lg = love.graphics 
		setColor(self.ambient_color or colors.ambient)
		lg.rectangle("fill",0,0,self.width,self.height)
		setColor(colors.white)
		lg.draw(self.mainCanvas)
		
		setColor(colors.white)
		
		if self.drawTools.grid[1] then self.drawTools.grid[2]() end
		if self.drawTools.stats[1] then self.drawTools.stats[3]() end
		
		local offsetY = self.OffsetX
		love.graphics.draw(self.rulerCanvas)
		
		love.graphics.setColor(255,0,0)
		love.graphics.line(0,love.mouse.getY(),30,love.mouse.getY())
		
		love.graphics.setColor(0,255,0)
		love.graphics.line(love.mouse.getX(),offsetY,love.mouse.getX(),offsetY + 30)
		
		self.ui:draw()
		love.graphics.draw(self.iconBatch)
		
		
		local txt = math.floor((100/self.scale)).." %"
		love.graphics.print(txt,self.width - (font:getWidth(txt)*2),self.height - (font:getHeight()*2))
		
		if not self.ui.activeObject and self.changeInPos and (keys.ui[3].f() or alt_keys.ui[3].f()) then 
			local valuex2 = self.changeInPos
			love.graphics.setColor(120,230,150)
			
			local x,y = self:camPos(self.mousepressedAX or 0,self.mousepressedAY or 0)
			local x2,y2 = self:camPos(self.mx,self.my)
			
			
			love.graphics.setColor(120,230,150)
			love.graphics.line(x,y,x2,y2)
			love.graphics.print(""..math.floor(valuex2),(x + x2)/2,(y + y2)/2)
			love.graphics.setColor(255,255,255)
		end 
		local x,y = self:camPos(0,0)
		if misc.showFPS then 
			showFPS()
		end 
		if self.quickSave >= 0 then 
			love.graphics.print("QuickSave: "..math.floor(200 - self.quickSave),25,self.height - 20)
		end 
		if sav_tween.active then 
			sav_tween:draw()
		end 
	else 
		self.menu:draw()
	end 
	drawRelease()
end 
function edit:getLeftClick()
	return keys.ui[3].f(), alt_keys.ui[3].f()
end 
function edit:remakeCamera()
	local width = self.width/self.scale
	local height = self.height/self.scale
	local s = self.scale
	local w,h = width,height
	self.shapeCollider:remove(self.CameraBox)
	self.CameraBox = self.shapeCollider:addRectangle(-(self.x - self.width/2)/self.scale,-(self.y - self.height/2)/self.scale,w,h)
end 
function edit:navigation(keyss,hKeys,dt)
	if not self.tool then 
		self.cursor = nil 
	end 
	local uiKeys,alt_uiKeys = keyss:unpack()
	local hKeys = hKeys:unpack()
	local function check(var,key,alt,func)	
		local b = key or alt
		local func = func or var.OnClick 
		if not var.inactive and not var.permaInactive and b then 
			func(var)
		end 
	end 
	local lm = uiKeys[3]
	local rm = uiKeys[4]
	local modx,mody = self:getMod()
	modx = -modx 
	local x,y = love.mouse.getPosition()
	x = x
	y = y
	if self.makingT and self.makingT == "line" then 
		self.target:update(hKeys[8],lm)
	end 

	if lm or rm then 
		self.mousepressedX, self.mousepressedY = self:worldPos(x + modx,y + mody)
		self.mousepressedAX, self.mousepressedAY = self:worldPos(x,y)
		self.cMousepressedX = x
		self.cMousepressedY = y
	end 
	if lm then 
		Timer.tween(0.01,self.tweens.lmClick,{radius = 10},nil,function() Timer.tween(0.2,self.tweens.lmClick,{radius = 0 }) end)
		if not self.ui.activeObject then 
			if not self.ui.activeObject and self.tool and self.makingT ~= "line" then 
				local t = self.tool
				local px,py = self.mousepressedX,self.mousepressedY
				if t == "rect" then 
					self.targetClamp = true
					local rect = self.shapeAdmin:newRectangle("fill",px,py)
					rect:makeColision()
					rect:setPointOfCreation(px,py)
					self.target = rect
					rect.zmap = self.current_zmap
					self.makingT = "rect"
				elseif t == "poly" then
					self.targetClamp = true
					local rect = self.shapeAdmin:newLine(nil,px,py)
					rect.OnMake = function(s)
						local points,x,y = s:returnPoints()
						local polygon = self.shapeAdmin:newPolygon("fill",x,y,points)
						if polygon then
							self.tool = nil
							-- polygon:makeColision()
							polygon.zmap = self.current_zmap
							self.makingT = false
							s:remove()
							self.target = nil
						end
					end 
					rect.zmap = self.current_zmap
					rect:makeColision()
					self.target = rect
					self.makingT = "line"
				elseif t == "circle"  then
					self.targetClamp = true
					local circle = self.shapeAdmin:newCircle("fill",px,py)
					circle.zmap = self.current_zmap
					circle:makeColision()
					self.target = circle
					self.makingT = "circle"
				elseif t == "random" then 
					self.makingT = "selection"
					self.target = selection_manager:new(self)
				end
			elseif self.target and not self.target.moving and not  self.target.make then
				if self.makingT == "line" and not self.target.moving then 
					self.target:addPoint(self.mousepressedX,self.mousepressedY)
				end
			end
		end 
	end	
	if rm then 
		Timer.tween(0.01,self.tweens.rmClick,{radius = 10},nil,function() Timer.tween(0.2,self.tweens.rmClick,{radius = 0 }) end)
		if self.tool then 
			if self.makingT == "line" then
				self.target:remove()
			elseif self.makingT == "selection" then 
				self.target:removeFocus()
				self.target = nil 
			end
			self.tool = false
			self.makingT = false
			self.targetClamp = false
		end 
	end 
	
	
	local lmDown,rmDown = keys.ui[3].f() or alt_keys.ui[3].f() , keys.ui[4].f() or alt_keys.ui[4].f()
	if lmDown then 
		local modx,mody = self:getMod()
		modx = -modx 
		if self.makingT then
			local x = self.mx + modx
			local y = self.my 
			if self.makingT == "rect" and self.target then
				local w,h = incrementPos(x) - incrementPos(self.mousepressedX),incrementPos(y) - incrementPos(self.mousepressedY)
				w,h = w,h
				if math.abs(w) < 5 then 
					w = 32	
				end
				if math.abs(h) < 5 then 
					h = 32
				end
				self.toFinialize = self.target
				w = w
				self.target:setWidth(w)
				self.target:setHeight(h)
				self.targetClamp = true
			elseif self.makingT == "circle" and self.target then
				local x1,y1 = x,y
				local x2,y2 = self.mousepressedX,self.mousepressedY
				local r = math.sqrt(math.abs(x1 - x2)^2 + math.abs(y1 - y2)^2)
				r = incrementPos(r) 
				self.target:setRadius(r)
			end
		end
	elseif self.toFinialize then 
		self.target.pocx = nil 
		self.target.pocy = nil 
		self.toFinialize = nil 
	end 
	if self.makingT and self.target then 
		if self.makingT == "line" then
			local x = (love.mouse.getX() - self.x)/self.scale
			local y = (love.mouse.getY() - self.y)/self.scale
			self.target:addTempPoint(x,y)
		elseif self.makingT == "generic" then
			if self.target.setPos then
				self.target:setPos(self.x3 + modx,self.y3 + modx)
			else
				print("No 'setPos' function found!")
			end
		end
	end 

	self.ui:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
	self.ui:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
	
	self.UiShapes:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
	self.UiShapes:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
	
	local lm,alt_lm = keys.ui[3].f(), alt_keys.ui[3].f()
	local rm,alt_rm = keys.ui[4].f(), alt_keys.ui[4].f()
	mapAdmin.navi(self,self.UiShapes:getmxb(),lm,alt_lm,rm,alt_rm,self.ui.activeObject)
	
	local left 	= hKeys[1] 
	local right = hKeys[2] 
	local down 	= hKeys[3] 
	local up	= hKeys[4]
	
	local toOrigin = hKeys[5]
	
	local undo = hKeys[6]
	local redo = hKeys[7]
	if hiden_keys[4].f()  then
		self.y = self.y + 300*dt
	elseif hiden_keys[3].f()  then
		self.y = self.y - 300*dt
	end
	if hiden_keys[1].f() then
		self.x = self.x + 300*dt
	elseif hiden_keys[2].f()  then
		self.x = self.x - 300*dt		
	end	
	
	if toOrigin then 
		self.x = self.width/2
		self.y = self.height/2
		self.scale = 1
	end 
	if hKeys[9] then 
		self.scale = 1
	end 
	self.cam:lookAt(self.x,self.y)
end 
function edit:save(name)
	local name = name or self.map
	print("Saved as: "..name)
	mapAdmin.save(self,name,mapAdmin.parse(self.shapeCollider._active_shapes,self.shapeCollider._passive_shapes),true)
end 
function edit:run()
	mapAdmin.clear(self.shapeCollider,self)
	self.ui:clear()
	if Collider then Collider:clear() end 
	if self.mode == "battle" then 
		self.scene_admin:play()
	else 
		Gamestate.switch(PlayMap,{map = self.map,Editor = {x = self.x,y = self.y}})
	end 
end 
function edit:leave()
	self.gameMode = nil 
	mapAdmin.clear(self.shapeCollider,self)
	self.ui:clear()
	if Collider then Collider:clear() end 
end 
function edit:load(name)	
	mapAdmin.clear(self.shapeCollider,self)
	mapAdmin.load(self,name)
	if err then 
		print("Failed to load map:"..err)
		self.current_map = "new"
		self.map = "new"
	end 
end 
function edit:refreshImageList()
	if self.pickImageFrame then 
		self.pickImageFrame:remove()
		self.pickImageFrame = nil
	end 
end 
function edit:getMapDir(tail)
	local a,b = string.find(self.map,"/")
	local tail = string.sub(self.map,1,b)
	local baseDir = love.filesystem.getSaveDirectory().."/"..tail
	if self.map then 
		if string.find(self.map,"maps/") then 
			baseDir = baseDir.."/"..string.gsub(self.map,tail,"")
		else 
			baseDir = baseDir.."/"..self.map
		end 
	end 
	return baseDir
end 
function edit:pickImage(func)	
 	if self.pickImageFrame then 
		self.pickImageFrame:remove()
		self.pickImageFrame = nil 
	end 
		local ui = self.ui
		local keyword
		local ActiveSheet 
		local w,h = 250,self.height - 150
		local frame = ui:addFrame("Images:",self.width/2,self.height/2,w,h)
		frame.isWindow = true 
		self.pickImageFrame = frame 
		frame:setHeader()
		local th = ui:getFont():getHeight()
		local tw = ui:getFont():getWidth("Src: ")
		frame.dragEnabled = true 
			local close = ui:addButton(frame,frame.width - 20,0,20,20)
			close.text = "X"
			close.zmap = frame.zmap  + 3
			close.OnClick = function()
				frame:remove()
				frame = nil
			end 
		frame.color = basicFrameColor

		local font = ui:getFont()
		local text = ui:addTextinput(frame,4,22,w-10,font:getHeight())
		text:setText("Search")
		
		local list = ui:addList(frame,2,20 + text.mainFrame.height +5,frame.width - 4,frame.height - 22 - 55)
		local back = ui:addButton(frame,2,frame.height - (2 + 25),frame.width -4,25)
		
		local selectSource
		local drawQuads
		local listImages
		local topLayer
		
		function selectSource(s)
			list:clear()
			local sheets = self.textureManager:prLoad(s)
			for i,v in ipairs(sheets) do 
				list:addChoice(v.name,function()
					list._LastPath = listImages
					local sheet = v
					ActiveSheet = sheet
					drawQuads(sheet,true)
				end)
			end 
		end 
		function drawQuads(sheet,outsider)
			list:clear()
			local img = sheet:getImage()
			for i,v in ipairs(sheet:getQuads()) do 
				if not keyword or string.find(v.name:lower(),keyword:lower()) then 
					local quad = v.actual 
					local w,h = list.mainFrame.width - list.gap,200	
					local b = ui:addButton(nil,0,0,w,h)
					local a = b
					b.dontDrawB = true 
					b.dontDrawC = true 
					
					local _,_,iw,ih = quad:getViewport() 
					local sx,sy =  a.width/iw,200/ih
					if type(v.actual) ~= "table" then
						b.OnDraw = function()
							love.graphics.draw(img,quad,a.x - a.width/2,a.y - a.height/2,0,sx,sy)
						end 
					else 
						local inst = quad:addInstance()
						b.OnDraw = function()
							inst:draw(a.x - a.width/2,a.y - a.height/2,0,sx,sy)
						end 
						b.OnUpdate = function(dt)
							inst:update(dt)
						end 
					end 
					b.OnClick = function()
						if func then func(sheet,v) end
						if outsider then self.textureManager:addSheet(sheet) end 
					end 
					list:addItem(b)
				end 
			end
		end 
		function listImages(c)
			list:clear() 
			list._LastPath = topLayer
			local a = self.textureManager:unpack()
			for i,v in ipairs(a) do 
				list:addChoice(v.name,function()
					list._LastPath = listImages
					local sheet = self.textureManager:seek(v.name)
					ActiveSheet = sheet
					drawQuads(sheet)
				end)
			end 
		end 
		function topLayer()
			list:clear()
			list._LastPath = topLayer
			list:addChoice("resources/",function()
				selectSource("resources/")
			end)
			list:addChoice(eCurrentMap,function()
				listImages()
			end)
		end 
		topLayer()
		text.OnChangeText = function(text)
			keyword = text
			if ActiveSheet then drawQuads(ActiveSheet) end 
		end
		back:setText("Back")
		back.OnClick = function()
			list._LastPath()
		end
end 
function edit:mousepressed(x,y,button)
	if self.HasFocus then
		self.ui:mousepressedList(x,y,button)
		if not self.ui.ActiveList and not self.BlockCamera then 
			if button == "wu" then
				local scale = self.scale - math.floor((0.05*self.scale)*1000)/1000
				if scale > 0 and scale > 0.1 then
					local lastzoom = self.scale
					local mouse_x = x - self.x
					local mouse_y = y - self.y
					self.scale = scale
					local newx = mouse_x * (self.scale/lastzoom)
					local newy = mouse_y * (self.scale/lastzoom)
					self.x = self.x + (mouse_x-newx)
					self.y = self.y + (mouse_y-newy)
				else
					self.scale = 0.1
				end
			elseif button == "wd" then
				local scale = self.scale + math.floor((0.05*self.scale)*1000)/1000
				local scalex = self.scale
				if scale > 0 and scale < 20 then
					local lastzoom = self.scale
					local mouse_x = x - self.x
					local mouse_y = y - self.y
					self.scale = scale
					local newx = mouse_x * (self.scale/lastzoom)
					local newy = mouse_y * (self.scale/lastzoom)
					self.x = self.x + (mouse_x-newx)
					self.y = self.y + (mouse_y-newy)
				else
					self.scale = 20
				end
			end
		elseif self.BlockCamera then  
			self.BlockCamera(x,y,button)
		end
	end
end 
function edit:keypressed(key,unicode)
	self.ui:keypressed(key,unicode)
	if not self.ui.textInputObject then 
		if key == "delete" and self.aTarget then 
			self.aTarget:remove()
		end 
		if self.ctrlDown then 
			if key == "s" then 
				self.quickSave = 0 
				self:save()
			elseif key == "q" then
				if self.quickSave < 0 then 
					self.quickSave = 0
				else 
					self.quickSave = -1 
				end 
			elseif key == "r" then 
				self:run()
			elseif key == "c" then 
				if self.aTarget and self.aTarget.colision and not self.aTarget.colision._module then 
					self.clipboard =  {t = shallowcopy(self.aTarget),tm = getmetatable(self.aTarget)}
				else 
					self.clipboard = nil
				end
			elseif key == "v" then 	
				if self.clipboard then 
					local x,y = self.mx,self.my
					local mt = self.clipboard.tm
					local nt = shallowcopy(self.clipboard.t)
					nt.colision = nil
					setmetatable(nt,mt)	
					nt.zmap = self.current_zmap
					nt.x = self.mx 
					nt.y = self.my
					if nt.makeColision then nt:makeColision() end
					if nt.OnCopy then nt:OnCopy() end
				end
			end 
		else 
			if key == "v" then 
				if self.realView then 
					self.realView = false 
				else 
					self.realView = true
				end 
			elseif key == "g" then 
				self.toggleGrid()
			elseif key == "r" then 
				self.toggleRuler()
			elseif key == "x" then 
				self.x = 0 
				self.y = 0 
				self.scale = 1
			elseif key == "l" then 
				self.toggleLock()
			end
		end  
		if not self.disableMenu and key == "escape"  and not self.menu.warning then 
			local menu  = self.menu
			if self.gameMode == "menu" then
				self.gameMode = self.old_gameMode
				menu:ends()
				self.menu.options = nil 
				self.menu.volutary = false 
			else
				self.gameMode = "menu"
				menu:start()
				self.menu.volutary = true 
			end
		end 
	end 
end 
function edit:textinput(...)
	self.ui:textinput(...)
end 
function edit:mousereleased(x,y,b)
	if self.ReleaseMouse then 
		self.ReleaseMouse(x,y,b)
	end 
end 
function edit:keyreleassed()
end
function edit:mousefocus(f)
	self.HasFocus = f 
end
function edit:shapeCol(dt,a,b)
	self.UiShapes:Hcol(dt,a,b)
end 
function edit:shapeStop(dt,a,b)
	self.UiShapes:Hstop(dt,a,b)
end 
function edit:Hcol(...)
	self.ui:Hcol(...)
end
function edit:Hstop(...)
	self.ui:Hstop(...)
end 
function edit:startSelection()

end
function edit:pickLayer(func)
	if self._activeFrame then self._activeFrame:remove() end 
	local ui = self.ui
	local la = self.layers
	local mi,ma = la:getRange()
	local font = ui:getFont()
	local tw = 0 
	for i = mi,ma do 
		local layer = la:seek(i)
		if layer then
			local t = font:getWidth(layer.actual_name or "")
			if tw < t then tw = t end 
		end
	end  
	local w,h = tw + 8,185
	local f = ui:addFrame(nil,self.width/2,self.height/2,w,h)
	self._activeFrame = f 
	local list = ui:addList(f,2,2,w - 4,h - 5)
	for i = mi,ma do 
		local layer = la:seek(i)
		if layer then 
			local b = ui:addButton(nil,0,0,w,font:getHeight())
			b:setText(layer.actual_name)
			b.Layer = layer
			b.OnClick = function()
				if func then func(b.Layer.zmap,layer) end 
				f:remove()
			end
			b.OnDraw = function()
				if b.Layer.sound then 
					b.color = {0,255,0}
				elseif b.Layer.colision then 
					local r,g,bx = unpack(colors.blue)
					b.color = {r,g,bx}
				else 	
					b.color = {255,255,255}
				end 
			end 
			b.OnUpdate = function(dt)
				b.text = b.Layer.actual_name
			end 
			list:addItem(b)
		end 
	end 
end 
function edit:pickSound(func)
	local salf = self
	local ui = self.ui
	local snd = salf.soundAdmin 
	if salf.soundFrame then 
		salf.soundFrame:remove()
		salf.soundFrame = nil
	end 
	local frame = ui:addFrame("Pick Sound:",salf.width/2,salf.height/2,200,400)
	self._activeFrame = frame 
	local close = ui:addButton(frame,0,0,20,20)
	close.text = "X"
	close.OnClick = function()
		salf._activeFrame:remove()
		salf._activeFrame = nil 
	end 
	frame.dragEnabled = true 
	frame:setHeader()
	local standH = ui:getFont():getHeight()
	local stdW = 200-4
	local src_list = ui:addList(frame,2,20,200 -4,standH)
	local glob_list = ui:addList(frame,2,20 + (standH) + 2,200-4,frame.height - (20 + (standH) + 4))
	local function listSound(dir,list)
		list:clear()
		local sounds = snd:getFromDir(dir.."/sounds")
		for i,v in ipairs(sounds) do
			local b = ui:addButton(nil,0,0,stdW,standH)
			b:setText(v)
			b.OnClick = function() 
				local name = v
				local dir = dir.."/sounds"
				if func then func(name,dir) end 
			end 
			list:addItem(b)
		end 
	end 
	local src = ui:addButton(nil,0,0,stdW,standH)
	src:setText(salf.map)
	src_list:addItem(src)
	src.OnClick = function()
		listSound(salf.map,glob_list)
	end 
	local src = ui:addButton(nil,0,0,stdW,standH)
	src:setText("resources/")
	src.OnClick = function()
		listSound("resources/",glob_list)
	end 
	src_list:addItem(src)
end 
function edit:getUi()
	return self.ui
end 
function edit:getTween()
	return self.tweenGroup
end
function edit:getTweenGroup()
	return self.tweenGroup
end
function edit:newMap(name)
	local function mapName(map)
		local invalidchars = {
			"\a",
			"\b",	
			"\f",	
			"\n",
			"\r",
			"\t",
			"\v",
			"\\",
			"\"",
			"\'",
			"/" ,
			"*" ,
			"?",
			"<",
			">" ,
			"|",
			":",
		}
		local date = os.date()
		for i,v in ipairs(invalidchars) do 
			date = string.gsub(date,v,"_")
		end
		return map.."_"..date
	end 
	mapAdmin.clear(self.shapeCollider,self) 
	self.map = mapName(name) 
	self.mode = "game" 
	self.ambient_color =  colors.ambient
	self.layers:addLayer("Layer 1",1,0,0,true)
end
function edit:getGameUi()
	return self.UiShapes
end 
return edit 
