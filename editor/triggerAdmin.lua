local triggers = {}
function triggers:new(self,colider,ui,global)
	local s = {
		parent = self,
		colider = colider, 
		ui = ui,
		global = global,
	}
	return setmetatable(s,{__index = triggers})
end 
function triggers:setEnviroment(global)
 	local env = getEnv()
	self.env = env
	for i,v in pairs(global) do 
		env[i] = v 
	end 
	return env
end 
function triggers:getEnviroment()
	local gs = Gamestate.current()
	if not self.env then self:setEnviroment(gs._G) end 
	for i,v in ipairs(gs._G) do 
		self.env[i] = v 
		print("Global: ",v)
	end 
	return self.env
end
function triggers:load(t)
	for i,v in ipairs(self.items) do
		if v.load then 
			v:load()
		end 
	end 
end 

function triggers:getModes()
	return {
		"player-in",
		"player-out",
		"Hplayer-in",
		"Hplayer-out",
		"load",
		"draw",
		"update",
	}
end 
function triggers:exec(fns,var)
	print(fns)
	local fna,err = loadstring(fns)
	if type(fna) == "function" then 
		s = true 
		fn = function(t)
			local a
			local t = t or {} 
			local s,err = pcall(function()
				local env = self:getEnviroment()
				env.self = var
				env.map_directory =  eCurrentMap or self.parent.map
				for i,v in pairs(t) do 
					env[i] = v 
				end 
				a = setfenv(fna,env)()
			end)
			if not s and err then 
				print(err)
			else 
				return a
			end 
		end  
		return fn()
	else 
		print(err)
	end
end 
	local trigger = {}
	function triggers:addTrigger(item)
		local s = {child = item,parent = self}
		setmetatable(s,{__index = trigger})
		return s 
	end
	function trigger:setActiv(mode,fns)
		print(" Setting trigger ")
		local s = false 
		local fn 
		self.TextFunction = fns 
		self.mode = mode 
		
		local fna = loadstring(fns)
		if type(fna) == "function" then 
			s = true 
			fn = function(t)
				local a
				local t = t or {} 
				if type(t) ~= "table" then 
					t = {} 
				end 
				local s,err = pcall(function()
					local env = self.parent:getEnviroment()
					env.self = self.child
					for i,v in pairs(t) do 
						env[i] = v 
					end 
					a = setfenv(fna,env)()
				end)
				if not s and err then 
					print(err)
				else 
					return a
				end 
			end 
		if string.find(mode,"player") and not string.find(mode,"Hplayer") then 
			if string.find(mode,"in") then 
				function self:beginContact(dt,a,b)
					local player,part,other = checkPlayerCol(a,b)
					local d 
					if other.getUserData then 
						d = other:getUserData()
					end 
					if d and d.source == self.child and player then 
						fn(part)
					end 
				end 		
			elseif string.find(mode,"out") then 
				function self:endContact(dt,a,b)
					local player,part,other = checkPlayerCol(a,b)
					local d 
					if other.getUserData then 
						d = other:getUserData()
					end 
					if d and d.source == self.child and player then 
						fn(part)
					end 
				end 
			else 
				print("Invalid mode for 'player':",mode)
			end 
		elseif string.find(mode,"Hplayer") then 
			if string.find(mode,"in") then
				function self:HCol(dt,a,b)
					local player,part,other = checkPlayerCol(a,b)
					local other 
					if (a == self.child.colision or b == self.child.colision) and player then 
						if fn then 
							local a = fn(player,part)
							if not a then 
								fn = nil 
							end 
						end 
					end 
				end 		
			elseif string.find(mode,"out") then 
				function self:HStop(dt,a,b)
					local player,part,other = checkPlayerCol(a,b)
					local other 
					if (a == self.child.colision or b == self.child.colision) and player then 
						if fn then 
							local a = fn(player,part)
							if not a then 
								fn = nil 
							end
						end 
					end 
				end 
			else 
				print("Invalid mode for 'player':",mode)
			end 
		elseif string.find(mode,"mouse") then 
			if string.find(mode,"in") then 
				local expectedShape = (self.parent.ui or Gamestate.current().ui):getmxb()
				function self:HCol(dt,a,b)
					local other 
					if a == expectedShape then 
						other = b 
					elseif b == expectedShape then 
						other = a 
					end 
					print(dt,a,b)
					if other == self.child:getColision() then 
						fn()
					end 
				end 
			elseif string.find(mode,"out") then 
				function self:HStop(dt,a,b)
					local other 
					if a == expectedShape then 
						other = b 
					elseif b == expectedShape then 
						other = a 
					end 
					if other == self.child:getColision() then 
						fn()
					end 
				end 
			else 
				print("Invalid mode for 'mouse': ",mode)
			end 
		elseif string.find(mode,"load") then 
			function self:load()
				fn()
			end 
		elseif string.find(mode,"draw") then 
			function self:draw() 
				fn()
			end 
		elseif string.find(mode,"update") then 
			function self:update(dt)
				fn({dt = dt})
			end 
		elseif string.find(mode,"contact") then 
			if string.find(mode,"begin") then 
				function self:beginContact(dt,a,b)
					fn(dt,a,b)
				end 
			elseif string.find(mode,"end") then 
				function self:endContact(dt,a,b)
					fn(dt,a,b)
				end 
			elseif string.find(mode,"post") then 
				function self:postSolve(dt,a,b)
					fn(dt,a,b)
				end 
			elseif string.find(mode,"pre") then 
				function self:preSolve(dt,a,b)
					fn(dt,a,b)
				end 
			end 
		else
			print("Invalid mode:",mode)
		end 
		else 
			print(loadstring(fns))
		end 
		return s,fn 
	end 
	function trigger:save()
		print("Saving: ",self.mode,self.TextFunction)
		local t = {mode = self.mode,func = self.TextFunction}
		return t 
	end 
	function trigger:load(t)
		local mode = t.mode
		local fn = t.func or "print('broken function')"
		print("--LOADING--")
		local s,fn = self:setActiv(mode,fn)
		if mode == "load" then 
			fn()
		end 
	end 
return triggers