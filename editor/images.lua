local imageAdmin = {}
local function makeShape(self,shape,polygon)
	self.Modx = 0 
	self.Mody = 0 
	
	local phyWorld = self.parent.phyWorld
	if phyWorld and shape then 
		if self.physicsData then 
			self.physicsData.body:destory()
			clearTable(self.physicsData)
		else 
			self.physicsData = {}
		end 
		
		local w,h = findDimensions(self.colision)
		local x,y = self.x or self.dx,self.y or self.dy
		local body
		if polygon then 
			x,y = x,y
		end 		
		if type(shape) == "table" then
			body = love.physics.newBody(phyWorld,x,y,"static")
			local fixtures = {}
			for i,v in ipairs(shape) do
				local fixture = love.physics.newFixture(body,v)
				fixture:setUserData({source = parent,part = "STATIC"})
				table.insert(fixtures,fixture)
			end
			fixtures = fixture
		else
			body = love.physics.newBody(phyWorld,x,y,"static")
			fixture = love.physics.newFixture(body,shape)
			fixture:setUserData({source = parent,part = "STATIC"})
		end
		body:setAngle(self.r or 0)
		self.physicsData.body = body 
		self.physicsData.fixture = fixture or fixtures
		self.physicsData.shape = shape
	else 
		print("No physics world!")
	end
end 
function imageAdmin:new(self,colider,phyWorld)
	local s = {parent = self,phyWorld = phyWorld,colider = colider,images = {},activeItems = {}}
	return setmetatable(s,{__index = imageAdmin})
end 

local image = {}
function imageAdmin:addImage(sheet,quad,x,y,r,sx,sy)
		local sa = {img = sheet:getImage(),x = x,y = y,r = r or 0,sx = sx or 1,sy = sy or 1,parent = self}
		sa.sheet = sheet
		sa.quad = quad
		sa.Modx = 0 
		sa.Mody = 0 
		if type(quad.actual) == "table" then 
			print("Adding instance")
			sa.actual = quad.actual:addInstance()
			print(sa.actual,quad.actual)
		else 
			sa.actual = quad.actual
		end 
		sa.zmap = self.parent.current_zmap
		table.insert(self.images,sa)
		setmetatable(sa,{__index = image})
		sa:makeColision()
	return sa
end 
function image:unpack()
	local a = {
		x = self.x ,
		y = self.y ,
		r = self.r ,
		sx = self.sx, 
		sy = self.sy,
		type = "_image",
		Free = self.Free,
		zmap = self.zmap,
		sheet = self.sheet.name,
		quad = self.quad.name,
	}
	return a
end
function image:mirror()
	self.sx = -self.sx
end 
function image:flip()
	self.sy = -self.sy
end 
function image:setScale(sx,sy)
	local sx = sx or 1
	local sy = sy or sx
	self.sx = sx 
	self.sy = sy 
	self:makeColision()
end
function image:setZmap(zmap)
	self.zmap = zmap 
	print("Setting Zmap: "..zmap)
end
function image:setRotation(r)
	self.r = math.rad(r) or 0
	self.colision:setRotation(math.rad(r))
end 
function image:setMod(modx,mody)
	self.modx = modx
	self.mody = mody
	if self.colision then 
		local w,h = (self.width*self.sx)/2,(self.height*self.sy)/2
		self.topX,self.topY = incrementPos(self.x,self.y,w,h)
		if not self.Free then 
			self.dx = self.topX + (self.modx or 0)  + self.Modx
			self.dy = self.topY + (self.mody or 0)  + self.Mody 
		else
			self.dx = self.x 
			self.dy = self.y
		end 
		self.colision:moveTo(self.dx,self.dy)
	end 
	if self.physicsData then 
		self.physicsData.body:setPosition(self.dx,self.dy)
	end 
end 
function image:makeColision(d)
	local x,y = self.x,self.y
	local _,_,w,h = self.quad.actual:getViewport()
	self.width = w 
	self.height = h
	local r   = self.r
	local sx  = self.sx 
	local sy  = self.sy
	local c = self.parent.colider:addRectangle(x,y,w*sx,h*sy)
	if self.colision then 
		self.parent.colider:remove(self.colision)
	end 
	self.colision = c
	c.editorShape = self
	c.source = self
	c:setRotation(r)
	self.parent.colider:setPassive(self.colision)
	-- local w,h = w/2,h/2 
	-- makeShape(self,{w,h,w,-h,-w,-h,-w,h})
	if d then 
		local width,height = w*sx,h*sy
		local x1,y1 = width/2,height/2
		local x2 = -(width/2)
		local y2 = -(height/2)
		local points = {
			x1,y1,
			x1,y2,
			x2,y2,
			x2,y1,
		}
		local shape = love.physics.newPolygonShape(unpack(points))
		makeShape(self,shape)
	end 
	return c 
end 
function image:update(dt)
	if type(self.actual) == "table" then 
		self.actual:update(dt)
	end 
end
function image:getPos()
	local x,y = self.colision:center()
	x = self.dx or  x 
	y = self.dy or  y
	return x,y
end
function image:getColision()
	return self.colision
end 
function image:rotate(r)
	self.r = self.r + r
end
function image:draw(DEBUG)
	local x,y = self:getPos()
	local r = self.r
	local _,_,w,h = self.quad.actual:getViewport()
	local sx,sy = self.sx,self.sy
	
	if self.drawBefore then self.drawBefore() end 
	
	if type(self.actual) == "table" then 
		self.actual:draw(x,y,r,sx,sy,w/2,h/2)
	else 
		love.graphics.draw(self.img,self.actual,x,y,r,sx,sy,w/2,h/2)
	end 
	if DEBUG then 
		setColor(colors.red)
		self.colision:draw("line")
		if self.physicsData then 
			setColor(colors.green)
			if self.parent.phyWorld then 
				local body = self.physicsData.body 
				local shape = self.physicsData.shape
				if type(shape) == "table" then 
					for i,v in pairs(shape) do 
						love.graphics.polygon("line",body:getWorldPoints(v:getPoints()))
					end 
				elseif shape.getPoints then 
					love.graphics.polygon("line",body:getWorldPoints(shape:getPoints()))
				else 
					local x,y = body:getWorldPoints(shape:getPoint())
					love.graphics.circle("line",x,y, shape:getRadius())
				end
			end 
		end 
	end 
	setColor()
	if self.OnDraw then self.OnDraw() end 
end	
function image:remove()
	local p = self.parent
	p.colider:remove(self.colision)
	if self.physicsData then 
		if not self.physicsData.body:isDestroyed() then 
			self.physicsData.body:destroy()
		end 
	end 
	for i,v in pairs(p.images) do 
		if v == self then 
			table.remove(p.images,i)
		end
	end 
end 
function image:OnMenu(list,ui)
	local a =  ui:addButton(nil,0,0,20,20)
	a:setText("Mirror")
	a.OnClick = function()
		self:mirror()
	end 
	list:addItem(a)
	local a =  ui:addButton(nil,0,0,20,20)
	a:setText("Flip")
	a.OnClick = function()
		self:flip()
	end 
	list:addItem(a)
	local a = ui:addButton(nil,0,0,20,20)
	if self.Free then 
		a:setText("Lock")
		a.OnClick = function()
			self.Free = false 
		end
	else 
		a:setText("Free")
		a.OnClick = function()
			self.Free = true
		end
	end
	list:addItem(a)
	list:addChoice("Set scale",function()
		local frame = ui:addFrame(nil,300,400,200,150)
		local vars = {
			"scale_x","scale_y",
		}
		local items = {
		}
		local fh = ui:getFont():getHeight()
		local gap = 5
		for i,v in ipairs(vars) do 
			local t = ui:addTextinput(frame,95,25 + ((gap + fh)*(i-1)),100,fh)
			t:setAllowed({"1","2","3","4","5","6","7","8","9","0","."})
			t:setText(self[v])
			local text = ui:addText(frame,5,25 + ((gap + fh)*(i-1)),v)
			
			table.insert(items,t)
		end 
		frame.dragEnabled = true 
		frame:addCloseButton(function()
			local sx,sy = tonumber(items[1]:getText()),tonumber(items[2]:getText())
			self:setScale(sx,sy)
			self:makeColision()
		end)
		frame:setHeader("Image scale")
	end)
end 
function imageAdmin:clear()
	for i,v in pairs(self.images) do 
		v:remove()
	end 
end 
function imageAdmin:getItems()
	return self.images
end 
function image:OnCopy()
	print("Copying")
	if type(self.actual) == "table" then 
		self.actual = self.quad.actual:addInstance()
	end 
	table.insert(self.parent.images,self)
end
function imageAdmin:save()
	local t = {}
	for i,v in pairs(self.images) do 
		local a = v:unpack()
		if v.__triggers then 
			a.__triggers = {}
			for i,v in ipairs(v.__triggers) do 
				a.__triggers[i] = v:save()
			end 
		end 
		if v.__name then 
			a.__name = v.__name 
		end 
		table.insert(t,a)
	end 
	return t
end 
function imageAdmin:load(tables,tm,editor)
	for i,v in pairs(tables) do 
		if v.sheet and v.quad then 
			local layer = editor.layers:seek(v.zmap) 
			local sheet = tm:seek(v.sheet) 
			local quad = sheet:seek(v.quad) 
			if quad then 
				local a = self:addImage(sheet,quad,v.x,v.y,v.r,v.sx,v.sy)
				if layer.colision then 
					a:makeColision(true)
				end
				
				a.Free = v.Free
				a.scale_x = v.scale_x or 1 
				a.scale_y = v.scale_y or 1 
				a.zmap = v.zmap
				if v.__triggers then 
					a.__triggers = {} 
					for i,v in ipairs(v.__triggers) do 
						local t = editor.triggerAdmin:addTrigger(a)
						t:load(v)
						table.insert(a.__triggers,t)
					end 
				end 
				if v.__name then 
					editor._G[v.__name] = a
					a.__name = v.__name
				end
			end 			
		end 
	end 
end 
return imageAdmin