local textureManager = {}
function textureManager:new(self)
	local s = {parent = self,sheets = {}}
	setmetatable(s,{__index = textureManager})
	return s
end 
local sheet = {}
local sheetMeta = {__index = sheet}
function textureManager:newSheet(name,image)
	assert(image,"CANNOT FIND IMAGE: ",name,image)
	if self:seek(name) then self:remove(name) end
	local s = {parent = self,name = name,image = image,quads = {}}
	table.insert(self.sheets,s)
	setmetatable(s,{__index = sheet})
	return s
end
function textureManager:prNewSheet(name,image)
	local s = {name = name,image = image,quads = {}}
	setmetatable(s,sheetMeta)
	return s
end
function sheet:addQuad(name,x,y,w,h,delay,frame1,frame2)
	local quad = {
		name = name,
		x = x,
		y = y,
		w = w, 
		h = h,
		delay = delay,
		frame1 = frame1,
		frame2 = frame2,
	}
	assert(self.image,"CANNOT FIND IMAGE:"..self.name,name)
	local iw,ih = self.image:getDimensions()
	self.image:setWrap("repeat","repeat")
	if findDistance(frame1,frame2) > 0 then  
		quad.actual = newAnimation(self:getImage(),w,h,delay or 0.09,frame1,frame2,x,y)
	else 
		quad.actual = love.graphics.newQuad(x,y,w,h,iw,ih)
	end 
	table.insert(self.quads,quad)
end 
function sheet:getImage()
	return self.image
end 
function sheet:seek(s,remove)
	local as 
	for i,v in ipairs(self.quads) do 
		if not as and v.name == s then 
			as = v 
			if remove then 
				table.remove(self.quads,i)
			end 
		end 
	end 
	return as 
end 
function sheet:getQuads()
	return self.quads,self.image
end 
function sheet:getQuad(name)
	local quad
	for i,v in pairs(self.quads) do 
		if v.name == name and not quad then 
			quad = v.actual
		end 
	end 
	return quad
end
function sheet:save()
	local g = {name = name}
	g.quads = deepCopy2(self.quads)
	for i,v in pairs(g.quads) do 
		g.quads[i].actual = nil
	end 
	return g 
end
function sheet:remove()
	if self.parent and self.parent.parent then 
		local parsed = self.parent.parent:parse()
		for i,v in ipairs(parsed) do 
			if v.image_source then 
				if v.sheet == self.name then 
					v:removeTexture()
				end 
			end 
		end 
		for i,v in pairs(self.parent.sheets) do 
			if v == self then 
				table.remove(self.parent.sheets,i)
			end 
		end 
	end 
end
function sheet:applyEdit()
if self.parent and self.parent.parent then 
	local parsed = self.parent.parent:parse()
		for i,v in ipairs(parsed) do 
			if v.image_source then 
				if v.sheet == self.name then 
					local found
					for k,b in ipairs(self.quads) do 
						if v.quad == b.name then 
							v:setTexture(self,b)
							found = true 
						end 
					end 
					if not found then 
						v:removeTexture()
					end 
				end 
			end 
		end 
	end 
end 
function textureManager:remove(sheet)
	if type(sheet) == "string" then 
		for i,v in pairs(self.sheets) do 
			if v.name == sheet then 
				table.remove(self.sheets,i)
			end 
		end 
	elseif type(sheet) == "table" then 
		for i,v in pairs(self.sheets) do 
			if v == sheet then 
				table.remove(self.sheets,i)
			end 
		end 
	else 
		error("Invalid parameter: "..type(sheet))
	end 
end  
function textureManager:addSheet(sheet)
	table.insert(self.sheets,sheet)
end 
function textureManager:seek(sheet)
	local sheets
	if type(sheet) == "string" then 
		for i,v in pairs(self.sheets) do 
			if v.name == sheet then 
				sheets = v
			end 
		end 
	end 
	return sheets
end 
function textureManager:save(map,parents)
	local g = {}
	local lfs = love.filesystem
	local dir = map.."/spritesheets/"
	if not lfs.exists(dir) then 
		lfs.createDirectory(dir)
	end 
	for i,v in ipairs(self.sheets) do 
		local a = v:save()
		local name = v.name
		local found 
		print(name)
		for i,v in ipairs(v.quads) do 
			print(v.name)
		end 
		print("SAVED AS: ",map.."/spritesheets/"..name)
		local parents = getParents(map)
		print("[-Looking for sprite-sheets in parents-")
		for i,v in ipairs(parents) do 
			print(v.."/spritesheets/"..name)
			if lfs.exists(v.."/spritesheets/"..name) and not found then 
				found = true 
				break 
			end 
		end 
		print(found)
		print("-]")
		if not found then 
			lfs.write(map.."/spritesheets/"..name,Tserial.pack(a))
			local dir = self.parent.map.."/spritesheets/"
			if not love.filesystem.exists(dir) then 
				love.filesystem.createDirectory()
			end 
			if not love.filesystem.exists(dir..v.name) then 
				if love.filesystem.exists("resources/spritesheets/"..name) then 
					copyFile("resources/spritesheets/"..name,dir..v.name)
				else 
					error("CANNOT FIND SPRITE SHEET: "..name.."\n either: ".."resources/spritesheets/"..name.."\n or "..dir..v.name)
				end 
			end 
		end 
		if not love.filesystem.exists(self.parent.map.."/image/"..name) then 
			local exists 
			for i,v in ipairs(parents) do 
				if love.filesystem.exists(v.."/image/"..name) then 
					exists = true
				end 
			end 
			if not exists then 
				if love.filesystem.exists("resources/image/"..name) then 
					local d = love.filesystem.newFileData("resources/image/"..name)
					love.filesystem.write(self.parent.map.."/image/"..name,d)
				else 
					print("Couldn't find image:"..tostring(name))
				end 
			end
		end
	end 
end
function textureManager:load(map,parents)
	print("--LOADING TEXTURES--")
	local availImages = {}
	local lfs = love.filesystem
	local dir = map.."/spritesheets/"
	print("FROM DIR: "..dir)
	local items = lfs.getDirectoryItems(dir)
	for i,v in ipairs(items) do 
		local name = v 
		print("Name: ",name)
		local v = Tserial.unpack(lfs.read(dir..v))
		local img 
		for k,b in pairs(availImages) do 
			if name == b.name then 
				img = b.image
			end 
		end 
		if not img then 
			print("LOOKING AT: ",self.parent.map.."/image/"..name) 
			if love.filesystem.exists(self.parent.map.."/image/"..name) then 
				img = love.graphics.newImage(self.parent.map.."/image/"..name)
			elseif parents then 	
				for i,v in ipairs(parents) do 
					print("LOOKING AT: ",v.."/image/"..name) 
					if love.filesystem.exists(v.."/image/"..name) then 
						img = love.graphics.newImage(v.."/image/"..name)
					end 
				end 
			elseif love.filesystem.exists("/resources/image"..name) then
				print("LOOKING AT:","/resources/image"..name) 
				img = love.graphics.newImage("/resources/image"..name)
			else 
				error("Image does not exist: "..name)
			end 
			local a = {name = name,image = img}
			table.insert(availImages,a)
		end 
		local s = self:newSheet(name,img)
		for i,v in ipairs(v.quads) do 
			print("Adding Quad: "..v.name,"WithFrames: "..tostring(v.frame1).."  "..tostring(v.frame2))
			s:addQuad(v.name,v.x,v.y,v.w,v.h,v.delay,v.frame1,v.frame2)
		end 
	end 
	local function make(v,dir)
		local name = v 
		local v = Tserial.unpack(lfs.read(dir..v))
		local img 
		for k,b in pairs(availImages) do 
			if name == b.name then 
				img = b.image
			end 
		end 
		if not img then 
			if love.filesystem.exists(self.parent.map.."/image/"..name) then 
				img = love.graphics.newImage(self.parent.map.."/image/"..name)
			elseif parents then 	
				for i,v in ipairs(parents) do 
					if love.filesystem.exists(v.."/image/"..name) then 
						img = love.graphics.newImage(v.."/image/"..name)
					end 
				end 
			elseif love.filesystem.exists("/resources/image"..name) then
				img = love.graphics.newImage("/resources/image"..name)
			else 
				error("Image does not exist: "..name)
			end 
			local a = {name = name,image = img}
			table.insert(availImages,a)
		end 
		print("MAKING: ",name,img)
		local s = self:newSheet(name,img)
		for i,v in ipairs(v.quads) do 
			s:addQuad(v.name,v.x,v.y,v.w,v.h,v.delay,v.frame1,v.frame2)
		end
	end 
	if parents then 
		for i,v in ipairs(parents) do 
			local dir = v.."spritesheets/"
			local items = lfs.getDirectoryItems(dir)
			for k,m in ipairs(items) do 
				if not love.filesystem.exists(self.parent.map.."/spritesheets/"..m) then				
						print("---------------")
						print("DOES NOT EXIST: ")
						print(self.parent.map.."/spritesheets/"..m)
					make(m,dir)
				end 
			end 
		end 
	end 
	return self.sheets
end  
function textureManager:unpack()
	return self.sheets
end 
function textureManager:clear() 
	print("CLEARING")
	for i,v in pairs(self) do 
		if type(v) ~= "function" and i ~= "parent" then 
			self[i] = nil
		end 
	end 
	self.sheets = {} 
end 
function textureManager:prLoad(map)
	local availImages = {}
	local sheets = {} 
	local lfs = love.filesystem
	local dir = map.."/spritesheets/"
	print(dir)
	local t = lfs.getDirectoryItems(dir)
	for i,v in ipairs(t) do 
		local name = v
		print(name)
		local v = Tserial.unpack(lfs.read(dir..v))
		local img 
		for k,b in pairs(availImages) do 
			if name == b.name then 
				img = b.image
			end 
		end 
		if not img then 
			if love.filesystem.exists(self.parent.map.."/image/"..name) then 
				img = love.graphics.newImage(self.parent.map.."/image/"..name)
			elseif love.filesystem.exists("/resources/image/"..name) then
				img = love.graphics.newImage("/resources/image/"..name)
			else 
				local found 
				local parents = getParents(eCurrentMap)
				for i,v in ipairs(parents) do 
					local d = v.."/image/"
					local t = lfs.getDirectoryItems(d)
					for i,v in ipairs(t) do 
						print(d..name)
						if love.filesystem.exists(d..name) then
							img = love.graphics.newImage(d..name)
						end 						
					end 
				end  
				assert(img,"Image does not exist: "..name.." at either:\n "..self.parent.map.."/image/"..name.." or\n ".."/resources/image/"..name)
			end 
			local a = {name = name,image = img}
			table.insert(availImages,a)
		end 
		local s = self:prNewSheet(name,img)
		for i,v in ipairs(v.quads) do 
			s:addQuad(v.name,v.x,v.y,v.w,v.h,v.delay,v.frame1,v.frame2)
		end
		table.insert(sheets,s)
	end 
	return sheets
end 
local textures = {}
function textures:new(shape,img,quad)
	local s = {
		shape = shape,
	}
	local shape = shape.colision or shape:getColision()
	local x,y = shape:bbox()
	local x2,y2 = shape:center()
	local w,h = findDistance(x,x2)*2,findDistance(y,y2)*2
	self.width = w 
	self.height = h
	setmetatable(s,{__index = textures})
 	s:setImage(img,quad)
	local iw,ih = s.canvas:getDimensions()
	s.quad = love.graphics.newQuad(0,0,w,h,iw,ih)
	return s 
end 
function textures:setImage(img,quad)
	img:setWrap("repeat","repeat")
	if type(quad) ~= "table" then 
		local _,_,w,h = quad:getViewport()
		self.canvas = love.graphics.newCanvas(w,h)	
		love.graphics.setCanvas(self.canvas)
		
		setColor()
		love.graphics.draw(img,quad,0,0,0,1,1)
		love.graphics.setCanvas()
	else 
		self.animation = quad
		local w,h = quad:unpack()
		self.canvas = love.graphics.newCanvas(w,h)	
		love.graphics.setCanvas(self.canvas)
			self.animation:draw(0,0,0,1,1)
		love.graphics.setCanvas()
	end 
	self.data = {quad:getViewport()}
	self.canvas:setWrap("repeat","repeat")
end 
function textures:update(dt)
	if self.animation then 
		self.canvas:clear()
		self.animation:update(dt)
		self.canvas:renderTo(function()
			self.animation:draw(0,0,0,1,1)
		end)
	end 
end 
function textures:updateCol()
	local shape = self.shape:getColision()
	local quad = self.quad 
	local x,y = shape:bbox()
	local x2,y2 = shape:center()
	local w,h = findDistance(x,x2)*2,findDistance(y,y2)*2
	local iw,ih = quad:getDimensions()
	quad:setViewport(0,0,w,h,iw,ih)
	self.width 	= w 
	self.height = h
end 
function textures:draw(sx,sy)
	local shape = self.shape
	local x,y = shape:getColision():center()
	local w,h = findDimensions(shape:getColision())
	love.graphics.setStencil(function()
		shape:getColision():draw("fill")
	end)
	love.graphics.draw(self.canvas,self.quad,x,y,0,sx,sy,w/2,h/2)
	love.graphics.setStencil()
end
function textures:remove()
	for i,v in ipairs(self) do 
		self[i] = nil 
	end 
end 
function textureManager:add(s,block) 
	local t = false 
	local name = s.name 
	local found 
	for i,v in ipairs(self.sheets) do 
		if v.name == name then 
			found = true
		end 
	end 
	if not found then 
		local t = deepCopy2(s)
		t.parent = self 
		for i,v in ipairs(t.quads) do 
			v.parent = t
		end 
		setmetatable(t,sheetMeta)
		table.insert(self.sheets,t)
	end 
	return t 
end 
local makeTexture = {}
function makeTexture:new(self,ui)
	local s = {parent = self,ui = ui}
	setmetatable(s,{__index = makeTexture})
	local activeSpritesheet
	local w,h = 600,400
	local mainFrame = ui:addFrame("Available sprite-sheets",self.width/2,self.height/2,w,h)
	mainFrame:setHeader()
	local defective
	local activeSheet 
	
	local new = ui:addButton(mainFrame,2 + 100/2,h - 20,100,20)
	new:setText("New")
	new.OnClick = function()
		s:makeSpritesheet(self,ui)
		
		mainFrame:remove()
		if defective then 
			for i,v in ipairs(defective) do 
				v:remove()
			end 
		end
		
	end 	
	local cancel = ui:addButton(mainFrame,w - 102,h - 20,100,20)
	cancel:setText("cancel")
	cancel.OnClick = function()
		mainFrame:remove()
		if defective then 
			for i,v in ipairs(defective) do 
				v:remove()
			end 
		end 
	end 
	local w,h = (w - 4 - 50)/2,h - 50
	local resourceA = ui:addFrame("Resources:",2,20,w,h,mainFrame)
	resourceA.color = colors.red
	resourceA:setHeader()
	local resourceB = ui:addFrame(""..self.map,2 + w + 50,20,w,h,mainFrame)
	resourceB:setHeader()
	resourceB.color = colors.green
	local del = ui:addButton(mainFrame,2 + w + 2 + 10,mainFrame.height/2,20)
	del:setText("D")
	del.OnClick = function()
		activeSheet:remove()
	end 
	del.OnUpdate = function()
		if activeSheet then 
			del:setInactive(false)
		else 
			del:setInactive(true)
		end 
	end 
	local listA = ui:addList(resourceA,2,20,w -4,h -22)
	local listB = ui:addList(resourceB,2,20,w -4,h -22)
	if love.filesystem.exists("resources/spritesheets") then 
		local t = self.textureManager:prLoad("resources")
		defective = t
		for i,v in ipairs(t) do 
			local b = ui:addButton(nil,0,0,w-2,30)
			b:setText(v.name .." : "..#v.quads)
			b.OnClick = function()
				mainFrame:remove()
				s:editSheet(nil,v.name,self,self.ui,nil,v,true)
			end 
			listA:addItem(b)
		end 
	end 
	if love.filesystem.exists(self.map.."/spritesheets") then  
		local t = self.textureManager:prLoad(self.map)
		for i,v in ipairs(t) do 
			local b = ui:addButton(nil,0,0,w-2,30)
			b:setText(v.name .." : "..#v.quads)
			b.OnClick = function()
				mainFrame:remove()
				s:editSheet(nil,v.name,self,self.ui,nil,v,true )
			end 
			listB:addItem(b)
		end 
	end 
end 

function makeTexture:editSheet(item,name,parent,ui,force,spriteSheet2,force2)
	local self = parent
	local w,h = self.width - 20,self.height - 150
	local mainFrame = ui:addFrame(nil,self.width/2,self.height/2,w,h)
	mainFrame.color = basicFrameColor
	local spriteSheet
	local exists
	if not force then 
		
		spriteSheet = spriteSheet2 or parent.textureManager:seek(name)
		if spriteSheet then 
			spriteSheet = deepCopy2(spriteSheet)
			setmetatable(spriteSheet,{__index = sheet})
			spriteSheet.parent = parent.textureManager
		end
	end
	local image
	if not spriteSheet then 
		image = love.graphics.newImage(item) 
		spriteSheet = self.textureManager:newSheet(name,image)
	else
		exists = true 
		image = spriteSheet:getImage()
	end 
	local quad 
	local cab = colors.green 
	local err = "Errors will be displayed here!"
	local fh = ui:getFont():getHeight()
	local make = ui:addButton(mainFrame,2,fh*8 + 25,100,30)
	local add = ui:addButton(mainFrame,2 + 120,fh*8 + 25,100,30)
	add:setText("Add")
	local close = ui:addButton(mainFrame,2 + 105,mainFrame.height - 20,100,20)
	close:setText("Close")
	close.OnClick = function()
		spriteSheet:remove()
		mainFrame:remove()
	end 
	local save = ui:addButton(mainFrame,2,mainFrame.height - 20,100,20)
	save:setText("Save")
	save.OnDraw = function()
		setColor(cab)
			love.graphics.print(err,save.x - save.width/2,save.y - save.height*2 + 7)
		setColor(colors.white)
	end 
	save.OnClick =  function()
		Gamestate.current().BlockCamera = nil
		mainFrame:remove()
		if exists then 
			local found 
			for i,v in ipairs(self.textureManager.sheets) do 
				if v.name == spriteSheet.name then 
					found = true 
					self.textureManager.sheets[i] = spriteSheet
				end 
			end 
			if not found then 
				spriteSheet.parent = self.textureManager
				table.insert(self.textureManager.sheets,spriteSheet)
			end 
			if force2 then 
				spriteSheet = self.textureManager:add(spriteSheet)
			end 
			if spriteSheet then 
				spriteSheet:applyEdit()
			end 
		end 
	end 
	
	
	
	make:setText("Make")
	local txt = {
		"Name:",
		"X:",
		"Y:",
		"Width:",
		"Height:",
		"Delay",
		"Start",
		"Finish"
	}
	local val = {
		"New",
		0,
		0,
		image:getWidth(),
		image:getHeight(),
	}
	local b = {}
	local al = {"1","2","3","4","5","6","7","8","9","0",".","-"}
	local fw = ui:getFont():getWidth("Height: ")
	local aw,ah = 800/5,mainFrame.height - 40 -2 
	local list = ui:addList(mainFrame,mainFrame.width - (aw + 5),5,aw,ah)
	local listWidth = aw
	local quads = spriteSheet:getQuads()
	for i,v in ipairs(quads) do 
		local a = ui:addButton(nil,0,0,aw,100)
		a.dontDrawL = true
		a.dontDrawB = true 
		a.dontDrawC = true
		a.OnClick = function()
			spriteSheet:seek(v.name,true)
			updateList()
		end 
		if type(v.actual) ~= "table" then 
			a.OnDraw = function()
				local _,_,w,h = v.actual:getViewport()
				local sx,sy =  aw/w,100/h
				love.graphics.draw(spriteSheet.image,v.actual,a.x - a.width/2,a.y - a.height/2,0,sx,sy)		
			end 
		else 
			local inst = v.actual:addInstance()
			a.OnDraw = function()
				local w,h = v.actual:unpack()
				local sx,sy =  aw/w,100/h
				inst:draw(a.x - a.width/2,a.y - a.height/2,0,sx,sy)
			end 
			function a.OnUpdate(dt)
				inst:update(dt)
			end
		end  
		list:addItem(a)
	end 
	function updateList()
		local quads = spriteSheet:getQuads()
		for i,v in ipairs(list.items) do 
			v:remove()
			list.items[i] = nil 
		end 
		for i,v in ipairs(quads) do 
			local a = ui:addButton(nil,0,0,aw,aw)
			a.dontDrawL = true
			a.dontDrawB = true 
			a.dontDrawC = true
			a.OnClick = function()
				spriteSheet:seek(v.name,true)
				updateList()
			end 
			if type(v.actual) ~= "table" then 
				a.OnDraw = function()
					local _,_,w,h = v.actual:getViewport()
					local sx,sy =  aw/w,aw/h
					love.graphics.draw(spriteSheet.image,v.actual,a.x - a.width/2,a.y - a.height/2,0,sx,sy)		
				end 
			else 
				local inst = v.actual:addInstance()
				a.OnDraw = function()
					local w,h = v.actual:unpack()
					local sx,sy =  aw/w,aw/h
					inst:draw(a.x - a.width/2,a.y - a.height/2,0,sx,sy)
				end 
				function a.OnUpdate(dt)
					inst:update(dt)
				end
			end 
			list:addItem(a)
		end 
	end 
	
	for i,v in ipairs(txt) do 
		txt[i] = v.."\n"
		local a = ui:addTextinput(mainFrame,fw + 2,20 + fh*(i-1),100,fh)
		if i ~= 1 then 
			a:setAllowed(al)
		end 
		a:setText(val[i])
		table.insert(b,a)
	end 
	local Animation  
	make.OnClick = function()
		Animation = nil 
		quad = nil 
		local name,x,y,w,h,delay,f1,f2 = b[1]:getText(),b[2]:getText(),b[3]:getText(),b[4]:getText(),b[5]:getText(),b[6]:getText(),b[7]:getText(),b[8]:getText()
		x,y,w,h = tonumber(x),tonumber(y),tonumber(w),tonumber(h)
		if x and y and w and h then 
			if findDistance(f1,f2) > 0 then
				Animation = newAnimation(image,tonumber(w),tonumber(h),tonumber(delay),tonumber(f1),tonumber(f2),tonumber(x),tonumber(y))
			else 
				quad = love.graphics.newQuad(x,y,w,h,image:getDimensions())
			end 
		end 
	end 
	local x = ui:getFont():getWidth("Height:")
	mainFrame.OnDraw = function()
		love.graphics.print(table.concat(txt),mainFrame.x - mainFrame.width/2 + 2,mainFrame.y - mainFrame.height/2 + 20)
	end 
	local fw,fh = (800/2)/2 + 20,450 - 85
	local fh2 = ui:getFont():getHeight()
	local DisplayWidth = fw + 4
	local displayF = ui:addFrame(nil,2,fh2*8 + 60,fw,fh/2,mainFrame)
	local fh = fh/2
	displayF.dontDraw = true
	displayF.OnDraw = function()
		if image and quad then 
			local _,_,w,h = quad:getViewport()
			local sx,sy =  displayF.width/w,displayF.height/h
			love.graphics.draw(image,quad,displayF.x - displayF.width/2,displayF.y - displayF.height/2,0,sx,sy)
		elseif Animation then
			local w,h = Animation:unpack()
			local sx,sy =  displayF.width/w,displayF.height/h
			Animation:draw(displayF.x - displayF.width/2,displayF.y - displayF.height/2,0,sx,sy)
		elseif image and not quad then 
			local w,h = image:getDimensions()
			local sx,sy =  displayF.width/w,displayF.height/h
			love.graphics.draw(image,displayF.x - displayF.width/2,displayF.y - displayF.height/2,0,sx,sy)
		end 
		displayF.colision:draw("line")
	end 
	function displayF.OnUpdate(dt)
		if Animation then 
			Animation:update(dt)
		end 
	end 
	add.OnClick = function()
		local name,x,y,w,h,delay,f1,f2 = b[1]:getText(),b[2]:getText(),b[3]:getText(),b[4]:getText(),b[5]:getText(),b[6]:getText(),b[7]:getText(),b[8]:getText()
		if not spriteSheet:seek(name) then 
			spriteSheet:addQuad(name,tonumber(x),tonumber(y),tonumber(w),tonumber(h),tonumber(delay),tonumber(f1),tonumber(f2))
			err = "Added "..name.." successfully!"
			cab = colors.green 
		else 
			err = "The name: '"..name.."' already exists."
			cab = colors.red
		end 
		updateList()
	end 
	local availw =  mainFrame.width - ((DisplayWidth +10) + listWidth+10)
	local availh = mainFrame.height - 44
	local DrawFrame = ui:addFrame(nil,DisplayWidth+5,5,availw,availh,mainFrame)
	local ax,ay = 0,0
	local ascale = 1
	local canvas = love.graphics.newCanvas(availw,availh)
	local lg = love.graphics
	local target 
	local shape 
	local making 
	local function getOther(exp,a,b)
		local other 
		if a == exp then 
			other = b 
		elseif b == exp then 
			other = a 
		end 
		return other
	end
	local shapeCollider
	local shapeCol = function(dt,a,b)
		if not making then 
			local other = getOther(shapeCollider.mxb,a,b)
			if other then 
				target = other
			end 
		end 
	end 
	local shapeStop = function(dt,a,b)
		if not making then 
			local other = getOther(shapeCollider.mxb,a,b)
			if other then 
				target = nil
			end 
		end 
	end 
	
	shapeCollider = HC(200,shapeCol,shapeStop)
	local pressX,pressY = 0,0
	shapeCollider.mxb = shapeCollider:addCircle(0,0,1)
	local function worldPos(x,y,ax,ay,scale)
		local x = (x-ax)/scale
		local y = (y-ay)/scale
		return x,y
	end
	local function BlockCamera(x,y,b)
		local cx,cy = DrawFrame.colision:bbox()
		local x,y =  x-cx,y -cy 
		if b == "wu" then 
			local scale = ascale - math.floor((0.05*ascale)*1000)/1000
			if scale > 0 and scale > 0.1 then
				local lastzoom = ascale
				local mouse_x = x - ax
				local mouse_y = y - ay
				ascale = scale
				local newx = mouse_x * (ascale/lastzoom)
				local newy = mouse_y * (ascale/lastzoom)
				ax = ax + (mouse_x-newx)
				ay = ay + (mouse_y-newy)
			else
				ascale = 0.1
			end
		elseif b == "wd" then 
			local scale = ascale + math.floor((0.05*ascale)*1000)/1000
			local scalex = ascale
			if scale > 0 and scale < 20 then
				local lastzoom = ascale
				local mouse_x = x - ax
				local mouse_y = y - ay
				ascale = scale
				local newx = mouse_x * (ascale/lastzoom)
				local newy = mouse_y * (ascale/lastzoom)
				ax = ax + (mouse_x-newx)
				ay = ay + (mouse_y-newy)
			else
				ascale = 20
			end
		end 
		pressX,pressY = worldPos(x,y,ax,ay,ascale)
		if not target then making = true 
		else 
			local x,y = target:center()
			target._nx = x - pressX 
			target._ny = y - pressY 
		end
	end
	local gamestate = Gamestate.current()
	DrawFrame.OnUpdate = function(dt)
		if DrawFrame.colision:collidesWith(ui:getmxb()) then 
		Gamestate.current().BlockCamera = BlockCamera
		shapeCollider:update(dt)
		local x,y = love.mouse.getPosition()
		local cx,cy = DrawFrame.colision:bbox()
		local x,y =  x-cx,y-cy  
		x,y = worldPos(x,y,ax,ay,ascale)
		
		shapeCollider.mxb:moveTo(x,y)
		if love.mouse.isDown("l") then 
			if target then 
				make.OnClick()
				target:moveTo((target._nx or 0) + x,(target._ny or 0) + y)
			else 
				make.OnClick()
				local w,h 
				if shape then shapeCollider:remove(shape) end
				local x,y = math.floor(x),math.floor(y)
				local pressX,pressY = math.floor(pressX),math.floor(pressY)
				if math.abs(x-pressX) <= 0 then 
					w = 2 
				else 
					w = x-pressX
				end 		
				if math.abs(y-pressY) <= 0 then 
					h = 2 
				else 
					h = y-pressY
				end
				local w,h = math.floor(w),math.floor(h)				
				shape = shapeCollider:addRectangle(pressX,pressY,w,h)
			end 
		else 
			making = false
		end
		local val = (200*dt)
		if love.keyboard.isDown("a") then
			ax = ax + val 
		elseif love.keyboard.isDown("d") then 
			ax = ax - val
		elseif love.keyboard.isDown("s") then 
			ay = ay -val
		elseif love.keyboard.isDown("w") then 
			ay = ay +val
		end 
		if shape then 
			local x,y = shape:bbox()
			local w,h = findDimensions(shape)
			b[2]:setText(math.floor(x))
			b[3]:setText(math.floor(y))
			b[4]:setText(math.floor(w))
			b[5]:setText(math.floor(h))
			local function changeText()
				if shape then 
					shapeCollider:remove(shape)
					local x,y,w,h = b[2]:getText(),b[3]:getText(),b[4]:getText(),b[5]:getText()
					local x,y,w,h = tonumber(x),tonumber(y),tonumber(w),tonumber(h)
					shape = shapeCollider:addRectangle(x or 0,y or 0,w or 2,h or 2)
					shapeCollider:update(dt)
					make.OnClick()
				end
			end
			b[2].OnChangeText = changeText
			b[3].OnChangeText = changeText
			b[4].OnChangeText = changeText
			b[5].OnChangeText = changeText
		end
		else 
			Gamestate.current().BlockCamera = nil
		end
		canvas:clear()
		canvas:renderTo(function()
			lg.push()
			lg.translate(ax,ay) -- TRANSLATE AND THEN SCALE!!!!
			lg.scale(ascale)
			lg.draw(image)
			local ol = lg.getLineWidth()
			if target then 
				setColor(colors.red)
				target:draw("line")
			elseif shape then 	
				setColor(colors.violet)
				shape:draw("line")
			end 
			setColor(colors.red)
			shapeCollider.mxb:draw("fill")
			setColor()
			lg.setLineWidth(ol)
			lg.pop()
		end)
	end
	
	DrawFrame.dontDraw = true
	DrawFrame.OnDraw = function()
		DrawFrame.colision:draw("line")
		local cx,cy = DrawFrame.colision:bbox()
		love.graphics.draw(canvas,cx,cy)
	end
end 
function makeTexture:makeSpritesheet(parent,ui,force)
	local self = parent
	local mainFrame = ui:addFrame("Pick an image",self.width/2,self.height/2,300,300)
	mainFrame:setHeader()
	local resourceImages = love.filesystem.getDirectoryItems("resources/image")
	local mapImages = love.filesystem.getDirectoryItems(self.map.."/image")
	
	local w,h = 300/2 - 4,300 - 80
	local ri = ui:addList(mainFrame,2,50,w,h)
	local mi = ui:addList(mainFrame,2 + w + 2,50,w,h)
	
	local function onClick(item,name)
		mainFrame:remove()
		makeTexture:editSheet(item,name,parent,ui,force)
	end 
	for i,v in ipairs(resourceImages) do 
		local b = ui:addButton(nil,0,0,w,30)
		b:setText(tostring(v))
		b.OnClick = function() onClick("resources/image/"..v,v) end 
		ri:addItem(b)
	end 
	for i,v in ipairs(mapImages) do 
		local b = ui:addButton(nil,0,0,w,30)
		b:setText(tostring(v))
		b.OnClick = function() onClick(parent.map.."/image/"..v,v) end 
		mi:addItem(b)
	end 
end 

local t = {textures,textureManager,makeTexture}
return t