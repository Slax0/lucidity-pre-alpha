local layers = {}
function layers:new(self)
	local s = {
		items = {},
		parent = self,
	}
	return setmetatable(s,{__index = layers})
end 

function layers:update(dt)
	for i,v in ipairs(self.items) do
		if self.parent.current_zmap == v.zmap then 
			v.actual_name = v.name.." (current)"
			v.active = true
		else 
			v.actual_name = v.name
			v.active = false
		end 
	end 
	local parsed = self.parent:parse()
	for i,v in pairs(parsed) do 
		local layer = self:seek(v.zmap or 1)
		if layer then 
			local modx,mody = layer.modx or 0 ,layer.mody or 0 
			if modx == "n" then 
				modx = -(self.parent.x/self.parent.scale)
			else 
				modx = (self.parent.x - love.graphics.getWidth()/2)*modx
			end 
			if mody == "n" then 
				mody = -(self.parent.y/self.parent.scale)
			else 
				mody = (self.parent.y - love.graphics.getHeight()/2)*mody	
			end 
			if v.setMod then v:setMod(modx,mody) end 
		end
	end 
end 
function layers:clear()
	for i,v in ipairs(self.items) do 
		self:removeLayer(v)
	end 
	clearTable(self.items)
	if self.parent.current_zmap then self:addLayer("Layer: 1",self.parent.current_zmap,0,0,true) end 
end 
function layers:unpack(zmap)
	local a 
	for i,v in ipairs(self.items) do
		if v.zmap == zmap then 
			a = v 
		else 
			error("Couldn't find layer!")
		end 
	end 
 	return a:unpack()
end 
function layers:save()
	local a = {}
	for i,v in ipairs(self.items) do 
		local b = {
			name = v.name,
			zmap = v.zmap,
			modx = v.modx,
			mody = v.mody,
			colision = v.colision,
			sound = v.sound,
			transp = v.transp,
		}
		table.insert(a,b)
	end 
	return a
end 
function layers:getRange()
	local min = math.huge 
	local max = 0 
	for i,v in ipairs(self.items) do 
		if v.zmap < min then 
			min = v.zmap 
		elseif v.zmap > max then 
			max = v.zmap
		end 
	end
	return min,max	
end 
function layers:seek(zmap)
	local layer 
	for i,v in ipairs(self.items) do 
		if v.zmap == zmap then 
			layer = v 
		end 
	end 
	return layer 
end 
function layers:load(a)
	self.items = {}
	for i,v in ipairs(a) do 
		print("HasColision: "..tostring(v.colision),v.zmap)
		local l = self:addLayer(v.name,v.zmap,v.modx,v.mody,v.colision,v.sound,v.transp)
		l.locked = v.locked
	end 
	return self.items
end 
function layers:removeLayer(lay)
	local zmap
	local d 
	local layer
	local parsed = self.parent:parse()
	local removed = 0 
	
	if #self.items > 1 then 
		if type(lay) == "number" then 
			zmap = lay
		end 
		for i,v in pairs(self.items) do
			if zmap and not d then 
				if v.zmap == zmap then 
					layer = v 
					table.remove(self.items,i)
				end 
			elseif lay == v then 
				d = true 
				zmap = lay.zmap
				table.remove(self.items,i)
			end
		end	
		for i,v in ipairs(parsed) do 
			if zmap == v.zmap then 
				removed = removed +1
				v:remove()
			end 
		end 
		self:sort()
	end 
	return removed
end
function layers:sort()
	local function check()
		local mi,ma = self:getRange()	
		for i=mi,ma do 
			if not self:seek(i) then 
				for k=i+1,ma do 
					self:move(self:seek(k),k-1)
				end 
				check()
			end 
		end 
	end
	check()
end
function layers:move(l,z)
	if l then 
		local parsed = self.parent:parse()
		for i,v in ipairs(parsed) do 
			if v.zmap == l.zmap then 
				parsed[i].zmap = z 
			end 
		end 
		l.zmap = z
	end	
end 
function layers:moveLayers(old,new)
	self:sort()
	local oldLayer = self:seek(old)
	local newLayer = self:seek(new)
	if newLayer and oldLayer then 
		local parsed = self.parent:parse()
		
		local nzmap = newLayer.zmap
		local ozmap = oldLayer.zmap 
		
		for i,v in pairs(parsed) do 
			if v.zmap == oldLayer.zmap then
				parsed[i].zmap = nzmap 
			elseif v.zmap == newLayer.zmap then
				parsed[i].zmap = ozmap 
			end 
		end 

		oldLayer.zmap  	= nzmap 
		newLayer.zmap 	= ozmap
	else 
		print("NO LAYERS:"..old.." or "..new)
	end 
end 
local layer = {} 
function layers:addLayer(name,zmap,modx,mody,colision,sound,transp)
	local s = {
		name = name,
		zmap = zmap or 1,
		modx = modx or 0 ,
		mody = mody or 0,
		colision = colision,
		sound = sound or false,
		transp = transp or 255,
		locked = locked,
		parent = self
	}
	s.transp = tonumber(s.transp)
	table.insert(self.items,s)
	return setmetatable(s,{__index = layer})
end 
function layer:getMod()
	local modx,mody = self.modx or 0 ,self.mody or 0 
	local editor = self.parent.parent
	if modx == "n" then 
		modx = -(editor.x/(editor.scale or 1))
	else 
		modx = (editor.x - love.graphics.getWidth()/2)*modx
	end 
	if mody == "n" then 
		mody = -(editor.y/(editor.scale or 1))
	else 
		mody = (editor.y - love.graphics.getHeight()/2)*mody	
	end 	
	return modx,mody
end
function layer:update(name,zmap,modx,mody,colision,sound,transp)
	self.name = name 
	self.zmap = zmap 
	self.modx = modx 
	self.mody = mody 
	self.colision = colision 
	self.sound = sound
	self.transp = tonumber(transp)
end 
function layer:setParalax(x,y)
	self.modx = x 
	self.mody = y 
end 
function layer:setColision(col)
	self.colision = col
end 
function layer:setSound(s)
	self.sound = s 
end
function layer:setName(name)
	self.name = name 
end 
function layer:getData()
	return self.name,self.modx,self.mody,self.colision,self.sound,self.transp
end 
function layer:setData(n,mx,my,col,sound,transp)
	self.name = n
	self.modx = mx
	self.mody = my
	self.colision = col 
	self.sound = sound
	self.transp = tonumber(transp)
end 
function layer:unpack()
	return self.zmap,self.modx,self.mody,self.colision,self.sound,self.transp
end 
return layers