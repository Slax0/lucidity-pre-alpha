local mapAdmin = {}
local scene_edtior = require("editor/scene_editor")
function mapAdmin.parse(active,passive,self)
	local a = {}
	local function scan(v)
		local item 
		if v.editorShape then
			item = v.editorShape
		elseif v._module then 
			item = v._module
		elseif v._sound then 
			item = v._sound
		end
		if item then 
			table.insert(a,item)
		end
	end 
	for shape in pairs(active) do 
		scan(shape)
	end 
	for shape in pairs(passive) do 
		scan(shape)
	end
	return a 
end 
function mapAdmin.scanModules(self,collider,ret)
	clearTable(ret)
	local passive = collider._active_shapes
	local active = collider._passive_shapes
	local scanTable = function(tables) 
		local scan = function(a)
			local b 
			if a._module then 
				b = a._module
			end 
			return b 
		end 
		for i,v in pairs(tables) do 
			local b = scan(v)
			if v._module then 
				table.insert(ret,v._module)
			end 
		end 
	end 
	scanTable(passive)
	scanTable(active)
	return ret
end 
function mapAdmin.updateSingle(shape,dt)
	local v = shape
	if v.update and not v._updatePriority then v:update(dt) end 
	if v.children then 
		for n,m in ipairs(v.children) do 
			if m.update then  m:update(dt) end  
		end 
	end 
	if v.__triggers then for n,m in ipairs(v.__triggers) do if m.update then m:update(dt) end end end 
end 
function mapAdmin.parseHead(self)
	TotalCount = 0 
	self.zmaps = {}
end 
function mapAdmin.parseSingle(self,shape,dt,rt,cond)
	if not self.zmaps then self.zmaps = {} end 
	if self.phyWorld and self.phyWorld:isDestroyed() then return end 
	local shp 
	local zmaps = self.zmaps 
	local v = shape
	if v.editorShape then 
		shp = v.editorShape
	elseif v._module then 
		shp = v._module
	elseif v._sound then 
		shp = v._sound
	end 
	if shp then 
		TotalCount = TotalCount + 1 
		if shp.updateCamera and rt then 
			table.insert(rt,shp)
		end 
		table.insert(self.all_active,shp)
		local v = shp 
		if v.zmap then 
			if not zmaps[v.zmap] then 
				zmaps[v.zmap] = {}
				zmaps[v.zmap].layer = self.layers:seek(v.zmap)
			end 
			if not zmaps[v.zmap].layer then 
				v:remove()
				zmaps[v.zmap] = nil 
			else 
				v._L = zmaps[v.zmap].layer
				table.insert(zmaps[v.zmap],v)
			end
		else 
			print("REMOVED; NO ZMAP")
			v:remove()
		end 
		shp.__hasFocus = true
		if not cond or (cond and not cond(shp)) then 
			mapAdmin.updateSingle(shp,dt)
		end 
	end 
end 
function mapAdmin.parseTail(self,dt)
	if self.creatingItem then mapAdmin.parseSingle(self,self.creatingItem:getColision(),dt) end 
end 
function mapAdmin.parseVisible(self,tables,p,tables2)
	if p then clearTable(p) end 
	if self.creatingItem then table.insert(tables,self.creatingItem) end
	for i,v in pairs(tables) do 
		local shp 
		if v.editorShape then 
			shp = v.editorShape
		elseif v._module then 
			shp = v._module
		elseif v._sound then 
			shp = v._sound
		end 
		if shp then 
			table.insert(tables2 or self.all_active,shp)
			shp.__hasFocus = true
			if shp.updatePriority and p then 
				shp._updatePriority = true
				table.insert(p,shp)
			end 
		end 
	end 
	if self.creatingItem then 
		table.insert(self.all_active,self.creatingItem)
	end 
end 
function mapAdmin.draw_visible_alt(self,layer,debug,tables )
	local tables = tables or self.all_active
	local mi,ma = layer:getRange() 
	local zmaps = {} 
	for z = mi,ma do 
		if zmaps[z] then 
			for i,v in ipairs(zmaps[z]) do 
				local aLayer = zmaps[z].layer
				if aLayer then 
					local color = aLayer.color or colors.white
					local r,g,b = unpack(color)
					setColor(r,g,b,aLayer.transp)
					if v.draw then v:draw(debug) end 
					setColor()
					if v.children then for n,m in ipairs(v.children) do if m.draw then  m:draw(debug) end end end 
					if v.__triggers then for n,m in ipairs(v.__triggers) do if m.draw then m:draw() end end end 
					setColor()
				else
					v:remove()
					print("MISSING LAYER, REMOVED: ",z)
				end 
			end 
		end
	end
	setColor()	
end 
function mapAdmin.draw_visible(self,zmap,current_zmap,tables)
	local drawTable = {}
	local zsorted = table.zsort(self.all_active)
	if not self.realView then 
		for i,v in ipairs(zsorted) do 
			local color = colors.white 
			if v.zmap ~= current_zmap then 
				color = {255,255,255,100}
				setColor(colors.violet)
				v:getColision():draw(line)
			end 
			setColor(color)
			v:draw()
			setColor()
		end 
	else 
		for i,v in ipairs(zsorted) do 
			if v._L and (v._L.transp ~= 0 or DEBUG) then
				local mov 
				local a = tonumber(v._L.transp)
				setColor(255,255,255,a)
				if v.draw then  v:draw(DEBUG) end
				if v.children then for n,m in ipairs(v.children) do if m.draw then  m:draw(DEBUG) end end end 
				if v.__triggers then for n,m in ipairs(v.__triggers) do if m.draw then m:draw(DEBUG) end end end 
				setColor()
			end
			v._L = nil 
		end 
	end
	
end 
function mapAdmin.update_visible(self,dt) 
	local tables = self.all_active
	for i,v in pairs(tables) do 
		if v.update and not v._updatePriority then v:update(dt) end 
		if v.children then 
			for n,m in ipairs(v.children) do 
				if m.update then  m:update(dt) end  
			end 
		end 
		if v.__triggers then for n,m in ipairs(v.__triggers) do if m.update then m:update(dt) end end end 
	end 
end 
function mapAdmin.update_priority(self,dt,p) 
	local tables = p
	for i,v in pairs(tables) do 
		if v.update then v:update(dt) end 
	end 
end 
function mapAdmin.navi(self,mxb,lhold,alt_lhold,rhold,alt_rhold,clamp)
	if self.aTarget or self.activeClamp and not clamp  then 
		if not self.aTarget then self.aTarget = self.activeClamp end 
		if not self.aTarget.static then 
			if love.mouse.isDown("m") or love.keyboard.isDown("'") then 
			self.activeClamp = self.aTarget
			local i = self.aTarget
			local p = self
			local x,y = self.mx,self.my 
			local x1,y1 = self.aTarget:getColision():center()
			local rads = getAngle(x1,-y1,x,-y)
			local degree = math.deg(rads)
			if degree >= 85 and degree <= 95 then
				degree = 90
			elseif degree >= 175 and degree <= 185 then
				degree = 180
			elseif degree >= -95 and degree <= -85 then
				degree = -90
			elseif degree >= -185 and degree <= -175 then
				degree = -180
			elseif degree >= -5 and degree <= 5 then
				degree = 0
			end
			if i.setRotation then i:setRotation(degree) end 
			elseif love.keyboard.isDown("-") then 
				if i.setScale then i:setScale(i.scale + 1) end
			elseif love.keyboard.isDown("+") then 
				if i.setScale then i:setScale(i.scale - 1) end 
			elseif not love.keyboard.isDown("r") then 
				self.activeClamp = nil 
			end 
		end 
		if lhold or rhold or alt_lhold or alt_rhold then 
			self.LastFramePress = true
		else
			self.LastFramePress = false
		end 
		if self._LastFramePress ~= self.LastFramePress then 
			local x,y = love.mouse.getPosition()
			local b
			if lhold or alt_lhold then 
				b = "l"
			elseif rhold or alt_rhold then 
				b = "r"
			end 
			if self.LastFramePress then 
				mapAdmin.mousepressed(self,mxb,x,y,b)
			else 
				mapAdmin.mousereleased(self,mxb,x,y,b)
			end 
			self._LastFramePress = self.LastFramePress
		end 
	end  
end 
function mapAdmin.mousepressed(self,mxb,x,y,b)
	if self.aTarget then 
		if self.aTarget then 
			local x,y = mxb:center()
			if not self.aTarget.static then 
				if not self.aTarget._nx and not self.aTarget._ny then 
					if not self.aTarget.vertOnly then 
						self.aTarget._nx = (self.aTarget.x) - x
					end 
					if not self.aTarget.horizOnly then 
						self.aTarget._ny = (self.aTarget.y) - y
					end 
				end
			end 
			if  b == "r" then 
				local r = self.aTarget.OnRightClick
				local a = self.aTarget.OnMenu
				local t = self.aTarget
				local ui  = self.ui
				if self.rMenu then 
					self.rMenu:remove()
				end 
				local mx,my = love.mouse.getPosition()
				local menu = ui:addMenu(nil,mx,my)	
				local mf = t:getColision()
				local x,y = mf:center()
				local cx,cy = mf:bbox()
				local w = findDistance(x,cx)
				local h = findDistance(y,cy)
				menu.mainFrame:moveTo(mx - w/2,my + h)
				menu.OnFocusLost = function()
					menu:remove()
				end 
				self.rMenu = menu
				if r then 
					t:OnRightClick(self.rMenu,ui,self)
				end 
				if a then 
					t:OnMenu(self.rMenu,ui,self)
				end 
				local del
				local prop 
				local layer 
				local trigger 
				if not self.aTarget.static then 
					for i,v in pairs(self.rMenu._items) do 
						local t = string.lower(v.text)
						if string.find(t,"delete") then 
							del = true 
						elseif string.find(t,"properties") then 
							prop = true 
						elseif string.find(t,"set layer") then
							layer = true 
						elseif string.find(t,"trigger") then 
							trigger = true
						end 
					end 
				else 
					del = true 
					prop = true 
					layer = true 
					trigger = true 
				end 
				if not del then 
					local b = ui:addButton(nil,0,0,2,2)
					b:setText("Delete")
					b.OnClick = function()
						local col = t:getColision()
						if col._OnRemove then col._OnRemove() end
						self.shapeCollider:remove(col)
						t:remove()
						menu:remove()
					end 
					menu:addItem(b)
				end 
				if not layer then 
					local b = ui:addButton(nil,0,0,2,2)
					b:setText("Set Layer")
					b.OnClick = function()
						menu:remove()
						self:pickLayer(function(zmap) t.zmap = zmap 
						print(zmap)
						if t.setZmap then 
							t:setZmap(zmap)
						end
						end)
					end 
					menu:addItem(b)
				end
				if not trigger then 
					local sw,sh = love.graphics.getDimensions()
					b = ui:addButton(nil,0,0,2,2)
					b:setText("Add Trigger")
					b.OnClick = function()
						menu:remove()
						local mode 
						if self._activeFrame then self._activeFrame:remove() end 
						local wah,hah = 250,300
						local f = ui:addFrame(nil,sw/2,sh/2,wah,hah)
						f.dragEnabled = true 
						f.color = basicFrameColor
						self._activeFrame = f
						local close = ui:addButton(f,2,2,20)
						close:setText("X")
						close.OnClick = function()
							if self._activeFrame then self._activeFrame:remove() end 
						end 
						local list = ui:addList(f,2,22,wah-4,50)
						list.color = colors.gray
						local ob = {}
						local w,h = wah-4,hah - (22+52 + 22)
						local textinput = ui:addTextinput(f,2,32 + 42,w,h) 
						
						for i,v in ipairs(self.triggerAdmin:getModes()) do
							local b = ui:addButton(nil,0,0,wah-4,20)
							b:setText(v)
							b.OnClick = function()
								for i,v in ipairs(t.__triggers or {}) do 
									if v.mode == b.text then 
										textinput:setText(v.TextFunction)
									end 
								end 
								for m,k in ipairs(ob) do 
									k:setInactive(false)
								end 
								b:setInactive(true)
								mode = v 
							end 
							if i == 1 then 
								b.OnClick() 
							end 
							list:addItem(b)
							table.insert(ob,b)
						end 
						textinput:setFree()
						textinput.multiline = true
						local b = ui:addButton(f,2,hah - 22,wah-4,20)
						b:setText("Apply")
						b.OnClick = function()
							local fn = textinput:getText()
							local _ = self.triggerAdmin:addTrigger(t)
							local s = _:setActiv(mode,fn)
							for i,v in ipairs(t.__triggers or {}) do 
								if v.mode == mode then 
									table.remove(t.__triggers,i)
								end 
							end 
							if s then 
								if not t.__triggers then t.__triggers = {} end 
								table.insert(t.__triggers,_)
							end 
						end 
					end 
					menu:addItem(b)
					d = ui:addButton(nil,0,0,2,2)
					menu:addItem(d)
					d:setText("Remove Trigger")
					d.OnClick = function()
						menu:remove()
						if self._activeFrame then self._activeFrame:remove() end 
						local wah,hah = 150,200
						local f = ui:addFrame(nil,sw/2,sh/2,wah,hah)
						self._activeFrame = f 
						local close = ui:addButton(f,2,2,20)
						close:setText("X")
						close.OnClick = function()
							if self._activeFrame then self._activeFrame:remove() end 
						end 
						local list = ui:addList(f,2,20,wah -4,hah - 22)
						local function makeList()
							list:clear()
							for i,v in ipairs(t.__triggers or {}) do 
								local _ = ui:addButton(nil,0,0,wah-4,20)
								_:setText(v.mode)
								_.OnClick = function()
									removeFromTable(t.__triggers,v)
									makeList()
								end 
								list:addItem(_)
							end 
						end 
						makeList()
					end 
				end 
				if not prop then 
					local b = ui:addButton(nil,0,0,2,2)
					b:setText("Properties")
					b.OnClick = function()
							menu:remove()
							local frame
							local function properties()
								if not frame then 
									if self._activeFrame then self._activeFrame:remove() end 
									local items = {name = t.__name or "",x = t.dx,y = t.dy,zmap = t.zmap,color = t.color}
									local font = ui:getFont()
									local maxW = 0
									local maxH = 0
									for i,v in pairs(items) do 
										local w = math.max(font:getWidth(i..": "),font:getWidth("255,255,255"))
										if maxW < w then 
											maxW = w 
										end 
										maxH = maxH + font:getHeight() 
									end 
									if maxW < 50 then 
										maxW = 50 
									end 
									maxW = maxW
									maxH = maxH/2 + 20
									local w,h = love.graphics.getDimensions()
									frame = ui:addFrame(nil,w/2,h/2,(maxW*2) + 10,maxH + 80)
									self._activeFrame = frame 
									
									frame.color = colors.gray
									local w,h =  maxW,maxH/#items
									local no = 1
									local txt = {}
									local ti 
									for i,v in pairs(items) do 
										local t = i.." : "..tostring(v).."\n"
										if i == "color" then 
											local r,g,b = unpack(v)
											t = i.." : "..r.." "..g.." "..b.."\n"
										end 
										if i == "name" then 
											t = i..": ".."\n"
											local fw = ui:getFont():getWidth(t)
											local fh = ui:getFont():getHeight()
											local w,h = maxW,fh
											ti = ui:addTextinput(frame,fw + 2,fh*(no-1),w,h)
											ti:setText(tostring(v))
										end 
										no = no + 1
										table.insert(txt,t)
									end
									frame.OnDraw = function()
										love.graphics.print(table.concat(txt),frame.x - frame.width/2,frame.y - frame.height/2)
									end 

									local w,h = maxW,30
									local b = ui:addButton(frame,maxW/2,maxH + 75 - h,w,h)
									b:setText("Done")
									b.OnClick = function()
										local tx = ti:getText()
										if tx and tx ~= "" then 
											t.__NAME = tx
											t.__name = tx 
											self._G[tx] = t
											self.triggerAdmin:setEnviroment(self._G)
										end 
										frame:remove()
										frame = nil
									end
								end 		
							end 
						properties()
					end 
					menu:addItem(b)
				end  
			end 
		end	
	end
end 
function mapAdmin.mousereleased()
end 
function mapAdmin.updateCol(self,mxb)
	local aob = self.aTarget
	local lhold,rhold = self:getLeftClick()

	if self.creatingItem then
		self.aTarget = self.creatingItem
		self.aTarget.zmap = self.current_zmap
		if (lhold or rhold) and not self.ui.activeObject then 
			self.creatingItem = nil
			self.activeClamp = nil 
			self.aTarget = nil 
		else 
			local x,y = mxb:center()
			self.creatingItem.x = x - (self.creatingItem.modx or 0)
			self.creatingItem.y = y - (self.creatingItem.mody or 0)
		end
	end
	if self.aTarget then
		local cond = love.mouse.isDown("l")
		if lhold ~= nil or rhold ~= nil then 
			cond = lhold or rhold
		end 
		local x,y = mxb:center()
		if cond and not self.aTarget.static then 
			if self.aTarget._nx then 
				self.aTarget.x = (self.aTarget._nx) + x
			end 
			if self.aTarget._ny then 
				self.aTarget.y = (self.aTarget._ny) + y
			end 
		else 
			self.aTarget._nx = nil
			self.aTarget._ny = nil
		end 	
		if not cond then 
			if not mxb:collidesWith(self.aTarget:getColision()) then
				if self.aTarget.OnFocusLost and not self.aTarget.inactive then  self.aTarget.OnFocusLost(self.aTarget) end 
				self.aTarget._HasFocus = false 
				self.aTarget.___done = false
				self.aTarget = nil
			end 
		end 
	end
end 
function mapAdmin.shapeCol(self,mxb,dt,a,b)
	local lhold,rhold = self:getLeftClick()
	local colision = self.all_active
	local activeModule 
	local activeImage 
	if not lhold and not rhold then 
		local other
		if a == mxb then
			other = b
		elseif b == mxb then
			other = a
		end
		if other then
			for i,v in pairs(colision) do
				local c = v:getColision()
				if other == c and not c._inactive then
					if (v.zmap or 1) == self.current_zmap then 
						local clamp  
						local src = v
						if self.aTarget and self.aTarget ~= src and not self.aTarget.inactive then
							if (self.aTarget.zmap or 1) < (src.zmap or 1) then
								if self.aTarget.OnFocusLost then
									self.aTarget.OnFocusLost(self.aTarget)
								end
								self.aTarget._HasFocus = false 
								self.aTarget = nil
							end
						end
						if not src.inactive and not src.permaInactive and not self.aTarget  then
							self.aTarget = src
							if not src.___done then 
								if src.OnFocus then src.OnFocus(src) end 
								src.___done = true
							end 
							src._HasFocus = true
						end
					end
				end 
			end
		end
	end
	if self.aTarget then 
		local c = self.aTarget:getColision()
		if c._module then 
			activeModule = self.aTarget
		elseif c.editorShape and c.type == "_image" then 
			activeImage = self.aTarget
		end 
	end 
end 
function mapAdmin.shapeStop(self,mxb,dt,a,b)
	local lhold,rhold = self:getLeftClick()
	local colision = self.all_active
	if not lhold and not rhold then 
		local other
		if a == mxb then
			other = b
		elseif b == mxb then
			other = a
		end
		if other then
			-- for i,v in pairs(colision) do
				-- if other == v:getColision() then
					-- if self.aTarget then
						-- if self.aTarget.OnFocusLost and not self.aTarget.inactive then  self.aTarget.OnFocusLost(self.aTarget) end 
						-- self.aTarget._HasFocus = false 
						-- self.aTarget.___done = false
						-- self.aTarget = nil
					-- end
				-- end
			-- end
		end 
	end
end 
function mapAdmin.clear(collider,self)
	self._G = {}
	local ac,pas = collider._active_shapes,collider._passive_shapes
	local a = mapAdmin.parse(ac,pas)
	for i=0,tableLenght(a) do 
		local v = a[i] 
		if v then 
			if v.remove then v:remove() else print("Failed to remove: ",v.name or v.type)end 
		end 
	end 
	if self.tool then 
		if self.makingT == "line" then
			self.target:remove()
		end
		self.tool = false
		self.cursor = nil
		self.makingT = false
		self.targetClamp = false
	end 
	self.imageAdmin:clear()
	self.moduleAdmin:clear()
	self.soundAdmin:clear()
	
	self.skybox = nil 
	self.current_zmap = 1 
	self.layers:clear()
	self.shapeCollider:clear()
	
	if self.UiShapes then self.UiShapes.mxb = self.shapeCollider:addPoint(1,1,1) end 
	if self.refreshImageList then self:refreshImageList() end 
	if self.refreshLayers then self.refreshLayers() end
	self.textureManager:clear()
end 	
function mapAdmin.save(self,name,items,force)
	local map = string.gsub(name,"maps/","",500)
	map = "maps/"..map
	local msg = "Failed to save: "..name
	
	if love.filesystem.exists(map) then 
		msg = "Saved over: "..map
	else 
		love.filesystem.createDirectory(map)
	end 
	
	local source = self.map
	if source ~= nil and not string.find(source,"maps/") then
		source = "maps/"..self.map
		if string.find(source,"map/resources/") then
			source = "resources/"
		end
	end
	
	print("Saving: ",map)
	print("Source: ",source)
	
	love.filesystem.createDirectory(map.."/image")
	love.filesystem.createDirectory(map.."/modules")
	love.filesystem.createDirectory(map.."/sounds")
	love.filesystem.createDirectory(map.."/children")
	love.filesystem.createDirectory(map.."/spritesheets")
	
	local itemsSaved = 0 
	local imagesSaved = 0 
	
	
	local general = {}
	local text = {} 
	for i,v in ipairs(items) do 
		if v.type == "shape" then 
			local t,i = v:unpack()
			itemsSaved = itemsSaved + 1
			if i then 
				t.image_source = {
					sheet = i.sheet,
					quad = i.quad,
				}
			end 
			if v.__triggers then 
				t.__triggers = {}
				for i,v in ipairs(v.__triggers) do 
					t.__triggers[i] = v:save()
				end 
			end 
			if v.__name then 
				t.__name = v.__name 
			end 
			table.insert(general,t)
		elseif v.type == "text" then 
			local t = v:save()
			if v.__triggers then 
				t.__triggers = {}
				for i,v in ipairs(v.__triggers) do 
					t.__triggers[i] = v:save()
				end 
			end 
			if v.__name then 
				t.__name = v.__name 
			end 
			table.insert(text,t)
		end 
	end 
	if #text ~= 0 then 
		love.filesystem.write(map.."/text",Tserial.pack(text))
	end 
	general.images = self.imageAdmin:save()
	local parents = getParents(map)
	local s = love.filesystem.write(map.."/general",Tserial.pack(general))
	if not s then 	
		if not love.filesystem.exists(map) then print("Map doesn't exist:"..map) end 
		msg = "FAILED TO SAVE TO FILESYSTEM"
	end 
	
	local misc = {
		mode = self.mode,
		ambient_color = self.ambient_color,
	} 
	if self.scene_admin then 
		misc.sceneData = self.scene_admin:save()
	end 

	love.filesystem.write(map.."/misc",Tserial.pack(misc))
	
	love.filesystem.write(map.."/layers",Tserial.pack(self.layers:save()))
	
	self.textureManager:save(self.map,parents)
	love.filesystem.write(map.."/mod",Tserial.pack(self.moduleAdmin:save(map,nil,parents))) 
	love.filesystem.write(map.."/random",Tserial.pack(self.random.saveInstances(self.instances or {})))
	if self.OnLoaded then 
		for i,v in ipairs(self.OnLoaded) do 
			local data = {}
			if love.filesystem.exists(v.map) then 
				if love.filesystem.exists(v.map.."/OnLoaded") then 
					data = Tserial.unpack(love.filesystem.read(v.map.."/OnLoaded"))
				end 
				local t = {target = v.target,
				type = v.type}
				data[v.name] = t 
				love.filesystem.write(v.map.."/OnLoaded",Tserial.pack(data))
			else 
				print("Does not exist: "..v.map)
			end
		end 
	end 
	return msg
end 
function mapAdmin.saveModules(self,name,func)
	local map = string.gsub(name,"maps/","",500)
	map = "maps/"..map
	if love.filesystem.exists(map) then 
		msg = "Saved over: "..map
	else 
		love.filesystem.createDirectory(map)
	end 
	moduleCopy = self.moduleMan:new(self,nil,self.shapeCollider)
	moduleCopy:load(name,Tserial.unpack(love.filesystem.read(name.."/mod")))
	if func then 
		func(moduleCopy:getInstances())
	end 
	local func = function(v) 
	if v._oneLoad then 
		return true
		end 	
	end
	moduleCopy:clear() 
end 

function mapAdmin.load(self,file)
	Fonts = {} 
	self._G = {}
	getFonts("")
	getFonts(eCurrentMap)
	mapAdmin.clear(self.shapeCollider,self)
	local s,b = self.soundAdmin:unpack()
	assert(b,"No bgm")
	self._G["SFX"] = s 
	self._G["BGM"] = b
	local file = file or self.map
	self.map = string.gsub(file,"maps/","")
	self.map = string.gsub(file,"//","")
	eCurrentMap = self.map
	file = self.map
	self.current_map = self.map
	local lfs = love.filesystem
	 
	print("Loading: "..file)
	local fs = love.filesystem
	local e = self 
	local env = e.triggerAdmin:getEnviroment()
	local dir = e.map.."/scripts"
	local parents = getParents(file)
	
	if fs.exists(dir) then 
		for i,v in ipairs(fs.getDirectoryItems(dir)) do 
			local _,t = pcall(fs.load,dir.."/"..v)
				if _ and t then 
				print("Global: ",self._G)
				env._G = self._G
				env.eCurrentMap = self.map
				setfenv(t,env)()
				print("LOADED: "..v)
			end 
		end 
	end
	for i,v in ipairs(parents) do 
		for i,k in ipairs(fs.getDirectoryItems(v.."/scripts")) do 
			local _,t = pcall(fs.load,v.."/scripts/"..k)
			if _ and t then 
				print("Global: ",self._G)
				env._G = self._G
				env.eCurrentMap = self.map
				setfenv(t,env)()
				print("LOADED: "..k)
			end
		end 
	end 
	e.triggerAdmin:setEnviroment(self._G)
	if not love.filesystem.exists(file) then 
		print("Map Does not Exist!!!")
	end 
	if not love.filesystem.exists(file.."/layers") then 
		local t = love.filesystem.getRealDirectory(file) or "" 
		error("Corrupted map: "..tostring(t.."/"..file))
	end 
	self.textureManager:load(file,parents)
	self.layers:load(Tserial.unpack(lfs.read(file.."/layers")))
	
	local general = Tserial.unpack(lfs.read(file.."/general"))
	print("General Items:",tableLenght(general))
	local function make(a,b)
		a.color = b.color
		a.zmap = b.zmap
		a.rotation = b.rotation
		a.sx = b.sx 
		a.sy = b.sy
		local is = b.image_source
		if b.sound then 
			local sn = b.sound 
			local snd = self.soundAdmin
			snd:addSoundEffect(a,file.."/sounds/",sn.name)
		end 
		if is then 
			local s = self.textureManager:seek(is.sheet)
			local q = s:seek(is.quad)
			if s and q then 
				assert(q,"Failed to find sprite: ",is.quad)
				print(s.name,q.name)
				a:setTexture(s,q)
			else 
				print("Failed to find: ",is.sheet,is.quad)
			end 
		end 
		local layer = self.layers:seek(a.zmap)
		if layer.colision then 
			a:makeColision(true)
		end 
		if b.__triggers then 
			a.__triggers = {} 
			for i,v in ipairs(b.__triggers) do 
				local t = self.triggerAdmin:addTrigger(a)
				t:load(v)
				table.insert(a.__triggers,t)
			end 
		end 
		if b.__name then 
			self._G[b.__name] = a
			a.__name = b.__name
		end 
	end 
	for i,v in pairs(general) do 
		local ty = v.type 
		if ty == "rectangle" then 
			local a = self.shapeAdmin:newRectangle(nil,v.x - v.width/2,v.y - v.height/2,v.width,v.height)
			make(a,v)
		elseif ty == "circle" then 
			local a = self.shapeAdmin:newCircle(nil,v.x,v.y,v.radius)
			make(a,v)
		elseif ty == "polygon" then 
			local a = self.shapeAdmin:newPolygon(nil,v.x,v.y,v.points)
			make(a,v)
		end 
	end 
	if lfs.exists(file.."/text") then 
		self.textAdmin:load(Tserial.unpack(lfs.read(file.."/text")))
	end 
	self.imageAdmin:load(general.images,self.textureManager,self)
	
	local misc = Tserial.unpack(lfs.read(file.."/misc"))
	local t = {} 

	self.mode = misc.mode 
	self.ambient_color = misc.ambient_color
	if self.mode == "battle" then 
		local t = misc.sceneData
		local team1,team2 
		if self._Stack then 
			team1 = self._Stack.team1
			team2 = self._Stack.team2 
		end 
		self.scene_admin = scene_edtior:new(self,t,team1,team2)
	end 
	
	if lfs.exists(self.map.."/skybox.png") then 
		local editor = self 
		local img = love.graphics.newImage(editor.map.."/skybox.png")
		img:setWrap("repeat","repeat")
		local quad = love.graphics.newQuad(0,0,editor.width*editor.scale,editor.height*editor.scale,img:getDimensions())
		local sk = 
		{
			draw = function(...)
				love.graphics.draw(img,quad, ...)
			end,
			update = function()
				quad:setViewport(0,0,editor.width/editor.scale,editor.height/editor.scale,img:getDimensions())
			end 
		}
		self.skybox = sk
	else 
		self.skybox = nil 
	end 
	self.moduleAdmin:load(file,Tserial.unpack(lfs.read(file.."/mod")),parents)
	if lfs.exists(file.."/random") then 
		self.random.loadInstances(Tserial.unpack(lfs.read(file.."/random")),self)
	end
	if self.refreshImageList then self:refreshImageList() end 
	_,self.current_zmap = self.layers:getRange()
	if self.refreshLayers then self.refreshLayers() end
	
	self.triggerAdmin:setEnviroment(self._G)
	if love.filesystem.exists(file.."/OnLoaded") then
		self.OnLoaded =  Tserial.unpack(love.filesystem.read(file.."/OnLoaded"))  
	end 
	if not self.OnLoaded then self.OnLoaded = {} end 
	 for i,v in pairs(self.OnLoaded) do 
		if v.type == "zmap" then
			local target = i 
			local t = self._G[target]
			local p = self._G["Player"] or self._G["player"] 
			print("Cannot find target: "..target)
			if t and p then 
				p:moveTo(t.x,t.y)
			end 
		end
	 end
	if Gamestate.current() == Editor then 
		Gamestate.current():remakeCamera()
	end 
end
return mapAdmin