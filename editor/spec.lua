function createSpecFrame(self,item,table1)
	local ilist = self.s_resource[item] 
	local frame = loveframes.Create("frame")
	local mc = loveframes.Create("multichoice",frame)
	mc:AddChoice("resources")
	mc:AddChoice(self.map)
	mc:SetPos(10,30)
	frame:SetSize(400,400)
	frame:SetName(""..item.."s")
	local list = loveframes.Create("list",frame)
	local w = frame.width
	local h = frame.height
	list:SetPos(10,60)
	list:SetSpacing(5)
	list:SetSize(frame.width - 100,frame.height - 100)
	mc.OnChoiceSelected = function(...)
		list:Clear()
		ilist = recordFolderNames(self.map.."/special/"..item)
		for i in ipairs(ilist) do
			local src = ilist[i]
			local img = love.graphics.newImage(self.map.."/special/"..item.."/"..src.."/image/image.png")
			local button = loveframes.Create("imagebutton")
			local w2 = img:getWidth()
			local h2 = img:getHeight()
			if h2 > h then
				h2 = h2/h
			else
				h2 = 1
			end
			if w2 > w then
				w2 = w2/w
			else
				w2 = 1
			end
			local img = crop(img,0,0,img:getWidth(),img:getHeight(),w2,h2)
			button:SetImage(img)
			button:SetText(src)
			button:SizeToImage()
			list:AddItem(button)
			button.OnClick = function(object)
				if not self.makingT then
					local platforma = self.map.."/special/"..item.."/"..src.."/src.lua"
					local dir = self.map.."/special/"..item.."/"..src
					local x = self.x - self.width/2
					local y = self.y - self.height/2
					local n = loadfile(love.filesystem.getRealDirectory(platforma).."/"..platforma)(self.map,dir,x,y,false,false)
					if self.player then
						zmap = self.player[1].zmap
					end
					n.zmap = zmap
					n.name = src
					self.targetClamp = true
					self.makingT = "generic"
					self.target = n
					table.insert(table1,n)
				end
			end
		end
	end
	for i in ipairs(ilist) do
		local src = ilist[i]
		local img = love.graphics.newImage("resources/special/"..item.."/"..src.."/image/image.png")
		local button = loveframes.Create("imagebutton")
		local w2 = img:getWidth()
		local h2 = img:getHeight()
		if h2 > h then
			h2 = h2/h
		else
			h2 = 1
		end
		if w2 > w then
			w2 = w2/w
		else
			w2 = 1
		end
		local img = crop(img,0,0,img:getWidth(),img:getHeight(),w2,h2)
		button:SetImage(img)
		button:SetText(src)
		button:SizeToImage()
		list:AddItem(button)
		button.OnClick = function(object)
			if not self.makingT then
				local ser = "resources/"
				local platforma = "resources/special/"..item.."/"..src.."/src.lua"
				local dir = "resources/special/"..item.."/"..src
				local x = self.x - self.width/2
				local y = self.y - self.height/2
				local n = love.filesystem.load(platforma)(ser,dir,x,y,false,false)
				local zmap = 1
				if self.player then
					zmap = self.player[1].zmap
				end
				n.zmap = zmap
				n.name = src
				self.targetClamp = true
				self.makingT = "generic"
				self.target = n
				table.insert(table1,n)
			end
		end
	end
end