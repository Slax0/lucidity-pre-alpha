local soundAdmin = {} 

function soundAdmin:new(editor)
	local c = {
		channels = {},
		parent = editor,
	}
	setmetatable(c,{__index = soundAdmin})
	c.sfx = c:addChannel()
	c.bgm = c:addChannel("bgm")
	return c 
end 
function soundAdmin:unpack()
	return self.sfx,self.bgm 
end 
function soundAdmin:scanDirs()
	local items = {} 
	local function registerItem(dir,name)
		
	end 
	local fs = love.filesystem
	local dir = eCurrentMap.."/sounds/"
	for i,v in ipairs(fs.getDirectoryItems(dir)) do
		items[v] = dir 
		print("Found:",v,dir)
	end 
	local parents = getParents(eCurrentMap)
	for i,v in ipairs(parents) do 
		for m,k in ipairs(fs.getDirectoryItems(v.."/sounds/")) do
			items[k] = v.."/sounds/"
		end 
	end 
	return items 
end 
function soundAdmin:pickSound(fn)
	local salf = self.parent
	local ui = salf.ui
	local dir = "/resources/"

	if self.soundFrame then 
		self.soundFrame:remove()
	end 
	local frame = ui:addFrame("Modules:",salf.width/2,salf.height/2,200,400)
	self.soundFrame = frame 
	frame:setHeader()
	frame.dragEnabled = true
	
	local close = ui:addButton(frame,frame.width - 20,0,20,20)
	close.text = "X"
	close.OnClick = function()
		self.soundFrame:remove()
		self.soundFrame = nil
	end 
	local list = ui:addList(frame,2,20,frame.width - 4,frame.height - 22 - 50)
	local function getFiles(dir)
		list:clear()
		if dir == "resources/" then 
			for i,v in ipairs(love.filesystem.getDirectoryItems("resources/sounds/")) do 
				list:addChoice(v,function()
					fn(v,"resources/sounds/"..v)
				end)
			end 
		else 
			local p = getParents(eCurrentMap)
			for i,v in ipairs(p) do 
				local files = love.filesystem.getDirectoryItems(p.."/sounds/")
				for k,m in ipairs(files) do 
					list:addChoice(m,function()
						fn(m,v)
					end)
				end 
			end 
		end 
	end
	local function topLayer()
		list:clear()
		list:addChoice("resources/",function()
			getFiles("resources/")
		end)
		list:addChoice(eCurrentMap,function()
			getFiles()
		end)
	end
	topLayer()
	local back = ui:addButton(frame,2,frame.height - (2 + 25),frame.width -4,25)
	back:setText("Back")
	back.OnClick = function()
		topLayer()
	end 
end 
function soundAdmin:clear()
end 
local channel = {} 
function soundAdmin:addChannel(mode)
	local c = {
		mode = mode,
		volume = volume,
		current = nil,
		tracks = {},
		parent = self,
	}
	if mode == "bgm" then 
		c.que = {} 
		c.pos = 0 
	end 
	setmetatable(c,{__index = channel})
	table.insert(self.channels,c)
	return c 
end 
	function channel:setListener(shape)
		local setor = love.audio.setOrientation
		if shape then 
			local x,y = shape:getColision():center()
			self:setPosition(x,y)
			local sx,sy = shape.scalex or shape.sx or 1,shape.scaley or shape.sy or 1
			setor(sx,0,0,0,sy,0)
		end 
	end 
	function channel:update(dt)
		if self.mode == "bgm" then 
			self.volume = misc.bgmvol
		elseif self.mode == "vocals" then 
			self.volume = misc.voicevol
		else
			self.volume = misc.sfvol
		end 
		if self.que and self.que[self.pos] then 
			self.current = self.que[self.pos]
		elseif self.loop then 
			self.pos = 0 
		end 
		if self.current then
			self.current:play() 
		end 
	end 
	function channel:setPosition(x,y)
		love.audio.setPosition(x,y)
	end 
	function channel:add(name)
		table.insert(self.que,self.tracks[name])
	end 
	function channel:remove()
		for i,v in ipairs(self.tracks) do 
			v:remove()
		end 
	end 
	function channel:delete(name)
		if self.que then 
			if type(name) == "string" then 
				for i,v in ipairs(self.que) do 
					if v.name == name then 
						table.remove(self.que,i)
						v:stop()
					end 
				end
			elseif type(name) == "number" then 
				if self.que[name] then self.que[name]:remove() end 
				table.remove(self.que,name)
			else 
				error("Expected string or number, got: ",type(name))
			end 
		else 
			print("Channel is a SFX, has no que.")
		end 
	end 
	function channel:push(name)
		if self.trackList[name] then 
			self.current = self.trackList[name]
			self.current:play()
			if self.que then 
				if self.current then self.current:reset() end 
				for i,v in ipairs(self.que) do 
					if v.name == name then 
						table.remove(self.que,i)
						table.insert(self.que,v)
					end 
				end 
			end 
		else 
			print("Not found: ",name)
		end 
	end 
	function channel:pop()
		if self.current then self.current:reset() end 
		if self.que then 
			table.remove(self.que,self.que[#self.que])
		end 
		self.current = nil 
	end 
		local track = {} 
		function channel:addTrack(dir,file)
			if dir and not file then 
				file = dir 
				dir = nil 
			end 
			print("Looking for: ",file)
			local dir = dir 
			if not dir then 
				local items = self.parent:scanDirs()
				dir = items[file]
			end 
			local type 
			local relative = false 
			if self.que then 
				type = "stream"
			else 
				relative = true 
				type = "static"
			end 
			local data = love.audio.newSource(dir.."/"..file,type)
			data:setRelative(relative)
			local t = 
			{
				parent = self,
				dir = dir,
				name = file,
				source = data,
			
			}
			setmetatable(t,{__index = track})
			table.insert(self.tracks,t)
			return t 
		end
			function track:play()
				self.source:play()
			end 
			function track:reset()
				self.source:stop()
			end 
			function track:setVolume(v)
				self.source:setVolume((v/100)*(misc.mastervol/100))
			end 
			function track:setPos(x,y)
				self.source:setPosition(x,y)
			end 
			function track:getSource()
				return self.source 
			end 
			function track:setSource(src) 
				self.source = src
			end 
			function track:once()
				local s = self.source:clone()
				s:setVolume(((self.parent.volume or 50)/100)*((misc.mastervol or 50)/100))
				s:play()
			end 
			function track:update(dt)
				self.volume = self.parent.volume or misc.sfvol
				self:setVolume(self.volume)
				if self.shape then 
					local x,y = self.shape:getColision():center()
					self:setPos(x,y)
				end 
			end 
			function track:remove()
				self:reset()
				if self.shape then 
					if self.shape.removeTrack then self.shape:removeTrack() end 
				elseif self.parent.que then 
					local q = self.parent.que 
					for i,v in ipairs(q) do 
						if v == self then 
							table.remove(q,i)
						end 
					end 
				end 
			end 
			function track:save()
				if string.find(self.dir,"resources/") then 
					local d = love.filesystem.newFileData(self.dir.."/"..self.name)
					love.filesystem.write(eCurrentMap.."/sounds/"..self.name,d)
				end 
				return t 
			end 
function soundAdmin:update(dt)
	for i,v in ipairs(self.channels) do 
		v:update(dt)
	end 
end 

return soundAdmin