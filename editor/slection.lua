local selection = {} 
function selection:new(parent)
	local self = {
		ax = parent.mousepressedX,
		ay = parent.mousepressedY,
		zmap = parent.current_zmap,
		parent = parent,
		focus = true,
		type = "selection",
	} 
	setmetatable(self,{__index = selection})
	self:update()
	return self 
end
function selection:removeFocus()
	self.focus = false 
	self:getActiveItems()
end 
function selection:update()
local p = self.parent 
	if love.mouse.isDown("l") and self.focus and p.tool == "random" then 
		local x,y = p.mousepressedX,p.mousepressedY
		local mx,my = p.mx,p.my
		local w = math.max(findDistance(x,mx),10)
		local h = math.max(findDistance(y,my),10)
		local x,y = math.min(x,mx),math.min(y,my)
		if self.colision then 
			p.shapeCollider:remove(self.colision)
		end 
		self.colision = p.shapeCollider:addRectangle(x,y,w,h)
		self.colision.editorShape = self 
		self.x,self.y = x + w/2,y + h/2
	elseif self.colision then 
		if self.lock then
			local w,h = findDimensions(self.colision)
			self.cx,self.cy = incrementPos(self.x,self.y,w/2,h/2)
		end 
		self.colision:moveTo(self.cx or self.x,self.cy or self.y)
	end 
end
function selection:getActiveItems()
	local items = {} 
	for other in pairs(self.colision:neighbors()) do 
		if self.colision:collidesWith(other) then 
			local v = other
			local shp
			if v.editorShape then 
				shp = v.editorShape
			elseif v._module then 
				shp = v._module
			elseif v._sound then 
				shp = v._sound
			end 
			local x,y = self.colision:bbox()
			if shp then 
				table.insert(items,{
					source = shp,
					relativePos = {shp.x - x,shp.y - y},
				})
			end 
			self.colision.OnMove = function()
				local x,y = self.colision:bbox()
				for i,v in ipairs(self.items) do 
					v.source.x = x + v.relativePos[1]  
					v.source.y = y + v.relativePos[2]
				end 
			end 
		end 
	end 
	self.items = items 
	return items
end 
function selection:remove()
	local p = self.parent 
	p.shapeCollider:remove(self.colision)
end 
function selection:draw()
	local lw = love.graphics.getLineWidth()
	love.graphics.setLineWidth(5)
	setColor(255,0,255)
	self.colision:draw("line")
	setColor()
	love.graphics.setLineWidth(lw)
end 
function selection:OnMenu(menu)
	menu:addChoice("Delete",function()
		warning:new(self.parent.ui,"This will delete all items in the selection.\n Confirm?!",function()
			for i,v in ipairs(self.items or self:getActiveItems()) do 
				v.source:remove()
			end 
			self:remove()
		end)
	end)
	menu:addChoice("Clear",function()
		self:remove()
	end)
	if not self.lock then 
		menu:addChoice("Lock",function()
			self.lock = true 
		end)
	else 
		menu:addChoice("Free",function()
			self.lock = false 
			self.cx = nil 
			self.cy = nil
		end)
	end 
end
function selection:getColision()
	return self.colision
end 
return selection