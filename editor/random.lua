local rand = {} 

function rand:new(self,x,y,zmap)
	local c = {
		x = x,
		y = y,
		zmap = zmap,
		mode = "npc",
		rangex = 100,
		rangey = 100,
		chance = 100,
		items = {},
		parent = self,
		children = {},
		colider = self.shapeCollider
	}
	setmetatable(c,{__index = rand})
	if not self.instances then self.instances = {} end
	table.insert(self.instances,c)
	c.index = #self.instances
	c:makeColision()
	return c 
end
function rand:getNpcs(func)
	local mod = self.parent.moduleAdmin:seek("npc")
	if mod then 
		local availnpcs = mod.derivatives
		for i,v in ipairs(availnpcs) do 
			if func then func(func) end 
		end
	end 
end
function rand:makeColision()
	local col = self.colider
	if self.col then col:remove(self.col) end 
	local col = col:addRectangle(self.x,self.y,self.rangex,self.rangey)
	col.getUserData = function()
		return {source = self}
	end
	self.col = col 
	col.editorShape = self
end 
function rand:remove()
	if self.col then 
		self.colider:remove(self.col)
	end 
	for i,v in ipairs(self.children) do 
		if v.remove then v:remove() end 
	end 
	if Gamestate.current() == Editor then 
		for i,v in ipairs(self.parent.instances) do 
			table.remove(self.parent.instances,i)
		end 	
	end 
end 
function rand:update(dt)
	self.col:moveTo(self.x,self.y)
end 
function rand:draw()
	self.col:draw("line")
	local x,y = self.col:center()
	love.graphics.print("RANDOM "..self.mode,x - love.graphics.getFont():getWidth("RANDOM "..self.mode)/2,y)
end 
function rand:OnMenu(list,ui)
	list:addChoice("Add",function()
		
		local fw,fh = 700,300
		local frame = ui:addFrame("Add item with probability",love.graphics.getWidth()/2,love.graphics.getHeight()/2,fw,fh)
		frame.color = basicFrameColor 
		frame.dragEnabled = true 
		frame:addCloseButton()
		local text = {
			"Mode:",
			"rangex:",
			"rangey:",
			"probability:",
		}
		local maxw = 0 
		for i,v in ipairs(text) do 
			text[i] = v.."\n"
			local w = ui:getFont():getWidth(v)
			if w > maxw then 
				maxw = w 
			end
		end 
		local th = ui:getFont():getHeight()
		local lw,lh = (fw/3) - 5*3,fh-50
		for i,v in ipairs(text) do 
			local ti = ui:addTextinput(frame,maxw+10,25 + th*(i-1),lw-maxw,th)
			local t = text 
			if i == 1 then 
				ti:setText(self.mode)
			elseif i == 2 then 
				ti:setText(self.rangex)
			elseif i == 3 then 
				ti:setText(self.rangey)
			elseif i == 4 then 
				ti:setText(self.chance)
			end 
		end 
		frame.OnDraw = function()
			local x,y = frame.colision:bbox()
			love.graphics.print(table.concat(text),x+5,y+25)
		end 
		
		local items = ui:addList(frame,lw*2+20,25,lw,lh)
		local avail = ui:addList(frame,lw+15,25,lw,lh)
		local mod = self.parent.moduleAdmin:seek("npc")
		local function updateLists()
			items:clear()
			avail:clear()
			for i,v in ipairs(self.items) do 
				items:addChoice(v,function()
					table.remove(self.items,i)
					updateLists()
				end)
			end 
			local mod = self.parent.moduleAdmin:seek("npc")
			if mod then 
				for i,v in ipairs(mod.derivatives) do 
					avail:addChoice(v.name,function()
						table.insert(self.items,v.name)
						updateLists()
					end)
				end 
			end 
		end 
		updateLists()
		local done = ui:addButton(frame,fw/2,fh-20,60,20)
		done:setText("Done")
		done.OnClick = function()
			self:makeColision()
			frame:remove()
		end
		
	end)
	self:getNpcs()
end 
function rand:load(t)
	self.x = t.x 
	self.y = t.y
	self.mode = t.mode 
	self.rangex = t.rangex 
	self.rangey = t.rangey 
	self.items = t.items or {}
	self:makeColision()
	if Gamestate.current() == Editor then 

	else
		if self.mode == "npc" then 
			local mod = self.parent.moduleAdmin:seek("npc")
			local x,y = self:getColision():bbox()
			local x = math.random(x,x + self.rangex)
				for i,v in pairs(self.items) do 
					local n = mod:addInstance(v,x + math.random(0,10),y,self.zmap) 
					n.actual.LParent = self.index 
					table.insert(self.children,n)
					self.colider:remove(self:getColision())
				end 
		elseif self.mode == "event" then 
			
		elseif self.mode == "costum" then 
		end 
	end 
end
function rand:save()
	local t = {
		x = self.x ,
		y = self.y ,
		zmap = self.zmap,
		rangex = self.rangex,
		rangey = self.rangey,
		mode = self.mode, 
		items = self.items,
	}
	return t 
end 
function rand:getColision()
	return self.col
end
function rand.saveInstances(s)
	local t = {} 
	for i,v in ipairs(s) do 
		print(i)
		if s[i] then 
			table.insert(t,s[i]:save())
		end 
	end 
	return t 
end 
function rand.loadInstances(s,self)
	for i,v in ipairs(s) do
		if s[i] then 
			local n = self.random:new(self,0,0,self.current_zmap)
			n:load(s[i])
		end 
	end 
end 
return rand 