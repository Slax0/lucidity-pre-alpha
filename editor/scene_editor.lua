local e = {}
local battlenpc = require "battle_npc.init"
local TeamRedSelection = require("battle_npc.TeamOutline")
local mobCards = require "ghostcards"

function e:new(oldEditor,t,team1,team2)
	local s = {}
	local speed = 350
	
	s.parent = oldEditor 
	s.ui = oldEditor.ui 
	s.shapeColider = oldEditor.shapeCollider
	s.shapeUi = oldEditor.shapeUi
	local width,height = 1920,1080
	local px,py = width/800,height/600
	
	local rangex = 100*px
	local rangey = 100*py 
	
	s.team1 = {}
	s.team2 = {} 
	
	local function addLayer(name,px,py,col,sound)
		local mi,ma = oldEditor.layers:getRange()
		local zmap = ma + 1
		oldEditor.current_zmap = zmap
		local l = oldEditor.layers:addLayer(name or ("Layer: "..zmap),zmap,px or 0 ,py or 0 ,col or true,sound or false)
		l.locked = true
		if oldEditor.refreshLayers then oldEditor.refreshLayers() end
		return zmap 
	end 
	local gzmap,szmap,tzmap 
	if not t then 
		szmap = addLayer("skies")
		gzmap = addLayer("ground")
		tzmap = addLayer("Teams")
	else 
		gzmap = t.gzmap 
		szmap = t.szmap
		tzmap = t.tzmap 
	end 
	
	
	local self = s 
	
	self._Team1 = {}
	self._Team2 = {} 
	if not team1 and not  team2 then 
		for i = 1,5 do 
			local o =  math.random(1,2)
			local mob = "slime"
			if o == 1 then 
				mob = "Avictus"
			end 
			table.insert(s._Team1,{name = mob})
			local mob = loadGhost:new(mob)
			local a = battlenpc:new(mob)
			table.insert(s.team1,a)
		end 
		for i = 1,5 do 
			local o =  math.random(1,2)
			local mob = "slime"
			if o == 1 then 
				mob = "Avictus"
			end 
			table.insert(s._Team2,{name = mob})
			local mob = loadGhost:new(mob)
			local a = battlenpc:new(mob)
			table.insert(s.team2,a)
		end 
	else 
		s.team1 = team1 
		s.team2 = team2
	end 
	local colider = self.shapeColider
	local ui  	  = self.shapeUi
	local cardsOn = false
	local team1W  = 0
	local team1H  = 0
	local team2W  = 0
	local team2H  = 0
	
	for i,v in ipairs(self["team1"]) do
		team1W = team1W + v.width
		team1H = team1H + v.height
	end
	
	for i,v in ipairs(self["team2"]) do
		team2W = team2W + v.width
		team2H = team2H + v.height
	end
	

	
	
	local TeamSeperation = 150
	local totalNum = #self["team1"]
	local nw,nh = 300,height - 400
	local ox,oy = (1920/2) - TeamSeperation,nh
	
	local avalW = 260
	local avalH = 220
	
	local prcW = team1W/1
	local prcH = team1H/1
	
	local team1C = colider:addRectangle(ox - avalW,oy,avalW+ (self.team1[1].
	width*math.abs(self.team1[1].sx))/2,avalH + (self.team1[#self.team1].
	height*math.abs(self.team1[#self.team1].sy))/2)
	
	self.team1Ox,self.team1Oy = ox - avalW,oy
	
	for i,v in ipairs(self.team1) do 
		local sx = v.sx 
		local Wseperation = 0
		local Hseperation = 0
		local x = ox + Wseperation
		local y = oy + Hseperation
		if self["team1"][i-1] then 
			local item = self["team1"][i-1]
			local width,height = item:getDimensions()
			local lastw = avalW*((width)/prcW)
			local lasth = avalH*((height)/prcH)
			local lastX = item.bx
			local lastY = item.by
			x = lastX - (lastw)
			y = lastY + (lasth)
		end
		v.bx = x
		v.by = y
		v.x = x 
		v.y = y 
		local item = v 
		if ui then 
			local mwidth,mheight = v:getDimensions()
			local button = ui:addButton(nil,x,y,mwidth,mheight)
			button:setZmap(button.zmap + (#self.team1 + 1) - i)
			button.dontDraw = true
			button.help = function()
				mobCards(oldEditor.ui,v)
			end 
			v:setButton(button)
			v.bx = x
			v.by = y
			button.oldsx = v.sx 
			v.x = x 
			v.y = y 
			button.OnFocus = function() 
	
			end
			button.OnFocusLost = function()

			end 

			function button.OnUpdate(dt)
				v.button.x = v.x
				v.button.y = v.y
			end 
			button.OnClick = function(a,b,c,d,e)
				local width,height = 1920,1024
				local self = oldEditor
				for i,v in ipairs(self._CircleButtons) do 
					v.ondeactivate()
					v:setInactive(true)
				end 
				button:setInactive(true)
				self.activeMob = v
				if button.tween then
					Timer.cancel(button.tween)
				end
				for n,k in ipairs(self["team1"]) do
					if k ~= v then
						local button = k.button
						if button.tween then
							Timer.cancel(button.tween)
							k.moving = true
							k.button:setInactive(k.moving)
							local distance 
							if button.x > 0 then
								distance = k.x - k.bx
							else
								distance = k.x + k.bx
							end
							distance = math.abs(distance)
							k.sx = button.oldsx 
							k.state = "run"
							button.tween = Timer.tween(distance/speed,k,{x = k.bx},nil,function() 
								k.state = "idle"
								k.sx = -button.oldsx 
								k.moving = false
								if not d then
									k.button:setInactive(k.moving)
								end
							end)
						end
					end
				end
				if math.floor(v.x) ~= math.floor(width/2 - mwidth/2) and not e then
					v.state = "run"
					v.moving = true
					local mwidth,mheight = v:getDimensions()
					local distance = v.x - width/2 - mwidth/2
					if v.x < 0 then
						distance = v.x + width/2 - mwidth/2
					else
						distance = v.x - (width/2 - mwidth/2)
					end
					distance = math.abs(distance)
					button.tween = Timer.tween(distance/speed,v,{x = width/2 - mwidth/2},nil,function() 
						v.state = "idle"
						v.moving = false
						if not d then
							button:setInactive(v.moving)
						end
						if not cardsOn then 
							for i in ipairs(self.updateButtons) do
								local upb = self.updateButtons[i]
								
								if not upb.Pressed then upb:setInactive(false) upb.onactivate() end
							end	
						end 
					end)
				elseif not v.moving or e then
					v.state = "run"
					v.moving = true
					self.activeMob = nil
					local distance = v.x - v.bx
					if v.x > 0 then
						distance = v.x - v.bx
					else
						distance = v.x + (v.bx)
					end
					distance = math.abs(distance)
					button.tween = Timer.tween(distance/speed,v,{x = v.bx},nil,function() 
						v.state = "idle"
						v.moving = false
						if not d then
							button:setInactive(v.moving)
						end
						if not cardsOn then 
							for i in ipairs(self.updateButtons) do
								self.updateButtons[i]:setInactive(false)
								self.updateButtons[i].onactivate()
							end
						end 
					end)
				end
			end
		end 
		
		
		for k,n in ipairs(item.states) do
			if n.name == "idle" then
				n.anim:update(2)
				n.anim.position = math.abs(#n.anim.frames - i)
			end
		end
		v.ox = v.width/2
		v.oy = v.height/2
		v.sx = -sx 
	end 
	

	
	local ox,oy = (1920/2) + TeamSeperation,nh
	
	self.team2Ox,self.team2Oy = ox + avalW,oy
	
	local prcW = team2W/1
	local prcH = team2H/1
	
	for i,v in ipairs(self.team2) do
		local Wseperation = 0
		local Hseperation = 0
		local x = ox + Wseperation
		local y = oy + Hseperation
		if self["team2"][i-1] then 
			local item = self["team2"][i-1]
			local lastw = avalW*((item.width*math.abs(item.sx))/prcW)
			local lasth = avalH*((item.height*math.abs(item.sy))/prcH)
			local lastX = item.bx
			local lastY = item.by
			x = lastX + (lastw)
			y = lastY +(lasth)
		end
		local item = v
		v.bx = x
		v.by = y
		v.x = x 
		v.y = y 
		v.sx = v.sx 
		local width,height = v.width*math.abs(v.sx), v.height*math.abs(v.sy)
		
		v.ox = v.width/2
		v.oy = v.height/2
		if ui then 
			local button = ui:addButton(nil,x,y,width,height)
			button.oldsx = v.sx
			button.help = function()
				mobCards(oldEditor.ui,v,true)
			end 
			button:setZmap(button.zmap + (#self.team2 + 1) - i)
			function button.OnUpdate()
				v.button.x = v.x
				v.button.y = v.y
			end 
			button.dontDraw = true
			button.OnFocus2 = function() end 
			v:setButton(button)
		end 
		for i,v in ipairs(item.states) do
			if v.name == "idle" then
				v.anim:update(2)
				v.anim.position = math.abs(#v.anim.frames - i)
			end
		end
	end
	local team2C = colider:addRectangle(ox - self.team2[1].width/2,oy,avalW + self.team2[1].width/2,avalH + self.team2[#self.team2].height/2)
	
	local ox,oy = self.team1Ox - (20),self.team1Oy
	local captain1 = {
		col = colider:addRectangle(ox - 40,oy - 10,50,50),
		static = true,
		zmap = tzmap,
		ind = 1, 
	}
	local ox,oy = self.team2Ox + (20*px),self.team2Oy
	local captain2 = {
		col = colider:addRectangle(ox -40,oy - 10,50,50),
		static = true,
		zmap = tzmap,
		ind = 2, 
	}
	
	captain1.col._module = captain1 
	captain2.col._module = captain2
	
	local function setTexture(self,sheet,quad,textureMapper)
		local img = sheet:getImage()
		local texture = textureMapper:new(self,img,quad.actual)
		self.image_source = {
			sheet = sheet.name,
			quad = quad.name, 
			img = img,
		}
		self.sheet = sheet.name 
		self.quad =  quad.name 
		self.OnUpdate = function(dt)
			if self.texture then self.texture:update(dt) end 
		end 
		self.OnDraw = function()
			if self.texture then self.texture:draw() end 
		end 
		self.texture = texture 	
	end
	local function setTextureAlt(self,sheet,quad,textureManager) 
		self.image_source = {
			sheet = sheet.name,
			quad = quad.name, 
			img = img,
		}
		self.sheet = sheet.name 
		self.quad =  quad.name 
		local quad = quad.actual
		local _,_,iw,ih = quad:getViewport()
		local x,y = self:getColision():bbox()
		local w,h = findDimensions(self:getColision())
		y = y + h/2 
		x = x + w/2
		if type(quad) == "table" then
			local quad = quad:addInstance()
			self.OnDraw = function()
				quad:draw(x,y,0,self.sx,self.sy,iw/2,ih/2)
			end
			self.OnUpdate = function(dt)
				quad:update(dt)
			end 
		else 
			local img = sheet:getImage()
			self.OnDraw = function()
				love.graphics.draw(img,quad,x,y,0,self.sx,self.sy,iw/2,ih/2)
			end 
		end 
	end 
	local function removeTexture(self) 
		if self.texture then self.texture:remove()  self.texture = nil end 
		if self.image_source then self.image_source = nil end
	end 
	
	local captain = {sx = 1,sy = 1}
	
	
	local isEditor = (Gamestate.current() == Editor) 

	
	function captain:getColision()
		return self.col 
	end 
	function captain:draw()
		local _,_,_,a = love.graphics.getColor() 
		if a <= 50 then 
			a = 255
		end 
		local c = self.color or colors.white
		setColor(c[1],c[2],c[3],a)	
		
		setColor(colors.white)
		if self.OnDraw then 
			self.OnDraw()
		end 
		local x,y = self.col:bbox()
		local w,h = findDimensions(self.col)
		if self.ind == 1 then
			if oldEditor.TeamCaptain1 or s.team1[1] then 
				local s =  oldEditor.TeamCaptain1 or s.team1[1]
				local sx = -(s.sx or 1) 
				if s.type == "player" then 
					sx = (s.sx or 1) 
				end 
				for i,v in ipairs(s.states) do 
					if v.name == s.state then 
						v.anim:draw(x + w/2,y - (s.height/2 - h/2),0,sx,1,s.width/2,s.height/2)
					end 
				end 
			end 
		elseif self.ind == 2 then 
			local cap = oldEditor.TeamCaptain2
			if isEditor then
				cap = s.team2[1]
			end 
			if cap  then 
				local s =  cap
				local sx = (cap.sx or 1)  
				if s.type == "player" then 
					sx = -(cap.sx or 1) 
				end 
				for i,v in ipairs(s.states) do 
					if v.name == s.state then 
						v.anim:draw(x + w/2,y - (s.height/2 - h/2),0,sx,1,s.width/2,s.height/2)
					end 
				end 
			end 
		end 
	end 
	function captain:update(dt)
		if self.ind == 1 then
			if oldEditor.TeamCaptain1 then 
				local s =  oldEditor.TeamCaptain1
				for i,v in ipairs(s.states) do 
					if v.name == s.state then 
						v.anim:update(dt)
					end 
				end 
			end 
		elseif self.ind == 2 then 
			if oldEditor.TeamCaptain2 then 
				local s =  oldEditor.TeamCaptain2
				for i,v in ipairs(s.states) do 
					if v.name == s.state then 
						v.anim:update(dt)
					end 
				end 
			end 
		end
		if self.OnUpdate then 
			self.OnUpdate(dt)
		end 		
	end 
	function captain:removeTexture()
		removeTexture(self)
	end 
	function captain:OnMenu(menu)
		local ui = oldEditor.ui
		local a = ui:addButton(nil,0,0,2,2)
		a:setText("Set Texture")
		a.OnClick = function()
			removeTexture(self)
			oldEditor:pickImage(function(sheet,quad)
				self:setTexture(sheet,quad)
				 if oldEditor.pickImageFrame then 
					oldEditor.pickImageFrame:remove()
					oldEditor.pickImageFrame = nil 
				end 
			end)
		end 
		menu:addItem(a)
		menu:addChoice("Mirror",function()
			self.sx = -(self.sx or 1)
		end)
		menu:addChoice("Flip",function()
			self.sy = -(self.sy or 1)
		end)
	end 
	function captain:setTexture(sheet,quad)
		setTextureAlt(self,sheet,quad,oldEditor.textureMapper)
	end 
	function captain:save()
		local t = {}
		if self.quad or self.sheet then 
			t.quad = self.quad 
			t.sheet = self.sheet 
		end
		t.sx = self.sx 
		t.sy = self.sy
		return t 
	end 
	function captain:load(is)
		if  is and (is.quad or is.sheet) then 
			local s = oldEditor.textureManager:seek(is.sheet)
			print(is.sheet)
			local q = s:seek(is.quad)
			if s and q then 
				self:setTexture(s,q)
			else 
				print("Failed to find: ",is.sheet,is.quad)
			end 
			self.sx = is.sx or 1 
			self.sy = is.sy or 1
		end 
	end 
	function captain:remove()
		colider:remove(self.col)
	end 
	
	setmetatable(captain1,{__index = captain})
	setmetatable(captain2,{__index = captain})
	
	local areas = {
		
		team1 = team1C,
		team2 = team2C,
		
		captain1 = captain1.col,
		captain2 = captain2.col,
		
		ground = colider:addRectangle(-rangex,(height - 400),(width ) + rangex*2,500 + rangey),
		skies = colider:addRectangle(-rangex,-rangey,width + rangex*2,height + rangey + rangey/2),
		
		
	}
	self.areas = areas
	oldEditor.SAreas = areas
	
	
	local function standardUI(self,menu,ui,editor)
		local p = editor
		local hasSound =  p.layers:seek(p.current_zmap).sound
		if not hasSound  then 
			local a = ui:addButton(nil,0,0,2,2)
			a:setText("Set Texture")
			a.OnClick = function()
			removeTexture(self)
				editor:pickImage(function(sheet,quad)
					self:setTexture(sheet,quad)
					 if editor.pickImageFrame then 
						editor.pickImageFrame:remove()
						editor.pickImageFrame = nil 
					end 
				end)
			end 
			menu:addItem(a)
		else  
			local a = ui:addButton(nil,0,0,2,2)
			a:setText("Set Sound")
			a.OnClick = function()
			removeTexture(self)
				p:pickSound(function(dir,file) 
					local snd = p.soundAdmin
					snd:addSoundEffect(self,file,dir)
				end)
			end
			menu:addItem(a)
		end	
	end 
	local gm  = {} 
	function gm:update(dt)
		if s.b then 
			if ui then 
				if oldEditor.ui.activeObject then
					ui.activeObject = nil 
				end 			
			end 
		end 
		if self.OnUpdate then 
			self.OnUpdate(dt)
		end 
	end 
	function gm:draw()
		local _,_,_,a = love.graphics.getColor() 
		if a <= 50 then 
			a = 255
		end 
		local c = self.color
		setColor(c[1],c[2],c[3],a)	
		
		self.col:draw("fill")
		
		setColor(colors.white)
		if self.OnDraw then 
			self.OnDraw()
		end 
	end 
	function gm:OnRightClick(menu)
		standardUI(self,menu,oldEditor.ui,oldEditor)
	end 
	function gm:setTexture(sheet,quad)
		setTexture(self,sheet,quad,oldEditor.textureMapper)
	end 
	function gm:getColision()
		return self.col 
	end 
	function gm:save()
		local t = {}
		if self.quad or self.sheet then 
			t.quad = self.quad 
			t.sheet = self.sheet 
		end
		return t 
	end 
	function gm:load(is)
		if  is and (is.quad or is.sheet) then 
			local s = oldEditor.textureManager:seek(is.sheet)
			print(is.sheet)
			local q = s:seek(is.quad)
			if s and q then 
				self:setTexture(s,q)
			else 
				print("Failed to find: ",is.sheet,is.quad)
			end 
		end 
	end 
	function gm:remove()
		colider:remove(self.col)
	end
	function gm:removeTexture()
		removeTexture(self)
	end 
	areas.ground._module = {
		col = areas.ground,
		static  = true,
		zmap = gzmap,
		b = true,
		color = {56,195,55}
	} 
	areas.skies._module = {
		col = areas.skies,
		static = true,
		zmap = szmap, 
		color = {116,151,255}
	}
	self.saving = {} 
	table.insert(self.saving,areas.ground._module)
	table.insert(self.saving,areas.skies._module)
	table.insert(self.saving,areas.captain1._module)
	table.insert(self.saving,areas.captain2._module)
	
	
	setmetatable(areas.ground._module,{__index = gm})
	setmetatable(areas.skies._module,{__index = gm})
	if t then 
		for i,v in ipairs(self.saving) do 
			v:load(t.mod[i])
		end 
	end 
	
	
	local tweenSel 
	if ui then 
		tweenSel = TeamRedSelection:init(nil,0,0,20,20)
		oldEditor._Areas = areas
	end 
	local team = {}
	local team1 = {
		col = areas.team1,
		team = self.team1,
		zmap = tzmap,
		static = true,
		ind = 1,
	}
	
	local team2 = {
		col = areas.team2,
		team = self.team2,
		zmap = tzmap,
		static = true, 
		ind = 2,
	}
	
	areas.team1._module = team1 
	areas.team2._module = team2 
	function team:draw() 
	end 
	local lastActive 
	local curActive
	local no = 0 
	function team:update(dt)
		if self.ind == 1 then 
			local cf = oldEditor.CardFunctions
			if cf and cf.update then 
				cf.update(dt)
			end 
		end 
		for i,v in ipairs(self.team) do 
			v:update(dt)
		end 
		if oldEditor.SelectingArea then 
			if ui then 
				if ui:getmxb():collidesWith(self:getColision()) then 
					self.Active = true 
					curActive = self.ind
				else 
					no = no + 1
					self.Active = false  
				end 
			end
			if self.ind == 1 then 
				if no == 2 then 
					curActive = nil 
				end 
				if lastActive ~= curActive then 
					local function s(shape)
						local x,y = shape:bbox()
						local w,h = findDimensions(shape)
						tweenSel:setPoints(x,y,x + w,y + h)
					end 
					if curActive == 1 then 
						s(areas.team1._module:getColision())
					elseif curActive == 2 then 
						s(areas.team2._module:getColision())
					end 
					lastActive = curActive
				end
				if curActive then 
					tweenSel:update(dt)
				end 
			end 
		end 
	end 
	function team:getColision()
		return self.col 
	end 
	function team:OnMenu(menu)
	end 
	function team:remove()
		colider:remove(self.col)
	end 
	
	setmetatable(areas.team2._module,{__index = team})
	setmetatable(areas.team1._module,{__index = team})
	function areas.team1._module:draw()
		local cf = oldEditor.CardFunctions
		if oldEditor.SelectingArea then 
			if curActive == 2 then 
				oldEditor._InTeam2  = true
				oldEditor._InTeam1	= false
			elseif curActive == 1 then 
				oldEditor._InTeam1  = true
				oldEditor._InTeam2  = false 
			else 
				oldEditor._InTeam1 = nil 
				oldEditor._InTeam2 = nil
			end 
			if curActive then 
				tweenSel:draw()
				oldEditor._AreaForTeam = tweenSel
			else 	
				oldEditor._AreaForTeam = nil 
			end 
		end 
		if cf and cf.drawBefore then 
			cf.drawBefore()
		end 
		for i,v in ipairs(s.team2) do 
			if not v.dead then 
				v:drawHealth()
			end 
		end 
		for i,v in ipairs(s.team1) do 
			if not v.dead then 
				v:drawHealth()
			end 
		end 
		for i,v in ipairs(s.team2) do 
			if not v.dead then 
				
				v:draw() 
			end 
		end
		for i,v in ipairs(s.team1) do 
			if not v.dead then 
				v:draw() 
			end 
		end
		if cf and cf.draw then 
			cf.draw()
		end 
	end	
	
	setmetatable(s,{__index = e})
	if oldEditor.refreshLayers then oldEditor.refreshLayers() end
	return s 
end 
function e:save()
	local t = {
		gzmap = self.areas.ground._module.zmap, 
		szmap = self.areas.skies._module.zmap, 
		tzmap = self.areas.team1._module.zmap, 
		mod = {} 
	} 
	for i,v in ipairs(self.saving) do 
		local c = v:save()
		table.insert(t.mod,c)
	end 
	return t 
end  
function e:play()
	local player = loadGhost:new("Avictus")
	local player = battlenpc:new(player)
	
	local cap2 = loadGhost:new("Avictus")
	local cap2 = battlenpc:new(cap2)
	local stack = {
		self._Team2,
		player,
		captain2 = cap2,
		Editor = true
	}
	local olmap = self.parent.map 
	stack.OnFinish = function()
		Gamestate.switch(Editor,{map = olmap})
	end 
	stack.map = self.parent.map
	stack.team1 = self._Team1
	
	Gamestate.switch(battle,stack)
end 
return e 