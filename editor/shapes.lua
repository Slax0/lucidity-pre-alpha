local shapeAdmin = {}
local function makeShape(self,shape,polygon)
	self.Modx = 0 
	self.Mody = 0 
	
	local phyWorld = self.parent.phyWorld
	
	if phyWorld and shape then 
		if self.physicsData then 
			self.physicsData.body:destory()
			clearTable(self.physicsData)
		else 
			self.physicsData = {}
		end 
		
		local w,h = findDimensions(self.colision)
		local x,y = incrementPos(self.x,self.y,w/2,h/2)
		local body
		if polygon then 
			x,y = x,y
		end 		
		if type(shape) == "table" then
			body = love.physics.newBody(phyWorld,x,y,"static")
			local fixtures = {}
			for i,v in ipairs(shape) do
				local fixture = love.physics.newFixture(body,v)
				fixture:setUserData({source = parent,part = "STATIC"})
				table.insert(fixtures,fixture)
			end
			fixtures = fixture
		else
			body = love.physics.newBody(phyWorld,x,y,"static")
			fixture = love.physics.newFixture(body,shape)
			fixture:setUserData({source = parent,part = "STATIC"})
		end
		body:setAngle(self.rotation or 0)
		self.physicsData.body = body 
		self.physicsData.fixture = fixture or fixtures
		self.physicsData.shape = shape
	end
end 
local function onCopy(self)
	local im = self.TData
	local s,q = unpack(im or {})
	if im then self:setTexture(s,q) end 
end 
local function setTexture(self,sheet,quad,textureMapper)
	local textureMapper = textureMapper or self.parent.parent.textureMapper
	local textureManager = self.parent.parent.textureManager
	if not sheet.getImage then 
		sheet = textureManager:seek(sheet)
		quad = sheet:seek(quad)
	end 
	local img = sheet:getImage()
	self.TData = {sheet.name,quad.name}
	local texture = textureMapper:new(self,img,quad.actual)
	self.image_source = {
		sheet = sheet.name,
		quad = quad.name, 
		img = img,
	}
	self.OnUpdate = function(dt)
		if self.texture then self.texture:update(dt) end 
	end 
	self.OnDraw = function()
		setColor(self.color)
		if self.texture then self.texture:draw(self.sx,self.sy) end 
		setColor()
	end
	self.texture = texture 	
end
local function setSound(self,soundObj) 
	if self.sound then self.sound:remove() end 
	self.sound = soundObj
end 
local function removeSound(self)
	if self.sound then self.sound:remove() end
	self.sound = nil 
end 
local function standardDraw(self,debug)
	if self.drawBefore then self.drawBefore() end 
	if not self.texture then 
		local lwd 
		local m 
		if self.sound then m = "line" lwd = love.graphics.getLineWidth() love.graphics.setLineWidth(4) setColor(colors.green) end 
		local _,_,_,a2 = love.graphics.getColor()
		local r,g,b,a = unpack(self.color)
		if a2 ~= 255 then 
			a = math.min((a or 255),(a2 or 255))
		end 
		setColor(r,g,b,a)
		self.colision:draw(m or self.mode)
		setColor(colors.white)
		if lwd then love.graphics.setLineWidth(lwd) setColor(colors.white) end 
	end
	if self.OnDraw then self.OnDraw() end 
	
	if self.physicsData and debug then 
		setColor(colors.green)
		local body = self.physicsData.body 
		local shape = self.physicsData.shape
		
		if type(shape) == "table" then 
			for i,v in pairs(shape) do 
				love.graphics.polygon("line",body:getWorldPoints(v:getPoints()))
			end 
		elseif shape.getPoints then 
			love.graphics.polygon("line",body:getWorldPoints(shape:getPoints()))
		else 
			local x,y = body:getWorldPoints(shape:getPoint())
			love.graphics.circle("line",x,y, shape:getRadius())
		end 
		setColor(colors.white)
	end 
end 
function standardRemove(self)
	self.parent.collider:remove(self.colision)
	if self.sound then self.sound:remove() end
	for i in ipairs(self) do
		self[i] = nil
	end
	if self.physicsData then 
		if not self.physicsData.body:isDestroyed() then self.physicsData.body:destroy() end 
		clearTable(self.physicsData)
	end 
end 
local function removeTexture(self) 
	if self.texture then self.texture:remove()  self.texture = nil end 
	if self.image_source then self.image_source = nil end
end 
local function standardUI(self,menu,ui,editor)
	local p = editor
	local hasSound =  p.layers:seek(p.current_zmap).sound
	
	local a = ui:addButton(nil,0,0,2,2)
	a:setText("Change color")
	
	a.OnClick = function()
		local a = {
			"1",
			"2",
			"3",
			"4",
			"5",
			"6",
			"7",
			"8",
			"9",
			"0",
		}
		local r,g,b = unpack(self.color)
		local w,h = love.graphics.getDimensions()
		local texW = ui:getFont():getWidth("255") + 20
		local texH = ui:getFont():getHeight()
		
		local f = ui:addFrame("Set Color",w/2,h/2,240,texH +30)
		f:setHeader()
		f:addCloseButton()
		f.dragEnabled = true 
		
		local function limiter(s)
			if string.len(s.text) > 3 then 
				s.text = string.sub(s.text,1,3)
				s.indicator.textPos = string.len(s.text)
			end 
		end 
		
		local colorinp = ui:addTextinput(f,5,25,230,texH)
		local color = self.color[1]..","..self.color[2]..","..self.color[3]..","
		colorinp:setText(color)
		colorinp.OnChange = function(t)
			self.color = parsecolor(t)
		end 
		
	end
	menu:addItem(a)
	if not hasSound  then 
		local a = ui:addButton(nil,0,0,2,2)
		a:setText("Set Texture")
		a.OnClick = function()
		removeTexture(self)
			editor:pickImage(function(sheet,quad)
				self:setTexture(sheet,quad)
			end)
		end 
		menu:addItem(a)
	else  
		local a = ui:addButton(nil,0,0,2,2)
		a:setText("Set Sound")
		a.OnClick = function()
		removeTexture(self)
			p:pickSound(function(file,dir) 
				local snd = p.soundAdmin
				local c = c.sfx
				self:setSound(c:addTrack(file,self,dir))
			end)
		end
		menu:addItem(a)
	end	
	if  self.texture then 
		local a = ui:addButton(nil,0,0,2,2)
		a:setText("mirror texture.")
		a.OnClick = function()
			self.sx = -(self.sx or 1)
		end 
		menu:addItem(a)
		local a = ui:addButton(nil,0,0,2,2)
		a:setText("flip texture.")
		a.OnClick = function()
			self.sy = -(self.sy or 1)
		end 
		menu:addItem(a)
	end 
end  
function shapeAdmin:new(parent,Collider,ui,ui2,phyWorld)
	self = {
		collider = Collider,
		ui = ui,
		ui2 = ui2,
		parent = parent,
		phyWorld = phyWorld,
	}
	return setmetatable(self,{__index = shapeAdmin})
end 
local function standardCol(self)
end 
local function checkVar(var)
	if var and var ~= 0 then 
		return true
	end 
end 
local function standardUpdate(self,dt)
	local width = self.width 
	local height = self.height 
	if not width or not height then 
		local x,y = self.colision:bbox()
		local x1,y1 = self.colision:center()
		local w = findDistance(x,x1)
		local h = findDistance(y,y1)
		width = w*2
		height = h*2
	end 
	local w,h = (width)/2,(height)/2
	self.topX,self.topY = incrementPos(self.x,self.y,w,h)
	self.dx = self.topX + (self.modx or 0) + (self.Modx or 0) 
	self.dy = self.topY + (self.mody or 0) + (self.Mody or 0)
	self.colision:moveTo(self.dx,self.dy)
	if self.physicsData then 
		if checkVar(self.modx) or checkVar(self.mody) or checkVar(self.Modx) or checkVar(self.Mody) then 
			self.physicsData.body:setPosition(self.dx,self.dy)
		end 
	end 
	if self.OnUpdate then self.OnUpdate(dt) end 
end 
local function standardSetMod(self)
	if self.colision then 
		local width = self.width 
		local height = self.height 
		if not width or not height then 
			local x,y = self.colision:bbox()
			local x1,y1 = self.colision:center()
			local w = findDistance(x,x1)
			local h = findDistance(y,y1)
			width = w*2
			height = h*2
		end 
		local w,h = (width)/2,(height)/2
		self.topX,self.topY = incrementPos(self.x,self.y,w,h)
		self.dx = self.topX + (self.modx or 0) + (self.Modx or 0) 
		self.dy = self.topY + (self.mody or 0) + (self.Mody or 0)
		self.colision:moveTo(self.dx,self.dy)
		if self.physicsData then 
			if checkVar(self.modx) or checkVar(self.mody) or checkVar(self.Modx) or checkVar(self.Mody) then 
				self.physicsData.body:setPosition(self.dx,self.dy)
			end 
		end 
	end 
end
local makeRectangle = {}
makeRectangle.__index = makeRectangle

function shapeAdmin:newRectangle(mode,x,y,width,height,color)
	local object = {
		x = x or 0,
		y = y or 0,
		mode = "fill" or "fill",
		width = width or 1,
		height = height or 1,
		rotation = 0,
		zmap = 1,
		type = "shape",
		parent = self,
		stype = "rectangle",
		color = color or {255,255,255,255}
	}
	setmetatable(object,{__index = makeRectangle })
	object:makeColision()
	return object
end
	function makeRectangle:unpack()
		local tables = {
			x = self.x + self.width/2,
			y = self.y + self.height/2,
			sx = self.sx, 
			sy = self.sy,
			width = self.width,
			height = self.height,
			zmap = self.zmap,
			color = self.color,
			type = self.stype,
		}
		if self.sound then tables.sound = self.sound:save(self.parent.parent.map) end 
		local img = self.image_source
		return tables,img
	end 
	function makeRectangle:OnCopy()
		onCopy(self)
	end
	function makeRectangle:setSound(obj)
		setSound(self,obj)
	end 
	function makeRectangle:setMod(px,py) 
		self.modx = px 
		self.mody = py
		standardSetMod(self)
	end 
	function makeRectangle:getColision()
		return self.colision
	end
	function makeRectangle:setWidth(width)
		if width ~= 0 then
			self.width = width
			self:makeColision()
		end
	end
	function makeRectangle:setHeight(height)
		if height ~= 0 then
			self.height = height
			self:makeColision()
		end
	end
	function makeRectangle:setRotation(r)
		self.rotation = r
	end
	function makeRectangle:draw(debug)
		standardDraw(self,debug) 
	end
	function makeRectangle:setPointOfCreation(x,y)
		self.pocx = x 
		self.pocy = y 
	end 
	function makeRectangle:makeColision(d)
		if self.colision then
			self.parent.collider:remove(self.colision)
		end 
		if self.pocx or self.pocy then 
			self.x = self.pocx + self.width/2 
			self.y = self.pocy + self.height/2 
		end 
		self.colision = self.parent.collider:addRectangle(self.x ,self.y,self.width,self.height)
		self.colision:setRotation(self.rotation or 0)
		self.colision.shape_source = self
		self.colision.editorShape = self
		self.colision.zmap = self.zmap
		self.colision.userData = "shape"
		self.colision.subUserData = "rectangle"
		self.parent.collider:setPassive(self.colision)
		standardCol(self)
		local phyWorld = self.parent.phyWorld 
		local width = self.width 
		local height = self.height
		if  d then
			local x1,y1 = width/2,height/2
			local x2 = -(width/2)
			local y2 = -(height/2)
			local points = {
				x1,y1,
				x1,y2,
				x2,y2,
				x2,y1,
				
			}
			local shape = love.physics.newPolygonShape(unpack(points))
			makeShape(self,shape)
		else 
			makeShape(self)
		end 
	end
	function makeRectangle:remove()
		standardRemove(self)
	end
	function makeRectangle:setColor(color)
		self.color = color
	end
	function makeRectangle:OnRightClick(menu,ui,editor)
		standardUI(self,menu,ui,editor)
	end
	function makeRectangle:setTexture(sheet,quad)
		print("Test: ",sheet,quad)
		setTexture(self,sheet,quad,self.parent.parent.textureMapper)
	end 
	function makeRectangle:update(dt)
		standardUpdate(self,dt)
	end
	function makeRectangle:removeTexture()
		removeTexture(self)
	end 
local makeCircle = {}
makeCircle.__index = makeCircle
function shapeAdmin:newCircle(mode,x,y,r,color)
		local object = {
		x = x or 0,
		y = y or 0,
		mode = mode or "fill",
		type = "shape",
		stype = "circle",
		parent = self,
		r = r or 1,
		rotation = 0,
		zmap = 1,
		color = color or {255,255,255,255}
	}

	setmetatable(object,{__index = makeCircle })
	object:makeColision()
	return object
end
function makeCircle:unpack()
	local tables = {
		x = self.x,
		y = self.y,
		sx = self.sx,
		sy = self.sy,
		radius = self.r,
		zmap = self.zmap,
		color = self.color,
		type = self.stype,
	}
	if self.sound then tables.sound = self.sound:save(self.parent.parent.map) end 
	local img = self.image_source
	return tables,img
end 
	function makeCircle:OnCopy()
		onCopy(self)
	end
	function makeCircle:setSound(o)
		setSound(self,o)
	end 
	function makeCircle:setMod(px,py) 
		self.modx = px 
		self.mody = py
		standardSetMod(self)
	end 
	function makeCircle:getColision()
		return self.colision
	end 
function makeCircle:setRadius(r)
	self.r = r
	self:makeColision()
end
function makeCircle:removeTexture()
		removeTexture(self)
end 
function makeCircle:update(dt)
	standardUpdate(self,dt)
end
function makeCircle:setTexture(sheet,quad)
	setTexture(self,sheet,quad,self.parent.parent.textureMapper)
end 
function makeCircle:draw(debug)
	standardDraw(self,debug)
end
function makeCircle:makeColision(d)
	if self.colision then
		self.parent.collider:remove(self.colision)
	end
	self.colision = self.parent.collider:addCircle(self.x,self.y,self.r)
	self.parent.collider:setPassive(self.colision)
	self.colision.source = self
	self.colision.editorShape = self
	self.colision.zmap = self.zmap
	self.colision.userData = "shape"
	self.colision.subUserData = "circle"
	standardCol(self)
	if d then 
		local shape = love.physics.newCircleShape(self.r)
		makeShape(self,shape)
	end 
end
function makeCircle:remove()
	self.parent.collider:remove(self.colision)
	if self.sound then self.sound:remove() end
	for i in ipairs(self) do
		self[i] = nil
	end
end
function makeCircle:OnRightClick(menu,ui,editor)
	standardUI(self,menu,ui,editor)
end
local makePolygon = {}
makePolygon.__index = makePolygon
local vector = require("hardon.vector-light")
local function toVertexList(vertices, x,y, ...)
	if not (x and y) then return vertices end -- no more arguments

	vertices[#vertices + 1] = {x = x, y = y}   -- set vertex
	return toVertexList(vertices, ...)         -- recurse
end
local function areCollinear(p, q, r, eps)
	return math.abs(vector.det(q.x-p.x, q.y-p.y,  r.x-p.x,r.y-p.y)) <= (eps or 1e-32)
end
local function onSameSide(a,b, c,d)
	local px, py = d.x-c.x, d.y-c.y
	local l = vector.det(px,py,  a.x-c.x, a.y-c.y)
	local m = vector.det(px,py,  b.x-c.x, b.y-c.y)
	return l*m >= 0
end
local function segmentsInterset(a,b, p,q)
	return not (onSameSide(a,b, p,q) or onSameSide(p,q, a,b))
end
local function isIntersecting(vertices)
	local q,p = vertices[#vertices]
	for i = 1,#vertices-2 do
		p, q = q, vertices[i]
		for k = i+1,#vertices-1 do
			local a,b = vertices[k], vertices[k+1]
			 if segmentsInterset(p,q, a,b) then
				return true
			 end
		end
	end
end
function shapeAdmin:newPolygon(mode,x,y,points,color)
		local object = {
		x = x,
		y = y,
		mode = mode or "fill",
		rotation = 0,
		zmap = 1,
		parent = self,
		color = color or {255,255,255,255},
		points = points,
		stype = "polygon",
		type = "shape",
	}
	if self.sound then tables.sound = self.sound:save(self.parent.parent.map) end 
	local vertices = toVertexList({},unpack(object.points))
	if tableLenght(object.points) < 5 then
		print("Not enough points! :",tableLenght(object.points))
		return false
	elseif isIntersecting(vertices) then
		print("Intersects!")
		return false
	else
		if not love.math.isConvex(object.points) then
			object.triangles = pcall(love.math.triangulate,object.points)
			if not object.triangles then
				return false
			else
				object.triangles = love.math.triangulate(object.points)
			end
		end
	end
	setmetatable(object,{__index = makePolygon })
	object:makeColision()
	local x,y = object.colision:bbox()
	local x1,y1 = object.colision:center()
	local w = findDistance(x,x1)
	local h = findDistance(y,y1)
	object.width = w*2 
	object.height = h*2 
	object:update()
	return object 
end
	function makePolygon:OnCopy()
		onCopy(self)
	end
	function makePolygon:removeTexture()
		removeTexture(self)
	end
	function makePolygon:setSound(o)
		setSound(self,o)
	end 
	function makePolygon:getColision()
		return self.colision
	end 
function makePolygon:update(dt)
	standardUpdate(self,dt)
end
function makePolygon:setTexture(sheet,quad)
	setTexture(self,sheet,quad,self.parent.parent.textureMapper)
end 
function makePolygon:setMod(px,py) 
	self.modx = px
	self.mody = py 
	standardSetMod(self)
end 
function makePolygon:unpack()
	local tables = {
		x = self.x,
		y = self.y,
		sx = self.sx, 
		sy = self.sy,
		rotation = self.rotation,
		zmap = self.zmap,
		color = self.color,
		points = self.points,
		type = self.stype,
	}
	if self.sound then tables.sound = self.sound:save(self.parent.parent.map) end 
	local img = self.image_source
	return tables,img
end 

function makePolygon:OnRightClick(menu,ui,editor)
	standardUI(self,menu,ui,editor)
end
function makePolygon:prop()
end
function makePolygon:setPoints(points)
	self.points = points
	self:makeColision()
end
function makePolygon:makeColision(d)
	if self.colision then
		self.parent.collider:remove(self.colision)
	end
	self.colision = self.parent.collider:addPolygon(unpack(self.points))
	if self.colision then
		self.colision.editorShape = self
		self.colision.zmap = self.zmap
		self.colision.userData = "shape"
		self.colision.subUserData = "polygon"
		self.parent.collider:setPassive(self.colision)
	else
		self:remove()
	end
	self.colision:moveTo(self.x,self.y)
	standardCol(self)
	if d then 
		local points = deepCopy2(self.points)
		local w,h = findDimensions(self.colision)
		for i,v in ipairs(points) do 
			if i %2 ~= 0 then 
				points[i] = points[i] - w/2
				points[i+1] = points[i+1] -h/2
			end 
		end 
		local shape = pcall(love.physics.newPolygonShape,unpack(points))
		local nPoints = {} 
		if not love.math.isConvex(points) then 
			shape = {}
			local triangles = love.math.triangulate(points)
			for i,v in ipairs(triangles) do
				shape[i] = love.physics.newPolygonShape(unpack(v))
			end
		else
			shape = love.physics.newPolygonShape(unpack(points))
		end
		makeShape(self,shape,true)
	end 
end
function makePolygon:remove()
	if self.colision then
		self.parent.collider:remove(self.colision)
	end
	if self.sound then self.sound:remove() end
	for i in ipairs(self) do
		self[i] = nil
	end
end
function makePolygon:draw(debug)
	standardDraw(self,debug)
end
local makeLine = {}
makeLine.__index = makeLine
function shapeAdmin:newLine(color,x,y)
	local object = {
		x = {},
		y = {},
		mode = mode or "fill",
		rotation = 0,
		points = {},
		zmap = 1,
		parent = self,
		colision = {},
		color = color or {255,255,255,255},
		aPoints = {},
		uiItems = {},
		stype = "line",
		dontDraw = true,
	}
	object.temp = {}
	table.insert(object.points,x)
	table.insert(object.points,y)
	
	setmetatable(object,{__index = makeLine })
	return object
end
function makeLine:addPoint(x,y)
	if not self.moving and not self.making then 
		if self.colision then
			if self.pointChanged then
				self.points = {}
				for i in ipairs(self.colision) do
					local x,y = self.colision[i]:center()
					table.insert(self.points,x)
					table.insert(self.points,y)
				end
				self.pointChanged = nil
			end
		end
		table.insert(self.points,x)
		table.insert(self.points,y)
		self:makeColision()
	end 
end
function makeLine:addTempPoint(x,y)
	self.temp.x = x
	self.temp.y = y
	if not self.tempColision then
		self.tempColision = self.parent.collider:addRectangle(x,y,10,10)
	end
	self.tempColision:moveTo(x,y)
end
function makeLine:update(key,key2)
	self.making = false
	if self.tempColision then 
		for i,v in ipairs(self.colision) do 
			if self.tempColision:collidesWith(v) then 
				if not self.moving then 
					v.color = colors.yellow
					if key2 then 
						self.making = true
						if not self.intersect and not self.tooSmall and self.OnMake then self.OnMake(self) end
					end 
				else 
					v.color = colors.blue
				end
			else 
				v.color = colors.green
			end 
		end 
		if key then 
			if self.moving then 
				self.pointChanged = true 
				self.moving = false
			else 
				self.pointChanged = true 
				self.moving = true
			end 		
		end 
	end 
end 
function makeLine:remove()
	for i in ipairs(self.colision) do
		self.parent.collider:remove(self.colision[i])
	end
	for i,v in pairs(self.uiItems) do 
		v:remove()
	end 
	for i in ipairs(self) do
		self[i] = nil
	end
	self.parent.collider:remove(self.tempColision)
end
function makeLine:makeSilent(x)
	if x then
		self.silent = x
	else
		if self.silent then
			self.silent = false
		else
			self.silent = true
		end
	end
end
function makeLine:draw()
	if not self.silent then
		local oldw = love.graphics.getLineWidth()
		love.graphics.setLineWidth(3)
		local points = self.aPoints

		self:checkPoints()
		
		love.graphics.setColor(unpack(self.color))
			if self.intersect then 
				setColor(colors.red)
			end
		if #points >= 4 then love.graphics.line(unpack(points)) end
		if tableLenght(self.x) >= 2 then
			for i in ipairs(self.x) do
				if self.x[i + 1] ~= nil then
					love.graphics.line(self.x[i],self.y[i],self.x[i + 1],self.y[i + 1])
				end
			end
		end
		local x,y = points[#points -1],points[#points]
		
		if self.tempColision then 
			local lx,ly = self.tempColision:center()
			love.graphics.line(x,y,lx,ly)
			if self.moving then 
				love.graphics.circle("line",lx,ly,20)
			end 
		end 
		
		for i in ipairs(self.colision) do
			love.graphics.setColor(unpack(self.colision[i].color))
			if self.intersect then 
				setColor(colors.red)
			end 
			self.colision[i]:draw("fill")
		end
		love.graphics.setColor(255,0,0)
		if self.tempColision then
			self.tempColision:draw("fill")
		end
		love.graphics.setColor(255,255,255)
		love.graphics.setLineWidth(oldw)
	end
end
function makeLine:makeColision(ui)
	if self.colision then
		if self.pointChanged then
			self.points = {}
			for i in ipairs(self.colision) do
				local x,y = self.colision[i]:center()
				table.insert(self.points,x)
				table.insert(self.points,y)
			end
			self.pointChanged = nil
		end
		for i in ipairs(self.colision) do
			self.parent.collider:remove(self.colision[i])
		end
	end
	local x = {}
	local y = {}

	for i in ipairs(self.points) do
		if i % 2 == 0 then
			table.insert(y,self.points[i])
		else
			table.insert(x,self.points[i])
		end
	end 
	for i in ipairs(x) do
		self.colision[i] = self.parent.collider:addRectangle(x[i],y[i],10,10)
		self.colision[i].subUserData = "point"
		self.colision[i].color = colors.green
		self.colision[i].point = i

	end
	for i,v in ipairs(self.colision) do 
		if self.colision[i].uiItem then self.colision[i].uiItem:remove() end 
		local frame = self.parent.ui:addFrame(self.colision[i])
		frame.shape = self
		frame.OnDraw = function() self:draw() end
		frame.dontDraw = true 
		frame.dragEnabled = true
		frame:setZmap(self.zmap)
		frame.OnUpdate = function() self.colision[i]:moveTo(frame.x,frame.y) end
		self.colision[i].uiItem = frame
		table.insert(self.uiItems,frame)
	end	
end
function makeLine:checkPoints()
	self.intersect = false
	for i,v in pairs(self.aPoints) do 
		self.aPoints[i] = nil
	end 
	for i,v in ipairs(self.colision) do 
		local x,y = v:center()
		table.insert(self.aPoints,x)
		table.insert(self.aPoints,y)
	end
	local p = deepCopy2(self.aPoints)
	if self.tempColision then 
		local x,y = self.tempColision:center()
		table.insert(p,x)
		table.insert(p,y)
	end 
	local r 
	local vertices = toVertexList({},unpack(p))
	if tableLenght(self.aPoints) < 5 then
		self.tooSmall = true 
	else
		self.tooSmall = false
	end 
	if isIntersecting(vertices) then
		self.intersect = true 
	else 
		self.intersect = false
	end 
	if not self.tooSmall and not love.math.isConvex(self.aPoints) then
		triangles = pcall(love.math.triangulate,self.aPoints)
		if not triangles then
			self.intersect = true 
		end
	end
end 
function makeLine:returnPoints()
	local points = {}
	local points2 = {}
	local px = {}
	local py = {}
	for i,v in pairs(self.aPoints) do 
		self.aPoints[i] = nil
	end 
	for i,v in ipairs(self.colision) do 
		local x,y = v:center()
		table.insert(self.aPoints,x)
		table.insert(self.aPoints,y)
	end 
	for i,v in ipairs(self.colision) do 
		self.colision[i].uiItem:remove()
	end 
	for i,v in pairs(self.uiItems) do 
		v:remove()
	end 
	for i,v in ipairs(self.aPoints) do 
		if i %2 == 0 then 
			table.insert(py,v)
		else 
			table.insert(px,v)
		end 
	end 
	
	local x = math.min(unpack(px))
	local y = math.min(unpack(py))
	for i in ipairs(px) do
		table.insert(points2,px[i])
		table.insert(points2,py[i])
	end
	for i in ipairs(px) do
		table.insert(points,px[i] - x)
		table.insert(points,py[i] - y)
	end
	
	self:remove()
	local col = self.parent.collider:addPolygon(unpack(points2))
	local x,y = col:center()
	self.parent.collider:remove(col)
	return points,x,y
end
return shapeAdmin