local mainMenu = {}
local mapAdmin = require ("editor/mapAdmin")
local t = require("editor/textures")
local scene_edtior = require("editor/scene_editor")
function fileStructure(map)
	local cd = love.filesystem.createDirectory
	cd(map.."/children/")
	cd(map.."/image/")
	cd(map.."/sounds/")
	cd(map.."/spritesheets/")
end
local textures,textureManager,makeTexture = t[1],t[2],t[3]
function mainMenu:new(salf,ui,OnOpen)
	local screen_w = love.graphics.getWidth()
	local screen_h = love.graphics.getHeight()
	local openButton = ui:addButton(nil,screen_w/2,0,200,20)
	local function saveAs(force)
		if self.activeFrame then 
			self.activeFrame:remove()
		end 
		local map = salf.map 
		local askForName = force 
		if map == "maps/new" or map == "new" or map == "maps/" or map == "" then 
			askForName = true 
		end 
		if askForName then 
			local w,h = 250,70
			local frame = salf.ui:addFrame(nil,salf.width/2,salf.height/2,w,h)
			
			local text = salf.ui:addTextinput(frame,2,2,w - 5,30)
			text:setText("Enter map name")
			local done = salf.ui:addButton(frame,2,h - 48 + 15,90,30)
			done:setText("Save")
			done.OnClick = function()
				local txt = text:getText()
				local v = Gamestate.current().map 
				local v = v:sub(1,v:len()-1)
				local s,e = v:match'^.*()//' or v:match'^.*()/'
				local t = string.sub(v,1,s-1)
				print(t,txt)
				if string.find(v,"/children/") then 
				end 
				txt = t.."/"..txt 
				local valid = true
				if txt == "maps" then 
					valid = false 
					text:setText("Map name cannot be 'maps' !")
				elseif txt == "" then 
					txt = "new"
				end 
				if valid then 
					salf:save(txt)
					self.activeFrame:remove()
					self.activeFrame = nil
				end					
			end 
			local close = salf.ui:addButton(frame,w - 48 - 90/2,h - 48 + 15,90,30)
			close:setText("Close")
			close.OnClick = function()
				self.activeFrame:remove()
				self.activeFrame = nil
			end 
			self.activeFrame = frame
		else 
			salf:save()
		end 
	end 
	local function loads()
		if self.activeFrame then 
			self.activeFrame:remove()
			self.activeFrame = nil 
		end
		local frame = salf.ui:addFrame(nil,salf.width/2,salf.height/2,200,400)
		self.activeFrame = frame 
		local items = {}
		local font = love.graphics.getFont()
		local dir = love.filesystem.getSaveDirectory()
		local files = love.filesystem.getDirectoryItems("/maps/")
		for i,v in ipairs(files) do 
			local a = {}
			local w,h = font:getWidth(files[i]),font:getHeight() 
			a.w = frame.width - 5
			a.h = 40
			table.insert(items,a)
		end 
		local amenu 
		items.sort = function(i,b)
			b.text = files[i]
			b.OnClick = function() if amenu then amenu:remove() end 
				salf:load("maps/"..files[i])
				if self.activeFrame then 
					self.activeFrame:remove()
					self.activeFrame = nil 
				end
			end
			b.OnRightClick = function() 
				if amenu then amenu:remove() end 
				local menu = ui:addMenu(nil,b.x + b.width/2 + 20,b.y)
				amenu = menu 
				local dir = "maps/"..files[i].."/children/"
				local parents = love.filesystem.getDirectoryItems(dir)
				for i,v in ipairs(parents) do 
					menu:addChoice(v,function() salf:load(dir..v) menu:remove() 
						self.activeFrame:remove()
						self.activeFrame = nil 
					end)
				end
				menu:refresh()
				menu.mainFrame.x = b.x +b.width/2 + menu.width/2
			end 
		end 
		local list = salf.ui:addList(frame,2,2,196,350,items)
		local close = salf.ui:addButton(frame,2,400 - 45,frame.width - 4,40)
		close.text = "Close"
		close.OnClick = function()
			if amenu then amenu:remove() end 
			self.activeFrame:remove()
			self.activeFrame = nil
		end
	end 
	local function bgm()
		local frame = ui:addFrame("BGM Tracks:",salf.width/2,salf.height/2,200,130)
		frame.color = basicFrameColor
		frame.dragEnabled = true
		frame:setHeader()
		local activeTrack 
		local snd = salf.soundAdmin
		local function updateBGMList(list)
			list:clear()
			local tracks = snd.sndBgm
			for i,v in ipairs(tracks) do 
				local b = ui:addButton(nil,0,0,180,20)
				b:setText(v.name)
				b.OnClick = function()
					activeTrack = v
				end 
				list:addItem(b)
			end 
		end 
		local list = ui:addList(frame,2,20,180,80)
		
		local n = ui:addButton(frame,2,105,20,20)
		n:setText("N")
		n.OnClick = function()
			local p = salf
			p:pickSound(function(dir,file) 
				local snd = p.soundAdmin
				snd:addBGM(file,dir)
				updateBGMList(list)
			end)
		end 
		local d = ui:addButton(frame,24,105,20,20)
		d:setText("D")
		d.OnClick = function()
			if activeTrack then activeTrack:remove() end
			updateBGMList(list)
		end 
		
		local play = ui:addButton(frame,46,105,150,20)
		play:setText("Play/stop")
		play.OnClick = function()
			snd.bgm:pPlay()
		end 
		updateBGMList(list)
	end 
	local function mapName(map)
		local invalidchars = {
			"\a",
			"\b",	
			"\f",	
			"\n",
			"\r",
			"\t",
			"\v",
			"\\",
			"\"",
			"\'",
			"/" ,
			"*" ,
			"?",
			"<",
			">" ,
			"|",
			":",
		}
		local date = os.date()
		for i,v in ipairs(invalidchars) do 
			date = string.gsub(date,v,"_")
		end
		return map.."_"..date
	end 
	local buttons = {
		{name = "File",tables 	= {{"Run",function() salf:save() salf:run() end,10},{"New",function()
		mapAdmin.clear(salf.shapeCollider,salf) 
		salf.map = mapName("maps/new") 
		salf.mode = "game" 
		salf.ambient_color =  colors.ambient
		salf.layers:addLayer("Layer 1",1,0,0,true)
		end},
		{"New Scene",function() 
			mapAdmin.clear(salf.shapeCollider,salf)
			salf.scene_admin = scene_edtior:new(salf)
			salf.mode = "battle" 
			salf.map = mapName("maps/newScene")
		end}, {"Load",function() loads() end,10},		{"Save",function() saveAs()  end}, 		{"Save As",function() saveAs(true) end,10},{"Open dir",function() love.system.openURL(love.filesystem.getSaveDirectory().."/maps") end}}},
		{name = "Edit",tables 	= {}},
		{name = "Sprites",tables = { {"New",function() makeTexture:makeSpritesheet(salf,salf.ui,true) end},  {"Edit",function() makeTexture:new(salf,salf.ui) end,10} } },
		{name = "Map",tables    = {{"Properties",function() end,10},{"Open dir",function() love.system.openURL(salf:getMapDir()) end}}},
		{name = "Utilities",tables = {{"Wipe",function() 
			mapAdmin.clear(salf.shapeCollider,salf) 
			salf.layers:addLayer("Layer 1",1,0,0,true)
			if salf.mode == "battle" then 
				salf.scene_admin = scene_edtior:new(salf)
			end 
		end},{"Ambient color",function()
			local acolor = salf.ambient_color or colors.ambient
			local w,h = love.graphics.getDimensions()
			local f = ui:addFrame(nil,w/2,h/2,240,salf.ui:getFont():getHeight() + 30)
			f:addCloseButton()
			f:setHeader("Set ambient color")
			f.dragEnabled = true 
			local colorinp = ui:addTextinput(f,5,25,190,salf.ui:getFont():getHeight())
			local color = (acolor[1] or 255)..","..(acolor[2] or 255)..","..(acolor[3] or 255)..","..(acolor[4] or 255)
			colorinp:setText(color)
			colorinp.OnChange = function(t)
				salf.ambient_color = parsecolor(t)
			end 
			local reset = ui:addButton(f,f.width - 25,25,20,20)
			reset:setText("R")
			reset.OnClick = function()
				acolor = colors.ambient
				local color = (acolor[1] or 255)..","..(acolor[2] or 255)..","..(acolor[3] or 255)..","..(acolor[4] or 255)
				colorinp:setText(color)
			end 
		end},
		{"Skybox",function() 
			local editor = salf 
			if editor.skybox then editor.skybox = nil end 
			if love.filesystem.exists(editor.map.."/skybox.png") then 
				local img = love.graphics.newImage(editor.map.."/skybox.png")
				img:setWrap("repeat","repeat")
				local quad = love.graphics.newQuad(0,0,editor.width/editor.scale,editor.height/editor.scale,img:getDimensions())
				local sk = 
				{
					draw = function(...)
						love.graphics.draw(img,quad, ...)
					end,
					update = function()
						quad:setViewport(0,0,editor.width/editor.scale,editor.height/editor.scale,img:getDimensions())
					end 
				}
				editor.skybox = sk
			else 
				print("No skybox: "..editor.map.."/skybox.png")
			end 
		end,10},
		{"Reload Scripts",function()
			local fs = love.filesystem
			local e = salf 
			local env = e.triggerAdmin:getEnviroment()
			local dir = e.map.."/scripts"
			local parents = getParents(eCurrentMap)
			
			if fs.exists(dir) then 
				for i,v in ipairs(fs.getDirectoryItems(dir)) do 
					local _,t = pcall(fs.load,dir.."/"..v)
					if _ and t then
						print("Global: ",salf._G)
						env._G = salf._G
						_,t = pcall(setfenv(t,env)) 
						if _ or t then 
							print(_,t)
						end 
						print("LOADED: "..v)
					else 
						print("ERROR LOADING SCRIPT: "..v.." With: \n "..t)
					end
				end 
			end
			for i,v in ipairs(parents) do 
				for i,k in ipairs(fs.getDirectoryItems(v.."/scripts")) do 
					local _,t = pcall(fs.load,v.."/scripts/"..k)
					if _ and t then 
						print("Global: ",salf._G)
						env._G = salf._G
						_,t = pcall(setfenv(t,env)) 
						if _ or t then 
							print(_,t)
						end 
						print("LOADED: "..k)
					end
				end 
			end 
			e.triggerAdmin:setEnviroment(salf._G)
		end,10},
		{"Settings",function() 
			-- local cols = {} 
			-- cols.f = basicFrameColor
			-- cols.b = basicButtonColor
			-- local e = salf 
			-- local f = ui:addFrame("Editor settings",e.width/2,e.height/2,700,400)
			-- f:setHeader()
			-- f:addCloseButton()
			-- f.color = cols.f
			
			-- local colors = ui:addFrame("Colors",5,25,350,160,f)
			-- colors.color = cols.f
			-- local font = ui:getFont()
			-- local fh = font:getHeight()
			-- local ox = colors.width/2
			-- local frameColor = ui:addTextinput(colors,ox,25,(colors.width-10)/2,fh)
			-- local c = cols.f
			-- frameColor:setText(c[1]..","..c[2]..","..c[3]..","..(c[4] or "0"))
			-- frameColor.OnChange = function()
				-- cols.f = parseColor(frameColor.text)
				-- colors.color = cols.f
			-- end 
			-- local buttonColor = ui:addTextinput(colors,ox,25 + fh + 5,(colors.width-10)/2,fh)
			-- local c = cols.f
			-- buttonColor:setText(c[1]..","..c[2]..","..c[3]..","..(c[4] or "0"))
			-- buttonColor.OnChange = function()
				-- cols.b = parseColor(buttonColor.text)
			-- end 
			-- local save = ui:addFrame("Saving",5,230,400,160,f)
			
			-- save.color = cols.f
			
		end}
		}},
	}
	openButton:setText("_ _ _ _")
	openButton.color = colors.gray
	openButton.dontDrawB = true
	local mainFrame
	local on = false 
	openButton.OnClick = function()
		if OnOpen then OnOpen(on) end 
		if not on then 
			if not salf.Lock then 
				openButton.Locked = true
				salf.Lock = true
			end 			
			on =  true 
			openButton.y = 30
			local pw = love.graphics.getWidth() -40
			local w = 90
			local ox = (pw - ((w)*#buttons))/#buttons
			mainFrame = ui:addFrame(nil,pw/2 + 20,15,pw,30)
			mainFrame.color = basicFrameColor
			mainFrame.dontDrawB = true 
			for i,v in ipairs(buttons) do 
				local x = (w + ox)*(i - 1) + ox/2
				local a = ui:addButton(mainFrame,x,5,w,20)
				local menu = ui:addMenu(a,nil,nil,v.tables)
				menu:setInactive(true)
				
				a.OnFocus = function()
					menu:setInactive(false)
					menu.OnFocusLost = function()
						menu:setInactive(true)
					end 
				end 
				
				a:setText(v.name)
				a.dontDrawB = true 
				a.color = basicButtonColor
			end 
		else
			if openButton.Locked then 
				salf.Lock = false
				openButton.Locked = false
			end
			mainFrame:remove()
			openButton.y = 0
			on =  false 
		end 
	end 
	
end 
return mainMenu