local _ = {}
function _:new(module,dir,editor,colider,phyWorld,x,y)
	local ghost = module:new(editor,colider,phyWorld,x,y,"_CardSales")
	local ui = editor.ui 
	local shop = self.dep[1].module
	ghost.dontCast = true 
	ghost:setAttackRange(0,0,100,100)
	ghost:setJumpEffect(stock_effects.jump.anim)
	ghost:addTo("whitelist","Yume")
	local time = 0 
	local texts = {
		"Hmph, not enough sales..",
		"Buy these cards before they kill you.",
		"Buy and sell, Buy and sell!",
		"Are you gonna buy or what?!",
		"Its harsh out there, get your cards here!",
		"Dont look at me, instead buy these cards.",
		"Hurry up and buy, or sell or anything!",
		"I got some cards,you got some essence, lets trade.",
	}
	
	
	local Que = msgQue:new(0,0,ghost.width,ghost.height)
	local timerd = 0 
		for i,v in ipairs(ghost.states) do 
			if v.name == "idleB" then 
				v.anim:OnFinish(function()
					ghost.forceState = "idle"
					time = 0 
					v.anim.position = 1
					v.anim.playing = true
				end)
				v.anim:setMode("once")
			end 
		end
	ghost.OnUpdate = function(dt)
		Que.Active = true
		Que:setPosition(editor:camPos(ghost.renderRange:center()))
		Que:update(dt)
		time = time + 5*dt
		if time > 50 then 
			ghost.forceState = "idleB"
		end 
	end
	local target = {} 
	local cardShop
	local lg = love.graphics
	ghost.OnFocusLost = function()
		Que.Active = false
	end 
	ghost.attack = {
		new = function(a)

			clearTable(target)  	
			target.source = a.source 
			Que:add(texts[math.random(1,#texts)])
		end,
		update = function(dt)
			if target  then
				local _,actionKey = queryKey("Use",editor.play_keyControler,function()
					if not cardShop then 
						cardShop = shop:new(editor.ui,lg.getWidth()/2,lg.getHeight()/2,editor:getTween())
					else
						cardShop:remove()
						cardShop = nil  
						Que:add("See ya 'round")
					end		
				end )			
			end 
		end,
		OnLost = function(trg)
			if target and cardShop then 
				cardShop:remove()
				cardShop = nil 
				clearTable(target)
			end 
		end 
	}
	local target = {} 
	local selfOld 
	local oldMenu = ghost.OnMenu or function() end 
	function ghost:OnMenu(menu)
		oldMenu(self,menu)
		local b = ui:addButton(nil,0,0,2,2)
		b:setText("Open Store")
		b.OnClick = function()
			if not cardShop then 
				cardShop = shop:new(editor.ui,lg.getWidth()/2,lg.getHeight()/2,editor:getTween())
				cardShop.OnDone = function()
					cardShop = nil 
				end 
			else
				cardShop:remove()
				cardShop = nil 
			end	
		end 
		menu:addItem(b)
	end 
	return ghost
end 
function _:getDependencies()
	local d = {}
	local dep = {module = "cardshop"}
	table.insert(d,dep)
	return d 
end 
function _:setDependencies(d)
	self.dep = d  
end 
return _