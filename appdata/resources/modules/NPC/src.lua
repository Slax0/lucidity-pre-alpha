local args = {...}
local source_dir = args[1]
local rayCast = require(source_dir.."/rayCast")
local npc = {}
function npc:new(editor,colider,phyWorld,x,y,width,height)
	local self = {}
	local s = self 
	s.x = x or 0 
	s.y = y or 0 
	s.waypoints = {} 
	
	s.scalex = 1 
	s.scaley = 1 
	
	s.info = {}
	s.width = width 
	s.height = height 
	
	s.buff = false 
	
	s.speed = 250 
	s.speed_limit = 500 
	
	s.direction = {}
	s.ctrl = {}
	s.effects = {}
	
	s.timestamp = {}
	s.timestamp.a = 0
	s.timestamp.b = 0
	s.timestamp.clamp = 0
	s.clamps = {}
	s.clamps.yvel = false 
	 
	s.Active = true 
	
	s.phyWorld = phyWorld
	s.colider = colider 
	
	s.yvel = 0
	s.xvel = 0
	
	s.colide = 0
	s.bodyColide = 0
	s.onGround = 0
	s.speed = 900
	s.gravity = 3000
	
	s._stack =   {}
	s.friends =  {}
	s.targets =  {}
	s.rayWidth = 5
	s.rays ={} 
	s.radius = 40
	
	s.type = "npc"
	setmetatable(self,{__index = npc})
	
	if type(width) == "string" then 
		self:loadByName(height,width,true)
	end 
	self:makeColision()
	if phyWorld then 
		s.fluxGroup = Flux.group()
	end
	for i,v in ipairs(self.states or {}) do 
		if v.name == "falling" or v.name == "jumping" then 
			self.HasJumpAnim = true
		end 
	end 
	return self 
end 
function npc:loadByName(dir,name,force)
	self.name = name
	local mob = loadGhost:new(name,nil,nil,dir)
	if mob then 
		self._backupInfo = {
			name = name,
			dir = dir,
		}
		self.width = mob.width
		self.height = mob.height
		self.info = {
			hp = mob.hp,
			mp = mob.mp,
			maxHp = mob.maxHp,
			maxMp = mob.maxMp,
			def = mob.def,
			atk = mob.atk,
			luck = mob.luck,
			level = mob.level,
			element = mob.element,
		}
		self.image = mob.image
		self.state = "idle"
		for i,v in ipairs(mob.states) do
			local a,s,d,f,g,h = unpack(v.anim)
			v.anim = newAnimation(mob.image,a,s,d,f,g,h)
		end 
		self.scalex = mob.scale or 1 
		self.scaley = mob.scale  or 1 
		self.mirror = mob.mirror
		self:setStates(mob.states)
	end	
	if not force then 
		self:makeColision()
	end 
end 
function npc:makeColision()
	local body 
	if self.body then 
		self.body:destroy()
		self.body = nil 
	end 
	local phyWorld = self.phyWorld
	local colider = self.colider
	if self.renderRange then colider:remove(self.renderRange) end 
	self.renderRange = colider:addRectangle(self.x or 0,self.y or 0,self.width*self.scalex,self.height*self.scaley)
	function self.renderRange.getUserData()
		return {source = self,part = "rendRange"}
	end 
	if self.OnMakeColision then 
		self.OnMakeColision(self.renderRange)
	end 
	
	if not self.body and not self.defined_Body and phyWorld then		
		if phyWorld and not self.dontCast then 
			self.raycast = rayCast:new(phyWorld,self.x - (self.width/2),self.y - (self.height/2))
			local func1 = function(fixture,x,y,xn,xy,fraction)
				if fixture:getMask() ~= 16 then 
					for i in ipairs(self.fixtures) do
						if fixture == self.fixtures[i] then
							return true
						end
					end
				else 
					return true
				end
			end
			self.raycast.mask = {}
			table.insert(self.raycast.mask,func1)
		end 
		local body = love.physics.newBody(phyWorld,self.x,self.y,"dynamic")
			body:isSleepingAllowed(false)
			local sx,sy = self.scalex,self.scaley
			local ow = self.bodyWidth or self.width
			local oh = self.bodyHeight or self.height
			local w = (ow *sx)
			local h = (oh *sy)
			
			local wa = 0
			local w2 = ((ow/2)*sx)/1.5
			local h2 = ((oh/2)*sy)/4 -(2.5*sx)
			if w2 <= 0 then
				w2 = 15
			end
			if h2 <= 0 then
				h2 = 5
			end
						-- Make the ellipse
			local diameter = h/2
			local oh = h - diameter
			if diameter > w/2 then
				diameter = w/2
				wa = w/2 - (diameter)
				oh = h - diameter
			end
			local fixtures = {}
			local shapes = {}
			
			

						-- Make the ellipse
			
			shapes[1] = love.physics.newRectangleShape(0,-h/2 + oh/2,w,oh)
			fixtures[1] = love.physics.newFixture(body,shapes[1])
			fixtures[1]:setUserData({source = self,part = "Body"})
			
			shapes[2] = love.physics.newRectangleShape(0,h/2-2,w-(w*0.10),5)
			fixtures[2] = love.physics.newFixture(body,shapes[2])
			fixtures[2]:setUserData({source = self,part = "Indicator"})
			fixtures[2]:setSensor(true)

			shapes[3] = love.physics.newCircleShape(w/2 - (diameter) - wa,h/2 - diameter,diameter)
			fixtures[3] = love.physics.newFixture(body,shapes[3])
			fixtures[3]:setUserData({source = self, part = "Ground"})
			
			if diameter < w then
				shapes[4] = love.physics.newCircleShape(-w/2 + (diameter) - wa,h/2 - diameter,diameter)
				fixtures[4] = love.physics.newFixture(body,shapes[4])
				fixtures[4]:setUserData({source = self, part = "Ground"})
			end
			
		self.shapes = shapes 
		self.fixtures = fixtures
		body:setFixedRotation(true)
		body:setMassData(0,0,0,0)
		body:setLinearDamping(0,0)
		self.body = body
		for i,v in ipairs(fixtures) do 
			v:setFriction(0)
			v:setCategory(16)
			v:setMask(16)
			v:setGroupIndex(-137)
		end 
		local x,y = 0,h/2
		if self.indicatorCol then colider:remove(self.indicatorCol) end 
		self.indicatorCol = colider:addRectangle(x,y,w-6,5)
		self.indicatorCol.ofx = x
		self.indicatorCol.ofy = y - (self.height - 3)
		function self.indicatorCol.getUserData()
			return {source = self,part = "Alt_Indicator"}
		end 
		colider:addToGroup(tostring(self),self.indicatorCol)
	end
	if self.attackRangeData then 
		colider:remove(self.attackRange) 
		self:setAttackRange(unpack(self.attackRangeData))
	end 
	self.colision = self.indicatorCol
	if self.indicatorCol then colider:addToGroup(tostring(self),self.indicatorCol) end 
	if self.renderRange then colider:addToGroup(tostring(self),self.renderRange) end 
	if self.spotRange then colider:addToGroup(tostring(self),self.spotRange) end
end
function npc:update(dt) 

	if self.indicatorCol then self.indicatorCol:moveTo(self.x - self.indicatorCol.ofx,self.y - self.indicatorCol.ofy) end 
	if not self.ctrl then return end
	local left,right,jump = self.ctrl["left"],self.ctrl["right"],self.ctrl["jump"]
	local scalex,scaley = self.scalex,self.scaley 
		if self.fluxGroup then self.fluxGroup:update(dt) end 
		if self.Active then
			if not self.dead then  				
				local waypoint = self.waypoint
				if waypoint then 
					local x,y = waypoint.x,waypoint.y
					local s = self:move(x,y)
					if s then 
						if waypoint.fn then waypoint.fn() end 
						self.waypoint = nil 
					end 
				end 
				local colider = self.colider
				if not self.direction then 
					return false 
				end 
				if left then
					self.direction["still"] = false
					if self.direction["right"] then 
						
						self.direction["right"] = false 
						self.direction["x_change"] = true 
					
					else 
						self.direction["x_change"] = false 
					end 
					self.direction["left"] = true
				elseif right then
					self.direction["still"] = false
					if self.direction["left"] then 
		
						self.direction["left"]  = false 
						self.direction["x_change"] = true 
					
					else 
						self.direction["x_change"] = false 
					end 
					self.direction["right"] = true
				else 
					self.direction["still"] = true
				end
			if self.body and not self.body:isDestroyed() then
				self.body:setActive(true)
				self:movement(dt)
				self.x,self.y = self.body:getPosition()
				self.renderRange:moveTo(self.x,self.y)
				if not self.dontCast then 
					self.raycast:setPosition(self.x,self.y)
					self.rayCast = self.raycast
				end 
				if self.target and not self.paused then
					self:setTargetPos(self.target.x,self.target.y)
				end
				if self.activeTarget  and self.activeTarget ~= self.old_activeTarget then
					local a = self.activeTarget
					if a.mode == "blacklist" then 
						if self.attack  then
							self.attacking = true
							if self.attack.new then self.attack.new(a) end 		
						else
							self.attacking = true
						end
					else 
						if self.embrace then self.embrace.new() end 
					end 
					self.old_activeTarget = self.activeTarget
				elseif self.activeTarget then 
					if self.attack then 
						if self.attack.update then  self.attack.update(dt) end 
					elseif self.embrace then 
						if self.embrace.update then self.embrace.update(dt) end 
					end 
				end
				self:makeRayPoints(self.rays)
				if not self.dontCast then self.raycast:cast() end 
				if self.onUpdate then
					self.onUpdate(dt)
				end
				if self.attackRange then self.attackRange:moveTo(self.x,self.y) end 
				if self.spotRange then self.spotRange:moveTo(self.x,self.y) end 
			else 
				local width = self.width
				local height = self.height
				local w,h = (width)/2,(height)/2
				self.topX,self.topY = incrementPos(self.x,self.y,w,h)
				self.dx = self.topX + (self.modx or 0) 
				self.dy = self.topY + (self.mody or 0)
				self.renderRange:moveTo(self.topX + (self.modx or 0),self.topY + (self.mody or 0))
			end

			if self.onGround > 0 then
				self.direction["jump"] = false
			elseif self.onGround == 0 then 
				self.direction["still"] = false
				self.direction["jump"] = true
			end
			local state 
			if not self.forceState then 
				if self.direction["still"] then 
					state = "idle"
				elseif  (self.direction["left"] or self.direction["right"]) and not _clamp then  
					state = "run"
				end 
				if not state then 
					state = "idle"
				end 
				if self.direction["x_change"] then 
					state = "x_change"
				end 
			
				if self.onGround == 0  and self.HasJumpAnim then 
					if self.yvel > 0 then
						state = "falling"
					elseif self.yvel < 0 then
						state = "jumping"
					end
				end 
			end 
			self.state = self.forceState or state
		end 
		for i,v in pairs(self.states) do 
			if self.state == v.name then 
				v.anim:update(dt)
			elseif v.restFrame then 
				v.anim:seek(v.restFrame)
			end 
		end
		self:updateEffects(dt)
	end
		if self.OnUpdate then
			self.OnUpdate(dt)
		end	
end 
function npc:OnCopy()
self.indicatorCol = nil 
self.renderRange = nil 
self.attackRange = nil 
self.spotRange = nil
end

function npc:draw(debug)
	local sx,sy = self.scalex,self.scaley
	if self.mirror then 
		sx = -sx 
	end 
	local x,y = self.x,self.y 
	if not self.body then 
		x,y = self.dx,self.dy
	else 
		if self.attack then
			if self.attack.draw then  self.attack.draw(debug) end 
		end
		if self.embrace then
			if self.embrace.draw then  self.embrace.draw(debug) end 
		end
	end 
	if self.direction["left"] then 
		sx = -sx 
	end 
	local state = self.state
	for i,v in pairs(self.states) do 
		if state == v.name then 
			v.anim:draw(x,y,self.r or 0,-sx,sy,self.width/2,self.height/2)
		end 
	end
	self:drawEffects()
	setColor(colors.green)
	if self.body and not self.body:isDestroyed() and debug then 
		for i,v in pairs(self.shapes) do 
			if v.getPoints then 
				love.graphics.polygon("line",self.body:getWorldPoints(v:getPoints()))
			else 
				local x,y = self.body:getWorldPoints(v:getPoint())
				love.graphics.circle("line",x,y, v:getRadius())
			end 
		end 
		if self.rayCast and not  self.dontCast then 
			self.rayCast:draw()
			self.rayCast:drawRange()
		end  
	end 
	if debug then 
		setColor(colors.white)
		if self.indicatorCol then setColor(colors.yellow) self.indicatorCol:draw("fill") end 
		if self.renderRange then setColor(colors.white) self.renderRange:draw("line") end 
		if self.attackRange then setColor(colors.red) self.attackRange:draw("line") end 
		if self.spotRange then setColor(colors.red) self.spotRange:draw("line") end 
	end  
	setColor(colors.white)

end 
function npc:drawEffects()
	for i,v in pairs(self.effects) do 
		v.draw()
	end 
end 
function npc:updateEffects(dt)
	for i,v in pairs(self.effects) do 
		v.update(dt)
	end 
end 
function npc:jumpEffect(x,y,r,sx,sy,dir)
	if self.jump_effect and not self.dead then 
		local anim = deepCopy2(self.jump_effect.anim)
		local w,h = anim.img:getDimensions()
		local w = w/#anim.frames
		setmetatable(anim,{__index = self.jump_effect.anim.__index})
		anim:setMode("once")
		local effect = {
			draw = function()
				anim:draw(x + (2*sx),y - h/2 +4,r,sx*dir,sy,w/2,h/2)
			end,

		}
		effect.update = function(dt)
			anim:update(dt)
			if not anim.playing then 
				removeFromTable(self.effects,effect)
			end 
		end 
		table.insert(self.effects,effect)
	end 
end 
function npc:turnTo(direction) 
	for i,v in pairs(self.direction) do 
		if i == direction then 
			self.direction[i] = true 
		else 
			self.direction[i] = false
		end 
	end 
end 
function npc:setJumpEffect(anim)
	self.jump_effect = {anim = anim}
end 
function npc:movement(dt)
	local body = self.body
	if not body:isDestroyed() then 
		local jump = false
		local _moveClamp
		local _clamp = false
		local timestamp = self.timestamp
		local timeJump = self.timestamp.a
		local timeKey = self.timestamp.b
		local brake = false
		local speed_limit = self.speed_limit
		local onGround = 0 
		for i,v in pairs(body:getContactList()) do 
			local other,part = self:checkCol(v:getFixtures())
			if other and part then 
				if part == "Indicator" then 
					onGround = onGround + 1
				end 
			end 
		end 
		if self.onGround ~= onGround then 
			self.onGround = onGround
		end 
		local c_left,c_right,c_jump = self.ctrl["left"],self.ctrl["right"],self.ctrl["jump"]
		
		if not self.speed then
			self.speed = 3000
		end

		if c_left or c_right then 
			self.timestamp.clamp = self.timestamp.clamp + 10*dt
			self.inactive = false
		else
			self.inactive = true
			self.timestamp.clamp = 0
		end
		if self.timestamp.clamp > 3 then
			_clamp = false
		end

		local xvel,yvel = body:getLinearVelocity()
		body:setAngularVelocity(0)
		body:setLinearVelocity(math.round(xvel,3),math.round(yvel,3))
		
		local groundColision
		if self.colide > 0 then
			groundColision = true
		else
			groundColision = false
		end
		
		local bodyColision
		if self.bodyColide > 0 then
			bodyColision = true
		else
			bodyColision = false
		end
		
		local groundConfirm = 0 
		if self.onGround > 0 then
			groundConfirm = true 
		else
			groundConfirm = false 
		end
		if groundColision then
			groundConfirm = true
		end
		
		self.directionChanged = false
		
		if self.direction["x_change"] then 
			local x,y = body:getLinearVelocity()
			body:setLinearVelocity(0,y)
		end 
		if not self.inactive then
			if xvel < speed_limit and  xvel > -speed_limit then
				if not  _clamp then
					local xvl,yvl = body:getLinearVelocity()
					if c_left then
						xvl = xvl - (self.speed*dt)
						body:setLinearVelocity(xvl,yvl)
					elseif c_right then
						xvl = xvl + ((self.speed*2)*dt)
						body:setLinearVelocity(xvl,yvl)		
					end
				end
			else
				local xvel,yvel = body:getLinearVelocity()
				if xvel > 0 then
					xvel = speed_limit
				elseif xvel < 0 then
					xvel = -speed_limit
				end
				
				body:setLinearVelocity(xvel,yvel)
			end
		else
			local xvel,yvel = body:getLinearVelocity()
			if xvel > 0 then
				xvel = xvel - (self.speed*4)*dt
			end
			if xvel < 0 then
				xvel = xvel + (self.speed*4)*dt
			end
			body:setLinearVelocity(xvel,yvel)
		end
		if self.onGround ~= 0 and groundColision and  c_jump then
			jump = true 
			local xvl,yvl = body:getLinearVelocity()
			body:setLinearVelocity(xvl,-(self.jump_velocity or 700))
		end
		
		local _ = false
		local xvl,yvl = body:getLinearVelocity()
		if self.inactive or _clamp then
			_ = true
		else
			_ = false
		end
		
		yvl = yvl + ((self.gravity)*dt)
		if self.onGround ~= 0 and groundColision and groundConfirm and _ then
			if math.abs(math.floor(yvl)) == 1 or  math.abs(math.floor(yvl)) < (1000*dt) then
				yvl = 0
				body:setLinearVelocity(xvl,yvl)
			end
			if math.abs(math.floor(xvl)) == 1 or  math.abs(math.floor(xvl)) < (7000*dt) then
				xvl = 0
			end
		end
		body:setLinearVelocity(math.floor(xvl),math.floor(yvl))
		if self.OnMovement then 
			self.OnMovement(dt)
		end 
		local xvl,yvl = body:getLinearVelocity()
		self.yvel = yvl
		self.xvel = xvl
	end 
end 
function npc:checkCol(a,b)
	local other 
	local otherSource
	local part 
	local ad = a:getUserData()
	local bd = b:getUserData()
	if ad and ad.source == self then
		part = ad.part 
		otherSource = bd 
		other = b 
	elseif bd and bd.source == self then 
		part = bd.part
		other = a 
		otherSource = ad
	end 
	if other and otherSource.source then 
		if otherSource.source.type == "npc" or otherSource.source.type == "player" then 
			other = nil 
		end 
	elseif other and other:isSensor() then 
		other = nil
	end 
	return other,part
end 
function npc:HcheckCol(a,b)
	local other 
	local part 
	local own 
	local otherPart 

	local ad 
	local bd 
	
	if a.getUserData then 
		ad = a:getUserData()
	end 
	if b.getUserData then 
		bd = b:getUserData()
	end
	if ad and ad.source == self then
		part = ad.part 
		own = a 
		if b.getUserData then 
			other = b:getUserData().source
			otherPart = b:getUserData().part
		end 
	elseif bd and bd.source == self then 
		part = bd.part
		own = b 
		if a.getUserData then 
			other = a:getUserData().source
			otherPart = a:getUserData().part
		end 
	end 
	return other,otherPart,own,part 
end 
function npc:beginContact(a,b,coll)
	local other,part = self:checkCol(a,b)
	if other then 
		if part == "Ground" then 
			if coll:isTouching() then
				self.colide = self.colide + 1
			end 
		elseif part == "Body" then 
			self.bodyColide = self.bodyColide + 1
			local x,y = coll:getNormal()
			local _,f = math.modf(y)
			local _,d = math.modf(x)
			if f ~= 0 then 
				self:onClip(other)
			elseif d ~= 0 then 
				self:onClip(other)
			end 
		elseif part == "Indicator" then 
			if coll:isTouching() then
				self.onGround = self.onGround + 1
			end
		end 
	end 
end 
function npc:onClip()
	if not self.dead then self:die() end 
end 
function npc:endContact(a,b,coll)
	local other,part = self:checkCol(a,b)
	if other then 
		if part == "Ground" then 
			if coll:isTouching() then
				self.colide = self.colide - 1
			end 
		elseif part == "Body" then 
			self.bodyColide = self.bodyColide - 1
		elseif part == "Indicator" then 	
			self.onGround = self.onGround - 1
			if self.onGround < 0 then 
				self.onGround = 0
			end 
			if self.onGround == 0 then 
				local x = self.x 
				local y = self.y + self.height/2 
				local sx = 1
				if self.direction["right"] then 
					sx = -1
				elseif self.direction["left"] then 
					sx = sx 
				end
				self:jumpEffect(x,y,0,self.scalex,self.scaley,sx)
			 end 
		end 
	end 
end 
function npc:setRadius(radius)
	self.radius = radius
	self:makeRayPoints()
end
function npc:makeRayPoints(rays,radius)
	clearTable(rays)
	local pi = math.pi
	local c = (radius or self.radius)
	c = 2 * pi * (radius or self.radius)
	local segments = c/self.rayWidth
	for i= 0,segments do 
		local ray = {}
		local n = (2*i*3.14)/segments
		local x = self.x +  c*math.cos(n) 
		local y = self.y +  c*math.sin(n) 
		ray.x = x
		ray.y = y 
		table.insert(rays,ray)
	end
	self.segments = segments
	if not self.dontCast then self.raycast:setRaytable(rays) end 
end
function npc:setTarget(target)
	self.target = target
	if target.getFixtures and not self.dontCast then 
		local fixturecheck = target:getFixtures()
		local func2 = function(fixture,x,y,xn,xy,fraction)
			for i in ipairs(fixturecheck) do
				if fixture == fixturecheck[i] then
					return true
				end
			end
		end
		table.insert(self.raycast.mask,func2)
	else 
		print("INVALID TARGET")
	end 
end
function npc:move(mx,my,dir)
		local mx = mx or self.x 
		local my = my or self.y 
		local stack = self._stack 
		local direction = stack.dir
		local sucess
		local clamp = stack.clamp
		local yList = math.ceil(self.segments/4)
		local jumpTreshold = math.floor((self.height/self.rayWidth)/2) + 10
		local ctrl = self.ctrl 
		local left,right,jump = stack.left,stack.right,false 
		
		local function detDir(no)
			local l,r 
			if no == 1 then 
				left = true 
				l = true 
			elseif no == -1 then 
				r = true 
				right = true
			end 
			return l,r
		end 
		local function resetStack()
			stack.right = false 
			stack.left = false
		end 
		if not self.renderRange.isDestroyed then 
			if self.renderRange:contains(mx,my) then
				stack.sucess = true
				sucess = true 
			else
				stack.sucess = false
				sucess = false 
			end
		end
		local horzFinal = false
		local vertFinal = false
		if not stack.sucess and not stack.fail then 
			if math.floor(self.x) == math.floor(mx) then
				horzFinal = true
			end
			if math.floor(self.y) == math.floor(my) then
				vertFinal = true
			end
			local selfX = math.floor(self.x)
			local selfY = math.floor(self.y)
			
			local _ 
			if stack.left or stack.right then 
				_ = true
			end 
			if not _ then
				if self.x > mx then
					direction = -1
				elseif self.x < mx then
					direction = 1
				else
					self.target = nil
				end
				local cm,clamp,am = self:rayViewHoriz(direction*-1,5)
				local needsJump = self:rayViewVert(direction*-1,jumpTreshold)
				if am then 
					jump = true 
					stack.left,stack.right = detDir(direction*-1)
				end 
				
				if not am then 
					if needsJump > 1 and clamp < 8 then 
						jump = true
						stack.left,stack.right = detDir(direction*-1)
					elseif needsJump >= 2 and clamp <= 8 then 
						jump = true
						stack.left,stack.right = detDir(direction*-1)
					else 
						jump = false 
						if cm then 
							detDir(direction*-1)
						elseif not am then 
							local _,clamp2,am = self:rayViewHoriz(direction,5)
							if am then 
								jump = true 
							end 
							if _ then 
								detDir(direction)
								if not overRide then 
									detDir()
								end 
							end 
						end 
					end 
					if needsJump == 0 then 
						jump = false
					end 
					if clamp == 0 then 
						resetStack()
						jump = false 
						stack.left,stack.right = detDir(direction)
					end 
				end
			elseif _ then
				if self.x > mx then
					direction = -1
				elseif self.x < mx then
					direction = 1
				else
					self.target = nil
				end
				local cm,clamp = self:rayViewHoriz(direction*-1,5)
				local needsJump = self:rayViewVert(direction*-1,jumpTreshold)
				if clamp >= 8 or needsJump > 4 then 
					resetStack()
					jump = false 
					stack.left = nil 
					stack.right = nil
				end 
			end
		if findDistance(self.x,(stack.px or -1000)) < 2 and findDistance(self.y,(stack.py or -1000)) < 2  then
			stack.stuck_clamp = (stack.stuck_clamp or 0) + 1
		else
			stack.stuck_clamp = 0
		end
	end 
	if force then 
		stack.stuck_clamp = 0 
	end 
	stack.targetx =  math.floor(mx)
	stack.targety =  math.floor(my)
	stack.px = self.x
	stack.py = self.y
	stack.dir =  direction
	ctrl["left"] = left  
	ctrl["right"] = right 
	ctrl["jump"] = jump
	return sucess,horzFinal,vertFinal
end
function npc:rayViewHoriz(direction,treshold)
	local hit,ray = self.raycast:returnHitlist()
	local seg = math.ceil(self.segments)
	local groundRayCount = 7 
	local clamp = 0
	local starty = 0
	local startx = 0
	local finishy = 0
	local finishx = 0 
	local treshold = treshold or 0
	local can_move = false
	local jump = 0
	local center = math.ceil(seg/4)

	local starty = 0
	local refY = 0
	if ray[center].data  then
		refY = math.floor(ray[center].data.y)
	end
	for i = 0,7 do
		if direction == 1 then
			if ray[center + i].mature then
				clamp = clamp + 1
			end
		elseif direction == -1 then
			local x = center - i
			if x < 0 then
				x = math.ceil(seg - i) 
			end
			if ray[x] and ray[x].mature then
				clamp = clamp + 1
			end
		end
	end

	if clamp > treshold then
		can_move = true
	end
	local am = 0 
	local leftC = math.floor(center + seg/4)
	local rightC = math.floor(center - seg/4)+1
		if direction == 1 then 
			for i = math.floor(leftC - seg/5),leftC  do 
				if ray[i].mature then
					am = am + 1 
				end 
			end
		elseif direction == -1 then 
			for i = rightC,math.floor(rightC + seg/4)  do 
				if ray[i].mature then
					am = am + 1 
				end 
			end
		end 
	if am <= 9 and self.onGround > 0 then 
		am = true 
	else 
		am = false 
	end 
	return can_move,clamp,am
end
function npc:rayViewVert(direction,treshold,returnList)
	local hit,ray = self.raycast:returnHitlist()
	local seg =  math.ceil(self.segments)
	local nam = math.ceil(seg/2)
	local clamp = 0
	if direction == 1 then
		for i = nam ,treshold +(math.floor(nam*0.55)) do
			if ray[i].mature then
				clamp = clamp + 1
				if returnList then
					table.insert(returnList,ray[i].data)
				end
			end
		end
	else 	
		for i = seg - treshold,seg+2 do
			if i > seg then
				i = (i - seg)
			end
			if ray[i].mature then
				clamp = clamp + 1
				if returnList then
					table.insert(returnList,ray[i].data)
				end
			end
		end
	end
	return clamp,returnList
end 
function npc:setAttackRange(...)
	if self.body then 
		if self.attackRange then 
			self.colider:remove(self.attackRange)
		end 
		self.attackRange = self.colider:addRectangle(...)
		function self.attackRange.getUserData()
			return {source = self,src = self,part = "Attack Range"}
		end 
		self.colider:addToGroup(tostring(self),self.attackRange)
		self.attackRangeData = {...}
	end 
end 
function npc:setWhitelist(whitelist)
	if whitelist then
		if type(whitelist) == "table" then 
			self.friends = whitelist
		else
			error("white list must be a table, such as {'name' , 'name2' }")
		end
	end
end
function npc:addTo(mode,name)
	if mode == "blacklist" then 
		table.insert(self.targets,name)
	elseif mode == "whitelist" then 
		table.insert(self.friends,name)
	end 
end 
function npc:removeFrom(mode,name)
	if mode == "blacklist" then 
		removeFromTable(self.targets,name)
	elseif mode == "whitelist" then 
		removeFromTable(self.targets,name)
	end 
end 
function npc:setBlacklist(blacklist) 
	if whitelist then
		if type(blacklist) == "table" then 
			self.targets = blacklist
		else
			error("black list must be a table, such as {'name' , 'name2' }")
		end
	end
end
function npc:setActiveTarget(mode,target,part)
	if part == "Attack Range" then  
		if (self.activeTarget and self.activeTarget.source ~= target) or not self.activeTarget then  
			self.activeTarget = {mode = mode,source = target}
		end 
	else 
		if self.OnSpot then self.OnSpot(mode,target,part) end 
	end 
end 
function npc:HCol(dt,a,b)
	if Gamestate.current() ~= battle then 
		local other,otherpart,own,part   = self:HcheckCol(a,b)
		if other and other.name and own then 
			local name = other.name 
			if name and otherpart == "body" then  
				local foe 
				for i,v in ipairs(self.friends) do 
					if name == v or other.__name == v then 
						foe = false 
					end 
				end 
				for i,v in ipairs(self.targets) do 
					if name == v or other.__name == v then 
						foe = true 
					end 
				end 
				if foe == nil then 
				elseif not foe then
					self:setActiveTarget("whitelist",other,part)
				elseif foe then
					if part == "rendRange" and self.activeTarget then
						if self.attack then
							self.timestamp.capture = (self.timestamp.capture or 0) + 22*dt
							Channel.MaxCapture = math.max(Channel.MaxCapture,self.timestamp.capture)
							if self.timestamp.capture > 2 then 
								local info = self:getInfo()
								self.Active = false 
								local team = {{name = info.name,dir = info.dir,_host = self}}
								local cols = self.renderRange:neighbors()
								local no = 0 
								local t = {}
								for shape in pairs(cols) do 
									if shape.getUserData and shape:collidesWith(self.renderRange) then 
										table.insert(t,shape)
									end 
								end
								local function findTarget(source,name)
									for i,v in ipairs(source.targets) do 
										if v == name then 
											return true 
										end 
									end 
								end
								for i,shape in ipairs(t) do 
									no = no +1 
									if #team <= 4 then 
										local d = shape.getUserData()
										for m,v in ipairs(team) do 
											if v._host ~= d.source then 
												if d.source ~= self and d.source.getInfo and d.source.type == "npc" then 
													if findTarget(d.source,name) then 
														d.source.Active = false 
														local info = d.source:getInfo()
														local t = {name = info.name, dir = info.dir,_host = d.source} 
														if #team <= 4 then  
															table.insert(team,t)
														end
													end 													
												end 
											end 
										end 
									end 
								end 
								if self.attack.OnAction then self.attack.OnAction(team) else 
									local t = deepCopy2(self.activeTarget.source)
									local current = Gamestate.current()
									if current == PlayMap then 
										PlayMap:OnBattleMode(team,t)
									end 
								end 
							end 
						end 
					else 
						self:setActiveTarget("blacklist",other,part)
					end 
				end 
			end 
		end 
	end 
end 
function npc:HStop(dt,a,b) 
if self.activeTarget then 
	local other,otherpart,own,part  = self:HcheckCol(a,b)
		if other and other.name and otherpart == "body" then 
			local name = other.name 
			if name then  
				local foe 
				for i,v in ipairs(self.friends) do 
					if name == v then 
						foe = false 
					end 
				end 
				for i,v in ipairs(self.targets) do 
					if name == v then 
						foe = true 
					end 
				end 
				if not foe then 
	
				elseif foe then 
					self.timestamp.capture = 0
				end 
				if part == "Attack Range" then
					if self.attack and self.attack.OnLost then self.attack.OnLost(self.activeTarget) end 
					if self.embrace and self.embrace.OnLost then self.embrace.OnLost(self.activeTarget) end 
					self.activeTarget = false 
				end 
			else 

			end  
		end 
	end 
end 
function npc:save()
	local t = {x = self.x,y = self.y,zmap = self.zmap,expectedOneLoad = self._oneLoad}
	t.waypoints = {}
	for i,v in ipairs(self.waypoints or {}) do 
		if v.save then table.insert(t.waypoints,v:save()) end 
	end 
	return t 
end 
function npc:load(t,dead)
	if dead or not t.x then 
		self:remove()
	else 
		self.x = t.x 
		self.y = t.y
		self.expectedOneLoad = t.expectedOneLoad
		self.zmap = t.zmap
		self:makeColision()
		if not self.body then 
			for i,v in ipairs(t.waypoints or {}) do 
				local a = self:addWaypoint(0,0)
				a.fresh = false 
				a:load(v)
			end  
		else 
			if not self.waypoints then self.waypoints = {} end 
			for i,v in ipairs(t.waypoints or {}) do 
				table.insert(self.waypoints,{
					x = v.x, 
					y = v.y,
				})
			end
		end 
	end 
end 
function npc:getColision()
	return self.attackRange or self.renderRange
end 
function npc:getInfo()
	return self._backupInfo 
end 
function npc:setState(state)
	self.state = state
end 
function npc:setStates(states)
	self.states = states
	for i,v in ipairs(self.states) do 
		if v.name == "falling" or v.name == "jumping" then 
			self.HasJumpAnim = true
		end 
	end 
end 
function npc:OnLoseFocus()
	print("npc lost focus.")
	if not self.body then 
		return false 
	end 
	self.body:setSleepingAllowed(true)
	self.body:setAwake(false)
	self.body:setActive(false)
	if self.OnFocusLost then 
		self.OnFocusLost()
	end 
end 
function npc:remove()
	if self.OnRemove then self.OnRemove() end 
	local c = self.colider
	if c then 
		if self.attackRange then  c:remove(self.attackRange) end 
		if self.spotRange then c:remove(self.spotRange) end
		if self.indicatorCol then c:remove(self.indicatorCol) end 
		if self.renderRange then c:remove(self.renderRange) end 
		c:remove(self:getColision())
	end 
	if self.body and not self.body:isDestroyed() then 
		self.body:destroy()
	end 
	for i,v in pairs(self) do 
		if type(v) ~= "function" then 
			self[i] = nil
		end 		
	end 
end
function npc:die()
	local found 
	self.dead = true 
	self.state = "death"
	for i,v in ipairs(self.states) do 
		if v.name == self.state then 
			local anim = v.anim 
			anim:OnFinish(function() self:remove() anim._onFin = nil end)
			anim:setMode("once")
			found = true 
		end 
	end 
	if not found then 
		self:remove()
	end 
end 
function npc:OnRightClick(menu,ui)
	if self.OnMenu then 
		self:OnMenu(menu,ui)
	end 
	local x,y = self.x,self.y
	menu:addChoice("Add Waypoint",function()
		self:addWaypoint(x,y)
	end)
end 
function npc:executeWaypoint(waypoint,fn)
	if type(waypoint) == "number" then 
		self.waypoint = self.waypoints[waypoint]
	else 
		self.waypoint = waypoint
	end 
	if fn then 
		self.waypoint.fn = fn
	end 
end
function npc:turnAround(fn)
	self.direction["x_change"] = true 
	Timer.add(0.2,function()
		if self.direction["left"] then 
			self.direction["left"] = false 
			self.direction["right"] = true 
		else 
			self.direction["left"] = true  
			self.direction["right"] = false  
		end 
		self.direction["x_change"] = false
		if fn then fn() end 
	end)
end 
local waypoint = {} 
function npc:addWaypoint(x,y)
	local s = {
		x = x,
		y = y,
		no = #self.waypoints +1,
		zmap = self.zmap,
		colider = self.colider,
		fresh = true,
		papa = self,
	}
	
	setmetatable(s,{__index = waypoint})
	s:makeColision()
	table.insert(self.waypoints,s)
	return s
end
function waypoint:setPosition()
	
end  
function waypoint:draw()
	local waypoint = self.papa.waypoints[self.no - 1]
	local olw = love.graphics.getLineWidth()
	love.graphics.setLineWidth(5)
	if  waypoint then 
		love.graphics.line(self.x,self.y,waypoint.x,waypoint.y)
	else 
		love.graphics.line(self.x,self.y,self.papa.x,self.papa.y)
	end 
	love.graphics.setLineWidth(olw)	
	self.shape:draw("fill")
	setColor(colors.green)
	love.graphics.circle("fill",self.x,self.y,15)
	setColor(colors.black)
	love.graphics.setNewFont(15)
	love.graphics.print(self.no,self.x-5,self.y-10)
	setColor()
end
function waypoint:update(dt)
	if self.fresh then 
		local x,y = Gamestate.current():getMousePos()
		self.x = x
		self.y = y
	end 	
	if love.mouse.isDown("r") then 
		self.fresh = false 
	end 
	self.shape:moveTo(self.x,self.y)
end
function waypoint:unpack()
	return self.x,self.y
end
function waypoint:makeColision()
	self.shape = self.colider:addCircle(self.x,self.y,20)
	self.shape._module = self
end  
function waypoint:remove()
	self.colider:remove(self.shape)
	if self.papa.waypoints then 
		removeFromTable(self.papa.waypoints,self)
	end 
end  
function waypoint:OnMenu(ui,list)
	ui:addChoice("Delete",function()
		self:remove()
		if self.papa.waypoints then 
			removeFromTable(self.papa.waypoints,self)
			for i,v in ipairs(self.papa.waypoints) do 
				v.no = i 
			end 
		end 
	end)
end
function waypoint:getColision()
	return self.shape
end
function waypoint:save()
	local t = {
		x = self.x, 
		y = self.y,
	}
	return t 
end
function waypoint:load(t)
	self.x = t.x 
	self.y = t.y
	self:makeColision()
end 
return npc 