local wind = {}
function wind:new(module,dir,mode,colider,phyWorld,x,y)
	local wind = module:new(mode,colider,phyWorld,x,y,200,200)
	local windimg = love.graphics.newImage(dir.."/wind.png")
	assert(windimg,dir.."/wind.png")
	local windAnims = {} 
	local fw,fh = 20,20
	for i=1,4 do
		windAnims[i] = newAnimation(windimg,fw,fh,1/5,1 + (8*(i-1)),8 + (8*(i-1)))
	end 
	local w,h = love.graphics.getDimensions()
	local last = 0 
	local upper = 0
	local lower = 0 
	local squares = {} 
	local size = 20
	
	local function start(square,delay)
		local n = 3
		local u,l = (n*2) -last,(n*3) + last
		if upper == u then
			u = math.max(math.random(n,n*2),math.random(n + last/2,n + last))
		end 
		if lower == l then 
			l = math.max(math.random(n,n*2 ),math.random(last/2,last))
		end 
		last = math.random(u,l) 
		last = math.max(n,last)
		upper = u
		lower = l
		if last == 4 then 
			last = love.math.random(n,n+2)
		end 
		local tweenGroup = wind:getGroup()
		local delay = delay or last
		local type = love.math.random(1,4)
		if type == 1 or type == 2  or math.random(1,2) == 2 then 
			type = 4
		end
		tweenGroup:to(square,delay,{
			x = love.math.random(w/2,math.floor(w)),
			y = math.random(love.math.random(h/1.5,h),love.math.random(h/2,h)),
			t = 0,
		}):oncomplete(function()
			square.x = math.random(square.x - w -10,square.x - w/2)
			square.t = 350
			square.type = windAnims[type]:addInstance()
			start(square)
		end)
	end
	
	local function draw(x,y)
		for i,v in ipairs(squares) do 
			setColor(255,255,255,v.t)
			local size = 30
			local x,y = (x + v.x),(y + v.y)
			v.type:draw(x,y)
		end 
	end
	wind.update_effect = function(dt)
		for i,v in ipairs(squares) do 
			v.type:update(dt)
		end 
	end
	wind.draw_effect = draw
	wind.start = function()
		local w,h = love.graphics.getDimensions()
		local x,y = 0,0
		for i=1,w,size do 
			local size = size*2
			local square = {
			
				x=love.math.random(0,w),
				
				y=love.math.random(h/1.5,h),
				
				t =355,
				delay = 0,
				type = windAnims[love.math.random(3,4)]:addInstance(),
			
			} 
			table.insert(squares,square)
			start(square,love.math.random(3.14,6.28))	
		end 
		
	end 
	wind:start()
	return wind
end 
return wind