local der = {}
function der:new(module,dir,mode,colider,phyWorld,x,y)
	local a = module:new(mode,colider,phyWorld,x,y)
	local img = love.graphics.newImage(dir.."/image.png")
	local anim = newAnimation(img,21,79,0.2,1,4)
	a:setAnimation(anim,1,4)
	a:makeColision(50)
	return a 
end 
return der 