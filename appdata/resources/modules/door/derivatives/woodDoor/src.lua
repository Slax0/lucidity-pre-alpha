local der = {}
function der:new(module,dir,mode,colider,phyWorld,x,y)
	local a = module:new(mode,colider,phyWorld,x,y)
	local img = love.graphics.newImage(dir.."/img.png")
	local w,h = img:getDimensions()
	local quads = {
		closed = love.graphics.newQuad(64,0,32,64,w,h),
		open_left = love.graphics.newQuad(0,0,64,64,w,h),
		open_right = love.graphics.newQuad(96,0,64,64,w,h),
	} 
	a:setImages(quads)
	a:setMainImage(img)	
	a:setRange(70,h)
	a.OnMenu = function(self,menu,ui)
		local b = ui:addButton(nil,2,2,20,20)
		if a.locked then 
			b:setText("set Open")
			b.OnClick = function()
				a.locked = false
				b:setText("set Locked")
			end 
		else 
			b:setText("set Locked")
			b.OnClick = function()
				a.locked = true
				b:setText("set Open")
			end 
		end 
		menu:addItem(b)
		
	end 
	a.OnUpdate = function()
		if a:playerIsInRange() then 
			local gs = Gamestate.current() 
			queryKey("Use",gs.play_keyControler,function()
				if a.status == "closed" then 
					a:open()
				else
					a:close()
				end				
			end) 
		end 
	end 
	return a 
end 
return der 