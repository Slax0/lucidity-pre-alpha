local _ = {}
local function keypad(ui,fn)
	local h = ui:getFont():getHeight() + 3
	local _w,_h = love.graphics.getDimensions()
	local frame = ui:addFrame(nil,_w/2,_h/2,h*3,h*3)
	local no = 0 
	for x = 1,frame.height,h do 
		for y = 1,frame.height,h do 
			no = no + 1 
			local b = ui:addButton(frame,x,y,w,h)
			if fn then fn(b,no) end 
		end 		
	end 
	return frame 
end 
function _:new(module,dir,editor,colider,phyWorld,x,y)
	local station = module:new(editor,colider,phyWorld,x,y,50,50)
	local img = love.graphics.newImage(dir.."/img.png")
	local side = crop(img,0,0,9,37)
	local center = crop(img,8,0,17,37)
	local Que = msgQue:new(0,0,17,37)
	Que:add("Pick me up.")
	local b = false
	station.Free = true
	local C = false 
	station.OnUpdate = function()
		local s = station
		local gs = Gamestate.current() 
		Que.Active = true
		Que:setPosition(editor:camPos(station:getColision():center()))
		Que:update(dt)
		if s._hasPlayer then 
			if not b then 
				Que:add("Pick me up.")
				b = true 
			end 
			queryKey("Use",gs.play_keyControler,function()
				Que:remove()
				Radio.Active = true
				Radio:add("Good. This should make it easy to speak!")
				Radio:add("Say.. Do you have any memories? Perhaps you'd tell me your name..")
				Radio:add("You're a mute huh, or perhaps you don't remember how to speak?")
				Radio:add("Not that I could hear you anyway. This radio has no microphone.")
				Radio:add("Why would I ask you to speak? Because I know you can't.")
				Radio:add("It's not the only thing you cannot do.")
				Radio:add("You might ask; 'Where am I' or 'Who are you'")
				Radio:add("You're in your room ofcourse, don't you remember?!")
				Radio:add("If so you should know your code, the code that opens the door.")
				Radio:add("But you don't know right..")
				Radio:add("How could you, you're just an empty shell, nothing of your former self.")
				Radio:add("Without memories, without cause, without direction.")
				Radio:add("I will give you the code. As long as you promise me.. \n To be loyal to me.")
				Radio:add("To the very end. No matter what happens you listen to me and no-one else.")
				Radio:add("So.. will you be loyal to me?",function()
					local w = love.graphics.getWidth()
					nw = w -w/3
					local ui = editor.ui
					local h = ui:getFont():getHeight()
					local b = ui:addButton(nil,w/2 - nw/2,love.graphics.getHeight()/2 + h + 5,nw,h)
					b.dontDrawB = true
					b:setText("No")
					b.color = colors.lightBlue 
					local b2 = ui:addButton(nil,w/2 - nw/2,love.graphics.getHeight()/2,nw,h)			
					b2.dontDrawB = true
					b2:setText("Yes")
					b2.color = colors.yellow
					b2.OnClick = function()
						C = true 
						b:remove()
						b2:remove()
						if gs._G["OWNDOOR"] then 
							local door = gs._G["OWNDOOR"]
							door.locked = false
							door.Direction = 1 
							door:open(true)
						end
						Radio:add("Thank you, at least I know that I can depend on you.")
						Radio:add("Until you betray me.")
						Radio:add("So let me give you a hint, the body you are in now, is not really your body.")
						Radio:add("You are still stuck in the title screen! Those crystals keep you there trapping you.")
						Radio:add("The only way to get rid of them is to find their owners, and to murder them, \n don't worry though they aren't like you.. they are pests.")
						Radio:add("Since your body is trapped, you will need other methods of killing.")
						Radio:add("Try walking out of the reception, there should be a door outside.")
						Radio:add("Oh, and don't take the fourth floor!")
					end
					b.OnClick = function()
						b:remove()
						C  = true 
						Radio:add("Fine. Then I will end your story here.",function()
							Timer.add(30,function()
								print("Voluntary close.")
								love.event.push("quit")
							end)
						end)
					end
				end)
				station:remove()
			end)
		else 
			 b = false
		end 
	end 
	station.OnDraw = function()
		local x,y = station:getColision():center()
		love.graphics.draw(side,x,y,math.rad(90),1,1,side:getWidth()/2,side:getHeight()/2)
	end 
	return station
end 
return _