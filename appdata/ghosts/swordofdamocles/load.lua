local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/anim.png")
local w,h = RCToWH(image,4,8)
local mob = {
		info = {
			name = "Sword of Damocles",	-- Name on card
			maxHp = 200,	--	100 or higher
			maxMp = 60, -- 100 or higher
			-- Below all stats cam go up to 100
			def = 5,
			atk = 15,
			luck = 5, 
			level = 1,
			--- STOP
			stillFrame = 1,
			skills = {
			{
				name = "Eye of the beholder",
				desc = "Stuns the target for one turn.",
				cost = 30,
				damage = 0,
				type = "dark",
				target = "foe",
				
				func = function(target,stack)
					local mob = stack[1]
					mob:cast()
					mob.OnCast = function()
						target:stun(1)
						stack[4] = true
						mob.OnCast = nil
					end 
				end,
				
			}
			},
			story = "A sword that hangs above the throne, can the king take his seat?", -- The story that shows up 
			rarity = 2, --super common 1 to 10 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 7%,6 = 5%, 7 = 3%, 8 = 2.5% , 9 = 1.5%, 10 = 1% 
			element = "attack", -- The element that shows up on the card when you capture it/buy it.
			weak = {
				"divine",
				"water",
			},
			strong = {
				"ice",
				"fire",
			},
			invin = { 		--Invincibility, you don't lose a heart with this
				"dark",
			},
		},
		w = w,
		h = h,
		states = {
			{
				name = "run",
				anim = {w,h,1/20,1,6},
				func = function() end,
			},
			{
				name = "attack",
				anim = {w,h,1/5,9,16},
				func = function() end,
			},
			{
				name = "death",
				anim = {w,h,1/6,24,32},
				func = function() end,
			},
			{
				name = "cast",
				anim = {w,h,1/5,17,24},
				func = function() end 
			},
			{
				name = "idle",
				anim = {w,h,1/11,1,6},
				func = function() end,
			},
		},
		state = "idle",
		image = image, -- defining own image, for manipulation
	}
return mob