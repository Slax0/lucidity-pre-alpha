local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/anim.png")
local w,h = RCToWH(image,5,8)
local mob = {
		info = {
			name = "Lupus",	-- Name on card
			maxHp = 50,	--	100 or higher
			maxMp = 100, -- 100 or higher
			-- Below all stats cam go up to 100
			def = 6,
			atk = 6,
			luck = 10, 
			level = 3,
			--- STOP
			stillFrame = 1,
			skills = {
			{
				name = "Hell fire",
				desc = "Sets the enemy team on fire.",
				cost = 80,
				damage = 0,
				-- if you want to heal do heal = no, damage = 0
				func = function(target,stack) -- Target is enemy -- tables is the damage meter -- stack: {self, team1, team2, finished}
					local damage = 6
					local mob = stack[1]
					local x,y = mob:getPos()
					mob:cast()
					mob.OnCast = function()
						for i,v in ipairs(target) do 
							local target = v 
							local no = 3
							local i = 0
							target.effect = "fire"
							target.OnChangeTurn = function() 
								local dmg = math.min(100,target.hp/5)
								local type = "fire"
								if i >= no then 
									target.effect = nil
									target.OnChangeTurn = nil
								end
								i = i + 1
								return dmg,type
							end 
						end 
						stack[4] = true
						mob.OnCast = nil
					end 
				end,
				type = "fire",
				target = "team", -- targets: fTeam, team, self, friend, foe  
				image = nil
			},
			{
				name = "Concentrate fire",
				desc = "Burns a target with [30]ATK",
				cost = 20,
				damage = 0,
				type = "fire",
				target = "foe",
				
				func = function(target,stack)
					local mob = stack[1]
					mob:cast()
					mob.OnCast = function()
						if math.random(1,2) == 2 then 
							target:setEffect("fire",2)
						end 
						target:applyDamage(30,"fire")
						stack[4] = true
						mob.OnCast = nil
					end 
				end,
			}},
			story = "A coat to hide the eyes.", -- The story that shows up 
			rarity = 2, --super common 1 to 10 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 7%,6 = 5%, 7 = 3%, 8 = 2.5% , 9 = 1.5%, 10 = 1% 
			element = "divine", -- The element that shows up on the card when you capture it/buy it.
			weak = {
				"dark",
				"attack",
				"water",
			},
			strong = {
				"fire",
			},
			invin = { 		--Invincibility, you don't lose a heart with this
				"divine",
			},
		},
		w = w,
		h = h,
		scale = 1.5,
		states = {
			{
				name = "run",
				anim = {w,h,0.14,1,5},
				func = function() end,
			},
			{
				name = "attack",
				anim = {w,h,0.09,9,14},
				func = function() end,
			},
			{
				name = "death",
				anim = {w,h,0.14,33,40},
				func = function() end,
			},
			{
				name = "cast",
				anim = {w,h,0.14,17,22},
				func = function() end 
			},
			{
				name = "idle",
				anim = {w,h,0.14,25,29},
				func = function() end,
			},
		},
		state = "idle",
		image = image, -- defining own image, for manipulation
	}
return mob