local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/anim.png")
print(_PACKAGE.."/anim.png")
local iw,ih = image:getDimensions()
local rows = 3
local cols = 10
local w,h = iw/cols,ih/rows
local mob = {
		info = {
			name = "_CardSales",	-- Name on card
			maxHp = 100,	--	100 or higher
			maxMp = 100, -- 100 or higher
			-- Below all stats cam go up to 100
			def = 5,
			atk = 5,
			luck = 2, 
			level = 3,
			sourceCrystal = "player",
			--- STOP
			stillFrame = 1,
			skills = {},
			story = "You shouldn't have her!", -- The story that shows up 
			rarity = 4, --super common 1 to 10 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 7%,6 = 5%, 7 = 3%, 8 = 2.5% , 9 = 1.5%, 10 = 1% 
			element = "hah broke ur game", -- The element that shows up on the card when you capture it/buy it.
			weak = {},
			strong = {
				"water",
			},
			invin = { 		--Invincibility, you don't lose a heart with this
				"dark",
			},
		},
		w = w,
		h = h,
		states = {
			{
				name = "idleB",
				anim = {w,h,0.42,22,30},
				func = function() end,
				restFrame = 1,
			},
			{
				name = "idle",
				anim = {w,h,0.9,11,16},
				func = function() end,
				restFrame = 1,
			}
		},
		state = "idleB",
		image = image, -- defining own image, for manipulation
	}
return mob