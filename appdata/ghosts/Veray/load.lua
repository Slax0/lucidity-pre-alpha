local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/anim.png")
local iw,ih = image:getDimensions()
local rows = 4
local cols = 8
local w,h = iw/cols,ih/rows
local mob = {
		info = {
			name = "Veray",	-- Name on card
			maxHp = 25,	--	10 or higher
			maxMp = 10, -- 10 or higher
			-- Below all stats cam go up to 100
			def = 1,
			atk = 15,
			luck = 2, 
			level = 1,
			sourceCrystal = "basic",
			--- STOP
			stillFrame = 1, -- The still frame found in the "Idle" state
			skills = {},
			story = "The wings with eyes can only see as far as heaven.", -- The story that shows up 
			rarity = 2, --super common 1,9 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 5%,6 = 2.5%, 7 = 2%, 8 = 1.8% , 9 = 1.5%, 10 = 1% 
			element = "wind", -- The element that shows up on the card when you capture it/buy it.
			weak = {
				"fire",
				"ice",
				"divine",	-- Weaknesses, do double damage vs you and the attacks/skills do half the damage
			},
			strong = {
				"attack", 	-- increases defense and attack for selected, keep this low to avoid super powers 
			},
			invin = { 		--Invincibility, you dont lose a heart with this
				"stone",
			},
		},
		w = w,
		h = h,
		states = {
			{
				name = "run",
				anim = {w,h,0.09,1,5},
				func = function() end,
			},
			{
				name = "attack",
				anim = {w,h,0.05,9,14},
				func = function() end,
			},
			{
				name = "death",
				anim = {w,h,0.10,17,24},
				func = function() end,
			},
			{
				name = "cast",
				anim = {w,h,0.10,17,24},
				func = function() end 
			},
			{
				name = "idle",
				anim = {w,h,0.10,26,32},
				func = function() end,
			},
		},
		filter = "linear",
		state = "idle",
		image = image, -- defining own image, for manipulation
	}
return mob