local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/anim.png")
local w,h = RCToWH(image,5,8)
local mob = {
		info = {
			name = "Sui-tan",	-- Name on card
			maxHp = 150,	--	100 or higher
			maxMp = 25, -- 100 or higher
			-- Below all stats cam go up to 100
			def = 10,
			atk = 30,
			luck = 5, 
			level = 1,
			--- STOP
			skills = {
				
			},
			story = "Those with many masks are masks themselves.", -- The story that shows up 
			rarity = 10, --super common 1 to 10 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 7%,6 = 5%, 7 = 3%, 8 = 2.5% , 9 = 1.5%, 10 = 1% 
			element = "attack", -- The element that shows up on the card when you capture it/buy it.
			weak = {
				"heal",
				"ice"
			},
			strong = {
				"fire",
			},
			invin = { 		--Invincibility, you don't lose a heart with this
				"dark",
				"divine",
				"restore",
			},
			statIncrease = {
				maxHp = 20,
				maxMp = 2, 
				def = 1,
				atk = 5,
			},
			OnLevelUp = function(level)
			end 
		},
		stillFrame = 1,
		w = w,
		h = h,
		scale = 1,
		mirror = true,
		states = {
			{
				name = "run",
				anim = {w,h,1/11,1,7},
				func = function() end,
			},
			{
				name = "attack",
				anim = {w,h,1/6,17,23},
				func = function() end,
			},
			{
				name = "death",
				anim = {w,h,1/6,25,32},
				func = function() end,
			},
			{
				name = "cast",
				anim = {w,h,1/4,33,38},
				func = function() end 
			},
			{
				name = "idle",
				anim = {w,h,1/4,9,14},
				func = function() end,
			},
		},
		state = "idle",
		image = image, -- defining own image, for manipulation
	}
return mob