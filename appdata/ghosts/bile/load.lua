local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/anim.png")
local w,h = 49,28
local mob = {
		info = {
			name = "Bile",	-- Name on card
			maxHp = 25,	--	10 or higher
			maxMp = 20, -- 10 or higher
			-- Below all stats cam go up to 100
			def = 4,
			atk = 15,
			luck = 10, 
			level = 1,
			--- STOP
			skills = { 
				{
				name = "Break spirit",
				desc = "Deals 30% of target health as ATK to the target.",
				cost = 10,
				damage = 20,
				type = "attack",
				target = "foe",
				
				func = function(target,stack)
					local mob = stack[1]
					mob:cast()
					mob.OnCast = function()
						target:applyDamage((target.hp/100)*30,"attack")
						stack[4] = true
						mob.OnCast = nil
					end 
				end,
				
				},
			},
			story = "a black sludge of body fluids", -- The story that shows up 
			rarity = 5, --super common 1,9 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 5%,6 = 2.5%, 7 = 2%, 8 = 1.8% , 9 = 1.5%, 10 = 1% 
			element = "dark", -- The element that shows up on the card when you capture it/buy it.
			weak = {
				"fire",
				"ice",
				"divine",	-- Weaknesses, do double damage vs you and the attacks/skills do half the damage
			},
			strong = {
				"stone", 	-- increases defence and attack for selected, keep this low to avoid super powers 
			},
			invin = { 		--Invincibility, you don't lose a heart with this
				"dark",
				"water",
			},
		},
		w = w,
		h = h,
		states = {
			{
				name = "run",
				anim = {w,h,0.09,1,13},
				func = function() end,
			},
			{
				name = "attack",
				anim = {w,h,0.10,30,35},
				func = function() end,
			},
			{
				name = "death",
				anim = {w,h,0.10,35,42},
				func = function() end,
			},
			{
				name = "cast",
				anim = {w,h,0.10,30,35},
				func = function() end 
			},
			{
				name = "idle",
				anim = {w,h,0.10,14,28},
				func = function() end,
			},
		},
		state = "idle",
		image = image, -- defining own image, for manipulation
	}
return mob