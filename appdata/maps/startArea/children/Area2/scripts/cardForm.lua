formCard = {}
newFormCard = formCard
w,h = 139,178 
image = love.graphics.newImage(eCurrentMap.."/image/card_animation.png")
anim = newAnimation(image,w,h,1/10,1,4)
anim:setMode("once")
anim:stop()
backQuad = love.graphics.newQuad(556,0,w,h,image:getWidth(),image:getHeight())
squares = {} 
tweenGroup = Flux.group()
active = false
drawBase = false 
stage = 1 
stage2Width = 94*5
charFloating = {
	x = 0,
	y = 0,
	wid = 120,
	transp = 0,
	anim = newAnimation(unpack(FloatingAnimation)),
	crystal = crystal["player"].anim,
	text1 = "", 
	text1pos = 0, 
	text1exp = "You have entered the selection process.",
	ctransp = 0,
	overTransp = 255,
	overTransp2 = 255,
	showMobs = 0,
	circles = {},
	bigCircle = 0,
}
floatingText = {} 
function reset(item)
	item.formCard.squares = {} 
	item.formCard.cards = {} 
	item.formCard.complete = false 
	local col = item:getColision()
	local sx,sy = col:bbox()
	local size = 10 
	for x=0 ,w,size do 
		for y=0,h,size do 
			local square = {
				x = sx  +  x,
				y = sy  +  y,
				oldx = sx  +  x, 
				oldy = sy  +  y,
				size = size,
				t = 0,
				}
			table.insert(item.formCard.squares,square)
		end 
	end
	local w,h = love.graphics.getDimensions()
	local x,y = item.x,item.y
	local choicePoints = {
		{x - w,y - h},
		{x + w,y - h},
		{x + w,y + h},
		{x - w,y + h},
	}
	for i=1,4 do 
		local card = {} 
		card.x,card.y = unpack(choicePoints[i])
		table.insert(item.formCard.cards,card)
	end 
end 
function formCard.load(item)
	local self = {} 
	self.__index = formCard
	local old = item.OnMenu
	if not item.formCard then 
		item.startShow = function()
			local _,t,a = pcall(item.formCard.start,item)
			print(_,t,a)
		end 
		function item:OnMenu(menu,ui)
			if old then old(item,menu,ui) end 
			menu:addChoice("start",function()
				item.startShow = function()
					local _,t,a = pcall(self.formCard.start,item)
					print(_,t,a)
				end 
				item.startShow()
			end)
			menu:addChoice("reload",function()
				formCard = _G["formCard"]
				formCard.load(item)
			end)
			menu:addChoice("stop",function()
				local _,t,a = pcall(self.formCard.stop,item)
				print(_,t,a)
			end)
		end 
	end 
	item.formCard = formCard
	reset(item)
end
function squareStencil(item)
	for i,v in ipairs(item.formCard.squares) do 
		setColor()
		love.graphics.rectangle("fill",v.x,v.y,v.size,v.size)
	end 
end
local delay = 5
local finished = false 
function formCard.start(self)
	local w,h = love.graphics.getDimensions()	
	local function createText(fn)
		local x,y = self:getColision():center()
		local texts = {
			"Happiness",
			"Dreams",
			"Liberty",
			"Safety",
			"Love",
			"Expression",
			"Connection",
			"Forgiveness",
			"Individuality",
			"Consciousness",
			
			"Hate",
			"Envy",
			"Misery",
			"Secrecy",
			"Dread",
			"Desperation",
			"Suppression",
			"Exploitation",
			"Demoralization",
			"Victimization",
			
			"Expectations",
			"Friendship",
			"Judgement",
			"Power",
			"Change",
			"Control",
			"Deviation",
			"Surveillance",
			"Expansion",
		}
		local choicePoints = {
			{x - w,y - h},
			{x + w,y - h},
			{x + w,y + h},
			{x - w,y + h},
		}
		for i,v in ipairs(texts) do 
			local text = {}
			text.x,text.y = unpack(choicePoints[math.random(1,#choicePoints)])
			text.x,text.y = text.x + math.random(-100,100),text.y  + math.random(-100,100)
			text.text = v 
			if i < 10 then 
				text.color = colors.green 
			elseif i < 20 then 
				text.color = colors.yellow
			elseif i <= 30 then 
				text.color = colors.red
			end 
			text.tween = tweenGroup:to(text,math.cos(0.05*i)*4,{x = x,y = y}):oncomplete(function()
				text.tween = nil 
				local clamp 
				for i,v in ipairs(floatingText) do 
					if v.tween then 
						clamp = true 
					end 
				end 
				if not clamp then 
					stage = 4 
					charFloating.bigCircle = 1 
					tweenGroup:to(charFloating,5,{bigCircle = math.max(w/1.5,h/1.5)}):ease("circout"):oncomplete(function()
						if fn then fn() end 
					end)
				end 
			end)
			table.insert(floatingText,text)
		end 
	end 
	local function finish(fin)
		anim.position = 1 
		anim.direction = 1 
		self.formCard.complete = false 

		local x,y = self.x,self.y
		reset(self)
		local choicePoints = {
			{x - w,y - h},
			{x + w,y - h},
			{x + w,y + h},
			{x - w,y + h},
		}
		
		for i,v in ipairs(self.formCard.squares) do 
			local points = choicePoints[math.random(1,#choicePoints)]
			v.x,v.y = unpack(points)
			v.tween = tweenGroup:to(v,delay,{x = v.oldx,y = v.oldy })
		end	
		local col = self:getColision()
		local x,y = col:bbox()
		local no = 1
		local function tweenCard(card,x,y)
			tweenGroup:to(card,1,{x = x,y = y}):oncomplete(function()
				removeFromTable(self.formCard.cardsb,card)
				local last = 0 
				for i,v in ipairs(self.formCard.cardsb) do 
					local card = v			
					if last < i then  
						tweenCard(card,x,y)
						last = i
						if last == #self.formCard.cardsb then 
							Timer.add(0.8,function()
								anim.direction = -1 
								anim:play()
								if fin then 
									fin()
								end 
							end)
						end 	
						break
					end 
				end 
			end)
		end 
		local completed = true 
		for i,v in ipairs(self.formCard.cards) do 
			v.tween = tweenGroup:to(v,delay,{x = x,y = y}):oncomplete(function()
				if completed then 
					completed = false 
					anim:play()
					self.formCard.complete = true
					self.formCard.cardsb = {} 
					local x,y = col:center()
					for i,v in ipairs(playersCards) do 
						local cw = v.width*0.7
						local ch = v.height*0.7
						local gap = 10 
						local card = {
							x = (x - (cw*4) + (gap)*2) + (gap + cw)*(i-1),
							y = (y - h/2) - gap - (ch + gap),
							card = v, 
						} 
						card.x = card.x + w/3
						if i == 4 then 
							card.x = (x - w/2) - cw 
							card.y = y - ch/2 
						elseif i == 5 then 
							card.x = (x + w/2) 
							card.y = y - ch/2 
						elseif i > 5 then 
							card.x = (x - (cw*4) + (gap)*2) + (gap + cw)*(i-4)
							card.y = (y + h/2)
						end 
						table.insert(self.formCard.cardsb,card)
					end 
					local x,y = col:bbox()
					tweenCard(self.formCard.cardsb[1],x,y)
				end 
			end)
		end 
	end 
	local x,y = self:getColision():center()
	charFloating.x = x -20
	charFloating.y = y - (h/2 + 100)
	tweenGroup:to(charFloating,6,{x = x -20,y = y}):after(charFloating,3,{wid = -10}):delay(3)
	:after(charFloating,1,{transp = 255})
	:after(charFloating,7,{text1pos = string.len(charFloating.text1exp)})
	:oncomplete(function() 
		charFloating.text1pos = 0 
		charFloating.text1exp = "I have given you a gift: Eight cards that you may use."
	end)
	:after(charFloating,6,{text1pos = string.len("I have given you a gift. Five cards that you may use.") + 2})
	:oncomplete(function() 
		charFloating.text1pos = 0 
		charFloating.text1exp = "These cards can alter the world to their will."
	end)
	:after(charFloating,5,{text1pos = string.len("These cards can alter the world to their will.")})
	:oncomplete(function() 
		charFloating.text1pos = 0 
		charFloating.text1exp = "You may use them to help those close to you or yourself, the choice is yours.."
	end)
	:after(charFloating,6,{ctransp = 50,text1pos = string.len("You may use them to help those close to you or yourself, the choice is yours..")})
	:oncomplete(function() 
		charFloating.text1pos = 0 
		charFloating.text1exp = "Every choice you make will have consequences."
	end)
	:after(charFloating,7,{text1pos = string.len("Every choice you make will have consequences."),
		ctransp = 105,
	})
	:oncomplete(function() 
		charFloating.text1pos = 0 
		charFloating.text1exp = "Good luck."
	end)
	:after(charFloating,5,{text1pos = string.len("Good luck."),ctransp = 210})
	:after(charFloating,4,{overTransp = 0}):oncomplete(function()
		finish(function()
			tweenGroup:to(charFloating,6,{overTransp2 = 0}):oncomplete(
			function()
				charFloating.overTransp = 255
				stage = 2 
				charFloating.text1pos = 0 
				charFloating.text1exp = "From the depths of your mind new lights emerge."
			end
			):after(charFloating,8,{text1pos = string.len("From the depths of your mind a new light dawns.")})
			:oncomplete(function()
				for i=1,5 do
					local gap = 10
					charFloating.circles[i] = {size = 0,x = 0 + (94 + gap)*(i-1)} 
					tweenGroup:to(charFloating.circles[i],2 + i,{size = 94/2})
				end 
				charFloating.text1pos = 0 
				charFloating.text1exp = "It's children stand before you, treat them well."
			end)
			:after(charFloating,6,{text1pos = string.len("It's children stand before you, treat them well."),showMobs = 5})
			:oncomplete(function()
				charFloating.text1pos = 0 
				charFloating.text1exp = "They will be your finest instruments."
			end)
			:after(charFloating,5,{text1pos = string.len("They will be your finest instruments.")})
			:after(charFloating,4,{overTransp = 0})
			:oncomplete(function()
				stage = 3 
				charFloating.overTransp = 255
				charFloating.overTransp2 = 0 
				tweenGroup:to(charFloating,0.1,{overTransp2 = 255})
				tweenGroup:to(charFloating,5,{bigCircle = math.min(love.graphics.getHeight()/2,love.graphics.getWidth()/2)}):ease("quadinout")
				:oncomplete(function()
					createText(function()
						Gamestate.current():switchMap(findMap("ErisIntro"))
						-- move on to the next map by name of:
					end)
				end)
			end)
		end)
	end)
end 

function formCard.stop(self)
	local squares = self.formCard.squares
	reset(self) 
	for i,v in ipairs(squares) do 
		v.x = v.oldx 
		v.y = v.oldy
		if v.tween then v.tween:stop() end 
	end 
end 

function formCard.update(dt)
	tweenGroup:update(dt)
	anim:update(dt)
	if stage == 1 then
		charFloating.anim:update(dt)
		charFloating.crystal:update(dt)
	elseif stage == 2 then 
		local circles = charFloating.circles
		for i =1,math.floor(charFloating.showMobs) do 
			local mob = playersMobs[i]
			if circles[i].size == 94/2 then 
				mob:update(dt)
			end 
		end 
	end 
end
local defaultFont = love.graphics.newFont(20)
function formCard.draw(item)
	local overall2 = charFloating.overTransp2
	local w,h = love.graphics.getDimensions()
	local col = item:getColision()
	local x,y = col:center()
	local font = love.graphics.getFont()
	setColor(colors.black)
	love.graphics.rectangle("fill",x - w/2,y - h/2,w,h)
	local x,y = col:bbox()
	local s = 1
	if stage == 1 then 
		if overall2 > 0 then 
			love.graphics.setStencil(function() squareStencil(item) end)
			setColor(255,255,255,overall2)
			if drawBase then 
				love.graphics.draw(image,backQuad,x,y,0,s,s)
			end 
			if not item.formCard.complete then 
				for i,v in ipairs(item.formCard.cards) do 
					anim:draw(v.x,v.y,0,s,s)
				end 
			else  
				anim:draw(x,y,0,s,s)
			end 
			love.graphics.setStencil()
			local c = item.formCard.cardsb
			if c then 
				for i,v in ipairs(c) do 
					setColor(255,255,255,overall2)
					love.graphics.draw(v.card:retCanvas(),v.x,v.y,0,0.7,0.7)
				end 
			end 
		end 
		local overall = charFloating.overTransp
		if overall > 0 then 
			local x,y = charFloating.x,charFloating.y
			setColor(255,255,255,math.min(255,overall))
			charFloating.anim:draw(x,y)
			
			love.graphics.setStencil(imagestencil(function()
				charFloating.anim:draw(x,y)
			end))
			setColor(255,255,255,math.min(charFloating.ctransp,overall))
			love.graphics.rectangle("fill",x,y,100,100)
			love.graphics.setStencil()
			
			local wid = charFloating.wid
			setColor(255,255,255,math.min(255 - charFloating.transp,overall))
			love.graphics.rectangle("fill",x - wid,y + 45 + wid,10,10)
			love.graphics.rectangle("fill",x + wid + 25,y + 45 + wid,10,10)
			setColor(255,255,255,math.min(charFloating.transp,overall))
			charFloating.crystal:draw(x - wid - 1,y + 35 + wid)
			setColor(255,255,255,overall)
			love.graphics.setFont(defaultFont)
			local t = string.sub(charFloating.text1exp,1,charFloating.text1pos)
			love.graphics.print(t,x + 20 - defaultFont:getWidth(t)/2 ,y - 150)
			setColor()
			love.graphics.setFont(font)
		else 
			drawBase = true 
		end 
	elseif stage == 2 then 
		local circles = charFloating.circles
		local overall = charFloating.overTransp
		setColor(255,255,255,overall)
		local x,y = charFloating.x,charFloating.y
		love.graphics.setFont(defaultFont)
		local t = string.sub(charFloating.text1exp,1,charFloating.text1pos)
		love.graphics.print(t,x + 20 - defaultFont:getWidth(t)/2 ,y - 150)
		love.graphics.setFont(font)
		local ox = x - stage2Width/2 + (94/2)
		for i,v in ipairs(circles) do 
			love.graphics.circle("fill",ox + v.x,y + 200,v.size)
		end 
		for i =1,math.floor(charFloating.showMobs) do 
			local mob = playersMobs[i]
			local w,h = mob:getDimensions()
			local anim = mob:getAnimation()
			local m = 1 
			if mob.mirror then 
				m = -1
			end 
			if circles[i].size == 94/2 then 
				mob:draw(ox + circles[i].x,y + 200,0,(mob.scale or 1)*m,(mob.scale or 1),mob.width/2,mob.height/2)
			end 
		end 
	elseif stage == 3 then 
		local lw = love.graphics.getLineWidth()
		local x,y = item:getColision():center()
		local transp1 = charFloating.overTransp
		local transp2 = charFloating.overTransp2
		setColor()
		love.graphics.setFont(defaultFont)
		love.graphics.print("Hope.",x-24,y)
		for i,v in ipairs(floatingText) do 
			love.graphics.print(v.text,v.x-24,v.y)
		end
		love.graphics.setFont(font)
		love.graphics.setLineWidth(2)
		love.graphics.circle("line",x,y,charFloating.bigCircle)
		love.graphics.setLineWidth(lw)
	elseif stage == 4 then 
		setColor()
		local x,y = item:getColision():center()
		love.graphics.circle("fill",x,y,charFloating.bigCircle)
	end 
end 
_G["formCard"] = formCard