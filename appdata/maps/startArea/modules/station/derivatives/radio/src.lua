local _ = {}
local function keypad(ui,fn)
	local h = ui:getFont():getHeight() + 3
	local _w,_h = love.graphics.getDimensions()
	local frame = ui:addFrame(nil,_w/2,_h/2,h*3,h*3)
	local no = 0 
	for x = 1,frame.height,h do 
		for y = 1,frame.height,h do 
			no = no + 1 
			local b = ui:addButton(frame,x,y,w,h)
			if fn then fn(b,no) end 
		end 		
	end 
	return frame 
end 
function _:new(module,dir,editor,colider,phyWorld,x,y)
	local station = module:new(editor,colider,phyWorld,x,y,50,50)
	local img = love.graphics.newImage(dir.."/img.png")
	local side = crop(img,0,0,9,37)
	local center = crop(img,8,0,17,37)
	local Que = msgQue:new(0,0,17,37)
	station.Que = Que 
	local b = false
	station.Free = true
	local C = false 
	Que.Active = true
	local function repeatque()
		Que:add("Pick me up.")
		Que:add("Hey!")
		Que:add("Don't listen to it!")
		Que:add("Don't leave me here..",function()
			repeatque()
		end)
	end 
	repeatque()
	station.OnUpdate = function()
		local s = station
		local gs = Gamestate.current() 
		Que.Active = true
		Que:setPosition(editor:camPos(station:getColision():center()))
		Que:update(dt)
		if s._hasPlayer then 
			queryKey("Use",gs.play_keyControler,function()
				Que:remove()
				Radio.Active = true
				if station.OnPickup then 
					print("Launching!")
					station.OnPickup(Radio)
				end 
				station:remove()
			end)
		else 
			 b = false
		end 
	end 
	station.OnDraw = function()
		local x,y = station:getColision():center()
		love.graphics.draw(side,x,y,math.rad(90),1,1,side:getWidth()/2,side:getHeight()/2)
	end 
	return station
end 
return _