local door = {}
door.__index = door
function door:new(mode,colider,phyWorld,x,y,zmap,width,height,scalex,scaley)
		local object = {
		x = x or 0,
		y = y or 0,
		width = width or 20,
		height = height or 150,
		colider = colider,
		phyWorld = phyWorld,
		body = false,
		image = false,
		image2 = false,
		scalex = 1,
		scaley = 1,
		shape = false,
		free = true,
		status = "closed",
		fixture = nil,
		zmap = zmap or 1,
		itype = "none",
		flip = false,
		animation = false,
		nRange = true,
		type = "door",
	}
	setmetatable(object,door)
	object:makeColision()
	return object 
end 
function door:setFlip(bool)
	self.flip = bool
end
function door:setImage(image,image2,fw,fh,sx,sy)
	self.itype = "image"
	self.image_o = image
	self.image_c = image2
	self.scalex = sx or 1
	self.scaley = sy or 1
	local w = fw or image:getWidth()
	local h = fh or image:getHeight()
	self.width = w*self.scalex
	self.height = h*self.scaley
end
function door:setMainImage(image)
	self.toCut = image 
end 
function door:setImages(mode,image,fw,fh,sx,sy)
	self.itype = "image"
	if type(mode) == "table" then
		if mode["closed"] then 
			local image = mode["closed"]
			self.image_c = image
			local w,h 
			if image:type() == "Quad" then 
				local _,_,n,m = image:getViewport()
				w,h = n,m
			else 
				w = fw or image:getWidth()
				h = fh or image:getHeight()
			end 
			self.scalex = sx or 1
			self.scaley = sy or 1
			self.width = w*self.scalex
			self.height = h*self.scaley
		end 
		if mode["open_right"] then
			self.image_or = mode["open_right"]
		end 
		if mode["open_left"] then 
			self.image_ol = mode["open_left"] 
		end 
	else 
		if mode == "closed" then 
			self.image_c = image
			local w = fw or image:getWidth()
			local h = fh or image:getHeight()
			self.scalex = sx or 1
			self.scaley = sy or 1
			self.width = w*self.scalex
			self.height = h*self.scaley
		elseif mode == "open_right" then 
			self.image_or = image
		elseif mode == "open_left" then 
			self.image_ol = image 
		end 
	end 
	if fw and fh then 
		self.width = fw*self.scalex
		self.height = fh*self.scaley
	end 
end
function door:setRange(w,h)
	self.rangeW = w 
	self.rangeH = h 
end 
function door:setAnimation(animation,closeFrame,openFrame,fw,fh,sx,sy)
	local sx = sx or self.scalex
	local sy = sy or self.scaley
	self.itype = "animation"
	self.scalex = sx
	self.width = fw or animation:getWidth()
	self.height = fh or animation:getHeight()
	self.image_c = closeFrame or 1
	self.image_o = openFrame or animation:getSize()
	animation:setMode("once")
	animation:stop()
	animation:reset()
	self.animation = animation
end
function door:update(dt)
	local anim = self.animation
	local mask 
	local dt = love.timer.getDelta()
	if self.fixture then 
		mask = self.fixture:getMask()
	end 
	if anim then
		if anim:getCurrentFrame() == self.image_c and mask ~= 1 then
			if self.fixture then self.fixture:setMask(1) end 
		end
		if anim:getCurrentFrame() == self.image_o and mask ~= 16 then
			if self.fixture then self.fixture:setMask(16) end 
		end
		anim:update(dt)
	end
	if self.onUpdate then 
		self.onUpdate(dt,self)
	end
	if self.OnUpdate then 
		self.OnUpdate(dt,self)
	end

	if not self.parent then 
		local w,h = findDimensions(self.door)
		local modx = (self.modx or 0)
		local mody = (self.mody or 0)
		self.topX,self.topY = incrementPos(self.x,self.y,w,h,16)
		self.dx = self.topX + modx
		self.dy = self.topY + mody
		if self.tempCol then self.tempCol:moveTo(self.topX + modx,self.topY + mody) end
		if self.range then self.range:moveTo(self.topX + modx,self.topY + mody) end 
		if self.door then self.door:moveTo(self.topX + modx,self.topY + mody) end 
	else 
		self.dx = self.x 
		self.dy = self.y
		if self.door and self.range then 
			self.door:moveTo(self.dx,self.dy)
			self.range:moveTo(self.dx,self.dy)
		end 
	end 
	if self.body then self.body:setPosition(self.dx,self.dy) end 
end
function door:getColision()
	if not self.tempCol and not self.parent then self.tempCol = self.colider:addRectangle(self.x,self.y,self.width,self.height) end 
	local col = self.tempCol or self.door 
	return 	col 
end 
function door:makeColision(width,height)
	local x = self.x
	local y = self.y
	local world = self.phyWorld
	local nwidth = self.rangeW or width or self.width*4
	local nheight = self.rangeH or height or self.height
	if self.range then 
		self.colider:remove(self.range)
	end 
	if self.door then
		self.colider:remove(self.door)
	end 
	if self.body then 
		self.body:destroy()
	end 
	self.door = self.colider:addRectangle(x - self.width/2,y- self.height/2,self.width,self.height)
	self.colider:addToGroup(tostring(self),self.door)
	if world then
		self.range = self.colider:addRectangle(x - nwidth/2,y - nheight/2,nwidth,nheight)
		self.body = love.physics.newBody(world,x,y)
		self.colider:addToGroup(tostring(self),self.range)
		local shape = love.physics.newRectangleShape(self.width,self.height)
		self.shape = shape
		self.fixture = love.physics.newFixture(self.body,shape)
		local salf = tostring(self)
		self.fixture:setUserData("door")
		function self.range.getUserData()
			return {source = self,part = "range"}
		end 
		function self.door.getUserData()
			return {source = self}
		end
	end 
end
function door:setMirror(r)
	self.mirror = r 
end 
function door:setReflect(r)
	self.reflect = r 
end 
function door:draw(wireframe)
	local mirror = 1
	local reflect = 1
	if self.mirror then 
		mirror = -1
	end 
	if self.reflect then 
		reflect = -1
	end 
	local scalex = self.scalex*mirror
	local scaley = self.scaley*reflect
	local x,y = self.dx,self.dy
	if wireframe then
		if self.status == "closed" then
			love.graphics.setColor(5,200,100)
		else
			love.graphics.setColor(0,0,255)
		end
		if self.range then self.range:draw("line") end 
		if self.door then self.door:draw("fill") end 
		if self.shape then
			love.graphics.push()
			love.graphics.translate(x,y)
			love.graphics.setColor(255,0,0)
			love.graphics.polygon("line",self.shape:getPoints())
			love.graphics.pop()
		end
		love.graphics.setColor(255,255,255)
	end
	local image
	local ox = 0 
	if self.status == "closed" and self.image_c then
		image = self.image_c
	elseif string.find(self.status,"open") then
		if self.status == "open_left" and self.image_ol then 
			image = self.image_ol
			if image:type() == "Quad" then 
				local _,_,w,_ = image:getViewport()
				ox = -w 
			else
				ox = -image:getWidth()
			end 		
			ox = ox + self.width
		elseif self.status == "open_right" and self.image_or then 
			image = self.image_or
		end
		if not image and self.image_o then 
			image = self.image_o
		end
	end
	if self.itype == "image" and self.toCut then
		love.graphics.draw(self.toCut,image,x + ox,y,0,scalex,scaley,(self.width/2)*math.abs(self.scalex),(self.height/2)*math.abs(self.scaley))
	elseif self.itype == "image"  then 
		love.graphics.draw(image,x,y,0,scalex,scaley,(self.width/2)*math.abs(self.scalex),(self.height/2)*math.abs(self.scaley))
	elseif self.itype == "animation" then
		self.animation:draw(x,y,0,scalex,scaley,(self.width/2)*math.abs(self.scalex),(self.height/2)*math.abs(self.scaley))
	end
	if self.onDraw then
		self.onDraw(self,dt)
	end
end
function door:open(force)	
	print("OPENING")
	print(self.locked)
	if not self.locked then 
		local x = 0
		local y = 0
		local clamp
		local range = self:playerIsInRange()
		if self.itype == "animation" and force then
			clamp = true
		end
		if force or range or not self.nRange then
			if self.free or force then
				if self.status == "closed" then
					self.status = "open"
					if self.Direction == 1 then 
						self.status = "open_left"
					else
						self.status = "open_right"
					end 
				end
				if clamp then
					self.animation:setMode("once")
					self.animation:play()
				else
					self.fixture:setMask(16)
				end
			end
		end
	end 
end
function door:close(force)
	print("CLOSING!!")
	if not self.locked then 
		local clamp
		self.free = true 
		local range = self:playerIsInRange()
		if self.itype == "animation" and force then
			clamp = true
		end
		if force or range or not self.nRange then
			if self.free or force then
				if string.find(self.status,"open") then
					self.status = "closed"
				end
				if clamp then
					self.animation:setMode("reverse2")
					self.animation:play()
				else
					self.fixture:setMask(1)
				end
			end
		end
	end 
end
function door:moveTo(x,y)
	if self.body then
		self.body:setPosition(x,y)
		local body = self.body
	end
	self.x = x
	self.y = y
	if self.range then
		self.range:moveTo(self.x,self.y)
	end
	if self.door then
		self.door:moveTo(self.x,self.y)
	end
end
function door:needsRange(bool)
	self.nRange = bool
end
function door:collidesWithPlayer()
	return self.colWPlayer 
end 
function door:playerIsInRange()
	return self.plIRange
end 
function door:HCol(dt,a,b)
	local player,part,other = checkPlayerCol(a,b)
	local d 
	if other.getUserData then 
		d = other:getUserData()
	end 
	if d and player then
		if d.source == self then 
			if d.part == "range" then 
				self.plIRange = true   
				local x,y = player:getColision():bbox()
				local sx = self:getColision():bbox()
				if x > sx then 
					self.Direction = 1 
				else 
					self.Direction = -1
				end 
			else 
				self.colWPlayer = true  
			end 
		end 
	end  
end 
function door:HStop(dt,a,b) 
	local player,part,other = checkPlayerCol(a,b)
	local d 
	if other.getUserData then 
		d = other:getUserData()
	end 
	if d and player then
		if d.source == self then 
			if d.part == "range" then 
				self.plIRange = false   
			else 
				self.colWPlayer = false  
			end 
		end 
	end   
end  
function door:remove()
	if self.body and not self.body:isDestroyed() then
		self.body:destroy()
	end
	if self.door then
		Collider:remove(self.door)
	end
	if self.range then
		Collider:remove(self.range)
	end
end
function door:save()
	return {x = self.x,y = self.y,zmap = self.zmap,locked = self.locked}
end 
function door:load(t)
	self.x = t.x 
	self.y = t.y 
	self.zmap = t.zmap 
	self.locked = t.locked
	self:makeColision()
end 
function door:setPos(x,y)
	self:moveTo(x,y)
end
return door 