local rayCast = {}
function rayCast:new(phyWorld,x,y)
	local self = {}
	self.x = x
	self.y = y
	self.rays = {}
	self.phyWorld = phyWorld
	setmetatable(self,{__index = rayCast})
	return self
end
function rayCast:setPosition(x,y)
	self.x = x
	self.y = y
end
function rayCast:setRaytable(tables)
	-- Table: (x2,y2) uses origin for x1,y1
	self.rays = tables
end
function rayCast:remove()
	for i in ipairs(self) do
		self[i] = nil
	end
end
function rayCast:setMask(func, ...)
	self.mask = func(...)
end
function rayCast:cast()
	self.hitlist = {}
	for i in ipairs(self.rays) do
		self.rays[i].data = nil
		self.rays[i].mature = nil
	end
	local x1 = self.x
	local y1 = self.y
	local world = self.phyWorld
		local function savePoint(x,y,xn,yn,f)
		local t = {
			x = x,
			y = y,
			xn = xn,
			yn = yn,
			f = f
		}
		return t
	end
	for i in ipairs(self.rays) do
	local x2 = self.rays[i].x
	local y2 = self.rays[i].y
	local n = 1
		local maxDist = nil
		local maxPoint = nil
		world:rayCast(x1,y1,x2,y2,function(fixture,x,y,xn,xy,fraction) 
			local clamp = false
			if self.mask then
				if type(self.mask) == "table" then
					local c = 0
					for i,v in ipairs(self.mask) do 
						if c <= 1 then
							local cond = self.mask[i](fixture,x,y,xn,xy,fraction)
							if cond then
								c = c + 1
							end
						else
							clamp = true
						end
					end
					if c >= 1 then
						clamp = true
					end
				else
					clamp = self.mask(fixture,x,y,xn,xy,fraction)
				end
			end
			if clamp then
			else
				if maxDist then
					if fraction < maxDist then
						maxDist = fraction
						maxPoint = savePoint(x,y,xn,yn,fraction)
						
					end
				else
					maxDist = fraction
					maxPoint = savePoint(x,y,xn,yn,fraction)
				end
			end
			return 1
		end)
		if maxPoint then
			self.rays[i].mature = true
			self.rays[i].data = maxPoint
			table.insert(self.hitlist,maxPoint)
		end
	end
end
function rayCast:returnHitlist()
	return self.hitlist,self.rays
end
function rayCast:draw()
	local hit = self.hitlist or {}
	local ray = self.rays
	for i in ipairs(ray) do
	local var = ray[i]
	if var.mature then
			local data = var.data
			local nextNode = ray[i + 1]
			local x1 = data.x
			local y1 = data.y
			if nextNode and nextNode.mature then
				local x = nextNode.data.x
				local y = nextNode.data.y
				love.graphics.line(x1,y1,x,y)
			end
		end
	end
end
function rayCast:drawRange()
for i in ipairs(self.rays) do
	local x = self.rays[i].x
	 local y = self.rays[i].y
	 if self.rays[i].mature then
		love.graphics.setColor(colors.red)
	 end
	 love.graphics.print(i,x,y)
	 love.graphics.setColor(colors.green)
	end
end
return rayCast