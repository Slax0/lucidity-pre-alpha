local gs = Gamestate.current()
local canvas
local w,h
local offset = 16
local base = 3.5
local large = base*5
local med = base*2
local small  = base  
local rectangles = {}
local rand_color = {
	"yellow",
	"red",
	"darkRed",
	"orange",
	
}
local function tweenRectangle(rectangle,tween,speed,size,vary)
	local dist = math.abs(findDistance(rectangle.x,w-offset))
	local delay = dist/speed 
	if vary then 
		delay = math.random(delay,delay*2)
	end 
	tween:to(rectangle,delay,{x =  w-offset-2}):oncomplete(function()
		rectangle.x = -size
		tweenRectangle(rectangle,tween,speed,size)
	end):ease("linear")
end 
_G["door_load"] = function(self)
	w,h = findDimensions(self:getColision())
	canvas = love.graphics.newCanvas(w-offset,h)
	rectangles = {
	layer1 = {
		{x = -15,y = h - large - 25},
		{x = w/2-med,y = h/2 + large/2 },
		{x = w/2,y = large - 10},
	} ,
	layer2 = {},
	layer3 = {},
	}
	local t = gs:getTween()
	for i,v in ipairs(rectangles.layer1) do 
		tweenRectangle(v,t,25,large)
	end
	for i=1,30 do 
		local f = (i/5)
		local s = math.random(-med*f,med*f)
		
		local s2 = math.random(0,-med*f) 
		if i %2 == 0 then 
			r = {x = -5 + s,y = h - med - 5 + s2}
		elseif i%3 == 0 then 
			r = {x = w/2-med + s,y = h/2 + med/2 + s2 }
		else
			r  = {x = w/2 + s ,y = med + s2}
		end 
		r.color = colors[rand_color[math.random(1,#rand_color)]]
		tweenRectangle(r,t,20,med,true)
		table.insert(rectangles.layer2,r)
	end 
	for i = 1,90 do 
		local f = (i/5)
		local w = math.random(-small,w-offset)
		local h = math.random(-small,(h-small))
		local r = {x = w,y = h}
		tweenRectangle(r,t,15,small,true)
		table.insert(rectangles.layer3,r)
	end 
end
local rectangle = love.graphics.rectangle
_G["door_update"] = function()
	canvas:renderTo(function()
		setColor(colors.black)
		love.graphics.rectangle("fill",0,0,w,h)
		for i,v in ipairs(rectangles.layer3 or {}) do 
			local r,g,b = unpack(colors.green)
			setColor(r,g,b,200)
			rectangle("fill",v.x,v.y,small,small)
		end 
		for i,v in ipairs(rectangles.layer2 or {}) do 
			local r,g,b = unpack(v.color)
			setColor(r,g,b,200)
			rectangle("fill",v.x,v.y,med,med)
		end 
		for i,v in ipairs(rectangles.layer1 or {}) do 
			local r,g,b = unpack(colors.blue)
			setColor(r,g,b,240)
			rectangle("fill",v.x,v.y,large,large)
		end 
		setColor()
	end)
end
_G["door_draw"] = function(self)
	if not canvas then 
		_G["door_load"](self)
	end 
	local x,y = self:getColision():center()
	love.graphics.setStencil(function()
		love.graphics.circle("fill",x,y-13,(w-10)/2)
		love.graphics.rectangle("fill",x - (w-offset)/2,y-5,(w-offset),h/2+5)
	end)
	love.graphics.draw(canvas,x,y,0,1,1,(w -offset)/2,h/2)
	love.graphics.setStencil()
	self:draw()

end 
_G["door_colide"] = function(target)
	local x,y = target:getColision():center()
	local c = gs:clampCamera()
	c.x = x
	c.y = y
	local PL = deepCopy2(gs:getPlayer())
	local w,h = 94,94
	local ox = ((w+5)*5) + 5 
	local ownTeam = {} 
	local ui = gs.ui 
	local frame = ui:addFrame(" Your team ",nil,nil,ox,h+30)
	local function createList(frame2,ghosts,x,y)
		local list = ui:addList(frame2,x,y,w,h)
		for i,v in ipairs(ghosts) do 
			local ghost
			if v.name:find("_") ~= 1 then 
				ghost,data = loadGhost:byName(v.name)
				data.name = v.name 
			end 
			if ghost and ghost.draw and ghost.update then 
				local b = ui:addButton(nil,0,0,w,h)
				b.data = data 
				local sx,sy = 94/ghost.width,94/ghost.height 
				b.OnDraw = function()
					local x,y = b:getColision():bbox()
					ghost:draw(x,y,0,sx,sy) 
				end
				b.OnUpdate = function(dt)
					ghost:update(dt/2)
				end
				b.OnClick = function()
					list:remove()
					local nb = ui:addButton(frame2,x,y,94,94)
					nb.OnClick = function()
						createList(frame2,ghosts,x,y)
						nb:remove()
					end
					nb.OnUpdate = function(dt)
						ghost:update(dt)
					end					
					nb.OnDraw  = function()
						local x,y = nb:getColision():bbox()
						ghost:draw(x,y,0,sx,sy) 
					end 
					nb.dontDraw = true
					nb.data = b.data 
				end 
				list:addItem(b)
			end 
		end 
	end
	local function createListCards(frame,cards,x,y)
		local list = ui:addList(frame,x,y,w,h)
		for i,v in ipairs(cards) do  
			local card 
			if v.name:find("_") ~= 1 then 
				local sucess,card = pcall(loadCards.new,loadCards,v.name)
				if sucess then 
					local b = ui:addButton(nil,0,0,w,h)
					local sx,sy = 94/card.width,94/card.height 
					b.OnDraw = function()
						card:draw(b.x,b.y,0,sx,sy)
					end
					b.OnClick = function()
						list:remove()
						local nb = ui:addButton(frame,x,y,w,h)
						nb.data = card
						nb.OnDraw = function()
							card:draw(nb.x,nb.y,0,sx,sy)
						end
						nb.OnClick = function()
							nb:remove()
							createListCards(frame,cards,x,y)
						end
						nb.dontDraw = true 
					end 
					list:addItem(b)
				end 
			end 
			if card and card.draw and card.update then 
			end 
		end 
	end
	
	local ghosts = findItem("ghost",true)
	for no=1,5 do 
		local x,y = 5 + (w+5)*(no-1),25
		createList(frame,ghosts,x,y)
	end
	local frame2 = ui:addFrame(" Their team ",frame.x,frame.y + frame.height,ox,h + 30)	
	for no=1,5 do 
		local x,y = 5 + (w+5)*(no-1),25
		createList(frame2,ghosts,x,y)
	end
	local c_frame = ui:addFrame("Cards",frame.x + frame.width/2 + 94/2 + 15,frame.y,104,ox + 25)
	local cards = findItem("card",true)
	for no=1,5 do 
		local x,y = 5,25 + (w+5)*(no-1)
		createListCards(c_frame,cards,x,y)
	end
	local start_battle = ui:addButton(nil,frame2.x,frame2.y + 120,100,ui:getFont():getHeight()*2)
	start_battle:setText("Start")
	local function getItems(frame)
		local items = {} 
		for i,v in pairs(frame.children) do 
			if v.type == "button" then 
				table.insert(items,v.data)
			end 			
		end 
		return items
	end 
	start_battle.OnClick = function()
		local items = getItems(frame2) 
		local ownteam = getItems(frame)
		for i,v in ipairs(ownteam) do 
			print(v.name)
		end 
		local cards = getItems(c_frame)
		gs:OnBattleMode(items,PL,ownteam,cards)
	end 
end