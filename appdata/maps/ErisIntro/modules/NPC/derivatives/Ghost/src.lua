local _ = {}
function _:new(module,dir,editor,colider,phyWorld,x,y)
	local ghost = module:new(editor,colider,phyWorld,x,y,"Avictus")
	ghost.name = "Avictus"
	local name = "Avictus"
	local oldname = "Avictus" 
	local defaultAttack = ([[
	target = {}
	selfOld = nil 
	return {		
		new = function(a)
			clearTable(target) 
			if not selfOld then selfOld = {x = self.x,y = self.y} end 	
			target.source = a.source 
		end,
		update = function(dt)
			if target then
				self:move(target.source.x or 0,target.source.y or 0)
			end 
		end,
		OnLost = function(trg)
			if target then 
				if trg.source == target.source then 
					self:move(selfOld.x or 0 ,selfOld.y or 0)
					target = {}
				end 
			end 
		end 
	}]])
	local defaultFriend = [[]]
	function ghost:OnMenu(menu,ui)
		local x,y = love.graphics.getDimensions()
		x = x/2
		y = y/2
		print(menu,ui)
		ui:addFrame(nil,0,0,100,100)
		menu:addChoice("Set Ghost",function()
			local f = ui:getFont()
			local h = f:getHeight()
			local frame = ui:addFrame("Set Ghost",x,y,300,h*2 + 40)
			local n = ui:addTextinput(frame,5,20,frame.width-10,h+5,name)
			
			frame:setHeader()
			frame:addCloseButton()
			frame.dragEnabled = true 
			
			local w = f:getWidth("Done")+10
			local b = ui:addButton(frame,frame.width/2-w/2,frame.height-(h+10),w,h+5)
			b.text = "Done"
			b.OnClick = function()
				name = n:getText()
				if name ~= oldName then
					ghost:loadByName(nil,name)
				end
			end
		end)
		menu:addChoice("Set Ghost",function()
			local mode 
			local f = ui:getFont()
			local h = f:getHeight()
			local frame = ui:addFrame("Set behaviour",x,y,400,400)
			frame:setHeader()
			frame:addCloseButton()
			frame.dragEnabled = true
			
			local mlist = ui:addList(frame,5,20,frame.width/2-5,50)
			local ox = frame.width/2 + 5 
			local target = ui:addList(frame,ox,25,frame.width/2-5,frame.height - 200)
			local oy = 25 + frame.height-200
			local addTarget = ui:addButton(frame,ox,oy+(20 + (h)),frame.width/2-5,(h+10))
			
			addTarget:setText("Add target")
			addTarget.OnClick = function()
				target:addChoice(addTarget.text,function(self)
					self:remove()
					target:refresh()
				end) 
			end 
			local targetInput = ui:addTextinput(frame,ox,oy +5,frame.width/2-5,h+5)
			targetInput:setText("Type target name")
			local w = f:getWidth("Done")+10
			local a = ui:addTextinput(frame,5,125-50,frame.width/2-5,frame.height-(h+20) -80,"Insert script here.")
			a.text = defaultAttack
			local b = ui:addButton(frame,frame.width/2-w/2,frame.height - (h+10),w,h+10)
			b:setText("Apply")
			b.OnClick = function()		
				if mode == "whitelist" then  
					ghost.friends = {}
					for i,v in ipairs(target.items) do 
						 ghost:addTo("whitelist",v)
					end
					ghost.embrace = editor.triggerAdmin:exec(a:getText(),ghost)
				else	
					ghost.targets = {}
					for i,v in ipairs(target.items) do 
						 ghost:addTo("blacklist",v)
					end
					ghost.attack = editor.triggerAdmin:exec(a:getText(),ghost)			
				end 
			end
			a.text = defaultFriend
			mode = "blacklist"
			for i,v in ipairs(ghost.targets) do 
				target:addChoice(v,function(self)
					target:removeItem(self)
				end)
			end 	
			mlist:addChoice("whitelist",function()
				target:clear()
				mode = "whitelist"
				for i,v in ipairs(ghost.friends) do 
					target:addChoice(v,function(self)
						target:removeItem(self)
					end)
				end 
				a.text = defaultFriend
			end)
			mlist:addChoice("blacklist",function()
				target:clear()
				mode = "blacklist"
				for i,v in ipairs(ghost.targets) do 
					target:addChoice(v,function(self)
						target:removeItem(self)
					end)
				end 				
				a.text = defaultAttack
			end)
		end)
	end
	ghost.attack = editor.triggerAdmin:exec(defaultAttack,ghost)
	ghost:setAttackRange(0,0,1500,500)
	ghost:addTo("blacklist","Yume")
	local oldS = ghost.save
	function ghost:save()
		local t = oldS(ghost)
		t.ghost_name = name 
		return t 
	end
	local oldL = ghost.load
	function ghost:load(t,dead)
		oldL(ghost,t,dead)
		if not dead then 
			name = t.ghost_name or "Avictus"
			ghost:loadByName(nil,name)
		end 
	end
	return ghost
end 
return _