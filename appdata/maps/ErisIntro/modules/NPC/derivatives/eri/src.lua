local _ = {}
function _:new(module,dir,editor,colider,phyWorld,x,y)
		local scalex = 1 
	local scaley = 1 
	local image = love.graphics.newImage(dir.."/eri.png")
	local width,height = RCToWH(image,2,10)
	local eri = module:new(editor,colider,phyWorld,x,y,width,height)
	eri.bodyWidth = 18
	eri.bodyHeight = 45 
	
	local w,h = width,height
	local runing = newAnimation(image,w,h,0.09,1,8)
	local jumping = newAnimation(image,w,h,0.7,9,9)
	local falling = newAnimation(image,w,h,0.7,10,10)
	local still = newAnimation(image,w,h,0.5,11,18)
	local xchange = newAnimation(image,w,h,1,19,19)
	local states = {{name = "fall",anim = falling, },{name = "jump",anim = jumping},{name = "idle",anim = still},
	{name = "run",anim = runing},
	{name = "x_change", anim = xchange}}
	eri:setStates(states)
	eri.name = "Eri"
	eri.mirror = true 
	
	eri.speed_limit = 470 
	eri.jump_velocity = 100 
	
	eri:setAttackRange(0,0,800,500)
	local anim = stock_effects.jump.anim:addInstance()
	eri:setJumpEffect(anim)
	
	-- ghost:addTo("blacklist","Yume")
	
	local target = {} 
	-- local selfOld 
	-- ghost.attack = {
		-- new = function(a)
			-- clearTable(target) 
			-- if not selfOld then selfOld = {x = self.x,y = self.y} end 	
			-- target.source = a.source 
		-- end,
		-- update = function(dt)
			-- if target then
				-- ghost:move(target.source.x or 0,target.source.y or 0)
			-- end 
		-- end,
		-- OnLost = function(trg)
			-- if target then 
				-- if trg.source == target.source then 
					-- ghost:move(selfOld.x or 0 ,selfOld.y or 0)
					-- target = {}
				-- end 
			-- end 
		-- end 
	-- }
	
	return eri
end 
return _