local yume = {}
function yume:new(module,dir,mode,colider,phyWorld,x,y)
	local width = 32
	local height = 49
	local scalex = 1 
	local scaley = 1 
	local image = love.graphics.newImage(dir.."/yume.png")
	local yume = module:new(mode,colider,phyWorld,x,y,width,height,scalex,scaley)
	yume.bodyWidth = 18
	yume.bodyHeight = 45 
	yume:makeColision()
	-- newAnimation(image,fw, fh, delay,startFrame,finalFrame)
	local w,h = width,height
	
	local runing = newAnimation(image,w,h,0.09,1,8)
	local jumping = newAnimation(image,w,h,0.7,9,9)
	local falling = newAnimation(image,w,h,0.7,10,10)
	local still = newAnimation(image,w,h,0.5,11,18)
	local xchange = newAnimation(image,w,h,1,19,19)
	local wall = newAnimation(image,w,h,1,21,22)
	
	local states = {{name = "falling",anim = falling, },{name = "jumping",anim = jumping},{name = "idle",anim = still},
	{name = "moving",anim = runing},
	{name = "x_change", anim = xchange},
	{name = "wall",anim = wall}}
	yume:setStates(states)
	yume.name = "Yume"
	
	local anim = stock_effects.jump.anim:addInstance()
	yume:setJumpEffect(anim)
	yume.cModY = -love.graphics.getHeight()/5 
	return yume
end 
return yume