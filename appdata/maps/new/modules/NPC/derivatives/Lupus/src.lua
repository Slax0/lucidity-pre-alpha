local _ = {}
function _:new(module,dir,editor,colider,phyWorld,x,y)
	local ghost = module:new(editor,colider,phyWorld,x,y,"Lupus")
	ghost.name = "Lupus"
	ghost:setAttackRange(0,0,1500,500)
	ghost:addTo("blacklist","Yume")
	local target = {} 
	local selfOld 
	ghost.attack = {
		new = function(a)
			clearTable(target) 
			if not selfOld then selfOld = {x = self.x,y = self.y} end 	
			target.source = a.source 
		end,
		update = function(dt)
			if target then
				ghost:move(target.source.x or 0,target.source.y or 0)
			end 
		end,
		OnLost = function(trg)
			if target then 
				if trg.source == target.source then 
					ghost:move(selfOld.x or 0 ,selfOld.y or 0)
					target = {}
				end 
			end 
		end 
	}
	
	return ghost
end 
return _