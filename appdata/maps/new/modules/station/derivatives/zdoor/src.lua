local _ = {}
local function findMap(str)
	local _ = string.find(str,".*/")
	if _ then 
		str = string.sub(str,_)
	end 
	return str 
end 
local function findChild(ch)
	if not love.filesystem.exists(ch) then 
		local map = Gamestate.current().map
		local lfs = love.filesystem
		local c = findMap(ch)
		if not lfs.exists(map.."/children/"..c) then 
			if lfs.exists("maps/"..c) then 
				copyFiles("maps/"..c,map.."/children/"..c) 
			else
				print("MAP "..c.." IS NOT FOUND PLEASE CHECK: maps/"..c)
			end 
		end 
	end 
end 
function _:new(module,dir,editor,colider,phyWorld,x,y)
	local posData = {y = 0}
	loopRange(posData,10,"y",0.5)
	local img = love.graphics.newImage(dir.."/img.png")
	local station = module:new(editor,colider,phyWorld,x,y,100,100)
	station:setImage(img)
	local ui = editor.ui
	local frame 
	local iconR = icons["indicator_red"]
	local iconB = icons["indicator_blue"]
	function station.OnUpdate(dt)
		local self = station
		if self._hasPlayer and self.Can_Use then 
			local gs = Gamestate.current() 
			local c 
			if self.child then 
				self.OnDraw = function() 
					local w,h = iconR:getDimensions()
					love.graphics.draw(iconR,station.x ,station.y - station.height/2 - h/2,math.rad(90),1,1,w/2,h/2)
				end 
				queryKey("Crouch",gs.play_keyControler,function()
					c = true 
					self:useC()
				end)
			end 
			if self.parent then
				self.OnDraw = function() 
					local w,h = 20,20 
					setColor(colors.green)
					love.graphics.rectangle("fill",station.x + w,station.y - station.height/2 - h,w,h)
					setColor()
				end 
				if not c then 
					queryKey("Move up",gs.play_keyControler,function()
						self:useP()
					end)
				end 
			end 
		else 
			self.OnDraw = nil 
		end 
	end 
	local zd = station
	function zd:setChild(c)
		if c then 
			local map = Gamestate.current().map
			local lfs = love.filesystem
			if not lfs.exists(map.."/children/"..c) then 
				if lfs.exists("maps/"..c) then 
					copyFiles("maps/"..c,map.."/children/"..c) 
				else
					print("MAP "..c.." IS NOT FOUND PLEASE CHECK: maps/"..c)
				end 
			end 
			print("Set Child:",c)
			self.child = c
		else 
			print("No value for Child please make sure its a string of the map directory!")
		end 
	end 
	function zd:setParent(p,block)
		if not block then 
			local map = Gamestate.current().map
			self.parent = p
		else
			self.parent = p 
		end 
	end
	function zd:getViableParents()
		local map = Gamestate.current().map
		local t   = {}
		if string.find(map,"/children/") then 
			t = getParents(map)
			table.remove(t,1)
		end 
		return t 
	end 
	function zd:getViableChildren()
		local t = {} 
		local map = Gamestate.current().map
		local a_map = string.gsub(map,"maps/","")
		local dir = map.."/children"
		local maps = {} 
		if love.filesystem.exists(dir) then 
			local f = love.filesystem.getDirectoryItems(dir) 
			for i,v in ipairs(f) do 
				table.insert(maps,v)
			end 
		end 
		local f = love.filesystem.getDirectoryItems("maps/")
		for i,v in ipairs(f) do 
			if v ~= a_map then 
				local exists
				for n,m in ipairs(maps) do 
					if v == m then 
						exists = true 
						table.insert(t,v)
					end 
				end 
				if not exists then 
					table.insert(maps,v)
				end 
			end 
		end 
		for i,v in ipairs(maps) do 
			if v ~= a_map then 
				local exists
				for m,k in ipairs(t) do 
					if v == k then 
						exists = true 
					end 
				end 
				if not exists then 
					table.insert(t,v)
				end 
			end
		end 
		return t 
	end 
	function zd:useP()
		if self.parent then 
			local gs = Gamestate.current()
			Gamestate.switch(gs,{map = self.parent})
		end 
	end 
	function zd:useC()
		if self.child then 
			local gs = Gamestate.current()
			local map = gs.map 
			Gamestate.switch(gs,{map = map.."/children/"..self.child})
		end 
	end 
	local target
	local oldName
	function zd:OnMenu(m,ui)
		if self.parent then 
			m:addItem("Use (Parent)",function()
				self:useP()
			end)
		end 
		if self.child then 
			m:addItem("Use (child)",function()
				self:useC()
			end)
		end 
		m:addItem("Set Child",function()
			local gs = Gamestate.current()
			if gs._activeFrame then gs:remove() end 
			
			
			local w,h = love.graphics.getDimensions()
			local f = ui:addFrame("Set Child",w/2,h/2,300,200)
			gs._activeFrame = f 
			f:setHeader()
			f.color = basicFrameColor
			local w,h = 20,20
			local b = ui:addButton(f,f.width- 22,0,w,h)
			b:setText("X")
			b.OnClick = function()
				f:remove()
				gs._activeFrame = nil 
			end 
			local list = ui:addList(f,2,20,f.width-2,f.height-22)
			for i,v in ipairs(self:getViableChildren()) do 
				local b = ui:addButton(nil,0,0,f.width-2,20)
				b:setText(v)
				list:addItem(b)
				b.OnClick = function()
					self:setChild(b.text) 
					f:remove()
					gs._activeFrame = nil
				end 
			end 
		end)
		m:addItem("Set Target",function()
			local x,y = love.graphics.getDimensions()
			x,y = x/2,y/2
			local f = ui:getFont()
			local h = f:getHeight()
			local frame = ui:addFrame("Set Target",x,y,300,h*2 + 40)
			local n = ui:addTextinput(frame,5,20,frame.width-10,h+5,target)
			
			frame:setHeader()
			frame:addCloseButton()
			frame.dragEnabled = true 
			
			local w = f:getWidth("Done")+10
			local b = ui:addButton(frame,frame.width/2-w/2,frame.height-(h+10),w,h+5)
			b.text = "Done"
			b.OnClick = function()
				target = n.text
			end
		end)
		m:addItem("Set Parent",function()
			local gs = Gamestate.current()
			if gs._activeFrame then gs:remove() end 
			local w,h = love.graphics.getDimensions()
			local f = ui:addFrame("Set Parent",w/2,h/2,100,200)
			gs._activeFrame = f 
			f:setHeader()
			f.color = basicFrameColor
			local w,h = 20,20
			local b = ui:addButton(f,f.width- 22,0,w,h)
			b:setText("X")
			b.OnClick = function()
				f:remove()
				gs._activeFrame = nil 
			end 
			local list = ui:addList(f,2,20,f.width-2,f.height-22)
			for i,v in ipairs(self:getViableParents()) do 
				local b = ui:addButton(nil,0,0,f.width-2,20)
				b:setText(v)
				list:addItem(b)
				b.OnClick = function()
					self:setParent(b.text)
					f:remove()
					gs._activeFrame = nil
				end 
			end 
		end)
		m:addItem("Remove Image",function()
			station.image = nil
			station.noImage = true 
		end)
	end 
	local o_save = station.save
	function zd:save()
		local t = o_save(zd) 
		t.child = self.child 
		if t.child then findChild(t.child) end 
		t.parent = self.parent 
		t.TargeT = target 
		if target then 
		local t = 
		{
			map = Gamestate.current().map.."/children/"..t.child,
			name = target,
			type = "zmap",
			}
			table.insert(editor.OnLoaded,t)
		end 
		return t 
	end 
	local o_load = station.load
	function zd:load(t) 
		if t.child then findChild(t.child) end 
		o_load(zd,t)
		self.child = t.child 
		self.parent = t.parent
		target = t.TargeT
	end 
	return station 
end 
return _