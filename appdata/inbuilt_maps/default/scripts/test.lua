radio = {} 
radio.phase1 = function()
	Radio:clearQue()
	Radio:add("Okay, listen here and stay still.")
	Radio:add("The writing on the walls.. it's nothing")
	Radio:add("Whatever happens, Do (not) listen to it")
	Radio:add("It wants you to give up")
	Radio:add("Okay.. O-kay lets assess the situation.")
	Radio:add("So, my name r-right? My.. name")
	Radio:add("I.. I can't remember so uh, call me Hope")
	Radio:add("Did you move?!")
	Radio:add("Jump down and go right, when you are down there, do not collect the coins!")
	Radio:add("Got it?! Then m-move!")
end 
radio.phase2 = function()
	Radio:clearQue()
	Radio:add("Listen, I know you're inclined to trust the text on the walls, but hear me out")
	Radio:add("You can't collect the coins. It's lying, don't even try it!")
	Radio:add("Instead go left, don't worry and leap, just leap! Trust me.")

end 
radio.phase3 = function()
	Radio:clearQue()
	Radio:add("Thank you.. [sigh], keep going left, you'll see an elevator, take it to the 3rd floor.")
	Radio:add("Do not take the 2nd floor, its meaningless, hurry to the 3rd floor")
	Radio:add("When you get there, I will explain, just don't read the text on the wall")
	Radio:add("Be careful, not to drop down or you will drop into infinity, if that happens I cannot help you.")
end 
radio.phase4 = function()
	Radio:clearQue()
	Radio:add("Do not read the text, please, let me explain.")
	Radio:add("Beyond that door is a key, t-this key will get you out of here")
	Radio:add("I know that you may not remember.. but you were someone")
	Radio:add("someone with dreams and aspirations, hopes and fears")
	Radio:add("Would you like to remember? So you can go back, I want to go back, we can work together.")
	Radio:add("So if you go through this door you agree to work with me")
	Radio:add("To trust me, so I can trust you")
	Radio:add("...")
	Radio:add("No matter what you trust me, just me!")
	Radio:add("Make your choice")
end 
radio.phase5 = function()
	Radio:clearQue()
	Radio:cypher()
	Radio:add("Lead me to them, I will eat them alive, you included.")
	Radio:add("I want you, I want you all to myself.",function() Timer.add(0.3,Radio:decypher()) end)

end 
_G.RadioQue = radio