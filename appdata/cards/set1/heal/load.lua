local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE .."/image.png")
local card = {
	name = "Healing",
	desc = "targets team \n heals [30%] of max hp each",
	cost = 10,
	rarity = 1, -- out of 10
	target = "team", -- team, none, single
	onUse = function(target,stack,item,area1,area2) -- Target is the target ::  stack: {team1, team2, finished} :: item is the item that you can draw with, it will allow you to draw things to screen
		for i,v in ipairs(target) do 
			v:applyDamage((v.maxHp/100)*30,"heal") 
		end 
		stack[4] = true 
	end,
	externalUse = function()
		for i,v in ipairs(playersMobs) do 
			local hp = v.info.hp 
			local maxHp = v.info.maxHp 
			v.info.hp = hp + (maxHp/100)*30
			if v.info.hp > maxHp then
				v.info.hp = maxHp
			end 			
		end 
	end,
	qualifier = function(stack) -- stack is same as above
		return true
	end,
	image = image,
	anim = {}, -- if animation then place here, else will display image without anim. Manage states using functions above.
}

return card