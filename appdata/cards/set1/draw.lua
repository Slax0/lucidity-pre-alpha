local _PACKAGE = getPackage(...)
local image = love.graphics.newImage(_PACKAGE.."/".."image.png")
local func 
local Shader = love.graphics.newShader("s/outline.glsl")
local TitleF = love.graphics.newFont(uniBody,12)
local TextF = love.graphics.newFont(uniBody,8)
local largeFont = love.graphics.newFont(60)
func = function(card) 
	local CardColor = card.cColor or {105,105,255}
	local oldFont = love.graphics.getFont()
	
	local lg = love.graphics 
	local rareCol = card.rarityCol
	local w,h = image:getDimensions()
	
	-- Corner crop for the beautiful coloring 
	local stencil = function() 
		local h = h - 2
		lg.polygon("fill",0,0, 13,0, 0,13 )
		lg.polygon("fill",w,0, w - 13,0, w,13)
		
		lg.polygon("fill",0,h, 13,h, 0,h - 12 )
		lg.polygon("fill",w,h, w - 14,h, w,h -14 )
	end 
	
	
	lg.setInvertedStencil(stencil)
		lg.setColor(rareCol)

	lg.rectangle("fill",1,1,w - 2,h - 2)
	lg.setColor(255,255,255)
	lg.setInvertedStencil()
	if not card.activeAnim then 
		lg.draw(card.image,20,16)
	else
		card.activeAnim:draw(19,16)
	end 	
	lg.draw(image,-1,-1)
	
	lg.setFont(TitleF)
	local text = card.name
	local tw = TitleF:getWidth(text)
	local th = TitleF:getHeight()
	local w,h = 146,18
	
	lg.setShader(Shader)
	
	lg.push()
	lg.scale(1.01)
	love.graphics.printf(text,16 + w/2 - tw/2,146 + h/2 -th/2 - 3,tw)
	lg.pop()
	
	Shader:send("color",{0,0,0})
	lg.setShader()
	
	lg.setColor(CardColor)
	love.graphics.printf(text,16 + w/2 - tw/2,146 + h/2 -th/2 - 3,tw)
	lg.setFont(TextF)
	local x,y = 26,168
	local w,h  = 127,42
	local text = card.desc
	lg.setColor(255,255,255)
	love.graphics.printf(text,x,y,w,"center")
	lg.setFont(oldFont)
	lg.setColor(255,0,0)

	lg.setColor(255,255,255)
	
end
return func