local _PACKAGE = getPackageAlt(...)
local card = {
	name = "Organic Trap",
	desc = "Traps a high value target.",
	cost = 290,
	rarity = 5, -- out of 10
	target = "single", -- team, none, single
	Organic = true,
	onUse = function(target,stack,item,area1,area2) -- Target is the target ::  stack: {team1, team2, finished} :: item is the item that you can draw with, it will allow you to draw things to screen
		-- Area polygon is only there if target = "team". ITs the target area for  the selected team in points eg [x,y,x1,y1,x2,y2]
		-- the draw item has the following func: drawBefore() draw() update()
		-- for i ==1,10 do 
			-- local x,y = math.random(0,)
		-- end 
		local w,h = target.width + 50,target.height + 90
		local grid =  {}
		local size = 5 
		local function make(w,h,size,fn)
			local delta = h/w 
			for x=0,w,size do 
				local sy 
				local s2y 
				if x > w/2 then 
					-- bottom right 
					s2y = delta*x - h
					-- bottom right 
					sy  = -delta*x + h
				elseif x < w/2 then 
					-- top right 
					s2y = -delta*x
					-- top left 
					sy	 = delta*x
				end 
				if fn then 
					fn(s,s2)
				else 
					if sy then 
						local s = {x = x - w/2,y = sy,t = 0}
						Flux.to(s,w/50 + x/50,{t = 255}):after(s,w/50 -x/50,{t = 0})
						table.insert(grid,s)
					end 
					if s2y then
						local s2 = {x = x - w/2,y = s2y,t = 0}
						Flux.to(s2,w/50 + x/50,{t = 255}):after(s2,w/50 -x/50,{t = 0})
						table.insert(grid,s2)
					end 
				end
			end 
		end  
		for y = size,h,size do  
			make(w,y,size)
		end 
		item.draw = function()
			for i,v in ipairs(grid) do 
				setColor(0,0,0,v.t)
				love.graphics.rectangle("fill",target.x + v.x,target.y + v.y,size,size)
				setColor()
			end 
		end
		item.update = function()
			if grid[1].t == 255 then 
				local tn = math.floor((precentOf(target.maxHp,target.hp)*target.luck)/100)
				local tn2 = math.floor((50*target.luck)/100)
				print("Has luck:"..target.luck)
				print("Chance of capture: ",tn,tn2)
				if math.random(tn,tn2) == tn2 then 
					local friendly 
					target.dead = true 
					for i,v in ipairs(stack[1]) do 
						if v == target then 
							friendly = true 
						end 
					end 
					if not friendly then 
						for i,v in ipairs(stack[2]) do 
							if v == target then 
								table.remove(stack[2],i)
							end 
						end 
						table.insert(stack[1],target)
					end 
				end 
				stack[4] = true 
			end 
 		end 
	end,
	qualifier = function(stack) -- stack is same as above
		if #playersMobs < 5 then 
			return true 
		end 
	end,
	anim = {}, -- if animation then place here, else will display image without anim. Manage states using functions above.
}

return card