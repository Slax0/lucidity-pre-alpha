local _PACKAGE = getPackage(...)
func = function(card) 
	local CardColor = card.cColor or {105,105,255}
	local oldFont = love.graphics.getFont()
	
	local lg = love.graphics 
	local rareCol = card.rarityCol
	-- this is for organic.
	if card.Organic then 
		setColor(rareCol)
		love.graphics.rectangle("fill",13,20,16,16)
		love.graphics.rectangle("fill",150,175,17,18)
		setColor()
	end 
	if not card.activeAnim then 
		lg.draw(card.image)
	else
		card.activeAnim:draw()
	end 		
end
return func