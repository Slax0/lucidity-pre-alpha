-- Game menu
gm = {}
tableLenght = {}
function tableLenght(T)
  local count = 0
  for _ in pairs(T or {}) do count = count + 1 end
  return count
end

function Main_Menu()
	for i,v in ipairs(gm) do
		love.graphics.setFont(title)
		love.graphics.print("Lucid Insomnia",100,91)
		love.graphics.setFont(font)
		local x,y
		x = 290
		y = 210
	
		love.graphics.setColor(255,255,255)
		if v.x == 1 then
			love.graphics.setColor(255,216,0)
		end
		love.graphics.setFont(gm_font)
		love.graphics.print("Start Game",x, y)
		love.graphics.setColor(255,255,255)
	
		love.graphics.setColor(255,255,255)
		if v.x == 2 then
			love.graphics.setColor(255,216,0)
		end
		love.graphics.print("Load Game",x + 10, y + 30)
		love.graphics.setColor(255,255,255)
		
		love.graphics.setColor(255,255,255)
		if v.x == 3 then
			love.graphics.setColor(255,216,0)
		end
		love.graphics.print("Options",x + 19, y + 60)
		love.graphics.setColor(255,255,255)
		
		love.graphics.setColor(255,255,255)
		if v.x == 4 then
			love.graphics.setColor(255,216,0)
		end
		love.graphics.print("Exit",x + 48, y + 90)
		love.graphics.setColor(255,255,255)
		love.graphics.draw(v.thix,600,400)
		love.graphics.setColor(20,20,20)
		love.graphics.rectangle("fill",0,546,800,60)
		love.graphics.setColor(25,25,25)
		love.graphics.rectangle("fill",0,567,800,60)
		love.graphics.setColor(255,255,255)
	end
end
function devmenu()
	love.graphics.setFont(font)
	local mx, my = love.mouse.getPosition()
	love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 30, 30)
	love.graphics.print("Delta Time: "..tostring(love.timer.getDelta( )), 40, 40)
	love.graphics.print("mouse position x is:"..mx)
    love.graphics.print("mouse position y is:"..my,10,10 )
end
function gm_load()
	gm[#gm + 1] = {
	thix = love.graphics.newImage("assets/title2.png"),
	x = 1,
	y = 0,
	nlinear = 0,
	}
end
function gm_background()
	for i,v in ipairs(gm) do
		love.graphics.setColor(255,255,255,255)
		love.graphics.draw(v.bg1_img, v.bg1_x, 0)
		love.graphics.draw(v.bg2_img, v.bg2_x, 0)
	end
end
function gm_update(dt)
	for i,v in ipairs(gm) do
		v.bg1_x = v.bg1_x + v.speed * dt
		v.bg2_x = v.bg2_x + v.speed * dt

		if v.bg1_x > v.windowx then
			v.bg1_x = v.bg2_x - v.bg1_width
		end
		if v.bg2_x > v.windowx then
			v.bg2_x = v.bg1_x - v.bg2_width
		end
	end
end

function gm_key(key)
	for i,v in ipairs(gm) do
		if key == "up" then
			v.x = v.x - 1
		end
		if key == "down" then
			v.x = v.x + 1
		end
		if v.x < 1 then
			v.x = 4 
		end
		if v.x > 4 then
			v.x = 1
		end
		if v.x == 1 and key == "return" then
			current_gamestate = "game"
			Gamestate.switch(game)
		end
		if v.x == 2 and key == "return" then
		current_gamestate = "game"
			Gamestate.switch(game)
		end
		if v.x == 3 and key == "return" then
		current_gamestate = "game"
			Gamestate.switch(game)
		end
		if v.x == 4 and key == "return" then
			love.event.push("quit")
		end
	end
end

--end



tiling = {}
function tiling:new(image,ix2,iy2,ox2,fx2,oy2,fy2)
	local object = {
		image = love.graphics.newImage(image) or love.graphics.newImage("assets/furniture/pavement.png"),
		nimage = love.graphics.newImage("assets/furniture/pavement_normal.png"),
		nt2 = 0,
		ox2 = ox2 or 0,
		oy2 = oy2 or 0,
		fx2 = fx2 or 0,
		fy2 = fy2 or 0,
		canvaseer = 0,
		ix2 = ix2 or 0,
		iy2 = iy2 or 0,
		shadow = 0
	}
	setmetatable(object, { __index = tiling })
	return object
end
function tiling:preload(normal)
	self.image:setWrap("repeat","repeat")
	local this23 = (self.fx2 - self.ox2)
	self.nt2 = love.graphics.newQuad(self.ox2,self.oy2,this23,self.image:getHeight(),self.image:getDimensions())
	if normal ~= nil then
		normal = love.graphics.newImage(normal)
		self.shadow = amber:newImage(self.image,self.ix2 + self.fx2/2,self.iy2 + self.image:getHeight()/2,self.fx2)
		self.shadow:setNormalMap(normal,self.fx2,self.image:getHeight())
	end
end
function tiling:draw()
	love.graphics.draw(self.image,self.nt2,self.ix2,self.iy2)
end
House = {}
A23xY8Z = {}
function House:init()
	A23xY8Z[#A23xY8Z + 1] = {
		canvasHouse = love.graphics.newCanvas(3224,2046),
		r = 0,
		g = 0,
		b = 0,
		inHouse = 0
		}
	local inHouse = 0
	local house = love.graphics.newImage("assets/house.png")
	for i,v in ipairs(A23xY8Z) do 
		love.graphics.setCanvas(v.canvasHouse)
		v.canvasHouse:clear()
		house:setFilter("nearest","nearest")
		love.graphics.draw(house,110,-300,0,2)
		love.graphics.setCanvas()
	end
end
function House:draw()
	for i,v in ipairs(A23xY8Z) do 
		love.graphics.draw(v.canvasHouse,0,-1826)
	end
end
function House:update(dt)
	for i,v in ipairs(A23xY8Z) do
		if player.y > 1820 or player.x > 3110 or player.x < 137  then
			v.inHouse = "true"
		else
			v.inHouse = "false"
		end	
		if v.inHouse == "true" then
			if v.r < 36 and v.g < 36 and v.b < 36 then
				v.r = v.r + (25 *dt)
				v.g = v.g + (25 *dt)
				v.b = v.b + (25 *dt)
				amber:setAmbientColor(v.r,v.g,v.b)
			end
		else
			if v.r < 0 or v.g < 0 or v.b < 0 then
				amber:setAmbientColor(0,0,0)
			elseif v.r > 1 and v.g > 1 and v.b > 1 then
				v.r = v.r - (55 *dt)
				v.g = v.g - (55 *dt)
				v.b = v.b - (55 *dt)
				amber:setAmbientColor(v.r,v.g,v.b)
			end
		end
	end
end

function House:drawSpecial()
			love.graphics.setColor(0,0,0)
			love.graphics.rectangle("fill", 180,97,2959,2000)
			love.graphics.setColor(255,255,255)
	for i,v in ipairs(A23xY8Z) do
		if v.inHouse == "true" and door1.baka == 2 then
			love.graphics.setColor(0,0,0)
			love.graphics.rectangle("fill", 0,-1826,3122,2000)
			love.graphics.setColor(255,255,255)
			
		end
		if v.inHouse == "true" then
			love.graphics.setColor(0,0,0)
			love.graphics.rectangle("fill", 0,-1826,3122,1660)
			love.graphics.setColor(255,255,255)
		
			love.graphics.setColor(0,0,0,25)
			love.graphics.rectangle("fill", 0,-167,3122,260)
			love.graphics.setColor(255,255,255)
			
		end
	end
end


elevator = {}
function elevator:new(x,y,r,g,b,speed2,floors,image)
	local object = {
		image = love.graphics.newImage("assets/furniture/elevator.png") or love.graphics.newImage(image),
		direction = "still",
		x = x or 0,
		y = y or -10,
		xvel = 0,
		xgrav = 4,
		speed = 10,
		speed2 = speed2 or 300,
		this = 0,
		r = r or 0,
		g = g or 0,
		b = b or 0,
		curfloor = 0,
		desfloor = 0,
		this = 0,
		floors = 0
	}
	setmetatable(object, {__index = elevator})
	return object
end
function elevator:draw()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(self.image, self.x - self.image:getWidth(), -self.y - self.image:getHeight()*2,0,2)
end

function elevator:update(dt,object,object2,object3)
	local direction2 = "still"
	local destination = 20
	local destination_reached = 1
	
	local floore = 0
	
	local floor0 = 45
	local floor1 = 60
	local floor2 = 293 -- 46 difference between o and f
	local x = 0
	if self.desfloor == 0 then
		x = -92  + 8 -45
	elseif self.desfloor == 1 then
		x = 170  + 8 - 45
	elseif self.desfloor == 2 then
		x = 443  + 8 - 45
	elseif self.desfloor == 3 then
		x = 715  + 8 - 45 --272
	elseif self.desfloor == 4 then
		x = 982  + 8 - 45
	elseif self.desfloor == 5 then
		x = 1238  + 8 - 45
	elseif self.desfloor == 6 then
		x = 1506  + 8 - 45
	else
		self.desfloor = 0
	end
	local this7 = 0
	
	if self.y >= x - 32 then
		self.left = true
		self.right = false
	elseif x - self.y < 32 then
		self.xvel = 0
	end
	if self.y <= x + 32 then
		self.right = true
		self.left = false
	elseif x - self.y > -40 then
		self.right = false
		self.left = false
		self.xvel = 0
	end
			

	if self.left then
		self.direction = "down"
	end
	if self.right then
		self.direction = "up"
	end
	if self.right == false and self.left == false then
		self.direction = "still"
	end
	-- local floor0 = 45
	-- local floor1 = 60
	-- local floor2 = 293 -- 268
	-- local floor3 = 567
	-- local floor4 = 835
	-- local floor5 = 1100
	-- local floor6 = 1359
	-- local floor7 = 1627
	-- self.this = self.y - 122 * -1
	-- if self.this < floor0 or self.this > floor0  then
		-- self.curfloor = 0
	-- end
	-- if self.this > floor1 then
		-- self.curfloor = 1
	-- end
	-- if self.this > floor2 then
		-- self.curfloor = 2
	-- end
	-- if self.this > floor3 then
		-- self.curfloor = 3
	-- end
	-- if self.this > floor4 then
		-- self.curfloor = 4
	-- end
	-- if self.this > floor5 then
		-- self.curfloor = 5
	-- end
	-- if self.this > floor6 then
		-- self.curfloor = 6
	-- end
	-- if self.this > floor7 then
		-- self.curfloor = 7
	-- end
	-- LOgical Issue fix ASAP
	-- if self.curfloor - self.desfloor < 0 then
		-- self.direction = "up"
	-- elseif self.curfloor - self.desfloor > 0 then
		-- self.direction = "down"
	-- else 
		-- self.xvel = 0
	-- end

	local dt1 = math.min(dt, 0.07)
	local frame = dt1 * self.speed2
	self.y = self.y + self.xvel * frame
	self.xvel = self.xvel * (1 - math.min(dt*self.xgrav, 1))
	if self.direction == "still" then
		self.xvel = 0
	end
	if self.direction == "down" and self.xvel > -6  then
		self.xvel = self.xvel - (self.speed * dt)
	elseif self.direction == "up" and self.xvel < 6 then
		self.xvel = self.xvel + (self.speed * dt)
	end
	
	if self.direction == "jump" then
		if self.yvelocity == 0 then
			self.yvelocity = self.jump_height
		end
	end
	-- if self.direction == "still" and object.james == 1 then
		-- player.y = self.y
		-- player.yvelocity = 0
		-- player.oy = object2.y + 190
	-- end
	
	
		if object.james == 1 then
			if self.direction ~= "still" then
				if love.keyboard.isDown(" ") == false then
				player.y = self.y
				end
			end
			player.oy = object2.y + 127 --Move value if Jittering
		else
			player.oy = 0
		end
		if object.james1 == 1 then
			if self.direction ~= "still" then
				if love.keyboard.isDown(" ") == false then
				soko.y = self.y
				end
			end
			soko.oy = object2.y + 127
		else
			soko.oy = 0
		end
end
function crop(image,imagex,imagey,fw,fh,scalex,scaley,direction)
	assert(image,"Image you are trying to crop is:"..tostring(image).." ,expected image data/image location")
	if image ~= 0 and type(image) == "string" then
		image = love.graphics.newImage(image)
	end
	local scalex = scalex or 1 
	local scaley = scaley or 1 
	local crop = love.graphics.newQuad(imagex,imagey,fw,fh,image:getDimensions())
	local canvas_n = love.graphics.newCanvas(fw*scalex,fh*scaley)
	love.graphics.setCanvas(canvas_n)
		canvas_n:clear()
		love.graphics.push()
				love.graphics.scale(scalex,scaley)
			love.graphics.setBlendMode('alpha')
			love.graphics.setColor(255,255,255)
			if direction == nil or direction == "left" then
				love.graphics.draw(image,crop,0,0,0,1,1)
			elseif direction == "right" then
				love.graphics.draw(image,crop,0,0,0,-1,1,fw)
			end
		love.graphics.pop()
	love.graphics.setCanvas()
	local uimage = canvas_n:getImageData()
	local nimage = love.graphics.newImage(uimage)
	return nimage
end
function crop_quad(image,quad,fw,fh,scalex,scaley,direction)
	assert(image,"Image you are trying to crop is:"..tostring(image).." ,expected image data/image location")
	if image ~= 0 and type(image) == "string" then
		image = love.graphics.newImage(image)
	end
	if scalex ~= nil then
		if scaley == nil then
				scaley = scalex
		end
	else
		scalex = 1
		scaley = 1
	end
	local crop = quad
	local canvas_n = love.graphics.newCanvas(fw*scalex,fh*scaley)
	love.graphics.setCanvas(canvas_n)
		canvas_n:clear()
		love.graphics.push()
				love.graphics.scale(scalex,scaley)
			love.graphics.setBlendMode('alpha')
			love.graphics.setColor(255,255,255)
			if direction == nil or direction == "left" then
				love.graphics.draw(image,crop,0,0,0,1,1)
			elseif direction == "right" then
				love.graphics.draw(image,crop,0,0,0,-1,1,fw)
			end
		love.graphics.pop()
	love.graphics.setCanvas()
	local uimage = canvas_n:getImageData()
	local nimage = love.graphics.newImage(uimage)
	return nimage
end
function scalez(image,scalex,scaley)
	local canvas_n = love.graphics.newCanvas(image:getWidth()*scalex,image:getHeight()*scaley)
	love.graphics.setCanvas(canvas_n)
		canvas_n:clear()
			love.graphics.setBlendMode('alpha')
			love.graphics.setColor(255,255,255)
			love.graphics.draw(image,0,0,0,scalex,scaley)
	love.graphics.setCanvas()
	local uimage = canvas_n:getImageData()
	local nimage = love.graphics.newImage(uimage)
	return nimage
end
function locals()
  local variables = {}
  local idx = 1
  while true do
    local ln, lv = debug.getlocal(2, idx)
    if ln ~= nil then
      variables[ln] = lv
    else
      break
    end
    idx = 1 + idx
  end
  return variables
end
function clearGlobals()
	for i in pairs(_G) do
		_G[i] = nil
	end
end
