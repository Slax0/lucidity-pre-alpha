import os
from pyglet.gl import *
from l_tools import colors
from Vector import Vector

mouseDown = [0,0,0,0,0]

def gPush():
    glPushMatrix()
def gPop():
    glPopMatrix()
def unpack(tuple):
    return tuple[:len(tuple)]
def setColor(tuple=(255,255,255,255)):
    glEnable (GL_BLEND)
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    alpha = 255
    if len(tuple) >= 4:
        alpha = tuple[3]
    r = max(1,tuple[0])/255
    g = max(1,tuple[1])/255
    b = max(1,tuple[2])/255
    a = max(1,alpha)/255
    glColor4f(r,g,b,a)

def drawRectangle(mode,x=0,y=0,w=100,h=100,rot=0):
    if mode == "fill":
        mode = GL_QUADS
    elif mode == "line":
        mode = GL_LINE_LOOP
    glMatrixMode(GL_PROJECTION)
    w,h = w/2,h/2
    glPushMatrix()
    glTranslatef(x,y,0)
    if rot != 0:
        glRotatef(rot,0,0,0)
    glBegin(mode)
    glVertex2f(w,h)
    glVertex2f(w,-h)
    glVertex2f(-w,-h)
    glVertex2f(-w,h)
    glEnd()
    glPopMatrix()
def glDrawVert(mode,verts):
    if mode == "fill":
        mode = GL_QUADS
    elif mode == "line":
        mode = GL_LINE_LOOP
    glBegin(mode)
    for vert in verts:
        glVertex2f(vert[0],vert[1])
    glEnd()

class Rectangle:
    def __init__(self,x,y,w,h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.size = [w,h]
        self.scale = (1,1)
        self.rot = 0
        self.verts = []
        w,h = w/2,h/2
        self.points = [Vector(w,h),Vector(w,-h),Vector(-w,-h),Vector(-w,h)]
        self.original = [Vector(w,h),Vector(w,-h),Vector(-w,-h),Vector(-w,h)]
        self.moveTo(x,y)
    def setRotation(self,rot):
        self.rot = rot
    def scale(self):
        pass
    def setDimensions(self,w,h):
        self.size = (w,h)
    def refresh(self):
        w,h = self.w/2,self.h/2
        self.original = [Vector(w,h),Vector(w,-h),Vector(-w,-h),Vector(-w,h)]
        if self.rot > 360:
            self.rot = 0
        for i in range(len(self.original)):

           v = self.original[i]
           pos = (self.x,self.y)
           v = v.rotate(self.rot)
           self.points[i] = v + pos
    def moveTo(self,x,y):
        self.x = x
        self.y = y
        self.refresh()
    def hasPoint(self,x,y):
        if point_in_poly(x,y,self.points):
            return True

    def unpack(self):
        self.verts = [
           self.points[0][0],self.points[0][1],
           self.points[1][0],self.points[1][1],
           self.points[2][0],self.points[2][1],
           self.points[3][0],self.points[3][1],
        ]
    def collidesWith(self,shape):
        for vector in shape:
            pass
    def draw(self,mode="fill"):
        glDrawVert(mode,self.points)
        pass

__author__ = 'X3-Slax0'

bColor = (128,128,128)
bDownColor = (68,68,68,100)
bfcolor = (168,168,168,100)
topColor = (82, 126, 199)


def point_in_poly(x,y,p):
    inside = False
    n = len(p)
    p1x,p1y = p[0]
    for i in range(n+1):
        p2x,p2y = p[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xints:
                        inside = not inside
        p1x,p1y = p2x,p2y
    return inside
class widget:
    def __init__(self,parent,x,y,w,h):
        self.x = x
        self.y = y
        self.width = w
        self.height = h
        self.papa = False
        self.parent = False
        if parent:
            parent.addItem(self)
    def getPos(self):
        return self.x,self.y
    def moveTo(self,x,y):
        self.x = x
        self.y = y
    def remove(self):
        if self.parent:
            list.remove(self.parent.items,self)
        elif self.papa:
            list.remove(self.papa.items,self)

class progressBar(widget):
     def __init__(self,parent,x,y,w,h,initial=0):
         self.start = initial
         self.val = initial
         self.back = Rectangle(x,y,w,h)
         self.actual = Rectangle(x+self.val/2,y,0,h)
         self.text = pyglet.text.Label("None",x = x,y = y)
         self.OnUpdate = None
         super(progressBar,self).__init__(parent,x,y,w,h)
     def setValue(self,val=0):
        #precentage
        if val > 100:
            val = 100
        self.val = val
        self.refresh()
     def getValue(self):
         return self.val
     def getWidth(self):
        if self.val > 100:
            self.val = 100
        pr = self.width/100
        w = pr*self.val
        return w
     def update(self,dt):
         self.text.text = str(self.val) + "%"
         self.text.x = self.x - 5
         self.text.y = self.y - 5
         self.text.font_size = 8
         if self.OnUpdate:
             self.OnUpdate(dt)
         pass
     def getPos(self):
         return self.x,self.y
     def moveTo(self,x,y):
         self.x = x
         self.y = y
         self.refresh()
     def refresh(self):
        x,y = self.x,self.y
        w,h = self.width,self.height
        rw = self.getWidth()
        self.back.x = x
        self.back.y = y
        self.back.w = w
        self.back.h = h
        self.actual.x = x-w/2+rw/2
        self.actual.y = y
        self.actual.w = rw
        self.actual.h = h

        self.back.refresh()
        self.actual.refresh()
     def draw(self):
        setColor(colors["lightGray"])
        self.back.draw()

        setColor((255,0,0))
        self.actual.draw()
        self.text.draw()
        pass

class recSpiner:
    def __init__(self,parent,x,y,w,h):
        self.x = x
        self.y = y
        self.width  = w
        self.height = h
        self.rot = 0
        self.hasFocus = False
        self.color = (255,255,255)

        self.OnClick = False
        self.OnUpdate = False
        self.rect = Rectangle(x,y,w,h)
        if parent:
            parent.addItem(self)
        self.rect2 = self.flash = Rectangle(x,y,w-10,h-10)
    def update(self,dt):
        self.rect.rot = self.rect.rot + dt*90
        self.rect2.rot = -self.rect.rot - dt*50
        if self.rect.rot > 360:
            self.rect.rot = 0
            self.rect2.rot = 0
        pass
    def draw(self):
        self.rect.x = self.x
        self.rect.y = self.y
        self.rect2.x = self.x
        self.rect2.y = self.y
        setColor((105,89,205))
        self.rect.refresh()
        self.rect.draw()
        self.rect2.refresh()
        setColor((255,215,0))
        self.rect2.draw()
        pass
    def moveTo(self,x,y):
        self.x = x
        self.y = y
    def getPos(self):
        return self.x,self.y

class Text(widget):
     def __init__(self,parent,x,y,text,size=8,w=100,h=100):
         self.text = text
         font = pyglet.font.load(None, size)
         self.label = pyglet.text.Label(
         text = self.text,
         font_size = size,
         x = x,y = y,
         anchor_x ='left',
         anchor_y ='center',)


         super(Text,self).__init__(parent,x,y,w,h)
         self.size = size
         self.color = (0,0,0,255)

         if parent:
            parent.addItem(self)
     def setText(self,text,size=None):
        size = size or self.size
        self.label.text = text
     def update(self,dt):
         self.label.x = self.x
         self.label.y = self.y
         pass
     def draw(self):
         self.label.draw()
class textFrame(widget):
    def __init__(self,parent,x,y,w,h,text=""):
        super(textFrame,self).__init__(parent,x,y,w,h)
        document = pyglet.text.decode_attributed(text)
        self.label = pyglet.text.DocumentLabel(
            document = document,
            x = self.x,
            y = self.y,
            anchor_x = "left",
            anchor_y = "top",
            width = w,
            height = h,
            multiline = True,
        )
        self.OnUpdate = None
    def setText(self,text):
        document = pyglet.text.decode_attributed(text)
        self.label = pyglet.text.DocumentLabel(
            document = document,
            x = self.x,
            y = self.y,
            anchor_x = "left",
            anchor_y = "top",
            width = self.width,
            height = self.height,
            multiline = True,
        )
    def update(self,dt):
        self.label.x = self.x - self.width/2
        self.label.y = self.y + self.height/2
        if self.OnUpdate:
            self.OnUpdate(dt)
        pass
    def draw(self):
        self.label.draw()
class button(widget):
    def __init__(self,parent,x,y,w,h,text=""):
        super(button,self).__init__(parent,x,y,w,h)
        self.rect = Rectangle(x,y,w,h)
        self.hasFocus = False
        self.OnClick = False
        self.OnUpdate = False
        self.color = (colors["darkGray"])
        self.inactive = False
        self.mousedown = False
        self.color2 = False
        self.line = False
        self.text = text
        self.setText(self.text)
        self.Label = False

    def setFoucs(self,bool):
        self.hasFocus = bool
    def mouse_press(self,x,y,b):
        if self.hasFocus:
            if self.OnClick and not self.inactive:
                self.OnClick(b)
    def setColor(self,color = None,outline = None):
        color = self.color or color
        color2 = self.color2 or outline
        self.color = color
        self.color2 = color2
        color2 = self.color or color2
    def setText(self,t,size=8):
        self.text = t
        self.label = pyglet.text.Label(
            text = self.text,
            font_size = size,
            x = self.x,y = self.y,
            anchor_x ='center',
            anchor_y ='center',
        )
    def update(self,dt):
        self.rect.refresh()
        self.rect.x = self.x
        self.rect.y = self.y
        self.label.x = self.x
        self.label.y = self.y
        oColor = self.color2
        if self.hasFocus:
            oColor = bfcolor
            if mouseDown[1]:
                oColor = bDownColor
        elif self.inactive:
            oColor = (55,55,55,100)
        else:
            oColor = False
        self.color2 = oColor
        if self.OnUpdate:
            self.OnUpdate(dt)
    def draw(self):
        r = self.rect
        x,y = self.rect.x,self.rect.y
        w,h = self.rect.size
        setColor(self.color)
        self.rect.draw("fill")
        if self.line:
            setColor(self.line)
            self.rect.draw("line")

        self.label.draw()
        if not self.inactive:
            if self.color2:
                setColor(self.color2)
                drawRectangle("fill",x,y,w,h)
        else:
            setColor((55,55,55,100))
            drawRectangle("fill",x,y,w,h)
        pass
    def moveTo(self,x,y):
        if hasattr(self,"rect"):
            self.rect.x = x
            self.rect.y = y
            self.rect.refresh()
        self.x = x
        self.y = y
    def getPos(self):
        x = self.x
        y = self.y
        return x,y
class Frame:
     def __init__(self,parent,x,y,w,h):
         self.x = x
         self.y = y
         self.width = w
         self.height = h
         self.parent = parent
         self.rect = Rectangle(x,y,w,h)
         self.hasFocus = False
         self.papa = False
         self.label = False
         self.drawTop = True
         self.items = []
         self.text = ""
         self.modex = "center"
         self.modey = "center"
         self.OnDraw = False
         self.drawLine = True
         self.color = (65,65,65)
         self.parent = False
         if parent:
             parent.addItem(self)
     def draw(self):
         setColor(self.color)
         drawRectangle("fill",self.x,self.y,self.width,self.height)
         if self.drawLine:
             setColor((255,255,255))
             drawRectangle("line",self.x,self.y,self.width,self.height)
             setColor()
         for item in self.items:
             item.draw()
         if self.label:
             r,g,b = topColor
             setColor((r,g,b))
             h = 22
             drawRectangle("fill",self.x,self.y +self.height/2 -h/2,self.width,h)
             setColor((255,255,255))
             drawRectangle("line",self.x,self.y +self.height/2 -h/2,self.width,h)
             self.label.draw()
         if self.OnDraw:
             self.OnDraw()
         pass
     def setLabel(self,t):
        self.text =t
        x = self.x
        if self.modex == "left":
            x = self.x - self.width/2 +5
        self.label = pyglet.text.Label(
            text = t,
            font_size = 10,
            x = x,y = self.y+self.height/2 - 10,
            anchor_x =self.modex,
            anchor_y =self.modey,
        )
     def setAnchor(self,modex="center",modey="center"):
         self.modex = modex
         self.modey = modey
         self.setLabel(self.text)
     def mouse_press(self,x,y,b):
         if self.hasFocus:
             for item in self.items:
                 if hasattr(item,"mouse_press"):
                    item.mouse_press(x,y,b)
     def update(self,dt):
         collides = False
         if self.papa.collides(self):
             collides = True
             self.hasFocus = True
         else:
             self.hasFocus = False
         for item in self.items:
            item.update(dt)
            if hasattr(item,"hasFocus"):
                 if collides:
                     if self.papa.collides(item):
                         item.hasFocus = True
                     else:
                         item.hasFocus = False
                 else:
                     item.hasFocus = False
         if hasattr(self,"OnUpdate"):
             self.OnUpdate(dt)
         pass
     def addItem(self,item):
         item.papa = self.papa
         item.parent = self
         x,y = item.getPos()
         x = self.x - (self.width/2 - item.width/2) + item.x
         y = self.y + (self.height/2 - item.height/2) - item.y
         item.moveTo(x,y)
         list.insert(self.items,len(self.items)+1,item)
     def remove(self):
        if self.parent:
            list.remove(self.parent.items,self)
        elif self.papa:
            list.remove(self.papa.items,self)

class UI:
    def __init__(self,scw,sch,window):
        self.mousePos = ()
        self.items = []
        self.scw = scw
        self.sch = sch
        self.window = window
    def clear(self):
        self.items = []
    def setWindow(self,w):
        self.window = w
    def update(self,dt):
        mx,my = self.window._mouse_x,self.window._mouse_y
        for b in self.items:
            b.update(dt)
            if hasattr(b,"hasFocus"):
                if b.rect.hasPoint(mx,my):
                    b.hasFocus = True
                else:
                    b.hasFocus = False
    def mouse(self):
        return self.window._mouse_x,self.window._mouse_y
    def collides(self,item):
        mx,my = self.mouse()
        if item.rect.hasPoint(mx,my):
            return True
        else:
            return False
    def draw(self):
        setColor((157, 167, 178))
        drawRectangle("fill",self.scw/2,self.sch/2,self.scw,self.sch)
        for b in self.items:
            b.draw()
        setColor()
    def mouse_pressed(self,x,y,b):
        mouseDown[b] = True
        for b in self.items:
            if hasattr(b,"mouse_press"):
                b.mouse_press(x,y,b)
    def mouse_released(self,x,y,b):
        mouseDown[b] = False
    def addButton(self,x,y,w,h):
        b = button(self,x,y,w,h)
        return b
    def addFrame(self,x,y,w,h):
        b = Frame(self,x,y,w,h)
        return b
    def addImage(self,x,y,url):
        return Image(self,x,y,url)
    def addItem(self,b):
        b.papa = self
        list.insert(self.items,len(self.items)+1,b)
        return b

class construct:
    def __init__(self,item):
        self.addItem = item.addItem
        pass
    def addFrame(self,x,y,w,h):
        f = Frame(self,x,y,w,h)
        self.addItem(f)
        return f
    def addButton(self,x,y,w,h):
        b = button(self,x,y,w,h)
        self.addItem(b)
        return b
class Image(widget):
    def __init__(self,parent,x,y,url):
        f = open(url,"rb")
        image = pyglet.image.load("image",f)
        data = image.get_image_data()
        w,h = data.width,data.height
        self.image = image
        self.data = data
        super(Image,self).__init__(parent,x,y,w,h)
    def update(self,dt):
        pass
    def draw(self):
        setColor()
        self.image.blit(self.x - self.width/2,self.y - self.height/2)

class State(widget,construct):
    def __init__(self,parent,x,y,w,h):
        super(State,self).__init__(parent,x,y,w,h)
        construct.__init__(self,self)
        self.states = {}
        self.active = False
        self.OnCreate = False
        self.OnRemove = False
        self.rect = Rectangle(x,y,w,h)
    def switch(self,state):
        try:
            if self.active:
                if "OnRemove" in self.active:
                    self.active["OnRemove"]()
            self.active = self.states[state]
            if self.active:
                if "OnMake" in self.active:
                    self.active["OnMake"]()
        except:
            print("No such state:" + str(state))
    def mouse_press(self,x,y,b):
     if self.hasFocus and self.active:
         for item in self.active["items"]:
             if hasattr(item,"mouse_press"):
                item.mouse_press(x,y,b)
    def draw(self):
        setColor()
        self.rect.draw("line")
        if self.active:
            for item in self.active["items"]:
                item.draw()
    def newState(self,name):
        self.states[name] = {
            "items":[],
        }
        self.active = self.states[name]
    def update(self,dt):
        self.rect.x = self.x
        self.rect.y = self.y
        self.rect.refresh()
        if self.active:
            collides = False
            if self.papa.collides(self):
                 collides = True
                 self.hasFocus = True
            else:
                 self.hasFocus = False

            for item in self.active["items"]:
                item.update(dt)
                if hasattr(item,"hasFocus"):
                 if collides:
                     if self.papa.collides(item):
                         item.hasFocus = True
                     else:
                         item.hasFocus = False
                 else:
                     item.hasFocus = False
    def addItem(self,item):
         item.papa = self.papa
         item.parent = self
         x = self.x - (self.width/2 - item.width/2) + item.x
         y = self.y + (self.height/2 - item.height/2) - item.y
         item.moveTo(x,y)
         list.insert(self.active["items"],len(self.active["items"])+1,item)
