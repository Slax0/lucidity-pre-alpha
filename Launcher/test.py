import os
import sys
from gui import progressBar,recSpiner, widget,button,UI,State,Image,Text,textFrame
from tempfile import NamedTemporaryFile
from l_tools import colors,launch,getFileFromUrl,extract
from pyglet.gl import *
from pyglet.font import ttf
from Tween import Tween
import string
import threading
import encodings.idna
u"яндекс.рф".encode('idna')
class Version:
    def __init__(self):
        self.version = ""
        self.expVersion = ""
        self.lversion = ""
        self.lexpVersion = ""
        self.hasback = False
        self.has = False
    def setActualVersion(self,v):
        self.expVersion = str(v).replace("\n","")
    def setActualLauncherVersion(self,v):
        self.lexpVersion = str(v).replace("\n","")
    def setVersion(self,v):
        self.version = v.replace("\n","")
    def setLauncherVersion(self,v):
        self.lversion = v.replace("\n","")
    def saveVersion(self):
        vs = self.expVersion  + "\n"
        lvs = self.lexpVersion + "\n"
        strs = vs + lvs
        f = open(appdata + "/Lucidity/VERSION.txt","w")
        f.write(strs)
        f.close()
    def hasBackground(self,bool):
        self.hasback = bool
    def hasSide(self,bool):
        self.has = bool
    def needsBackground(self):
        return (not self.hasback)
    def needsSide(self):
        return (not self.has)
    def unpack(self):
        return self.version,self.expVersion,self.lversion,self.lexpVersion
    def compareVerL(self):
        print(self.version)
        print(self.expVersion)
        return (self.version == self.expVersion)
    def compareVer(self):
        return (self.lversion == self.lexpVersion)
    def hasImages(self):
        return (self.has and self.hasback)
    def passage(self):
        return (self.hasImages() and self.compareVer())
    def read(self,st):
        ver = ""
        lver = ""
        st = st.splitlines()
        ver = st[0]
        lver = st[len(st)-1]
        self.version = ver
        self.lversion = lver

Ver = Version()
import pyglet
def Update(x):
    pass
def Download(x):
    pass
def Check(x):
    pass

appdata = os.getenv('APPDATA')
urls = ["","","","","",""]
MakeUrls = True
def readVersion(st):
    ver = ""
    lver = ""
    st = st.splitlines()
    ver = st[0]
    lver = st[len(st)-1]
    return ver,lver

if os.path.exists(appdata + "/Lucidity"):
    dir = appdata + "/Lucidity"
    if os.path.exists(dir + "/bin/lucidity.exe"):
        Downloaded = True
    if os.path.exists(dir + "/VERSION.txt"):
        file = open(dir + "/VERSION.txt")
        sta = file.read()
        Version,l_Version = readVersion(sta)
        Ver.setVersion(Version)
        Ver.setLauncherVersion(l_Version)

    if os.path.exists(dir + "/launcher/"):
        dir = dir + "/launcher/"
        if os.path.exists(dir + "/background.png"):
            Ver.hasBackground(True)
        if os.path.exists(dir + "/side.png"):
            Ver.hasSide(True)
        if os.path.exists(dir + "/urls.txt"):
            MakeUrls = False
            f = open(dir + "/urls.txt","r")
            sta = f.read()
            no = 0
            sta = sta.splitlines()
            for lines in sta:
                urls[no] = lines
                no += 1
    else:
        os.makedirs(dir + "/launcher/")
else:
    os.makedirs(appdata + "/Lucidity")
if MakeUrls:
    # Version file
    # background
    # title
    # News
    # lucidity zip file to unpack in appdata:
      # windows
      # Max
      # Linux
    urls[0] = "https://www.dropbox.com/s/7y5lamxropu8kxs/VERSION.txt?raw=1"
    urls[1] = "https://www.dropbox.com/s/td3iif5qqueem6u/background.png?raw=1"
    urls[2] = "https://www.dropbox.com/s/jfxh4hpdj4wcsdm/side.png?raw=1"
    urls[3] = "https://www.dropbox.com/s/gy0xbhqwwbcu79l/News.txt?raw=1"

    urls[4] = "https://www.dropbox.com/s/0ck3evro8l1mvs5/release_win.zip?raw=1"






#THIS HERE IS FOR DOWNLOADING IMAGES/VERSION.
from queue import Queue
Verfile = NamedTemporaryFile(delete=False)
news = NamedTemporaryFile(delete=False)
zip = NamedTemporaryFile(delete=False)

def getVersion(que):
    getFileFromUrl(urls[0],que,Verfile)
    s = open(Verfile.name)
    s = s.read()
    expVersion,l_expVersion = readVersion(s)
    que.put([expVersion,l_expVersion])
    sys.exit(1)
def getSide(que):
    file = open(appdata + "\\Lucidity\\launcher\\side.png","wb")
    getFileFromUrl(urls[2],que,file)
    file.close()
    sys.exit(1)
def getBackground(que):
    file = open(appdata + "\\Lucidity\\launcher\\background.png","wb")
    getFileFromUrl(urls[1],que,file)
    file.close()
    sys.exit(1)
def getMain(que):
    extract(getFileFromUrl(urls[4],que,zip),(appdata + "\\Lucidity\\"))
def fetchNews(que):
    file = NamedTemporaryFile(delete=False)
    q = Queue()
    getFileFromUrl(urls[3],q,file)
    file = open(file.name)
    t = str(file.read())
    que.put(t)
# out must look like [msg,progress]
if __name__ == "__main__":
    SCREEN_WIDTH = 800
    SCREEN_HEIGHT = 600

    if os.path.exists(appdata + "/Lucidity"):
        dir = appdata + "/Lucidity"
        if os.path.exists(dir + "/bin/lucidity.exe"):
            Downloaded = True
        if os.path.exists(dir + "/VERSION.txt"):
            file = open(dir + "/VERSION.txt")
            sta = file.read()
            Version,l_Version = readVersion(sta)
            Ver.setVersion(Version)
            Ver.setLauncherVersion(l_Version)

        if os.path.exists(dir + "/launcher/"):
            dir = dir + "/launcher/"
            if os.path.exists(dir + "/background.png"):
                Ver.hasBackground(True)
            if os.path.exists(dir + "/side.png"):
                Ver.hasSide(True)
            if os.path.exists(dir + "/urls.txt"):
                MakeUrls = False
                f = open(dir + "/urls.txt","r")
                sta = f.read()
                no = 0
                sta = sta.splitlines()
                for lines in sta:
                    urls[no] = lines
                    no += 1
        else:
            os.makedirs(dir + "/launcher/")
    else:
        os.makedirs(appdata + "/Lucidity")
    config = pyglet.gl.Config(
    sample_buffers=1,samples=4,vsync = True
    )
    window = pyglet.window.Window(config = config,width=800,height = 600)
    window.set_caption("Firefly Launcher Version: " + Ver.version)
    timer = Tween()
    gui = UI(SCREEN_WIDTH,SCREEN_HEIGHT,window)

    Progress = Queue()
    prc = threading.Thread(target =getVersion,args = (Progress,))
    prc.start()
    Progress1 = Queue()
    downSide = threading.Thread(target = getSide,args = (Progress1,))
    Progress2 = Queue()
    downBack = threading.Thread(target = getBackground,args = (Progress2,))
    downBack.setDaemon(True)
    newsQue = Queue()
    n = threading.Thread(target = fetchNews,args= (newsQue,))
    n.setDaemon(True)
    n.start()
    def initial():
        gui.clear()
        backg = gui.addImage(SCREEN_WIDTH/2,SCREEN_HEIGHT/2,appdata + "/Lucidity/launcher/background.png")

        w = 100
        bottom = gui.addFrame(SCREEN_WIDTH/2,w/2,SCREEN_WIDTH,w)
        bottom.drawLine = False
        img = Image(bottom,0,0,appdata + "/Lucidity/launcher/side.png")
        img.moveTo(img.x,img.y + img.height - bottom.height)


        w,h = 50,50
        recSpiner(bottom,bottom.width-w - 20,h/2,w,h)
        import re
        newsText = ""

        w,h = 300,340
        frame = gui.addFrame(SCREEN_WIDTH - w/2 - 10,SCREEN_HEIGHT/2+20,w,h)
        ox,oy = frame.x -frame.width/2,frame.y + frame.height/2
        def OnDraw():
            pass
        frame.OnDraw = OnDraw
        frame.color = (95,95,95,240)
        w,h = 120,30


        state = State(frame,5,5,frame.width-10,frame.height-10)
        state.newState("Play")
        w,h = 200,30
        Main1 = button(state,state.width/2 - w/2,state.height -(h +10)*2,w,h)
        Main1.color = (9, 112, 84)
        DownloadBar = progressBar(bottom,bottom.width - 460,5,350,20)
        errorText = Text(bottom,bottom.width - 460,60,"Current ver: " + Ver.lversion)

        downprc = Queue()
        downld = threading.Thread(target =getMain,args = (downprc,))
        def Play(n):
            launch(appdata + "/Lucidity/bin/lucidity.exe")
            sys.exit(1)
        def Dow(n):
            downld.start()
            Main1.inactive = True
            Main1.setText("Downloading..")
            pass
        def checkProg(dt):
            if not downprc.empty():
                t = downprc.get()
                err = t[1]
                DownloadBar.setValue(t[0])
                if err != "":
                    errorText.setText(err,10)
                if DownloadBar.val == 100:
                    Main1.setText("Play",12)
                    Main1.OnClick = Play
                    Main1.color = (0, 153, 204)
                    Main1.inactive = False
                    DownloadBar.OnUpdate = None
        DownloadBar.OnUpdate = checkProg

        if not os.path.exists(appdata + "/Lucidity/bin/lucidity.exe"):
            Main1.setText("Download",12)
            Main1.OnClick = Dow
        else:
            Main1.setText("Play",12)
            Main1.OnClick = Play
            Main1.color = (0, 153, 204)

        Main1.line = (0,0,0)

        main2 = button(state,state.width/2 - w/2,state.height -(h +10),w,h)
        def force(x):
            Dow(x)
            main2.inactive = True

        if Ver.compareVer():
            main2.setText("Force Update",12)
            main2.color = (193, 49, 0)
            main2.OnClick = force
        elif not os.path.exists(appdata + "/Lucidity/bin/lucidity.exe"):
            main2.setText("Update",12)
            main2.inactive = True
        else:
            main2.setText("Update",12)
            main2.color = (255, 153, 0)
            main2.OnClick = force
        main2.line = (0,0,0)

        text = textFrame(state,5,10,state.width-15,state.height-20,newsText)
        def getNews(dt):
            if not newsQue.empty():
                txt = newsQue.get()
                text.setText(txt)
                text.OnUpdate = False

        state.switch("Play")
        frame.drawLine = False
        text.OnUpdate = getNews


    frame = gui.addFrame(SCREEN_WIDTH/2,SCREEN_HEIGHT/2,200,230)
    frame.setLabel("Updating Launcher")
    w,h = 40,40
    rectSp = recSpiner(frame,frame.width/2 - w/2,40,w,h)
    text = Text(gui,5,SCREEN_HEIGHT - 10,"Fetching version",size=12)
    text.label.anchor_x = "left"
    pr1 = progressBar(frame,5,100,frame.width-10,20)

    pr2 = progressBar(frame,5,100 + (20+10)*1 ,frame.width-10,20)

    pr3 = progressBar(frame,5,100 + (20+10)*2,frame.width-10,20)
    Loaded = False
    def fn(dt):
        if not Progress.empty():
            data = Progress.get()
            if data[1] != "":
                text.setText(data[1])
                pr1.setValue(data[0])
            if data[0] == 100:
                l = Progress.get()
                while not Progress.empty():
                    l = Progress.get()
                Ver.setActualLauncherVersion(l[0])
                Ver.setActualVersion(l[1])
                print(Ver.compareVerL())
                if not Ver.compareVerL():
                     downSide.start()
                     downBack.start()
                else:
                    if Ver.needsBackground():
                        downBack.start()
                    else:
                        pr3.setValue(100)
                    if Ver.needsSide():
                        downSide.start()
                    else:
                        pr2.setValue(100)
        if not Progress1.empty():
            global HasSide

            l = Progress1.get()
            if l[1] != "":
                text.setText(l[1])
            if l[0] == 100:
                Ver.hasBackground(True)
            pr2.setValue(l[0])

        elif not Progress2.empty():
            global HasBackground
            l = Progress2.get()
            if l[1] != "":
                text.setText(l[1])
            if l[0] == 100:
                Ver.hasBackground(True)
            pr3.setValue(l[0])

        if pr1.val == 100 and pr2.val == 100 and pr3.val == 100:
           def move(dt):
                Ver.saveVersion()
                initial()

           pyglet.clock.schedule_once(move, 0.1)
           del frame.OnUpdate





    frame.OnUpdate = fn
    b = button(frame,5,230 - 35,frame.width - 10,25)
    b.color = (0, 153, 204)
    b.setText("Play Offline")
    b.line = (0,0,0)
    if os.path.exists(appdata + "/Lucidity/bin/lucidity.exe"):
        def Click(x):
            launch(appdata + '/Lucidity/bin/lucidity.exe')
            os._exit(0)
        b.OnClick = Click
    else:
        b.inactive = True


    def OnDraw():
        gui.draw()
        pass
    def OnUpdate(dt):
        timer.update(dt)
        gui.update(dt)

    @window.event
    def on_mouse_motion(x,y,dx,dy):
        pass
    @window.event
    def on_mouse_press(x,y,b,mod):

        gui.mouse_pressed(x,y,b)
    @window.event
    def on_mouse_release(x,y,b,mod):
        gui.mouse_released(x,y,b)
    @window.event
    def on_draw():
        window.switch_to()
        glClear(GL_COLOR_BUFFER_BIT)
        OnDraw()
        window.flip()
    pyglet.clock.schedule_interval(OnUpdate, 0.03)
    gui.update(0.1)
    pyglet.app.run()