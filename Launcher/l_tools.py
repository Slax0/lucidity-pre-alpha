__author__ = 'X3-Slax0'
from multiprocessing import Process,Queue,Pipe
import encodings
import os
import math
import sys
import zipfile,urllib.request,shutil
import encodings.idna


def getFileFromUrl(url,out,out_file):
    file_name = ""
    pak = ["0",""]
    def stats(b,cs,ts):
        prc = int(round((float(b)/ts)*100,2))
        pak[0] = prc
        if prc == 100:
            pak[1] = "Done"
        out.put(pak)
    def read(response,size=128,hook=None):
        tsize = response.headers['content-length']
        tsize = int(tsize)
        bytes = 0
        data = bytearray()
        while 1:
            c = response.read(size)
            data += c
            bytes += len(c)
            if not c:
                break
            if hook:
                hook(bytes,size,tsize)
        return bytes,data
    try:
        with urllib.request.urlopen(url) as response:
            pak = ["",""]
            pak[0] = 0
            if response:
                pak[1] = "Connection made."
                b,data = read(response,hook=stats)
                out_file.write(data)
                out_file.close()
                file_name = out_file.name
            else:
                pak[1] = ("Cannot connect!")
                out.put(pak)
    except IOError as obj:
        pak[1] = str(obj)
        out.put(pak)
    return file_name

def path(data,item):
    relative = os.path.join(data, item)
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, relative)
    return os.path.join(relative)
colors = {
    "red":(255,0,0),
    "r":(255,0,0),
    "blue":(0,0,255),
    "green":(0.255,0),
    "gray":(128,128,128),
    "lightGray":(168,168,168),
    "darkGray":(68,68,68),
}
def extract(filename,dest):
    with zipfile.ZipFile(filename) as zf:
        zf.extractall(dest)
def launch(program):
  import subprocess
  """launch(program)
   Run program as if it had been double-clicked in Finder, Explorer,
   Nautilus, etc. On OS X, the program should be a .app bundle, not a
   UNIX executable. When used with a URL, a non-executable file, etc.,
   the behavior is implementation-defined.

   Returns something false (0 or None) on success; returns something
   True (e.g., an error code from open or xdg-open) or throws on failure.
   However, note that in some cases the command may succeed without
   actually launching the targeted program."""
  if sys.platform == 'darwin':
    ret = subprocess.call(['open', program])
  elif sys.platform.startswith('win'):
    ret = os.startfile(os.path.normpath(program))
  else:
    ret = subprocess.call(['xdg-open', program])
  return ret