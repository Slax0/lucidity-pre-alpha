from multiprocessing import Process,Queue,Pipe
import os
import math
import sys
import zipfile,urllib.request,shutil
import tempfile
import sfml.system
import sfml.window
import sfml.graphics
import sfml.network
import sfml.audio
import sfml as sf


appdata = os.getenv('APPDATA')
def percentage(whole,part):
  return 100 * float(part)/float(whole)
def percentageOf(whole,percent):
  return (percent * whole) / 100.0
def getDirSize(host,dir,size):
    print("Geting filesize: " + dir)
    names = host.listdir(dir)
    for name in names:
        name = dir + "/" + name
        if host.path.isfile(name):
            size += host.path.getsize(name)
        elif host.path.isdir(name):
            size += getDirSize(host,name,size)
    return size
def recrusiveDownload(ftp_host,curdir,dest,exp,size):
    ftpIn = Queue()
    def ClearQue(q):
        while not q.empty():
            q.get()
    size = size or 0
    prc = size/exp
    print("Progress: " + str(prc))
    print("Checking dir: " + curdir)
    names = ftp_host.listdir(curdir)
    for name in names:
        name = curdir + "/" + name
        print("procesing: " + name)
        if ftp_host.path.isfile(name):
            size =+ int(ftp.path.getsize(name))
            try:
                ftp_host.download(name,dest + name)
                ClearQue(ftpIn)
            except OSError as obj:
                print(obj.strerror)
        elif ftp_host.path.isdir(name):
            print("Is Dir: " +name)
            recrusiveDownload(ftp,name,dest,exp,size)
            try:
              os.makedirs(dest + name)
            except IOError as obj:
                print(obj.strerror)
        prc = size/exp
    print("Done")
global ftp
url = "https://www.dropbox.com/s/cqnx9zlnqttelc2/settings.zip?raw=1"
dest = appdata + "/Lucidity/"
if not os.path.exists(dest):
    os.makedirs(dest)
def getFileFromUrl(url,out):
    file_name = ""
    def stats(b,cs,ts):
        prc = int(round((float(b)/ts)*100,2))
        out.put(prc)
    def read(response,size=2048,hook=None):
        tsize = response.headers['content-length']
        tsize = int(tsize)
        bytes = 0
        data = bytearray()
        while 1:
            c = response.read(size)
            data += c
            bytes += len(c)
            if not c:
                break
            if hook:
                hook(bytes,size,tsize)
        return bytes,data
    try:
        with urllib.request.urlopen(url) as response,body as :
            print("Response: " + str(response))
            b,data = read(response,hook=stats)
            out_file.write(data)
            out_file.close()
            file_name = out_file.name
    except IOError as obj:
        print(str(obj))
    return file_name
def extract(filename,dest):
    with zipfile.ZipFile(filename) as zf:
        zf.extractall(dest)
# extract(getFileFromUrl(url),dest)

def threadFn(q):
    while True:
        extract(getFileFromUrl(url,q),dest)
        sys.exit()


import sfml as sf
window = sf.RenderWindow(sf.VideoMode(800,600),"New Window")

while window.is_open:
    for event in window.events:
        print(event)
        if type(event) is sf.CloseEvent:
            window.close()
    window.clear()
    window.display()


