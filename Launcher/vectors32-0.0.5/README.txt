
================
README.txt
=============

Introduction - place holder only!
=============

This is written in the reST format, so it looks a little odd. As always, reST
source is plain text and easy to read.

To install this matrix algebra module issue the following command::

  sudo python3 setup.py install

On a kubuntu 11.04 "natty" platform the module and its test unit are installed in::

  /usr/local/lib/python3.2/dist-packages/matalg/

The directory **matalg** is created by the **setup.py install** command and the files are copied to it. Of course, **Your Mileage May Vary** depending on the platform. The module is designed to work with Python 3.2 or higher.

Included with the package are **Quick Start**, **User Manual** and 
**Reference Manual** in pdf format. Please read this information at your lea sure. The pdf files are in the matalg-pdf sub-directory. The author of this package can be contacted by email at the following address::

  algis.kabaila@gmail.com


Good computing!


Algis Kabaila, 2011-07-25
