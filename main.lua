_RELEASE = "ALPHA RELEASE 0.1B"
SHOWUI = false 
DEBUG = false 
STATS = false
PROFILER = false 
function removeHud()
	_G["SHOWUI"] = false	
end 
love.filesystem.setIdentity("Lucidity")
Channel = {
	MaxCapture = 0 
} 
getEnv = function()
	local env = {}  
	for i,v in pairs(_G) do 
		if i ~= "os" then 
			env[i] = v  
		end 
	end  
	local banned = {
		"setfenv",
		"getfenv",
		"debug",
		"collectgarbage",
		"dofile",
		"_G",
		"getfenv",
		"getmetatable",
		"load",
		"os",
		"loadfile",
		"loadstring",
		"coroutine",
		"module",
		"require",
		"package",
		"string",
		"math",
		"io",
		"newproxy",
	}
	for i,v in ipairs(banned) do 
		env[v] = nil 
	end 
	env["string"] = deepCopy2(string)
	env["table"] = deepCopy2(table)
	env["math"] = deepCopy2(math)
	env["setmetatable"] = function(...) return setmetatable(...) end 
	env["require"] = function(dir)
		local fn,err
		if love.filesystem.isDirectory(dir) then 
			local files = love.filesystem.getDirectoryItems(dir)
			local found = false 
			for i,v in ipairs(files) do 
				if not found then 
					if v == "init.lua"  then 
						found = true 
						fn,err =  pcall(love.filesystem.load(dir.."/init.lua"))
					elseif v == "main.lua" then
						found = true 
						fn,err =  pcall(love.filesystem.load(dir.."/main.lua"))
					end 
				end 
			end 
			assert(found,"Cannot find: "..dir.."/init.lua or "..dir.."/main.lua" )
		elseif love.filesystem.exists(dir..".lua") then 
			err,fn = pcall(love.filesystem.load(dir..".lua"))
		else 
			error("Does not exist:"..dir..".lua")
		end 
		assert(fn,err)
		return fn 
	end
	env.love = deepCopy2(love)
	env.love.filesystem.remove = nil
	env.love.system.openURL = nil 
	return env
end
love.window.setIcon(love.image.newImageData("assets/icon.png"))
function getPackage(...)
	local _PACKAGE  = ...
	_PACKAGE = string.gsub(_PACKAGE,"[.]","/")
	local index = string.find(_PACKAGE, "/[^/]*$")
	_PACKAGE = string.sub(_PACKAGE,1,index)
	_PACKAGE = string.gsub(_PACKAGE,"//","/")
	return _PACKAGE
end 
function getPackageAlt(...)
	local _PACKAGE  = ...
	_PACKAGE = string.gsub(_PACKAGE,"[.]","/")
	_PACKAGE = string.gsub(_PACKAGE,"//","/")
	return _PACKAGE
end 
function getPackageAlt2(...)
	local _PACKAGE  = ...
	local index = string.find(_PACKAGE, "/[^[.]]*$")
	_PACKAGE = string.sub(_PACKAGE,1,index)
	print(_PACKAGE)
	return _PACKAGE
end 
function getCleanDate()
	local date = os.date()
	local s = string.find(date," ")
	local time = string.sub(date,s+1)
	local date =  string.sub(date,1,s)
	return {date,time}
end
function copyFile(dir,dest)
	local d = love.filesystem.newFileData(dir)
	love.filesystem.write(dest,d)
end
colors = {
	red = {255,0,0},
	darkRed = {127,0,0},
	green = {0,255,0},
	darkGreen = {38,127,0},
	blue = {0,0,255},
	darkBlue = {0,19,127},
	lightBlue = {0,148,255},
	paleBlue = {163,169,206},
	yellow = {255,216,0},
	orange = {255,106,0},
	ambient = {57,96,124},
	black = {0,0,0},
	gray = {128,128,128},
	darkGray = {28,28,28},
	violet = {255,0,255},
	dgray = {228,228,228},
	lightGray = {64,64,64},
	white = {255,255,255},
	cyan = {0,255,255},
} 
function copyFiles(dir,dest) 
	print("Copying Files: ",dir,dest)
	if not love.filesystem.exists(dest) then love.filesystem.createDirectory(dest) end 
	local lfs = love.filesystem
	local files = lfs.getDirectoryItems(dir)
	for i,v in ipairs(files) do 
		local f = dir.."/"..v
		if lfs.isFile(f) then 
			local a = love.filesystem.newFileData(f)
			love.filesystem.write(dest.."/"..v,a)
		elseif lfs.isDirectory(f) then 
			love.filesystem.createDirectory(dest.."/"..v)
			copyFiles(f,dest.."/"..v)
		end 
	end 
end 
Fonts = {}
function getFonts(dir)
	if love.filesystem.exists(dir.."/fonts/") then 
		local fdir = love.filesystem.getDirectoryItems(dir.."/fonts/")
		for i,v in ipairs(fdir) do
			local err,font = pcall(love.graphics.newFont,dir.."/fonts/"..v)
			local dir = dir.."/fonts/"..v
			v = v:gsub(".otf","")
			v = v:gsub(".ttf","")
			print(err)
			print(v,font)
			if font then 
				Fonts[v] = {dir = dir,src = font} 
			end 
		end 
	end 
end 
if love.filesystem.isFused() then
    local dir = love.filesystem.getSourceBaseDirectory()
	local success = love.filesystem.mount(dir, "")
	print(dir,success)
	function getActiveDirectory()
		return dir 
	end 
	print("FUSED!")
else 
	function getActiveDirectory()
		return love.filesystem.getActiveDirectory()
	end 
end
local function plain_date()
	local t = string.gsub(os.date(),"/","")
	t = string.gsub(t,":","_")
	t = string.gsub(t," ","_")
	return t 
end
function saveLog(bool)
	local name = "LOG"
	if bool then 
		name = "LOG_"..plain_date()
	end 
	local new = {} 
	for i,v in ipairs(debug_text) do 
		new[i] = v.."\r\n"
	end 
	love.filesystem.write(name,table.concat(new))
end
getFonts("")
TotalCount = 0 
require("profiler")
local net = require("network")
require("editor/replicate")
Flux = require("lib.flux")
Gamestate = require "hump/gamestate"
Timer = require "hump.timer"
vector = require "hump/vector" 
HC = require "hardon"
local animationMeta = require "anal"
Network = net:new()
Intro = require "intro"

Credits = require "credits"

ui_package = require "menu.ui"
Console = require("console")
msgQue = require("playmap/msgQue")  
EmptyTable = {}
debug_text = {} 
local olp = print
MaxCapture = 0 
function print(...)
	olp(...)
	local arg = {...}
	for i,v in pairs(arg) do 
		table.insert(debug_text,tostring(v))
	end 
end
local gname,gver,gvendor,gdevice = love.graphics.getRendererInfo()
local data = {
	_VERSION,
	"Lucidity: ".._RELEASE,
	"Date:  "..os.date(),
	"--------------",
	"Supports:",
	"--------------",
	"Framebuffer: "..tostring(love.graphics.isSupported("canvas")),
	"Shader:"..tostring(love.graphics.isSupported("shader")),
	"HDR framebuffer: "..tostring(love.graphics.isSupported("hdrcanvas")),
	"Multi-framebuffer: "..tostring(love.graphics.isSupported("multicanvas")),
	"MipMap: "..tostring(love.graphics.isSupported("mipmap")),
	"DXT: "..tostring(love.graphics.isSupported("dxt")),
	"BC5: "..tostring(love.graphics.isSupported("bc5")),
	"--------------",
	"Graphics data: ",
	"---------------",
	tostring(gname),
	tostring(gver),
	tostring(gvendor),
	tostring(gdevice),
}
for i,v in ipairs(data) do 
	print(v.."\n")
end 
local function error_printer(msg, layer)
	print((debug.traceback("Error: " .. tostring(msg), 1+(layer or 1))))
end
function love.errhand(msg)
	saveLog()
	local lg = love.graphics
	msg = tostring(msg)
	print(debug.traceback())
	error_printer(msg, 2)
 
	if not love.window or not love.graphics or not love.event then
		return
	end
 
	if not love.graphics.isCreated() or not love.window.isCreated() then
		local success, status = pcall(love.window.setMode, 800, 600)
		if not success or not status then
			local success, status =  pcall(love.window.setMode, 680, 400)
			if not success or not status then
				return
			end 
		end
	end
	love.window.setMode(800,600,{fullscreen = false,fullscreentype = "normal",borderless = false})
 
	-- Reset state.
	if love.mouse then
		love.mouse.setVisible(true)
		love.mouse.setGrabbed(false)
	end
	if love.joystick then
		-- Stop all joystick vibrations.
		for i,v in ipairs(love.joystick.getJoysticks()) do
			v:setVibration()
		end
	end
	if love.audio then love.audio.stop() end
	love.graphics.reset()
	local font = love.graphics.setNewFont(math.floor(love.window.toPixels(14)))
 
	local sRGB = select(3, love.window.getMode()).srgb
	if sRGB and love.math then
		love.graphics.setBackgroundColor(love.math.gammaToLinear(0, 0,0))
	else
		love.graphics.setBackgroundColor(0,0,0)
	end
 
 
	local trace = debug.traceback()
 
	love.graphics.clear()
	love.graphics.origin()
 
	local err = {}
 
	table.insert(err, "Error\n")
	table.insert(err, msg.."\n\n")
 
	for l in string.gmatch(trace, "(.-)\n") do
		if not string.match(l, "boot.lua") then
			l = string.gsub(l, "stack traceback:", "Traceback\n")
			table.insert(err, l)
		end
	end
 
	local p = table.concat(err, "\n")
 
	p = string.gsub(p, "\t", "")
	p = string.gsub(p, "%[string \"(.-)\"%]", "%1")
	print(p)
	if not DEBUG then 
		local op = p 
		p = "Lucidity has crashed!\n\n Use this log file for forums to traceback your issue (press enter): "..love.filesystem.getRealDirectory("LOG").."/LOG\n\n\n"
		p = p.."Details: \n ------------------------------------------------------------- \n"..op.." \n ------------------------------------------------------------- \n"
	end 
	local function draw()
		local pos = love.window.toPixels(70)
		love.graphics.clear()
		love.graphics.printf(p, pos, pos, love.graphics.getWidth() - pos)
		love.graphics.present()
	end
	local savedLog
	while true do
		love.event.pump()
 
		for e, a, b, c in love.event.poll() do
			if e == "quit" then
				if not savedLog then 
					saveLog(true)
				end 
				return
			end
			if e == "keypressed" then 
			if a == "escape" then
				if not savedLog then saveLog(true) end 
				return
			elseif a == "return" then 
				local name = saveLog(true)
				love.system.openURL(love.filesystem.getRealDirectory(name or "LOG"))
			end 
			end
		end
 
		draw()
 
		if love.timer then
			love.timer.sleep(0.1)
		end
	end
end
Console:attach(debug_text)
function table.zsort(t)
  local a = {}
  for _,n in pairs(t) do table.insert(a, n) end
	table.sort(a,function(a,b)return (a.zmap or 1) < (b.zmap or 1) end)
  return a
end
function table.reverse(t)
    local reversedTable = {}
    local itemCount = #t
    for k, v in ipairs(t) do
        reversedTable[itemCount + 1 - k] = v
    end
    return reversedTable
end 
function RCToWH(image,rows,columns)
	local iw,ih = image:getDimensions()
	local w,h = iw/columns,ih/rows
	return w,h
end 
function love.quit() 
	saveLog()
end 
function printStats(x,y)
	local stats = love.graphics.getStats()
	local s = #Collider._active_shapes
	local memory
	local text = {
		"Draw calls:"..stats.drawcalls,
		"Canvas Uses: "..stats.canvasswitches,
		"Texture memory: "..math.floor(stats.texturememory/1024).." KB",
		"Lua memory: "..math.floor(collectgarbage("count")),
		"Images: "..stats.images,
		"Framebuffers: "..stats.canvases,
		"Fonts: "..stats.fonts,
		"Shapes: "..s,
	}
	for i,v in ipairs(text) do 
		text[i] = v.."\n"
	end 
	love.graphics.print(table.concat(text),x,y)
end 
function upDown(item,range,original,name,speed,mode)
	local v = item.__Tweens or {} 
	if not v[name] then v[name] = {} end 
	local fin = v[name].finished
	local tables = {}
	local tables2 = {}
	tables[name] = original + range
	tables2[name] = original - range
	if not fin then
		v[name].finished = 1
	elseif fin == 1 then 
		v[name].tween = Timer.tween(speed or 2,item,tables,mode,function() v[name].finished  = 2 end)
		v[name].finished   = 0
	elseif fin == 2 then 
		v[name].meta_tween = Timer.tween(speed or 2,item,tables2,mode,function() v[name].finished  = 1 end)
		v[name].finished  = 0
	end
	item.__Tweens = v 
end
function loopRange(root,range,name,speed,mode)
	upDown(root,range,root[name],name,speed,mode)
end 


restart = {}
menu = {}
conf = {}
game = {}
blanc = {}
local test = {}
testRun = {}
batle = {}
local _dir = "/assets/crystals"
local files = love.filesystem.getDirectoryItems(_dir)

crystals = {}
for i,v in ipairs(files) do
	crystals[string.gsub(v,".png","")] = love.graphics.newImage(_dir.."/"..v)
end
function setColor(...)
	local args = {...} 
	local r,g,b,a = args[1] or 255, args[2] or 255 ,args[3] or 255,args[4]
	love.graphics.setColor(r,g,b,a)
end
function parsecolor(t)
	local c = {}
	for k in string.gmatch(t, "-?[0-9]+[.]?[0-9]*") do
		if #c < 4 then 
			table.insert(c, tonumber(k))
		end 
	end
	if not c[1] then 
		c[1] = 0 
	end 
	if not c[2] then 
		c[2] = 0 
	end 
	if not c[3] then 
		c[3] = 0 
	end 
	return c
end 
function parseTextVariables(t)
	local c = {}
	for k in string.gmatch(t, "-?[0-9]+[.]?[0-9]*") do
		table.insert(c, tonumber(k))
	end
	return c
end 
parseColor = parsecolor
crystal = {}
for i,v in pairs(crystals) do 
	local a = {}
	local img = v
	img:setFilter("linear","linear")
	local row = 1
	local column = 15 
	local w = img:getWidth()/column
	local h = img:getHeight()/row
	a.anim = newAnimation(img,w,h,0.07)
	a.w = w
	a.h = h
	a.delay = 0.07
	a.data = {img,w,h,0.07} 
	crystal[i] = a
end




Outline = love.graphics.newShader("s/outline.glsl")
Editor = require "editor/editor"
require "Tserial"
mousespeed = 20
warning = require("menu.warningMsg")
TweenStorage = require("tweens")
PlayMap = require("playmap/play")
loadGhost = require("Ghosts/load")
hashHide = require("hashids")

playerMaskCategory = 16

function checkPlayerColision(col)
	local player,part
	local data 
	if col.getUserData then 
		data = col:getUserData()
	end 
	if data then 
		if data.source then 
			if data.source.type == "player" then 
				player = data.source
				part = data.part 
			end 
		end 
	end 
	return player,part
end 
function checkPlayerCol(a,b)
	local player,part 
	player,part = checkPlayerColision(a)
	other = b
	if not player then 
		player,part = checkPlayerColision(b)
		other = a
	end 
	return player,part,other
end 
function tableLenght(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end
function getAngle(x1,y1, x2,y2) return math.atan2(x2-x1, y2-y1) end
function findDistance(x,y)
	local cond 
	local x,y = tonumber(x),tonumber(y)
	if not x or not y then 
		cond = true 
	elseif type(x) ~= "number" or type(y) ~= "number" then 
		cond = true 
	end 
	
	if cond then 
		x = 0 
		y = 0 
	end 
	local xd = math.max(x,y) - math.min(x,y)
	return xd
end 
function findDimensions(shape,y,x1,y1)
	local w,h 
	if type(shape) ~= "number" then 
		local x,y = shape:bbox()
		local x1,y1 = shape:center()
		w = findDistance(x,x1)*2
		h = findDistance(y,y1)*2
	else 
		w = findDistance(shape,x1)
		h = findDistance(y,y1)
	end
	return w,h
end 
function clearTable(tables)
	assert(tables,"No table to clear!")
	for i,v in ipairs(tables) do 
		tables[i] = nil
	end 
end 
function drawRelease()

	local olf = love.graphics.getFont()
	love.graphics.setFont(dfs)
	local r,g,b,a = unpack(colors.red)
	a = 150
	setColor(255,255,255,a)
	love.graphics.rectangle("fill",5,55,dfs:getWidth(_RELEASE),dfs:getHeight())
	
	setColor(r,g,b,a)
	love.graphics.print(_RELEASE,5,55)
		-- love.graphics.print(_RELEASE,5,25)
	setColor(255,255,255)
	love.graphics.setFont(olf)
end 
function incrementPos(x,y,ofx,ofy,degree)
	local ofx,ofy = ofx or 0,ofy or 0 
	local ox,oy = (x or 0) - ofx,(y or 0) - ofy
	local degree = degree or 32
	local deg64 = ox %degree  
	if x then 
		if deg64 ~= 0 then 
			local dist64 = findDistance(ox,ox + deg64)
			local dist64b = findDistance(ox,ox - deg64)
			if dist64 > dist64b then 
				x = x + deg64
			else
				x = x - deg64
			end 
		end 
	end 
	if y then 
		local deg64 = oy %degree
		if deg64 ~= 0 then 
			local dist64 = findDistance(oy,oy + deg64)
			local dist64b = findDistance(oy,oy - deg64)
			if dist64 > dist64b then 
				y = y + deg64
			else
				y = y - deg64
			end 
		end
	end 
	return x,y
end 


function showFPS()
	local shader = love.graphics.getShader()
	
	local fps = love.timer.getFPS()
	local h = fps
	if h > 300 then 
		h = 300 
	end 
	local color = hsvToRgb(h,100,100)
	-- if fps < 30 then 
	love.graphics.setColor(color)
	love.graphics.print("FPS:"..fps,5,5)
	love.graphics.setColor(255,255,255)
end 
function limitLag(dt,func)
	local accum = dt
	while accum > 0 do
		local dt = math.min(1/30,accum)
		accum = accum - dt
		func() 
	end
end 
function inRange(x,item,y)
	if x < item and item < y then 
		return true 
	else 
		return false 
	end 
end 
function inRangeHigher(x,item,y)
	if x < item and item <= y then 
		return true 
	else 
		return false 
	end 
end 
function inRangeLower(x,item,y)
	if x <= item and item < y then 
		return true 
	else 
		return false 
	end 
end 
function round(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end
px =	love.graphics.getWidth()/800 
py = 	love.graphics.getHeight()/600

current_map = "maps/new"

tablelenght = {}

testBed = {}
mainMenu = require "mainMenu.main"
uniBody = "mainMenu/fonts/Unibody8Pro-Regular.otf"
Glyhps = love.graphics.newImageFont("fonts/crypt.png","abcdefghijklmnoprstuvwxyzABCDEFGHIJKLMNOPRST")
uniBody_italic = "mainMenu/fonts/Unibody8Pro-RegularItalic.otf"
uniBody_caps = "mainMenu/fonts/Unibody8Pro-RegularSmallCaps.otf"
uniBody_bold = "mainMenu/fonts/Unibody8Pro-Bold.otf"
uniBody_black = "mainMenu/fonts/Unibody8Pro-Black.otf"
function makeFont(size,type)
	local ftype = uniBody
	if type == "italic" then 
		ftype = uniBody_italic
	elseif type == "caps" then 
		ftype = uniBody_caps
	elseif type == "bold" then 
		ftype = uniBody_bold
	elseif type == "black" then 
		ftype = uniBody_black
	end 
	local font = love.graphics.newFont(ftype,size)
	return font
end
transferProtocol = {
	from = nil,
	to = nil,
}

uiBase = require("menu.ui")

effectColors = {
	poison = {function(x,y,size) love.graphics.circle("line",x,y,size) end ,{0,255,48,255}}, -- func,color,
	fire =   {function(x,y,size) love.graphics.rectangle("fill",x,y,size,size) end ,{255,0,0,255}},
	curse = {function(x,y,size) love.graphics.circle("fill",x,y,size/2) end,{math.random(67,120),math.random(0,120),math.random(67,120),255}},
	drown = {function(x,y,size)love.graphics.circle("line",x,y,size) end, love.graphics.circle,{0,0,255,255}},
}

cardColors = {
	basic = {243,202,91},
	blue = {130,119,252},
	cyan = {208,245,248},
	green = {178,231,126},
	inverted = {11,23,61},
	pink = {254,118,142},
	player = {223,198,154},
	silver = {209,195,219},
}
basicFrameColor = {125,125,125,195}
basicButtonColor = {55,55,55,155}

loadMob = require("Ghosts/load")
loadCards = require("Ghosts/cards/loadCards")
battle = require "battle"
keyControl = require("menu/keyControl")



_currentGamestate = {}
local _dir = "assets/effects/"
local jmp = love.graphics.newImage(_dir.."jump.png")
local img = love.graphics.newImage("assets/float.png")
local row = 2
local column = 8 
local w = img:getWidth()/column
local h = img:getHeight()/row
local s = 1
local speed = 0.20
FloatingAnimation = {img,w,h,speed,1,11}

stock_effects = {
	jump = {
		image = jmp,
		anim = newAnimation(jmp,62,45,0.1,1,5)
	}
}

mColors = {} 
mColors["Strong"] 			= colors.green
mColors["Weak"] 			= colors.violet 
mColors["Block"] 			= colors.blue 
mColors["Critical"] 		= colors.yellow 
mColors["Miss!"] 			= colors.white 
mColors["Crit Miss!"] 		= colors.red
mColors["Deathblow!!"] 		= colors.darkRed
function hsvToRgb(h, s, v)
	local h,s,v = h,1,1
    local h,s,v = math.fmod(h,360),s,v
    
    if s==0 then return { v,v,v } end
    
    h=h/60
    i=math.floor(h)
    f=h-i
    p=v*(1-s)
    q=v*(1-s*f)
    t=v*(1-s*(1-f))
    
    if i==0 then r=v g=t b=p
    elseif i==1 then r=q g=v b=p
    elseif i==2 then r=p g=v b=t
    elseif i==3 then r=p g=q b=v
    elseif i==4 then r=t g=p b=v
    else r=v g=p b=q
    end
    
    r=r*255
    g=g*255
    b=b*255
    
    return { r,g,b }

end
function MinMaxFromTable(tables)
	local min,max = 1,1 
	for i,v in pairs(tables) do 
		if type(i) ~= "number" then
			error("Index is not a number: "..i) 
		end 
		if i > max then
			max = i 
		end 
		if i < min then 
			min = i
		end
	end 
	return min,max
end 
function genColor(rarity,degree,hueEffect,satEffect,velEffect) -- degree aka out of X
	local h,s,v = 180,100,100
	if hueEffect then 
		local start = 180
		local onPrcS = (360 - start)/100
		h = start + onPrcS*(rarity*degree)
	end 
	if satEffect then 
		local start = 0
		local onPrcS = (100 - start)/100
		s = start + onPrcS*(rarity*degree)
	end 
	if velEffect then 
		local start = 0
		local onPrcS = (100 - start)/100
		v = start + onPrcS*(rarity*degree)
	end 
	local color = hsvToRgb(h,s,v)
	return color
end 
local _dir = "/assets/icons"
local files = love.filesystem.getDirectoryItems(_dir)
GrayCard = love.graphics.newImage("Ghosts/cards/gray.png")
icons = {}
for i,v in ipairs(files) do
	icons[string.gsub(v,".png","")] = love.graphics.newImage(_dir.."/"..v)
end
local w,h = RCToWH(icons["attack"],1,7)
icons["attack"] = newAnimation(icons["attack"],w,h,0.05,1,7)
function crop(image,imagex,imagey,fw,fh,scalex,scaley,direction)
	assert(image,"Image you are trying to crop is:"..tostring(image).." ,expected image data/image location")
	if image ~= 0 and type(image) == "string" then
		image = love.graphics.newImage(image)
	end
	local scalex = scalex or 1 
	local scaley = scaley or 1 
	local crop = love.graphics.newQuad(imagex,imagey,fw,fh,image:getDimensions())
	local canvas_n = love.graphics.newCanvas(fw*scalex,fh*scaley)
	love.graphics.setCanvas(canvas_n)
		canvas_n:clear()
		love.graphics.push()
				love.graphics.scale(scalex,scaley)
			love.graphics.setBlendMode('alpha')
			love.graphics.setColor(255,255,255)
			if direction == nil or direction == "left" then
				love.graphics.draw(image,crop,0,0,0,1,1)
			elseif direction == "right" then
				love.graphics.draw(image,crop,0,0,0,-1,1,fw)
			end
		love.graphics.pop()
	love.graphics.setCanvas()
	local uimage = canvas_n:getImageData()
	local nimage = love.graphics.newImage(uimage)
	return nimage
end

function removeFromTable(tables,item)
	local removed = 0 
	assert(tables,"No Table!")
	for i,v in pairs(tables) do
		if item == v then 
			removed = removed + 1
			table.remove(tables,i)
		end 
	end 
	return removed
end 
function getFactors(number)
    --[[--
    Gets all of the factors of a given number
    @Parameter: number
        The number to find the factors of
    @Returns: A table of factors of the number
    --]]--
    local factors = {}
    for possible_factor=1, math.sqrt(number), 1 do
        local remainder = number%possible_factor
        if remainder == 0 then
            local factor, factor_pair = possible_factor, number/possible_factor
            table.insert(factors, factor)
            if factor ~= factor_pair then
                table.insert(factors, factor_pair)
            end
        end
    end
    table.sort(factors)
    return factors
end
function mergeTable(table1,table2,mode)
	local mode = mode or "" 
	local force = false 
	local add = false 
	if mode == "add" then 
		add = true 
	elseif mode == "force" then 
		force = true 
	end 
	for k,m in pairs(table2) do 
		local v = table1[k]
		if not v then
			table1[k] = m
		elseif type(table1[k]) == "table" then 
			mergeTable(table1[k],table2[k],modes)
		elseif force or add then  
			if force then 
				table1[k] = m 
			else
				if type(v) == "string" then 
					table1[k] = v..m 
				elseif type(v) == "number" then 
					table1[k] = v + m 
				end 
			end 				
		end			
	end 
end 
function withinRange(v,x,y)
	-- format: item, min, max
	if v < x and y < v then
		return true 
	end 
end 
local rect = love.graphics.newMesh{{0,0, 0,0}, {1,0, 0,0}, {1,1, 0,0}, {0,1, 0,0}}
function DrawRectangle(x, y, width, height, angle, ox, oy, kx, ky)
    love.graphics.draw(rect, x, y, angle, width, height, (ox or 0)/width, (oy or 0)/height, kx, ky)
end


function getTime(sec)
	local sec = sec or 0
	local hr = math.floor(sec/(60*60))
	local dm = sec %(60*60)
	local m = math.floor(dm/60)
	local ds = dm % 60
	local s = math.ceil(ds/60)
	return hr,m,s
end
LOCKCONTROLS = false 
function nokey()
	return false,"none"
end
function keyboardpress(key,block)
	if LOCKCONTROLS and not block then 
		return false 
	else 
		local a = love.keyboard.isDown(key)
		if a then 
			LOCKCONTROLS = false 
		end 
		return a,"keyboard "..key,"keyboard",key	
	end
end
function mousepress(key,block)
	if LOCKCONTROLS and not block then 
		return false 
	else 
		local a = love.mouse.isDown(key)
		if a then 
			LOCKCONTROLS = false 
		end 
		return a,"mouse "..key,"mouse",key
	end
end
function gamepadpress(joystick2,key,block)
	if LOCKCONTROLS then 
		return false 
	else 
		local a = joystick2:isDown(key)
		if a then 
			LOCKCONTROLS = false 
		end 
		return a,"joystick "..key,"joystick",joystick2
	end
end
function combo(k,k2)
	local t = false 
	if k and k2 then 
		t =  true
	end 
	return t
end 


playerMobs = {
	{
		name = "tremble",
	},
	{
		name = "lupus",
	},
	{
		name = "bile",
	},
	{
		name = "bile",
	},
	{
		name = "Sui-tan",
	},
}
playerCards = {
	"trap",
	"heal",
	"redpurge",
	"icestorm",
	"redpurge",
	"icestorm",
	"redpurge",
	"redpurge",
}
playerData = {
	saveName = "new",
	essence = 200,
	playTime = 0,
	children_maps = {}
} 
SaveName = "untitled"
loadTime = love.timer.getTime()
playersMobs = {} 
local lfs = love.filesystem
local function checkRes()
	local dir = "resources/"
		local function make(name) 
			if not lfs.exists(name) then 
				lfs.createDirectory(name)
			end 
		end 
	make(dir)
	make("save_data")
	make("maps/")
	make(dir.."image")
	make(dir.."modules")
	make(dir.."skyboxes")
	make(dir.."sounds")
	make(dir.."special")
	make(dir.."spritesheets")
	if not lfs.exists("cards") then 
		copyFiles("copy/cards","cards") 
	end 
	if not lfs.exists("ghosts") then 
		copyFiles("copy/mobs","mobs") 
	end 
end 
checkRes()
function findMap(name) 
	local found 
	local dir = love.filesystem.getDirectoryItems("maps/")
	for i,v in ipairs(dir) do 
		if v == name then 
			found = "maps/"..v 
			break
		end 
	end 
	if not found then 
		local dir = love.filesystem.getDirectoryItems("inbuilt_maps/")
		for i,v in ipairs(dir) do 
			if v == name then 
				found = "inbuilt_maps/"..v 
				break
			end 
		end 
	end 
	return found 
end 
function saveGame(name,map)
	local data 
	local cg = Gamestate.current()
	local t = {}
	local saveTime = love.timer.getTime()
	local elapsed = findDistance(loadTime,saveTime)
	local date = getCleanDate()
	local name = name or playerData.saveName
	local mapName = map or Gamestate.current().map
	
	local mapMode
	mapMode = cg.getType
	if mapMode then 
		mapMode = mapMode()
	end 
	t.playTime = playerData.playTime + elapsed 
	t.date = date
	SaveName = name 
	t.essence = playerData.essence
	local cards = {}
	for i,v in ipairs(playersCards) do 
		local name = v:getName() or v._name
		table.insert(cards,name)
	end 
	t.cards = cards 
	t.mapName = mapName 
	t.map = mapName
	t.mapMode = mapMode 
	t.children_maps = playerData.children_maps or {}
	t.gameData = {} 
	if cg.saveGame then 
		t.gameData = cg:saveGame()
	end 
	local mobs = {}
	for i,v in ipairs(playersMobs) do 
		local t = v:save()
		table.insert(mobs,t)
	end 
	t.mobs = mobs
	if not love.filesystem.exists("save_data/") then 
		love.filesystem.createDirectory("save_data/")
	end 
	data = Tserial.pack(t)
	
	local ch = love.thread.getChannel("save")
	if not ST then 
		ch:push(name)
		ch:push(data) 
		local fn = function()
			require("love.filesystem")
			local ch = love.thread.getChannel("save")
			local name = ch:pop() 
			local data = ch:pop()
			if not love.filesystem.exists("save_data/") then 
				love.filesystem.createDirectory("save_data")
			end 
			local a,b = love.filesystem.write("save_data/"..name,data)
			assert(a,b)
		end 	
		ST = love.thread.newThread(string.dump(fn))
		ST:start()
	else
		if not ST:isRunning() then
			ST = nil
		end 
	end 
	return data 
end 
function getSaves()
	local saves = {} 
	local lfs = love.filesystem
	for i,v in ipairs(lfs.getDirectoryItems("save_data")) do 
		if not string.find(v,".trollh") then 
			local t = {
				name = v, 
				mapName = "",
				playTime = {} 
			} 
			local v = Tserial.unpack(love.filesystem.read("save_data/"..v))
			local map = v.mapName or "/none"
			local s = string.find(map,"/",-string.len(map))
			t.mapName = string.sub(map,s +1)
			t.map = t.mapName
			t.cards = v.cards 
			t.mobs = v.mobs
			local h,m,s = getTime(v.playTime)
			t.playTime = {h = h,m = m,s = s}
			t.date = v.date
			table.insert(saves,t)
		end 
	end 
	return saves
end 
function saveGameShort(name)
	local cg = Gamestate.current()
	if cg.saveGame then 
		local t = cg:saveGame()
		if not playerData.children_maps then playerData.children_maps = {} end
		playerData.children_maps[name] = t 
	end 
end
function loadGameShort(name)
	if not playerData.children_maps then playerData.children_maps = {} end
	return playerData.children_maps[name] 
end
function loadGame(name,dontswitch) 
	local name = name or SaveName
	if not string.find(name,".troll")  then 
		if love.filesystem.exists("save_data/"..name) then 
			local t = Tserial.unpack(love.filesystem.read("save_data/"..name))
			local cards = t.cards 
			local mobs  = t.mobs 
			loadTime = love.timer.getTime()
			playersCards = {}
			for i,v in ipairs(cards) do 
				local a = loadCards:new(v)
				table.insert(playersCards,a)
			end 
			playersMobs = {}
			for i,v in ipairs(mobs) do
				local a = loadGhost:load(v)
				a.image:setFilter("nearest","nearest")
				table.insert(playersMobs,a)
			end 
			playerData = {
				essence = t.essence, 
				playTime = t.playTime
			}
			local mode = t.mapMode 
			Gamestate.switch(PlayMap,{map = t.mapName,gameData = t.gameData}) 
		else 
			print("Failed to Load Game:",name,"No such directory: ","save_data/"..name)
		end
	else 
		Gamestate.switch(PlayMap,{load_one = {name}})
	end 
end 
keys = {
	game = {
		{name = "Move left",f = function() return keyboardpress("a") end},
		{name = "Move right",f = function() return keyboardpress("d") end},
		{name = "Crouch",f = function() return keyboardpress("s") end},
		{name = "Move up",f = function() return keyboardpress("w") end},
		{name = "Jump",f = function() return keyboardpress(" ") end},
		{name = "Use",f = function() return keyboardpress("e") end},
	},
	battle = {
		{name = "Attack",f = function() return keyboardpress("q") end},
		{name = "Defend",f = function() return keyboardpress("w") end},
		{name = "Skill",f = function() return keyboardpress("e") end},
		{name = "Suicide",f = function() return keyboardpress("'") end},
		{name = "Retreat",f = function() return keyboardpress("rshift") end},
		{name = "End turn",f = function() return keyboardpress("return") end},
		{name = "Cards",f = function() return keyboardpress("space") end},
		{name = "Card 1",f = function() return keyboardpress("1") end},
		{name = "Card 2",f = function() return keyboardpress("2") end},
		{name = "Card 3",f = function() return keyboardpress("3") end},
		{name = "Card 4",f = function() return keyboardpress("4") end},
		{name = "Card 5",f = function() return keyboardpress("6") end},
	},
	ui = {
		{name = "Next button",f = function() return keyboardpress("w")end},
		{name = "Last button",f = function() return keyboardpress("s")end},
		{name = "Left Click",f = function() return mousepress("l",true) end},
		{name = "Right Click",f = function() return mousepress("r",true) end},
		{name = "Advance",f = function() return keyboardpress("kp5") end},
		{name = "Retreat",f = function() return keyboardpress("kp0") end},
		{name = "Escape",f = function() return keyboardpress("kp-") end},
	}
}
hiden_keys = {
	{name = "Move left",f = function() return keyboardpress("a") end},
	{name = "Move right",f = function() return keyboardpress("d") end},
	{name = "Down ",f = function() return keyboardpress("s") end},
	{name = "Up",f = function() return keyboardpress("w") end},
	{name = "return to origin",f = function() return combo(keyboardpress("rctrl") or keyboardpress("lctrl"),keyboardpress(" ")) end},
	{name = "redo",f = function() return combo(keyboardpress("rctrl") or keyboardpress("lctrl"),keyboardpress("z")) end},
	{name = "undo",f = function() return combo(keyboardpress("rctrl") or keyboardpress("lctrl"),keyboardpress("r")) end},
	{name = "midMouse",f = function() return mousepress("m") end},
	{name = "space",f = function() return keyboardpress(" ") end},
	{name = "mouseUp",f = function() return mousepress("wu") end},
	{name = "mouseDown",f = function() return mousepress("wd") end}
}
alt_keys = {
	game = {
		{name = "Move left",f = function() return keyboardpress("left") end},
		{name = "Move right",f = function() return keyboardpress("right") end},
		{name = "Crouch",f = function() return keyboardpress("down") end},
		{name = "Move up",f = function() return keyboardpress("up") end},
		{name = "Jump",f = function() return nokey(" ") end},
		{name = "Use",f = function() return nokey("e") end},
	},
	battle = {
		{name = "Attack",f = function() return nokey("a") end},
		{name = "Defend",f = function() return nokey("a") end},
		{name = "Skill",f = function() return nokey("a") end},
		{name = "Suicide",f = function() return nokey("a") end},
		{name = "Retreat",f = function() return nokey("a") end},
		{name = "End turn",f = function() return nokey("a") end},
		{name = "Cards",f = function() return nokey("a") end},
		{name = "Card 1",f = function() return nokey("a") end},
		{name = "Card 2",f = function() return nokey("a") end},
		{name = "Card 3",f = function() return nokey("a") end},
		{name = "Card 4",f = function() return nokey("a") end},
		{name = "Card 5",f = function() return nokey("a") end},
	},
	ui = {
		{name = "Next button",f = function() return nokey("w")end},
		{name = "Last button",f = function() return nokey("s")end},
		{name = "Left Click",f = function() return nokey("return",true)end},
		{name = "Right Click",f = function() return keyboardpress("return",true)end},
		{name = "Advance",f = function() return nokey("a") end},
		{name = "Retreat",f = function() return nokey("d") end},
		{name = "Escape",f = function() return nokey("d") end},
	}
}
function queryKey(exp_name,c,fn)
	local isDown = false 
	local found  = false 
	local no = 0 
	local table 
	local ui = Gamestate.current().ui
	local cl  
	if ui and ui.textInputObject then 
		cl = true 
	end 
	if not cl then 
		if not c then 
			for k,m in pairs(keys) do  
				for i,v in ipairs(m) do 
					if v.name == exp_name then 
						found = v
						isDown = v.f()  
						table = m
						no = i 
						break 
					end 
				end 
			end 
			if not found then 
				for k,m in pairs(alt_keys) do  
					for i,v in ipairs(m) do 
						if v.name == exp_name then 
							found = v
							isDown = v.f()  
							table = m 
							no = i 
							break 
						end 
					end 
				end  
			end 
		else 
			isDown,found = c:queryKey(exp_name,fn)
		end 
	end 
	return found,isDown,no,table
end 
SaveName = "settings"
scaleFactor = 1
cameraSlide = true

misc = {
	showFPS = false,
	scaleFactor = 1,
	cameraSlide = true,
	cameraSlideB = true,
	battleCamera = true,
	mouseLock = false,
	trapMouse = false,
	textSpeed = 1,
	sfvol = 55,
	bgmvol = 55,
	voicevol = 55,
	mastervol = 100,
	seenIntro = false,
}

video = {
	width = 800,
	height = 600,
	trapMouse = false,
	mouseVisible = true,
	flags = {
	fullscreentype = "normal",
	fullscreen = false,
	vsync = true,
	highdpi = false,
	display = 1,
	borderless = false,
	srgb = false,
	},
}
function loadVideoOptions(videot)
	local w = videot.width
	local h = videot.height
	love.mouse.setGrabbed(videot.trapMouse)
	love.mouse.setVisible(videot.mouseVisible)
	love.window.setMode(w,h,videot.flags)
	px =	love.graphics.getWidth()/800 
	py = love.graphics.getHeight()/600
	video = videot
end 
love.filesystem.setIdentity("Lucidity")
function saveOptions(a)
	local data = Tserial.pack(a)
	return data 
end 
function saveKeyOptions(name,keyTable,keytable2,miscTable,videoTable)
 local gtable = {}
 local aTable = saveOptions(miscTable)
 local vTable = saveOptions(videoTable)
 local tables = {}
 local tables2 = {}
 for i,v in pairs(keyTable) do 
	local a = {}
	for k,j in ipairs(v) do 
		local _,_,src,key,joystick = j.f() 
		local b = {j.name,src,key,joystick}
		table.insert(a,b)
	end 
	tables[i] = a
 end
  for i,v in pairs(keytable2) do 
	local a = {}
	for k,j in ipairs(v) do 
		local _,_,src,key,joystick = j.f() 
		local b = {j.name,src,key,joystick}
		table.insert(a,b)
	end 
	tables2[i] = a
 end
	gtable["keys"] = tables
	gtable["alt_keys"] = tables2
	gtable["misc"] = aTable
	gtable["video"] = vTable
	love.filesystem.write(name,Tserial.pack(gtable))
end
function loadKeyOptions(file,_x)
	local masterTable = Tserial.unpack(file)
	local a = masterTable["keys"]
	for _,j in pairs(a) do 
		for i,v in ipairs(j) do 
			local name = v[1]
			local src = v[2]
			local key = v[3]
			local joystick = v[4]
			local f		
			local block = (_ == "ui" and string.find(string.lower(name),"click"))
			if src == "keyboard" then 
				f = function() return keyboardpress(key,block) end 
			elseif src == "mouse" then 
				f = function() return mousepress(key,block) end 
			elseif src == "joystick" then
				f = function() return gamepadpress(joystick,key,block) end 
			elseif src == "none" then 
				f = function() return nokey() end 
			else 
				f = function() return nokey() end 
			end
			j[i] = {name = name ,f = f}
		end 
	end
	
	local d = masterTable["alt_keys"]
	for _,j in pairs(d) do 
		for i,v in ipairs(j) do 
			local name = v[1]
			local src = v[2]
			local key = v[3]
			local f		
			if src == "keyboard" then 
				f = function() return keyboardpress(key) end 
			elseif src == "mouse" then 
				f = function() return mousepress(key) end 
			elseif src == "joystick" then
				f = function() return gamepadpress(key) end 
			elseif src == "none" then 
				f = function() return nokey() end 
			else 
				f = function() return nokey() end 
			end
			j[i] = {name = name ,f = f}
		end 
	end
	local b = Tserial.unpack(masterTable["misc"])
	local v = Tserial.unpack(masterTable["video"])
	if _x then 
		loadVideoOptions(v)
	end 
	return a,d,b
end
local miscItems = {
	cameraSlide,
	scaleFactor,
	battleCamera,
}
if love.filesystem.exists(SaveName) then
	-- load the shit in 
	keys,alt_keys,misc = loadKeyOptions(love.filesystem.read(SaveName),true)
else 
	-- save shit 
	saveKeyOptions(SaveName,keys,alt_keys,miscItems,video)
end
function deepCopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end
function shallowcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end
function deepCopy2(original)
    local copy = {}
    for k, v in pairs(original) do
        -- as before, but if we find a table, make sure we copy that too
	   if type(v) == 'table' then
            v = deepCopy(v)
        end
        copy[k] = v
    end
    return copy
end
function compareTable(t1,t2,ignore_mt)
   local ty1 = type(t1)
   local ty2 = type(t2)
   if ty1 ~= ty2 then return false end
   -- non-table types can be directly compared
   if ty1 ~= 'table' and ty2 ~= 'table' then return t1 == t2 end
   -- as well as tables which have the metamethod __eq
   local mt = getmetatable(t1)
   if not ignore_mt and mt and mt.__eq then return t1 == t2 end
   for k1,v1 in pairs(t1) do
      local v2 = t2[k1]
      if v2 == nil or not compareTable(v1,v2) then return false end
   end
   for k2,v2 in pairs(t2) do
      local v1 = t1[k2]
      if v1 == nil or not compareTable(v1,v2) then return false end
   end
   return true
end 
	
function math.round(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end
function tablelenght(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end
function reverseTable(t)
    local reversedTable = {}
    local itemCount = #t
    for k, v in ipairs(t) do
        reversedTable[itemCount + 1 - k] = v
    end
    return reversedTable
end
function table.pack(...)
      return { ... }, select("#", ...)
    end
function globalKeystrokes()
	local key = love.keyboard.isDown
	local dt = love.timer.getDelta()
	if key("lalt") and key("return") then
		local w,h,fl = love.window.getMode()
		local bool = true
		if fl.fullscreen then 
			bool = false 
		end 		
		love.window.setFullscreen(bool)
	end
end
round = {}
	love.keyboard.setKeyRepeat(true)

resources = {
	outlineShader = love.graphics.newShader("s/outline.glsl"),
}
particles = {}
local _dir = "/assets/particles/"
local files = love.filesystem.getDirectoryItems(_dir)
for i,v in ipairs(files) do 
	particles[v] = love.graphics.newImage(_dir.."/"..v)
end 
function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end
function findPrecent(full,current)
	return (current/full)*100
end 
function precentOf(full,current)
	return findPrecent(full,current)
end 
function love.load()
	love.graphics.setDefaultFilter("nearest","nearest")
	phyWorld = love.physics.newWorld(0,0)
	phyWorld:setCallbacks(onContact,endContact,preSolve,postSolve)
	Collider = HC(200, on_collide)
	Collider:setCallbacks(nil, stop)
	playersCards = {}
	for i,v in ipairs(playerCards) do 
		local a = loadCards:new(v)
		table.insert(playersCards,a)
	end 
	for i,v in ipairs(playerMobs) do
		local a = loadGhost:new(v.name)
		a.image:setFilter("nearest","nearest")
		table.insert(playersMobs,a)
	end 
	current_gamestate = "none"
	Gamestate.registerEvents()
	if misc.seenIntro then 
		Gamestate.switch(mainMenu)
		current_gamestate = "menu"
	else 
		Gamestate.switch(Intro)
	end 
	
	-- Gamestate.switch(Editor)
	 -- current_gamestate = "editor"
	-- Gamestate.switch(PlayMap,{map = "maps/new"})
	
	-- Gamestate.switch(testRun)
	-- current_gamestate = "test"
	
	-- Gamestate.switch(battle)
	-- current_gamestate = "battle"
	
	-- Gamestate.switch(testBed)
	-- current_gamestate = "testBed"
	font = love.graphics.newFont(uniBody,6)
	
	screen_w = love.graphics.getWidth()
	screen_h = love.graphics.getHeight()
	if video.width > screen_w then 
		video.width = screen_w
	end 
	if video.height > screen_h then 
		video.height = screen_h
	end 
end














function on_collide(...)
	if _currentGamestate.Hcol then _currentGamestate:Hcol(...) end 
	if current_gamestate == "menu" then
		mainMenu:Hcol(...)
	elseif current_gamestate == "test" then
		testRun:HCol(...)
	end
	
end
function stop(...)
	if _currentGamestate.Hstop then _currentGamestate:Hstop(...) end 
	if current_gamestate == "menu" then
		mainMenu:Hstop(...)
	end
	if current_gamestate == "test" then
		testRun:Hstop(...)
	end
end
function love.threaderror(t, e)
    return error(e)
end
function findItem(mode,name,parentPath)
	local searchSets
	local dump_this
	local mode = string.lower(mode)
	if name == true then 
		name = "" 
		dump_this = true 
	end 
	if mode == "ghost" then 
		mode = "ghosts"
	elseif mode == "card" or mode == "cards" then
		mode = "cards"
		searchSets = true 
	end 
	local files = {} 
	local getFiles = love.filesystem.getDirectoryItems
	local exists = love.filesystem.exists
	local item = string.lower(name)
	local function addFiles(dir)
		print("Searching:"..dir)
		for i,v in ipairs(getFiles(dir)) do 
			if searchSets then 
				local ndir = dir.."/"..v
				for k,m in ipairs(getFiles(ndir)) do 
					if not string.find(m,".lua") and not string.find(m,".png")then 
						local f = {name = m,path = ndir,set = v,setPath = ndir}
						table.insert(files,f)
					end 
				end 
			else 	
				local f = {name = v,path = dir}
				table.insert(files,f)				
			end
		end
	end  
	addFiles(mode)
	local function getKids(map,fn)
		local d = map.."/children/"
		for i,v in ipairs(getFiles(d)) do 
			if fn then fn(d) end 
			getKids(d..v)
		end 
	end
	local function fromMaps(dir)
		for i,v in ipairs(getFiles(dir)) do 
			if exists(dir..v.."/"..mode) then 
				addFiles(dir..v.."/"..mode)
			end
			getKids(dir..v,function(map)
				if exists(map.."/"..mode) then 
					addFiles(map.."/"..mode)
				end
			end)
		end 
	end 
	fromMaps("maps/")
	fromMaps("inbuilt_maps/")
	local items 
	if dump_this then 
		items = {}
	end 
	for i,v in pairs(files) do 
		local name = string.lower(v.name)
		local set,setPath,path 
		path = v.path.."/"..v.name
		if searchSets then 
			set = v.set
			setPath = v.setPath
		end 
		if dump_this then 
			table.insert(items,{
				name = name,
				path = path,
				set = set,
				setPath = setPath,
			})
		else 
			if item == name then
				return path,set,setPath
			end 
		end 
	end 
	if dump_this then 
		return items 
	end 
end 
function getParents(map,tail) 
	print("PARENTS FOR:",map)
	if not tail then 
		local a,b = string.find(map,"/")
		tail = string.sub(map,1,b)
	end 
	if not map then 
		map = ""
		print("NO MAP")
	end
	local function fmap(map)
		local m
		local parents = {} 
		if string.find(map,"children/") then 
			m = string.gsub(map,tail,"")
			m = string.gsub(m,"//","/")
			local _,_1 = string.find(m,"/")
			
			local mainp = string.sub(m,1,_1-1)
				
			m = string.sub(m,_1)
			m = string.gsub(m,"/children","")
			while string.find(m,"/") do
				local _,_1 = string.find(m,".*/")
				local p = string.sub(m,_1)
				m = string.sub(m,1,_1-1)
				p = string.gsub(p,"/","")
				table.insert(parents,p)
			end
			table.insert(parents,mainp)				
		end 
		parents = table.reverse(parents)
		return parents
	end 
	local parents = {} 
	local t = fmap(map)
	for i,v in ipairs(t) do
		local m = ""
		for n =2,i do 
			m = m.."/children/"..t[n] 
		end
		m = tail..t[1].."/"..m
		m = string.gsub(m,"//","/")
		table.insert(parents,m)
	end 
	return parents 
end
local rectangle = {} 
function rectangle:new(x,y,w,h)
	local self = {
		x = x, 
		y = y,
		w = w, 
		h = h, 
		scale = {1,1},
		rot = 0,
		verts = {},
	} 
	self.points = {
		w/2,h/2,
		w/2,-h/2,
		-w/2,-h/2,
		-w/2,h/2,
	} 
	self.original = deepCopy2(self.points)
	setmetatable(self,{__index = rectangle})
	self:moveTo(x,y)
	return self 
end 

function rectangle:rotate(r)
	self.rot = r
	self:refresh()
end 
local function rotate(x,y,angle)
	local a = math.rad(angle)
	local c = math.cos(a)
	local s = math.sin(a)
	return c*x - s*y,s*x + c*y
end 
function rectangle:setDimensions(w,h)
	self.w = w 
	self.h = h
	local w,h = w/2,h/2
	self.original = {
		w,h,
		w,-h,
		-w,-h,
		-w,h,
	} 
	self:refresh()
end
function rectangle:refresh()
	if self.rot > 360 then 
		self.rot = 0 
	end 
	for i,v in ipairs(self.original) do 
		if i %2 == 0 then 
			local x,y = self.original[i-1],self.original[i]
			x,y = rotate(x,y,self.rot)
			x,y = x + self.x,y + self.y
			self.points[i-1],self.points[i] = x,y
		end 
	end 
end 
function rectangle:moveTo(x,y)
	if x and y then 
		if self.x ~= x or self.y ~= y then 
			
			self.x = x 
			self.y = y
			
			self:refresh()
		end 		
	end 
end 
function rectangle:draw()
	love.graphics.polygon("fill",self.points)
end 
local s = 40
local size = 8
sav_tween = {
	no = 0,
	timer = 0,
	rect1 = rectangle:new(0,0,50,50),
	rect2 = rectangle:new(0,0,40,40),
	transp = 255,
} 
sav_tween.rect = {
	{x = 0,y = 0},
	{x = 0,y = s},
	{x = s,y = 0},
	{x = s,y = s},
} 
local c = {x = s/2,y = s/2}
for i,v in ipairs(sav_tween.rect) do 
	v.olx = v.x 
	v.oly = v.y 
end 
function sav_tween:update(tween,complete,dt)
	if self.active then 
		-- self.timer = self.timer + dt
		-- if self.active and self.timer > 5 then 
			-- self:reset()
			-- self.active = false
			-- self.timer = 0
		-- end 
		-- for i,v in ipairs(self.rect) do 
			-- if v._comp then 
				-- self.no = self.no +  1
			-- end 
		-- end 
		-- local function tweenSquare(sq)
			-- local v = sq
			-- local no 
			-- for i,v in ipairs(self.rect) do
				-- if v == sq then 
					-- no = i
				-- end 
			-- end 
			-- v._fin = true
			-- tween:to(v,0.5,{x = c.x,y = c.y}):oncomplete(function()
				-- v._comp = true
				-- if self.rect[no +1] then 
					-- tweenSquare(self.rect[no+1])
				-- end 
			-- end)
		-- end 
		-- if not self.tweened then 
			-- local v = self.rect[1]
			-- v._fin = true
			-- tween:to(v,0.5,{x = c.x,y = c.y}):oncomplete(function()
				-- v._comp = true
				-- tweenSquare(self.rect[2])
			-- end)
			-- self.tweened = true
		-- end 
		-- if self.no == #self.rect then 
			-- local no = 0 
			-- for i,v in ipairs(self.rect) do 
				-- tween:to(v,0.5,{x = v.olx,y = v.oly}):oncomplete(function()
					-- no = no + 1
					-- v._comp = false
					-- v._fin = false
					-- if no == #self.rect then 
						-- if complete then 
							-- self:reset()
							-- self.active = false
						-- else 
							-- self.tweened = false
						-- end 
					-- end 
				-- end)
			-- end 
		-- end 
		-- self.no = 0 
			local dt = dt*2
			local r1 = self.rect1.rot + dt*90 
			self.rect1:rotate(r1)
			local r2 = -self.rect1.rot - dt*50
			self.rect2:rotate(r2)
			if self.rect1.rot > 360 then 
				self.rect1.rot = 0
				self.rect2.rot = 0 
			end 
		if complete then 
			if self.transp < 0 then
				self.active = false 
				self.transp = 255 
			else 
				self.transp = self.transp - 50*dt
			end 
		end 
	end 
end 

function sav_tween:reset()
	self.rect = {
		{x = 0,y = 0},
		{x = 0,y = s},
		{x = s,y = 0},
		{x = s,y = s},
	} 
	for i,v in ipairs(sav_tween.rect) do 
		v.olx = v.x 
		v.oly = v.y 
	end 
	self.timer = 0
	self.tweened = false
	self.transp = 255 
end 
function sav_tween:draw(x,y)
	-- setColor(colors.red)
	-- for i,v in ipairs(self.rect) do 
		-- love.graphics.rectangle("fill",(x or 0) + v.x,(y or love.graphics.getHeight() - (s+12)) + v.y,size,size)
	-- end 
	local x = x or 60
	local y = y or love.graphics.getHeight() - 50
	local rect = self.rect1
	local rect1 = self.rect2
	setColor(105,89,205,self.transp)
	rect:moveTo(x,y)
	rect:draw()
	setColor(255,215,0,self.transp)
	rect1:moveTo(x,y)
	rect1:draw()
	setColor()
end
dfs = love.graphics.newFont(uniBody_bold,12)
local drawProf = false 
Profiler = require("graphic_profiler")
prof = Profiler:new()
local globals = {}
function globals:update(dt)
	if drawProf then 
		prof:attach()
	end 
	Console:update(dt)
end
function globals:updateAfter(dt)
	if drawProf then 
		prof:dettach()
	end 
end
function globals:textinput(...)
	Console:textinput(...)
end
function globals:keypressed(key)
	if misc.console then 
		Console:listener(key,self.ui)
	end 
	if PROFILER then 
		if drawProf then 
			prof:keypressed(key)
		end 
		if key == "p" then 
			drawProf = not drawProf
		end 
	end 
end 
function globals:mousepressed(x,y,b)
	prof:mousepressed(x,y,b)
	Console:mousepressed(x,y,b)
end 
function globals:drawAfter()
	if drawProf then 
		prof:draw()
	end 
	Console:draw()
end 
local maskShader = love.graphics.newShader[[
   vec4 effect (vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
      if (Texel(texture, texture_coords).rgb == vec3(0.0)) {
         // a discarded pixel wont be applied as the stencil.
         discard;
      }
      return vec4(1.0);
   }
]]
function imagestencil(fn)
	return function()
		love.graphics.setShader(maskShader)
		fn()
		love.graphics.setShader()
	end 
end 
Gamestate.setGlobal(globals)
local msg = {
" is at times, not welcome.",
" help, get me out of this title!",
" I hope it doesn't break.",
" I run at 60 mph",
" Don't drink the kool-aid",
" You are here forever!",
" am I a game?",
" My name is <insert name here>",
" Ants are people too!",
" I live under a rock, its cosy here."
}
math.randomseed(os.time())
local rand = math.random(1,#msg)
love.window.setTitle("Lucidity - "..msg[rand])
love.mouse.setGrabbed(misc.mouseLock)