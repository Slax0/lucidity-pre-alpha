local battle = {}
local LargeFont = love.graphics.newFont(20)
local _PACKAGE = "editor"
local sa = require (_PACKAGE.. "/shapes")
local mapAdmin = require (_PACKAGE.. "/mapAdmin")
local layers = require (_PACKAGE.. "/layer")
local t = require(_PACKAGE.."/textures")

local imgMan = require(_PACKAGE.."/images")
local moduleMan = require(_PACKAGE.."/moduleAdmin")
local soundMan = require(_PACKAGE.."/soundAdmin")
local triggers = require(_PACKAGE.."/triggerAdmin")
local rand = require(_PACKAGE.."/random")
local mobCards = require "ghostcards"

local uiImages = love.graphics.newImage("battle_npc/ui.png")
local w,h = uiImages:getDimensions()
local UI = {}

local function upDown(item,range,nom,name,speed,mode)
	local fin = item.____finished
	local v = item
	local tables = {}
	local tables2 = {}
	tables[name] = nom + range
	tables2[name] = nom - range
	if not fin then
		v.____finished = 1
	elseif fin == 1 then 
		v.________tween = Timer.tween(speed or 2,v,tables,mode,function() v.____finished  = 2 end)
		v.____finished  = 0
	elseif fin == 2 then 
		v.________tweentween = Timer.tween(speed or 2,v,tables2,mode,function() v.____finished  = 1 end)
		v.____finished  = 0
	end
end
local function checkActive(team)
	local s = true
	for i,v in ipairs(team) do 
		if not v.button.permaInactive then 
			s = false
		end
	end 
	return s
end 
local function getTeamHealth(self)
	local team1h,team2h = 0,0
	for i,v in ipairs(self.team1) do
		if not v.dead then 
			team1h = team1h + v.hp
		end 
	end
	for i,v in ipairs(self.team2) do
		if not v.dead then 
			team2h = team2h + v.hp
		end 
	end
	return team1h,team2h
end 
local ui = require("menu.ui")
local uiInst = require("menu.ui")
function battle:enter(prev,info_stack) -- stack should contain: {mobs,scene name,team2Captain,OnFinish} 
	UI = {
		cap = love.graphics.newQuad(1,1,25,14,w,h),
		gradient = love.graphics.newQuad(28,1,1,14,w,h),
		endTurn = love.graphics.newQuad(0,16,104,44,w,h),
		text = love.graphics.newQuad(105,16,265,43,w,h),
		cards = newAnimation(uiImages,141,76,0.09,1,4,0,60),
		crystal = newAnimation(unpack(crystal["player"].data)),
		cardBack = love.graphics.newQuad(317,137,141,77,w,h),
		circle = {
			main	 = love.graphics.newQuad(0,137,167,183,w,h),
			buttons  =  {
				love.graphics.newQuad(167,137,140,26,w,h),
				love.graphics.newQuad(169,162,148,25,w,h),
				love.graphics.newQuad(168,187,149,27,w,h),
				love.graphics.newQuad(169,162,148,25,w,h),
				love.graphics.newQuad(167,137,140,26,w,h),
			}
			
		}, 
		}
	print("--ENTERED BATTLE MODE--")
	require "anal"
	local battleReport = require("battle_npc.battleReport")
	local sceneLoader = require("battle_npc.sceneLoader")
	local selectCards = require("Ghosts.cards.selectCards")
	local menu = require("menu.init")
	local default_setting = "blueprint"
	local cam = require "hump.camera"
	local battlenpc = require "battle_npc.init"
	
	-- local OnFinish = info_stack[4]
	-- local sceneName = info_stack[2]
	
	
	
	
	self.random = rand 
	self.scale = 1
	self.UsedKeys = {}
	self.usedCards = {} 
	
	self.font = love.graphics.newFont(uniBody,30)
	
	self.guards = {
		scale = 1,
	}
	self.bubbleTween = require("battle_npc.bubbleTween")
	self.updateStack = {}
	
	local speed = 350
	local width,height = love.graphics.getWidth(), love.graphics.getHeight()
	self.width,self.height = width,height
	self.retreatFunction = function() 
	end
	_currentGamestate = self
	self.gamestate = "playing"
	self.OldGamestate = self.gamestate
	local ed = info_stack.Editor
	local E_Mode = ed 
	local txt,func 
	print("EDITOR: ",ed)
	if ed then 
		txt = {
			"Return to Editor"
		}
		func = {
			function()
				Gamestate.switch(Editor,{map = self.map})
			end 
		}
	end 
	self.menu = menu:init(txt,func)
	self.menu.OnRelease = function()
		self.gamestate = self.OldGamestate
	end 
	
	
	self.OnFinish = info_stack.OnFinish
	
	
	local function targetSelection(self,team,func,stack,cond,target,returnCond,retFunc) 
		local stack = stack
		self._stack = stack
		local mob = stack[1]
		local positive = false
		if team == self.team1 then
			oteam = self.team2
		else
			positive = true
			oteam = self.team1
		end
		local usedUp
		local Used = false 
		local sx = mob.sx
		if not target then 
			for i,v in ipairs(oteam) do
				v.button:setInactive(true)
			end
			for i,v in ipairs(team) do
				v.button.oldOnClick = v.button.OnClick 
				v.button.oldOnFocus = v.button.OnFocus
				v.button.oldOnFocusLost = v.button.OnFocusLost
				v.button.permaDis = v.button.permaInactive 
				v.button.permaInactive = false 
				if self.activeMob == v then
					v.button.permaDis = true
				end
				local button = v.button
				local smob = v
				v.button.OnFocus = function()
					if not usedUp then 
						self.activeMob2 = v	
						local pos = false 
						for i,v in ipairs(self.team1) do 
							if v == smob then 
								pos = true 
							end 
						end 
						local width = v:getDimensions()
						if positive then
							self.activeMob2.TempStack = {
								x = v.x - (width) ,
								y = v.y,
								ix = v.x - (width),
								c = 255,
								finished = 1,
								p = pos,
							}
						else
							self.activeMob2.TempStack = {
								x = v.x + (width),
								y = v.y,
								ix = v.x + (width),
								c = 255,
								finished = 1,
								p = pos,
							}
						end
						self.activeMob2.Positive = positive
					end 
				end
				if v.button._HasFocus then 
					v.button.OnFocus()
				end 
				v.button.OnFocusLost = function() 
					if self.activeMob2 then
						self.activeMob2.TempStack = nil
						self.activeMob2 = nil
					end
				end
				local function updating(dt)
					if stack[4] or cond then
						if not mob.dead then 
							if smob.dead or smob.hp <= 0 then 
								local x = smob.level
								local exp = math.log(x/200) + 8 + (x^2/200)
								if x < 10 then 
									exp = exp + 10  
								end 
								mob:addExp(exp)
							end 
						end
						self.focusMob =	mob
						self.activeMob2 = nil
						local desx,desy = mob.bx,mob.by
						local cx,cy = mob:getPos()
						local distance = ((math.min(desx,cx) - math.max(desx,cx))*-1) + ((math.min(desy,cy) - math.max(desy,cy))*-1)
						mob.state = "run"
						mob.sx = -sx 
						mob.moving = true 
						Timer.tween(distance/speed,mob,{x = desx,y = desy},nil,function()
							mob.moving = false 
							for i,v in ipairs(oteam) do
								v.button:setInactive(false)
							end
							self.activeMob = nil
							mob.state = "idle"
							mob.sx = sx
							local c
							if positive then
								for i,v in ipairs(oteam) do
									if not c and not v.button.permaInactive then
										if v.button.OnClick then v.button.OnClick() end 
										c = true
									end
								end
							end
						end)
						for i,v in ipairs(team) do
							v.button.OnClick = v.button.oldOnClick
							v.button.OnFocus = v.button.oldOnFocus
							v.button.OnFocusLost = v.button.oldOnFocusLost
							v.button.permaInactive = v.button.permaDis
							v.button.oldOnClick = nil
							v.button.oldOnFocus = nil
							v.button.oldOnFocusLost = nil
							v.button.permaDis = nil
						end
						self.updatingMob = nil
					elseif stack[5] then 
						for i,v in ipairs(oteam) do
							v.button:setInactive(false)
						end
						self.activeMob = nil
						self.activeMob2 = nil
						mob.state = "idle"
						mob.sx = sx
						local c
						if positive then
							for i,v in ipairs(oteam) do
								if not c and not v.button.permaInactive then
									v.button.OnClick()
									c = true
								end
							end
						end
						for i,v in ipairs(team) do
							v.button.OnClick = v.button.oldOnClick
							v.button.OnFocus = v.button.oldOnFocus
							v.button.OnFocusLost = v.button.oldOnFocusLost
							v.button.permaInactive = v.button.permaDis
							v.button.oldOnClick = nil
							v.button.oldOnFocus = nil
							v.button.oldOnFocusLost = nil
							v.button.permaDis = nil
						end
						self.updatingMob = nil
					end
				end
				local nod = 0
				v.button._upd = updating
				v.button.OnClick = function()
					Used = true 
					if self.aciveMob2 then self.activeMob2.TempStack = nil end 
					self.activeMob2 = nil
					if nod < 1 then 
						usedUp = true
						self.activeMob2 = nil
						local desx,desy = smob:getPos()
						local cx,cy = mob:getPos()
						local width = smob:getDimensions()
						local width2 = mob:getDimensions()
						local desx = desx - width/2
						if positive then
							desx = desx - width2/2 - 5
						else
							mob.sx = sx 
							desx = smob:getPos() + width2 +5
						end
						local distance = ((math.min(desx,cx) - math.max(desx,cx))*-1) + ((math.min(desy,cy) - math.max(desy,cy))*-1) 
						mob.state = "run"
						mob.moving = true  
						function self._stack.fun()
							func(smob,stack)
						end
						Timer.tween(distance/speed,mob,{x = math.floor(desx),y = math.floor(desy)},nil,function()
							mob.moving = false 
							mob.state = "idle"
							func(smob,stack)
						end)
							self.focusMob = mob
							self.activeMob = nil
							mob.button.permaInactive = true
							self.updatingMob = updating
						end
						team[i].button.permaInactive = nil
						v.button:setInactive(false)
						nod = nod + 1
					end
				end
			local s = {}
			s.update = function(dt) 
				for i,v in ipairs(team) do
					v.button:setInactive(false)
				end
				if self.UsedKeys["cancel"] and not Used and self.team1Turn then
					stack[5] = true 

					self.updatingMob = nil
					self.activeMob2 = nil
					for i,v in ipairs(oteam) do
						v.button:setInactive(false)
					end
					team[1].button._upd(dt)
					mob.button.permaInactive = false 
					for i,v in ipairs(team) do
						v.button._upd = nil 
						-- v.button.OnClick = nil
						-- v.button.OnFocus = nil
						-- v.button.OnFocusLost = nil
					end
					
					for i,v in ipairs(self.updateStack) do
						if v == s then
							table.remove(self.updateStack,i)
						end
					end
				end 
			end
			table.insert(self.updateStack,s)
		else
			local smob = target
			local mob = stack[1]
			local oldSx = mob.sx
			local positive = false
			if team == self.team1 then
				oteam = self.team2
			else
				positive = true
				oteam = self.team2
			end
			local function updating(dt)
				if stack[4] or cond then
					if not mob.dead then 
						if smob.dead or smob.hp <= 0 then 
							local x = smob.level
							mob:addExp(math.log(x/200) + 8 + (x^2/200))
							print(math.log(x/200) + 8 + (x^2/200))
						end 
					end
					self.focusMob =	mob
					local desx,desy = mob.bx,mob.by
					local cx,cy = mob:getPos()
					local distance = ((math.min(desx,cx) - math.max(desx,cx))*-1) + ((math.min(desy,cy) - math.max(desy,cy))*-1)
					mob.state = "run"
					mob.sx = -oldSx
					Timer.tween(distance/speed,mob,{x = desx,y = desy},nil,function()
						mob.state = "idle"
						mob.sx = oldSx
						returnCond = true
						retFunc()
					end)
					self.updatingMob = nil
				end
			end
			local function onClick()
				
				if not mob.dead and not smob.dead then 
				local desx,desy = smob:getPos()
				local cx,cy = mob:getPos()
				local desx = desx 
				local smobW = smob:getDimensions()
				if positive then
					mob.sx = -oldSx
					desx = desx - smobW - 5
				else
					desx = desx + smobW + 5
				end
				local distance = ((math.min(desx,cx) - math.max(desx,cx))*-1) + ((math.min(desy,cy) - math.max(desy,cy))*-1) 
				mob.state = "run"
				Timer.tween(distance/speed,mob,{x = desx,y = desy},nil,function()
					mob.sx = oldSx
					mob.state = "idle"
					func(smob,stack)
				end)
					self.focusMob = mob
					self.updatingMob = updating
				end
			end
			onClick()
		end
	end
	-- menu:setParams(mstack,mstack1)
	self.ui = ui:init()
	
	local _w,_h = 30,30
	local _b = self.ui:addButton(nil,_w/2 + 5,love.graphics.getHeight()-_h/2,_w,_h)
	_b.dontDraw = true 
	_b.OnDraw = function()
		local x,y = _b.colision:bbox()
		love.graphics.draw(icons["none"],x,y)
	end
	_b.OnClick = function()
		self.Tool = "?"
		
	end 
	
	self.name = "Test Bed"
	local ui = self.ui
	local stack = {}

	self.conditions = {}
	local win = function()
		local victory 
		for i,v in ipairs(self.team1) do
			if v.dead then 
				victory = self.team2
			end
		end
		if not victory then
			for i,v in ipairs(sefl.team2) do 
				if v.dead then 
					victory = self.team2
				end 
			end
		end
		if victory then 
			self.pauseEverything()
		end
	end
	-- return info stack
	-- self.scene = sceneLoader(info_stack[2])
	stack["team1"] = {}
	stack["team2"] = {} -- info_stack[1]
	local enemyMobs = info_stack[1] 
	self.__Player = info_stack[2]
	self.TeamCaptain1 = info_stack[2]
	self.TeamCaptain2 = info_stack["captain2"]
	
	self.map = info_stack["map"]
	print("MAP: ",self.map)
	eCurrentMap = self.map
	
	-- loading all of the modules for loading the map:
	local textures,textureManager,makeTexture = t[1],t[2],t[3]
	
	
	local shapeCol = function(dt,a,b)
	
	end 
	local shapeStop = function(...)

	end 
	local shapeCol2 = function(...)
		self:HCol(...)
	end 
	local shapeStop2 = function(...)
		self:HStop(...)
	end 
	self._G = {}
	self.mode = "battle"
	
	self.moduleMan = moduleMan
	
	
	self.shapeCollider = HC(200,shapeCol,shapeStop)
	self.UiColider = HC(200,shapeCol2,shapeStop2)
	
	self.shapeUi = uiInst:init(self.UiColider)
	
	self.textureMapper = textures
	
	self.triggerAdmin = triggers:new(self)
	
	self.imageAdmin = imgMan:new(self,self.shapeCollider)

	self.moduleAdmin = moduleMan:new(self,nil,self.shapeCollider,self.phyWorld)
	self.soundAdmin = soundMan:new(self,self.shapeCollider)
	self.shapeAdmin = sa:new(self,self.shapeCollider,nil,nil,self.phyWorld)
	

	self.textureManager = textureManager:new(self) 
	self.textureMapper = textures
	
	self.layers = layers:new(self)
	self.realView = true 
	
	local rangex = 100*px
	local rangey = 100*py
	
	self.ActiveShapes = {}
	self.active_modules = {}
	self.priority = {}
	
	

	-- end 
	
	local team1 = info_stack.team1 
	local playerMobs = team1 or playersMobs
	
	self.__Player.state = "idle" 
	for i,v in ipairs(playerMobs) do
		if not team1 then 
			local a = battlenpc:new(v)
			table.insert(stack.team1,a)
		else
			local mob = v 
			mob = loadGhost:new(mob.name,nil,nil,mob.dir)
			mob.image:setFilter("nearest","nearest")
			mob:getExpNeeded(mob.level)
			mob.info.exp = math.random(0,mob:getExpNeeded())
			local a = battlenpc:new(mob)
			table.insert(stack.team1,a)
		end 
	end 
	for i,v in ipairs(enemyMobs) do 
		local mob = v 
		mob = loadGhost:new(mob.name,nil,nil,mob.dir)
		mob.image:setFilter("nearest","nearest")
		mob:getExpNeeded(mob.level)
		mob.info.exp = math.random(0,mob:getExpNeeded())
		local a = battlenpc:new(mob)
		table.insert(stack.team2,a)
	end 
	
	
	if #enemyMobs == 0 then 
		for i = 1,2 do 
			local mob = loadGhost:new("slime")
			local a = battlenpc:new(mob)
			table.insert(stack.team2,a)
		end 
	end 
	
	local oldTeam1 = deepCopy2(stack["team1"])
	local oldTeam2 = deepCopy2(stack["team2"])
	
	local team1 = stack["team1"]
	local team2 = stack["team2"]
	
	self._Stack = stack 
	self.team1 = self._Stack.team1
	self.team2 = self._Stack.team2
	
	
	mapAdmin.load(self,self.map)
	if self.OnLoad then 
		self.OnLoad()
	end 
	
	
	local findMaxLvL = function(team)
		local lvl = 0 
		local maxi
		for i,v in ipairs(team) do
			if v.level > lvl then
				lvl = v.level
				maxi = i
			end 
		end
		return lvl,maxi
	end
	
	
	local _,i = findMaxLvL(self.team2)
	
	self.team2Captain = self.team2[i]
	
	self.team1h,self.team2h = getTeamHealth(self)
	self.OldTeam1h = self.team1h
	self.OldTeam2h = self.team2h
	self.ATeam1h = self.team1h
	self.ATeam2h = self.team2h
	self.bubbleTween:new(self.width - 20,10)
	
	
	self.cam = cam(self.width/2,self.height/2)
	self.x = 0
	self.y = 0
	local cardsOn = false
	local team1W = 0
	local team1H = 0
	local team2W = 0
	local team2H = 0
	
	-- FIND TOTAL WIDTH INSTEAD OF MAX WIDTH!!!!!
	for i,v in ipairs(stack["team1"]) do
		team1W = team1W + v.width
		team1H = team1H + v.height
	end
	
	for i,v in ipairs(stack["team2"]) do
		team2W = team2W + v.width
		team2H = team2H + v.height
	end
	
	local TeamSeperation = 150
	local nw,nh = 300,height - 400
	local ox,oy = (1920/2) - TeamSeperation,nh
	
	local avalW = 200
	local avalH = 150
	
	local prcW = team1W/1
	local prcH = team1H/1
	
	local team1C = Collider:addRectangle(ox - avalW,oy,avalW,avalH)
	
	self.team1Ox,self.team1Oy = ox - avalW,oy
	
	local prcW = team1W/1
	local prcH = team1H/1
	
	self.damage = {}
	self.team1Turn = true
	
	
	local ox,oy = (1920/2) + TeamSeperation,nh
	
	self.team2Ox,self.team2Oy = ox + avalW,oy
	
	local prcW = team2W/1
	local prcH = team2H/1
	for i,v in ipairs(self.team1) do 
		v:setAttackTable(self.damage)
	end 
	for i,v in ipairs(self.team2) do
		v:setAttackTable(self.damage)
	end
	
	-- MAKE QUADRATIC EQUATION MAX VERTEX = {ix,iy}
	-- self.ui:addButton(nil,width/2,height/2,100,100)
	
	local overheadText = self.ui:addButton(nil,width/2, 93,300,40)
	overheadText.dontDrawC = true 
	overheadText.dontDrawB = true
	overheadText.OnDrawB = function()
		local x,y = overheadText.colision:center()
		local nw,nh = overheadText.width,overheadText.height
		local q = UI.text
		local _,_,w,h = q:getViewport() 
		local sx,sy = nw/w,nh/h
		love.graphics.draw(uiImages,q,x,y,overheadText.rotation,sx,sy,w/2,h/2)
	end 
	overheadText:setRotation(0)
	overheadText.OnUpdate = function()
		local oldText = overheadText.oldText
			if not oldText or oldText ~= overheadText.text then
				local text = overheadText.text
				local w = ui.font:getWidth(text)
				local h = ui.font:getHeight(text)
				overheadText:setSize(w + 20,h + 20)
				overheadText.oldText = overheadText.text
			end
		local time = 3
		local rot = 10
		local rot2 = -10
		if not overheadText.tb then
			overheadText.tb = {rotation = rot - 1}
		end
		local tb  = overheadText.tb
		local rotatMade = overheadText.rotatMade
		if math.floor(overheadText.tb.rotation) > rot - 2 then
			Timer.cancel(overheadText.tween or {})
			overheadText.tween = Timer.tween(time,tb,{rotation = rot2})
		elseif math.floor(overheadText.tb.rotation) < rot2 + 2 then
			Timer.cancel(overheadText.tween or {})
			overheadText.tween = Timer.tween(time,tb,{rotation = rot})
		end
		overheadText.rotation = math.rad(tb.rotation)
	end
	love.graphics.setDefaultFilter("linear","linear")
	overheadText:setText("Welcome to the '"..self.name.."' battlefield")
	overheadText.OnClick = function()
		overheadText:setText("Less talking more raiding!")
	end 
	overheadText.OnRightClick = function()
		overheadText:setInactive(false)
		overheadText.dontDraw = true 
		overheadText.zmap = 1
	end 
	self.overheadText = overheadText
	local ox = 210
	local oy = height - 185
	local nw = 140
	local nh = 20
	-- local y = x^2 + 3
	local function basicOn()
		for i in ipairs(self.updateButtons) do
			self.updateButtons[i]:setInactive(true)
			self.updateButtons[i].ondeactivate()
		end	
	end
	local buttons = {}
	self.updateButtons = buttons
	local button = ui:addButton(nil,ox,oy,nw,nh)
	button:setText("Attack")
	button.OnClick = function()
		if button.tween then 
			Timer.cancel(button.tween)
		end 
		local oldMob = self.activeMob
		basicOn()

			button.OnUpdate2 = function()
				if self.UsedKeys["cancel"] and self.team1Turn then
					for i,v in ipairs(self.updateButtons) do 
						v:setInactive(false)
						v.onactivate()
					end
				end
			end
		local function func(target,stack) -- "stack: {self, team1, team2}"
			local mob = stack[1]
			local damage = mob.atk
			local delay = 0.1
			local x,y = mob:getPos()
			local frames = 3
			local v = target
			local x2,y2 = target:getPos()
			local time = 0 
			mob.state = "attack"
			local anim
			for i,v in ipairs(mob.states) do
				if v.name == "attack" then
					anim = v.anim
					anim.position = 1
				end
			end
			mob.OnUpdate = function(dt)
			time = time + dt
			if time > 15 then 
				local dmg,modif = target:applyDamage(damage,"attack")
				stack[4] = true
				mob.state = "idle"
				mob.OnUpdate = nil
			end 
			if anim.position >= #anim.frames then 	
					local dmg,modif = target:applyDamage(damage,"attack")
					stack[4] = true
					mob.state = "idle"
					mob.OnUpdate = nil
				end 
			end
		end
		targetSelection(self,self.team2,func,{self.activeMob,self.team1,self.team2,condition})		
	end

	
		if self.CameraBox then self.shapeCollider:remove(self.CameraBox) end 
	self.CameraBox = self.shapeCollider:addRectangle(0,0,self.width,self.height)
	
	button.onFocus = function()
		overheadText:setText("The best defence is a strong offence")
	end
	table.insert(buttons,button)
	local button = ui:addButton(nil,ox,oy,nw,nh)
	button:setText("Defend")
	button.OnClick = function() 
		basicOn()
		local mob = self.activeMob
		local button = mob.button
		self.activeMob.guard = true 
		overheadText:setText("Defence is key to persistence")
		button.permaInactive = true
		button.OnClick(a,b,c,d,true)
		self.aciveMob = nil 
		self.aciveMob2 = nil 
		self.focusMob = nil 
		button:setInactive(true)
	end
	button.onFocus = function() 
		overheadText:setText("Boost the defence by 25%!")
	end

	table.insert(buttons,button)
	local button = ui:addButton(nil,ox,oy,nw,nh)
	button:setText("Skill")
	button.OnClick = function()
		local tables = self.activeMob:getSkills()
		if #tables > 0 then 
			basicOn()
			local wa = 0 
			local font = ui.font
			for i,v in ipairs(tables) do 
				local w = font:getWidth(v.name)
				if icons[v.type] then
					local img = icons[v.type]
					local w2 = img:getWidth()
					w = w + (w2*2 + 5)
				end
				if w > wa then 
					wa = w 
				end 
			end
			if wa < font:getWidth("Select Skill") then 
				wa = font:getWidth("Select Skill")
			end 
			local no = #tables
			local w = wa + 10
			local h = no*(50) + 10
			local frame = self.ui:addFrame("Select Skill",width/2 ,height/2 - h/2,w,h)
			
			frame.color = basicFrameColor
			frame:setHeader("Select Skill")
			local oldMob = self.activeMob
			frame.OnUpdate = function()
				if self.activeMob ~= oldMob or (self.UsedKeys["cancel"] and self.team1Turn) then
					frame:remove()
					for i,v in ipairs(buttons) do 
						v:setInactive(false)
						v.onactivate()
					end
				end
			end
			for i,v in ipairs(buttons) do 
				v:setInactive(true)
				v.ondeactivate()
			end
			for i,v in ipairs(tables) do
				local x = 5
				local y = 30*i
				if i > 3 then
					x = 150
					y = 30*(i-2)
				else
					x = 5
				end
				local ha = font:getHeight()
				local a = ui:addButton(frame,x,30*i,wa,25)
				ui:addTooltip(a,v.desc or "Author needs to add description")
				if oldMob.mp < v.cost then 
					a.inactive = true 
				end 
				a:setText(v.name)
				a._cost = v.cost
				local stack = {}
				local reqSelect
				local selecting 
				a.OnClick = function()
					local am = self.activeMob
					local fn = function(...)
						oldMob.mp = oldMob.mp - v.cost
						v.func(...)
					end 
					frame:setInactive(true)
					frame.dontDraw = true 
					stack = {self.activeMob,self.team1,self.team2,condition,a._cost}
					if v.target == "fTeam" then
						fn(self.team1,stack)
					elseif v.target == "team" then
						fn(self.team2,stack)
					elseif v.target == "self" then
						fn(self.activeMob,stack)
					elseif v.target == "friend" then
						reqSelect = "friend"
						overheadText:setText("Pick an ally!")
						targetSelection(self,self.team1,fn,{self.activeMob,self.team1,self.team2,condition})
						selecting = true 
					elseif v.target == "foe" then
						reqSelect = "foe"
						overheadText:setText("Pick a foe!")
						targetSelection(self,self.team2,fn,{self.activeMob,self.team1,self.team2,condition})
						selecting = true
					end
					for i,v in ipairs(self.team1) do
						v.button:setInactive(true)
					end
					self.activeMob.button:setInactive(true)
				end
				a.OnUpdate = function()
					if selecting then 
						frame:remove()
					end 
					if stack[4] then
						for i,v in ipairs(self.team1) do
							v.button:setInactive(false)
						end
						if frame then frame:remove() end 
						self.activeMob.button.permaInactive = true
						self.activeMob.button.OnClick()
						self.activeMob = nil
						self.focusMob = nil
						if v.target == "fTeam" or v.target == "team" or v.target == "self" then 
							self.activeMob = nil 
						end
					end
				end
				if icons[v.type] then
					local img = icons[v.type]
					local w = img:getWidth()
					local h = img:getHeight()
					if img and type(img) ~= "table" then 
						a.OnDraw = function()
							if img then love.graphics.draw(img,a.x - a.width/2 + w,a.y,0,1,1,w,h/2) end 
						end
					end 
				end
			end
		end 
	end
	table.insert(buttons,button)
	local button = ui:addButton(nil,ox,oy,nw,nh)
	button:setText("Sacrifice")
	button.OnClick = function() 
		basicOn()
		overheadText:setText("Pick a prey worthy of your predator.")
		local function func(target,stack) -- "stack: {self, team1, team2}"
			local mob = stack[1]
			local damage = mob.atk + mob.hp
			local delay = 0.1
			local x,y = mob:getPos()
			local frames = 3
			local v = target
			local x2,y2 = target:getPos()
			local time = 0 
			mob.state = "attack"
			local anim
			for i,v in ipairs(mob.states) do
				if v.name == "attack" then
					anim = v.anim
					anim.position = 1
				end
			end
			anim:reset()
			mob.OnUpdate = function(dt) 
				time = time + dt
				if time > 15 then 
					local dmg,modif = target:applyDamage(damage,"attack")
					stack[4] = true
					mob.state = "idle"
					mob.OnUpdate = nil
				end 
			if anim.position == #anim.frames then 	
					local dmg,modif = target:applyDamage(damage)
					mob.state = "idle"
					mob.OnUpdate = nil
					mob.suicide = true
					mob.OnDeath = function()
						stack[5] = true
						overheadText:setText("Ashes to ashes, dust to dust.")
						self.focusMob = nil
					end 
					mob.hp = 0 
				end 
			end
		end
		targetSelection(self,self.team2,func,{self.activeMob,self.team1,self.team2,condition})	 
	end
	button.onFocus = function()
		overheadText:setText("Lets play our lives away!")
	end
	table.insert(buttons,button)
	local button = ui:addButton(nil,ox,oy,nw,nh)
	button:setText("Retreat")
	button.OnClick = function()
		basicOn()
		self.pauseEverything()
		local luck  = 0 
		local luck2 = 0 
		for i,v in ipairs(self.team1) do 
			luck = luck + v.luck
		end 
		for i,v in ipairs(self.team2) do 
			luck2 = luck2 + v.luck
		end 
		if luck > luck2 then 
			self.retreat = 1
		else 
			local range = findDistance(luck,luck2)/2
			local chance = love.math.random(0,math.ceil(range))
			if chance == 0 then 
				overheadText:setText("A wise choice.")
				self.retreat = 1
			else
				self.retreat = 0
			end
		end 
		self.activeMob.button.permaInactive = true
	end
	button.onFocus = function()
		overheadText:setText("A good tactician knows when to retreat.")
	end 
	table.insert(buttons,button)
	local img = love.graphics.newImage("assets/tiles/test.png")
	for i,v in ipairs(buttons) do
		local cx = -5*(i - 3)^2 
		if i > 3 then
			col = Collider:addPolygon(0,0, nw,0, nw + cx,nh, 0,nh)
		else
			col = Collider:addPolygon(0,0, nw + cx,0, nw,nh,0,nh)
		end
		local x = ox + (cx)
		local y = oy + (nh*i) + 5*i
		v.OnFocus = function()
			if v.tween then
				Timer.cancel(v.tween)
			end
			if v.onFocus then
				v.onFocus()
			end
			buttons[i].tween = Timer.tween(0.3,v,{x = x + 20,y = y},'in-quad')
		end
		v.OnFocusLost = function()
			if v.onFocusLost then
				v.onFocusLost()
			end
			if v.tween then
				Timer.cancel(v.tween)
			end
			buttons[i].tween = Timer.tween(0.3,v,{x = x,y = y},'out-quad')
		end
		v.onactivate = function() 
			if v.tween then
				Timer.cancel(v.tween)
			end
			buttons[i].tween = Timer.tween(0.7,v,{x = x,y = y},'out-quad')
			v:setInactive(false)
		end
		v.ondeactivate = function()
			if v.tween then
				Timer.cancel(v.tween)
			end
			buttons[i].tween = Timer.tween(0.7,v,{x = x - 80,y = y},'out-quad')
			v:setInactive(true)
		end
		buttons[i]:setShape(col)
		buttons[i]:setTexture(img)
		buttons[i]:moveTo(x,y)
		v.dontDrawB = true 
		v.dontDrawC = true 
		v.dontDrawD = true
		v.mesh = false
		v.textOX = 15
		buttons[i].OnDrawB = function()
			local no = i
			local r = 0  
			if i > 3 then
				no = no -2
				r = math.rad(180)
			end 
			local x,y = buttons[i].colision:bbox()
			local nw,nh = findDimensions(v.colision)
			local q = UI.circle.buttons[i] 
			local _,_,w,h = q:getViewport()  
			local sx,sy = (nw)/(w-(4)),(nh)/(h-(4))
			if i == 1 then 
				sx = (nw)/(w-(10))
			elseif i == 2 then 
				sx = (nw)/(w-(5))
			end 
			if i > 3 then
				if i == 5 then 
					sx = (nw)/(w-(10))
				elseif i == 4 then 
					sx = (nw)/(w-(5))
				end 
				y = y + h*sy
				sx = -sx 
			end 
			love.graphics.draw(uiImages,q,x,y,r,sx,sy)
		end 
	end
	self._CircleButtons = buttons
	
	self.retreatFunction = function()
		if self.retreat == 1 then 
			self.OnFinish(self)
		elseif self.aciveMob then 
			if self.activeMob == self.team1[#self.team1] then 
				local cx,cy = self.activeMob.x,self.activeMob.y
				local desx,desy = self.activeMob.bx,self.activeMob.by
				local distance = ((math.min(desx,cx) - math.max(desx,cx))*-1) + ((math.min(desy,cy) - math.max(desy,cy))*-1)
				self.activeMob.button.tween = Timer.tween(distance/speed,self.activeMob,{x = desx,y = desy},nil,function()
					self.activeMob.button.tween = nil
					self.activeMob = nil
				end)
			end
		end
	end
	
	local function CardQualifier(card,stack)
		local ab 
		local msg
		if card.qualifier then 
			stack = {self.team1,self.team2}
			ab,msg = card.qualifier(stack)
		else 
			ab = true 
		end
		return ab,msg
	end 
	
	
	
	-- DOING THE CARD BUISNESS HERE 
	self.deck = {}
	self.varUpdateStack = {}
	local points1 = {}
	local points2 = {}
	local newPoints = {}
	local gap = 10
	local angle = 15
	local cardson = ui:addButton(nil,width - 70,height - 38,140,90)
	self.cardsOn = cardson
	local nw,nh = 300*px,height - 400
	local avalW = 150
	local avalH = 150



	self.pickedCards = {
		playersCards[1],
		playersCards[2],
		playersCards[3],
		playersCards[4],
		playersCards[5],
	}
	
	local function CardTargetSelection(card,button)
		local sally = self.ui:addButton(nil,9000,9000,1,1)
		self.CardFunctions = {}
		local targeta
		local stack 
		local sallysOnUpdate
		local sallysOnDraw
		local drawIndicator = true
		local target = card.target
		local point = Collider:addPoint(0,0)		
		local LockDown = false 
		local w,h = 50,5
		local TargetRect = {
			{x = 10 +w,y = 0,w = w,h = h},
			{x = -60 +w,y = 0,w = w,h = h},
			{x = 0,y = 10 +w,w = h,h = w},
			{x = 0,y =-60 +w,w = h,h = w },
		} 
		local a
		local function sallyDrawIndicator(x,y)
			local x,y = x,y 
			local lg = love.graphics
			if not x or not y then 
				x,y = self.ui.mxb:center()
			end 
			local startx,starty = button.x,button.y
			local finalx,finaly = x,y
			local offset = 10 
			local w = (finalx -offset/2) - startx
			local h = (finaly-offset/2) - starty
			local midx,midy = startx + w/2,starty - h/2
			local points = {startx,starty,midx,midy,finalx - offset/2,finaly-offset/2}
			local curve = love.math.newBezierCurve(points)
			local newPoints = curve:render()
			setColor(colors.red)
			lg.setLineWidth(10)
			lg.line(newPoints)
			lg.setLineWidth(5)
			lg.circle("line",finalx,finaly,10)
			local delay = 0.5
			for i,v in ipairs(TargetRect) do
				if i == 1 then
					upDown(v,20,v.x,"x",delay)
				elseif i == 2 then 
					upDown(v,-20,v.x,"x",delay)
				elseif i == 3 then 
					upDown(v,20,v.y,"y",delay)
				elseif i == 4 then 
					upDown(v,-20,v.y,"y",delay)
				end 
				lg.rectangle("fill",finalx- v.x,finaly - v.y,v.w,v.h)
			end 
			lg.setLineWidth(1)
			setColor()
		end 
		local function cleanUp(card,stack)
			card.lockOut = true
			local squares = {}
			local size = 5
			local v = card.button
			local squareNoW = (card.width*1.1)/size 
			local squareNoH = (card.height*1.1)/size
			
			local posx,posy = v.x - card.width/2 - size,v.y - card.height/2 - size
			local no = 0
			for x = 0,squareNoW do
				for y = 0,squareNoH do 
					local square = {x = x * size + posx,y = y * size + posy,size = size}
					table.insert(squares,square)
					Flux.to(square,0,{y = y*size - 600})
					:ease("backout")
					:delay(((1*(squareNoH - y) + (x^1.2)) + math.random(0,5))/40)
					:oncomplete(function() 
						no = no + 1
						if no >= #squares-1 then 
							for i,v in ipairs(self.pickedCards) do
								if v == card then 
									button:remove()
									table.remove(self.pickedCards,i)
									v:remove()
									table.insert(self.usedCards,card)
									stack[5] = true 
								end 
							end
						end
					end)
				end 				
			end
			card.Stencil = function()
				for i,v in ipairs(squares) do
					love.graphics.rectangle("fill",v.x,v.y,v.size,v.size)
				end 
			end 
			self.SelectingArea = false
			sallysOnUpdate = nil 
			sally.OnDraw = nil 
			Collider:remove(point)
		end 
		sally:setZmap(1)
		local function retPoints(points)
			local pants = {}
				for i,v in ipairs(points) do 
					if i == 9 or i == 10 then
						
					else
						pants[i] = points[i]
					end 					
				end 
			return pants
		end 
		
		local function trigger(target,area1,area2)
			stack = {self.team1,self.team2}
			card.onUse(target,stack,self.CardFunctions,area1,area2)
			cleanUp(card,stack)
		end 
		local rectangles1 = {}
		local rectangles2 = {}
		
		local team1Finished = true 
		local team2Finished = false 
		local function lock(bool)
			team1Finished = true 
			team2Finished = true
			self.paused = bool
			for i,v in ipairs(self.varUpdateStack) do
				v.inactive = bool
				if v ~= button and v.tween then
					Timer.cancel(v.tween)
					v.tween = Timer.tween(0.4,v,{color = color,x = v.ox, y = v.oy, rotation =  v.Ar, scale = 0.9},'out-sine')
				end 
				cardson:setInactive(bool)
			end
			for i,v in ipairs(self.team1) do 
				if v.button.tween then 
					Timer.cancel(v.button.tween)
				end 
				self.activeMob = nil
				v.button.OnClick(_,_,_,_,true)
				v.button:setInactive(bool)
			end
			for i,v in ipairs(self.team2) do 
				v.button:setInactive(bool)
			end
			self._endTurn:setInactive(bool)
		end
		
		
		local SalX,SalY
		lock(true)
		if button.tween then
			Timer.cancel(button.tween)
		end 
		button.tween = Timer.tween(0.2,button,{x = (button.width*1.1)/2 + 5,y = (button.height*1.1)/2 + 5,rotation = 0, scale = 1})
		
		if target == "team" then 
			function sallysOnUpdate(dt)
				local inTeam1 = self._InTeam1
				local inTeam2 = self._InTeam2
				
				self.SelectingArea = true 
				if self.UsedKeys["do"] then 
					if inTeam1 then 
						trigger(self.team1,self.SAreas.team1)
					elseif inTeam2 then 
						trigger(self.team2,self.SAreas.team2)
					end 
				end 
			end 
			function sallysOnDraw()
			end 
		elseif target == "none" then 
			drawIndicator = false 
			local areas = self._Areas
			local x1,y1 = areas.team1:bbox()
			local w,h = findDimensions(areas.team1)
			
			local x2,y2 = areas.team2:bbox()
			local w2,h2 = findDimensions(areas.team2)
			
			trigger(nil,self.SAreas.team1,self.SAreas.team2)
		elseif target == "single" then 
			local function takeAction(team)
				local x,y,mob
				for i,v in ipairs(team) do
					if self.shapeUi.mxb:collidesWith(v.button.colision) then
						x,y = self:camPos(v.button.colision:center()) 
						mob = v
					end 
				end
				return x,y,mob
			end 
			sallysOnUpdate = function()
				local x,y,mob = takeAction(self.team1)
				if not mob then 
					x,y,mob = takeAction(self.team2)
				end
				self.focusMob = mob
				SalX = x 
				SalY = y
				if mob and self.UsedKeys["do"] then 
					trigger(mob)
				end 
			end 
		end
		function sally.OnUpdate(dt) 
			for i,v in ipairs(self.team1) do 
				v.button.inactive = true 
			end 
			
			if sallysOnUpdate then sallysOnUpdate(dt) end
				if not team1Finished then 
					newGroup:update(dt)
				else 
					newGroup = Flux.group()
					for i,v in ipairs(rectangles1) do 
						v.tween = nil
						v.size = math.max(5*px,5*px)
					end 
				end 
			if stack then stack[4] = stack[4] or stack[3] end 
			if stack and stack[4] and stack[5] then 
				
				lock(false)
				sally:remove()
				self.CardFunctions = nil
				
			end 
			if self.UsedKeys["cancel"] and not card.lockOut  and self.team1Turn then 
				LockDown = true
				self.SelectingArea = false
				button.dontMindMe = true 
				lock(false)
				sally:remove()
				self.CardFunctions = nil 
				sally = nil
				button:setInactive(true)
				button.tween = Timer.tween(0.8,button,{color = color,x = button.ox, y = button.oy, rotation =  button.Ar, scale = 0.9},'out-sine',function()
					button.dontMindMe = false 
					button:setInactive(false)
					button.Timer = 0 	
					button.HasFocus = false
					button.OldFocus = true
				end)
			end 
		end 
		function sally.OnDraw()
			if sallysOnDraw then sallysOnDraw() end
			if drawIndicator then sallyDrawIndicator(SalX,SalY) end 
		end 
	end 	

	

	
	local q = UI.cards
	self.cardSelection = cardson
	cardson:setZmap(50)
	local initS = 0.9
	local aW,aH = 180 - 2,270 - 5
	dW,dH = aW*initS,aH*initS
	cardson.OnClick = function(a,b,c,d)
		self.ui.aciveObject = nil
		local ox,oy = cardson.x,(cardson.y + 200/2) - cardson.height/2 + 10
		if not self.cardsDrawn and not d then
			q:play()
			cardson.dontDraw = true
			local numOfCards = #self.pickedCards
			local anum = numOfCards + 1
			local half = math.floor(anum/2)
			local ix,iy = width/2 - (90*half) ,height - 120
			
			cardson.inactive = true
			cardsOn = true
			
			
			for i,v in pairs(self.pickedCards) do
		-- button:setImage(deck[i].image)
			local x,y,r 
			x = ix + 90*i
			if i < half then
				y = iy + (gap*(anum-i))
				r = -(angle*(half - i))
				if i == 1 then
					y = y + 28
				end
			elseif i == half then
				y = iy + (gap*i)
				r = 0
			else
				r = angle*(i -half)
				y = iy + (gap*i)
				if i == numOfCards then
					y = y + 28
				end
			end
			local button = ui:addButton(nil,ox,oy,dW,dH)
			
			-- button.OnClick = function()
				-- button:remove()
				-- table.remove(pickedCards,i)
			-- end 
			button.oy = y
			button.ox = x
			button.Ar = math.rad(r)
			button.dragEnabled = true 
			button:setRotation(0)
			button:setText(i)
			button:setZmap(i + 1)
			button:setInactive(true)
			button.scale = initS
			
			v.button = button
			button.HasFocus = false 
			button.OldFocus = false 
			button.Timer = 0 
			local limit = iy - 250
			local ReadyUp
			button.OnDraw = function()
				local b = button 
				local canv = v:retCanvas()
				if ReadyUp then
					-- DRaw the ready up animation for card
				end 
				
			if v.Stencil then 
				love.graphics.setStencil(v.Stencil)
			end 
				love.graphics.draw(canv,b.x,b.y,b.rotation,b.scale,b.scale,aW/2,aH/2)
				love.graphics.setStencil()
			end 
			local dragging = false
			button.OnClick = function()
				dragging = true 
			end 
			function button.OnRelease()
				dragging = false
				if button.Activate or ReadyUp and not button.dontMindMe then 	
					 button.Activate  = false
					local a,msg = CardQualifier(v)
					if a then 
						CardTargetSelection(v,button)
					else 
						if not msg then 
							msg = "You cannot use this card!"
						end
						overheadText:setText(msg)
					end 
				end 
			end  
			button.OnUpdate = function(dt)
				local w,h = love.graphics.getDimensions()
				if button.x < (aW*px) and button.y < (aH*py) then
					ReadyUp = true 
				else 
					ReadyUp = false 
				end 
				v:update(dt/2)
				local timer = button.Timer			
				local hasFocus = button.HasFocus
				local oldFocus = button.OldFocus 
				if hasFocus ~= oldFocus and not button.inactive and not dragging then 
					button.Timer = timer + dt
						if oldFocus and timer > 1 then 
							if button.tween then
								Timer.cancel(button.tween)
							end
							-- button.color = color
							button.tween = Timer.tween(0.8,button,{color = color,x = button.ox, y = button.oy, rotation = math.rad(r), scale = 0.9},'out-sine',function()
								button.dontMindMe = false 
								button:setInactive(false)
							end)
							button.OldFocus = hasFocus 
							button.Timer = 0 
						elseif not oldFocus and timer > 0 then 
							if button.tween then
								Timer.cancel(button.tween)
							end
							button.tween = Timer.tween(1,button,{color = {255,255,255},x = button.ox, y = limit,rotation = 0, scale = 1.1},'in-sine')
							button.OldFocus = hasFocus 
							button.Timer = 0 
						end
					else 
					button.Timer = 0
				end 
			end
	
			local color = {math.random(1,255),math.random(1,255),math.random(1,255)}
			button.color = {color[1],color[2],color[3]}
			Timer.tween(1,button,{y = y,x = x,rotation = math.rad(r)},'in-sine',function()
				button.inactive = false
				cardson.inactive = false
			end)
			table.insert(self.varUpdateStack,button)
			button.OnFocus = function()
				button.HasFocus = true 
			end
			button.dontDraw = true
			button.OnFocusLost = function()
				button.HasFocus = false 
			end
			self.cardsDrawn = true 
		end
		
		elseif not inProgress and self.pickedCards then
			cardson.inactive = true
			local num = 0 
			local expected = #self.pickedCards
			for i,v in ipairs(self.varUpdateStack) do
				v.inactive = true
				if v.tween then
					Timer.cancel(v.tween)
				end
				self.varUpdateStack[i].tween = Timer.tween(1,v,{x = ox,y = oy,rotation = 0,scale = 0.9}, 'out-sine',
				function()

					num = num + 1
					if num == expected then
						q.direction = -1
						q:play()
						function q.OnUpdate()
							if q.position == 1 then 
								cardson.dontDraw  = false
								for n,b in ipairs(self.varUpdateStack) do 
									b:remove()
								end 
								if not d then
									cardson.inactive = false
								else
									cardson.inactive = true
								end
								cardsOn = false
								for i,v in ipairs(buttons) do
									if v.onactivate and not d then
										v.onactivate()
									end
								end
								num = 0
								inProgress = false
								self.varUpdateStack = {}
								self.cardsDrawn = false
								q.direction = 1 
								q:setMode("once")
								q:stop()
								q.OnUpdate = nil 
							end 
						end 
					end
				end)
			end
		end
		if cardsOn then
			for i,v in ipairs(buttons) do
				if v.ondeactivate then
					v.ondeactivate()
				end
			end
		end
	end
	
	q:setMode("once")
	q:stop()
	cardson.dontDrawC = true 
	cardson.dontDrawB = true
	cardson.dontDrawD = true 
	cardson.OnUpdate = function(dt)
		q:update(dt)
		if #self.pickedCards == 0 then 
			q.direction = -1
			q:play()
		end 
		if self.pickedCards and #self.pickedCards == 0 then 
			cardson.inactive = true
			local num = 0 
			local expected = #self.pickedCards
			for i,v in ipairs(self.varUpdateStack) do
				for n,b in ipairs(self.varUpdateStack) do 
					b:remove()
				end 
				if not d then
					cardson.inactive = false
				else
					cardson.inactive = true
				end
				cardsOn = false
				for n,m in ipairs(buttons) do
					if m.onactivate and not d then
						m.onactivate()
					end
				end
				inProgress = false
				self.varUpdateStack = {}
				self.cardsDrawn = false
			end
		end 
	end
	cardson.OnDraw = function()
		local x,y = cardson.colision:bbox()
		local nw,nh = findDimensions(cardson.colision)
		local _,_,w,h = q:getViewport()  
		local sx,sy = (nw)/(w),(nh)/(h)
		q:draw(x,y,0,sx,sy)
	end 
	
	
	
	
	
	
	local endTurn = ui:addButton(nil,width/2, 45,100,40)
	self._endTurn = endTurn
	endTurn.transp = 0
	endTurn:setText("End turn")
	endTurn.OnClick = function() 
		local function onChangeTurn(team)
			for i,v in ipairs(team) do 
				v:changeTurn()
			end 
		end 
		if cardsOn then
			cardson.OnClick(a,b,c,true)
			cardson:setInactive(true)
		end
		for i,v in ipairs(buttons) do
			v:setInactive(true)
			v.ondeactivate()
		end
		for i,v in ipairs(self.team1) do
			if self.activeMob == v then
				v.button.OnClick(_,_,_,true)
			end
			v.button:setInactive(true)
		end
		for i,v in ipairs(self.team2) do
			if self.activeMob == v then
				v.button.OnClick(_,_,_,true)
			end
			v.button:setInactive(true)
		end
		onChangeTurn(self.team1)
		cardson:setInactive(true)
		self.team2Turn = true
		self.team1Turn = false
		self.activeMob = nil
		self.focusMob = nil
		self.activeMob2 = nil
		for i,v in ipairs(self.team1) do
			v.permaInactive = true
		end
		self.turn = self.turn + 1
		local function checkDead(team)
			for i,v in ipairs(team) do 
				if v.dead and v.hp <=0 then 
					table.remove(team,i)
				end 
			end 
			return team 
		end
		local ai = battlenpc:newAi(self.team1,self.team2)
		ai:figure(checkDead(self.team1),checkDead(self.team2))
		local condition
		local function clearEndTurn() 
			for i,v in ipairs(self.team1) do
				if self.activeMob == v then
					v.button.OnClick(_,_,_,true)
				end
				v.button:setInactive(false)
			end
			for i,v in ipairs(self.team2) do
				if self.activeMob == v then
					v.button.OnClick(_,_,_,true)
				end
				v.button:setInactive(false)
			end
			onChangeTurn(self.team2)
			
			self.team2Turn = false
			self.team1Turn = true
			self.activeMob = nil
			self.focusMob = nil
			self.activeMob2 = nil
			endTurn:setInactive(false)
			for i,v in ipairs(self.team1) do
				v.button.permaInactive = nil
			end
			for i,v in ipairs(self.team2) do
				v.button.permaInactive = nil
			end
			if cardson then 
				cardson:setInactive(false)
			end
		end
		local function getAlive(team)
			local a = 0
			for i,v in ipairs(team) do 
				if not v.dead and v.hp > 0 then 
					a = a + 1
				end 
			end
			return a 
		end 
		local finished = 0 
		local alive = getAlive(self.team1)
		local function goAttack(clamp)
			local clamp = true
			local curAlive = getAlive(self.team1)
			local done 
			for i,v in ipairs(self.team2) do
				if not v.dead and not v.button.permaInactive and clamp then 
					local action = v.action
					if not action then 
						return false
					end 
					local mode = action.mode or "guard"
					print("Attack Mode:",mode)
					local target = action.target
					v.button.permaInactive = true
					local deadtarget = false 
					if target and target.dead and target.hp <=0 then 
						deadtarget = true 
					end 
					if target and deadtarget then 
						v.button.permaInactive = false
						v.action = nil 
						local team1 = {}
						local team2 = {} 
						ai:figure(checkDead(self.team1),checkDead(self.team2))
						goAttack()
					else 
						if type(mode) == "string" then
							if mode == "attack" then
								local function func(target,stack) -- "stack: {self, team1, team2}"
									if not target then 
										for i,v in ipairs(self.team1) do 
											if not v.permaInactive then 
												target = v
											end 
										end 
									end 
									local mob = stack[1]
									local damage = mob.atk
									local delay = 0.1
									local x,y = mob:getPos()
									local frames = 3
									local v = target
									local x2,y2 = target:getPos()
									local timer = 0 
									mob.state = "attack"
									
									local anim
									for i,v in ipairs(mob.states) do
										if v.name == "attack" then
											anim = v.anim
											anim.position = 1
										end
									end
									mob.OnUpdate = function(dt) 
										timer = timer + dt
										if timer > 15 then 
											stack[4] = true
											mob.state = "idle"
											mob.OnUpdate = nil
										end
									if anim.position == #anim.frames then 	
											local dmg,modif = target:applyDamage(damage,"attack")
											stack[4] = true
											mob.state = "idle"
											mob.OnUpdate = nil
										end 
									end
								end						
								if target.dead or target.hp <= 0 then 
									v.button.permaInactive = false
									ai:figure(checkDead(self.team1),checkDead(self.team2))
									goAttack()
								else 
								targetSelection(self,self.team1,func,{v,self.team1,self.team2,condition},nil,target,clamp,function()
									v.action = nil
									finished = finished + 1
									goAttack() end)	
								end
							elseif mode == "guard" then
								v.guard = true
								v.action = nil
								finished = finished + 1
								goAttack()
							end
						else
							local func = mode.func
							if heal then
								targetSelection(self,self.team2,func,{v,self.team1,self.team2,condition},nil,target,clamp,function()
								v.action = nil
								finished = finished + 1
								goAttack() end)
							else
								targetSelection(self,self.team1,func,{v,self.team1,self.team2,condition},nil,target,clamp,function()
								v.action = nil	
								finished = finished + 1
								goAttack() 
								end)
							end
						end
					end
					clamp = nil
				end
			end
		end
		self.checkIfMoving = function() 
			local moving 
			local clamp = true 
			for i,v in ipairs(self.team1) do
				if v.moving then 
					clamp = false
				end
			end
			if clamp then 
				goAttack()
				self.checkIfMoving = function()
					local expected = 0 
					for i,v in ipairs(self.team2) do 
						if not v.dead and v.hp ~= 0 and not v.stunned then 
							expected = expected + 1
						end 
					end 
					if finished == expected then
						clearEndTurn()
						self.checkIfMoving = nil
						for i,v in ipairs(self.team1) do 
							v.guard = false
						end
					end
				end 
			end
		end
		endTurn:setInactive(true)
	end
		endTurn.dontDrawB = true 
		endTurn.dontDrawC = true 
		endTurn.dontDrawD = true
		endTurn.mesh = false
		endTurn.OnUpdate = function()
			local lock = false 
			for i,v in ipairs(self.team1) do 
				if not v.dead and v.state ~= "idle" then 
					lock = true 
				end 
			end 
			if not lock then 
				for i,v in ipairs(self.team2) do 
					if not v.dead and  v.state ~= "idle" then 
						lock = true 
					end 
				end 
			end 
			if self.team1Turn  then 
				endTurn:setInactive(lock)
			end 
		end
		endTurn.OnDrawB = function()
			if checkActive(self.team1) then 
				upDown(endTurn,125,125,"transp",1)
				local r,g,b = unpack(colors.yellow)
				setColor(r,g,b,endTurn.transp)
				local olw = love.graphics.getLineWidth()
				love.graphics.setLineWidth(10)
					endTurn.colision:draw("line")
				love.graphics.setLineWidth(olw)
			end 
			setColor()
			local no = i
			local r = 0  
			local x,y = endTurn.colision:center()
			local nw,nh = endTurn.width,endTurn.height
			local q = UI.endTurn 
			local _,_,w,h = q:getViewport()  
			local sx,sy = nw/w,nh/h
			love.graphics.draw(uiImages,q,x,y,r,sx,sy,w/2,h/2)
		end
	self.turn = 1
	self.team1Turn = true
	local circle = ui:addCostum(100,love.graphics.getHeight() - 110)
	circle.zmap = 100
	circle.color = {155,155,155}
	circle.colision = Collider:addCircle(circle.x,circle.y,80)
	circle.colision.source = circle
	table.insert(ui.colision,circle.colision)
	local stencil
	local canvas 
	local width2,height2 = 200,200
	circle.OnDraw = function() 
		love.graphics.setColor(255,255,255)
		if stencil and canvas then
			love.graphics.setStencil(stencil)
			love.graphics.draw(canvas,(circle.x) - width2/2,circle.y - height2/2)
			love.graphics.setStencil()
		end
		
		local x,y = circle.colision:bbox()
		local nw,nh = findDimensions(circle.colision)
		local q = UI.circle.main
		local _,_,w,h = q:getViewport() 
		local h = h -24
		local w = w -6
		local sx,sy = nw/w,nh/h
		love.graphics.draw(uiImages,q,x-3,y-3,0,sx,sy)
		UI.crystal:draw((x-7) + w/2,(y-3) + 142)
	end
	
	circle.OnUpdate = function(dt) 
		UI.crystal:update(dt)
		limitLag(dt,function()
			if self.activeMob or self.focusMob then
				local v = self.activeMob or self.focusMob 
				if circle.oldMob or 0 ~= v then
					local b = v.button
					canvas = love.graphics.newCanvas(width2,height2)
					canvas:renderTo(function()
						local x,y = self:camPos(b.x,b.y)
						if x - v.width > v.width/2 and y - v.height > v.height/2 then 
							love.graphics.draw(self.mainCanvas,0,0,0,1,1,(x) - height2/2,y - width2/2)
						end 
					end)
					stencil = function()
						love.graphics.circle("fill",circle.x ,circle.y,70)
					end
					circle.oldMob = v 
				end
			else
				canvas = nil
				stencil = nil
			end
		end)
	end
	
	self.pauseEverything = function() 
		self.paused = true 
		if cardson then
			cardson.OnClick(a,b,c,true)
			cardson:setInactive(true)
		end
		for i,v in ipairs(buttons) do
			v:setInactive(true)
			v.ondeactivate()
		end
		for i,v in ipairs(self.team1) do
			v:getButton():setInactive(true)
		end
		for i,v in ipairs(self.team2) do
			v:getButton():setInactive(true)
		end
		endTurn:setInactive(true)
		cardson:setInactive(true)
	end
	self.resumeEverything = function()
		self.paused = false
		for i,v in ipairs(self.team1) do
			v.button:setInactive(false)
		end
		for i,v in ipairs(self.team2) do
			v.button:setInactive(false)
		end
		endTurn:setInactive(false)
		cardson:setInactive(false)
	end
	
	
	self.gamestate = "playing"
	self.mainCanvas = love.graphics.newCanvas(width + rangex*2,height)
	
	
	
	local sTeamPolygon = {
		points = {0,0,0,height,width,0},
		x = x,
		y = y,
	}
	local oTeamPolygon = {
		points = {width,height, 0, height, width,0},
		x = x,
		y = y,
	}
		local w = math.sqrt(width^2 + height^2)
		local h = 10
	local loadRectangle = {
		width = w,
		height = h,
		width2 = w,
		x = width/2 ,
		y = height/2 ,
	}
	local no = 0
	local oldW = love.graphics.getLineWidth()
	local began
	self.pauseEverything()
	local points1 = {
		x = 0,
		y = height,
	}
	local points2 = {
		x = width,
		y = 0
	}
	local alpha = {
		a = 255
	}
	self.begining = {
		update = function(dt)
			self.team2Captain.image:setFilter("nearest","nearest")
			if began and no == 0 then 
				no = no + 1
				Timer.tween(1.5,alpha,{a = 0},"out-expo",function() 
					self.resumeEverything()
					self.begining = nil
					if self.team2Captain.hp then 
						self.team2Captain.Pause = false
					end 
				end)
			end
		end,
		draw = function()
			local reqWidth,reqHeight = self.width/2,self.height
			
			local olFont = love.graphics.getFont()
			local font2 = love.graphics.setNewFont(131)
			local w,h = font2:getWidth("VS"),font2:getHeight()
			local a = alpha.a
			local r,g,b = unpack(colors.red)
			love.graphics.setStencil(function() love.graphics.polygon("fill",unpack(sTeamPolygon.points)) end)
			love.graphics.setColor(r,g,b,a)
			love.graphics.polygon("fill",unpack(sTeamPolygon.points))
			
			love.graphics.setColor(255,255,255,a)
			local Player = self.__Player
			local player_w = Player.width 
			local player_h = Player.height 
			local sx = reqWidth/player_w
			local sy = reqHeight/player_h
			local playerAnimation_idle
			for i,v in ipairs(Player.states) do
				if v.anim.__index ~= animationMeta then 
					v.anim.__index = animationMeta
				end 
				if v.name == "idle" then 
					playerAnimation_idle = v.anim 
					v.anim:seek(1)
				end 
			end 
			playerAnimation_idle:draw(0,0,0,sx,sy)
			
			love.graphics.setStencil()
			local r,g,b = unpack(colors.blue)
			love.graphics.setColor(r,g,b,a)
			love.graphics.polygon("fill",unpack(oTeamPolygon.points))
			love.graphics.setColor(255,255,255,a)
			
			love.graphics.setStencil(function() love.graphics.polygon("fill",unpack(oTeamPolygon.points)) end)
			if self.team2Captain.hp then
				local mob = self.team2Captain
				local anim 
				for i,v in ipairs(mob.states) do
					if v.name == "idle" then
						anim = v.anim
					end
				end
				local sx = reqWidth/mob.width
				local sy = reqHeight/mob.height
				mob.Pause = true
				anim:seek(mob.stillFrame or 1)
				anim:draw(self.width/2,self.height/2,0,sx,sy)
			else
				local sx = reqWidth/self.team2Captain.w
				local sy = reqHeight/self.team2Captain.h
			
				self.team2Captain.anim.idle:draw(self.width,self.height/2,0,-sx,sy)
			end
			love.graphics.setStencil()
			local sx = reqWidth/player_w
			local sy = reqHeight/player_h
			
			
			if not began then 
				local x,y = points1.x,points1.y
				local x2,y2 = points2.x,points2.y
				love.graphics.setLineWidth(16)
				love.graphics.line(x,y,x2,y2)
				love.graphics.setLineWidth(oldW)
				love.graphics.print("VS",width/2 - w/2,height/2 - h/2)
			end
			love.graphics.setFont(olFont)
			
		end,
	}
	Timer.tween(2,points1,{x = width/2,y = height/2},nil,function() 
		began = true 
	end)
	Timer.tween(2,points2,{x = width/2,y = height/2},nil,function() 
		began = true
	end)
	local firstTime = true 
	local noCard
	local placeholder  = {a = 0,a2 = 0,a3 = 0}
	local screenBlack = false
	local clamp1 
	local resolutionDraw = false
	local fDone = false 
	local delay = 2

	
	self.team1.style = "player"
	self.team2.style = "basic"
	self.CrystalRange = 8
	for i,v in ipairs(self.team1) do 
		local var = crystal[self.team1.style]
		local w,h = var.w,var.h
		local width,height = v:getDimensions()
		v.CrystalPosX = math.ceil(v.x)
		v.CrystalPosY  = math.ceil(v.y - height/2 - w - self.CrystalRange)
	end
	for i,v in ipairs(self.team2) do 
		local var = crystal[self.team2.style]
		local w,h = var.w,var.h
		local width,height = v:getDimensions()
		v.CrystalPosX = math.ceil(v.x)
		v.CrystalPosY  = math.ceil(v.y - height/2 - w - self.CrystalRange)
	end
	
	
	
	
	
	local function lockAll(bool,pInactive)
		if pInactive then 
			for i,v in ipairs(pInactive) do 
				v.permaInactive = bool
			end 
		end 
		local kap = {}
		for i,v in ipairs(self.ui.colision) do 
			v.source.permaInactive = bool
			table.insert(kap,v.source)
		end
		return kap
	end 
	
	if not info_stack.pickedCards then 
		local a = lockAll(true)
		selectCards:new(self.ui,self.width/2,self.height/2,(800 - 10),(600 - 50),function(cards)
			lockAll(false,a)
			self.pickedCards = cards
		end)
	else 
		self.pickedCards = info_stack.pickedCards
	end 
	local firstTime = false 
	self.OnVictory = function()
		if not firstTime then 
			firstTime = true
			Timer.add(3,function()
				local mode 
				if self.loser == self.team1 then 
					mode = true
				end 
				self.ui:clear(true)
				self.battleReport = battleReport:new(self.ui,self.width/2,self.height/2,self.usedCards,oldTeam1,self.team1,oldTeam2,self.team2,mode,function()
					self.OnFinish(self,mode)
				end,E_Mode)
				self.disableMenu = true
				self.gamestate = "endGame"
			end)
		end
	end 

	self.keyControler = keyControl:new("battle")
	self.ui_keyControler = keyControl:new("ui") 	
end
function battle:updateCamera(dt)
	local rangex = 100*px
	local rangey = 100*py
		if not self.loser and not self.begining and misc.cameraSlideB then 
			local mx,my = self:worldPos(love.mouse.getPosition())
			local mx,my = mx,-my
			local x,y = -(self.x - self.width/2),-(self.y - self.height/2)
			local _,ay = self.SAreas.ground:bbox()
			local ax,ay = (1920/2),-ay
			local speed = 2
			local hdist = findDistance(ax,mx)
			local vdist = findDistance(ay,my)
			local rx = 0 
			local ry = 0 
			if ax < mx then 
				rx = rx - math.floor(hdist/speed)*(dt)
			elseif ax > mx then 
				rx =  rx +  math.floor(hdist/speed)*(dt)
			end
			
			if ay < my then 
				ry = ry  +  math.floor(vdist/speed)*(dt)
			elseif ay > my then 
				ry =  ry -  math.floor(vdist/speed)*(dt)
			end
			self.x = self.x + rx
			self.y = self.y + ry 
			local x,y = -(self.x - self.width/2),-(self.y - self.height/2)
			
			if x > math.floor(ax + rangex) then 
				self.x = (math.floor(-(ax + rangex) + self.width/2))
			elseif x < math.floor(ax  - rangex) then 
				self.x = (math.floor(-(ax - rangex) + self.width/2))
			end 
			local oy = self.y -self.height/2 
			local mod = 0 
			if love.graphics.getHeight() < 800 then 
				mod = 80
			end 
			if oy > (ay + rangey - mod) then 
				self.y = (ay + rangey - mod) + self.height/2
			elseif oy < (ay - (rangey/2 + mod)) then 
				self.y = (ay - (rangey/2 + mod)) + self.height/2
			end 

		else 
			local mod = 0
			if love.graphics.getHeight() <= 800 then 
				mod = 135
			end 
			local _,ay = self.SAreas.ground:bbox()
			self.x = -(1920/2 - self.width/2) 
			self.y = -(ay - self.height/2 + mod) 
		end
	
	
	
	-- alt 
	local x,y = love.mouse.getPosition()
	self.mx,self.my = self:worldPos(x,y)
	
	self.shapeUi:setMousePos(self.mx,self.my)
end 
function battle:update(dt)
	if self.gamestate == "playing" then
		local function scanDead(team)
			for i,v in ipairs(team) do 
				if v.dead then 
					v:getButton().permaDis = true 
					v:getButton().permaInactive = true
					if self.aciveMob == v then 
						self.aciveMob = nil
					end 
				end 
			end 
		end
		self.CameraBox:moveTo(-self.x + self.width/2,-self.y + self.height/2)
		self:updateCamera(dt)
		love.graphics.setDefaultFilter("nearest","nearest")
		
		scanDead(self.team1)
		
		if self.ui.activeObject then
			self.shapeUi.activeObject = nil
		end
		
		
		
		self.keyControler:attach(dt)
		self.ui_keyControler:attach(dt)
		
		self:navigation(self.keyControler,self.ui_keyControler)
		Flux.update(dt)
			if self.Team1Outline then 
				self.Team1Outline:update(dt)
			end 
		if self.begining then 
			self.begining.update(dt)
		end
		if self.checkIfMoving then self.checkIfMoving() end
		if not self.paused and not self.activeMob and not self.focusMob and self.team1Turn then
			local clamp 
			for i,v in ipairs(self.team1) do
				if not v.dead and not v.button.permaInactive and not clamp and v.state == "idle" then
					clamp = true
					v.button.OnClick()
				end
			end	
		end
		if self.damage then
			for i in ipairs(self.damage) do
				if not self.damage[i].tween then
					local y = self.damage[i].y
					local x = self.damage[i].x
					self.damage[i].alpha = 255
					self.damage[i].tween = Timer.tween(2,self.damage[i],{y = y - 90,sx = 2,sy = 2,alpha = 0},nil,function()
						for v,k in ipairs(self.damage) do
							if k == self.damage[i] then
								table.remove(self.damage,v)
							end
						end
					end)
				end
			end
		end
 
		if self._oldStack ~= self._stack then 
			self._timer2 = 0 
			self._oldStack = self._stack
		elseif self._stack then 
			self._timer2 = self._timer2 + dt
			if self._timer2 > 10 and not self._stack[4] then 
				if self._stack.fun then self._stack.fun() end 
				self._stack[4] = true 
			elseif self._stack[4] then 
			end 
		end 
		for i,v in ipairs(self.updateStack) do
			v:update(dt)
		end
		for i,v in ipairs(self.varUpdateStack) do
			v:update(dt)
		end
		if self.updatingMob then
			self.updatingMob(dt)
		end
		for i,v in ipairs(self.updateButtons) do
			if not self.activeMob then
				self.updateButtons[i]:setInactive(true)
				self.updateButtons[i].ondeactivate()
			end
			v:update(dt)
		end
			Timer.update(dt)
		-- self.cam:lookAt(self.x,self.y)
		
		local function VictoryCondition(team,onwin) 
			local win = true 
			for i,v in ipairs(team) do 
				if not v.dead and not v.retreated then
					win = false 
				end 
			end 
			for i,v in ipairs(self.team1) do 
				if v.levelingUp then 
					win = false 
				end 
			end 
			if win then 
				onwin()
			end 
		end 
			if not self.team1 then 
				return 
			end 
		VictoryCondition(self.team1,function() self.loser = self.team1 self.OnVictory() end)
		VictoryCondition(self.team2,function() self.loser = self.team2 self.OnVictory() end)
		if self.activeMob then
		elseif self.infoFrame then
			self.infoFrame:remove()
			self.infoFrame = nil
		end
		
			self.team1h,self.team2h = getTeamHealth(self)
			if self.team1h ~= self.OldTeam1h or self.team2h ~= self.OldTeam2h then 
				Flux.to(self,3,{ATeam1h = self.team1h,ATeam2h = self.team2h}):ease("quadin")
				self.OldTeam1h = self.team1h
				self.OldTeam2h = self.team2h
			end 
			local team = self.team1
			if not self.team1Turn then
				team = self.team2
			end
			
			for i,v in pairs(team) do
				if v.button and  not v.button.permaInactive and not v.button.permaDis and v ~= self.activeMob then
					upDown(v,self.CrystalRange,v.CrystalPosY,"CrystalPosY",1)
				end
			end
			crystal[team.style].anim:update(dt)
			
			
		
		
		
		self.UiColider:update(dt)
		
		self.shapeUi:update(dt)
		
		self.layers:update()
		Collider:update(dt)
		self.shapeCollider:update(dt)
		
		self.all_active = {} 
		self.ActiveShapes = {}
		self.active_modules = {} 
		
		
		mapAdmin.parseHead(self)
		for shape in pairs(self.CameraBox:neighbors()) do
			if shape:collidesWith(self.CameraBox) then 
				mapAdmin.parseSingle(self,shape,dt)
			end
		end
		self:render()
		self.ui:update(dt)
		
		
		self.keyControler:detach()
		self.ui_keyControler:detach()

	elseif self.gamestate ~= "menu" then 
		local _,ay = self.SAreas.ground:bbox()
		self.x = -(1920/2 - self.width/2) 
		self.y = -(ay - self.height/2) 
	end
	if self.gamestate == "endGame" then 
		if self.battleReport then self.battleReport:update(dt) end
		self.keyControler:attach(dt)
		self.ui_keyControler:attach(dt)
		
		self:navigation(self.keyControler,self.ui_keyControler)
		
		self.keyControler:detach(dt)
		self.ui_keyControler:detach(dt)
		Collider:update(dt)
		self.ui:update(dt)
	end 
	if self.gamestate == "menu" then 
		self.menu:update(dt)
	else 
		self.OldGamestate = self.gamestate
	end
end
function battle:render()
	local lg = love.graphics
	self.mainCanvas:clear()
	self.mainCanvas:renderTo(function()
			lg.push()
			lg.scale(self.scale or 1)
			lg.translate(self.x,self.y)
			self.CameraBox:draw("fill")
			local width,height = love.graphics.getWidth(), love.graphics.getHeight()

			
			
			mapAdmin.draw_visible(self)
			
			if self.Team1Outline then 
				self.Team1Outline:draw(dt)
			end 
			if self._team1Points then 
				self._team1Points:draw("fill")
			end 
			if self._team2Points then 
				self._team2Points:draw("fill")
			end 
			
			if self.activeMob2 and self.activeMob2.TempStack then
				-- DRAW THE INDICATOR
				local w,h = icons.indicator_red:getDimensions()
				local a = self.activeMob2
				local s = a.TempStack
				local v = s
				local sx = 1 
				if s.p then 
					sx = -1 
				end 
				local variation = 10
				if s.p then 
					variation = -variation
				end 
				local mode = nil
				if not v.visible then 
					Timer.tween(0.6,v,{c = 255},"in-quad")
					v.visible = true
				end
				if v.finished == 1 then
					v.tween = Timer.tween(0.2,v,{x = v.x + variation},mode,function() v.finished = 2 end)
					v.finished = 0
				elseif v.finished == 2 then 
					v.tween = Timer.tween(0.2,v,{x = v.ix},mode,function() v.finished = 1 end)
					v.finished = 0
				end
				love.graphics.setColor(255,255,255,v.c)
				love.graphics.draw(icons.indicator_red,s.x,s.y,0,sx,1,w/2,h/2)
				love.graphics.setColor(colors.white)
			end
			if self.activeMob and not self.activeMob.dead then
				local img = icons.indicator
				local w = img:getWidth()
				local h = img:getHeight()
				local v = self.activeMob
				local time = 0.8
				local rot = v.y - (v.height*math.abs(v.sy)) + 8
				local rot2 = v.y - (v.height*math.abs(v.sy)) - 15
				if not self.activeStack or  self.activeStack.oldMob ~= self.activeMob then
					self.activeStack = {
						y = rot
					}
					self.activeStack.oldMob = v
				end
				local stack = self.activeStack
				if not stack.tb then
					stack.tb = {y = stack.y}
				end
				local tb  = stack.tb
				local rotatMade = stack.rotatMade
				if math.floor(tb.y) > rot - 2 then
					Timer.cancel(stack.tween or {})
					stack.tween = Timer.tween(time,tb,{y = rot2})
				elseif math.floor(tb.y) < rot2 + 2 then
					Timer.cancel(stack.tween or {})
					stack.tween = Timer.tween(time,tb,{y = rot})
				end
				stack.y = tb.y
				love.graphics.draw(img,v.x,stack.y,0,1,1,w/2,h/2)
			else
				self.activeStack = nil
			end

			-- Draw the crystals.
			
			local team = self.team1
			if not self.team1Turn then
				team = self.team2
			end
			
			local style = team.style
			for i,v in ipairs(team) do
				if not v.dead and not v.button.permaInactive and not v.button.permaDis and v ~= self.activeMob then
					i = i + 1
					local var2 = v
					local var = crystal[style]
					local x,y = var2.x,var2.CrystalPosY
					local w,h = var.w,var.h
					var.anim:draw(math.ceil(x),math.ceil(y),0,1,1,w/2,h/2)
				end
			end
			
			--
			
			
			-- Drawing Damage 
			
			if self.damage then
				for i,v in ipairs(self.damage) do 
					local r,g,b
					local fps = love.timer.getFPS()
					local heal,restore = v.heal,restore
					if not heal or restore then 
						local degree = v.mobHp/v.dmg
						if degree < 1 then 
							r,g,b = unpack(colors.red)
						elseif degree < 10/v.mobHp then 
							r,g,b = unpack(colors.darkRed)
						elseif degree < 100/v.mobHp then
							r,g,b = unpack(colors.yellow)
						elseif degree < 200/v.mobHp then 
							r,g,b = 104,49,87
						else 
							r,g,b = 0,0,0
						end 
					elseif heal then 
						r,g,b = unpack(colors.red)
					elseif restore then 
						r,g,b = unpack(colors.blue)
					end 
					local olf = love.graphics.getFont()
					love.graphics.setNewFont(uniBody,16*((px + py)/2))
					love.graphics.setColor(r,g,b,self.damage[i].alpha)
					local w = font:getWidth(self.damage[i].dmg)
					local h = font:getHeight()
					love.graphics.print(tostring(self.damage[i].dmg),self.damage[i].x,self.damage[i].y,0,v.sx,v.sy,w/2,h/2)
					if self.damage[i].modif then
						local modif = tostring(self.damage[i].modif)
						local font = love.graphics.setNewFont(uniBody,10)
						local w = font:getWidth(self.damage[i].modif)
						local r,g,b,a = love.graphics.getColor()
						r,g,b = unpack(mColors[modif] or {}) 
						setColor(r,g,b,a)
						love.graphics.print(modif,self.damage[i].x,self.damage[i].y + 40,0.1,v.sx,v.sy,w/2,h/2)
						setColor()
					end
					love.graphics.setFont(olf)
				end
				love.graphics.setColor(255,255,255)
			end
			
			
			self.shapeUi:draw()
			self.shapeUi:getmxb():draw("fill")
			lg.pop()
		end)
end 
function battle:draw()
	if self.gamestate == "playing" then
		love.graphics.draw(self.mainCanvas,0,0)
			local i = self.cardSelection
			local x,y = i.colision:bbox()
			local q = UI.cardBack
			local nw,nh = findDimensions(i.colision)
			local _,_,w,h = q:getViewport()  
			local sx,sy = (nw)/(w),(nh)/(h)
			love.graphics.draw(uiImages,q,x,y,0,sx,sy)
			
			local total = self.ATeam1h + self.ATeam2h		
			local widthx = love.graphics.getWidth() - 20
			local pr = widthx/total
			local width1 = pr*self.ATeam1h
			love.graphics.setColor(0,19,127)
			love.graphics.rectangle("fill",10,10,width1,10)
			
			local width3 = pr*(self.ATeam2h - self.team2h)
			
			local width2 = pr*self.team1h
			love.graphics.setColor(255,0,0)
			love.graphics.rectangle("fill",10,10,width2,10)

			local width5 = pr*self.ATeam2h
			
			love.graphics.setColor(127,0,0)
			love.graphics.rectangle("fill",width1 + 10,10,width5,10)
			
			local width4 = pr*self.team2h
			love.graphics.setColor(0,0,255)

			love.graphics.rectangle("fill",width1 + 10 + width3,10,width4,10)
		
			love.graphics.setColor(255,255,255)
			love.graphics.setStencil(function() 
				love.graphics.setColor(255,0,0)
				love.graphics.rectangle("fill",10,10,width2,10)

				love.graphics.setColor(0,0,255)
				love.graphics.rectangle("fill",width1 + 10,10,width4,10)
			end)
			self.bubbleTween:draw(10,10)
			love.graphics.setStencil()
			local q = UI.gradient
			local _,_,w,h = q:getViewport()
			local nw = widthx
			local nh = 12 
			local sx,sy = nw/w,nh/h
			love.graphics.draw(uiImages,q,10,9,0,sx,sy)
			local nh = 12
			local _,_,w,h = UI.cap:getViewport()
			local sx,sy = 0.8,nh/h
			love.graphics.draw(uiImages,UI.cap,10 -(w*sx)/2,9,0,sx,sy)
			love.graphics.draw(uiImages,UI.cap,widthx + (w*sx),h*sy + 9,math.rad(180),sx,sy)
			
			
						-- RETREAT
			if self.retreat then 
				local height,width = love.graphics.getDimensions()
				self.pauseEverything()
				local ret = self.retreat
				local text
				if ret == 1 then 
					text = "Retreating"
				elseif ret == 0 then 
					text = "Failed!"
				end
				if not self.retOldFont then
					self.retOldFont = love.graphics.getFont()
					self.retStack = {x = 0,y = height/2 - 150,width = 2}
					local no = 0
					local delay = 2
					local a = Timer.tween(1,self.retStack,{x = width - (width-100)/2,y = height/2 - 150,width = width-100},"out-quart",function()
						if no < 1 then 
							local w = self.retStack.width                          
							Timer.tween(2,self.retStack,{x = width + (width-100)/2,width = 20},"in-quart",function()
								if ret == 0 then 
									self.resumeEverything()
								end 
								self.retreatFunction()
								self.retreat = nil
								self.retOldFont = nil
								self.retStack = nil
								self.activeMob = nil
							end)
						end
						no = no + 1
					end)
				end
				local ofont = love.graphics.getFont()
				local font = LargeFont
				local  w = font:getWidth(text)
				local h = font:getHeight()
				local x,y,width = self.retStack.x,self.retStack.y,self.retStack.width
				if ret == 1 then 
					love.graphics.setColor(colors.yellow)
				else
					love.graphics.setColor(colors.darkRed)
				end
				love.graphics.setFont(LargeFont)
				love.graphics.print(text,self.retStack.x - w/2,self.retStack.y -h - 5)
				love.graphics.setColor(colors.red)
				love.graphics.rectangle("fill",x - width/2,y - 5,width,5)
				love.graphics.setColor(255,255,255)
				love.graphics.setFont(ofont)
			end
			
			
		self.ui:draw()
		if self.begining then 
			self.begining.draw()
		end
		if self.Tool then 
			if self.ui:getRightClick() then 
				self.Tool = false 
			end 
			self.ui.help = true 
			self.shapeUi.help = true 
			local x,y = self.ui:getmxb():bbox()
			local olf = love.graphics.getFont()
			local f = self.ui:getFont()
			love.graphics.setFont(f)
			setColor(colors.gray)
			x,y = x-f:getWidth("?"),y-f:getHeight()
			local r = f:getHeight()/2
			love.graphics.circle("fill",x+f:getWidth("?")/2,y+f:getHeight()/2,r)
			setColor()
			love.graphics.print("?",x,y)
			love.graphics.setFont(olf)
		else 
			self.ui.help = false 
			self.shapeUi.help = false  
		end 
	elseif self.gamestate == "endGame" then 
		if self.battleReport then self.battleReport:draw(dt) end
	else
		self.menu:draw()
	end
	if misc.showFPS then 
		showFPS()
	end 
	if DEBUG or STATS then
		printStats(5,205)
	end 
	drawRelease()
end
function battle:navigation(Bkeys,uiKeys)
	local BKeys,alt_BKeys = Bkeys:unpack()
	local uiKeys,alt_uiKeys = uiKeys:unpack()
	
	local function circleButtons(self,keys,alt)
		for i,v in ipairs(self.updateButtons) do 
			local a = keys[i] or alt[i]
			if not v.inactive and a then 
				v.OnClick(v)
			end 
		end 
	end 
	local function check(var,key,alt,func)	
		local b = key or alt
		local func = func or var.OnClick 
		if not var._inactive and not var.inactive and not var.permaInactive and b then 
			func(var)
		end 
	end 
	circleButtons(self,BKeys,alt_BKeys)
	self.ui:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
	self.ui:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
	
	self.shapeUi:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
	self.shapeUi:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
	
	self.UsedKeys["do"] = keys.ui[3].f() or alt_keys.ui[3].f()
	self.UsedKeys["cancel"] = keys.ui[4].f() or alt_keys.ui[4].f()
	
	local EndTurn = self._endTurn
	check(EndTurn,BKeys[6],alt_BKeys[6])
	local cards = self.cardSelection
	check(self.cardsOn,BKeys[7],alt_BKeys[7])
	
	if self.cardsDrawn then 
		for i,v in ipairs(self.varUpdateStack) do 
			check(v,BKeys[7 + i],alt_BKeys[7 + i],function(v) v.x,v.y = 0,0  v.Activate = true v.OnRelease() end)
		end 	
	end 
end 
function battle:keypressed(key)
	if self.gamestate == "playing" or self.gamestate == "endGame" then
		self.ui:keypressed(key)
	elseif self.gamestate == "menu" then
		self.menu:keypressed(key)
	end
	if not self.disableMenu and key == "escape" and not self.menu.warning then
		local menu  = self.menu
		if self.gamestate == "menu" then
			self.gamestate = self.OldGamestate
			menu:ends()
			self.menu.options = nil 
			self.menu.volutary = false 
		else
			self.gamestate = "menu"
			menu:start()
			self.menu.volutary = true 
		end
	end
end
function battle:worldPos(x,y)
	local x = (x-self.x)/(self.scale or 1)
	local y = (y-self.y)/(self.scale or 1)
	return x,y
end
function battle:camPos(x,y)
	local x = x*(self.scale or 1) + self.x
	local y = y*(self.scale or 1) + self.y
	return x,y 
end 
function battle:keyreleased(key)
	self.ui:keyreleased(x,y,key)
end
function battle:mousepressed(x,y,key)
	if self.gamestate == "menu" then 
		self.menu:mousepressed(x,y,key)
	else
	end
end
function battle:mousereleased(x,y,key)
end
function battle:Hcol(dt,a,b)
	if self.gamestate ~= "menu" then 
		if self.ui then  self.ui:Hcol(dt,a,b) end
	end 
end
function battle:Hstop(dt,a,b)
	if self.gamestate ~= "menu" then 
		if self.ui then self.ui:Hstop(dt,a,b) end 
	end 
end
function battle:visible()
end 
function battle:parse()
	local a = mapAdmin.parse(self.shapeCollider._active_shapes,self.shapeCollider._passive_shapes)
	return a
end 
function battle:HCol(dt,a,b)
	self.shapeUi:Hcol(dt,a,b)
end 
function battle:HStop(dt,a,b)
	self.shapeUi:Hstop(dt,a,b)
end 
function battle:mousefocus(f)
	if not self.disableMenu then 
		if not f and not self.menu.volutary then 
			self.gamestate = "menu"
			self.menu.volutary = false
			self.menu:start()
		elseif not self.menu.warning and not self.menu.volutary then
			self.gamestate = self.OldGamestate
			self.menu:ends()
		end
	end
end 
function battle:getType()
	return "battle"
end 
function battle:leave()
	local whitelist = {
		"enter",
		"updateCamera",
		"update",
		"render",
		"draw",
		"navigation",
		"keypressed",
		"worldPos",
		"camPos",
		"keyreleased",
		"mousepressed",
		"mousereleased",
		"Hcol",
		"Hstop",
		"HCol",
		"HStop",
		"visible",
		"parse",
		"mousefocus",
		"getType",
		"leave",
	} 
	print("LEAVING!!")
	self.ui:clear()
	Collider:clear()
	self.shapeCollider:clear()
	self.shapeUi:clear()
	for i,v in pairs(self) do 
		local w 
		for m,n in ipairs(whitelist) do 
			if i == n then 
				w = true
			end 			
		end
		if not w then 
			self[i] = nil 
		end 
	end  
	Timer.clear()
	collectgarbage()
end

return battle