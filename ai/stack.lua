local _PACKAGE = getPackage(...)
local stack = {
		class = require(_PACKAGE .. '.third.middleclass'),
		enemy = require (_PACKAGE .. '.enemy'),
		rayCast = require(_PACKAGE .. '.rayCast'),
	}
return stack