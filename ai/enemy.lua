-- local npc = require (_PACKAGE .. ".npc")
local _PACKAGE = getPackage(...)
local class = require(_PACKAGE .. '.third.middleclass')
local enemy = class("enemy")
local attack = require(_PACKAGE .. '.attacks')
local wireColor = false
local clear = {255,255,255,255}
local rayCast = require (_PACKAGE .. '.rayCast')
function enemy:initialize(x,y,health,armor)
	self.x = x
	self.y = y
	self.r = 0
	self.w = 50
	self.h = 50
	self.sx = 1
	self.sy = 1
	self.info = {
		--name = "Adam",
		--hp = 100,
		--mp = 100,
		--def = 10,
		--atk = 10,
		--luck = 10,
		--level = 0,
		--exp = 0,
		--attacks = {
			-- attacks go here
		--},
		--story = "The first of them all, the one to be born of god, later betrayed and turned to ashes that would form the new world.",
		--rarity = 10, --super rare
		--element = "devine",
	}
	self.buff = "none"
	self.state = "still"
	self.zmap = 1
	self.skills = {}
	self.effects = {}
	self.radius = 1
	self.states = {}
	self.speed = 150
	self.speed_limit = 450
	self.moving = false
	self.jumping = false
	self.timestamp = {}
	self.timestamp.a = 0
	self.timestamp.b = 0
	self.timestamp.clamp = 0
	self.clamps = {}
	self.clamps.yvel = false 
	self.colide = 0
	self.bodyColide = 0
	self.onGround = 0
	self.gravity = 2000
	self.segments = 0
	self.aiStack = {}
	self.rayWidth = 3
	self.attacks = {}
	self.direction = "right"
	self.raycast = rayCast:new(x - (self.w/2),y - (self.h/2))
	
	local func1 = function(fixture,x,y,xn,xy,fraction)
		for i in ipairs(self.fixtures) do
			if fixture == self.fixtures[i] then
				return true
			end
		end
	end

	self.raycast.mask = {}
	table.insert(self.raycast.mask,func1)

end
function enemy:setInfo(table)
	self.info = table
end
function enemy:setSpeedLimit(speed)
	self.speed = speed
end
function enemy:newAttack(...)
	local a = attack:new(self,...)
	self.attack = a
	return a
end
function enemy:setPosition(x,y)
	if self.body then
		self.body:setPosition(x,y)
	end
	self.x = x
	self.y = y
	self:makeRayPoints()
end
function enemy:remove()
	self.raycast:remove()
	for i in ipairs(self) do
		self[i] = nil
	end
end
function enemy:setPhyWorld(phyworld)
	self.raycast:setPhyWorld(phyworld)
	self.phyWorld = phyWorld
end
function enemy:setColWorld(world)
	self.colWorld = world
end
function enemy:setEffect(buff)
	for i in ipairs(effects) do
		for v in ipairs(self.invc) do
			if buff == effects[i] then
				if effect ~= self.invc[v] then
					self.buff = buff
					table.insert(self.effects,buff)
					return true
				end
			end
		end
	end
end
function enemy:setWeakness(tables)
	if type(tables) == "table" then
		self.weakness = tables
	else
		self.weakness = {
			tables
		}
	end
end
function enemy:setStrenghts(tables)
	if type(tables) == "table" then
		self.strenghts = tables
	else
		self.strenghts = {
			tables
		}
	end
end
function enemy:setInvicibility(tables)
	if type(tables) == "table" then
		self.invc = tables
	else
		if self.invc then
			self.invc = {
				tables
			}
		else
			table.insert(self.invc,tables)
		end
	end
end
function enemy:setRotation(x)
	self.r = x
end
function enemy:setOffset(x,y)
	self.ox = x
	self.oy = y
end
function enemy:setScale(x,y)
	self.sx = x or self.sx
	self.sy = y or self.sy
end
function enemy:setPos(x,y)
	self.x = x 
	self.y = y 
end
function enemy:moveTo(x,y)
	self.target = {}
	self.target.x = x
	self.target.y = y
end
function enemy:setSpeed(speed)
	self.speed = speed
end
function enemy:movement(dt)
	local jump = false
	local _moveClamp
	local _clamp = true
	
	if not self.speed then
		self.speed = 0
	end
	if self.moving then
		self.timestamp.clamp = self.timestamp.clamp + 10*dt
	else
		self.timestamp.clamp = 0
	end
	if self.timestamp.clamp > 3 then
		_clamp = false
	end
	local timestamp = self.timestamp
	local timeJump = self.timestamp.a
	local timeKey = self.timestamp.b
	local brake = false
	if not self.moving then
		self.inactive = true
	else
		self.inactive = false
	end
	
	local body = self.body
	
	local xvel,yvel = body:getLinearVelocity()
	if math.abs(math.floor(xvel)) == 1 or  math.abs(math.floor(xvel)) < 10 then
		if self.inactive then
			xvel = 0
		end
	end

	body:setAngularVelocity(0)
	body:setLinearVelocity(math.round(xvel,3),math.round(yvel,3))
	local colision
	if self.colide > 0 then
		colision = true
	else
		colision = false
	end
	local colision3
	if self.bodyColide > 0 then
		colision3 = true
	else
		colision3 = false
	end
	local colision4 
	if self.onGround > 0 then
		colision4 = true 
	else
		colision4 = false 
	end
	if colision then
		colision4 = true
	end
	self.directionChanged = false
	local colision2 = false
	if colision4 then
		self.timestamp.a = self.timestamp.a + 10*dt
		if self.timestamp.a > 4 then
			colision2 = true
			self.timestamp.a = 0
		end
	end
	local direction = self.moving
	local direction_old = self._odlDirection
	if direction_old and direction ~= direction_old then
		self.directionChanged = true 
		local body = self.body
		local x,y = body:getLinearVelocity()
		body:setLinearVelocity(0,y)
	end

	local speed_limit = self.speed_limit
	if self.inactive == false then
		if xvel < speed_limit and  xvel > -speed_limit then
			if  _clamp == false then
				local xvl,yvl = body:getLinearVelocity()
				if direction == "left" then
					xvl = xvl - (self.speed*dt)
					body:setLinearVelocity(xvl,yvl)
				elseif direction == "right" then
					xvl = xvl + (self.speed*dt)
					body:setLinearVelocity(xvl,yvl)		
				end
			end
		else
			local xvel,yvel = body:getLinearVelocity()
			if xvel > 0 then
				xvel = speed_limit
			elseif xvel < 0 then
				xvel = -speed_limit
			end
			
			body:setLinearVelocity(xvel,yvel)
		end
	else
		local xvel,yvel = body:getLinearVelocity()
		if xvel > 0 then
			xvel = xvel - (self.speed*4)*dt
		end
		if xvel < 0 then
			xvel = xvel + (self.speed*4)*dt
		end
		body:setLinearVelocity(xvel,yvel)
	end
	if self.colide > 0 and self.colide and  self.jumping then
		jump = true 
		local xvl,yvl = body:getLinearVelocity()
		local yvl = yvl - 900*dt
		body:setLinearVelocity(xvl,-700)
	end
	local _ = false
	local xvl,yvl = body:getLinearVelocity()
	if self.inactive or _clamp then
		_ = true
	else
		_ = false
	end
	yvl = yvl + ((self.gravity)*dt)
	if self.colide ~= 0 and self.onGround ~= 0 and _ then
		if math.abs(math.floor(yvl)) == 1 or  math.abs(math.floor(yvl)) < 20 then
				yvl = 0
				body:setLinearVelocity(xvl,yvl)
		end
	end
		if self.colide ~= 0 and self.onGround ~= 0 then
			if self.clamps.yvel then
				-- yvl = -.5
				self.clamp = true
				-- xvl = xvl + (29*self.scalex)
				local speed_limit = 200
				local difx = self.stairsW
				local dify = self.stairsH
				local ratx,raty
				if difx == 0 or dify == 0 then
					ratx = 1
					raty = 1
				else
					ratx = 1
					raty = tonumber(dify/difx)
				end
				local speed = speed_limit
				local a = speed/(raty+ratx)
				local speedy = (a*raty)
				local speedx = (a*ratx)
				if not self.inactive and not _ then
					if math.abs(yvl) + math.abs(xvl) < speed_limit then
						if direction == "left" then
							xvl = -speedx
							yvl = yvl - (self.gravity*dt)
							yvl = speedy
						end
						if direction == "right" then
							xvl = (speedx)
							yvl = yvl - (self.gravity*dt)
							yvl = -speedy
						end
					else
						yvl = yvl - (self.gravity*dt)
						xvl = 0
					end
				end
				if not jump and not self.inactive then
					local xvl,yvl = body:getLinearVelocity()
					if math.abs(yvl) + math.abs(xvl) > speed_limit then
						yvl = 0
						xvl = 0
					end
				end
				if not jump and self.inactive then
					xvl = 0
					yvl = 0
				end
			end
	end
	body:setLinearVelocity(xvl,yvl)
	local xvl,yvl = body:getLinearVelocity()
	self.yvel = yvl
	self.xvel = xvl
	self._odlDirection = direction
end
function enemy:move(mx,my,dir,moving)
		local stack = self.aiStack
		local direction
		local sucess = true
		local clamp = stack.clamp
		local yList = math.ceil(self.segments/4)
		local jumpTreshold = math.floor((self.h/self.rayWidth)/2) + 10
		-- if yList >= math.ceil(self.segments) then
			
		-- end
		-- MAKE FAIL CONDITION -- 
		-- create a func to deal with each failure independently
		-- extend ray lenght to that of overseer box to make sure it can jump to
		-- if returns sucess then make it run backwards then forwards
		-- If Mx My is out of the box then leave it.
		local horzFinal = false
		local vertFinal = false
		if math.floor(self.x) == math.floor(mx) then
			horzFinal = true
		end
		if math.floor(self.y) == math.floor(my) then
			vertFinal = true
		end
		local selfX = math.floor(self.x)
		local selfY = math.floor(self.y)
		if self.jumping then self.jumping = nil end
		if stack.fail and stack.mx == math.floor(mx) and stack.my == math.floor(my) and selfX == (stack.x or 0) and selfY == (stack.y or 0) then
			stack.clamp = true
			clamp = true
			sucess = false
		else
			if (stack.fail or 0) > 1 then
				stack.fail = nil
			end
			stack.clamp = false
			clamp = false
		end
		if not clamp and not stack.moving then
			if not stack.dir then
				if self.x > mx then
					direction = 1
				elseif self.x < mx then
					direction = -1
				else
					self.target = nil
				end
			else
				direction = stack.dir
				local hasJump
				local cm,clamp,hasJump,needsJump = self:rayView(direction*-1)
				local _,clamp2,hasJump,needsJump = self:rayView(direction)
				if needsJump then
					self.jumping = true
					if direction == 1 then
							self.moving = "left"
					elseif direction == -1 then
							self.moving = "right"
					end
					stack.direction = direction
					stack.moving = self.moving
				end
				if cm and clamp2 < 6 then
					direction = direction*-1
					if direction == 1 then

							self.moving = "left"

					elseif direction == -1 then

							self.moving = "right"

					end
					local clampY = self:rayViewVert(direction,yList)
					if clampY >= 1 and clampY < jumpTreshold then

							self.jumping = true

						if direction == 1 then

								self.moving = "left"

						elseif direction == -1 then

								self.moving = "right"

						end
						stack.direction = direction
						stack.moving = self.moving
					elseif clampY > 5 then
						stack.fail = (stack.fail or 0) + 1
						sucess = false
						stack.x = math.floor(self.x)
						stack.y = math.floor(self.y)
						stack.mx = math.floor(mx)
						stack.my = math.floor(my)
					end
				else
					if self.onGround == 1 then
						stack.fail = (stack.fail or 0) + 1
						sucess = false
						stack.x = math.floor(self.x)
						stack.y = math.floor(self.y)
						stack.mx = math.floor(mx)
						stack.my = math.floor(my)
						stack.dir = nil
						self.moving = false
					end
				end
				direction = nil
			end
		if direction then
			local clampY = self:rayViewVert(direction,yList)
			if clampY >= 1 and clampY < jumpTreshold then
					self.jumping = true
				if direction == 1 then
					self.moving = "left"
				elseif direction == -1 then
					self.moving = "right"
				end
				stack.direction = direction
				stack.moving = self.moving
			elseif clampY > 5 then
				stack.fail = (stack.fail or 0) + 1
				sucess = false
				stack.x = math.floor(self.x)
				stack.y = math.floor(self.y)
				stack.mx = math.floor(mx)
				stack.my = math.floor(my)
			end
		end
		if direction then
			local can_move,clamp,needsJump = self:rayView(direction,1)
			if can_move then
				if direction == 1 then

						self.moving = "left"

				elseif direction == -1 then

						self.moving = "right"

				end
			else
				stack.dir = direction
				self.moving = false
			end
		end
	elseif not clamp and stack.moving and self.onGround > 0 then

				self.jumping = false
				stack.moving = nil

	end
	if sucess then
		if math.floor(self.x) == stack.px and math.floor(self.y) == stack.py then
			stack.stuck_clamp = (stack.stuck_clamp or 0) + 1
		else
			stack.stuck_clamp = 0
		end
		if (stack.stuck_clamp or 0) > 300 then

				self.jumping = true

		end
	end
	stack.targetx =  math.floor(mx)
	stack.targety =  math.floor(my)
	stack.px = math.floor(self.x)
	stack.py = math.floor(self.y)
	return sucess,horzFinal,vertFinal
end
function enemy:rayView(direction,treshold)
	local hit,ray = self.raycast:returnHitlist()
	local seg = math.ceil(self.segments)
	local clamp = 0
	local starty = 0
	local startx = 0
	local finishy = 0
	local finishx = 0 
	local treshold = treshold or 0
	local can_move = false
	local jump = 0
	
	if seg then
	local starty = 0
	local center = math.ceil(seg/4)
	local refY = 0
		if ray[center].data  then
			refY = math.floor(ray[center].data.y)
		end
		for i = 0,7 do
			if direction == 1 then
				if ray[center + i].mature then
					clamp = clamp + 1
				end
			elseif direction == -1 then
				local x = center - i
				if x < 0 then
					x = math.ceil(seg - i) 
				end
				if ray[x] and ray[x].mature then
					clamp = clamp + 1
				end
			end
		end
		for i = 0,7 do
			if direction == 1 then
				if ray[center + i].mature then
					if math.floor(ray[center + i].data.y) ~= refY then
						jump = jump + 1
					end
				end
			elseif direction == -1 then
				local x = center - i
				if x < 0 then
					x = math.ceil(seg - i) 
				end
				if ray[x] and ray[x].mature then
					if math.floor(ray[x].data.y) ~= refY then
						jump = jump + 1
					end
				end
			end
		end
	end
	if clamp > treshold then
		can_move = true
	end
	local am 
	if jump >= 5 then
		am = true
	end
	return can_move,clamp,am
end
function enemy:rayViewVert(direction,treshold,returnList)
	local hit,ray = self.raycast:returnHitlist()
	local seg =  math.ceil(self.segments)
	local nam = math.ceil(seg/2)
	local clamp = 0
	if direction == 1 then
		for i = nam,treshold +nam do
			if ray[i].mature then
				clamp = clamp + 1
				if returnList then
					table.insert(returnList,ray[i].data)
				end
			end
		end
	else 
		for i = seg - treshold,seg + 2 do
			if i > seg then
				i = (i - seg)
			end
			if ray[i].mature then
				clamp = clamp + 1
				if returnList then
					table.insert(returnList,ray[i].data)
				end
			end
		end
	end
	return clamp,returnList
end
function enemy:setZmap(zmap)
	self.zmap = zmap
end
function enemy:setAnimation(anim,state)
	for i in ipairs(self.states) do
		if self.states[i].name == state then
			self.states[i].anim = anim
		end
	end
end
function enemy:addState(name,anim,func)
	local state = 
	{
		name = name, 
		anim = anim,
		func = func,
	}
	table.insert(self.states,state)
end
function enemy:setStates(tables)
	self.states = tables
	self.state = tables[1]
end
function enemy:getState()
	return self.state
end
function enemy:draw(wireframe,body)
	for i in ipairs(self.states) do
		local var = self.states[i]
		if var.name == self.state and var.anim then
			var.anim:draw(self.x,self.y,self.r,self.sx,self.sy,self.ox,self.oy)
		end
	end
	if self.body then
		for i = #self.shapes,1,-1 do
			love.graphics.setColor(0,50*i,0)
				if self.shapes[i]:getType() == "circle" then
					local x,y = self.body:getWorldPoints(self.shapes[i]:getPoint())
					 love.graphics.circle("fill",x,y, self.shapes[i]:getRadius())
				else
					love.graphics.polygon("fill", self.body:getWorldPoints(self.shapes[i]:getPoints()))
				end
			love.graphics.setColor(255,255,255)
		end
	end
	if wireframe then
		if self.colision then
			self.colision:draw("line")
			self.detect_range:draw("line")
			love.graphics.print(""..self.colide.."Body: "..self.bodyColide.." "..self.onGround.. "Moving: "..tostring(self.moving).."Jumping: "..tostring(self.jumping),self.x,self.y)
		end
		if self.attack then
			self.attack:draw()
		end
		love.graphics.setColor(255,0,0)
		self.raycast:draw()
		love.graphics.setColor(0,0,255)
		self.raycast:drawRange()
		love.graphics.setColor(clear)
	end
end
function enemy:pause()
	self.paused = true
	self.moving = false
end
function enemy:play()
	self.paused = false
end
function enemy:setColShape(shape)
	self.shape = shape
	self.shape:setUserData("enemy")
end
function enemy:setBody(body)
	body:setPosition(self.x,self.y)
	body:setUserData("enemy")
	self.uBody = true
	self.body = body
end
function enemy:setShapes(body,ground,indicator)
	if not ground or not body then
		error("Must include both ground and body shapes!")
	end
	table.insert(self.shapes,body)
	table.insert(self.shapes,ground)
	table.insert(self.shapes,indicator)
end
function enemy:setTargetPos(x,y,wakeup)
	if not x or not y then
		error("Wrong co ordinates, got: "..tostring(x).."  "..tostring(y))
	end
	if not self.paused then
		local x,y = math.floor(x),math.floor(y)
		local resultnormal
		local resultanti
		local stack = self.aiStack
		if not stack._globalFail and not stack.func then
			resultnormal = self:move(x,y)
			if not resultnormal then
				stack.resNormal = (stack.resAnti or 0) + 1
			end
		end
		if not resultnormal and not stack.func and not stack._globalFail then
				local direction 
				if not direction then
					if self.x < x then
						direction = -1
					elseif self.x > x then
						direction = 1
					end
				end
				if self.radius ~= (self.w*self.sx)/2 then
					stack.oldRadius = self.radius
					self:setRadius((self.w*self.sx)/2)
				end
				self.raycast:cast()
				local yTresh = math.ceil(self.segments/4)
				local maxX = 0
				local maxY = 0
				local minX = 0
				local minY = 0
				local retStack = {}
				local clampY,retStack = self:rayViewVert(direction,yTresh,retStack)
				for i in ipairs(retStack) do
					if retStack[i].x > maxX then
						maxX = retStack[i].x
						maxY = retStack[i].y
					end
					if retStack[i].x < minX then
						minX = retStack[i].x
						minY = retStack[i].y
					end
				end
				local maxX = math.floor(maxX)
				local maxY = math.floor(maxY)
				local minX = math.floor(minX)
				local minY = math.floor(minY)
				local clamp2 = false
				if direction > 0 and maxX == 0 then
					clamp2 = true
				elseif direction < 0 and minX == 0 then
					clamp2 = true
				end
				if not clamp2 then
					stack.func = function() 
						local s
						if direction > 0 then
							if x == maxX - self.w/2 then
								stack.func = nil
							end
							s,hf,vf = self:move(maxX - self.w/2,maxY)

						else
							if x == minX - self.w/2 then
								stack.func = nil
							end
							s,hf,vf = self:move(minX + self.w/2,minY)
						end
						return s,hf,vf
					end
				end
				self:setRadius(stack.oldRadius)
		end
		if stack.func then
			resultanti = stack.func()
			if not resultanti then
				stack.func = nil
				stack.resAnti = (stack.resAnti or 0) + 1
			end
		end
		local direction = 1
		local yTresh = math.ceil(self.segments/4)
		local clamp_anti
		local clamp_normal
		
		if self.x < x then
			direction = -1
		elseif self.x > x then
			direction = 1
		end
		if not stack._globalFail then
			local clampY = self:rayViewVert(direction,yTresh)
			local jumpTreshold = math.floor((self.h/self.rayWidth)/2) -1
			if clampY >= 1 and clampY < jumpTreshold then
			elseif clampY > jumpTreshold then
				clamp_normal = true
			end
			local clampY = self:rayViewVert(direction*-1,yTresh)
			local jumpTreshold = math.floor((self.h/self.rayWidth)/2) -1
			if clampY >= 1 and clampY < jumpTreshold then
			elseif clampY > jumpTreshold then
				clamp_anti = true
			end
			if clamp_anti and clamp_normal then
				-- stack._globalFail = true
			end
		end
			if (stack.resNormal or 0) >= 1 and (stack.resAnti or 0) >= 1 then
				stack._globalFail = true
				stack.resAnti = 0
				stack.resNormal = 0
			end
			if stack._targetx ~= x or stack._targety ~= y then
				stack._globalFail = false
			end
		stack._targetx = x
		stack._targety = y
	end
	
	-- local stack = self.aiStack
	-- local result
	-- local possitive = false
	-- local resultNormal
	-- local resultAnti
	-- -- if not stack._global_fail then
		-- -- if stack.localFail then
			-- -- if stack.localFail >= 15 then
				-- -- stack.func = nil
				-- -- stack.fFunc = nil
				-- -- local result
				-- -- if stack.localFail < 18 then
					-- -- result = self:move(x,y)
				-- -- else
					-- -- stack._global_fail = true
				-- -- end
				-- -- if not result then
					-- -- stack.localFail = stack.localFail + 1
				-- -- end
				-- -- if stack.localFail <= 15 then
					-- -- stack.localFail = nil
				-- -- elseif stack.localFail >= 18 and not self.jumping then
					-- -- stack.localFail = nil
				-- -- end
			-- -- end
		-- -- end
		-- if stack.fFunc and not stack._global_fail then
			-- stack.fFunc()
		-- elseif not stack._global_fail then
			-- local clamp = fals
			-- if not stack.func then
				-- if stack._global_fail 

				-- then
					-- clamp = true
				-- else
					-- stack._global_fail = nil
					-- result = self:move(x,y)
					-- resultNormal = result
				-- end
				-- if not result and not clamp then
					-- if not self.jumping then
						-- stack.medFail = (stack.medFail or 0) + 1
					-- end
					-- local it = 0
					-- stack.fFunc = function()
						-- it = it  + 1
						-- local direction 
							-- if not direction then
								-- if self.x < x then
									-- direction = -1
								-- elseif self.x > x then
									-- direction = 1
								-- end
							-- end
							-- if self.radius ~= (self.w*self.sx)/2 then
								-- stack.oldRadius = self.radius
								-- self:setRadius((self.w*self.sx)/2)
							-- end
							-- local yTresh = math.ceil(self.segments/4)
							-- local maxX = 0
							-- local maxY = 0
							-- local minX = 0
							-- local minY = 0
							-- local retStack = {}
							-- local clampY,retStack = self:rayViewVert(direction,yTresh,retStack)
							-- for i in ipairs(retStack) do
								-- if retStack[i].x > maxX then
									-- maxX = retStack[i].x
									-- maxY = retStack[i].y
								-- end
								-- if retStack[i].x < minX then
									-- minX = retStack[i].x
									-- minY = retStack[i].y
								-- end
							-- end
							-- local maxX = math.floor(maxX)
							-- local maxY = math.floor(maxY)
							-- stack.func = function() 
								-- local s
								-- if direction > 0 then
									-- s,hf,vf = self:move(maxX - self.w/2,maxY)
									-- -- print(maxX,maxY)
								-- else
									-- s,hf,vf = self:move(minX + self.w/2,minY)
									-- -- print(minX,minY)
								-- end
								-- return s,hf,vf
							-- end
							-- if it > 5 then
								-- stack.fFunc = nil
								-- self:setRadius(stack.oldRadius)
							-- end
						-- end
					-- end
			-- else
				-- local val,hf,vf = stack.func()
				-- local resultAnti = val
				-- if not val then
					-- stack.localFail = (stack.localFail or 0) + 1
				-- end
				-- if hf then
					-- stack.func = nil
				-- end
			-- end
		-- end
	
	-- -- if (stack.medFail or 0) >= 3 then
		-- -- stack._global_fail = true
	-- -- end
	-- -- if wakeup then
		-- -- stack.medFail = nil
		-- -- stack._global_fail = false
	-- -- end
	-- print(resultAnti,resultNormal)
	-- if not resultAnti and not resultNormal then
		-- stack._global_fail = true
	-- end
end
function enemy:makeColision()
	local body = 0
	if not self.body and not self.uBody then
		body = love.physics.newBody(self.phyWorld,self.x,self.y,"dynamic")
		if not self.shapes then
			self.shapes = {}
			self.fixtures = {}
			local ow = self.w 
			local oh = self.h
			local w = (ow *self.sx)
			local h = (oh *self.sy)
			
			local wa = 0
			local w2 = ((ow/2)*self.sx)/1.5
			local h2 = ((oh/2)*self.sy)/4 -(2.5*self.sx)
			if w2 <= 0 then
				w2 = 15
			end
			if h2 <= 0 then
				h2 = 5
			end
						-- Make the ellipse
			local diameter = h/5
			local oh = h - diameter
			if diameter > w/2 then
				diameter = w/2
				wa = w/2 - (diameter)
				oh = h - diameter
			end
			self.shapes[1] = love.physics.newRectangleShape(0,-h/2 + oh/2,w,oh)
			self.fixtures[1] = love.physics.newFixture(body,self.shapes[1])
			self.fixtures[1]:setUserData("Body")
			
			self.shapes[2] = love.physics.newRectangleShape(0,h/2,w,5)
			self.fixtures[2] = love.physics.newFixture(body,self.shapes[2])
			self.fixtures[2]:setUserData("Indicator")
			self.fixtures[2]:setSensor(true)

			self.shapes[3] = love.physics.newCircleShape(w/2 - (diameter) - wa,h/2 - diameter,diameter)
			self.fixtures[3] = love.physics.newFixture(body,self.shapes[3])
			self.fixtures[3]:setUserData("Ground")
			if diameter < w then
				self.shapes[4] = love.physics.newCircleShape(-w/2 + (diameter) - wa,h/2 - diameter,diameter)
				self.fixtures[4] = love.physics.newFixture(body,self.shapes[4])
				self.fixtures[4]:setUserData("Ground")
				if diameter*2 < w then
					local wo = w/2 - (diameter) - (-w/2 + (diameter))
					local ho = diameter
					self.shapes[5] = love.physics.newRectangleShape(0,oh - diameter*2,wo,ho)
					self.fixtures[5] = love.physics.newFixture(body,self.shapes[5])
					self.fixtures[5]:setUserData("Ground")
				end
			end
		end
	end
	body:setFixedRotation(true)
	body:setLinearDamping(0,0)
	self.body = body
	self.colision = self.colWorld:addRectangle(self.x,self.y,self.w,self.h)
	local w = (self.w*self.sx)*5
	local h = (self.h*self.sy)*5
	self.detect_range = self.colWorld:addRectangle(self.x + self.w/2,self.y + self.h/2,w,h)
	self.colision.userData = "AI"
	if self.userData then
		self.colision.subUserData = self.userData
	end
end
function enemy:detectRange(...)
	self.detect_range = self.colWorld:addRectangle(...)
end
function enemy:onContact(a,b,coll)
	local fixtures = self.fixtures
	if fixtures then
		for i in ipairs(fixtures) do
			local other
			local salf
			if a == fixtures[i] then
				other = b
				salf = a
			elseif b == fixtures[i] then
				other = a
				salf = b
			end
			if other and not other:isSensor()  then 
				local myUserData = salf:getUserData()
				local userData = other:getUserData()
				if myUserData == "Body" then
					coll:setEnabled(false)
					self.bodyColide = self.bodyColide + 1 
				elseif myUserData == "Ground" then
					if coll:isTouching() then
						coll:setEnabled(false)
						self.colide = self.colide + 1
					end
				elseif myUserData == "Indicator" then
					coll:setEnabled(false)
					self.jumped_from = {}
					self.jumped_from.x = 0
					self.jumped_from.y = 0
					self.jumped_from.direction = 0
					self.onGround = self.onGround + 1
				end
			end
		end
	end
end
function enemy:endContact(a,b,coll)
	local fixtures = self.fixtures
	if fixtures then
		for i in ipairs(fixtures) do
			local other
			local salf
			if a == fixtures[i] then
				other = b
				salf = a
			elseif b == fixtures[i] then
				other = a
				salf = b
			end
			if other and not other:isSensor() then 
				local myUserData = salf:getUserData()
				if myUserData == "Body" then
					if self.bodyColide ~= 0 then
						coll:setEnabled(false)
						self.bodyColide = self.bodyColide - 1
					end
				elseif myUserData == "Ground" then
					if self.colide ~= 0 then
						coll:setEnabled(false)
						self.colide = self.colide - 1
					end
				elseif myUserData == "Indicator" then
					if self.onGround ~= 0 then
						if self.jumping then
							self.jumped_from.x = self.x
							self.jumped_from.y = -self.y + 9
							self.jumped_from.direction = self.sx
						end
					end
					coll:setEnabled(false)
					self.onGround = self.onGround - 1
				end
			end
		end
	end
end
function enemy:setUserData(data)
	self.userData = data
	if self.colision then
		self.colision.subUserData = data
	end
end
function enemy:setRadius(radius)
	self.radius = radius
	self:makeRayPoints()
end
function enemy:makeRayPoints(rays,radius)
	clearTable(rays)
	local pi = math.pi
	local c = (radius or self.radius)
	c = 2 * pi * (radius or self.radius)
	local segments = c/self.rayWidth
	for i= 0,segments do 
		local ray = {}
		local n = (2*i*3.14)/segments
		local x = self.x +  c*math.cos(n) 
		local y = self.y +  c*math.sin(n) 
		ray.x = x
		ray.y = y 
		table.insert(rays,ray)
	end
	self.segments = segments
	self.raycast:setRaytable(rays)
end
function enemy:update(dt)
	for i in ipairs(self.states) do
		local var = self.states[i]
		if var.name == self.state and var.anim then
			var.anim:update(dt)
		end
	end
	self.raycast:setPosition(self.x,self.y)
	self:makeRayPoints()
	self.raycast:cast()
	if self.body then
			self:movement(dt)
		self.x,self.y = self.body:getPosition()
		self.colision:moveTo(self.x,self.y)
			self.detect_range:moveTo(self.x,self.y)
		

		if self.target and not self.paused then
			self:setTargetPos(self.target.x,self.target.y)
		end
	end
	if self.activeTarget then
		local a = self.activeTarget
		if self.OnAttack then
			self.attacking = true
			self.OnAttack(dt,a.source,a.type,a.mode,a.pos)
		else
			self.attacking = true
		end
	end
	if self.moving then
		self.direction = self.moving
	end
	if self.attack then
		self.attack:update(dt)
	end
	if self.onUpdate then
		self.onUpdate(dt)
	end
end
function enemy:setArmor(armor)
	self.armor = armor
end
function enemy:setAttack(attack)
	self.attack = attack
end

function enemy:HCol(dt,a,b,coll)
	if self.attack then
		self.attack:HCol(dt,a,b,coll)
	end
	if not self.activeTarget then
		if self.targets or self.friends then
			local other 
			if b == self.detect_range then
				other = a
			elseif a == self.detect_range then
				other = b
			end
			if other then
				 if other.source then
					local types = other.source.type
					if self.targets then
						for i in ipairs(self.targets) do
							if self.targets[i] == types then
								local px,py = other:center()
								self.activeTarget = {
									colObj = other,
									source = other.source,
									type = types,
									mode = "blacklist",
									py = py,
									px = px,
								}
							end
						end
					end
					if self.friends then
						for i in ipairs(self.friends) do
							if self.friends[i] == types then
								local px,py = other:center()
								self.activeTarget = {
									colObj = other,
									source = other.source,
									type = types,
									mode = "whitelist",
									px = px,
									py = py,
								}
							end
						end
					end
				end
			end
		end
	end
end
function enemy:Hstop(dt,a,b,coll)
	if self.attack then
		self.attack:HCol(dt,a,b,coll)
	end
	if self.targets or self.friends then
		local other 
		if b == self.detect_range then
			other = a
		elseif a == self.detect_range then
			other = b
		end
		if other then
			if self.activeTarget then
				if self.activeTarget["colObj"] == other then
					self.activeTarget = nil
					collectgarbage()
				end
			end
		end
	end
end
function enemy:setTarget(target)
	local fixturecheck = target:getFixtures()
	local func2 = function(fixture,x,y,xn,xy,fraction)
		for i in ipairs(fixturecheck) do
			if fixture == fixturecheck[i] then
				return true
			end
		end
	end
	table.insert(self.raycast.mask,func2)
end
function enemy:setWhitelist(whitelist)
	if whitelist then
		if type(whitelist) == "table" then 
			self.friends = whitelist
		else
			error("white list must be a table, such as {'type' , 'type2' }")
		end
	end
end
function enemy:setBlacklist(blacklist) 
	if whitelist then
		if type(blacklist) == "table" then 
			self.targets = blacklist
		else
			error("black list must be a table, such as {'type' , 'type2' }")
		end
	end
end
package.loaded[...] = enemy 