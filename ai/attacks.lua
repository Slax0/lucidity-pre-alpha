local _PACKAGE = getPackage(...)
local class = require(_PACKAGE .. '.third.middleclass')
local attack = class("attack")
function attack:initialize(parent,width,height,delay,ox,oy,cx,cy)
	self.parent = parent
	self.timer = 0
	self.type = "attack"
	local cx,cy = cx,cy 
	local ox,oy = ox,oy
	if not cx or not cy then
			cx,cy = parent.colision:center()
		end
	if parent.direction == "left" then
		if not ox or not oy then
			ox,oy = cx - (parent.w/2 + width),cy - height/2
		end		
	else
		if not ox or not oy then
			ox,oy = cx + (parent.w/2 + width),cy - height/2
		end 
	end
	self.ox = ox
	self.oy = oy
	self.delay = delay or 0
	self.position = 1
	self.speed = 1
	self.w = width
	self.h = height
	self.damage = 1
	self.colision = {}
end
function attack:setFrames(no,wh,hh,ox,oy)
	
	self.frames = {}
	for i = 0,no do
		local frame = {}
		frame.x = (wh + (ox or 0))*i
		frame.y = (hh + (oy or 0))*i
		frame.w = self.w
		frame.h = self.h
		frame.damage = self.damage
		frame.effects = self.effects
		frame.delay = self.delay
		table.insert(self.frames,frame)
	end
end
function attack:setFrame(no,w,h)
	
end
function attack:setAnimMode(mode)
	-- Continous: ---___, Procedural, _ - ______ instant --------
	self.mode = mode
end
function attack:update(dt)
	if self.frames and self.playing then
		local digit = self.direction
		self.timer = self.timer + dt * self.speed
		if self.timer > self.frames[self.position].delay then
			self.timer = self.timer - self.frames[self.position].delay
			self.position = self.position + 1 --*self.direction
		end
		if self.position > #self.frames then 
			self:stop()
		else
			local frame = self.frames[self.position]
			if self.lastFrame ~= frame then
				self.clamp = true
				local a = Collider:addRectangle(self.ox + (frame.x*digit),self.oy + (frame.y*digit),frame.w,frame.h)
				a.source = self 
				table.insert(self.colision,a)
				self.lastFrame = frame
			end
		end
	elseif self.playing then
		self.timer = self.timer + dt * self.speed
		if self.timer > self.delay then
			self.timer = self.timer - self.delay
			self.position = self.position + 1 --*self.direction
		end
		if self.position > 1 then 
			self:stop()
		else
			if not self.colision then
				local a = Collider:addRectangle(self.ox,self.oy,self.w,self.h)
				a.source = self 
				table.insert(self.colision,a)
			end
		end 
	end
end
function attack:stop()
	if type(self.colision) == "table" then
		for i in ipairs(self.colision) do
			Collider:remove(self.colision[i])
		end
	else
		Collider:remove(self.colision)
	end
	self.colision = {}
	self.position = 1
	self.playing = false
	self.clamp = false
	self.remove = true
end
function attack:play()
	local fx,fy = self.parent.activeTarget.px,self.parent.activeTarget.py
	local direction = "left"
	local digit
	if not self.playing then
		if fx < self.parent.x then
			digit = -1
			self.parent.direction = "left"
		else
			digit = 1
			self.parent.direction = "right"
		end
		self.direction = digit
	end
	direction = self.parent.direction
	local cx,cy = cx,cy 
	local ox,oy = ox,oy
	local parent = self.parent
	local width = self.w
	local height = self.h
	if not cx or not cy then
			cx,cy = parent.colision:center()
	end
	if direction == "left" then
		if not ox or not oy then
			ox,oy = cx - (parent.w/2 + width),cy - self.h/2
		end		
	else
		if not ox or not oy then
			ox,oy = cx + (parent.w/2 ),cy - self.h/2
		end 
	end
	if not self.playing and not self.clamp then
		self.ox = ox
		self.oy = oy 
	end
	self.playing = true
end
function attack:remove()
	if type(self.colision) == "table" then
		for i in ipairs(self.colision) do
			Collider:remove(self.colision[i])
		end
	else
		Collider:remove(self.colision)
	end
	self.colision = nil
	self.remove = true
	collectgarbage()
end
function attack:setCooldown(time)
	self.cooldown = time
end
function attack:setDelay(delay,frame)
	if frame then
		self.frames[frame].delay = delay
	else
		self.delay = delay
	end
end
function attack:HCol(dt,a,b,coll)
	if type(self.colision) == "table" then
		for i in ipairs(self.colision) do
			local other 
			if b == self.colision[i] then
				other = a
			elseif a == self.colision[i] then
				other = b
			end
			if other then
				if other.source then
					local osrc = other.source
					if osrc then
						if self.frames then
							local dmg = (self.frames[self.position].damage or self.damage)
							local eff = (self.frames[self.position].effects or self.effects)
							if osrc.dealDamage then
								osrc:dealDamage(dmg)
							end
						else
							local dmg = (self.damage)
							local eff = (self.effects)
							if osrc.dealDamage then
								osrc:dealDamage(dmg)
							end
						end
					end
				end
			end
		end
	else
		local other 
		if b == self.colision then
			other = a
		elseif a == self.colision then
			other = b
		end
		if other then
			 if other.source then
			end
		end
	end
end
function attack:setOverallDamage(damage,effects)
	if self.frames then
		for i in ipairs(self.frames) do
			if damage then
				self.frames[i].damage = damage
			end
			if effects then
				self.frames[i].effects = effects
			end
		end
	end
	self.damage = damage
	self.effects = effects
end
function attack:setFrameDamage(frame,damage,effects)
	if damage then
		self.frames[frame].damage =  damage
	end
	if effects then
		self.frames[frame].effects = effects
	end
end
function attack:setDamage(damage)
	
end
function attack:setEffects(effects)
	
end
function attack:draw()
	if self.colision then 
		if type(self.colision) == "table" then
			for i in ipairs(self.colision) do
				self.colision[i]:draw("line")
			end
		else
			self.colision:draw("line")
		end
	end
end
return attack