--[[------------------------------------------------
	-- Artificial Inteligence for Lucidity 
	-- Copyright (c) 2015-2017 Neka tek --
--]]------------------------------------------------
local _PACKAGE = getPackage(...)
local class = require(_PACKAGE .. '.third.middleclass')
local aiWorld = class('aiWorld')
function aiWorld:initialize(phyWorld,colWorld)
	self.enemyStack = {}
	self.npcStack = {}
	self.zmap = 1
	self.entity = {}
	self.color = {}
	self.color.entity = {0,0,255}
	self.color.rays = {0,255,0}
	self.phyWorld = phyWorld
	self.colWorld = colWorld
	
	class = require(_PACKAGE .. '.third.middleclass')
	rayCast = require (_PACKAGE .. '.rayCast')
	aiWorld = class('aiWorld')
	enemy = require (_PACKAGE .. '.enemy')
	return self
end
function aiWorld:finalize()
	local stack = {}
	stack.aiWorld = self
	stack.phyWorld = self.phyWorld
	stack.colWorld = self.colWorld
	aiWorldStack = stack
end
function aiWorld:getTools()
	local tools = {
		enemy = enemy,
		rayCast = rayCast,
	}
	return tools
end
function aiWorld:clear()
	for i in ipairs(self.enemyStack) do
		self.enemyStack[i] = nil
	end
	for i in ipairs(self.npcStack) do
		self.npcStack[i] = nil
	end
end
function aiWorld:__tostring()
	return "AI world, Pointer: "..self
end
function aiWorld:setZmap(zmap)
	self.zmap = zmap
end
function aiWorld:setColisionWorld(world)
	self.colWorld = world
end
function aiWorld:setPhysicsWorld(world)
	self.phyWorld = world
end
function aiWorld:totalEntities()
	return #self.enemyStack + #self.npcStack
end
function aiWorld:newEnemy(...)
	local s = enemy:new(...)
	s:setPhyWorld(self.phyWorld)
	s:setColWorld(self.colWorld)
	s.state = "still"
	table.insert(self.entity,s)
	return s
end
function aiWorld:newNPC(...)
	local s = npc:new(...)
	return s 
end
function aiWorld:remove(body)
	if body then
	else
		
		for i in ipairs(self.entity) do
			self.entity[i]:remove()
			self.entity[i] = nil
		end
		aiWorldStack = nil
	end
end
function aiWorld:setWireframeColor(entity,rays)
	if type(entity) == "table" then 
		self.color.entity = entity
	else
		self.color.entity = self.color.entity
	end
	if type(rays) == "table" then
		self.color.rays = rays
	else
		self.color.rays = self.color.rays
	end
end
return aiWorld