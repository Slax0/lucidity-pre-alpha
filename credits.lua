local credits = {}
local LargeFont = love.graphics.newFont(18)
local fluxGroup = Flux.group()
local gap = 20
function credits:enter()
	self.text = {} 
	local function insert(t)
		table.insert(self.text,t)
	end
	insert("----------Credits-------------")
	insert("Thank you for playing Lucidity.")
	insert("As of Alpha 0.1, I am the sole developer of Lucidity")
	insert("--------Special Thanks--------")
	insert("Special thanks goes out to producers of love2d libraries")
	insert(" ")
	
	insert("Hump - Matthias Richter")
	insert("Hardon Collider - Matthias Richter")
	insert("Flux - rxi")
	insert("AnAl - Bart Bes")
	insert("And most importantly:")
	insert("Love2D - LOVE Development Team")
	insert(" ")
	insert("Kevin Ge - Avictus ghost design and animation")
	insert("----------After Word----------")
	insert("Lucidity is my first game and my first endeavour as an indie developer.")
	insert("My story dates back a few years when I started learning programming ")
	insert("I was a kid back then with little to nothing except a passion for video games")
	insert("at the time programming was a far away lighthouse in the sea of confusion.")
	insert("I learned from online tutorials, trial and error. ")
	insert("With little to no experience in OOP I had jumped into OpenGL and 3D")
	insert("as you may have guessed this overwhelmed me.")
	insert("I failed, over and over.")
	insert("That's when I found Love2D.")
	insert("Step by step I built my game and before I knew it I had become fairly competent")
	insert("this is the result of my effort not only to learn programming but to make a game.")
	insert("I hope you will and have enjoyed lucidity as much as I enjoyed making it,")
	insert("------------------------------")
	insert("Copyright (C) 2015 J Mikus Sabanskis")
	insert(" ")
	insert("This software is licensed under:")
	insert("Creative Commons Attribution-NonCommercial 4.0")
	insert("------------------------------")
	for i=1,#self.text do 
		
	end 
	self.x = love.graphics.getWidth()/2
	self.y = (LargeFont:getHeight())*(#self.text-1)
	self.tween = fluxGroup:to(self,70,{y = -((LargeFont:getHeight()+gap)*#self.text)}):oncomplete(function()
		Gamestate.switch(mainMenu)
	end):ease("linear")
end
function credits:draw()
	love.graphics.setFont(LargeFont)
	for i,v in ipairs(self.text) do 
		love.graphics.print(v,self.x - LargeFont:getWidth(v)/2,self.y + (LargeFont:getHeight() + gap)*i)
	end 
	love.graphics.setFont(font)
end 
function credits:update(dt)
	fluxGroup:update(dt)
end 
function credits:keypressed(key)
	if key == "escape" then 
		Gamestate.switch(mainMenu)
	end 
end
return credits