local battleNpc = {}
local smallFont = love.graphics.newFont(8)
local function createParticles(self,type,image)
		print("CREATING PARTICLES")
		self.StopEffect = false
		assert(effectColors[type],"No Such Type: "..type)
		local ranGap = math.random(1,10)
		local color = effectColors[type][2]
		local method = effectColors[type][1]
		local availSpace = self.width + math.random(1,5)
		local circles = {}
		for x = 0,availSpace,20 do
			local y = 1
			local size = love.math.random(5,math.max(5,20))
			local color = {color[1],color[2],color[3],color[4]}
			local circle = {ox = x, oy = y,osize = size,x = x, y = y ,color = color,size = size }
			table.insert(circles,circle)
		end 
		local width,height = self:getDimensions()
		local function contEffect(circle)
			circle.x = circle.ox
			circle.y = circle.oy
			circle.color = {color[1],color[2],color[3],color[4]}
			circle.size = math.random(math.min(circle.osize,10),20)
			local function tween(circle)
				if not self.StopEffect then 
					local distance = math.random(height/2,height)
					local speed = (height*2)/(circle.size/5)
					local delay = distance/speed
					circle.tween = Timer.tween(delay,circle,{y = -distance,color = {0,0,0,0}},nil,function() contEffect(circle) end)
				else 
					circles = {} 
				end
			end 
			tween(circle)
		end  
		for i,v in ipairs(circles) do 
			contEffect(v)
		end  
		
		local function draw(x,y)
			for i,v in ipairs(circles) do 
				love.graphics.setColor(v.color)
				method(x + v.x,y + v.y,v.size)
				love.graphics.setColor(colors.white)
			end 
		end 
	return draw,tween
end 
local function healing(self,type) 
	local icon = icons[type]
	local icons = love.graphics.newParticleSystem(icon)
	icons:setAreaSpread("uniform",self.width/2,10)
	icons:setLinearAcceleration(0,-100,0,-200)
	icons:setParticleLifetime(0.8,1)
	icons:setEmissionRate(3)
	icons:setColors(255, 255, 255, 255, 255, 255, 255, 100)
	return icons 
end 
local function singleH(self,type)
	local width,height = self:getDimensions()
	local icon = icons[type]
	local icons = {}
	local w = width 
	for i=0,6 do 
		local ic = {} 
		if i%2 == 0 then 
			ic = {
				x = w - icon:getWidth()/2,
				y = 0
			} 
		else 
			ic = {
				x = -icon:getWidth()/2,
				y = 0 
			}
		end 
		ic.t =  255
		table.insert(icons,ic)
	end 
	
	local h = height + icon:getHeight()
	local hf = h/3
	
	local mode = nil
	local no = 0 
	for i,v in ipairs(icons) do 
		local w = icon:getWidth()
		local x = v.x 
		local delay = 0.5
		local f = -1 
		if i %2 == 0 then 
			f = 1
		end 
		v.tw = Timer.add(i/10,function()
		v.tw = Timer.tween(delay,icons[i],{x = x + (w*f),y= -(hf*1)},mode,function()
			v.tw = Timer.tween(delay,icons[i],{x = x+ (w*-f),y = -(hf*2)},mode,function()
				v.tw = Timer.tween(delay,icons[i],{x = x + (w*f),y = -(hf*3)},mode,function()
						no = no +1 
						if no >= #icons then 
							print("CANCELED")
							self.heal_effect = nil 
							collectgarbage()
						end 
					end)
				end)
			end)
		v.t2 = Timer.tween(delay*3,icons[i],{t = 50})
		end)
	end 
	function icons:draw(x,y)
		for i,v in ipairs(icons) do
			setColor({255,255,255,v.t})
			love.graphics.draw(icon,x + v.x,y + v.y)
			setColor()
		end 
	end 
	return icons
end 
function battleNpc:new(npc,temp)
	local info = npc.info
	local self = {
		width = npc.w,
		height = npc.h,
		name = info["name"],
		hp = info["hp"] or info["maxHp"],
		mp = info["mp"] or info["maxMp"],
		maxHp = info["maxHp"],
		maxMp = info["maxMp"],
		def = info["def"],
		atk = info["atk"],
		luck = info["luck"],
		level = info["level"],
		exp = info["exp"] or 0,
		skills = info["skills"],
		story = info["story"],
		rarity = info["rarity"],
		element = info["element"],
		weak = info["weak"],
		strong = info["strong"],
		inv = info["invin"],
		stillFrame = info["stillFrame"],
		state = "idle",
		states = npc.states,
		image = npc.image,
		scale = npc.scale or 1,
		mirror = npc.mirror,
		sourceCrystal = info["sourceCrystal"],
		filter = npc.filter,
		expChange = 0,
		info = info,
		meta = npc.meta 
	}
	self.damageTaken = 0
	self.aHp = self.hp
	self.oldHp = self.hp
	self.oldMp = self.mp
	self.aMp = self.mp
	self.sx = npc.scale or 1 
	self.sy = npc.scale or 1 
	if not temp then 
		for i,v in ipairs(self.states) do
			if not v.anim.type == "animation" then 
				local a,s,d,f,g,h = unpack(v.anim)
				v.anim = newAnimation(self.image,a,s,d,f,g,h)
			elseif v.anim.__index ~= animationMeta then 
				local a,s,d,f,g,h = unpack(v.anim.data or v.anim)
				local fil = self.filter or "nearest"
				self.image:setFilter(fil,fil)
				v.anim = newAnimation(self.image,a,s,d,f,g,h)
			else 
				v.anim:reset()
				v.anim:setMode("loop")
			end 
		end 
	end 
	setmetatable(self, { __index = battleNpc })
	return self
end
function battleNpc:setButton(b)
	self.button = b
end
function battleNpc:getButton()
	return self.button
end 
function battleNpc:attack(mob,types)
	self.attacking = 1
	local attacktpy
	local damage
	if types == "basic" or not types then
		self.state = "attack"
		attacktpy = "attack"
	else 
		local clamp
		for i,v in ipairs(self.states) do
			if v.name == types then
				self.state = v.name
				attacktpy = v.name
				clamp = true
			end
		end
		if not clamp then
			attacktpy = v.name
		end
	end
	if attacktpy ~= "attack" then
		local result
		for i,v in ipairs(self.skills) do
			if v == types then
				attacktpy = types
				self.adStack = v
			end
		end
		assert(result,"Invalid form of attack, got: "..tostring(result))
	else
		self.atk = self.atk
		self.attacktyp = attacktpy
		self.mob = mob
	end
end
function battleNpc:setGuard(bool)
	self.guard = bool
end
function battleNpc:getDimensions()
	return self.width*math.abs(self.sx),self.height*math.abs(self.sy)
end 
function battleNpc:cast(cost,state)
	self.mp = self.mp - (cost or 0) 
	self.state = state or "cast"
	self.casting = true
end 
function battleNpc:getSkills()
	return self.skills
end
function battleNpc:draw(mode)
	local v = self
	local x,y,r,sx,sy,ox,oy = self.x,self.y,self.rotation,self.sx,self.sy,self.ox,self.oy
	if self.mirror then 
		sx = -sx 
	end 
	local fil = self.filter or "nearest"
	self.image:setFilter(fil,fil)
	if self.stunned then 
		setColor({63,63,63})
	end 
	if self.animation then
		self.animation:draw(x,y,r or 0,sx or self.sx or 1,sy or self.sy or 1,ox or self.ox or 0,oy or self.oy or 0)
	else
		for i,v in ipairs(self.states) do
			if v.name == self.state then
				v.anim:draw(x,y,r or 0,sx or self.sx or 1,sy or self.sy or 1,ox or self.ox or 0,oy or self.oy or 0)
			end
		end
	end
	setColor()
	local width,height = self:getDimensions()
	if self.drawEffect and not mode then self.drawEffect(self.x - width/2,self.y + height/2) end
	if self._tAnim then 
		local a = self._tAnim
		local _,_,w,h = a:getViewport()
		local sx,sy = width/(w+10),height/(h+10)
		a:draw(self.x,self.y,0,sx*(self.sx*-1),sy,width/2,height/2)
	end 
	if self.heal_effect then 
		self.heal_effect:draw(self.x - width/2,self.y + height/2)
	end 
	if self.OnDraw then
		self.OnDraw(self)
	end

	if self.particles then 
		love.graphics.draw(self.particles,self.x,self.y + height/2)
	end 
end	
function battleNpc:drawHealth()
	local v = self
	local width,height = self:getDimensions()
	if not v.dead and not mode then 
		local x,y = v.x,v.y
		if v.maxHp - v.hp >= 0 then 
			local rectangle = love.graphics.rectangle
			local pr = width/v.maxHp
			local w2 = pr*v.aHp
			local DH = 3
			setColor(127,0,0)
			love.graphics.rectangle("fill",x - width/2,y + height/2 + 5,w2,DH)
			local w = pr*v.hp
			setColor(255,0,0)
			rectangle("fill",x - width/2,y + height/2 + 5,w,DH)
			local pr = width/v.maxMp
			local ws = pr*v.aMp
			setColor(255,0,255)
			rectangle("fill",x - width/2,y + height/2 + 11,ws,DH)
			local ws2 = pr*v.mp
			setColor(0,0,255)
			rectangle("fill",x - width/2,y + height/2 + 11,ws2,DH)
			setColor(255,255,255,255)
			local f2 = love.graphics.getFont()
			local f = smallFont
			love.graphics.setFont(f)
				local w = font:getWidth("H: "..v.hp.." ") + 3
				local h = font:getHeight()
				love.graphics.print("H: "..v.hp,(v.x - width/2),v.y + height/2 +11 - ( DH) + h)
				love.graphics.print("M: "..v.mp,(v.x - width/2 + w),v.y + height/2 +11 - ( DH) + h)
			love.graphics.setFont(f2)
		end
		if v.guard then
			local w,h = icons.shield:getDimensions()
			local maxScale = v.height/h 
			local maxXscale = (width/2)/w
			if not v.guardTable then 
				v.guardTable = {
					scale = 1,
					xscale = 1,
					finished = 1,
				}
			end 
			if v.guardTable.finished == 1 then
				Timer.tween(1,v.guardTable,{scale = maxScale,xscale = maxXscale},"in-out-quad",function() v.guardTable.finished = 2 end)
				v.guardTable.finished = 0
			elseif v.guardTable.finished == 2 then
				Timer.tween(1,v.guardTable,{scale = 1,xscale = 1},"in-out-quad",function() v.guardTable.finished = 1 end)
				v.guardTable.finished = 0
			end 
			love.graphics.draw(icons.shield,v.x + 5 + width/2,v.y,0,v.guardTable.xscale,v.guardTable.scale,w/2,h/2)
			love.graphics.draw(icons.shield,v.x - 5 - width/2,v.y,0,-v.guardTable.xscale,v.guardTable.scale,w/2,h/2)
		end
		if self.levelingUp then self.levelingUp() end 
	end
end 
--types means element
function battleNpc:getPos()
	return self.x,self.y
end
function battleNpc:setEffect(effect,duration,cont,dmg)
	local duration = duration or 3 
	if effect == "heal" or effect == "restore" then 
		self.particles = healing(self,effect)
	else
		self.effect = effect
		self.effectDuration = duration or 3 
	end 
	
	if not cont then 
		local i = 0 
		self.OnChangeTurn = function() 
			local dmg = dmg or (self.hp/100)*10
			local type = effect
			if i >= (duration or 3) then 
				self.effect = nil
				if self.particles then self.particles:stop() end 
				self.particles = nil 
				self.OnChangeTurn = nil
				collectgarbage()
			end
			i = i + 1
			return dmg,type
		end 
	end 
end 
function battleNpc:setAnimation(anim,state) 
	self.state = state
	self.animation = anim
	if not anim then
		self.state = "idle"
	end
end
function battleNpc:changeTurn()
	local dmg
	if self.OnChangeTurn then 
		if self.guard then self.guard = false end 
		dmg,types = self.OnChangeTurn(self)
		if dmg then self:applyDamage(dmg,types) end 
		if self.effect == self.OldEffect then 
			if self.effectDuration then 
				if self.effectDuration < (self._sameEffectFor or 0) then 
					print("Removing effect")
					self.effect = nil
				end 
			end 
			self._sameEffectFor = (self._sameEffectFor or 0) + 1
		end 
	end
	if self.stunned then 
		local s = self.stunned
		s.cur = s.cur + 1 
		if s.cur > s.dur-1 then 
			self.stunned = nil 
			if self.button  then self.button.permaInactive = false end 
		end 		
	end 
end 
function battleNpc:stun(dur)
	self.stunned = {
		cur = 0,
		dur = dur or 1,
	}
end
function battleNpc:applyDamage(atk,types,stack)
	if not self.dead then 
		local heal 
		local restore
		if self.OnAttack then atk,types,stack = self.OnAttack(self,atk,types,stack) end
		for i,v in ipairs(self.states) do
			if v == "damage" then
				self.state = v
			end
		end
		local range = 30 - (self.luck*2)
		if range <= 0 then
			range = 5
		end
		local critChance = math.random(1,range)
		local prc = 1
		local modif
		local damageBlock
		for i,v in ipairs(self.strong) do
			if v == types then
				prc = 0.5
				modif = "Strong"
			end
		end
		for i,v in ipairs(self.weak) do
			if v == types then
				prc = 2 
				modif = "Weak"
			end
		end
		for i,v in ipairs(self.inv) do
			if v == types then
				atk = 0
				modif = "Block"
			end
		end
		if critChance == 1 then
			prc = prc*2
			modif = "Critical"
		end
		if types and types ~= "attack" then
			damageBlock = atk*prc
			if types == "heal" then 
				heal = true 
			elseif types == "restore" then 
				restore = true 
			end 
		end
		if types and types == "attack" then 
			self._tAnim = icons["attack"]:addInstance()
			self._tAnim.OnFin = function()
				self.stiches = false
				self._tAnim = nil
			end
			self.stiches = true 
		end
		local damage = damageBlock or (atk - self.def)*prc
		if damage < 0 then
			damage = 0
		end
		if self.guard then
			local pc = damage/100
			damage = damage - (pc*25) 
			self.guard = false
		end
		if stack then
			damage = stack(self,self.def,prc)
		end
		damage = math.floor(damage)
		if not modif and damage == 0 then
			modif = "Miss!"
		elseif modif == "Critical" and damage == 0 then
			modif = "Crit Miss!"
		end
		if heal then 
			self.heal_effect = singleH(self,"heal")
			self.hp = self.hp + damage 
			if self.hp > self.maxHp then 
				self.hp = self.maxHp
			end 
		elseif restore then 
			self.heal_effect = singleH(self,"restore")
			self.mp = self.mp + damage
			if self.mp > self.maxMp then 
				self.mp = self.maxMp 
			end 
		else 
			self.hp = self.hp - damage
		end 
		if self.hp == 0 or self.hp < 0 then
			modif = "Deathblow!!"
		end
		if self.attackTable then 
			local x2,y2 = self:getPos()
			local a = {
				x = x2,
				y = y2 - self.height,
				sx = 1,
				sy = 1,
				heal = heal,
				restore = restore,
				mobHp = self.hp,
				dmg = damage,
				modif = modif,
				mob = target,
			}
			table.insert(self.attackTable,a)		
		end 
		if not heal and not restore then 
			self.damageTaken = self.damageTaken + damage
			self.shake = true
		end 
		return damage,modif
	end
end
function battleNpc:setAttackTable(tablez)
	self.attackTable = tablez
end 
function battleNpc:getDamage()
	return self.damageDealt,self.modif
end
function battleNpc:addExp(ex)
	self.exp = math.floor(self.exp + ex) 
	if self:getExpNeeded() <= 0 then 
		self.info.level = self.info.level + 1 
		self.level = self.level + 1 
		self:levelUp()
	end 
	self.expChange = math.floor(self.expChange + ex) 
end
function battleNpc:retExpChange()
	return self.expChange
end
function battleNpc:getExpNeeded()
	local x = self.level
	local expToLevel = math.floor(math.log(x)*60 + 30+(x^2/5))
	return expToLevel - self.exp 
end
local battleAi = {}
function battleNpc:newAi(oppositeTeam,ownTeam)
	local ai = {
		mode = mode,
		oTeam = oppositeTeam,
		sTeam = ownTeam,
	}
	setmetatable(ai, { __index = battleAi })
	return ai
end
function battleNpc:levelUp()
	if self.y and self.x  then 
		local rectangles = {}
		local finished 
		function curve(x)
			local x = x -100/2
			return -0.015*x^2 
		end 
		local width,height = self:getDimensions()
		local text = "Level up: "..self.level
		local h = font:getHeight()
		local levelUp = {
			y = (self.y - height) - h,
			t = 255,
		}
		Timer.tween(4,levelUp,{
			y = levelUp.y - 100,
			t = 0, 
		},nil,function()
			self.levelingUp = false 
		end)
		for i=0,100,10 do 
			local t = {
				x = i,
				y = 0,
				t = 0,
				size = 10,
			}
			table.insert(rectangles,t)
			Timer.tween(i/50,t,{
				x = i,
				y = curve(i),
				t = 255,
			},nil,function()
				if i == 100 then 
					for i,v in ipairs(rectangles) do 
						Timer.tween(i/30,v,{t = 0})
					end 
					finished = true 
				end 
			end)
		end 
		mergeTable(self.info,self.info.statIncrease,"add")
		self.hp = self.info.maxHp
		self.mp = self.info.maxMp
		self.maxHp = self.hp 
		self.maxMp = self.mp 
		local width,height = self:getDimensions()
		local font = love.graphics.newFont(uniBody_caps,24)
		local time = 0 
		local drawFn = function()
			local cx,cy = self.x - width,self.y + height/2
			for i,v in ipairs(rectangles) do 
				local t = v.t
				local r,g,b = unpack(colors.yellow)
				setColor(r,g,b,t)
				love.graphics.rectangle("fill",cx + v.x,cy + v.y,10,10)
				setColor()
			end 
			local olf = love.graphics.getFont()
			love.graphics.setFont(font)
			local w = font:getWidth(text)
			local r,g,b = unpack(colors.yellow)
			local t = levelUp.t
			setColor(r,g,b,t)
			love.graphics.print(text,self.x-w/2,levelUp.y)
			setColor()
			love.graphics.setFont(olf)
		end
		if self.info.OnLevelUp then 
			self.info.OnLevelUp(self.info.level)
		end 
		self.levelingUp = drawFn
	end 
end
function battleAi:figure(team1,team2)
	print("--AI STARTS TO FIGURE--")
	local sTeam = team2
	local oTeam = team1
	for i,v in ipairs(sTeam) do
		v.action = nil 
		v.selected = nil
	end
	for i,v in ipairs(oTeam) do
		v.selected = nil
		v.action = nil
		v._tPriority = nil 
	end
	
	local freeEnemies = {} 
	local freeFriends = {}
	for i,v in ipairs(sTeam) do 
		if not v.button.permaInactive and not v.dead then 
			if v.dead and v.hp <=0 then 
			else 
				table.insert(freeFriends,v)
			end 
		end 
	end 
	for i,v in ipairs(oTeam) do 
		if  v.dead and v.hp <=0 then 
		else
			table.insert(freeEnemies,v)
		end 
	end 
	for i,v in ipairs(freeEnemies) do 
		for k,m in ipairs(freeFriends) do 
			if v.atk >= m.hp then 
				if (m.atk - m.def) > m.hp then 
					v._tPriority = 2 
				else 
					v._tPriority = 1 
				end 
			elseif v.mp > 0 then 
				for b,n in ipairs(v.skills) do
					if n.cost <= v.mp then 
						if n.type == "heal" or n.type == "restore" then 
							v._tPriority = 3
						elseif n.damage > m.hp then 
							v._tPriority = 2 
						end 
					end 
				end 
			end 
		end
		if not v._tPriority then 
			v._tPriority = 4 
		end 
	end 
	local function checkFree()
		freeFriends = {} 
		for i,v in ipairs(sTeam) do 
			if not v.button.permaInactive and not v.dead and not v.selected then 
				if v.hp <=0 then 
				else 
					table.insert(freeFriends,v)
				end 
			end 
		end 
	end
	local function setTask(mob,target,mode)
		mob.action = {
			target = target,
			mode = mode,
		}
		mob.selected = true 
	end
	local function checkIfCankill(self,enemy)
		local mode = "attack"
		local can = false 
		if (enemy.hp + enemy.def) > self.atk then  
			for i,v in ipairs(self.skills) do 
				if v.cost < self.mp then 
					if v.type ~= "heal" or v.type ~= "restore" then 
						if v.target == "foe" or v.target == "team" then 
							if v.damage > enemy.hp then 
								can = true 
								mode = v
							end 
						end 
					end 			
				end 
			end 
		else 
			can = true 
		end 
		return can,mode
	end 
	local function checkForLowHealth()
		local heal,target 
		for i,v in ipairs(sTeam) do 
			for k,m in ipairs(freeEnemies) do 
				if not v.selected and  m.atk > v.hp and precentOf(v.maxHp,v.hp) < 80 then 
					heal = true
					target = v  
				end 
			end 
		end 
		return heal,target
	end 
	local function hCheck()
		local h,t = checkForLowHealth()
			local selected 
			if h then 
				for i,v in ipairs(freeFriends) do 
					for k,m in ipairs(v.skills) do 
						if m.type == "heal" then 
							setTask(v,t,m)
						end 
					end 
				end 
			end 
		return h 
	end
	local function cumulativeCheck(enemy)
		local a = freeFriends
		local e = enemy
		table.remove(a,1)
		local dmg = 0 
		local can = false 
		local stack = {} 
		for i,v in ipairs(a) do
			if not v.selected and not can then 
				dmg = dmg + v.atk
				table.insert(stack,v) 
				if dmg > e.hp + e.def then 
					can = true 
				end 
			end 
		end 
		return can,stack 
	end 
	for i,v in ipairs(freeEnemies) do 
		local p = v._tPriority 
		print("Priority for team2:"..i.." is "..p)
		if p == 1 then 
			local can = false 
			local mode 
			checkFree()
			for k,m in ipairs(freeFriends) do 
				if not can then 
					can,mode = checkIfCankill(m,v)
					if can then 
						setTask(m,v,mode)
					end
				end 
			end 
			if not can then 
				can,stack = cumulativeCheck(v)
				if not can then 
					-- leave it till last 
				else 
					for k,m in ipairs(stack) do 
						setTask(m,v,"attack")
					end 
				end 
			end 
		elseif p == 2 then 
			local can = false 
			local mode 
			checkFree()
			for k,m in ipairs(freeFriends) do 
				if not can then 
					can,mode = checkIfCankill(m,v)
					if can then 
						setTask(m,v,mode)
					end
				end 
			end 
			if not can then 
				can = cumulativeCheck(v)
				if not can then 
					hCheck()
				end 
			end 
		elseif p == 3 then
			checkFree()
			for k,m in ipairs(freeFriends) do 
				local mode 
				if not can then 
					can,mode = checkIfCankill(m,v)
					if can then 
						setTask(m,v,mode)
					end
				end 
			end 
			if not can then  
				hCheck()
			end 
		end 
				-- check for low health 
	
		end 
		hCheck()
		checkFree()
		local minHealth = math.huge 
		local target 
		for i,v in ipairs(freeEnemies) do 
			if v.hp < minHealth then 
				minHealth = v.hp 
				target = v 
			end 
		end 
		for i,v in ipairs(freeFriends) do 
			for k,m in ipairs(freeEnemies) do 
				local chance = love.math.random(1,20)
				if chance == 1 then 
					setTask(v,nil,"guard")
				else 
					setTask(v,target or m,"attack")
				end 
			end 
		end 
		print("--AI FINISH--")
	end 


function battleNpc:death()
	local clamp
	if math.floor(self.hp) == math.floor(self.aHp) then 
		clamp = true
	end 
	if clamp then 
		for i,v in ipairs(self.states) do
			if v.name == "death" then
				self.state = v.name
				clamp = false
				v.anim:setMode("once")
				v.anim:play()
				if v.anim.position >= #v.anim.frames  then
					clamp = true
				end
			end
		end
	end 
	if clamp then
		local a 
		if self.OnDeath then a = self.OnDeath(self) end
		if not a then
			self.dead = true
			if self.button and not self.button._DontTouch then 
				self.button:remove()
			end 
		end
	end
	return self.dead
end
function battleNpc:update(dt)
	if not self.dead then
		if self.stunned and self.button then 
			self.button.permaInactive = true 
		end 
		if self.dead then 
			self.StopEffect = true
		end 
		if self.particles then self.particles:update(dt) end 
		if self.shake then 
			local pr = 100-findPrecent(self.aHp,self.hp)
			local rang = math.min(math.max(pr,3),20)
			local oldx = self.x
			Timer.tween(0.1,self,{x = oldx+rang },nil,function()
				Timer.tween(0.1,self,{x = oldx-rang },nil,function()
					Timer.tween(0.1,self,{x = oldx },nil)
				end)
			end)
			self.shake = nil 
		elseif self.stiches then 
			self._tAnim:update(dt)
		end 
		if self.effect ~= self.OldEffect then 
			if self.effect then 
				self.StopEffect = false
				self.drawEffect = createParticles(self,self.effect)
			else
				self.StopEffect = true
			end 
			self.OldEffect = self.effect
		end 
		if self.hp < 0 then
			self.hp = 0
		end
		if self.hp ~= self.oldHp then
			Timer.tween(1,self,{aHp = self.hp})
			self.oldHp = self.hp
		end 
		if self.mp ~= self.oldMp then
			Timer.tween(1,self,{aMp = self.mp})
			self.oldMp = self.mp
		end 
		if self.OnUpdate then
			self.OnUpdate(dt,self)
		end
		if self.hp <= 0 then
			self:death()
		elseif self.hp >= self.maxHp then
			self.hp = self.maxHp
		end
		if self.animation then
			self.animation:update(dt)
		elseif not self.Pause then 
			for i,v in ipairs(self.states) do
				if v.name == self.state then
					v.anim:update(dt)
					if self.attacking then
						if v.anim.position == #v.anim.frames-1 then
							self.damageDealt,self.modif = self.mob:applyDamage(self.atk,self.attacktyp,self.adStack)
							self.attacktyp = nil
							self.mob = nil
							self.adStack = nil
							self.attacking = false
						end
					elseif self.casting then
						if v.anim.position >= #v.anim.frames-1 then
							self.OnCast()
							self.casting = nil 
						end 
					end
				end
			end
		end
	end
end
return battleNpc