local a = {}
local meta = {__index = a}
function a:new(ui,x,y,cards,victorPrev,victorNow,defeatedOld,defeatedNew,mode,OnDonePress,E_Mode)
	Timer.clear(dt)
-- Monster resolution is about 94 x 94 pixels wide so we gota make buttons for Each
-- However this may change so I will put them down as constants, to later transform into variables by the use of main.lua
	local tweenGroup = Flux.group()
	self.tweenGroup = tweenGroup
	local deathMsg = {
		"Devastating defeat:",
		"Dust to dust:",
		"Nothing left to bury:",
		"Annihilated:",
		"Left for the crows:",
	}
	local suicideMsg = {
		"A worthy sacrifice.",
		"One to save the many.",
		"May he find peace in death.",
		"A cost of a life to further another.",
		"Loyal till the end.",
		"Victim of total war.",
	}
	local woundedMsg = {
		"Sustained ",
		"A crippling hit of ",
		"Lucky to be alive, hit with ",
		"Near death experience of ",
	}
	local none = {
		"Like new.",
		"Not a scratch!",
		"Untouched.",
	}
	local self = {
		x = x,
		y = y,
		deathMsg = deathMsg,
		suicideMsg = suicideMsg,
		woundedMsg = woundedMsg,
		none = none,
		ui = ui,
		image = love.graphics.newImage(love.graphics.newScreenshot()),
		tweens = {},
		OnDonePress = OnDonePress
	}
	setmetatable(self,meta)
	self.theDead = {} 
	local buttons = {}
	self.buttons = buttons
	local once
	self.OnComplete = function()
		if not once then 
			local theDead = {} 
			for i,v in ipairs(buttons) do 
				v:setInactive(false)
			end 
			local a = self.a
			Timer.tween(1,a,{ActualEssence = a.essence},"in-quad")
			local function drawDead(tables,buttons,no)
				for i,v in ipairs(tables) do 
					local i = (no or 0) + i 
					local b = buttons[i]
					local nused = 0
					if v.dead or math.abs(v.hp) == 0 then
						local wi,hi = 94,94
						local line1 = {
							x = 0,
							y = 0,
							
							x2 = 0, 
							y2 = 0 ,
							
							ex = wi ,
							ey = hi ,
						} 
						local line2 = {
							x = wi ,
							y = 0 ,
							
							x2 = wi,
							y2 = 0 ,
							
							ex = 0,
							ey = hi,
						} 
						nused = nused + 0.6
						table.insert(theDead,{src = v,x = -100,y=-100})
						local v = b
						local no = #theDead
						local x,y = v.colision:center()
						Timer.add(nused, function() 
							tweenGroup:to(line1,0.6,{x2 = line1.ex,y2 = line1.ey}):after(line2,0.6,{x2 = line2.ex,y2 = line2.ey})
						end)
						local oldDraw = b.OnDraw
						b.OnDraw = function()
							local lw = love.graphics.getLineWidth()
							oldDraw()
							love.graphics.setLineWidth(10)
							setColor(colors.red)
							local l = line1
							local ox,oy = b.colision:bbox()
							local x,y = l.x,l.y
							local x2,y2 = l.x2,l.y2
							love.graphics.line(ox + x,oy + y,ox + x2,oy + y2)
							l = line2 
							local x,y = l.x,l.y
							local x2,y2 = l.x2,l.y2
							love.graphics.line(ox + x,oy + y,ox + x2,oy + y2)
							love.graphics.setLineWidth(lw)
						end
					end 
				end 
			end
			self.theDead = theDead
			drawDead(victorNow,buttons)
			drawDead(defeatedNew,buttons,#victorNow)
			once = true
		end
	end 
	
	
	
	local function tween1(oncomplete)
		local no = 0
		local w,h = love.graphics.getDimensions()
		local square = {alpha = 0}
		tweenGroup:to(square,4,{alpha = 255})
		:oncomplete(function() 
			if oncomplete then 
				oncomplete()
			end 
		end)
		self.OnDraw = function()
			love.graphics.rectangle("fill",0,0,w,h)
		end 
	end 
	
	
	local function tween(x,y,square,squares,size,oncomplete)
		local no = 0 
		tweenGroup:to(square,1,{x = self.x, y = self.y,alpha = 0})
		:delay(math.random(200,2000)/200)
		:oncomplete(function() 
			if oncomplete then 
				no = no + 1
				if no == #squares then 
					oncomplete()
				end
			end 
		end)
	end 
	self:addTween(tween)
	local function tween(x,y,square,squares,size,oncomplete)
		local no = 0 
		tweenGroup:to(square,1,{x = x + math.random(0,200),y = y + 2*size - love.graphics.getHeight() - 100})
		:ease("sinein")
		:delay(y/5)
		:oncomplete(function() 
			if oncomplete then 
				no = no + 1
				if no == #squares then 
					oncomplete()
				end
			end 
		end)
	end
	self:addTween(tween)
	local function tween(x,y,square,squares,size,oncomplete)
		local no = 0 
		tweenGroup:to(square,1,{y = y + 2*size - love.graphics.getHeight() - 100})
		:delay(math.cos(x^2)/2)
		:ease("cubicinout")
		:oncomplete(function() 
			if oncomplete then 
				no = no + 1
				if no == #squares then 
					oncomplete()
				end
			end 
		end)
	end
	self:addTween(tween)
	local function tween(x,y,square,squares,size,oncomplete)
		local no = 0 
		tweenGroup:to(square,1,{y = y + 2*size - love.graphics.getHeight() - 100})
		:delay(math.cos(x^2)/2)
		:ease("cubicinout")
		:oncomplete(function() 
			if oncomplete then 
				no = no + 1
				if no == #squares then 
					oncomplete()
				end
			end 
		end)
	end
	self:addTween(tween)
	local function deathTween(x,y,square,squares,size,oncomplete)
		local no = 0 
		tweenGroup:to(square,6,{x = math.random(-love.graphics.getWidth(),love.graphics.getWidth()) ,y = math.random(-love.graphics.getHeight(),-100)})
		:delay(math.sqrt(math.abs(x^2 + y^2))/8)
		:ease("expoout")
		:oncomplete(function() 
			if oncomplete then 
				no = no + 1
				if no == #squares then 
					oncomplete()
				end
			end 
		end)
	end
	if not mode then mode = love.math.random(1,#self.tweens) end 
	local function fillWithSquares(size,tween,oncomplete) -- Tween is a function
		local squares = {}
		local squareNoW = (love.graphics.getWidth())/size 
		local squareNoH = (love.graphics.getHeight())/size
		self.squareNoH = squareNoH
		self.squareNoW = squareNoW
		local posx,posy = 0,0
		local no = 0
		for x = 0,squareNoW do
			for y = 0,squareNoH do 
				local square = {x = x * size + posx,y = y * size + posy,size = size,alpha = 255}
				table.insert(squares,square)
				local posx,posy = x * size + posx,y * size + posy
				tween(x,y,square,squares,size,oncomplete)
			end 
		end 
		self.squares = squares
	end  
	
	if type(mode) == "number" then 
		self:parse(victorPrev,victorNow,defeatedOld,defeatedNew,cards)
		for i,v in ipairs(self.tweens) do
			if mode == i then
				fillWithSquares(10,v,oncomplet)
			end 
		end
	elseif mode then 
		self.dontDraw = true
		fillWithSquares(10,deathTween)
		self.OnComplete = function()
			local placeHolder = {
				a = 0, 
				a2 = 0
			}
			local anim -- Link to animation
			local finished 
			Timer.tween(1,placeHolder,{a = 255},nil,function() Timer.tween(2,placeHolder,{a2 = 255},nil,function()  end) end)
			local bFont = love.graphics.setNewFont(85)
			self.OnDraw = function()
				local width,height = love.graphics.getDimensions()
				local olf = love.graphics.getFont()
				
				local r,g,b = unpack(colors.red)
				love.graphics.setColor(r,g,b,placeHolder.a)
				local w,h = bFont:getWidth("Defeat"),bFont:getHeight("Defeat")
				love.graphics.print("Defeat",45 ,height - (h + 450*py))
				local font = love.graphics.setNewFont(15)
				local h = font:getHeight()
				
				-- anim:draw(width/2,height/2)
				--Once she shoots herself in the head FORCE quit the game
				love.graphics.setColor(r,g,b,placeHolder.a2)
				love.graphics.print("Death isn't the real punishment.\n The real punishment is the duty to begin again, to suffer through what you have already done.",10,height -(h*2 + 10))
				love.graphics.setColor(colors.white)
				
				love.graphics.setFont(olf)
			end 
			self.OnUpdate = function(dt)
				-- anim:update(dt)
				-- if anim.position >= #anim.frames-1 then
					-- love.event.push("quit")
				-- end
			end 
			self.OnComplete = nil
		end
	end 
	self.ui = ui 
	return self
end 
function a:addTween(tween)
	table.insert(self.tweens,tween)
end 
function a:parse(victorPrev,victorNow,defeatedOld,defeatedNew,usedCards)
	

	local deathMsg   = 	self.deathMsg
	local suicideMsg =  self.suicideMsg
	local woundedMsg =  self.woundedMsg
	local none 		 =  self.none
	local ui 		 = 	self.ui
	

	self.parsed = true 
	self.team1 = victorPrev
	self.team2 = defeatedOld
	local expValue = 0 
	for i,v in ipairs(self.team2) do 
		local x = v.level 
		expValue = expValue + math.log(x/200) + 8 + (x^2/200)
	end 
	for i,v in ipairs(self.team1) do 
		local prc = math.max(10,v.hp/(v.maxHp/100)) 
		print(prc,expValue)
		v:addExp(math.min(5,((expValue/5)/prc)))
	end 
	
	local function sortVictims(old,new)
		for i,v in ipairs(old) do 
			local newMob = new[i]
			if newMob then 
				if newMob.dead then
					local damage = newMob.damageTaken
					v.result = { 
						msg = deathMsg[math.random(1,#deathMsg)].." taken "..damage.." damage.",
						damage = damage,
					}
				elseif v.suicide then 
					v.result = {
						msg = suicideMsg[math.random(1,#suicideMsg)],
					}
				elseif v.hp ~= newMob.hp and v.info.level == newMob.info.level then 
					local damage = newMob.damageTaken
					v.result = {
						msg = woundedMsg[math.random(1,#woundedMsg)]..damage.." damage",
					}
				elseif v.hp == newMob.hp then 
					v.result = {msg = none[math.random(1,#none)]}
				elseif v.info.level ~= newMob.info.level then 
					v.result = {msg = "A fruitful endeavour"}
					v.levelUP = true 
				end
				v.hpChange = newMob.hp - v.hp 
				v.mpChange = newMob.mp - v.mp
				v.expChange = findDistance(newMob.exp,newMob.info.exp) 
				else 
					v.mpChange = 0 
					v.hpChange = 0 
					v.result = {msg = "Lost in action"}
			end
		end 
	end 
	
	sortVictims(victorPrev,victorNow)
	sortVictims(defeatedOld,defeatedNew)
	
	
	local w,h = 94,94 -- w,h = standardGhostWidth,standardGhostHeight
	local gapx = 5
	local gapy = 5
	local fW,fH = (w+gapx)*5,(h+gapy*2)
	local bGapY = 50
	local x = self.x
	local y = self.y
	
	local totalW,totalH = fW,(fH*2 + bGapY)
	
	x = x
	y = y - totalH/2
	local s = 0.35
	local gap = 7
	local cw,ch = (gap + 180*s),267*s
	local f_w = cw + 10 
	local sw,sh = love.graphics.getDimensions()
	local cards = ui:addFrame(nil,sw - f_w,gap*5,f_w,ch*5)
	local t = "Used Cards"
	local fow,foh = font:getWidth(t),font:getHeight()
	cards.OnDraw = function()	
		local x,y = cards.colision:bbox()
		love.graphics.print(t,x - fow/2,y - foh)
	end 
	cards.y = sh/2 
	cards.dontDrawSelf = true 
	for i=1,5 do 
		local card = ui:addButton(cards,0,(ch + gap)*(i-1),cw,ch + 10)
		card.dontDraw = true 
		card._s = s 
		card.OnDraw = function()
			local x,y = card.colision:center()
			local s = card._s
			if usedCards[i] then 
				usedCards[i]:draw(x,y,0,s,s)
			else
				local w,h = GrayCard:getDimensions()
				love.graphics.draw(GrayCard,x,y,0,s,s,w/2,h/2)
				card.inactive = true 
			end 
		end 
		local tween = false
		local oldzmap = card.zmap 
		card.OnFocus = function()
			if tween then Timer.cancel(tween) end 
			tween = Timer.tween(0.5,card,{_s = 1})
			card.zmap = card.zmap + 1 
		end 
		card.OnFocusLost = function()
			if tween then Timer.cancel(tween) end 
			tween = Timer.tween(0.5,card,{_s = s})
			card.zmap = oldzmap 
		end 
	end 
	local Victors = ui:addFrame(nil,x,y,fW,fH)
	Victors.dontDrawSelf = true 
	local Defeated = ui:addFrame(nil,x,y + fH + bGapY,fW,fH)
	Defeated.dontDrawSelf = true 
		local function sortPos(tables,frame,onDraw)
		if frame.que then 
			for i,v in ipairs(frame.que) do 
				v:remove()
			end
		end 
		local maxNoX = 5
		local maxNoY = 0 
		local maxNumber = maxNoX
		local ox,oy

			local additional = 0 
			local cardNo = maxNumber - #tables
			if cardNo > 0 then 
				additional = maxNumber - #tables
			end 
			local freeSpace = frame.width - ((maxNumber - additional)*(w + gapx))
			ox = freeSpace/2 
			
		for i,v in ipairs(tables) do 
			if i <= (maxNumber) then 
				local gapx = gapx
				local gapy2 = 0 
				local w,h = w,h
				local lastx,lasty = (ox or 0) - gapx/2, (oy or 0 ) + gapy
				if i % maxNoX == 0 then 
					gapy = gapy
				end 
				if tables[i - 1] then 
					local b = tables[i - 1]
					lastx = b.lastx
					lasty = b.lasty
				end 
				local x = lastx  + gapx
				local y = lasty  + gapy2
				
				local b = ui:addButton(frame,x,y,w,h)
				local a = ui:addTooltip(b,v.result.msg)
				local wi,hi = v:getDimensions()
				
				a:setCenter(b.x,b.y - hi/2,b.x,b.y - hi/2)
				b.dontDrawC  = true 
				b.dontDrawB  = true 
				b.dontDrawActive = true
				table.insert(self.buttons,b)
				v.lastx = x + w
				v.lasty = y
				b.item = v
				v.x = b.x - (wi/2)
				v.y = b.y - (hi/2)
				v:setButton(b)
				if v.mirror then 
					v.x = (b.x - (wi/2)) + wi
				end 
				b.mob = v 
				v.hp = 100 
				v.state = "idle"
				b.OnUpdate = function(dt) v:update(dt) if self.tooltip then self.tooltip:update(dt) end end 
				b.OnDraw = function() onDraw(v) if self.tooltip then self.tooltip:draw() end 
				end 
			end
		end
	end 
	
	self.width = fW + 5 -- 5 for gap
	self.height = fH + bGapY
	
	
	local font = love.graphics.newFont(uniBody,14)
	local function draw(item)
		local msg = "Error 404"
		if item.result then 
			msg = item.result.msg
		end
		item:draw(true)
		love.graphics.setColor(colors.red)
		local f = love.graphics.getFont()
		local w,h = f:getWidth(item.hpChange.." hp"), f:getHeight()
		local wi,hi = item:getDimensions()
		local x = item.x + wi/2 
		local y = item.y + hi
		if item.mirror then 
			x = item.x - wi/2
		end 
		love.graphics.print(item.hpChange.." hp",x - w/2,y)
		

		love.graphics.setColor(colors.blue)
		local exc = item:retExpChange()
		local w = f:getWidth(exc.." exp")
		love.graphics.print(exc.." exp",x - w/2,y + h)
		local h = f:getHeight()
		local lf = love.graphics.getFont()
		if item.levelUP then 
			local t = "Level Up"
			local w = f:getWidth(t)
			setColor(colors.yellow)
			love.graphics.setFont(font)
			
			love.graphics.print(t,x - w/2,y - (94+h))
			setColor()
			
			love.graphics.setFont(lf)
		end 
		love.graphics.setColor(colors.white)
	end 
	sortPos(victorPrev,Victors,draw)
	sortPos(defeatedOld,Defeated,draw)
	
		for i,v in ipairs(self.buttons) do 
			v:setInactive(true)
		end 
		local a = {
			essence = 0 ,
			ActualEssence = 0,
		}
		self.a = a

	for i,v in ipairs(defeatedNew) do 
		if v.dead then 
			a.essence = a.essence + (v.level*v.rarity)
		end 
	end 
	local done = ui:addButton(nil,love.graphics.getWidth()/2,self.x + self.height/2,90*py,25*px)
	done.color = colors.white
	done:setText("Done")
	done.OnClick = function() self.OnDonePress()
		if not E_Mode then 
			for i,v in ipairs(victorNow) do 
				local hp = v.hp
				if not playersMobs[i] then 
					local mn = victorNow[i].name
					playersMobs[i] = loadGhost:new(mn)
				else 
					playersMobs[i].info.hp = hp
				end
				
				
			end 
			playerData.essence = playerData.essence + a.essence
		end	
			for i,v in ipairs(victorNow) do 
				if v.dead or math.abs(v.hp) == 0 then 
					table.remove(playersMobs,i)
				end 
			end
		end
	local img = icons["essence_small"]
	local w,h = img:getDimensions()
	done.OnDraw = function()
		local txt = "+"..math.floor(self.a.ActualEssence)
		local x,y = done.colision:bbox()
		local font = ui:getFont()
		local w = font:getWidth(txt)
		local h = font:getHeight()
		x = x + (done.width/2 - (w/2 + img:getWidth()/2))	
		y = y - (h + img:getHeight())
		setColor(colors.green)
		love.graphics.print(txt,x,y)
		setColor()
		love.graphics.draw(img,x + w + img:getWidth()/2,y)		
	end
end 
function a:update(dt)
	if self.OnUpdate then self.OnUpdate(dt) end
	if #self.tweenGroup == 0 then 
		if self.OnComplete then self.OnComplete() end
	end 
	Timer.update(dt)
	self.tweenGroup:update(dt)
end 

function a:draw()
	if not self.dontDraw then 
		love.graphics.setColor(colors.gray)
		love.graphics.rectangle("fill",0,0,love.graphics.getWidth(),love.graphics.getHeight())
		love.graphics.setColor(colors.yellow)
		local propx = self.width/800 
		local propy = self.height/600
		local prop = (propx + propy)/2 
		local olf = love.graphics.getFont()
		local f = love.graphics.setNewFont(uniBody,90*prop)
		local w = f:getWidth("Victory!")
		local h = f:getHeight()
		love.graphics.print("Victory!",love.graphics.getWidth()/2 - w/2,15)
		love.graphics.setColor(colors.white)
		love.graphics.setFont(olf)
	end 
	self.ui:draw()
	if self.OnDraw then 
		self.OnDraw()
	end 

	love.graphics.setStencil(function()
		for i,v in ipairs(self.squares or {}) do 
			if v.alpha ~= 0 then 
				love.graphics.rectangle("fill",v.x,v.y,v.size,v.size)
			end
		end 
	end)
	love.graphics.draw(self.image,0,0)
	love.graphics.setStencil()
end 

function a:remove()
	Timer.clear()
end 
return a