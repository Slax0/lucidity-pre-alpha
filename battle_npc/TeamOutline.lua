local a = {}
local meta = {__index = a}
function a:update(dt)
	if self.Group and not self.dontUpdate then self.Group:update(dt) end 
end

function a:draw() 
	if not self.dontDraw then 
		self.accum = self.accum + 1
		if self.accum > 25 then
			if self.rand == 2 then
				self.rand = 3
			else 
				self.rand = 2
			end  
			self.accum = 0
		end 
		for i,v in ipairs(self.rectangles) do 
			local rads = v.size
			local r,g,b = 84,77,67
			local rand = self.rand
			local rat = v.size/self.size
			if i %rand == 0 then 
				r,g,b = 178,0,255
			end 
			if rat ~= 1 then 
				r,g,b = 255,0,0
			end 
			love.graphics.setColor(r,g,b)
			love.graphics.circle("line",v.x - rads/2,v.y - rads/2,rads/4)
			love.graphics.setColor(colors.white)
		end 
	end 
end  
function a:remove()
	self.Group = nil
	for i,v in ipairs(self) do  -- NECESARY this might look stupid but it actualy does a good job of clearing memory!
		self[i] = nil 
	end 
	setmetatable(self,{})
	collectgarbage()
	return nil
end 
function a:pause()
	if not self.dontDraw then 
		self.dontUpdate = true
		self.dontDraw = true 
		self.Group = Flux.group()
		for i,v in ipairs(self.rectangles) do 
			v.size = self.size
			v.tween = nil
		end 
		self.rectangles = {}
		self:createTweens()
	end 
end 
function a:reset()
	self:pause()
end 
function a:setPositive(positive)
	if self.positive ~= positive then 
		self.Group = nil
		self.Group = Flux.group()
		self:setPoints(self.ax,self.ay,self.bx,self.by,positive)
	end 
end  
function a:getArea()
	return {self.ax,self.ay,self.bx,self.by}
end 
function a:createTweens()
	local function squareTween(square,no,size,maxNo,g)
			local maxNo = math.floor(maxNo)
			square.tween = self.Group:to(square,0.22,{size = size}):delay(0.03*no)
			square.tween = square.tween:after(square,0.22, { size = size*2 })
			square.tween = square.tween:after(square,0.22,{size = size}):oncomplete(function()	
			if not team1Finished then 
				table.insert(g,{square,no,size,maxNo})
				if #g >= maxNo then
					local a = {}
					for i,v in ipairs(g) do 
						squareTween(v[1],v[2],v[3],v[4],a,v[5])
					end 
				end
			end 					
		end)
	end
	local g = {}
	for i,v in ipairs(self.rectangles) do  
		if not v.tween then  
			squareTween(self.rectangles[i],i,v.size,#self.rectangles,g,team1Finished)
		end 
	end 
end 
function a:setPoints(ax,ay,bx,by,positive)
	self:pause()
	self.ax = ax 
	self.ay = ay 
	self.bx = bx 
	self.by = by
	self.dontDraw = false 
	self.dontUpdate = false
	self.positive = positive
 
	local g = {}
	local w = math.max(ax,bx) - math.min(bx,ax)
	local h = math.max(ay,by) - math.min(ay,by)
	local rectangles = self.rectangles
	local size = math.max(5*px,5*py)
	local gap = 3
	local numReqx = w/(size + gap)
	local numReqy = h/(size + gap)
	local no = 0 
	local w,h = size,size
	self.size = w 
	for x = 1,math.floor(numReqx) do 
		no = no + 1
		local a,y = ax - (w + gap)*(x),ay 
		if not positive then 
			a = ax + (w + gap)*(x)
		end 
		x = a 
		if not rectangles[no] then rectangles[no] = {x = x,y = y,size = w}  else rectangles[no].x = x rectangles[no].y = y end
	end
	
	for y = 1, math.floor(numReqy) do 
		no = no + 1
		local x,y = bx,ay + (h + gap)*y
		
		if not rectangles[no] then rectangles[no] = {x = x,y = y,size = w}  else rectangles[no].x = x rectangles[no].y = y end
	end

	
	for x = math.floor(numReqx), 1, -1 do 
		no = no + 1
		local a,y = ax - (w + gap)*(x),by
		if not positive then 
			a = ax  + (w + gap)*(x)
		end 
		x = a 
		if not rectangles[no] then
		rectangles[no] = {x = x,y = y,size = w}
		
		else rectangles[no].x = x rectangles[no].y = y end
	end
	local ino = 0

	for y = math.floor(numReqy + 1),1, -1 do 
		no = no + 1
		local x,y = ax, (ay + (h + gap)*(y-1))
		
		if not rectangles[no] then rectangles[no] = {x = x,y = y,size = w}  else rectangles[no].x = x rectangles[no].y = y end
	end 
	self:createTweens()
end 
function a:init(ui,ax,ay,bx,by,positive)
	self = {ui = ui,ax = ax,ay = ay,bx = bx,by = by,positive = positive,accum = 0,rand = 2}
	setmetatable(self,meta)
	self.rectangles = {}
	self.Group = Flux.group()
	return self
end 
return a