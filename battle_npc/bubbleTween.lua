local bubbles2 = {}
bubbles2.__index = bubbles
function bubbles2:new(w,h)
	local bubbles = {}
	local w = w
	for x = 1,w,20 do 
		for y = 1,h,4 do 
			local size = love.math.random(0.2,2)
			local color = {255,255,255,160}
			local bubble = {x = 0,y = y,ox = 0,oy = y,osize = size,size = size,color = color}
			table.insert(bubbles,bubble)
		end 
	end 
		local function contEffect(circle)
			circle.x = 0
			circle.y = circle.oy
			circle.size = circle.osize
			local function tween(circle)
				if not self.StopEffect then 
					local distance = w
					local speed = (w/10)/circle.size
					local delay = love.math.random(15,50)
					circle.tween = Timer.tween(delay,circle,{x = distance},nil,function() contEffect(circle) end)
				else 
					for i,v in ipairs(bubbles) do 
						if v == circle then 
							table.remove(bubbles,i)
						end  
					end 
				end
			end 
			tween(circle)
		end  
		for i,v in ipairs(bubbles) do 
			local delay = love.math.random(5,10)
			v.tween = Timer.tween(delay,v,{x = distance},nil,function() contEffect(v) end)
		end 
		self.bubbles = bubbles
end 
function bubbles2:draw(x,y)
	for i,v in ipairs(self.bubbles) do
		love.graphics.setColor(v.color)
		love.graphics.circle("line",x + v.x,y + v.y,v.size)
	end 
			love.graphics.setColor(colors.white)
end 

function bubbles2:setColor(color)
	
end 
return bubbles2