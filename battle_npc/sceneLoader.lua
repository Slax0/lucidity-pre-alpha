local sceneLoader = {}
local _PACKAGE = getPackage(...)
function sceneLoader.load(name,dir)
	local parentDir = dir or _PACKAGE.."/scenes"
	local files = love.filesystem.getDirectoryItems(parentDir)
	local scene = false
	if not mode then 
		mode = "write"
	end 
	for i,v in pairs(files) do 
	local v = string.lower(v)
	local vName = string.lower(name)
		if string.find(v,vName) then
			local path = parentDir.."/"..v
			local chunk,err = love.filesystem.load(path.."/load.lua")
			if not chunk then 
				return err
			end 
			scene = chunk(path)
			scene.name = name
		end  
	end
	assert(scene,"Couldn't find scene by the name of: "..name) 
	setmetatable(scene,{__index = sceneLoader})
	return scene
end 
function sceneLoader:enter(cam)
	self.cam = cam 
	if self.enter then self.enter() end
end 
function sceneLoader:drawFore()
	if self.drawForeGround then self.drawForeGround() end
end 
function sceneLoader:drawBack()
	if self.drawBackGround then self.drawBackGround() end 
end 
function sceneLoader:update(dt)
	if self.OnUpdate then self.OnUpdate(dt) end
end 