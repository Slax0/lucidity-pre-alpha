local console = {}
console.__index = console

function console:attach(stack)
	console.stack = stack
	self.last = {} 
	
	local shapeCol = function(...)
		self:HCol(...)
	end 
	local shapeStop = function(...)
		self:HStop(...)
	end 
	
	self.realView = true
	
	self.moduleMan = moduleMan
	self.collider = HC(200,shapeCol,shapeStop)
	self.ui = ui_package:init(self.collider)
	
	
	
	self.last_index = 0 
	self.primed = false 
	
	function self:HStop(...)
		self.ui:Hstop(...)
	end 	
	function self:HCol(...)
		self.ui:Hcol(...)
	end 	
	return self
end 
local function getHeight(t,h)
	local no = 0 
	for word in t:gmatch("\n") do 
		no = no +1 
	end 
	return h*no
end 
function console:listener(key) 
	self.ui:keypressed(key)
	if key == "`" then 
		if self.Active then 
			local ui = self.ui
			if self.textinputs then 
				if ui.textInputObject then 
					if ui.textInputObject == self.textinputs then ui.textInputObject = nil end 
				end 
				self.textinputs:remove()
				self.dump:remove()
				self.slider:remove()
			end 
			self.Active = false 
		else 
			local ui = self.ui
			if self.textinputs then 
				if ui.textInputObject then 
					if ui.textInputObject == self.textinputs then ui.textInputObject = nil end 
				end 
				self.textinputs:remove()
			end 
			local font = love.graphics.getFont()
			local h = font:getHeight()*8
			local w = love.graphics.getWidth() - 10
			local h2 = ui:getFont():getHeight()
			self.mainFrame = ui:addFrame(nil,w/2 +5,h/2,w,h + font:getHeight())
			self.mainFrame.OnFocus = function()
				self.slider:setFocus(true)
			end
			self.mainFrame.OnFocusLost = function()

				self.slider:setFocus()
			end
			self.mainFrame.OnDraw = function()
				
				local lg = love.graphics
				local font = lg.getFont()
				local r,g,b = unpack(colors.gray)
					local h = font:getHeight()*8
					setColor()
					local no = #self.stack
					local pos = math.ceil(math.ceil(no/9)-self.slider:getValue()) 
					local o = 0 
					local text = {} 
					for i=no-(7*(pos+1)),no - (7*(pos)) do
						if getHeight(table.concat(text),font:getHeight()) < h then 
							local t = self.stack[i]
							if t then 
								table.insert(text,t.."\n")
								o = o + 1 
							end
						end 
					end 
					lg.print(table.concat(text),10)
				setColor()
			end
			local r,g,b = unpack(self.mainFrame.color)
			self.mainFrame.color = {r,g,b,200}
			local slider = ui:addSlider(self.mainFrame,self.mainFrame.width -20,25,5,self.mainFrame.height-35,"vert")
			self.slider = slider 
			self.textinputs = ui:addTextinput(nil,5 + w/2,h + font:getHeight() + h2/2 -3,w,h2) 
			local w = ui:getFont():getWidth("Dump")
			local h = h2*2
			self.dump = ui:addButton(nil,5 + w/2,self.textinputs.y + h-5,w,h)
			self.dump:setText("Dump")
			self.dump.OnClick = function()
				saveLog()
				love.system.openURL(love.filesystem.getRealDirectory("LOG"))
			end
			
			local no = #self.stack/8
			self.slider:setRange(0,no,true)
			self.slider.stepSize = 1 
			if no ~= self.oldNo then 
				self.slider:setValue(no)
				self.oldNo = no 
			end 
			if self.oldNo then 
				self.slider:setValue(self.oldNo)
			end 
			self.Active = true 
		end
	elseif key == "return" and self.textinputs and self.Active then 
		local t = self.textinputs:getText()
		if t ~= "" then 
			local fn,err = loadstring(t)
			if not fn then 
				print(err)
			else 
				print(fn())
				print("U_EXEC: "..t)
			end
			table.insert(self.last,t)
			self.last_index = #self.last
			self.mainFrame:setText("")
		end 
	elseif key == "up" and self.Active then 
		self.last_index = math.max(1,self.last_index - 1)
		self.mainFrame:setText(self.last[self.last_index])
	elseif key == "down" and self.Active then 
		self.last_index = math.min(#self.last,self.last_index + 1)
		self.mainFrame:setText(self.last[self.last_index])
	end
end 
function console:draw()
	if self.Active then
		self.ui:draw()
	end 
end 
function console:update(dt)
	if self.Active then 
		self.collider:update(dt)
		self.ui:update(dt)
		self.ui:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
		self.ui:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
		local no = #self.stack/8
		self.slider:setRange(0,no,true)
	end 
end 
function console:mousepressed(...)
	if self.Active then self.ui:mousepressedList(...) end 
end
function console:textinput(...)
	self.ui:textinput(...)
end
return console