local intro = {}
local stage = 1
local stages = {} 
misc = {
	showFPS = false,
	scaleFactor = 1,
	cameraSlide = true,
	cameraSlideB = true,
	battleCamera = true,
	mouseLock = false,
	trapMouse = false,
	textSpeed = 1,
	sfvol = 55,
	bgmvol = 55,
	voicevol = 55,
	mastervol = 100,
	seenIntro = false,
}
function intro:enter()
	local finish_fn = function()
		misc.seenIntro = true
		saveKeyOptions(SaveName,keys,alt_keys,misc,video) 
		Gamestate.switch(mainMenu)
		current_gamestate = "menu"		
	end
	self.tweenGroup = Flux.group()
	self.current_data = {
		text = "When you see this animation the game is saving, please do not turn off the computer",
		fadeout = 255,
	} 
	local tween = self.tweenGroup
	local stage2 = function(finish)
		stage = 2 
		local cd = self.current_data
		cd.text = {} 
		cd.text[1] = {t = "Please note that this is the ALPHA release of Lucidity",c = 0}
		cd.text[2] = {t = "All that you see here is subject to change",c = 0}
		cd.text[3] = {t = "if you encounter any bugs please inform me",c = 0}
		cd.text[4] = {t = "~Thank you~",c = 0}
		for i,v in ipairs(cd.text) do 
			tween:to(v,i*2,{c = 255}):oncomplete(function()
				if i == 4 then 
					finish()
				end 
			end)
		end 
	end
	
	local circleSize = 0 
	local w,h = love.graphics.getDimensions()
	local data = {
		circleSize = 0,
		circleSize2 = 0,
		color = colors.yellow, 
		recNo = 0,
		delay = 20,
		pointNo = 0,
		old_pointNo = 0,
		crystals = {},
	}
	local rectangles = {} 
	local points = {} 
	local function addPoint()
		if math.floor(data.pointNo) ~= data.old_pointNo then 
			local point = {
				x = 0,y = 0, 
				size = 15,
				angle = 0,
				randr = 0,
				color = {178,0,255}
			} 
				point.oldx = point.x 
				point.oldy = point.y
				local r = (w + h)
				local a = love.math.random(0,math.pi*10)
				point.x,point.y = r*math.cos(a),r*math.sin(a) 
			local function tweenPoint(point)
				if math.round(data.delay,3) < 0.01 then
					
				else 		
				tween:to(point,data.delay,{x = w/2,y = h/2,size = 0}):oncomplete(function()
					local a = love.math.random(0,math.pi*10)
					point.x,point.y = r*math.cos(a),r*math.sin(a) 
					point.size = 15
					tweenPoint(point)
				end)
				end 
			end
			tweenPoint(point,data.delay)
			table.insert(points,point)
			data.old_pointNo = math.floor(data.pointNo)
		end 
	end 
	local function createRectangle()
		if math.floor(data.recNo) ~= data.old_recNo then 
			local a = love.math.random(0,math.pi*10)	
			local color = {
				math.random(-20,20),math.random(2,20),math.random(2,20),255*(data.recNo/100)
			}
			local rect = {
				size = 0,
				angle = a, 
				color = color,
				factor = data.circleSize/300,
			}
			Timer.tween(3,rect,{color = {color[1],color[2],color[3],255},size = math.random(1,data.circleSize/10),})
			table.insert(rectangles,rect)
			data.old_recNo = math.floor(data.recNo)
		end 
	end 
	local function rotatePoint(point,finish,delay)
		if not finish then 
			local t = tween:to(point,delay,{angle = math.rad(360),randr = math.random(190,210)}):ease("linear"):oncomplete(function()
				rotatePoint(point,finish,delay)
				point.angle = 0 
			end):onupdate(function()
				point.x = w/2 + point.randr*math.cos(point.angle)
				point.y = h/2 + point.randr*math.sin(point.angle)
			end)
			point.tween = t 
		end 
	end 
	stages[4] = {
		init = function()
			local delay = 6 
			stage = 4 
			tween:to(data,delay,{circleSize = 100,recNo = 100}):oncomplete(function()
				for i,v in pairs(rectangles) do 
					tween:to(v,i/3,{angle = 0,factor = 300/data.circleSize,size = 0}):ease("linear"):oncomplete(function()
						tween:to(data,0.3,{circleSize = data.circleSize + 1})
						table.remove(rectangles,i)
					end)
					Timer.tween(i/3,v,{color = {0,0,0,255}})
				end			
				Timer.tween(100/3,data,{color = {130,5,0}})
				local finish = false
				tween:to(data,100/3,{pointNo = 200,delay = 0}):oncomplete(function()
					local r = 200
					for i,v in ipairs(points) do 
						local a = math.pi*(i/20)
						v.x = w/2 
						v.y = h/2 
						tween:to(v,2,{x = w/2 + r*math.cos(a),y = h/2 + r*math.sin(a),size = 2}):oncomplete(function()
							v.randr = r
							v.angle = a 
							rotatePoint(v,finish,i/2)
						end)
					end
					Timer.tween(2,data,{circleSize = 10,color = {255,255,255}},nil,function()
						finish = true 
						local fin = function()
						local t = {
							"basic",
							"pink",
							"blue",
							"Inverted",
							"cyan",
							"silver",
							"green",
						}
						local done = false 
						data.crystalColor = {255,255,255,0}
						Timer.tween(6,data,{crystalColor = {255,255,255,255},circleSize = 0.000001,},nil,function()
							data.circleSize = 0 
							Timer.tween(6,data,{circleSize2 = (w+h)/2},nil,function()
									stage = 5
									stages[5].init()
							end)
						end)
						for i=1,7 do 
							local a = math.rad(45*(i-1)-45)
							local d = {
								x = w/2 + 200*math.cos(a),
								y = h/2 + 200*math.sin(a),
								oldx =  w/2 + 200*math.cos(a), 
								oldy =  h/2 + 200*math.sin(a),
								crystal = crystal[t[i]],
								color = {255,255,255,0}
							}
							table.insert(data.crystals,d)
							Timer.tween(6,d,{color = {255,255,255,255}})
						end 
							for i,v in ipairs(points) do 
								local a = 0
								local b = 200/8 
								local color = colors.orange
								if i < b then
									color = colors.red
									a = 45
								elseif i < b*2 then 
									color = colors.blue
									a = 45*2
								elseif i < b*3 then 
									color = colors.black
									a = 45*3
								elseif i < b*4 then 
									a = 45*4
									color = colors.cyan
								elseif i < b*5 then 
									a = 45*5
									color = colors.white
								elseif i < b*6 then
									a = 45*6
									color = colors.green
								end  
								a = math.rad(a-45)
								v.color[4] = 255
								tween:to(v,6,{angle = a}):onupdate(function()
									if v.tween then v.tween:stop() end
									v.x = w/2 + v.randr*math.cos(v.angle)
									v.y = h/2 + v.randr*math.sin(v.angle)
								end)
								Timer.tween(6,v,{color = {color[1],color[2],color[3],0}},nil,function()
								end)
							end 
						end
						Timer.add(3,fin)
					end)		
				end)
			end)
			Timer.tween(delay,data,{color = {236,116,19}})
		end,
		draw = function()
			setColor(55,55,55)
			love.graphics.circle("fill",w/2,h/2,data.circleSize2)
			for i,v in ipairs(rectangles) do 
				local r,g,b,a = unpack(data.color)
				local r2,g2,b2,a = unpack(v.color)
				r = r + r2
				g = g + g2
				b = b + b2 
				setColor(r,g,b,a)
				local r = data.circleSize/v.factor
				local a = v.angle
				local x,y = r * math.cos(a),r * math.sin(a)
				love.graphics.circle("fill",x + w/2,y + h/2,v.size) 
				setColor()
			end 
			if data.crystalColor then 
				setColor(data.crystalColor)
				local v = crystal["player"]
				v.anim:draw(w/2,h/2,0,1,1,v.w/2,v.h/2)
			end 
			for i,v in ipairs(points) do 
				setColor(v.color)
				love.graphics.rectangle("fill",v.x,v.y,v.size,v.size)
			end 
			for i,v in ipairs(data.crystals) do 
				setColor(v.color)
				local w,h = v.crystal.w,v.crystal.h
				v.crystal.anim:draw(v.x,v.y,0,1,1,w/2,h/2)
			end 
			setColor(data.color)
			love.graphics.circle("fill",w/2,h/2,data.circleSize)
		end,
		update = function(dt)
			Timer.update(dt)
			createRectangle()
			addPoint()
			for i,v in ipairs(data.crystals) do 
				v.crystal.anim:update(dt)
			end 
			if data.crystalColor then 
				crystal["player"].anim:update(dt)
			end 
		end,
	}
	stages[5] = {
		init = function()
			local function charCount(str)
				local i = 0
				for c in str:gmatch"." do
					i = i + 1
				end
				return i
			end
			local text = "Lucidity"
			local Width,Height = w,h 
			local centerPosx,centerPosy = Width/2,Height/4
			local centerPosx,centerPosy = Width/2,Height/4
			local fontS = love.graphics.newFont(uniBody,60*1)
			local gap = 15 
			local titleW = fontS:getWidth(text) + (gap * (charCount(text) - 1)) 
			local titleH = fontS:getHeight(text)

			local startX = centerPosx - titleW/2
			local startY = centerPosy - titleH/2
			
			local playerX,playerY
			local img = love.graphics.newImage("assets/float.png")
			local row = 2
			local column = 8 
			local w = img:getWidth()/column
			local h = img:getHeight()/row
			local s = 1
			local speed = 0.20
			local a = {} 
			a.paralaxFactor = 0.3
			a.anim = newAnimation(img,w,h,speed,1,11)
			a.x = Width/2 - w/2  
			a.y = startY + titleH + Height/17 
			a.ix = a.x 
			a.iy = a.y 
			playerX = a.ix + w/2
			playerY = a.iy + h*s
			a.scale = s
			local w = crystals["basic"]:getWidth()/15
			local i = 0 
			local gap = 25
			local leng = ((w/2 + gap)*7)
			for i,v in ipairs(data.crystals) do 
				i = i -1 
				local x = (playerX - leng/2 - 20) + (((w*a.scale)+ gap)*i) + v.crystal.w/2
				local y = (playerY + 30) + -6*(i - 3)^2 + v.crystal.h/2
				Timer.tween(3,v,{x =x,y=y})
				v.x = v.oldx 
				v.y = v.oldy
			end 
			data.player = a 
			data.colorF = 255 
			tween:to(data,4,{colorF = 0}):oncomplete(function()
				finish_fn()
			end)
		end ,
		draw = function()
			setColor(55,55,55)
			love.graphics.circle("fill",w/2,h/2,data.circleSize2)
			if data.crystalColor then 
				setColor(255,255,255,data.colorF)
				local v = crystal["player"]
				v.anim:draw(w/2,h/2,0,1,1,v.w/2,v.h/2)
			end 
			if data.player then 
				local a = data.player
				setColor(255,255,255,255-data.colorF)
				a.anim:draw(a.x,a.y)
			end 
			for i,v in ipairs(data.crystals) do 
				setColor(v.color)
				local w,h = v.crystal.w,v.crystal.h
				v.crystal.anim:draw(v.x,v.y,0,1,1,w/2,h/2)
			end 
		end ,
		update = function(dt)
			Timer.update(dt)
			for i,v in ipairs(data.crystals) do 
				v.crystal.anim:update(dt)
			end 
			if data.player then 
				local a = data.player
				a.anim:update(dt)
			end 
			if data.crystalColor then 
				crystal["player"].anim:update(dt)
			end 
		end ,
	}
	tween:to(self.current_data,5,{fadeout = 0}):oncomplete(function()
		stage2(stages[4].init)
	end)
	sav_tween.active = true 
end 
local w,h = love.graphics.getDimensions()
local font = love.graphics.newFont(16)
local gap = 40
function intro:pralaxPos(factor)
	local x = ((0) - love.graphics.getWidth()/2)*factor
	local y = ((0) - love.graphics.getHeight()/2)*factor
	return x,y
end
function intro:draw()
	local old = love.graphics.getFont()
	love.graphics.setFont(font)
	if stage == 1 then 
		sav_tween.transp = self.current_data.fadeout
		setColor(255,255,255,self.current_data.fadeout)
		local neww = font:getWidth(self.current_data.text)
		love.graphics.print(self.current_data.text,w/2 - neww/2,h/3)
		if sav_tween.active then 
			sav_tween:draw(w/2,h/2)
		end 
	elseif stage == 2 then 
		for i,v in ipairs(self.current_data.text) do 
			local neww = font:getWidth(v.t)
			setColor(255,255,255,v.c)
			love.graphics.print(v.t,w/2 - neww/2,h/2 + ((font:getHeight() + gap)*(i) - (((font:getHeight() + gap)*(6))/2)))
			setColor(255,255,255,0)
		end 
	end 
	local s = stages[stage]
	if s then 
		if s.draw then s.draw() end 
	end 
	love.graphics.setFont(old)
end 
function intro:update(dt)
	self.tweenGroup:update(dt)
	sav_tween:update(self.tweenGroup,false,dt)
	local s = stages[stage]
	if s then 
		if s.update then s.update(dt) end
	end 	
end 
return intro 