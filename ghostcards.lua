local function renderCard(ui,mob,Hide)
	if mob then 
		local m = mob.info
		local meta = mob.meta 
		local f = ui:getFont()
		local exp = mob.exp or m.exp 
		local items = {
			"Level: "..m.level,
			"EXP: "..exp.."/"..mob:getExpNeeded(),
			"HP: "..m.hp.."/"..m.maxHp,
			"MP: "..m.mp.."/"..m.maxMp,
			"ATK: "..m.atk,
			"DEF: "..m.def,
			"Luck: "..m.luck,		
		}
		local maxw = 0 
		local maxw2 = 0 
		local maxh = f:getHeight()*#items
		for i,v in ipairs(items) do 
			if f:getWidth(v) > maxw then 
				maxw = f:getWidth(v)
			end 		
			items[i] = v.."\n"
		end 
		for i,v in ipairs(m.skills) do 
			if f:getWidth(v.name) > maxw2 then 
				maxw2 = f:getWidth(v.name.."["..v.cost.."]")
			end 
		end 
		if maxw2 > 0 then 
			maxw2 = maxw2 + 20
		end 		
		maxw = maxw + 10
		local w,h = love.graphics.getDimensions()
		local nw,nh = 94 + math.max(94,maxw) + maxw2 + (10*3),math.max(120,maxh) + 70
		local frame = ui:addFrame(meta:getName(),w/2,h/2,nw,nh) 
		frame.dragEnabled = true 
		frame:addCloseButton(function()
			if frame.nframe then frame.nframe:remove() end 
		end)
		frame:setHeader(meta:getName())
		local _fw = ui:getFont():getWidth("Edit") + 20 
		local edit = ui:addButton(frame,(frame.width - 30)-_fw,0,_fw,20)
		edit:setText("Edit")
		edit.dontDrawB = true
		edit.dontDrawC = true 
		edit.dontDrawActive = true  
		local image = ui:addFrame(nil,5,25,94,94 + (font:getHeight() + 10),frame)
		local storyTooltip = image:addTooltip(meta:getStory())
		edit.OnClick = function()
			if not frame.nframe then 
				local nframe = ui:addFrame(nil,0,0,frame.width,200)
				nframe:setHeader("Editing: "..meta:getName())
				nframe.dragEnabled = true 
				nframe.OnUpdate = function()
					nframe:moveTo(frame.x,frame.y - frame.height)
				end 
				nframe.OnMove = function()
					frame:moveTo(nframe.x,nframe.y + (nframe.height + 20 ) )
				end 
				frame.nframe = nframe
				local nameinput = ui:addTextinput(nframe,20,25,frame.width-40,ui:getFont():getHeight() + 5)
				nameinput.OnChange = function()
					mob:setUsername(nameinput:getText())
					frame:setHeader(mob:getName())
				end 
				nameinput:setText(meta:getName())
				
				local frameinput = ui:addTextinput(nframe,20,ui:getFont():getHeight() + 5 + 30,frame.width-40,frame.height - (ui:getFont():getHeight() + 95))
				frameinput.OnChange = function()
					mob:setStory(frameinput:getText())
					storyTooltip:setText(frameinput:getText())
				end 
				frameinput.multiline = true 
				frameinput:setText(meta:getStory())
				nframe.zmap = frame.zmap 
			else 
				frame.nframe:remove()
				frame.nframe = nil 
			end
		end
		local w,h = image.width,image.height
		local nw,nh = mob.width,mob.height
		local sx,sy  = w/nw,h/nh
		local elements = {} 
		local function add(v,color)
			local img = icons[v] or icons["none"]
			if v == "attack" then 
				img = icons["attack2"]
			end 
			local e = {
				img = img,
				color = color
			}
			table.insert(elements,e)
		end 
		for i,v in ipairs(m.weak) do 
			add(v,colors.red)
		end
		for i,v in ipairs(m.strong) do 
			add(v,{182,255,0})
		end 
		for i,v in ipairs(m.invin) do
			add(v,{0,255,255})
		end 
		local anim = mob._Anim 
		if not anim then 
			for i,v in ipairs(mob.states) do
				if v.name == "idle" then
					anim = v.anim
				end
			end
		end 
		anim = anim:addInstance()
		image.OnDraw = function()
			local b = image
			local x,y = b.colision:bbox()
			anim:update(love.timer.getDelta())
			anim:draw(x,y,0,sx,sy)
			local hp,mhp = mob.info.hp,mob.info.maxHp
			if hp <= 0 then 
				local image = icons["dead"]
				local x,y = b.colision:center()
				local w,h = image:getDimensions()
				love.graphics.draw(image,x,y,0,1,1,w/2,h/2)
			end 
			for i,v in ipairs(elements) do
				local y = y + (frame.height -60)
				local x = x+(33*(i-1))
				setColor(v.color)
				love.graphics.circle("fill",x -3 + 15,y+15,15)
				setColor()
				if Hide then 
					love.graphics.draw(icons["none"],x,y+3)
				else 
					love.graphics.draw(v.img,x+2,y+4)
				end 
			end 
			local lw = love.graphics.getLineWidth()
			x,y = frame.colision:bbox()
			setColor(colors.gray)
			love.graphics.circle("fill",x+(15)/2,y+(15)/2,15)
			setColor()
			if m.element == "attack" then 
				love.graphics.draw(icons["attack2"],x-4,y-4)	
			else 
				love.graphics.draw(icons[m.element],x-4,y-4)			
			end 
		end 
		
		local props = ui:addFrame(nil,110,25,maxw,maxh,frame)
		props.OnDraw = function()
			local x,y = props.colision:bbox()
			love.graphics.print(table.concat(items),x,y)
		end 
		if maxw2 > 0 then 
			local skills = ui:addList(frame,110 + maxw +5,25,maxw2,maxh)
			for i,v in ipairs(m.skills) do 
				local name = v.name 
				local desc = v.desc
				local cost = v.cost
				local c = skills:addChoice(name.."["..cost.."]")
				c:addTooltip(desc)
			end
		end 
	end 
end 
return renderCard
