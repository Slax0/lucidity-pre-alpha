local _PACKAGE = getPackage(...)
local sound = {}
local filenames = {}
local fileName = ""
function sound:loadFromDir(dir)
	io.write("Directory of files: ")
	local a = dir
	 if love.filesystem.exists(a) then
		filenames = love.filesystem.getDirectoryItems(a)
		for k,file in ipairs(filenames) do
			print(k .. ": " .. file)
			fileName = a .. filenames[k]
			self:addTrack(fileName, "stream")
		end
	else
		print("Invalid directory")
	end
end

function sound:new(name, loop)
	local a = {
		name = name or "newChannel",
		loop = loop,
		currentTrack = 1,
		que = {},
		volume = 1,
	}
	return setmetatable(a, {__index = sound})
end

function sound:addTrack(names, sourceType)
	local a = love.audio.newSource(names, sourceType)
	self:push(a)
end

function sound:push(track)
	self:pop(track)
	self.currentTrack = #self.que + 1
	table.insert(self.que, track)
end
function sound:update(dt)
	if self.playing then 
		if #self.que == 0 then self.currentTrack = 0 end
		if self.activeTrack and not self.activeTrack:isPlaying() then
			local num = #self.que
			self.currentTrack = self.currentTrack + 1
			if self.currentTrack > num then
				if self.loop then
					self.currentTrack = 1
					if num == 1 then
						self.activeTrack:play()
					end
				else
					self.currentTrack = 0
				end
			end
		end
		if self.currentTrack ~= 0 and self.oldTrack ~= self.currentTrack then
			local a = self.que[self.currentTrack]
			a:play()
			self.activeTrack = a
			self.oldTrack = self.currentTrack
		end
		if self.activeTrack then self.activeTrack:setVolume(self.volume) end
	end 
end
function sound:play()
	self.playing = true 
end 
function sound:stop()
	self.playing = false
end 
function sound:pPlay()
	if self.playing then 
		self.playing =  false 
	else 
		self.playing =  true
	end 
end 
function sound:pop(track)
	for i,v in ipairs(self.que) do
		if v == track then table.remove(self.que, i)
		end
	end
end

function sound:getVolume()
	return self.volume
end 
function sound:setVolume(percent)
	self.volume = percent
	
end







return sound