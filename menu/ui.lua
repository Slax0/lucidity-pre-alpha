local ui = {}
local _PACKAGE = getPackage(...)
local tweens = Flux.group()
local tween = Timer:new()
local clear = {255,255,255} 
local buttonImage = love.graphics.newImage(_PACKAGE.."/buttonTest.png")
local utf8 = require("utf8_spec")
if not font then 
	font = love.graphics.newFont(12)
end 
local HeaderColor = {228, 155, 18} -- safety orange 

local function drawChildren(children,func)
	local zmaps = {} 
	local maxZmap = 1
	local minZmap = math.huge
	for i,v in ipairs(children) do 
		local z = zmaps[v.zmap or 1]
		if not z then 
			z = {}
			zmaps[v.zmap or 1] = z
		end
		table.insert(z,v)
		if (v.zmap or 1) > maxZmap then 
			maxZmap = v.zmap
		end
		if (v.zmap or 1) < minZmap then 
			minZmap = v.zmap
		end 
	end 
	for i = minZmap,maxZmap do  
		if zmaps[i] then 
			for i,v in ipairs(zmaps[i]) do 
				if not func and not v.inList then 
					if v.dontDraw then 
						if v.OnDraw then v.OnDraw(v) end 
					else 
						v:draw()
					end
				elseif func then 
					func(v)
				end 
			end 
		end 
	end 
end 

function ui:init(Collider2)
	local self = {}
	self.Collider = Collider2 or Collider
	self.zmap = 1
	self.items = {}
	self.colision = {}
	self.focused = {}
	self.max_zmap = 1
	self._LastFramePress = false
	self.font = love.graphics.newFont(uniBody_caps,12)
	self.mxb = self.Collider:addCircle(0,0,1)
	setmetatable(self, { __index = ui })
	return self
end
function ui:initTasks(item)
	item.zmap = self.max_zmap + 1 
	self.max_zmap = item.zmap 
end 
function ui:getFont()
	return self.font
end 
function ui:setMousePos(x,y)
	self.mousex = x 
	self.mousey = y 
end
function ui:setTileIncrement(degree)
	self.tile_increment = degree
end  
function ui:setLeftClick(hold,hold2)
	self.LkeyHold = hold
	self.alt_LkeyHold = hold2
end
function ui:getRightClick()
	return self.RkeyHold,self.alt_RkeyHold 
end 
function ui:setRightClick(hold,hold2)
	self.RkeyHold = hold
	self.alt_RkeyHold = hold2
end  
function ui:setMiddleMouse(down,up)
	self.mmup = down
	self.mmdown = up 
end 
function ui:setFont(font)
	self.font = font
end 


local button = {}
function ui:addButton(parent,x,y,w,h,text,shape)
	local w = w or 10
	local h = h or 10
	local item = {
		text = text,
		x = x,
		y = y,
		ox = x,
		oy = y,
		zmap = 1,
		width = w,
		height = h,
		rotation = 0,
		interface  = self,
		inactive = false,
		children = {},
		color2 = {0,0,0,0},
		OnClick = function() end,
		type = "button",
	}
	setmetatable(item, { __index = button })
	self:initTasks(item)
	item.colision = shape or self.Collider:addRectangle(x,y,w,h)
	self.Collider:setPassive(item.colision)
	item.colision.source = item
	table.insert(self.colision,item.colision)
	if not parent then
		table.insert(self.items,item)
	else
		parent:addItem(item)
		item.x = parent.x - (parent.width/2 - item.width/2) + item.x
		item.y = parent.y - (parent.height/2 - item.height/2) + item.y
	end
	item.interface  = self
	item.colision:moveTo(item.x,item.y)
	item:update(0.2)
	return item
end
	function button:getColision()
		return self.colision
	end
	function button:addItem(item)
		local clamp
		for i,v in ipairs(self.children) do 
			if v ~= item then 
				clamp = true 
			end 
		end 
		if not clamp then 
			table.insert(self.children,item)
		end 
	end 
	function button:moveTo(x,y)
		if self.interface.tile_increment then 
			local x,y = self.colision:bbox()
			x,y = incrementPos(x,y,self.interface.tile_increment)
		end 
		self.x = x
		self.y = y
		self.colision:moveTo(x,y)
	end
	function button:setTooltip(tooltip,delay)
		self.tooltip = tooltip
	end
	function button:addTooltip(text,delay)
		self.tooltip = self.interface:addTooltip(self,text)
		self.tooltip.delay = delay or self.tooltip.delay
		return self.tooltip
	end 
	function button:setZmap(z)
		if not self.inList then 
			self.zmap = z
			for i,v in pairs(self.children) do 
				v:setZmap(self.zmap + 1)
			end
		else 
			self.zmapModif = z
		end 
	end
	function button:update(dt)
		if self.OnUpdate then
			self.OnUpdate(dt)
		end
		if self.OnUpdate2 then self.OnUpdate2() end
		if self.interface.tile_increment then 
			local x,y = self.colision:bbox()
			local x1,y1 = self.colision:center()
			local w = findDistance(x,x1)
			local h = findDistance(y,y1)
			self.x,self.y = incrementPos(self.x,self.y,w,-h)
		end 
		self.colision:moveTo(self.x,self.y)
		self.colision:setRotation(self.rotation)
			local d = self._HasFocus
			if self._OldFocus ~= d then 
				if d then 
					if self.tween then tween:cancel(self.tween) end
					self.color2 = {255,255,255,35}
					self._activeColoring = true
				else
					if self.tween then tween:cancel(self.tween) end
					self.color2 = {0,0,0,0}
					self._activeColoring = true
				end 
				self._OldFocus = self._HasFocus
			end 
		if self.tooltip then 
			self.tooltip:update(dt)
		end 
		for i,v in ipairs(self.children) do
			if not v.inList then v:update(dt) end 
		end 
	end
	function button:setShape(shape)
		local ui = self.interface
		for i,v in pairs(ui.colision) do
			if v == self.colision then
				ui.Collider:remove(v)
				ui.colision[i] = nil
			end
		end
		self.colision = shape
		self.colision.source = self
		table.insert(ui.colision,self.colision)
	end
	function button:setRotation(rot)
		self.rotation = rot
		self.colision:setRotation(rot)
	end
	function button:mousepressed(x,y,key)
		self._ActivatedColoring = {64,64,64,220}
		if self._mousepressed then self._mousepressed(key) end 
		if not self.inactive and not self._inactive and key == "l" then
			if self.help and self.interface.help then self.help(self) else 
				if self.OnClick then self.OnClick(self) end 
			end 
			if self._OnClick then 
				self._OnClick()
			end 
		elseif self.OnRightClick and not self.inactive then
			self.OnRightClick(self)
		end
		self.active = true
	end
	function button:setText(text,x,y)
			self.text = text
			self.textX = x
			self.textY = y
	end
	function button:mousereleassed(x,y,key)
		self._ActivatedColoring = nil
		if self.OnRelease and not self.inactive then
			self.OnRelease(key)
		end
		self.active = false
	end
	function button:setInactive(t)
		self.inactive = t
	end
	function button:setSize(w,h)
		local col = self.interface.Collider:addRectangle(self.x- w/2,self.y - h/2,w,h)
		self.width = w
		self.height = h
		self:setShape(col)
	end
	function button:setImage(image,ofedge1,ofedge2,ofedge3,ofedge4)
			local bw,bh = image:getDimensions()
			local ofedge1 = ofedge1 or 0
			local ofedge2 = ofedge2 or 0
			local ofedge3 = ofedge3 or 0
			local ofedge4 = ofedge4 or 0
			
			bw = bw - (ofedge1 + ofedge2)
			bh = bh - (ofedge3 + ofedge4)
			love.graphics.setColor(self.color or {255,255,255})
			self.drawButton = function(x,y)
				local scaleFactorx = self.width/bw
				local scaleFactory = self.height/bh
				local x,y = x - (ofedge1*scaleFactorx) ,y - (ofedge3*scaleFactory)
				setColor(colors.white)
				love.graphics.draw(image,x,y,self.rotation,scaleFactorx,scaleFactory,bw/2,bh/2)
			end
	end 
	function button:draw()
			if self.OnDrawB then
				self.OnDrawB(self)
			end
			local x,y = self.x,self.y
			local r = self.rotation or 0
			local sx = self.sx or 1
			local sy = self.sy or 1 
			local ox = self.width/2
			local oy = self.height/2
			local cx,cy = self.colision:bbox()
			
			local bw,bh = buttonImage:getDimensions()
			bw = bw
			bh = bh 
			
			local scaleFactorx = self.width/bw
			local scaleFactory = self.height/bh
			if self.drawButton  then self.drawButton(x,y) end	
			love.graphics.setColor(self.color or {255,255,255})
			if not self.dontDrawB and not self.mesh and not self.image then 
				love.graphics.draw(buttonImage,x ,y,0,scaleFactorx,scaleFactory,bw/2,bh/2)
			elseif not self.dontDrawC  and not self.drawButton then  
				self.colision:draw("fill")
			end
			if self.image then
				love.graphics.draw(self.image,x,y,r,sx,sy,ox,oy)
			end
			if self.mesh then
				love.graphics.draw(self.mesh,cx,cy,r,sx,sy)
			end
			if self.text and not self.dontDrawText then
				local oldFont = love.graphics.getFont()
				if self.textFont then
					love.graphics.setFont(self.textFont)
				end
				local text = self.text
				if type(self.text) == "table" then
					text = table.concat(self.text)
				end			
				local font = self.textFont or self.interface.font or font  
				local h = font:getHeight(text)
				local w = font:getWidth(text)
				love.graphics.setFont(font)
				love.graphics.setColor(self.textColor or {255,255,255})
				love.graphics.print(text,self.x + (self.textOX or 0),self.y + (self.textOY or 0),r,sx,sy,self.textX or (ox - (self.width - w)/2),self.textY or (oy - (self.height - h)/2))
				love.graphics.setFont(oldFont)
			end
			love.graphics.setColor(clear)
		if self.OnDraw then
			self.OnDraw(self)
		end
		if not self.dontDrawActive then 
			local c = false 
			if not self.activeColoring and not self.dontDrawC then 
				c = true 
			elseif not self.activeColoring and self.dontDrawD then 
				c = true 
			elseif self.activeColoring then 
				c = true 
			end 
			if c then 
				love.graphics.setColor(self.color2)
				self.colision:draw("fill")
			end 
			if self.OnActiveColoring then self.OnActiveColoring(self._activeColoring) end 
			local c =   self.ActivatedColoring  or self._ActivatedColoring
			if c then 
				love.graphics.setColor(c)
				self.colision:draw("fill")
			end 
			if self.InactiveColor or self.inactive then 
				love.graphics.setColor(self.InactiveColor or {55,55,55,100})
				self.colision:draw("fill")
			end 
		end 
		love.graphics.setColor(colors.white)
		if self.tooltip and not self.inList then 
			if not self.tooltip.dontDrawB then self.tooltip:draw() end 
		end	
	end
	function button:setTexture(image,wrap,mode)
		local c = self.colision._type
		if c == "polygon" then
			local points = self.colision:getPoints()
			local verticies = {}

			local maxX = 0
			local minX = math.huge
			local minY = math.huge
			local maxY = 0
			for i,v in ipairs(points) do
				if v.x > maxX then
					maxX = v.x
				end
				if v.x < minX then
					minX = v.x
				end
				if v.x > maxY then
					maxY = v.y
				end
				if v.x < minY then
					minY = v.y
				end
			end
			for i,v in ipairs(points) do
				local x = v.x 
				local y = v.y
				local u = (x + (0 - minX))/(minX - maxX)*-1
				local v = (y + (0 - minX))/(minY - maxY)*-1
				table.insert(verticies,{x,y,u,v})
			end
			image:setWrap("repeat")
			self.mesh = love.graphics.newMesh(verticies,image,'fan')
		end
	end
	function button:remove()
		if self.OnRemove then 
			self.OnRemove()
		end 
		for i,v in pairs(self.interface.colision) do
			if v == self.colision then
				self.interface.colision[i] = nil
			end
		end
		if self.interface.activeObject == self then
			self.interface.activeObject = nil
		end
		if self.children then
			for i,v in pairs(self.children) do
				v:remove()
			end
			self.children = {}
		end
		if not self.parent then 
			for i,v in pairs(self.interface.items) do
				if v == self then
					table.remove(self.interface.items,i)
					self.activeObject = nil
				end
			end
		else
			for i,v in pairs(self.parent.children) do
				if v == self then 
					table.remove(self.parent.children,i)
				end
			end
		end
		self.interface.Collider:remove(self.colision)
	end
local frames = {}
function ui:addFrame(name,x,y,w,h,parent)
	local w = w or 10
	local h = h or 10
	if type(name) == "table" then 
		parent = name 
	end 
	local x,y = x or love.graphics.getWidth()/2, y or love.graphics.getHeight()/2
	local item = {
		name = name,
		x = x,
		y = y,
		ox = x,
		oy = y,
		zmap = 1,
		width = w,
		height = h,
		rotation = 0,
		interface  = self,
		color = basicFrameColor,
		children = {},
		inactive = false,
		type = "frame",
	}
	setmetatable(item, { __index = frames })
	if name then 
		item:setHeader()
	end 

	local shape 

	if string.find(tostring(name),"Shape") then 
		shape = name
		item.x,item.y = shape:center()
	end 
	self:initTasks(item)
	item.ox = x 
	item.oy = y
	item.colision = shape or self.Collider:addRectangle(x,y,w,h)
	self.Collider:setPassive(item.colision)
	item.colision.source = item
			local x,y = item.colision:bbox()
			local x1,y1 = item.colision:center()
			local w = findDistance(x,x1)
			local h = findDistance(y,y1)
			item.width,item.height = w*2,h*2
	table.insert(self.colision,item.colision)
	item.colision:moveTo(item.x,item.y)
	if not parent then
		table.insert(self.items,item)
	else
		item.parent = parent
		parent:addItem(item)
		item.x = parent.x - (parent.width/2 - item.width/2) + item.x
		item.y = parent.y - (parent.height/2 - item.height/2) + item.y
	end
	item:update(0.2)
	return item
end
	function frames:setOffset(x,y)
		self.offsetx = x 
		self.offsety = y 
	end 
	function frames:getColision()
		return self.colision
	end
	function frames:addCloseButton(fn)
		if self._closebutton then self._closebutton:remove() end 
		local close = self.interface:addButton(self,self.width-20,0,20,20)
		close.text = "X"
		self._closebutton = close 
		close.OnClick = function()
			if fn then fn() end 
			self:remove()
		end 
	end
	function frames:addItem(item)
		if item.setZmap then item:setZmap(self.zmap +1) else item.zmap = self.zmap end 
		item.parent  = self
		table.insert(self.children,item)
		for i,v in pairs(self.interface.items) do
			if v == item then
				self.interface.items[i] = nil
			end
		end
	end
	function frames:moveTo(x,y)
		local olx = self.ox 
		local oly = self.oy
		local olw = self.width
		local olh = self.height
		self.x = x  
		self.y = y 
		self.colision:moveTo(x + (self.offsetx or 0),y + (self.offsety or 0))
		if self.dragEnabled  or self.tempDMonitor or self.inList then 
			if math.floor(self.x or 0) ~= math.floor(olx or 0) or math.floor(self.y or 0) ~= math.floor(oly or 0) then 
				for i,v in ipairs(self.children) do 
					if v.colision or v.sliderButton or v.isText then 
						local item = v 
						local parent  = self
						local olx = item.olx or (item.x - (olx - (olw/2 - item.width/2)))
						local oly = item.oly or (item.y - (oly - (olh/2 - item.height/2)))
						item.x = parent.x - (parent.width/2 - item.width/2) + olx
						item.y = parent.y - (parent.height/2 - item.height/2) + oly
						if v.OnMove then 
							v.OnMove(item.x,item.y)
						end 
						if v.onMove then 
							v:onMove(item.x,item.y)
						end 
					end 
				end 
				self.ox = self.x 
				self.oy = self.y
			end 
		end 
	end
	function frames:setZmap(z)
		self.zmap = z
		for i,v in pairs(self.children) do 
			if v.setZmap then v:setZmap(self.zmap + 1) else 
				v.zmap = self.zmap + 1 
			end 
		end
	end
	function frames:update(dt)
		if self.OnUpdate then
			self.OnUpdate(dt)
		end
		if self.interface.tile_increment then 
			local x,y = self.colision:bbox()
			local x1,y1 = self.colision:center()
			local w = findDistance(x,x1)
			local h = findDistance(y,y1)
			self.width,self.height = w*2,h*2
			self.x,self.y = incrementPos(self.x,self.y,w,h)
		end 
		self:moveTo(self.x,self.y)
		self.colision:setRotation(self.rotation)
		for i,v in pairs(self.children) do
			v:update(dt)
		end
		if self.tooltip then self.tooltip:update(dt) end 
	end
	function frames:setShape(shape)
		local ui = self.interface
		for i,v in pairs(ui.colision) do
			if v == self.colision then
				self.interface.Collider:remove(v)
				ui.colision[i] = nil
			end
		end
		self.colision = shape
		self.colision.source = self
		table.insert(ui.colision,self.colision)
	end
	function frames:setImage(image)
		self.image = image
	end
	function frames:setTooltip(tooltip,delay)
		self.tooltip = tooltip
	end
	function frames:addTooltip(text,delay)
		self.tooltip = self.interface:addTooltip(self,text)
		self.tooltip.delay = delay or self.tooltip.delay
		return self.tooltip
	end 
	function frames:setRotation(rot)
		self.rotation = rot
		self.colision:setRotation(rot)
		for i,v in pairs(self.children) do
			v:setRotation(rot)
		end
	end
	function frames:mousepressed(x,y,key)
		if self.OnClick and not self.inactive and key == "l" then
			self.OnClick(x,y)
		elseif self.OnRightClick and not self.inactive then
			self.OnRightClick(x,y)
		end
		self.active = true
	end
	function frames:setText(text,x,y)
			self.text = text
			self.textX = x
			self.textY = y
	end
	function frames:mousereleassed(x,y,key)
		if self.OnRelease and not self.inactive then
			self.OnRelease(key)
		end
		self.active = false
	end
	function frames:keypressed(...)
		if self.OnKeypressed then self.OnKeypressed(...) end
	end 
	function frames:setInactive(t)
		self.inactive = t
		for i,v in ipairs(self.children) do 
			v.inactive = t
		end 
	end
	function frames:setSize(w,h)
		local olx = self.x
		local oly = self.y 
		local olw = self.width
		local olh = self.height
		local col = self.interface.Collider:addRectangle(self.x,self.y,w,h)
		self.width = w
		self.height = h
		self:setShape(col)
		for i,v in pairs(self.children) do
			local item = v 
			local parent  = self
			local olx = item.x - (olx - (olw/2 - item.width/2))
			local oly = item.y - (oly - (olh/2 - item.height/2))
			item.x = parent.x - (parent.width/2 - item.width/2) + olx
			item.y = parent.y - (parent.height/2 - item.height/2) + oly
			
		end 
	end
	function frames:draw()
		if not self.dontDrawSelf then 
			local x,y = self.x,self.y
			local r = self.rotation or 0
			local sx = self.sx or 1
			local sy = self.sy or 1 
			local ox = self.width/2
			local oy = self.height/2
			local cx,cy = self.colision:bbox()
			love.graphics.setColor(self.color or {255,255,255})
			self.colision:draw("fill")
			if self.image then
				love.graphics.draw(self.image,x,y,r,sx,sy,ox,oy)
			end
			if self.mesh then
				love.graphics.draw(self.mesh,cx,cy,r,sx,sy)
			end
			
			if self.header then
				local olFont = love.graphics.getFont()
				love.graphics.setFont(self.textFont or self.interface.font or font)
				local text = self.text or self.name or ""
				local font = self.textFont or self.interface.font or font 
				if type(text) == "table" then
					text = table.concat(text)
				end			
				local h = font:getHeight(text)
				local w = font:getWidth(text)
				local x,y = self.colision:bbox()
				setColor(self.headerColor or HeaderColor)
				love.graphics.rectangle("fill",x,y,self.width,h-1)
				setColor(self.textColor)
				love.graphics.print(text,self.x,self.y,r,sx,sy,self.textX or (ox - (self.width + 5 - w)/2),self.textY or (oy - (16 - h)/2))
				love.graphics.setFont(olFont)
			end
			setColor()
		end 
		if self.OnDraw then
			self.OnDraw(self)
		end
		drawChildren(self.children)
		if self.tooltip then 
			self.tooltip:draw()
		end	
		if self.OnDrawTop then 
			self.OnDrawTop(self)
		end 
	end
	function frames:setTexture(image,wrap,mode)
		local c = self.colision._type
		if c == "polygon" then
			local points = self.colision:getPoints()
			if points then
				local verticies = {}
				if not mode or mode == "overlap" then
					local maxX = 0
					local minX = math.huge
					local minY = math.huge
					local maxY = 0
					for i,v in ipairs(points) do
						if v.x > maxX then
							maxX = v.x
						end
						if v.x < minX then
							minX = v.x
						end
						if v.x > maxY then
							maxY = v.y
						end
						if v.x < minY then
							minY = v.y
						end
					end
					for i,v in ipairs(points) do
						local x = v.x 
						local y = v.y
						local u = (x + (0 - minX))/(minX - maxX)*-1
						local v = (y + (0 - minX))/(minY - maxY)*-1
						table.insert(verticies,{x,y,u,v})
					end
				else
					for i,v in ipairs(points) do
						local x = v.x 
						local y = v.y
						local u = i
						local v = i
						table.insert(verticies,{x,y,u,v})
					end
				end
				image:setWrap(wrap or "repeat")
				self.mesh = love.graphics.newMesh(verticies,image,'fan')
			else
				
			end
		elseif c == "circle" then
			
		end
	end
	function frames:setHeader(name,w,h,color)
		if type(name) == "number" then 
			w = name 
			h = w 
			color = h 
		end 
		self.header = {
			w = w or self.width,
			h = h or 20,
			color = color or {155,155,155},
		}
		self.name = name or self.name 
	end
	function frames:textinput(...)
		if self.OnTextinput then self.OnTextinput(...) end
	end 
	function frames:remove()
		if self.OnRemove then 
			self.OnRemove()
		end 
		if self.interface.activeObject == self then
			self.interface.activeObject = nil
		end
		if self.children then
			local items = {}
			for i,v in pairs(self.children) do
				table.insert(items,v)
			end
			for i,v in pairs(items) do 
				items[i]:remove()
			end
		end
		if not self.parent then 
			for i,v in pairs(self.interface.items) do
				if v == self then
					table.remove(self.interface.items,i)
					self.activeObject = nil
				end
			end
		else
			for i,v in pairs(self.parent.children) do
				if v == self then 
					table.remove(self.parent.children,i)
				end
			end
		end
		self.activeObject = nil
		self.interface.Collider:remove(self.colision)
		self.children = {}
		for i,v in pairs(self.interface.colision) do
			if v == self.colision then
				self.interface.colision[i] = nil
			end
		end
	end
	
	
local text = {}
function ui:addText(parent,x,y,texts,fonta,color)
	local item = {
		text = tostring(texts),
		x = x,
		y = y,
		ox = x,
		oy = y,
		interface  = self,
		font = fonta,
		zmap = 1,
		color = color or {255,255,255},
		type = "text",
		isText = true,
	}
	setmetatable(item, { __index = text })
	item.width = font:getWidth(item.text)
	item.height = font:getHeight(item.text)
	if not parent then
		table.insert(self.items,item)
	else
		parent:addItem(item)
		item.x = parent.x - (parent.width/2 - item.width/2) + item.x
		item.y = parent.y - (parent.height/2 - item.height/2) + item.y
	end
	return item
end
function text:draw()
	love.graphics.setColor(self.color)
	local font = love.graphics.getFont()
	love.graphics.setFont(self.font  or self.interface.font or font )
	love.graphics.print(self.text,self.x - self.width/2,self.y - self.height/2)
	love.graphics.setColor(clear)
	love.graphics.setFont(font)
	local x,y = self.x,self.y 
	local x2,y2 = self.x - self.width,self.y
	if self.DrawPrefix then self.DrawPrefix(x,y) end
    if self.DrawSuffic then self.DrawSuffic(x2,y2) end
end
function text:getColision()
	return false 
end
function text:setText(text,font,color)
	if text then self.text = text end
	if font then self.font = font end
	if color then self.color = color end 
end
function text:update(dt)
end
function text:setZmap(z)
	self.zmap = z
end
function text:remove()
	if not self.parent then
		for i,v in pairs(self.interface.items) do
			if v == self then
				self.interface.items[v] = nil
				self.activeObject = nil
			end
		end
	end
end

local costum = {}
function ui:addCostum(x,y,colision)
	local item = {
		x = x,
		y = y,
		zmap = 1,
		interface  = self,
		color = {255,255,255}
	}
		setmetatable(item, {__index = costum})
	if colision then
		item.colision = colision
		colision.source = item
		table.insert(self.colision,colision)
	end
	self:initTasks(item)
	table.insert(self.items,item)
	return item
end
	function costum:draw()
		love.graphics.setColor(self.color)
		if self.colision and not self.dontDraw then
			self.colision:draw("fill")
		end
		love.graphics.setColor(clear)
		if self.OnDraw then
			self.OnDraw(self)
		end
	end
	function costum:getColision()
		return self.colision
	end
	function costum:update(dt)
		if self.OnUpdate then
			self.OnUpdate(dt)
		end
	end
	
local tooltip = {}
function ui:addTooltip(parent,text,mode,fonta,color) -- mode can be: left, right, up, down
	local item = {
		text = texts,
		x = x,
		y = y,
		ox = x,
		py = y,
		width = 1,
		height = 1,
		interface  = self,
		font = fonta,
		zmap = 1,
		oH2 = 0,
		delay = 0.8,
		timer = 0,
		alpha = 0,
		points = {},
		triangle = {},
		parent = parent,
		mode = mode or "up",
		color = color or {255,255,255},
		type = "tooltip"
	}
	setmetatable(item, { __index = tooltip })
	self:initTasks(item)
	local font = fonta  or self.font or font 
	assert(parent,"Tool tip must be have a parent!! Don't leave a child on the road!")
	if parent then
		parent:setTooltip(item)
	end
	item:setText(text or "")
	return item
end
function tooltip:getColision()
	return self.colision
end 
function tooltip:draw()
	if self.drawingEnabled then 
		local g = tooltipGradientx
		local g2 = tooltipGradienty
		local oldLW = love.graphics.getLineWidth()
		local points = self.points
		local boxColor = colors.gray
		local linecolor1 = colors.white
		local linecolor2 = colors.white
		local mode = "fill"
		local a = self.alpha
			local smoothing = 2
			local function drawCircle(mode,color)
				local r,g,b = unpack(color)
				love.graphics.setColor(r,g,b,a)
				love.graphics.circle(mode,points[1],points[2],smoothing*2)
				love.graphics.circle(mode,points[3],points[4],smoothing*2)
				love.graphics.circle(mode,points[5],points[6],smoothing*2)
				love.graphics.circle(mode,points[7],points[8],smoothing*2)
			end 
		

		local r,g,b = unpack(boxColor)
		love.graphics.setColor(r,g,b,a)
		love.graphics.polygon("fill",unpack(self.triangle))
		love.graphics.polygon("fill",unpack(points))
		
		local r,g,b = unpack(linecolor1)
		love.graphics.setColor(r,g,b,a)
		
		if self.height > 10 and self.width > 15 then 
			love.graphics.setInvertedStencil()
			love.graphics.setStencil()

			local r,g,b = unpack(linecolor2)
			love.graphics.setInvertedStencil(function() love.graphics.polygon("fill",unpack(points)) end)
			love.graphics.line(unpack(self.triangle))
			love.graphics.setInvertedStencil(function() love.graphics.polygon("fill",unpack(self.triangle)) end)
			love.graphics.line(unpack(points))


		end 

			
			
		love.graphics.setStencil(function()love.graphics.polygon("fill",unpack(points))end)
			if self.height > 10 and self.width > 15 then 
				local of = love.graphics.getFont()
				love.graphics.setFont(self.font or self.interface.font or font)
				local r,g,b = unpack(colors.white)
				love.graphics.setColor(r,g,b,a)
				love.graphics.print(self.text,self.points[1] + self.textGap,self.points[6] +self.textGap)
				love.graphics.setColor(r,g,b,a)
				love.graphics.setFont(of)
			end 
		love.graphics.setStencil()
	end 
	if self.OnDraw then self.OnDraw(self) end 
end
function tooltip:setText(text)
	self:setZmap(self.parent.zmap)
	local font = self.font  or self.interface.font or font
	local w = font:getWidth(text)
	local h = font:getHeight()
	local no = 1
	for word in string.gmatch(text,"\n") do 
		no = no + 1
	end 
	h = h * no
	self.oH = h
	self.textGap = 6 
	local w,h = w + self.textGap*3, h + self.textGap*3
	self.w = w -self.textGap
	self.h = h -self.textGap
	self.text = text 
		
		
end
function tooltip:setCenter(cx,cy,px,py)
	self.Cx = cx
	self.Cy = cy
	self.Px = px
	self.Py = py 
	self:setPoints()
end 
function tooltip:setPoints()
	local m = self.mode 
	local p = self.points
	local p2 = self.triangle
	if not m or m == "up" then
	local x,y = (self.Px or self.parent.x)  - self.width/2,(self.Py or self.parent.y - self.parent.height/2) - (self.oH2/2)
	
	x = x -3
	y = y -3
	p[1] = x 
	p[2] = y
	
	p[3] = x + self.width
	p[4] = y
	
	p[5] = x + self.width
	p[6] = y - self.height
	
	p[7] = x
	p[8] = y - self.height
	
	p[9] = x 
	p[10] = y
	
	self.x = x 
	self.y = y
	
	local size = self.width/15
	local cx,cy = x + self.width/2 - size ,y -3
	local cx2,cy2 = x + self.width/2 + size,y -3 
	p2[1] = cx
	p2[2] = cy
	
	p2[3] = cx2
	p2[4] = cy2
	
	p2[5] = self.Cx or self.parent.x
	p2[6] = self.Cy or (self.parent.y - self.parent.height/2)
	
	p2[7] = cx
	p2[8] = cy
	elseif m == "left" then 
		local x = (self.Px or self.parent.x) - self.width - self.parent.width/2
		local y = (self.Py or self.parent.y) + self.height/2
		x = x -30
		y = y -3
		p[1] = x 
		p[2] = y
		
		p[3] = x + self.width
		p[4] = y
		
		p[5] = x + self.width
		p[6] = y - self.height
		
		p[7] = x
		p[8] = y - self.height
		
		p[9] = x 
		p[10] = y
		
		self.x = x 
		self.y = y
		
		local size = 5
		local cx,cy = x + self.width - size ,y -self.height
		local cx2,cy2 = x + self.width - size,y 
		p2[1] = cx
		p2[2] = cy
		
		p2[3] = cx2
		p2[4] = cy2
		local x = self.parent.colision:bbox() 
		p2[5] = self.Cx or x
		p2[6] = self.Cy or (self.parent.y)
		
		p2[7] = cx
		p2[8] = cy
	end 
	
	self.oldX = math.floor(self.parent.x)
	self.oldY = math.floor(self.parent.y)
end 
function tooltip:update(dt)
	local d = self.TurnedOn or self.parent._HasFocus 
	if self.manual then 
		d = self.TurnedOn
	end 
	local tdelay = self.appearDelay or 0.5 
	local ddelay = self.disappearDelay or 0.2
	if d then 
		self.timer = self.timer + dt
		if self.delay < self.timer then
			if not self.OldDraw then 
				if self.tween then self.tween:stop() end 
				self.tween = tweens:to(self,tdelay,{width = self.w,height = self.h,oH2 = self.oH,alpha = 255}):onupdate(function(dt) self:setPoints() end):oncomplete(self.OnDrawTween or function()end)
				self.OldDraw = true				
			end 
			self.timer = 0
			self.drawingEnabled = true
		else 
			self.OldDraw = false
		end
	elseif self.drawingEnabled then 
		if self.tween then self.tween:stop() end
		self.tween = tweens:to(self,ddelay,{width = 1,height = 1,oH2 = 1,alpha = 0}):onupdate(function(dt)  self:setPoints() 
			if math.floor(self.height) < 5 and math.floor(self.width) < 5 then 
				self.OldDraw = false
				self.drawingEnabled = false	
			end 
		end)
	end 
	if math.floor(self.parent.x) ~= math.floor(self.oldX or 0) or math.floor(self.parent.y) ~= math.floor(self.oldX or 0) then
		self:setPoints()
	end 	
	if self.OnUpdate then self.OnUpdate(dt) end 
end
function tooltip:setZmap(z)
	self.zmap = z
end
function tooltip:remove()
	if not self.parent then
		for i,v in pairs(self.interface.items) do
			if v == self then
				self.interface.items[v] = nil
				self.activeObject = nil
			end
		end
	end
end

local menu = {}
function ui:addMenu(parent,x,y,tables) -- tables as follows: {{name,func,space},{name,func,space}} etc -- space is optional and in px!
	local item = {
		text = texts,
		x = x,
		y = y,
		ox = x,
		oy = y,
		interface  = self,
		zmap = 1,
		children = {},
		_items = {},
		parent = parent,
		oldFocus = true,
		type = "menu",
		timer = 0,
		timer2 = 0,
	}
	setmetatable(item, { __index = menu })
	self:initTasks(item)
	local font = self:getFont()
	local gap = 5
	local maxW = 0
	local maxH = 1
	
	if tables then
		 maxH = ((font:getHeight() + (gap)) * #tables) + gap
		for i,v in pairs(tables) do 
			self.static = true
			local w = font:getWidth(v[1])
			if w > maxW then 
				maxW = w
			end 
			maxH = maxH + (v[3] or 0)
		end
	end 
	local maxW = (maxW + gap*2)

	if not parent then
		table.insert(self.items,item)
	else
		item.parent = parent
		parent:addItem(item)
		item.x = parent.x 
		item.y = parent.y + parent.height/2
	end
	item.mainFrame = self:addFrame(nil,item.x,item.y + maxH/2 + 5,maxW,maxH)
	item.oldX = item.x 
	item.oldY = item.y
	item.mainFrame.tempDMonitor = true
	item.mainFrame.color = basicFrameColor
	if tables then 
		for i,v in ipairs(tables) do 
			local lastx
			local lasty
			local text =  v[1]
			local func =  v[2]
			local space = 0
			if tables[i-1] then 
				space = tables[i-1][3] or 0
			end 
			local a  
			local x,y
			local w,h 
			if not tables[i - 1] then 
				lastx = gap
				lasty = gap
				w,h = maxW - gap*2,font:getHeight()
				x,y = lastx,lasty
				a = self:addButton(item.mainFrame,x,y,w,h)
			else 
				local v = tables[i - 1]
				lastx = v[4].lx
				lasty = v[4].ly
				w,h = maxW - gap*2,font:getHeight()
				x,y = gap,lasty + gap + space + h
				a = self:addButton(item.mainFrame,x,y,w,h)
			end 
			a.color = basicButtonColor
			a.dontDrawB = true
			a.OnClick = func
			a:setText(text)
			a.lx = x 
			a.ly = y 
			tables[i][4] = a 
			table.insert(item._items,a)
		end 
	end 
	table.insert(item.children,item.mainFrame)
	item.table = tables
	return item
end 
function menu:addChoice(text,fn)
	local b = self.interface:addButton(nil,2,2,2,2)
	b:setText(text)
	b.OnClick = fn 
	self:addItem(b)
end 
function menu:getColision()
	return self.mainFrame.colision
end
function menu:addItem(item,func)
	if type(item) == "string" then 
		local i = self.interface:addButton(nil,0,0,20,20)
		i:setText(item)
		i.OnClick = func
		item = i 
	end 
	table.insert(self._items,item) 
	self:refresh()
end 
function menu:clear()
	for i,v in ipairs(self._items) do 
		item._items[i] = nil 
	end 
	self.mainFrame:remove()
	self:refresh()
end 
function menu:refresh()
	if not self.static then 
		local items = self._items
		local font = self.font or self.interface:getFont()
		local gap = 5
		local maxW = 0
		local maxH = ((font:getHeight() + (gap)) * #items) + gap
		for i,v in pairs(items) do 
			local w = font:getWidth(v.text or "")
			if w > maxW then 
				maxW = w
			end 
			maxH = maxH + (v.space or 0)
		end
		local maxW = (maxW + gap*2)
		self.mainFrame:remove()
		self.mainFrame = nil
		local interface  = self.interface:addFrame(nil,self.oldX,self.oldY + maxH/2 - 5,maxW,maxH)
		self.mainFrame = interface 
		self.mainFrame.tempDMonitor = true
		self.mainFrame.color = basicFrameColor
		for i,v in ipairs(items) do 
			local item = v 
			local lastx 
			local lasty  
			local w,h = maxW - gap*2,font:getHeight()
			local x,y 
			local space = 0 
			if items[i-1] then
				local it =  items[i-1]
				lastx,lasty = it.lx,it.ly
				space = it.space or 0 
				x,y = gap,lasty + gap + space + h 
			else 
				lastx,lasty = gap,gap
				x,y = lastx,lasty 
			end
			item.lx = x 
			item.ly = y 
			item.color = basicButtonColor
			item.dontDrawB = true
			local parent = self.mainFrame
			item.x = parent.x - (parent.width/2 - w/2) + x
			item.y = parent.y - (parent.height/2 - h/2) + y
			item:setSize(w,h)
			for i,v in pairs(self.interface.items) do 
				if v == item then 
					table.remove(self.interface.items,i)
				end 
			end 
			self.mainFrame:addItem(item)
		end 
	end 
	self.width = self.mainFrame.width 
	self.height = self.mainFrame.height
end 
function menu:addSubmenu(tableItemNo,tables) -- same as with the menu
	assert(self.table[tableItemNo],"Number: "..tostring(tableItemNo).." Doesn't exist!")
	local v = self.table[tableItemNo][4]
	local c = self.interface:addMenu(nil,v.x + v.width + 3,v.y,tables)
	table.insert(self.children,c)
	return c
end 
function menu:setInactive(bool)
	for i,v in ipairs(self.mainFrame.children) do
		v.dontDraw = bool
		v:setInactive(bool)
	end 
	self.mainFrame:setInactive(bool)
	self.mainFrame.dontDraw = bool
	self.inactive = bool
end 
function menu:update(dt)
	if not self.inactive then
		local curFocus = true
		if self.OnUpdate then
			self.OnUpdate(dt)
		end
		for i,v in pairs(self.children) do
			v:update(dt)
		end
		if self.OnFocus or self.OnFocusLost then
			local foc = self.OnFocus or function() end 
			local nFoc = self.OnFocusLost or function() end
			local focus = false
			for i,v in ipairs(self.mainFrame.children) do 
				if not focus and self.interface.mxb:collidesWith(v.colision) then
					focus = true 
				end 
			end 
			local a 
			if self.parent then 
				a = self.parent._HasFocus
			end 
			focus = focus or self.interface.mxb:collidesWith(self.mainFrame.colision) or a
			if focus ~= (self.oldFocus or curFocus) then 
				self.timer = self.timer + dt
				if self.timer > 0.1 then 
					if not focus then 
						nFoc()
					else
						foc()
					end 
					self.timer = 0 
					self.oldFocus = focus
				end 
			end 
		end 
	end 
end 
function menu:remove()
	if self.interface.activeObject == self then
		self.interface.activeObject = nil
	end
	if self.children then
		local items = {}
		for i,v in pairs(self.children) do
			table.insert(items,v)
		end
		for i,v in pairs(items) do 
			items[i]:remove()
		end
	end
	if not self.parent then 
		for i,v in pairs(self.interface.items) do
			if v == self then
				table.remove(self.interface.items,i)
				self.activeObject = nil
			end
		end
	else
		for i,v in pairs(self.parent.children) do
			if v == self then 
				table.remove(self.parent.children,i)
			end
		end
	end
	if self.mainFrame then 
		self.mainFrame:remove()
	end 
	self.activeObject = nil
	self.children = {}
end 
function menu:draw()
	if self.OnDraw then
		self.OnDraw(self)
	end
	drawChildren(self.children)
end 



local slider = {}
local sliderButton = {}
function ui:addSlider(parent,x,y,w,h,mode)
	local w = w or 10
	local h = h or 10
	local item = {
		name = name,
		x = x - w,
		y = y - h/2,
		ox = x,
		oy = y,
		zmap = 1,
		width = w,
		height = h,
		rotation = 0,
		interface  = self,
		children = {},
		inactive = false,
		type = "slider",
		color = basicButtonColor,
		percent = 0,
	}
	setmetatable(item, { __index = slider })
	-- self = ui
	
	self:initTasks(item)
	if not parent then
		table.insert(self.items,item)
	else
		item.parent = parent
		parent:addItem(item)
		item.x = parent.x - (parent.width/2 - item.width/2) + item.x
		item.y = parent.y - (parent.height/2 - item.height/2) + item.y
	end
	
	local item2 = {
		name = name,
		x = item.width/2,
		y = 0,
		width = math.max(10,(item.width/100)*10),
		height = 10,
		olxLeft = 0,
		olyVert = 0,
	}
	setmetatable(item2, {__index = sliderButton})
	local posX = item2.x + (item.width/2 - item2.width/2)
	local posY = item2.y + (item.height/2 - item2.height/2)
	local bh 
	if mode and string.find(mode,"vert") then
		item2.width = item.width*2
		posX = item2.x + (item.width/2 - item2.width/2)
		bh = math.max(10,(item.height/100)*10)
	end 
	local sliderButton = self:addButton(item, posX, 0, bw or item2.width, bh or item.height*2)
	sliderButton.dragEnabled = true
	sliderButton.dragAnywhere = true 
	mode = mode or ""
	if string.find(mode,"horiz") then 
		item.mode = true
		sliderButton.horizOnly = true
	elseif string.find(mode,"vert") then 
		item.mode = false 
		sliderButton.vertOnly = true 
	else 
		item.mode = true
		sliderButton.horizOnly = true 
	end 
	sliderButton.OnDraw = function()
	end
	item.sliderButton = sliderButton
	local diff = item2.x - (item.x - item.width/2)
	sliderButton.OnMove = function(x,y)
		local cDist = findDistance(item.x,x)
		sliderButton._activeDistance = cDist
		if item.func then item.func(item:getDecimal()) end 
	end 
	return item
end
function slider:addItem(item)
	item:setZmap(self.zmap +1)
	item.interface  = self
	table.insert(self.children,item)
	for i,v in pairs(self.interface.items) do
		if v == item then
			self.interface.items[i] = nil
		end
	end
end
function slider:setZmap(zmap)
	self.zmap = zmap
end
function slider:getFloat()
	local percent
	if self.mode then 
		percent = (self.sliderButton.olxLeft or 0)/(self.width - self.sliderButton.width)
	else
		percent = (self.sliderButton.olyVert or 0)/(self.height)
	end 
	self.percent = percent
	return percent
end
function slider:setDecimal(d,prc)

	if d > 100 or d < 0 then d = 50 end 
	local dist = (prc)*self:getStepSizePX()
	if self.sliderButton.vertOnly then 
		self.sliderButton.y = self.y + dist
	else 
		self.sliderButton.x = self.x + dist
	end 
end 
function slider:setFocus(bool)
	if bool then 
		self.interface.ActiveSlider = self 
	else 
		if self.interface.ActiveSlider == self then 
			self.interface.ActiveSlider = nil 
		end 
	end 
end
function slider:setFloat(d) 
	self:setDecimal(d)
end 
function slider:getDecimal()
	return math.abs(self:getFloat()*100)
end 
function slider:getStepSizePX()
	local s = self.width/100
	if self.sliderButton.vertOnly then 
		s = self.height/100
	end 
	return s 
end 
function slider:setRange(min,max,bool)
	self.max = max 
	self.min = min 
	self.integer = bool
	self.range = findDistance(min,max)
end
function slider:getValue()
	local prc = self:getDecimal()
	local val = math.floor((self.range/100)*prc)
	return val
end 
function slider:setValue(val)
	local val = val
	local prc = findPrecent(self.range,val)
	self:setDecimal(0,prc)
end 
function slider:setPos(x,y)
	local olx = self.x
	local oly = self.y 
	local olw = self.width
	local olh = self.height
	
	local item 	=  self.sliderButton
	
	self.x = x + self.ox  
	self.y = y + self.oy 
	item.x = self.x 	
	item.y = self.y 
	self:setValue(self:getValue())
end 
function slider:moveTo(x,y)
	self:setPos(x,y)
end
function slider:buttonRange()
	local function d()
		self.sliderButton.dragEnabled = false
		if self.interface.activeObject == self.sliderButton then 
			self.interface.activeObject = nil 
		end 
	end 
	if self.mode then 
		if self.sliderButton.olxLeft < 0 then
			d()
			self.sliderButton.x = self.x + self.sliderButton.width/2 
		elseif math.ceil(self.sliderButton.olxLeft) > math.floor(self.width - self.sliderButton.width/2) then
			d()
			self.sliderButton.x = self.x + (self.width - self.sliderButton.width/2)
		end
		if self:getValue() > (self.max or 100) then 
			self.sliderButton.x = self.x + (self.width - self.sliderButton.width/2)
		end
	else
		if self.sliderButton.olyVert <= 0 then
			d()
			self.sliderButton.y = self.y+0.1
			self.sliderButton.olyVert = 0 
		elseif self.sliderButton.olyVert > (self.height) then
			d()
			self.sliderButton.y = self.y + (self.height)
		end
	end 
	self.sliderButton.dragEnabled = true
end 
function slider:draw()
	setColor(self.color)
	love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
	if self.sliderButton.vertOnly then 
		love.graphics.print(self:getValue(),self.x - font:getWidth(self:getValue())/2,self.y - font:getHeight()-5)
	else 
		love.graphics.print(self:getValue(),self.x + self.width/2,self.y)
	end
	setColor()
	
	if self.OnDraw then
		self.OnDraw()
	end
	drawChildren(self.children)
end 
function slider:update(dt)
	for i,v in pairs(self.children) do
		v:update(dt)
	end
	local olx = self.x
	local oly = self.y 
	local olw = self.width
	local olh = self.height
	
	olx = self.sliderButton.x - (olx - (olw/2 - self.width/2))
	oly = self.sliderButton.y - (oly - (olh/2 - self.height/2))
	if self.sliderButton.colision:collidesWith(self.interface.mxb) then 
		self.interface.sliderButton = self.sliderButton
	elseif self.interface.sliderButton == self.sliderButton then 
		self.interface.sliderButton = nil 
	end 
	
	self.sliderButton.olxLeft = olx - self.sliderButton.width/2
	self.sliderButton.olyVert = oly
	self:buttonRange()
	
	
	if self.OnUpdate then self.OnUpdate(dt) end
	local val = self:getValue()
	if self.OldValue ~= val then 
		if self.OnChange then self.OnChange(val) end 
		self.OldValue = val 
	end 
end
function slider:OnMove(x,y)
	self:setPos(x,y)
end 
function slider:remove()
	if self.interface.activeObject == self then
		self.interface.activeObject = nil
	end
	if self.children then
		for i,v in pairs(self.children) do
			v:remove()
		end
		self.children = {}
	end
	if not self.parent then 
		for i,v in pairs(self.interface.items) do
			if v == self then
				table.remove(self.interface.items,i)
				self.activeObject = nil
			end
		end
	else
		for i,v in pairs(self.parent.children) do
			if v == self then 
				table.remove(self.parent.children,i)
			end
		end
	end
end
function slider:mousepressed(b)
	local factor = self.stepSize or (self.max/100)*10 
	if self.integer then 
		factor = math.max(factor,1)
	end 
	local old = self.Old or self:getValue()
	if b == "wu" then 
		self.Old = old-factor
	elseif b == "wd" then 
		self.Old = old+factor
	end 
	if self.max and self.min then 
		if self.Old > self.max then 
			self.Old = self.max 
		elseif self.Old < self.min then 
			self.Old = self.min
		end 
		if self.Old ~= self.OOld then 
			self.OOld = self.Old
			self:setValue(self.Old )
		end 
	end 
end 
local invalidchars = {
"\a",
"\b",	
"\f",	
"\n",
"\r",
"\t",
"\v",
"\\",
"\'",
"/" ,
"*" ,
"<",
">" ,
"|",
}
local textinput = {}
local function parseText(t)
	local t = tostring(t) or "" 
	local text = {}
	local no = 0 
	for line in t:gmatch("[^\r\n]+") do 
		no = no + 1 
		text[no] = line		
	end 
	if t == "" then 
		text[1]  = ""
	end 
	return text 
end 
function ui:addTextinput(parent,x,y,w,h,text)
		local item = {
		text = text or "",
		x = x,
		y = y,
		ox = x,
		oy = y,
		interface  = self,
		zmap = 1,
		children = {},
		indicator = {
			textPos = 1,
			linePos = 1,
		},
		multiline = true,
		parent = parent,
		type = "menu",
		timer = 0,
		lineColor = colors.white,
		color = colors.gray,
		drawIndicator = true,
	}
	setmetatable(item,{__index = textinput})
	
	item.parsedtext = parseText(text or "Hello there")
	item:resetPos()
	self:initTasks(item)
	item.font = item.font or self.font or love.graphics.getFont()
	local frame = self:addFrame(nil,x,y,w,h,parent)
	
	frame.OnClick = function()
		item:click()
		item.lastPos = {item.indicator.textPos,item.indicator.linePos} 
	end
	local olup = frame.update
	function frame:update(dt)
		item:update(dt)
		if item.OnUpdate then 
			item.OnUpdate(dt)
		end 
		olup(frame,dt)
	end	
	frame.OnDraw = function()
		item:draw()
	end 
	item.mainFrame = frame 
	return item 
end 
function textinput:getLocalText()
	local x,y = self.mainFrame.colision:bbox()
	local font = self.font
	local fontH = font:getHeight()
	
	local availW,availH = self.mainFrame.width,self.mainFrame.height
	local lines = #self.parsedtext
	local visibleText = {}
	local indicator = self.indicator
	local textPos = indicator.textPos
	local linePos = indicator.linePos
	
	local heightCapacity = math.floor(availH/fontH)-1
	local relativePos = 0 
	
	local lq = math.max(0,(linePos) - (heightCapacity))
	self.lowerq = lq 
	self.higherq = math.floor(lq + heightCapacity)
	local no = 0
	for i = lq,math.floor(lq + heightCapacity),1 do
		local t = self.parsedtext[i]
		if t and t ~= "" then 
			if linePos == i then 
				relativePos = math.abs(no)
			end 
			no = no +1 
			t = string.gsub(t,"\n","")
			table.insert(visibleText,t.."\n")
		end 
	end		
	local lineText = self.parsedtext[indicator.linePos]
	lineText = lineText or "" 
	local before = string.utf8sub(lineText,0,textPos)
	if textPos == 0 then 
		before = ""
	end 
	local usedwidth = font:getWidth(before)
	local max = usedwidth + font:getWidth("A")
	if max > availW then 
		x = x - (max- availW)
	end 
	return visibleText,usedwidth,fontH,relativePos,x,y
end 
function textinput:draw()
	if self.text then 
		local oldFont = love.graphics.getFont()
		love.graphics.setFont(self.font)
		local visibleText,usedwidth,fontH,relativePos,x,y = self:getLocalText()
		love.graphics.setStencil(function()
			self.mainFrame.colision:draw("fill")
		end)
		setColor()
		love.graphics.print(table.concat(visibleText),x,y)
		if self.drawIndicator and self.active then 
			love.graphics.rectangle("fill",x + usedwidth,y + (fontH*(relativePos)),1,fontH)
		end 
		setColor()
		self.mainFrame.colision:draw("line")
		love.graphics.setStencil()
		love.graphics.setFont(oldFont)
	end 
end 
function textinput:getColision()
	return self.mainFrame.colision
end
function textinput:getPosFromCords(x,y)
	local col = self.mainFrame.colision
	local x,y = x,y 
	self:checkLinePos()
	if not x or y then 
		x,y = self.interface.mxb:center()
	end 
	if col:contains(x, y) then 
		local lx,ly = col:bbox()
		lx = findDistance(lx,x)
		ly = findDistance(ly,y)
		local font = self.font 
		local visibleText = self:getLocalText()
		local posx = 0 
		local posy = 1
		local no = 0
		for i = self.lowerq,self.higherq do 
			if self.parsedtext[i] then 
				no = no + 1 
				local py = font:getHeight()*no
				if ly < py then 
					posy = i
					break
				end 
			end 
		end
		self.indicator.linePos = posy or self.indicator.linePos
		
		local lineText = self.parsedtext[self.indicator.linePos]
		local totalLen = string.utf8len(lineText)		
		for i= totalLen,1,-1 do 
			local px = font:getWidth(string.utf8sub(lineText,totalLen-i,totalLen))
			if px < lx then
				posx = string.utf8len(string.utf8sub(lineText,totalLen-i,totalLen))
				break 
			end 
		end
		self.indicator.textPos = posx or self.indicator.textPos
	else 
	end 
end 
function textinput:resetPos()
	local maxPos = #self.parsedtext
	self.indicator.linePos = maxPos
	self.indicator.textPos = string.utf8len(self.parsedtext[maxPos])
end
function textinput:setMultiline()
	local a = deepCopy2(invalidchars)
	for i,v in ipairs(a) do 
		if v:find("\n") then 
			table.remove(a,i)
		end 
	end 
	self.multiline = true 
	self.disallowed = a
end 
function textinput:update(dt)
	self.text = tostring(self.text)
	if self.text ~= self.OldText then 
		self.parsedtext = parseText(self.text)
		if self.OnChange then self.OnChange(self.text) end 
		self.OldText = self.text
	end 
	self._blocked = false 
	self.x,self.y = self.mainFrame.x,self.mainFrame.y
	local t = table.concat(self.parsedtext)
	if t ~= self.old_parsedText then 
		if self.OnChange then self.OnChange(self:getText()) end 
		self.old_parsedText = t
	end 
	if self.drawIndicator then 
		self.timer = self.timer + dt 
		if self.timer > 0.2 then 
			self.drawIndicator = false 
			self.timer = 0 
		end 
	else 
		self.timer = self.timer + dt 
		if self.timer > 0.2 then 
			self.drawIndicator = true 
			self.timer = 0 
		end 
	end 
	local ctrlDown = love.keyboard.isDown("lctrl") or love.keyboard.isDown("rctrl")
	if ctrlDown then 
		if love.keyboard.isDown("a") then
			self.parsedtext = parseText("")
			self:resetPos()
		elseif love.keyboard.isDown("c") then 
			love.system.setClipboardText(table.concat(self.parsedtext))
		elseif love.keyboard.isDown("v") then 	
		end 
	end
	local o = LockKeypress
	LockKeypress = false
	local k = keys.ui[3].f() or alt_keys.ui[4].f()
	LockKeypress = o 
	if not self.mainFrame.colision:collidesWith(self.interface.mxb) and k then 
		if self.interface.textInputObject == self then 
			self.active = false
			self.interface.textInputObject = nil
			self._blocked = true
		end 
	end 
end 
function textinput:getText()
	local t = "" 
	for i,v in ipairs(self.parsedtext) do 
		local nt = string.gsub(v,"\n","")
		if t ~= "" then 
			t = t.."\n"..nt
		else 
			t = nt 
		end 
	end 
	return t
end
function textinput:moveTo(x,y)
	self.mainFrame:moveTo(x,y)
end 
function textinput:setAllowed(a)
	self.allowed = a
end 
function textinput:setFree()
	self.Free = true
end 
local function addText(self,t)

end 
local function getVert(self)
	local no = 0 
	for word in string.gmatch(self.text,"\n") do no = no + 1 end
	return no 
end 
function textinput:moveLine(bool)
	local indicator = self.indicator
	local linePos,textPos = indicator.linePos,indicator.textPos
	local lines = #self.parsedtext	
	if not bool then 
		indicator.linePos = linePos - 1
	else 
		indicator.linePos = linePos + 1
	end
	indicator.textPos = string.utf8len(self.parsedtext[indicator.linePos])	
	self:checkLinePos()
end
function textinput:checkLinePos()
	local lines = #self.parsedtext	
	if self.indicator.linePos < 1 then 
		self.indicator.linePos = 1 
	elseif self.indicator.linePos > lines then 
		self.indicator.linePos = lines
	end
end
function textinput:checkTextPos()
	
end 
function textinput:getStringBetween(a,b)

end 
function textinput:keypressed(key)
	local ctrlDown = love.keyboard.isDown("lctrl") or love.keyboard.isDown("rctrl")
	local indicator = self.indicator
	local changedT 
	local linePos,textPos = indicator.linePos,indicator.textPos
	local lines = #self.parsedtext
	if key == "backspace" then 
		changedT = true 
		if textPos > 0  or linePos > 1 then 
			local t = ""
			local textPos = math.max(0,self.indicator.textPos)
			local lineText = self.parsedtext[indicator.linePos]
			local before = string.utf8sub(lineText,0,textPos)
			local after = string.utf8sub(lineText,textPos+1)
			local char = string.utf8sub(before,string.utf8len(before))
			 t = string.utf8sub(before, 1,string.utf8len(before)-1)
			 indicator.textPos = indicator.textPos -1 
			 self.parsedtext[indicator.linePos] = t..after
			 if indicator.textPos <= 0 and indicator.linePos > 1 then 
				if after == "" then 
					table.remove(self.parsedtext,indicator.linePos)
				end 
				indicator.linePos = math.max(1,indicator.linePos -1)
				self:checkLinePos()
				indicator.textPos = string.utf8len(self.parsedtext[indicator.linePos])
				lines = #self.parsedtext
			 end 
		 end 
	elseif key == "return" then 
		changedT = true 
		local textPos = self.indicator.textPos
		local lineText = self.parsedtext[indicator.linePos]
		local before = string.utf8sub(lineText,0,textPos)
		local after = string.utf8sub(lineText,textPos+1)
		if indicator.textPos == 0 then 
			before = "\n" 
		end 
		self.parsedtext[linePos] = before
		local tail = "\n"..after
		indicator.textPos = 0
		table.insert(self.parsedtext,linePos+1,tail)
		indicator.linePos = linePos + 1
	elseif key == "left" then 
		indicator.textPos = textPos - 1
		if indicator.textPos < 1 then 
			if linePos -1 > 0  then 
				indicator.linePos = indicator.linePos-1
				indicator.textPos = string.utf8len(self.parsedtext[linePos-1])
			elseif indicator.textPos < 0 then 
				indicator.textPos = 0 
			end 
		end 
	elseif key == "tab" then 
		changedT = true 
		local textPos = math.max(0,self.indicator.textPos)
		if textPos == 0 then 
			self.parsedtext[indicator.linePos] = "\t"..self.parsedtext[indicator.linePos]
		else 
			local lineText = self.parsedtext[indicator.linePos]
			local before = string.utf8sub(lineText,0,textPos)
			local after = string.utf8sub(lineText,textPos+1)
			self.parsedtext[indicator.linePos] = before.."\t"..after
		end 
		indicator.textPos = indicator.textPos + 1 
	elseif key == "right" then 
		local len = string.utf8len(self.parsedtext[linePos])
		indicator.textPos = textPos + 1
		if indicator.textPos >=  len then 
			if linePos +1 <= lines then 
				indicator.linePos = linePos + 1
				indicator.textPos = 0 
			else 
				indicator.textPos = len
			end 
		end
	elseif key == "v" and ctrlDown then 
		changedT = true 
		local textPos = math.max(0,self.indicator.textPos)
		local linePos = self.indicator.linePos 
		local textbefore = {} 
		local textafter  = {} 
		local lines = #self.parsedtext
		for i=0,lines do
			if self.parsedtext[i] then 
				if i == linePos then 
					local textPos = math.max(0,self.indicator.textPos)
					local lineText = self.parsedtext[linePos]
					local before = string.utf8sub(lineText,0,textPos)
					local after = string.utf8sub(lineText,textPos+1)
					table.insert(textbefore,before..tostring(love.system.getClipboardText()))
					table.insert(textafter,after.."\n")
				elseif i > linePos then 
					table.insert(textafter,self.parsedtext[i].."\n")
				else 
					table.insert(textbefore,self.parsedtext[i].."\n")
				end 
			end 
		end 
		self.text = table.concat(textbefore)..table.concat(textafter)
		self.parsedtext = parseText(self.text)
		if not self.Free then 
			for i,v in ipairs(self.disallowed or invalidchars) do 
				self.text = string.gsub(self.text,v,"")
			end 
		end 
	elseif self.multiline then   
		if key == "up" then
			indicator.linePos = indicator.linePos -1 
			self:checkLinePos()
		elseif key == "down" then 
			indicator.linePos = indicator.linePos +1
			self:checkLinePos()
		end
	end
	lines = #self.parsedtext	
	local lineLenght = string.utf8len(self.parsedtext[indicator.linePos])
	if changedT then 
		local fn = self.OnChangeText or self.OnChange
		if fn then fn(table.concat(self.parsedtext)) end
	end 
end 
local text = ""
function textinput:textinput(t)
	local changedT
	local indicator = self.indicator
	if self.active then 
		if self.allowed then
			local b 
			for i,v in ipairs(self.allowed) do 
				if t == v then  
					b = true
					changeT	 = true 				
				end 
			end
			if not b then 
				changeT = false 
				t = ""
			end 
		else 
			changeT = true
		end 
		if changeT then 
			local textPos = math.max(0,self.indicator.textPos)
			if textPos == 0 then 
				self.parsedtext[indicator.linePos] = t..self.parsedtext[indicator.linePos]
				indicator.textPos = indicator.textPos + 1 
			else 
				local lineText = self.parsedtext[indicator.linePos]
				local before = string.utf8sub(lineText,0,textPos)
				local after = string.utf8sub(lineText,textPos+1)
				local s = pcall(love.graphics.print,t)
				if s then 
					self.parsedtext[indicator.linePos] = before..t..after
					indicator.textPos = indicator.textPos + 1 
				end 
			end 
		end 
		if not self.Free then 
			for i,char in ipairs(self.disallowed or invalidchars) do 
				for i,v in ipairs(self.parsedtext) do 
					self.parsedtext[i] = string.gsub(v,char,"")
				end 
			end 
		end 
		local fn = self.OnChangeText or self.OnChange
		if fn then fn(table.concat(self.parsedtext)) end
	end
end 
function textinput:remove()
	self.mainFrame:remove()
end 
function textinput:setText(text)
	self.text = text 
	self.parsedtext = parseText(text)
end 
local first 
function textinput:click()
	self.interface.textInputObject = self
	self:getPosFromCords()
	self.active = true
end 

local list = {}
function ui:addList(parent,x,y,w,h,items,horiz)
		local item = {
		text = text or "",
		x = x,
		y = y,
		ox = ox,
		oy = oy,
		interface  = self,
		pos = 0,
		gap = 5,
		zmap = 1,
		color = colors.grey,
		children = {},
		acItems = {},
		parent = parent,
		type = "menu",
		vert = true,
		horiz = horiz,
		color = colors.gray,
		items = {},
	}
	setmetatable(item,{__index = list})
	self:initTasks(item)
	local frame = self:addFrame(nil,x,y,w,h,parent)
	frame.OnUpdate = function(dt)
		item:update(dt)
	end 
	frame.OnDraw = function()
		setColor(item.color)
		frame.colision:draw("fill")
		setColor()
		item:draw()
	end 
	frame.color = colors.grey
	item.mainFrame = frame 	
	if items then 
		for i,v in ipairs(items) do 
			local v = items[i]
			local lastx,lasty = 2,2
			if items[i-1] then 
				lastx = items[i-1].lx
				lasty = items[i-1].ly
			end 
			local x,y = frame.width/2 - v.w/2,lasty
			local g = self:addButton(nil,x,y,v.w,v.h)
			if items.sort then items.sort(i,g) end
			if horiz then 
				items[i].lx = x + v.w + item.gap
				items[i].ly = y
			else 
				items[i].lx = x			
				items[i].ly = y + v.h + item.gap
			end 
			if v.func then 
				v.func(g)
			end 
			g.inList = true 
			g.oly = g.y
			g.oldX = x  
			g.oldY = y 
			g.dontDraw = true
			table.insert(item.items,g)
		end 
		function frame.OnMove(nx,ny) 
			for i,v in ipairs(item.items) do 
				local x = frame.x - (frame.width/2 - v.width/2) + v.oldX
				local y = frame.y - (frame.height/2 - v.height/2) + v.oldY
				v.x = x 
				v.y = y 
				v.oly = v.y
				v.olx = v.x 
			end 
		end 
	end 
	item:refresh()
	if not parent then
		table.insert(self.items,item)
	else
		item.parent = parent
		table.insert(parent.children,item)
	end
	return item 
end 
function list:getColision()
	return self.mainFrame.colision
end
function list:setHeader(text)
	if type(name) == "number" then 
		w = name 
		h = w 
		color = h 
	end 
	self.header = {
		w = w or self.width,
		h = h or 20,
		color = color or {155,155,155},
		text = text,
	}
	self.name = name or self.name 
end 
function list:addChoice(text,fn)
	local b = self.interface:addButton(nil,0,0,self.mainFrame.width - self.gap,self.interface:getFont():getHeight())
	b:setText(text)
	b.OnClick = fn 
	self:addItem(b)
	return b 
end 
function list:addItem(item,pos)
	if not self.items then 
		self.items = {}
	end 
	if self.OnAddItem then self.OnAddItem(item) end 
	if not pos then table.insert(self.items,item) else  table.insert(self.items,pos,item) end
	for i,v in pairs(self.interface.items) do 
		if v == item then 
			table.remove(self.interface.items,i)
			break
		end 		
	end 
	self:refresh()
end 
function list:setZmap(zmap)
	self.zmap = zmap 
	self.mainFrame.zmap = zmap 
end
function list:refresh()
	local frame = self.mainFrame
	frame._hidenItems = {} 
	for i,v in ipairs(self.items) do 
		local items = self.items
		local v = items[i]
		local lastx,lasty = 2,2
		if items[i-1] then 
			lastx = items[i-1].lx 
			lasty = items[i-1].ly
		end 
		local x,y = lastx + frame.width/2 - v.width/2,lasty
		if self.horiz then 
			x = lastx + v.width
			if i == 1 then 
				x = lastx 
			end 
		else 
			x = 2 
		end 
		table.insert(frame._hidenItems,v)
		v.dontDraw = true
		local item = v 
		local function notify(item)
			if item.children then 
				for i,v in ipairs(item.children) do 
					v.parentInsideList = self 
					notify(v)
				end 
			end 
		end
		notify(item)
		v.inList = true
		v.blocked = false
		local parent = frame
		item.x = parent.x - (parent.width/2 - item.width/2) + x
		item.y = parent.y - (parent.height/2 - item.height/2) + y
		item.oldX = x
		item.oldY = y 
		local g = v 
		if self.horiz then 
			items[i].lx = x + self.gap
			items[i].ly = y 
		else
			items[i].lx = x
			items[i].ly = y + v.height + self.gap
		end 
		g.oly = g.y
		g.olx = g.x
		g.zmapModif = 0 
	end 
	function self.mainFrame.OnMove(nx,ny) 
		for i,v in ipairs(self.items) do 
			local x = frame.x - (frame.width/2 - v.width/2) + v.oldX
			local y = frame.y - (frame.height/2 - v.height/2) + v.oldY
			v.x = x 
			v.y = y 
			v.olx = v.x
			v.oly = v.y
		end 
		if self.slider then self.slider:OnMove(frame.x-frame.width/2,frame.y-frame.height/2) end 
	end 
	if not self.goMin then self:goMax() end
	self:update(0.1)

	local leftover = -self:getLeftOver()
	if  leftover > 0 then 
		if not self.horiz then 
			local pos 
			local mf = self.mainFrame
			local slider = self.slider 
			if not slider then 
				slider = self.interface:addSlider(mf,mf.width+10,10,5,mf.height,"vert")
			end 
			slider:setRange(0,leftover)
			slider.OnChange = function(val)
				self.pos = -val 
			end
			slider.sliderButton.OnUpdate = function()
				if pos ~= self.pos then 
					slider:setValue(-self.pos)
					pos = self.pos 
				end
			end 
			self.slider = slider 
		end 
	else 
		if self.slider then self.slider:remove()  self.slider = nil  end 
	end 
end 
function list:setMode(horiz,vert)
	if vert then 
		self.horiz = false 
	elseif horiz then 
		self.horiz = true
	end 
	self:refresh()
end 
function list:getStencil()
	return function() self.mainFrame.colision:draw("fill") end 
end 
function list:render()
	if not self.canvas then 
		self.canvas = love.graphics.newCanvas(findDimensions(self.mainFrame.colision))
	end 
	
	self.canvas:clear()
	
	if not self.dontDraw then 
		local x,y = self.mainFrame.colision:bbox()
		
		love.graphics.setCanvas(self.canvas)
		love.graphics.push()
		love.graphics.translate(-x,-y)
		
		
		local stencil
		if not self.dontCensor then 
			love.graphics.setStencil(function() 
				self.mainFrame.colision:draw("fill")
			end) 
		end 
		drawChildren(self.acItems,function(v)
			if not v._inactive then 
				if not v.dontDrawL then 
					v:draw()
				elseif v.OnDraw then 
					v.OnDraw()
				end 
			end 
		end)
		
		love.graphics.setStencil(stencil)
			if self.sImage then 
					local x,y = self.mainFrame.colision:bbox()
					love.graphics.draw(self.sImage,x,y)
					end 
		for i,v in pairs(self.acItems) do 
			if v.tooltip then 
				v.tooltip:draw()
			end 
		end 
		if self.header then
			local olFont = love.graphics.getFont()
			love.graphics.setFont(self.textFont or self.interface.font or font)
			local text = self.header.text
			local font = self.textFont or self.interface.font or font 
			if type(text) == "table" then
				text = table.concat(text)
			end			
			local h = font:getHeight(text)
			local w = font:getWidth(text)
			setColor(self.headerColor or HeaderColor)
			local mf = self.mainFrame
			local ux,uy = mf.colision:bbox()
			local x,y = mf.x,mf.y
			local width,height = mf.width,mf.height 
			love.graphics.rectangle("fill",ux,uy,width,h-1)
			setColor(self.textColor)
			love.graphics.print(text,x - w/2,uy,r,sx,sy)
			love.graphics.setFont(olFont)
		end
		
		love.graphics.pop()
		love.graphics.setCanvas()
	
	end 
end 
function list:draw()
	local x,y = self.mainFrame.colision:bbox()
	love.graphics.draw(self.canvas,x,y)
end 
function list:clear()
	for i,v in ipairs(self.items) do 
		v:remove()
		self.items[i] = nil 
	end 
end 
function list:goMax()
	local height = 0
	local width  = 0 
	local horiz = self.horiz  	
	local leftOver 
	if horiz then 
		for i,v in ipairs(self.items) do 
			width = width +  v.width + self.gap 
		end 
		leftOver = self.mainFrame.width - width 
	else 
		for i,v in ipairs(self.items) do 
			height = height +  v.height + self.gap 
		end 
		leftOver = (self.mainFrame.height ) - height
	end 
	leftOver = leftOver - self.gap/2
	if leftOver < 0 then 
		self.pos = leftOver - self.gap
	else 
		self.pos = 0 
	end 
end
function list:getLeftOver()
	local height = 0
	local width  = 0 
	local horiz = self.horiz  	
	local leftOver 
	local headerh = 0 
	if self.header then 
		headerh = self.header.h
	end 
	if horiz then 
		for i,v in ipairs(self.items) do 
			width = width +  v.width + self.gap 
		end 
		leftOver = self.mainFrame.width - width 
	else 
		for i,v in ipairs(self.items) do 
			height = height +  v.height + self.gap
		end 
		leftOver = (self.mainFrame.height ) - height
	end 
	return leftOver
end 
function list:update(dt)
	local height = 0
	local width  = 0 
	local horiz = self.horiz  	
	local leftOver 
	local headerh = 0 
	if self.header then 
		headerh = self.header.h
	end 
	if horiz then 
		for i,v in ipairs(self.items) do 
			width = width +  v.width + self.gap 
		end 
		leftOver = self.mainFrame.width - width 
	else 
		for i,v in ipairs(self.items) do 
			height = height +  v.height + self.gap
		end 
		leftOver = (self.mainFrame.height ) - height
	end 
	leftOver = leftOver + self.gap
	if leftOver < headerh then 
		if self.pos > headerh then 
			self.pos = headerh
		elseif self.pos < (leftOver - self.gap) then 
			self.pos = leftOver - self.gap
		end 
	else 
		self.pos = 0 
	end 
	self.height = height
	self.width = width 
	for i,v in pairs(self.acItems) do 
		v:update(dt)
		self.acItems[i] = nil 
	end 
	local focus 
		local Active = self.mainFrame.colision:collidesWith(self.interface:getmxb()) 
		if not Active then 
			for i,v in pairs(self.items) do 
				v._inactive = true 
				v._blockCol = true 
				v.zmap = 0 
			end 
			if self.interface.ActiveList == self then 
				self.interface.ActiveList = nil
			end 
		else 
			local activeList = self.interface.ActiveList
			if (activeList and activeList.zmap < self.zmap) or not activeList then 
				self.interface.ActiveList = self
			end 
		end 
		for i,v in pairs(self.items) do  
			v._inactive = true
			v._blockCol = true
			v.zmap = 0 
			if horiz then 
				local width = self.mainFrame.width 
				v:moveTo(v.olx + self.pos,v.y)
			else 
				v:moveTo(v.olx,v.oly + self.pos)
			end 
			if v._HasFocus then 
				focus = true 
			end 
			if self.mainFrame.colision:collidesWith(v.colision) then 
				v._inactive = false
				if Active then 
					v._blockCol = false
					v.colision._blockCol = false 
				else 
					v._blockCol = true
					v.colision._blockCol = true
					if self.interface.activeObject == v then 
						self.interface.activeObject = nil
					end 
				end 
				v.zmap = self.mainFrame.zmap +2 + (v.zmapModif or 0) 
				table.insert(self.acItems,v)
			else 
				v._inactive = true 
				v._blockCol = true 
			end 
		end 
	if not focus then 
		focus = self.mainFrame._HasFocus
	end 
	self:render()
end 
function list:removeItem(item,r)
	removeFromTable(self.items,item)
	if not r then item:remove() else if item.OnFocusLost then item.OnFocusLost() end end 
	self:refresh()
end 
function list:mousepressed(x,y,b)
	local horiz = self.horiz
	if not horiz then 
		if b == "wu" then 
			self.pos = self.pos + math.min(self.height/15,50)
		elseif b == "wd" then 
			self.pos = self.pos - math.min(self.height/15,50)
		end 
	else 
		if b == "wu" then 
			self.pos = self.pos + math.min(self.width/15,50)
		elseif b == "wd" then 
			self.pos = self.pos - math.min(self.width/15,50)
		end 
	end 
end 
function list:remove()
	for i,v in pairs(self.mainFrame._hidenItems) do 
		v:remove()
	end 
	self.mainFrame:remove()
	if self.interface.ActiveList == self then
		self.interface.ActiveList = nil
	end
	if self.children then
		for i,v in pairs(self.children) do
			v:remove()
		end
		self.children = {}
	end
	if not self.parent then 
		for i,v in pairs(self.interface.items) do
			if v == self then
				table.remove(self.interface.items,i)
				self.activeObject = nil
			end
		end
	else
		for i,v in pairs(self.parent.children) do
			if v == self then 
				table.remove(self.parent.children,i)
			end
		end
	end
end 
 

function ui:Hcol(dt,a,b)
	if not self.LkeyHold and not self.alt_LkeyHold then 
		local other
		if a == self.mxb then
			other = b
		elseif b == self.mxb then
			other = a
		end
		if other then
			for i,v in pairs(self.colision) do
				if other == v and not v._inactive and not v._blockCol then
					local clamp  
					if self.activeObject and self.activeObject ~= v.source and not self.activeObject.inactive then
						if not self.activeObject.colision._blockCol and self.activeObject.zmap < v.source.zmap then
							if self.activeObject.OnFocusLost then
								self.activeObject.OnFocusLost(self.activeObject)
							end
							self.activeObject._HasFocus = false 
							self.activeObject = nil
						end
					end
					local clamp = true 
					if  v.source.permaInactive and self.help then
						clamp = false 
					elseif not v.source.permaInactive then 
						clamp = false 
					end 
					if not v.source.inactive and not clamp and not self.activeObject  then
						self.activeObject = v.source
						if not v.source.___done then 
							if v.source.OnFocus then v.source.OnFocus(v.source) end 
							v.source.___done = true
						end 
						v.source._HasFocus = true
					end
				end
			end
		end
	end 
end
function ui:Hstop(dt,a,b)
	if not self.LkeyHold and not self.alt_LkeyHold then 
		if not self.paused then 
			local other
			if a == self.mxb then
				other = b
			elseif b == self.mxb then
				other = a
			end
			if other then
				for i,v in pairs(self.colision) do
					if other == v then
						if self.activeObject then
							if self.activeObject.OnFocusLost and not self.activeObject.inactive then  self.activeObject.OnFocusLost(v.source) end 
							self.activeObject._HasFocus = false 
							self.activeObject.___done = false
							self.activeObject = nil
						end
					end
				end
			end
		end 
	end
end
function ui:keypressed(...)
	if self.activeObject and self.activeObject.keypressed then
		self.activeObject:keypressed(...)
	end
	if self.textInputObject then self.textInputObject:keypressed(...) end
end
function ui:keyreleased(...)
	if self.activeObject and self.activeObject.keyreleassed then
		self.activeObject:keyreleassed(...)
	end
end
function ui:mousepressedList(x,y,b)
	if self.ActiveList then self.ActiveList:mousepressed(x,y,b) end
	if self.ActiveSlider then self.ActiveSlider:mousepressed(b) end 
end 
function ui:mousepressed(x,y,b,z)
	if not self.paused then  
		if self.activeObject and not self.activeObject.inactive and not self.activeObject.blocked then 
			self.pressedObject = self.activeObject
			if (self.LkeyHold == nil and self.RkeyHold == nil) or z then 
				if self.activeObject.mousepressed then self.activeObject:mousepressed(x,y,b)	end
				if self.activeObject and not self.activeObject.inactive and self.activeObject.dragEnabled then 
				if self.activeObject.isWindow then 
					if self.activeObject.setZmap then 
						self.activeObject:setZmap(self.max_zmap+1) 
					else 
						self.activeObject.zmap = self.max_zmap +1  
					end 					
					self.max_zmap = self.activeObject.zmap
						local obj = self.activeObject
						for i,v in ipairs(obj.children) do 
							local nzmap = self.max_zmap + findDistance(obj.zmap,v.zmap) 
							self.max_zmap = math.max(nzmap,self.max_zmap)+1 
							if v.setZmap then v:setZmap(nzmap)
							else v.zmap = nzmap 
							end 
						end 
					end 
				local x,y = self.mxb:center()
					if not self.activeObject.nx and not self.activeObject.ny then 
						if not self.activeObject.vertOnly then 
							self.activeObject.nx = (self.activeObject.x) - x
						end 
						if not self.activeObject.horizOnly then 
							self.activeObject.ny = (self.activeObject.y) - y
						end 
					end
				end	
			end		
		end
	end 
end
function ui:mousereleased(x,y,b,z)
	if not self.paused then 
		if self.pressedObject and  self.pressedObject.mousereleassed and z then 
			self.pressedObject:mousereleassed(x,y,b,z)
			self.pressedObject = nil
		end
	end 
end
function ui:update(dt)
	tweens:update(dt)
	tween.update(dt)
	local x,y = love.mouse.getPosition()
	x = self.mousex or x
	y = self.mousey or y
	self.mxb:moveTo(x,y)
	for i,v in pairs(self.items) do
		v:update(dt)
	end
	if self.activeObject then
		local cond = love.mouse.isDown("l")
		if self.LkeyHold ~= nil or self.alt_LkeyHold ~= nil then 
			cond = self.LkeyHold or self.alt_LkeyHold
		end 
		local activeObj = self.activeObject or self.sliderButton
		if self.activeObject.dragEnabled then
			local x,y = self.mxb:center()
			if cond  then 
				if self.activeObject.nx then 
					self.activeObject.x = (self.activeObject.nx) + x
				end 
				if self.activeObject.ny then 
					self.activeObject.y = (self.activeObject.ny) + y
				end 
				if self.tile_increment then
					self.activeObject.x,self.activeObject.y = incrementPos(self.activeObject.x,self.activeObject.y,self.activeObject.width/2,self.activeObject.height/2)
				end
				if self.activeObject.OnMove then 
					self.activeObject.OnMove(self.activeObject.x,self.activeObject.y)
				end 
			else 
				self.activeObject.nx = nil
				self.activeObject.ny = nil
			end 
		end		
		if not cond and self.activeObject then 
			if not self.mxb:collidesWith(self.activeObject:getColision()) then
				if self.activeObject.OnFocusLost and not self.activeObject.inactive then  self.activeObject.OnFocusLost(self.activeObject.source) end 
				self.activeObject._HasFocus = false 
				self.activeObject.___done = false
				self.activeObject = nil
			end 
		end 
	end
	self:navi()
end
function ui:navi()
	if self.LkeyHold or self.RkeyHold or self.alt_LkeyHold or self.alt_RkeyHold or self.mmup or self.mmdown then 
		self.LastFramePress = true
	else
		self.LastFramePress = false
	end 
	if self._LastFramePress ~= self.LastFramePress then 
		local x,y = love.mouse.getPosition()
		local b
		if self.LkeyHold or self.alt_LkeyHold then 
			b = "l"
		elseif self.RkeyHold or self.alt_RkeyHold then 
			b = "r"
		elseif self.mmup then 
			b = "wu"
		elseif self.mmdown then 
			b = "wd" 
		end 
		if self.LastFramePress then 

			self:mousepressed(x,y,b,true)
		else 
			self:mousereleased(x,y,b,true)
		end 
		self._LastFramePress = self.LastFramePress
	end 
end 
function ui:getZorder(tables)
  local a = {}
  for i,n in pairs(tables) do table.insert(a, n) end
  table.sort(a,function(a,b)
	return a.zmap < b.zmap 
  end)
  return a
end
function ui:draw(debugs)
	local olf = love.graphics.getFont()
	love.graphics.setFont(self.font)
	for i,v in pairs(self:getZorder(self.items)) do 
		if not v.inList then 
			if v.dontDraw then 
				if v.OnDraw then v.OnDraw(v) end 
				if v.DrawChildren then 
					drawChildren(v.children)
				end 
			else 
				v:draw()
			end
			setColor()
		end
	end
	if debugs then 
		self.mxb:draw()
		for i,v in pairs(self.colision) do
			v:draw("line")
		end
		love.graphics.print(tostring(self.activeObject).."  "..tostring(tablelenght(Collider._active_shapes).." "..math.ceil(collectgarbage("count"))),20,20)
	end
	love.graphics.setFont(olf)
end

function ui:getItemCount()
	local total = 0 
	local function count(items)
		for i,v in pairs(items) do 
			total = total + 1 
			if v.children then 
				count(v.children)
			end 
		end 
	end 
	count(self.items)
	return total
end 
function ui:clear(remXMB)
	if not remXMB then 
		self.Collider:remove(self.mxb)
	end
	for i,v in pairs(self.colision) do
		self.Collider:remove(v)
	end
	for i,v in pairs(self.items) do 
		if v.remove then self.items[i]:remove() end
		self.items[i] = nil
	end
	self.colision = {}
	self.items = {}
end
function ui:textinput(...)
	if self.textInputObject then self.textInputObject:textinput(...) end
end 
function ui:getmxb()
	return self.mxb
end
return ui