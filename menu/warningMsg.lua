local warning = {}
local meta = {__index = warning}
function warning:new(ui,msg,onconfirm,oncancel)
	local self = {}
	setmetatable(self,meta)
	
	local uiz = ui.zmap + 1 
	local font = love.graphics.newFont(14*math.max(px,py))
	local standardW,standarH = 300*px,300*py
	local w,h = love.graphics.getDimensions()
	local mainframe = ui:addFrame(nil,w/2,h/2,standardW,standarH)
	mainframe.isWindow = true 
	mainframe.color = colors.white
	local header = ui:addButton(mainframe,0,0,standardW,40)
	header:setText("WARNING!")
	header.color = colors.gray
	header.textColor = colors.yellow
	header.dontDrawB = true
	header.textFont = love.graphics.newFont(26)
	
	local gapx = 10*px
	local gapy = 20*py
	
	local width,height = standardW - gapx*2,standarH - ((25 + 100)*py)
	local x,y = gapx,45*px

	local info = ui:addButton(mainframe,x,y,width,height)
	info.dontDraw = true 
	
	info.OnDraw = function()
		local olf = love.graphics.getFont()
		love.graphics.setFont(font)
		
		love.graphics.setColor(colors.red)
		info.colision:draw("line")
		love.graphics.setColor(colors.black)
		
		love.graphics.printf(msg,info.x - info.width/2,info.y - info.height/2 + 10*py,info.width,"center")
		
		love.graphics.setColor(colors.white)
		love.graphics.setFont(olf)
	end 
	
	local bw,bh = (standardW - (100*px) + (gapx*2))/2,(50*py)
	local confirm = ui:addButton(mainframe,gapx,standarH - gapy - bh,bw,bh)
	confirm:setText("Confirm")
	confirm.OnClick = function() onconfirm(self) self:remove() end
	confirm.dontDrawB = true 
	confirm.color = {0,155,0}
	local cancel = ui:addButton(mainframe,standardW - (gapx + bw),standarH - gapy - bh,bw,bh)
	cancel.OnClick = function() if oncancel then oncancel(self) end self:remove() end
	cancel:setText("Cancel")
	cancel.dontDrawB = true 
	cancel.color = {155,0,0}
	self.mainframe = mainframe
	return self
end 
function warning:remove()
	self.mainframe:remove()
	if self.OnRemove then self.OnRemove() end
end 
return warning