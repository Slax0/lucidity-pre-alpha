local menu = {}
local _PACKAGE = getPackage(...)
local Timer =  require "hump.timer"
local ui = require(_PACKAGE .. '.ui')
local options = require("mainMenu/options")

local function lockButtons(self,bool)
	for i,v in ipairs(self.item) do 
		v:setInactive(bool)
	end 
end 
function menu:navigate(key,alt)
	local a = key[1] or alt[1]
	local b = key[2] or alt[2]
	local c = key[3] or alt[3]
	
	self.ui:setLeftClick(keys.ui[3].f(), alt_keys.ui[3].f())
	self.ui:setRightClick(keys.ui[4].f(), alt_keys.ui[4].f())
	local index = self.index
	if not self.options then 
		if c and not love.mouse.isDown("l") then
			self.item[self.index].OnClick()
		end
		if b then
			self.index = self.index - 1
		elseif a then
			self.index = self.index + 1
		end
		if self.index > self.itemNo then
			self.index = 1
		elseif self.index < 1 then
			self.index = self.itemNo
		end
		if index ~= self.index then
			self.item[self.index].OnFocus()
			self.activeItem.OnFocusLost()
			self.activeItem = self.item[self.index]
		end
	else
		-- self.options:keyreleased(key)
	end
end

function menu:init(text,func)
	local object = {
		item = {},
		maxWidth = 0,
		font = {},
		screen = {},
		index = 1,
	}
	setmetatable(object, { __index = menu })
	local self = object
	

	
	for i,v in ipairs(self.item) do 
		v:setInactive(false)
	end
	self.width,self.height = love.graphics.getDimensions()
		self.text = {
			"Settings",
			"Main Menu",
			"Return to desktop",
			"Return to game",
		}
	
	
		self.functions = {
		function()
				self.options = options:enter(self,800/self.width,600/self.height,Collider)
				self.options.OnRemove = function()
					lockButtons(self,false)
					self.options = nil
				end
			end,
		function()
			self.warning = warning:new(self.ui,"Progress will not be saved and you will return to the Title screen.\n\n Are you sure?!",function() 			
				current_gamestate = "menu"
				Gamestate.switch(mainMenu)
			end,function() lockButtons(self,false) end )
		end,
		function()
				warning:new(self.ui,"Progress will not be saved and you will return to the Desktop.\n\n Are you sure?!",function()
				love.event.push("quit")
			end,function() lockButtons(self,false) end )
		end,
		function()
			if self.OnRelease then self.OnRelease() end
		end 
		}
		if text then 
			for i,v in ipairs(text) do 
				table.insert(self.text,v)  
			end 
		end 
		if func then 
			for i,v in ipairs(func) do 
				table.insert(self.functions,v)
			end 
		end 
	self.itemNo = tableLenght(self.text)
	local function HCol(dt,a,b)
		self.ui:Hcol(dt,a,b)
	end
		local function Hstop(...)
		self.ui:Hstop(...)
	end
	self.Collider = HC(200, HCol)
	self.Collider:setCallbacks(nil, Hstop)

	local Collider = self.Collider
	
	self.ui = ui:init(Collider)
	
	local _ = {}
	local w,h = love.graphics.getDimensions()
	

	local bw,bh = 230,30
	local gapy = 10
	
	local fh = gapy + (bh+gapy)*#self.text
	local frame = self.ui:addFrame(nil,w/2,h/2,280,fh)
	frame.color = {55,55,55,155}
	self.hasFocus = 0
	for i,v in ipairs(object.text) do
		local w,h = 230,30
		local a = self.ui:addButton(frame,25,gapy + ((h+gapy)*(i-1)),w,h)
		local fontSize = 16
		a:setText(v)
		a.color = {225,225,225,200}
		a.OnFocus = function()
			self.index = i
			if self.activeItem ~= a then 
				self.activeItem.OnFocusLost()
			end
			a.posInd = {
				x = a.x + a.width/2 + 10,
				y = a.y,
				ix = a.x + a.width/2 + 10,
				finished = 1,
				transp = 0,
			}
			a.posInd2 = {
				x = a.x - a.width/2 - 10,
				y = a.y,
				ix = a.x - a.width/2 - 10,
				finished = 1,
			} 
			a.OnDraw = function()
				local w,h = icons.indicator_red:getDimensions()
				love.graphics.setColor(255,255,255,a.posInd.transp)
				love.graphics.draw(icons.indicator_red,a.posInd.x,a.posInd.y,0,-1,1,w/2,h/2)
				love.graphics.draw(icons.indicator_red,a.posInd2.x,a.posInd2.y,0,1,1,w/2,h/2)
				love.graphics.setColor(255,255,255)
			end
			a.OnUpdate = function()
				self.hasFocus = 1
				local v = a.posInd
				local variation = 10
				local mode = nil
				if not v.visible then 
					Timer.tween(0.2,v,{transp = 255},"in-quad")
					v.visible = true
				end
				if v.finished == 1 then
					v.tween = Timer.tween(0.5,v,{x = v.x + variation},mode,function() v.finished = 2 end)
					v.finished = 0
				elseif v.finished == 2 then 
					v.tween = Timer.tween(0.5,v,{x = v.ix},mode,function() v.finished = 1 end)
					v.finished = 0
				end
				local v = a.posInd2
				if v.finished == 1 then
					v.tween = Timer.tween(0.5,v,{x = v.x - variation},mode,function() v.finished = 2 end)
					v.finished = 0
				elseif v.finished == 2 then 
					v.tween = Timer.tween(0.5,v,{x = v.ix},mode,function() v.finished = 1 end)
					v.finished = 0
				end
			end
		end
		a.OnFocusLost = function()
			a.OnDraw = nil
			a.OnUpdate = nil
			a.posInd = nil
			a.posInd2 = nil
			collectgarbage()
		end
		table.insert(self.item,a)
	end
	
	for i,v in ipairs(self.item) do 
		self.item[i].OnClick = function()lockButtons(self,true) self.functions[i]() end 
	end 
	object.screen = love.graphics.newImage(love.graphics.newScreenshot())
	object.itemNo = tableLenght(object.text)
	local maxTable = {}
	for i in ipairs(object.text) do
		
	end
	self.keyControler = keyControl:new("ui") 

	return object
end
function menu:start()
	lockButtons(self,false)
	if self.options then 
		self.options:leave()
		self.options = nil
	end
	self.screen = love.graphics.newImage(love.graphics.newScreenshot())
	self.oldFont = love.graphics.getFont()
	self.active = true
end
function menu:ends()
	if self.options then 
		self.options:leave()
		self.options = nil
	end
	if self.warning then self.warning:remove() end
end
function menu:getUI()
	return ui
end
function menu:update(dt)
	if self.warning and not self.warning.OnRemove then self.warning.OnRemove = function() self.warning = nil end end 
	self.keyControler:attach(dt)
	
	self.hasFocus = 0
	self.ui:update(dt)
	local index = self.index
	if self.hasFocus == 0 and not self.options then
		self.activeItem = self.item[self.index]
		self.item[self.index].OnFocus()
	end
	Timer.update(dt)
	
	
	self.Collider:update(dt)
	self:navigate(self.keyControler:unpack())
	
	self.keyControler:detach()
end
function menu:draw()
	love.graphics.draw(self.screen,0,0)
	local width = love.graphics.getWidth()
	local height = love.graphics.getHeight()
	love.graphics.setColor(155,155,155,155)
	love.graphics.rectangle("fill",0,0,width,height)
	love.graphics.setColor(255,255,255,255)
	local itemNo = self.itemNo
	self.ui:draw()
end

function menu:setParams(text,func)
	self.text = text
	self.item = {}
	for i in ipairs(self.text) do
		self.item[i] = {}
		self.item[i].func = func[i] or function() end
	end
end
function menu:HCol(...)
	self.ui:Hcol(...)
end
function menu:Hstop(...)
	self.ui:Hstop(...)
end
function menu:keypressed(key)
	local index = self.index
	if not self.options then 

	else
		self.options:keypressed(key)
	end
end
function menu:mousepressed(...)
	self.ui:mousepressed(...)
	if self.options then
		self.options:mousepressed(...)
	else
	end
end
function menu:gamepadpressed(...)
	if self.options then
		self.options:gamepadpressed(...)
	else
	end 
end
function menu:joystickpressed(...)
	if self.options then
		self.options:joystickpressed(...)
	else
	end
end
function menu:mousereleased(...)
	self.ui:mousereleased(...)
end
function menu:keyreleased(...)
	self.ui:keyreleased(...)
end
function menu:remove()
	for i,v in ipairs(self.item) do 
		v:remove()
	end
	self.Collider:clear()
	self.ui:clear()
	self.ui = nil
	for i in ipairs(self) do
		self[i] = nil
	end
	return self
end
return menu