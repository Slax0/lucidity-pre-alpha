local keyControler = {}
function keyControler:new(stage)
	local self = {
		pressedKeys 	= {},
		keyClamp 		= {},
		keytable = keys[stage],
		alt_pressedKeys = {},
		alt_keyClamp 	= {},
		stage = stage,
		alt_keytable = alt_keys[stage]
	}
	for i,v in ipairs(keys[stage]) do 
		self.pressedKeys[i] 		= false
		self.alt_pressedKeys[i] 	= false
	end
	return setmetatable(self, { __index = keyControler })
end 
function keyControler:newHidden(tables)
	local self = {
		pressedKeys 	= {},
		keyClamp 		= {},
		keytable = tables,
		hidden = true,
	}
	for i,v in ipairs(self.keytable) do 
		self.pressedKeys[i] = false
	end
	return setmetatable(self, { __index = keyControler })
end 
function keyControler:attach(dt)
	if not self.hidden then 
		self.keytable = keys[self.stage]
		self.alt_keytable = alt_keys[self.stage]
	end 
	for i,v in ipairs(self.keytable) do 
		local a = v.f()
		if not self.keyClamp[i] and a then
			self.pressedKeys[i] = true
			self.keyClamp[i] = true
		end
		if not a then 
			self.keyClamp[i] = false
		end
	end
	if self.alt_keytable then 
		for i,v in ipairs(self.alt_keytable) do 
			if v.f then 
				local a = v.f()
				if not self.alt_keyClamp[i] and a then
					self.alt_pressedKeys[i] = true
					self.alt_keyClamp[i] = true
				end
				if not a then 
					self.alt_keyClamp[i] = false
				end
			end 
		end
	end 
end 
function keyControler:unpack()
	return self.pressedKeys, self.alt_pressedKeys
end
function keyControler:queryKey(name,func)
	local found,isDown,no,tables = queryKey(name) 
	local found,isDown 
	local keytable = self.keytable
	local alt_keytable = self.alt_keytable
	if no and tables and tables[no].f() then 
		self:attach()
		local once 
		if tables == keytable then 
			if self.pressedKeys[no] then 
				if not once then 
					func()
					once = true 
				end 
			end 
		elseif tables == alt_keytable then 
			if self.alt_pressedKeys[no] then 
				if not once then 
					func()
					once = true 
				end 
			end 
		end 
		self:detach()
	end 
	return found,isDown 
end 
function keyControler:detach()
	for i,v in pairs(self.pressedKeys) do 
		self.pressedKeys[i] = false
	end
	if self.alt_pressedKeys then 
		for i,v in pairs(self.alt_pressedKeys) do 
			self.alt_pressedKeys[i] = false
		end
	end 
end 
return keyControler