local tweens = {}

local function fireWorks(fluxGroup,size,height,burstSize,color,oncomplete)
	local f = fluxGroup
	local squares = {}
	local size = size or 5 
	local minSize = 20
	local function tween(square,y)
		local r,g,b
		if not color then 
			r = math.random(0,255)
			g = math.random(0,255)
			b = math.random(0,255)
		else 
			r = color[1]
			g = color[2]
			b = color[3]
		end 
		fluxGroup:to(square,3,{y = -height,a = 255})
		:oncomplete(function() 
			square.r = r 
			square.g = g 
			square.b = b
		end)
		:after(square,1,{x = math.random(5,(burstSize + minSize)) - (burstSize + minSize)/2,y = -height + math.random(-burstSize)})
		:after(square,2,{a = 0,y = square.y - math.random(0,1)})
		:delay(math.random(0.0003,0.003))
		:oncomplete(function() 
			if oncomplete then 
				no = no + 1
				if no == #squares then 
					oncomplete()
				end
			end
		end)
	end 
	for i = 1,burstSize + minSize do 
		local color = colors.yellow
		local size = math.random(1,size/2)
		local square = {x = 0,y = 0,mode = math.random(1,2),r = color[1],g = color[2],b = color[3],a = 255,size = size}
		table.insert(squares,square)
		tween(square)
	end 
	local function draw(x,y)
		for i,v in ipairs(squares) do
			local mode = v.mode 
			love.graphics.setColor(v.r,v.g,v.b,v.a)
			if mode == 1 then 
				love.graphics.circle("fill",x + v.x,y+v.y,v.size)
			else
				love.graphics.rectangle("fill",x + v.x,y+v.y,v.size,v.size)
			end
			love.graphics.setColor(colors.white)
		end 
	end 
	return draw
end 
tweens["fireWorks"] = fireWorks

return tweens