local loadGhost = {}
local _PACKAGE = "mobs"
local loaded = {} 
local loaded_items = {} 
function loadGhost:new(name,params,mode,dir)
	local parentPath = dir or "ghosts"
	local ghost = false
	if not mode then 
		mode = "write"
	end 	
	path = loaded[name]
	if not  path then 
		path = findItem("ghost",name,parentPath)
		loaded[name] = path 
	end 
	print(path)
	local chunk,err = love.filesystem.load(path.."/load.lua")
	if not chunk then 
		return err
	end 
	ghost = chunk(path)
	ghost.name = name
	ghost.width = ghost.w
	ghost.height = ghost.h
	ghost.info.hp = ghost.info.maxHp
	ghost.info.mp = ghost.info.maxMp
	ghost.info.exp = 0 
	assert(ghost,"Couldn't find ghost by the name of: "..tostring(name)) 
	if params then 
		for i,v in pairs(ghost.info) do 
			if params[i] then 
				if mode == "write" then
					ghost.info[i] = params[i]
				elseif mode == "add" then 
					if type(ghost.info[i]) ~= type(params[i]) then 
						error("Type mismatch: Ghost:"..type(ghost.info[i]).." Param:"..type(params[i]))
					end 
					if type(ghost.info[i]) == "table" then 
						table.insert(ghost.info[i],params[i])
					elseif type(ghost.info[i]) == "number" then 
						ghost.info[i] = ghost.info[i] + params[i]
					end 
				end 
			end 
		end
	end
	if not ghost.stillFrame then ghost.stillFrame = 1 end
	ghost.items = {}
	if not ghost.info.statIncrease then 
		ghost.info.statIncrease = {
			maxHp = 5,
			maxMp = 2,
			def = 1,
			atk = 1,
			luck = 1,
		}
	end 	
	ghost.meta = ghost 
	setmetatable(ghost,{__index = loadGhost})
	return ghost
end
function loadGhost:giveItem(name)
	local path = findItem("items",name)
	loaded_items[name] = path 
	local chunk,err = love.filesystem.load(path.."/load.lua")
	if not chunk then 
		return err
	end
	local info = chunk()
	self:setParams(info,"add")
	table.insert(self.items,name)
end 
function loadGhost:setParams(params,mode)
	local ghost = self
	if not mode then 
		mode = "write"
	end 
	if params then 
		for i,v in pairs(ghost.info) do 
			if params[i] then 
				if mode == "write" then
					ghost.info[i] = params[i]
				elseif mode == "add" then 
					if type(ghost.info[i]) ~= type(params[i]) then 
						error("Type mismatch: Ghost:"..type(ghost.info[i]).." Param:"..type(params[i]))
					end 
					if type(ghost.info[i]) == "table" then 
						for a,s in ipairs(params[i]) do 
							table.insert(ghost.info[i],s)
						end 
					elseif type(ghost.info[i]) == "number" then 
						ghost.info[i] = ghost.info[i] + params[i]
					end 
				end 
			end 
		end
	end
end
function loadGhost:byName(name)
	local data = {} 
	data.name = name
	local mob = loadGhost:new(name)
	if mob then 
		data._backupInfo = {
			name = name,
			dir = dir,
		}
		data.width = mob.width
		data.height = mob.height
		data.info = {
			hp = mob.hp,
			mp = mob.mp,
			maxHp = mob.maxHp,
			maxMp = mob.maxMp,
			def = mob.def,
			atk = mob.atk,
			luck = mob.luck,
			level = mob.level,
			element = mob.element,
		}
		data.image = mob.image
		data.state = "idle"
		data.scalex = mob.scale or 1 
		data.scaley = mob.scale  or 1 
		data.mirror = mob.mirror
		data.states = mob.states
	end	
	return mob,data 
end
function loadGhost:getName()
	print(self.meta)
	return self.meta.user_name or self.info.name
end 
function loadGhost:setUsername(name)
	print("setting name:",name)
	self.user_name = name 
end 
function loadGhost:getStory()
	return self.info.story or self.info.user_story 
end 
function loadGhost:setStory(s)
	self.info.user_story = s 
end 
function loadGhost:draw(...)
	local arg = {...} 
	if not self.unpacked  then 
		for i,v in ipairs(self.states) do
			local a,s,d,f,g,h = unpack(v.anim)
			v.anim = newAnimation(self.image,a,s,d,f,g,h)
		end 
		self.unpacked = true
		self.state = "idle"
	end 
	for i,v in ipairs(self.states) do
		if v.name == "idle" then
			anim = v.anim
		end
	end
	self.Pause = true
	if not anim.Enabled then anim:seek(self.stillFrame or 1) end 
	setColor(255,255,255,255)
	anim:draw(...)
	self._Anim = anim
end 
function loadGhost:update(dt)
	if self._Anim then 
		self._Anim.Enabled = true
		self._Anim:update(dt)
	end 
end
function loadGhost:getAnimation()
	local anim 
	for i,v in ipairs(self.states) do
		if v.name == "idle" then
			anim = v.anim
		end
	end
	self._Anim = anim
	return anim 
end 
function loadGhost:getDimensions()
	return self.width*math.abs(self.scale or 1),self.height*math.abs(self.scale or 1)
end 
function loadGhost:getExpNeeded()
	local x = self.info.level
	local expToLevel = math.floor(math.log(x)*60 + 30+(x^2/5))
	return expToLevel - self.info.exp 
end
function loadGhost:save(mode,map) 
	if self.info.hp > 0 then 
		local v = self
		local hp = v.info.maxHp
		local mp = v.info.maxMp
		local items = v.items 
		if mode == "checkpoint" then 
			hp = v.info.hp
			mp = v.info.mp
		end 
		local a = {
			name = v.name,
			hp = hp,
			mp = mp,
			items = items,
			user_name = v.meta.user_name,
			story = v.meta.user_story,
		}
		return a
	end 
end 
function loadGhost:load(t)
	local name = t.name 
	local hp = t.hp 
	local mp = t.mp 
	local exp = t.exp 
	local items = t.items
	local mob = self:new(name,{hp = hp,mp = mp,exp = exp})
	mob.meta.user_name = t.user_name
	mob.meta.story = t.user_story
	for i,v in ipairs(items) do 
		mob:giveItem(v)
	end 
	return mob
end 
return loadGhost