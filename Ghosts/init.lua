--[[------------------------------------------------
	-- Ghost system for Lucidity 
	-- Copyright (c) 2015-2017 Neka tek --
--]]------------------------------------------------
local _PACKAGE = (...):match("(.-)[^%.]+$")
if _PACKAGE == "" then
	_PACKAGE = ...
end
local class = require(_PACKAGE .. '.third.middleclass')
local ghosts = class('aiWorld')
function ghosts:initialize(x,y,w,h,parent)
	self.info = {
		name = "Adam",
		hp = 100,
		mp = 100,
		def = 10,
		atk = 10,
		luck = 10,
		level = 1,
		exp = 0,
		attacks = {
			-- attacks go here
		},
		story = "The first of them all, the one to be born of god, later betrayed and turned to ashes that would form the new world.",
		rarity = 10, --super rare
		element = "devine",
	}
	self.status = status
	self.x = x or 0
	self.y = y or 0
	self.sx = 1
	self.sy = 1
	self.r = 0
	self.image = nil
	self.width = w or 100
	self.height = h or 100
	self.parent = nil

	return self
end
function ghost:getExpNeeded(x)
	local x = x or self.info.level
	local expToLevel = math.floor(math.log(x)*60 + 30+(x^2/5))
	return math.floor(expToLevel - self.info.exp) 
end
function ghost:setParent(target)
	self.parent = target
	target:setChild(self)
end
function ghost:attack(target)
	local luckModif = 0
	if self.parent then
		luckModif = self.parent.info.luck
	end
	
	local lucky = love.math.random()
	local target.info = 
	if not target.invincible then 
	
end
function ghost:applyEffects(effects)
	self.effects = effects
end
function ghost:applyEffect(effect)
	if not self.effects["effect"] then
		table.insert(self.effects,effect)
	end
end
function ghost:dropCard()
end
function ghost:setResistance(resistance)
	local clamp = false
	for i,v in ipairs(self.resistance) do
		if v == resistance then 
			clamp = true
		end
	end
	if not clamp then 
		table.insert(self.resistance,resistance)
		return true
	else
		return false
	end
end
function ghost:setResistances(resistances)
	self.resistance = resistances
end
function ghost:draw()
	if not self.animation then 
		love.graphics.draw(self.image,self.x,self.y,self.r,self.sx,self.sy)
	else
		self.animation:draw(self.image,self.x,self.y,self.r,self.sx,self.sy)
	end
end
function ghost:update()
	for i in ipairs(self.effects) do
		for i in ipairs(self.resistances) do
			if self.effects[i] ~= self.resistances then
				self.effects[i].exec()
			end
		end
	end
end

function ghost:setAnimation(animation)
	self.animation = animation
end
