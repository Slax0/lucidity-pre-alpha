local _PACKAGE = getPackage(...)
local renderCards = {}
local baseCard = love.graphics.newImage(_PACKAGE.."./cards/basic.png")
baseCard:setFilter("linear","linear")
local maxHSV = 360
local maxLvl = 100
local one_prc = 360/100
function renderCards:update(dt)
end
function renderCards:setMob(mob)
	local atk = mob.atk
	local def = mob.def
	local luck = mob.luck
	local mobColor
	local levelColor = hsvToRgb(one_prc * mob.level,100,100)
	local one_prc = 180/100
	local one_prc2 = 40/100
	local rarityColor = hsvToRgb(180 + one_prc * (mob.rarity*10),60 + one_prc2 * (mob.rarity*10),100)
	for i,v in pairs(cardColors) do 
		if i == mob.sourceCrystal then 
			mobColor = v
		end 
	end 
	local uFont = love.graphics.newFont(uniBody,7)
	local text = {
		0,
		0,
		tostring(mob.atk).."\n",
		tostring(mob.def).."\n",
		tostring(mob.luck).."\n",
	}
	self.frame.OnDraw = function()
		local w,h = self.w,self.h
		local cx,cy = self.frame.x,self.frame.y
		text[1] = mob.hp.."/"..mob.maxHp.."\n"
		text[2] = mob.mp.."/"..mob.maxMp.."\n"
		love.graphics.setColor(mobColor)
		local topX,topY = cx - w/2 ,cy - h/2 +1
		local cw,ch = 75,197
		love.graphics.rectangle("fill",topX,topY,cw,ch)
		local cw,ch = 166,104
		love.graphics.rectangle("fill",topX ,topY + 93,cw,ch)
		love.graphics.setColor(255,255,255)
		love.graphics.draw(baseCard,cx,cy,0,1,1,w/2,h/2)
		local radius = 15
		local lol = love.graphics.getLineWidth()
		local lol2 = love.graphics.getFont()

		love.graphics.setLineWidth(1)
		love.graphics.setStencil(function() love.graphics.rectangle("fill",(topX + 60),(topY + 91 - radius/2),radius,radius*2) end)
		love.graphics.setColor(levelColor)
		love.graphics.circle("fill",topX + 60,topY + 93 + radius/3,radius)
		love.graphics.setColor(rarityColor)
		love.graphics.setStencil(function() love.graphics.rectangle("fill",(topX + 60 - radius),(topY + 91 - radius/2),radius,radius*2) end)
		love.graphics.circle("fill",topX + 60,topY + 93 + radius/3,radius)
		love.graphics.setColor(255,255,255)
		love.graphics.setStencil()
		love.graphics.setColor(0,0,0)
		love.graphics.line(topX + 60,topY + 93 + radius/3 - radius,topX + 60,topY + 93 + radius/3 + radius)
		love.graphics.circle("line",topX + 60,topY + 93 + radius/3,radius)
		love.graphics.setColor(255,255,255)
		for i,v in ipairs(text) do 
			love.graphics.print(v,topX + 4,topY + 3 +(10*(i-1)))
		end 
		love.graphics.setLineWidth(lol)

	end 
	
end 
function renderCards:pop()
	self.frame.OnDraw = nil 
end 
function renderCards:load(ui,x,y)
	self = {}
	local w,h = baseCard:getDimensions()
	self.w,self.h = w,h
	self.frame = ui:addFrame(nil,x - w,y + h/2 + 30,w,h)
	self.frame.dontDraw = true 
	return setmetatable(self, { __index = renderCards })
end

return renderCards
