local loadCards = {}
local loaded = {} 
local _PACKAGE = "/cards"
local no = 0 
function loadCards:new(cardName) -- pass set as nil and recrusive as true to get recrusive mode
	no = no + 1
	local lfs = love.filesystem
	local cardName = string.lower(cardName)
	if string.find(cardName,".lua") then 
		error("Nice try but we don't accept lua files for cards: "..cardName)
	end 
	
	local file
	local path,set,setPath
	if loaded[cardName] then 
		path,set,setPath = unpack(loaded[cardName])
	else 
		path,set,setPath = findItem("card",cardName)
		loaded[cardName] = {path,set,setPath} 
	end 
	assert(path,"Cannot find: "..tostring(cardName))
	print("LOOKING AT: "..path)
	local chunk,err = love.filesystem.load(path.."/load.lua")
	assert(chunk,err)
	local card = chunk(path)
	card._name = cardName 
	card.set = tostring(set)
	-- if lfs.exists(_PACKAGE.."/"..setName.."/draw") then 
		-- error("Please do not call folders in sets 'draw'!")
	-- end 
	card.OnDraw = require(setPath.."/draw")
	
	card.rarityCol = genColor(card.rarity,10,true) -- rarity color manager
	
	local image
	if love.filesystem.exists(setPath.."/image.png") then 
		image = love.graphics.newImage(setPath.."/image.png")
	elseif love.filesystem.exists(path.."/image.png") then 
		image = love.graphics.newImage(path.."/image.png")
	end 
	local w,h = image:getDimensions()
	if not card.image then 
		card.image = image 
	end 
	if card.anim and card.anim[1] then 
		local w,h,d,f,ff = unpack(card.anim)
		card.activeAnim = newAnimation(card.image,w,h,d,f,ff)
	end 
	card.width = w - 2
	card.height = h - 2 -- this is to prevent quad bleeding
	card._index = no 
	
	card.activeImage = love.graphics.newCanvas(card.width ,card.height)
	card.activeImage:setFilter("linear","linear")
	card.activeImage:renderTo(function()
		card.OnDraw(card)
	end)
	return setmetatable(card,{__index = loadCards})
end
function loadCards:update(dt)
	local card = self 
	if card.activeAnim then 
		local dt = dt or love.timer.getDelta()
		card.activeAnim:update(dt)
		self.activeImage:renderTo(function()
			self.OnDraw(card)
		end)
	end
end 
function loadCards:getName()
	return self._name
end 
function loadCards:remove()
	for i,v in ipairs(playersCards) do 
		if v._index == self._index then 
			table.remove(playersCards,i)
		end 
	end 
end
function loadCards:draw(...)
	local args = {...}
	love.graphics.draw(self.activeImage,args[1],args[2],args[3],args[4],args[5],self.width/2 - (args[6] or 0),self.height/2 - (args[7] or 0))
end 
function loadCards:retCanvas()
	return self.activeImage
end 
return loadCards