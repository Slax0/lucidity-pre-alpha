local cardShop = {}
local _PACKAGE = getPackage(...)

function cardShop:new(ui,x,y,w,h,playerInv,shopInv,genSettings)
	local a = {
		x = x, 
		y = y,
		w = w or 700,
		h = h or 500,
		ui = ui,
		shopInv = shopInv,
		playerInv = playerInv,
	}
	setmetatable(a,{__index = cardShop})
	if not shopInv then 
		local settings = genSettings or {{"set1",70},{"trap",50}}
		a:setInventory(loadMob,40,settings)
	end 
	print("Shops Items:",tableLenght(a.shopInv))
	a.mainFrame = ui:addFrame(nil,x + w/2,y + h/2,w,h)
	local mainFrame = a.mainFrame
	a.mainFrame.color = basicFrameColor
	
	local xgap = 5
	local gapy = 5
	local seperation = 50 -- For the sell button
	local w,h = (w - (xgap)*2),(h - seperation - (gapy*2)) /2
	
	local shop_inv = ui:addFrame(nil,xgap,gapy,w,h,mainFrame)
	shop_inv.color = {255,0,0}
	local your_inv = ui:addFrame(nil,xgap,gapy + seperation + h,w,h,mainFrame)
	your_inv.color = {0,0,255}
	
	local gapx = 11
	local gapy = 11
	local sW,sH = 180 - 2,270 - 5
	local sx,sy =  0.7,0.7
	local aw,ah  = sW*sx,sH*sy
	local function detMaxNo(w,h)
			local maxNoX =  math.floor((shop_inv.width) / (aw + gapx*2)) -- width plus gaps
			local maxNoY = math.floor((shop_inv.height) / (ah + gapy*2))
			local maxNumber = maxNoX*maxNoY
		return maxNoX,maxNoY,maxNumber
	end 
	
	local maxNoX,maxNoY,maxNumber =  detMaxNo()

	
	
	local function sortPos(tables,frame)
		for i,v in ipairs(tables) do 
			if i <= maxNumber then 
				local gapx = gapx
				local gapy2 = 0 
				local w,h = aw,ah
				local lastx,lasty = gapx,gapy
				if i % maxNoX == 0 then 
					gapy = gapy
				end 
				if playerInv[i - 1] then 
					local b = playerInv[i - 1]
					lastx = b.lastx
					lasty = b.lasty
				end 
				local x = lastx  + gapx
				local y = lasty  + gapy2
				local b = ui:addButton(frame,x,y,w,h)
				frame.que = {}
				b.ChildCard = v
				b.dontDraw = true
				b.OnDraw = function() 
					local canv = v:retCanvas()
					love.graphics.draw(canv,b.x,b.y,0,sx,sy,sW/2,sH/2)
				end 
				b.OnUpdate = function()
					v:update()
				end 
				v.lastx = x + w
				v.lasty = y
				table.insert(frame.que,b)
			end
		end
	end 
	sortPos(a.playerInv,your_inv)
	sortPos(a.shopInv,shop_inv)
	return a
end

function cardShop:setInventory(loadFunction,numberofcards,tableOfCards) -- tableOfCards = {{set -- string ex "set1" ,chance -- out of 100 ex 10},{set2,chance2}}
	local inventory = {}
	while numberofcards > #inventory do 
		for i,v in ipairs(tableOfCards) do 
			if numberofcards > #inventory then  
				local set = v[1]

				local lfs = love.filesystem
				local path = _PACKAGE.."/"..set
				assert(lfs.exists(path),"The set you asked for doesn't exist:"..set)
				local files = lfs.getDirectoryItems(path)
				for k,b in ipairs(files) do 
					if lfs.isDirectory(path.."/"..b) then 
						local chance = math.floor(math.random(v[2],100))
						local name = b
						local setName = set
						if chance == 100 then 
							card = loadCards:new(name,set)
							table.insert(inventory,card)
						end
					end 
				end 
			end
		end
	end
	self.shopInv = inventory
end 
function cardShop:remove()
	self.mainFrame:remove()
end 
function cardShop:update(dt)
	
	
end 

return cardShop