local selectCards = {}
local meta = {__index = selectCards}
function selectCards:new(ui,x,y,w,h,onComplete)
	self = {
		x = x,
		y = y,
		w = w,
		h = h,
	}
	setmetatable(self,meta)
	for i,v in ipairs(playersCards) do
		playersCards[i].index = i		
	end 
	local ChosenCards = {}
	self.centerFrame = ui:addFrame(nil,x,y,w,h)
	self.centerFrame:setZmap(100)

	local scale = 0.8
	local FreeCards = {}
	local cardStack = deepCopy2(playersCards)
	
	local function refreshCard()
		local toRemove = {}
		local toRemove2 = {}
		if #cardStack > 5 then 
			for i,v in ipairs(FreeCards) do 
				FreeCards[i] = nil
			end 
			for i,v in ipairs(playersCards) do 
				if v.index > 5  then 
					table.insert(FreeCards,v)
					table.insert(toRemove,i)
				end 
			end 
		end
		for i,v in ipairs(toRemove) do
			table.remove(cardStack,v)
		end 
		if #cardStack < 5  then 
			for i,v in ipairs(FreeCards) do 
				if #cardStack < 5 then 
					table.insert(cardStack,v)
					table.insert(toRemove2,i)
				end 
			end 
		end

		for i,v in ipairs(toRemove2) do
			table.remove(FreeCards,v)
		end 
	end 
	
	refreshCard()
	refreshCard()
	local mainFrame = self.centerFrame
	mainFrame.color = basicFrameColor
	local cW,cH = cardStack[1].width,cardStack[2].height
	actualCW,actualCH = cW,cH
	cW,cH = cW*scale,cH*scale
	
	local mW,mH = w,h
	local xgap = 5
	local gapy = 5
	
	local seperation = 50 -- For the sell button
	
	local w,h = (cW + xgap)*5 +xgap,cH + (gapy*2)
	local playerInv = ui:addFrame(nil,mW/2 - w/2,gapy,w,h,mainFrame)
	playerInv.color = basicButtonColor
	playerInv.zmap = playerInv.zmap + 1
	local xa,ya = mW/2 - w/2,gapy
	local left = ui:addButton(mainFrame,xa - 23,ya,20,h)
	left.zmap = 101
	left.dontDrawB = true 
	left.color = basicButtonColor
	left:setText("<")
	local right = ui:addButton(mainFrame,xa + w + 3,ya,20,h)
	right.zmap = 101
	right.dontDrawB = true 
	right.color = basicButtonColor
	right:setText(">")
	
	local nw,nh = w,cH + (gapy*2)
	
	local selCards = ui:addFrame(nil,mW/2 - nw/2,mH - nh - gapy,nw,nh,mainFrame)
	selCards.color = basicButtonColor
	selCards.zmap = 2 
	
	
	local finished = ui:addButton(mainFrame,mainFrame.width/2 - 50,mainFrame.height/2 - 25,100,50)
	finished:setText("Done")
	finished.OnClick = function()
		mainFrame:remove()
		if onComplete then onComplete(ChosenCards) end 
	end 
	local gapx = 5
	local gapy = 5
	local function detMaxNo(wk,hk)
			local maxNoX =  math.floor((wk) / (cW + gapx*2 - gapx)) -- width plus gaps
			local maxNoY = math.floor((hk) / (cH + gapy*2))
			local maxNumber = maxNoX*maxNoY
		return maxNoX,maxNoY,maxNumber
	end 

	local placeHolderTable = {{},{},{},{},{}}
	
	local function sortPos(tables,frame,frame2,onDraw)
		if frame.que then 
			for i,v in ipairs(frame.que) do 
				v:remove()
			end
		end 
		local maxNoX,maxNoY,maxNumber = detMaxNo(frame.width,frame.height)
		local ox,oy

			local additional = 0 
			local cardNo = maxNumber - #tables
			if cardNo > 0 then 
				additional = maxNumber - #tables
			end 
			local freeSpace = frame.width - ((maxNumber - additional)*(cW + gapx))
			ox = freeSpace/2 
			frame.que = {}
			
		for i,v in ipairs(tables) do 
			if i <= (maxNumber) then 
				local gapx = gapx
				local gapy2 = 0 
				local w,h = cW,cH
				local lastx,lasty = (ox or 0) - gapx/2, (oy or 0 ) + gapy
				if i % maxNoX == 0 then 
					gapy = gapy
				end 
				if tables[i - 1] then 
					local b = tables[i - 1]
					lastx = b.lastx
					lasty = b.lasty
				end 
				local x = lastx  + gapx
				local y = lasty  + gapy2
				
				local b = ui:addButton(frame,x,y,w,h)
				b.scale = scale
				table.insert(frame.que,b)
				
				if not onDraw then 
					b.ChildCard = v
					b.OnDraw = function() 
						local canv = v:retCanvas()
						love.graphics.draw(canv,b.x,b.y,0,b.scale,b.scale,actualCW/2,actualCH/2)
					end 
					b.OnUpdate = function()
						v:update()
						-- if not ui.mxb:collidesWith(b.colision) then 
							-- if b.AhasFocus and b.OnFocusLost then b.OnFocusLost(b) b.AhasFocus = false end
						-- else 
							-- b.AhasFocus = true 
						-- end 
					end 
					b.OnFocus = function(self)
						local self = b
						self:setZmap(111)
						if self.tween then self.tween:stop() end 
						self.tween = Flux.to(self,0.5,{scale = 1})
					end 
					b.OnFocusLost = function(self)
						local self = b
						self:setZmap(110)
						if self.tween then self.tween:stop() end 
						self.tween = Flux.to(self,1,{scale = 0.8})
					end 
					function onClick(b) 
						b.OnClick = function(sally)
							refreshCard()
							local num = 0 
							for i = 1,5 do  
								if num == 0 and not placeHolderTable[i].taken then 
									num = i
								end 
							end
							
							if num == 0 then num = #ChosenCards + 1 end 
							if num <= 5 then 
								v.inactive = true
								placeHolderTable[num].taken = true
									for i,v in ipairs(frame.que) do 
										if v ~= b then 
											v:remove()
										end 
									end 
									frame.que = {}
									
									b.OnClick = function(b)
										b:remove()
										placeHolderTable[num].taken = false
										local toRemove = {}
										local clamp 
										for n,c in pairs(ChosenCards) do 
											if not clamp and b.item == c then
												clamp = true
												table.insert(FreeCards,c)
												table.insert(toRemove,n)
											end  
										end 
										
										for i,v in ipairs(toRemove) do 
											table.remove(ChosenCards,v)
										end 
									
										refreshCard()

										sortPos(cardStack,playerInv,selCards)
									end 
									
									table.insert(ChosenCards,b.item)
									local clamp 
									for i,v in ipairs(cardStack) do 
										if not clamp and v == b.item then 
											table.remove(cardStack,i)
											clamp = true
										end 
									end 

									refreshCard()

									for i,v in ipairs(frame.que) do 
										v:remove()
									end 
									sortPos(cardStack,playerInv,selCards)
									if b.tween then b.tween:stop() end
									b.OnFocus = nil
									b.OnFocusLost = nil
									b.tween = Flux.to(b,0.3,{x = frame2.que[num].x,y = frame2.que[num].y,scale = scale},nil):oncomplete(function() 
										function b.OnFocus(self)
											local self = b 
											self:setZmap(111)
											if self.tween then self.tween:stop() end 
											self.tween = Flux.to(self,0.5,{scale = 1})
										end 
										function b.OnFocusLost(self)
											local self = b 
											self:setZmap(110)
											if self.tween then self.tween:stop() end
											self.tween = Flux.to(self,1,{scale = 0.8})
										end
									end)
							end 

						end
					end
					onClick(b)
				else 
					b.OnDraw = onDraw
					b:setInactive(true)
				end 
				v.lastx = x + w
				v.lasty = y
				b.item = v
				b.dontDraw = true
			end
		end
	end 
	left.OnClick = function() 
		refreshCard()
		if #FreeCards > 0 then
			table.insert(cardStack,1,FreeCards[#FreeCards])
			table.remove(FreeCards,#FreeCards)
			table.insert(FreeCards,1,cardStack[#cardStack])
			table.remove(cardStack,#cardStack)
			sortPos(cardStack,playerInv,selCards)
		end 
	end 
	
	right.OnClick = function()
		refreshCard()
		if #FreeCards > 0 then
			table.insert(cardStack,FreeCards[1])
			table.insert(FreeCards,cardStack[1])
			table.remove(cardStack,1)
			table.remove(FreeCards,1)
			sortPos(cardStack,playerInv,selCards)
		end 
	end 
	sortPos(cardStack,playerInv,selCards)
	sortPos(placeHolderTable,selCards,playerInv,function(bat) 
		setColor()
		local nw,nh = bat.width,bat.height 
		local w,h = GrayCard:getDimensions()
		local sx,sy = nw/w,nh/h
		love.graphics.draw(GrayCard,bat.x - bat.width/2,bat.y - bat.height/2,0,sx,sy)
	end)
	
	
	
end 
return selectCards