local cardShop = {}
local _PACKAGE = getPackage(...)
function cardShop:new(ui,x,y,tween,shopInv,genSettings)
	local a = {
		x = x, 
		y = y,
		w = 700,
		h = 500,
		ui = ui,
		shopInv = shopInv,
		playerInv = playersCards,
	}
	
	local w,h = 700,500
	setmetatable(a,{__index = cardShop})
	if not shopInv then 
		local settings = genSettings or {{"set1",7},{"trap",3}}
		a:setInventory(nil,20,settings)
	end 
	print("Shops Items:",tableLenght(a.shopInv))
	a.mainFrame = ui:addFrame(nil,x,y,w,h)
	local mainFrame = a.mainFrame
	a.mainFrame.color = basicFrameColor
	local scaleH = 1
	local scaleL = 0.6
	
	local lh = 269*scaleL
	local list = ui:addList(mainFrame,2 + 182*scaleL/2,2,w-4 - 182,lh,nil,true) 
	local pList = ui:addList(mainFrame,2 + 182*scaleL/2,h - (lh) - 4 ,w-4 - 182,lh,nil,true) 
	pList.goMin = true
	list.zmap = mainFrame.zmap + 2
	list.horiz = true
	list.dontCensor = true
	local delay
	local runingText = ""
	local function resetText()
		if delay then Timer.cancel(delay) end 
		delay = Timer.add(1,function()
			runingText = ""
		end)
	end 
	mainFrame.OnDraw = function()
		local f = love.graphics.getFont()
		local w,h = f:getWidth(runingText),f:getHeight()
		setColor(colors.red)
		love.graphics.print(runingText,mainFrame.x - w/2,mainFrame.y + h)
		setColor()
	end 
		mainFrame.OnUpdate = function()
			for i,a in ipairs(list.items) do 
				if not ui:getmxb():collidesWith(a.colision) then 
					a.OnFocusLost()
				end 
			end 
			for i,a in ipairs(pList.items) do 
				if not ui:getmxb():collidesWith(a.colision) then 
					a.OnFocusLost()
				end 
			end 
		end 
	for i,v in ipairs(a.shopInv) do 
		local a = ui:addButton(nil,0,0,180*scaleL,267*scaleL)
		a.___scale = scaleL
		a.dontDrawL = true 
		a.card = v
		local txt = v.cost.." Essence"
		a.OnDraw = function()
			local s = a.___scale
			v:draw(a.x,a.y,0,s,s)
			v.i = i 
			if a._HasFocus then 
				local font = love.graphics.getFont()
				local h = font:getHeight()
				local w = font:getWidth(txt)
				setColor(colors.gray) 
				love.graphics.rectangle("fill",a.x - w/2,a.y + ((269)*s)/2,w,h)
				setColor(colors.green)
				love.graphics.print(txt,a.x - w/2,a.y + ((269)*s)/2)
				setColor(colors.white)
			end 
		end 
		a.OnFocus = function()
			a:setZmap(1)
			if a.Tween then 
				a.Tween:stop()
			end 
			if tween then 
				a.Tween = tween:to(a,0.5,{___scale = scaleH})
			else 
				a.___scale = scaleH 
			end 
		end 
		a.OnFocusLost = function()
			a:setZmap(0)
			if a.Tween then 
				a.Tween:stop()
			end 
			if tween then 
				a.Tween = tween:to(a,0.5,{___scale = scaleL})
			else 
				a.___scale = scaleL
			end 
		end 
		function a.OnClick()
			local e = playerData.essence
			local cond 

			if e < v.cost then 
				runingText = "Not enough essence!"	
			elseif #pList.items >= 20 then 
				runingText = "You can only hold 20 cards at a time!"
				resetText()
			else 
				cond = true 
			end 
			if cond then 
				playerData.essence = e - v.cost
				pList:addItem(a,1)
				list:removeItem(a,true)
				a.___scale = scaleL
			end 
		end 
		list:addItem(a)

	end 
	list.OnAddItem = function(item) 
		local a = item
		local v = item.card 
		local txt = v.cost.." Essence"
		a.OnDraw = function()
			local a = item
			local v = item.card 
			local s = a.___scale
			v:draw(a.x,a.y,0,s,s)
			if a._HasFocus then 
				local font = love.graphics.getFont()
				local h = font:getHeight()
				local w = font:getWidth(txt)
				setColor(colors.gray) 
				love.graphics.rectangle("fill",a.x - w/2,a.y + ((269)*s)/2,w,h)
				setColor(colors.green)
				love.graphics.print(txt,a.x - w/2,a.y + ((269)*s)/2)
				setColor(colors.white)
			end 
		end 
		function a.OnClick()
			local e = playerData.essence
			local cond 

			if e < v.cost then 
				runingText = "Not enough essence!"	
			elseif #pList.items >= 20 then 
				runingText = "You can only hold 20 cards at a time!"
				resetText()
			else 
				cond = true 
			end 
			if cond then 
				playerData.essence = e - v.cost
				pList:addItem(a,1)
				list:removeItem(a,true)
				a.___scale = scaleL
			end 
		end 
	end  
	
	local shopList = list 
	local list = pList 
	list.zmap = mainFrame.zmap + 2
	list.horiz = true
	list.dontCensor = true
	
	for i,v in ipairs(playersCards) do 
		local a = ui:addButton(nil,0,0,180*scaleL,267*scaleL)
		a.___scale = scaleL
		a.dontDrawL = true 
		local txt = v.cost.." Essence"
		a.card = v
		a.OnDraw = function()
			local s = a.___scale
			v:draw(a.x,a.y,0,s,s)
			if a._HasFocus then 
				local font = love.graphics.getFont()
				local h = font:getHeight()
				local w = font:getWidth(txt)
				
				setColor(colors.gray) 
				love.graphics.rectangle("fill",a.x - w/2,a.y - ((269)*s)/2 - (h),w,h)
				setColor(colors.blue)
				love.graphics.print(txt,a.x - w/2,a.y - ((269)*s)/2 - (h))
				setColor()
			end 
		end 
		a.OnFocus = function()
			if tween then 
				tween:to(a,0.5,{___scale = scaleH})
			else 
				a.___scale = scaleH 
			end 
			a:setZmap(1)
		end 
		a.OnFocusLost = function()
			if tween then 
				tween:to(a,0.5,{___scale = scaleL})
			else 
				a.___scale = scaleL
			end 
			a:setZmap(0)
		end 
		a.OnClick = function()
			playerData.essence = playerData.essence + v.cost
			shopList:addItem(a)
			removeFromTable(playersCards,v)
			list:removeItem(a,true)
		end 
		list:addItem(a)
	end 
	list.OnAddItem = function(item) 
		local a = item
		local v = item.card 
		local txt = v.cost.." Essence"
		table.insert(playersCards,v)
		a.OnDraw = function()
			local s = a.___scale
			v:draw(a.x,a.y,0,s,s)
			if a._HasFocus then 
				local font = love.graphics.getFont()
				local h = font:getHeight()
				local w = font:getWidth(txt)
				
				setColor(colors.gray) 
				love.graphics.rectangle("fill",a.x - w/2,a.y - ((269)*s)/2 - (h),w,h)
				setColor(colors.blue)
				love.graphics.print(txt,a.x - w/2,a.y - ((269)*s)/2 - (h))
				setColor()
			end 
		end 
		a.OnClick = function()
			playerData.essence = playerData.essence + v.cost
			shopList:addItem(a)
			removeFromTable(playersCards,v)
			list:removeItem(a,true)
		end 
	end 
	local sw,sh = 60,30 
	local done = ui:addButton(mainFrame,w/2 - sw/2,h/2 - sh/2,sw,sh)
	done:setZmap(mainFrame.zmap + 1)
	done.OnClick = function()
		if a.OnDone then a.OnDone() end 
		mainFrame:remove()
	end 
	done:setText("Done")
	return a
end

function cardShop:setInventory(dir,numberofcards,tableOfCards) -- tableOfCards = {{set -- string ex "set1" ,chance -- out of 10 ex 1},{set2,chance2}}
	local inventory = {}
	local dir = dir or "Ghosts/cards"
	
	while numberofcards > #inventory do 
		for i,v in ipairs(tableOfCards) do 
			if numberofcards > #inventory then  
				local set = v[1]

				local lfs = love.filesystem
				local path = dir.."/"..set
				if not lfs.exists(path) then
					local oldPath = path
					path = "cards/"..set
					assert(lfs.exists(path),"The set you asked for doesn't exist:"..set,"In Either "..oldPath.." or \n "..lfs.getSaveDirectory().."/"..path)
				end 
				local files = lfs.getDirectoryItems(path)
				for k,b in ipairs(files) do 
					if lfs.isDirectory(path.."/"..b) then 
						local chance = math.floor(math.random(v[2],10))
						local name = b
						local setName = set
						if chance == 10 then 
							card = loadCards:new(name,set)
							table.insert(inventory,card)
						end
					end 
				end 
			end
		end
	end 
	self.shopInv = inventory
end 
function cardShop:remove()
	self.mainFrame:remove()
end 
function cardShop:update(dt)
	
	
end 

return cardShop