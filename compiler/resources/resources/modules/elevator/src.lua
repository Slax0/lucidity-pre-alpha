local platform = {}
platform.__index = platform
function platform:new(mode,colider,phyWorld,x,y,width,height,scalex,scaley)
	local object = {
		x = x  or 0,
		y = y  or 0,
		width = width or 100,
		height = height or 20,
		mode = mode,
		colider = colider,
		phyWorld = phyWorld,
		speed = 5,
		floor = {},
		ox = x or 0,
		oy = y or 0,
		fixture = {},
		_makeShapes = {},
		des = 1,
		body = nil,
		shape = nil,
		current = 1,
		smooth = smooth or false,
		drawPath = true,
		image = image,
		keyp = false,
		keyr = false,
		onCol = false,
		contact = false,
		shapes = {},
		pause = 0,
		timer = 0,
		active = false,
		items = {},
		scalex = scalex or 1,
		scaley = scaley or 1,
		colisions = {},
		renderRange = nil,
		cycle2 = false,
		rail = false,
		railEnd = false,
		floors = {},
		active_contacts = {},
		status = "moving",
		type = "elevator",
		xvl = 0,
		yvl = 0
	}
	setmetatable(object,platform)
	object:makeColision()
	return object
end
function platform:addFrame(shape)
		table.insert(self._makeShapes,shape)
		local shape,x,y,types = shape(self)
		if not shape:getPoints() then
			error("invalid shape")
		end
		table.insert(self.shapes,shape)
	self:makeColision()
end
function platform:setImage(image,type,width,height,ox,oy)
	local mag = {}
	self.scalex = sx or self.scalex
	self.scaley = sy or self.scaley
	mag.x = 0 - (image:getWidth()*self.scalex)/2 + (ox or 0)
	mag.y = 0 - (image:getHeight()*self.scaley) + (oy or 0)
	if width then
		self.width = image:getWidth()*self.scalex
	end
	if height then 
		self.height = image:getHeight()*self.scaley
	end
	mag.image = image
	if type == nil then
		type = "image"
	end
	mag.type = type
	self.imgH = image:getHeight()*self.scaley
	self.imgW = image:getWidth()*self.scalex
	if ox then self._ox = ox end 
	if oy then self._oy = oy end 
 	table.insert(self.items,mag)
end
function platform:attachImage(x,y,image,type)
	local mag = {}
	mag.x = x
	mag.y = y
	table.insert(self.items,mag)
end
function platform:attachItem(item)
	table.insert(self.items,item)
end
function platform:setOrigin(ox,oy)
	self.ox = ox
	self.oy = oy
end
function platform:getOrigin()
	return self.ox,self.oy
end
function platform:setDrawPath(path)
	self.drawPath = path
end
function platform:setSmooth(smooth)
	self.smooth = smooth
end
function platform:adjustSpeed(horiz,vert,destination)
	local body = self.body
	local cx = body:getX()
	local cy = body:getY()
	local nx = nil
	local ny = nil
	local dx = self.floor[destination].x
	local dy = self.floor[destination].y
	dx = math.floor(dx)
	dy = math.floor(dy)
	cx = math.floor(cx)
	cy = math.floor(cy)
	local difx = math.max(dx,cx) - math.min(dx,cx)
	local dify = math.max(dy,cy) - math.min(dy,cy)
	if horiz then
		self.speedx = math.floor(math.abs(difx*100))
	end
	if vert then
		self.speedy = math.floor(math.abs(dify*100))
	end
end
function platform:getSmooth()
	return smooth
end
function platform:setSpeed(speed)
	self.speed = speed
end
function platform:getSpeedVec()
	return self.speedx,self.speedy
end
local floors = {}
platform._fmeta = floors
function platform:newFloor(x,y,no,rebuild)
	if y == nil then
		y = self.x or self.body:getY()
	end
	if x == nil then
		x = self.x or self.body:getX() 
	end
	if no == nil then
		no = tableLenght(self.floor) + 1
	end

	local floor = {}
	floor.shape = self.colider:addRectangle(self.x,self.y,self.width,self.height)
	floor.shape._module = floor 
	floor.x = x 
	floor.y = y
	floor.papa = self
	floor.zmap = self.zmap
	floor.oldX = x 
	floor.oldY = y 
	if self.floor[no] ~= nil then
		table.insert(self.floor,no,floor)
	else
		self.floor[no] = floor
	end 
	floor.index = no
	local tablex = {}
	local tabley = {}
	for i in ipairs(self.floor) do
		local x = self.floor[i].x
		local y = self.floor[i].y
		table.insert(tablex,x)
		table.insert(tabley,y)
	end
	local Xmax = math.max(unpack(tablex))
	local Xmin = math.min(unpack(tablex))
	local Ymax = math.max(unpack(tabley))
	local Ymin = math.min(unpack(tabley))
	local width = Xmax - Xmin
	local height = Ymin - Ymax
	if Xmin < 0 then
		width = Xmax - Xmin
	end
	if Ymin < 0 then
		height = Ymax - Ymin
	end
	local tablew = {}
	local tableh = {}
	for i in ipairs(self.items) do
		if self.items[i].image then
			table.insert(tablew,self.items[i].image:getWidth())
			table.insert(tableh,self.items[i].image:getHeight())
		end
	end
	
	local maxw = math.max(unpack(tablew))*self.scalex
	local maxh = math.max(unpack(tableh))*self.scaley
	
	local width2 = width
	width = width 
	local x = math.min(Xmin)
	local y = math.min(Ymin)
	if rebuild then 
		self:rebuildRails()
	end 
	if self.body then 
		if self.renderRange then
			self.colider:remove(self.renderRange)
		end
		local w,h = width + maxw ,height + maxh
		self.renderRange = self.colider:addRectangle(x - w/2,y - h,width + maxw ,height + maxh)
		self.renderRange.dontMove = true
		if self.OnMakeColision then 
			self.OnMakeColision(self.renderRange)
		end 
	end 
	setmetatable(floor,{__index = floors})
	
	
	local width,height = findDimensions(floor.shape)
	local w,h = (width)/2,(height)/2
	floor.width = width 
	floor.height = height
	floor.topX,floor.topY = incrementPos(floor.x,floor.y - height/2,w,h)
	floor.dx = floor.topX + (floor.modx or 0) 
	floor.dy = floor.topY + (floor.mody or 0)
	floor.shape:moveTo(floor.topX + (floor.modx or 0),floor.topY + (floor.mody or 0))
	floor._x,floor._y = floor.shape:center()
	if not self.children then self.children = {} end
	return floor
end
function floors:getColision()
	return self.shape
end 
function floors:update(dt)
	local width,height = findDimensions(self.shape)
	local w,h = (width)/2,(height)/2
	self.width = width 
	self.height = height
	self.topX,self.topY = incrementPos(self.x,self.y - height/2,w,h)
	self.dx = self.topX + (self.modx or 0) 
	self.dy = self.topY + (self.mody or 0)
	self.shape:moveTo(self.topX + (self.modx or 0),self.topY + (self.mody or 0))
	if self.x ~= self.oldX or self.y ~= self.oldY then
		self.papa:rebuildRails()
		self.oldX = self.x
		self.oldY = self.y
	end 	
end 
function floors:draw(p,debug)
	if not self.papa.phyWorld or p then 
		local v = self
		local floors = self.papa.floor
		local no = 0
		local papa = self.papa	
		local x,y = v.shape:center()
		
		for i,v in ipairs(floors) do 
			if v == self then 
				no = i 
			end 
		end 
		if debug or Gamestate.current() == Editor then 
			local w = self.papa.width/2
			local h = self.papa.height/2
			v:getColision():draw("fill")
			
			if floors[no + 1]  then
				local x1,y1 = floors[no + 1].shape:center()
				love.graphics.line(x,y,x1,y1)
			end
		end 
	end 
end 
function floors:remove()
	if self.shape then self.papa.colider:remove(self.shape) end 
	removeFromTable(self.papa.floor,self)
	self.papa:rebuildRails()
end 
function platform:OnMenu(list,ui,editor)
	local a = ui:addButton(nil,0,0,2,2)
	a:setText("Add Floor")
	function a.OnClick()
		local x,y = editor.mx,editor.my
		self:newFloor(x,y,nil,true)
	end 
	list:addItem(a)
end 
function platform:makeColision() 
	local shape = self.shape
	local body = self.body
	if self.colisions then 
		for i,v in pairs(self.colisions) do 
			self.colider:remove(v)
			self.colisions[i] = nil 
		end
	end 
	if self.colision then 
		self.colider:remove(self.colision)
	end 
	if self.body then 
		self.body:destroy()
	end 
	if self.renderRange then 
		self.colider:remove(self.renderRange)
	end 
	local phyWorld = self.phyWorld
	if phyWorld then
		clearTable(self.shapes)
		shape = love.physics.newRectangleShape((self._ox or 0),(self._oy or 0),self.width*self.scalex,self.height*self.scaley)
		body = love.physics.newBody(phyWorld,self.x,self.y,"kinematic")
		local fixture = love.physics.newFixture(body,shape)
		fixture:setUserData({source = self})
		body:setPosition(self.x,self.y) 
		for i,v in ipairs(self._makeShapes) do 
			local shape,x,y,types = v(self)
			local fixture = love.physics.newFixture(body,shape)
			local part
			if types ~= "dynamic" then 
				part = "side"
			else 
				local colision = self.colider:addPolygon(shape:getPoints())
				colision:moveTo(body:getX() - (self._ox or 0),body:getY() - (self._oy or 0))
				function colision.getUserData()
					return {source = self}
				end 
				colision.modx = x 
				colision.mody = y 
				table.insert(self.colisions,colision)
			end 
			fixture:setUserData({source = self })
 			table.insert(self.shapes,shape)
		end 
	end
	if self.colider then
		local colision
		if shape then
			colision = self.colider:addPolygon(shape:getPoints())
			colision:move(body:getX() + (self._ox or 0),body:getY() + ((self._oy or 0)))
		else
			colision = self.colider:addRectangle(self.x,self.y,self.width*self.scalex,self.height*self.scaley)
			colision:moveTo(self.x ,self.y)
		end
			colision.modx = -(self._ox or 0)
			colision.mody = -(self._oy or 0)
		function colision.getUserData()
			return {source = self}
		end 
		table.insert(self.colisions,colision)
	end
	self.shape = shape
	self.body = body
	local width = {}
	local height = {}
	for i in ipairs(self.items) do
		table.insert(height,self.items[i].image:getHeight())
		table.insert(width,self.items[i].image:getWidth())
	end
	if #width ~= 0 then 
		width = math.max(unpack(width)) * self.scalex
		height = math.max(unpack(height)) * self.scaley
	else 
		width = self.width 
		height = self.height 
	end 
	self._width = width
	self._height = height
	self.renderRange = self.colider:addRectangle((self.x or 0)  - (width or self.width)/2,(self.y or 0) - (height or self.height),(width or self.width or 100),(height or self.height or 100))
	if self.OnMakeColision then 
		self.OnMakeColision(self.renderRange)
	end 
end
function platform:getColision()
	if not self.renderRange then 
		self:makeColision()
	end 
	return self.renderRange
end 
function platform:cUpdate()
	local width = self._width
	local height = self._height
	local w,h = (width)/2,(height)/2
	self.topX,self.topY = incrementPos(self.x,self.y - height/2,w,h)
	self.dx = self.topX + (self.modx or 0) 
	self.dy = self.topY + (self.mody or 0)
	self.renderRange:moveTo(self.topX + (self.modx or 0),self.topY + (self.mody or 0))
end
function platform:draw(debug)
	local floorline = {} 
	local r,g,b,a = love.graphics.getColor()
	for i,v in ipairs(self.floor) do 
		if self.phyWorld then 
			v:draw(p,debug)
		end 
		if self.railEnd then
			local w,h = self.railEnd.image:getDimensions()
			local x,y = v.shape:center()
			love.graphics.draw(self.railEnd.image,x,y,0,1,1,w/2,h/2)
		end 
		if self.drawPath and self.rail then 
			-- local img = self.rail.image 
			-- local x2 = img:getWidth()
			-- local y2 = img:getHeight() 
			-- x = (x or v.x) - (v.ofx or 0) 
			-- y = (y or v.y) - (v.ofy or 0) 
			-- local r = v.r
			-- if v.quad then
				-- love.graphics.draw(img,v.quad,x,y,r,self.scalex,self.scaley,0,y2/2)
			-- end
			local x,y = v.shape:center()
			table.insert(floorline,x)
			table.insert(floorline,y)
		end 
	end 
	local ollw = love.graphics.getLineWidth()
	setColor(colors.gray)
	love.graphics.setLineWidth(20)
	if #floorline >= 4 then 
		love.graphics.line(unpack(floorline))
		love.graphics.setLineWidth(5)
		love.graphics.line(unpack(floorline))
	end
	love.graphics.setLineWidth(ollw)
	setColor(r,g,b,a)
	for i in ipairs(self.items) do
		local x = self.items[i].x
		local y = self.items[i].y
		local x2 = (self.dx or self.x)
		local y2 = (self.dy or self.y ) + self._height/2
		if self.body then
			x2 = self.body:getX()
			y2 = self.body:getY()
		end
		if self.items[i].type == "image" then
		local w = (self.items[i].image:getWidth())*self.scalex
		local h =  (self.items[i].image:getHeight())*self.scaley
			love.graphics.draw(self.items[i].image,x2,y2 ,0,self.scalex,self.scaley,w/2,h)
		elseif self.items[i].type == "animation" then
			self.items[i]:draw(x + x2,y + y2,self.scalex,self.scaley)
		end
	end

		-- if self.renderRange then
			-- self.renderRange:draw("line")
		-- end
		if self.body and debug then
				self.renderRange:draw("line")
			love.graphics.push()
			love.graphics.translate(self.body:getX(),self.body:getY())
			love.graphics.setColor(0,200,50)
			for i in ipairs(self.shapes) do
				love.graphics.polygon("fill",self.shapes[i]:getPoints())
			end
			love.graphics.polygon("fill",self.shape:getPoints())
			love.graphics.pop()
		end
		love.graphics.setColor(255,255,255)

		
	if self.onDraw then
		self.onDraw(self)
	end
	if self.OnDraw then
		self.OnDraw(self)
	end
	if self.body and debug then 
		for i in ipairs(self.colisions) do
			love.graphics.setColor(10*i,0,0)
			self.colisions[i]:draw("line")
		end
	end 

end
function platform:setPause(time)
	self.pause = time
end
function platform:OnLoseFocus()
	print("Platform lost focus.")
	self.body:setSleepingAllowed(true)
	self.body:setAwake(false)
	self.body:setActive(false)
end 
function platform:update(dt)
	local des = self.des
	local body = self.body
	local current = self.current
	local fulse = true
	if not self.body then 
		self:cUpdate(dt)
	elseif not self.body:isDestroyed() then  
		function self.renderRange.getUserData()
			return {src = self}
		end 
		self.body:setSleepingAllowed(false)
		self.body:setAwake(true)
		self.body:setActive(true)
		if des ~= self.current and self.floor[des] ~= nil then
			if not self.cycle2 then
				local direction = 1
				if des - current < 0 then
					direction = -1
				end
				local tables = {}
				for i = current,des,direction do
					local x,y = self.floor[i].shape:center()
					local cx,cy = self.floor[current].shape:center()
					if x == cx and y ~= cy then
						des = i
						table.insert(tables,i)
						fulse = true
					elseif y == cy and x ~= cx then
						des = i
						fulse = true
						table.insert(tables,i)
					end
				end
				if direction < 0 then
					des = math.ceil(math.min(unpack(tables)))
				else
					des = math.floor(math.max(unpack(tables)))
				end
			end
			if not self.cycle2 then 
				des = self.des
			end 
			if not fulse or self.cycle2 then
				local val = self.des - self.current
				if val == 1 then
					des = self.des
				else
					if val < 0 then
						des = self.current - 1 
					elseif val > 0 then
						des = self.current + 1
					end
				end
			end
			local cx = self.x
			local cy = self.y
			if body then 
				cx,cy = body:getPosition()
			end 
			local nx = nil
			local ny = nil
			local dx,dy = self.floor[des].shape:center()
			dx = math.floor(dx)
			dy = math.floor(dy)
			cx = math.floor(cx)
			cy = math.floor(cy)
			if not self.active or self.active == false then
				if dy ~= cy or dx ~= cx then
					self.timer = self.timer + 10*dt
				end
			end
			if self.timer >= (self.pause or 0)  then
				self.active = true
				self.timer = 0
			end
			if self.smooth and self.active and not self.clamp then
				local difx = 0
				local dify = 0
				if dy ~= cy or dx ~= cx then 
					difx = dx - cx
					dify = dy - cy
					nx = difx*(self.speed/100)
					ny = dify*(self.speed/100)
					if nx or ny then
						local xvl = nx or 0
						local yvl = ny or 0
						self.body:setLinearVelocity(xvl,yvl)
					end
				end
				if dy == cy and dx == cx then
					self.current = des
					self.active = false
					self.body:setLinearVelocity(0,0)
				end
			end
				

		if not self.smooth and self.active then
				local body = self.body
				body:setLinearVelocity(0,0)
				local x,y = body:getPosition()
				local dx,dy = self.floor[des]._x,self.floor[des]._y
				x,y = math.floor(x),math.floor(y)
				dx,dy = math.floor(dx),math.floor(dy)
				local cx,cy = self.floor[self.current].shape:center()
				cx,cy = math.floor(cx),math.floor(cy)
				local difx = dx - cx
				local dify = dy - cy
				local ratx,raty
				if difx == 0 or dify == 0 then
					ratx = 1
					raty = 1
				else
					ratx = 1
					raty = tonumber(dify/difx)
				end
				local speed = self.speed
				local a = speed/(math.abs(raty)+math.abs(ratx))
				local speedy = (a*math.abs(raty))
				local speedx = (a*math.abs(ratx))
					speedx = speedx
					speedy = speedy
					if x ~= dx then
						if math.floor(x - dx) > 0 then
							speedx = -speedx 
						elseif math.floor(x - dx) < 0 then
							speedx = speedx
						else
							speedx = 0
						end
					end
					if y ~= dy then
						if math.floor(y - dy) > 0 then
							speedy = -speedy
						elseif math.floor(y - dy) < 0 then
							speedy = speedy
						else
							speedy = 0
						end
					end
				body:setLinearVelocity(speedx,speedy)
				dx,dy = body:getPosition()
				if dy == cy and dx == cx then
					self.body:setLinearVelocity(0,0)
					self.current = des
					self.active = false
				end
			end
			if dy == cy and dx == cx then
				self.current = des
				self.active = false
				self.body:setLinearVelocity(0,0)
			end
		end
		local x,y 
		if body then 
			x,y = body:getX(),body:getY()
		else 
			x,y = self.dx,self.dy+ self._height/2
		end 
		for i in ipairs(self.colisions) do
			self.colisions[i]:moveTo(x - self.colisions[i].modx,y - self.colisions[i].mody)
		end
		if self.active then
			self.status = "moving"
		else
			self.status = "still"
		end
		for i,v in pairs(self.active_contacts) do 
			local xvl,yvl = self.body:getLinearVelocity()
			local xvel,yvel = v.body:getLinearVelocity()
			local left,right,jump = v.ctrl["left"],v.ctrl["right"],v.ctrl["jump"]
			if left or right then 
				xvl = xvel 
			end 
			if jump then 
				yvl = yvel 
			end 
		end 
	end
		if self.onUpdate then
			self.onUpdate(dt,self)
		end	
end
function platform:goTo(floor)
	if self.body and findDistance(self.body:getLinearVelocity()) == 0 then 
	self.des = floor
	assert(self.floor[floor],"The floor: "..floor.." is nil")
	end
end
function platform:cycle(floor1,floor2)
	self.cycle2 = true
	if self.current == floor1 then
		self.des = floor2
	elseif self.current == floor2 then
		self.des = floor1
	end
end
function platform:destroy(phyWorld)
	self.colider:remove(self.colision)
	self.body:remove()
	self = nil
end
function platform:HStop(dt,a,b)
	local player,part,other = checkPlayerCol(a,b)
	local d 
	if other.getUserData then 
		d = other:getUserData()
	end 
	if d and player then 
		if part ~= "body" then 
			if d.source == self then 
				local v = player
				if v.type == "player" then 
					self._HasPlayer = false
					if self.OnPlayerExit then 
						self.OnPlayerExit()
					end 
				end 
				v.OnMovement = nil 
			end 
		end 
	end 
end
function platform:OnFocusLost() 
	if self.body and not self.body:isDestroyed() then 
		self.body:setActive(false)
	end 
end 
function platform:HCol(dt,a,b)
	local player,part,other = checkPlayerCol(a,b)
	local d 
	if other.getUserData then 
		d = other:getUserData()
	end 
	if d and player then 
		if part ~= "body" then 
			if d.source == self then 
				local v = player
				if v.type == "player" then 
					self._HasPlayer = true
					if self.OnPlayerEnter then 
						self.OnPlayerEnter()
					end 
				end 
				function v.OnMovement(dt) 
					local xvl,yvl = self.body:getLinearVelocity()
					local xvel,yvel = v.body:getLinearVelocity()
					local left,right,jump = v.ctrl["left"],v.ctrl["right"],v.ctrl["jump"]
					if left or right then 
						xvl = xvel 
					end 
					if jump then 
						yvl = yvel
					end 
					v.body:setLinearVelocity(xvl,yvl)
				end 
			end 
		end 
	end 
end
function platform:beginContact(a,b,coll)

end 
function platform:endContact(a,b,coll)
end 
function platform:setRail(image,sx,sy)
	self.rail = {}
	self.rail.image = image
	self.rail.image:setWrap("repeat")
	self.rail.sx = sx or self.scalex
	self.rail.sy = sy or self.scaley
end
function platform:setRailConnector(image,ox,oy)
	self.railConnector = {}
	self.railConnector.image = image
	self.railConnector.ox = ox or image:getWidth()/2
	self.railConnector.oy = oy or image:getHeight()/2
end
function platform:rebuildRails()
		local w = self.rail.image:getWidth()
		local h = self.rail.image:getHeight()
		if self.rail then
			for i in ipairs(self.floor) do
				local x = self.floor[i].x
				local y = self.floor[i].y
				local no = i + 1 
				if self.floor[no] ~= nil then
					local x1 = self.floor[i + 1].x
					local y1 = self.floor[i + 1].y
					local w2 = x - x1
					local h2 = y - y1
					if w2 ~= 0 or h2 ~= 0 then
						self.floor[i].r = math.atan(h2/w2)
						self.floor[i].ofx = w2
						self.floor[i].ofy = h2
						if w2 < 0 then
							self.floor[i].ofx = 0
							self.floor[i].ofy = 0
						end
						w2 = math.sqrt(w2^2 + h2^2)
						self.floor[i].quad = love.graphics.newQuad(0,0,w2/self.scalex,h,w,h)
					else
						if h2 == 0 then
							self.floor[i].ofx = 0
							self.floor[i].r = 0
						end
						if w2 == 0 then
							self.floor[i].ofx = 0
							w2 = (w*self.scalex)
							self.floor[i].r = math.rad(90)
						end
						
						local mx = math.sqrt(w2^2 + h2^2)
						if not self.floor[i].ofx then
							self.floor[i].ofx = w2
						end
						if not self.floor[i].ofy then
							self.floor[i].ofy = h2
						end
						self.floor[i].quad = love.graphics.newQuad(0,0,mx/self.scalex,h,w,h)
					end
				else
					self.floor[i].quad = false
					self.floor[i].ofx = 0
					self.floor[i].ofy = 0
					self.floor[i].r = 0
				end
			end
		end
		if self.railConnector and self.doAngles then
			for i in ipairs(self.floor) do
				local no = i + 1
				if self.floor[no] ~= nil then
					local primR = math.max(self.floor[i].r,self.floor[no].r)
					local w = (self.railConnector.image:getWidth())
					local h = (self.railConnector.image:getHeight())
					local w = w*math.sin(primR) + h*math.cos(primR)
					local h = h*math.sin(primR) + w*math.cos(primR)
					if tableLenght(self.floor) > 1 then
						w = self.width*self.scalex
						h = self.height*self.scaley
					end
					local can = love.graphics.newCanvas(w,h)
					love.graphics.setCanvas(can)
						can:clear()
						local ox = self.railConnector.ox
						local oy = self.railConnector.oy
						love.graphics.draw(self.railConnector.image,w/2,h/2,self.floor[i].r,1,1,ox,oy)
						love.graphics.draw(self.railConnector.image,w/2,h/2,self.floor[no].r,1,1,ox,oy)
					love.graphics.setCanvas()
					self.floor[i].connect = love.graphics.newImage(can:getImageData())
					self.floor[i].cdata = {}
					self.floor[i].cdata.ox = self.floor[i].connect:getWidth()/2
					self.floor[i].cdata.oy = self.floor[i].connect:getHeight()/2
				end
			end
		end
end
function platform:setRailEnd(image,ofx,ofy,sx,sy)
	self.railEnd = {}
	self.railEnd.image = image
	self.railEnd.ox = ofx or 0
	self.railEnd.oy = ofy or 0
	self.railEnd.sx = sx or self.scalex
	self.railEnd.sy = sy or self.scaley
end
function platform:updateFloor(floor,x,y)
	self.floors[floor].x = x
	self.floors[floor].y = y
	self.floors[floor].src:moveTo(x,y)
end
function platform:returnFloors()
	return self.floors
end 
function platform:remove()
	if self.onRemove then
		self.onRemove(self)
	end
	if self.body then
		self.body:destroy()
	end
	if self.colisions then
		for i in ipairs(self.colisions) do
			self.colider:remove(self.colisions[i])
		end
	end
	if self.renderRange then
		self.colider:remove(self.renderRange)
	end
	if self.colision then
		self.colider:remove(self.colision)
	end
	for i,v in ipairs(self.floors) do
		v:remove()
	end
end
function platform:setPosition(x,y)
	self.x = x 
	self.y = y
	if self.body then self.body:setPosition(self.x,self.y) end
end
function platform:load(tables)
	if not tables then 
		tables = {}
	end 
	self.x = tables.x or 0 
	self.y = tables.y or 0
	self.zmap = tables.zmap or 1
	self.width = tables.width or 100 
	self.height = tables.height or 10
	self.scalex = tables.scalex or 1 
	self.scaley = tables.scaley or 1
	self:makeColision()
	for i,v in ipairs(tables.floor or {}) do 
		local a = self:newFloor(v.x,v.y)
		a.zmap = v.zmap
		if v.OnSave then 
			if a.OnLoad then a:OnLoad(v.OnSave) end 
		end 
	end 
	
	self:rebuildRails()
	local x,y = tables.x,tables.y
	if self.floor[1] and self.phyWorld then 
		local w,h = findDimensions(self.floor[1].shape)
		x,y = self.floor[1].shape:center()
		x = x
		y = y 
	end 
	self.x = x 
	self.y = y 
	self:setPosition(x,y)
	if self.phyWorld then 
		self.active = true
	end 
end 
function platform:save()
	local floorz2 = self.floor
	local floorz = {}
	
	for i,v in ipairs(floorz2) do 
		floorz[i] = {}
		for m,k in pairs(v) do 
			if type(k) == "number" then 
				floorz[i][m] = k
			end 
		end 
		if floorz2[i].OnSave then 
			floorz[i].OnSave = floorz2[i]:OnSave()
		end 
	end 
	local x,y = self:getColision():center()
	local s = {
		x = x,
		y = y + self._height/2,
		zmap = self.zmap, 
		width = self.width,
		height = self.height,
		scalex = self.scalex, 
		scaley = self.scaley,
		floor = floorz,
	}
	return s
end 
return platform
