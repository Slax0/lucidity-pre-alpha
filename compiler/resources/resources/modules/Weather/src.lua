local weather = {}
function weather:new(mode,colider,phyWorld,x,y,width,height,scalex,scaley)
	local s = {
		colider = colider,
		phyWorld = phyWorld,
		x = x or 0,
		y = y or 0,
		topX = 0,
		topY = 0,
		scalex = scalex or 1,
		scaley = scaley or 1,
		width = width or 100,
		height = height or 100,
		squares = {},
		tweenGroup = Flux.group(),
		type = "Weather",
	}
	setmetatable(s,{__index = weather})
	if s.start then s:start() end 
	return s
end 
function weather:OnMenu(menu,ui)
	local x,y = 300,400
	menu:addChoice("Config",function()
		local frame = ui:addFrame(nil,x,y,200,150)
		local vars = {
			"width","height",
		}
		local items = {
		}
		local fh = ui:getFont():getHeight()
		local gap = 5
		for i,v in ipairs(vars) do 
			local t = ui:addTextinput(frame,95,25 + ((gap + fh)*(i-1)),100,fh)
			t:setAllowed({"1","2","3","4","5","6","7","8","9","0","."})
			t:setText(self[v])
			local text = ui:addText(frame,5,25 + ((gap + fh)*(i-1)),v)
			
			table.insert(items,t)
		end 
		frame.dragEnabled = true 
		frame:addCloseButton(function()
			for i,v in ipairs(vars) do 
				self[v] = tonumber(items[i]:getText())
			end 
			self:makeColision()
		end)
		frame:setHeader("Weather Config")
	end)
end
function weather:makeColision()
	local body 
	if self.body then 
		self.body:destroy()
		self.body = nil 
	end 
	local phyWorld = self.phyWorld
	local colider = self.colider 
	if self.renderRange then colider:remove(self.renderRange) end  
	self.renderRange = colider:addRectangle(self.x or 0,self.y or 0,self.width*self.scalex,self.height*self.scaley)
		function self.renderRange.getUserData()
			return {source = self,part = "body"}
		end 
	colider:addToGroup(tostring(self),self.renderRange)
	
	if self.OnMakeColision then 
		self.OnMakeColision(self.renderRange)
	end 
end 
function weather:getGroup()
	return self.tweenGroup
end
function weather:setMod(mx,my)
	self.modx = mx 
	self.mody = my
end
function weather:update(dt)
	local width = self.width
	local height = self.height
	local w,h = (width)/2,(height)/2
	local gs = Gamestate.current()
	self.posx,self.posy = gs.x,gs.y
	if not self.global then 
		self.renderRange:moveTo(self.x + (self.modx or 0),self.y + (self.mody or 0))
	else 
		self.renderRange:moveTo(gs.CameraBox:center())
	end 
	self.tweenGroup:update(dt)
	if self.update_effect then 
		self.update_effect(dt)
	end 
end 
function weather:setDimensions(w,h)
	self.width = w 
	self.height = h 
	if self.renderRange then colider:remove(self.renderRange) end  
	self.renderRange = colider:addRectangle(self.x or 0,self.y or 0,self.width*self.scalex,self.height*self.scaley)	
end
function weather:getColision()
	if not self.renderRange then 
		self:makeColision()
	end 
	return self.renderRange 
end  

function weather:save()
	local t = {
		x = self.x,
		y = self.y,
		width = self.width,
		height = self.height,
		zmap = self.zmap,
	}
	return t 
end 
function weather:load(table)
	self.x = table.x
	self.y = table.y
	self.width = table.width or 100 
	self.height = table.height or 100
	self.zmap = table.zmap
	self:makeColision()
end  

function weather:draw(debug)
	local sx,sy = self.scalex,self.scaley
	local x,y = -self.posx,-self.posy
	setColor(colors.green)
	if Gamestate.current() == Editor then 
		self:getColision():draw("line")
	end 
	if not self.global then 
		x,y = self:getColision():center()
		x = -self.posx
		y = y - (love.graphics.getHeight()) + ((self.height/2)*self.scaley)
	else 
		x,y = -self.posx,-self.posy
	end 
	
	if self.draw_effect then
		self.draw_effect(x,y)
	end
	setColor(colors.white)
end 
function weather:remove()
	self.colider:remove(self.renderRange)
end 
return weather