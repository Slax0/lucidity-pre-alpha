local station = {} 
function station:new(editor,colider,phyWorld,x,y,width,height)
	local s = {
		x = x or 0, 
		y = y or 0 , 
		width = width or 20,
		height = height or 20,
		colider = colider,
		scalex = 1,
		scaley = 1,
		papa = editor,
		type = "station",
	} 
	setmetatable(s,{__index = station})
	s:makeColision()
	return s 
end 
function station:setSize(w,h)
	self.width = w 
	self.height = h 
	self:makeColision()
end
function station:makeColision()
	local x,y,r,sx,sy = self.x,self.y,self.r,self.scalex,self.scaley
	local w,h = self.width or 20,self.height or 20
	local col = self.colider 
	if self.colision then col:remove(self.colision) end 
	
	self.colision = col:addRectangle(x,y,w*sx,h*sy)
	col:addToGroup(tostring(self),self.colision)
	function self.colision.getUserData()
		return {source = self,part = "main"}
	end 
	if self.OnMakeColision then 
		self.OnMakeColision(self.colision)
	end 
	if r then 
		self.colision:setRotation(r)
	end 
end 
function station:update(dt)
	if self.animation then self.animation:update(dt) end 
	if self.OnUpdate then self.OnUpdate(dt) end 
	if self._OnUpdate then self._OnUpdate(dt) end 
	local gs = Gamestate.current()
	if gs == Editor then 
		local width = self.width
		local height = self.height
		local w,h = (width)/2,(height)/2
		self.topX,self.topY = incrementPos(self.x,self.y,w,h) 
		if not self.Free then 
			self.dx = self.topX + (self.modx or 0) 
			self.dy = self.topY + (self.mody or 0)
		else 
			self.dx = self.x 
			self.dy = self.y
		end 
		self.colision:moveTo(self.dx,self.dy)
	else 
		self.colision:moveTo(self.x,self.y)
	end 
	if self._hasPlayer then 
		queryKey("Use",gs.play_keyControler,function()
			self:use()
		end)
	end 
end 
function station:setAnimation(anim)
	local _,_,w,h = anim:getViewport()
	self.width = w 
	self.height = h
	self.animation = anim 
	
	self:makeColision()
end 
function station:setImage(img)
	self.image = img
	local w,h = self.image:getDimensions()
	self.width =  w 
	self.height = h 
	
	self:makeColision()	
end 
local gs = Gamestate.current()
function station:draw(debug)
	local x,y,r,sx,sy = self.dx,self.dy,self.r,self.scalex,self.scaley
	if not x then 
		x,y = self.colision:center()
	end 	
	setColor(colors.green)
	if gs == Editor then 
		self.colision:draw("line")
	end 
	setColor()
	if self.image then 
		love.graphics.draw(self.image,x - self.width/2,y - self.height/2,r,sx,sy) 
	elseif self.animation then 
		self.animation:draw(x,y,r,sx,sy) 
	elseif debug then 
		self.colision:draw("line")
	end 
	if self.OnDraw then self.OnDraw(x,y,r,sx,sy) end 
	if self._OnDraw then self._OnDraw(x,y,r,sx,sy) end 
end 
function station:getColision()
	return self.colision
end 
function station:checkCol(a,b)
	local other 
	local part 
	local own 
	local otherPart 

	local ad 
	local bd 
	
	if a.getUserData then 
		ad = a:getUserData()
	end 
	if b.getUserData then 
		bd = b:getUserData()
	end
	if ad and ad.source == self then
		part = ad.part 
		own = a 
		if b.getUserData then 
			other = b:getUserData().source
			otherPart = b:getUserData().part
		end 
	elseif bd and bd.source == self then 
		part = bd.part
		own = b 
		if a.getUserData then 
			other = a:getUserData().source
			otherPart = a:getUserData().part
		end 
	end 
	return other,otherPart,own,part 
end 
function station:HCol(dt,a,b)
	local other,opart,own,part = self:checkCol(a,b)
	if other and other.type == "player" then 
		if self.OnEnter then self.OnEnter(other) end 
		self._hasPlayer = true
		self.target = other
	end 
end 
function station:HStop(dt,a,b) 
	local other,opart,own,part = self:checkCol(a,b)
	if other and other.type == "player" then 
		if self.OnExit then self.OnExit(other) end 
		self._hasPlayer = false 
		self.target = nil 
	end 
end 
function station:save()
	local t  = {
		x = self.dx or self.x, 
		y = self.dy or self.y,
		width = self.width,
		height = self.height,
		zmap = self.zmap,
		noImage = self.noImage,
	} 
	if self.image_source then 
		t.image_data = {
			sheet = self.image_source.sheet,
			quad = self.image_source.quad
		}
	end 
	return t 
end
function station:remove()
	self.colider:remove(self:getColision()) 
end 
function station:isPlayerInside()
	return self._hasPlayer
end 
function station:load(t)
	local editor = Gamestate.current()
	self.x = t.x
	self.y = t.y 
	self.zmap = t.zmap 
	if not t.noImage then 
		if t.image_data then 
			local is = t.image_data
			local s = editor.textureManager:seek(is.sheet)
			local q = s:seek(is.quad)
			if s and q then 
				assert(q,"Failed to find quad: ",is.quad)
				self:setTexture(s,q)
			else 
				print("Failed to find: ",is.sheet,is.quad)
			end 
		end
	else 
		self.noImage = t.noImage
		self.image = nil 
		self.width = t.width 
		self.height = t.height
		self:makeColision()
	end 
end 
function station:use()
	if self.OnUse then self.OnUse(self,target) end
end
local function setTexture(self,sheet,quad)
	self.image = nil 
	local editor = Gamestate.current()
	local textureMapper = textureMapper or editor.textureMapper
	local textureManager = editor.textureManager
	if not sheet.getImage then 
		sheet = textureManager:seek(sheet)
		quad = sheet:seek(quad)
	end 
	local img = sheet:getImage()
	self.TData = {sheet.name,quad.name}
	local x,y,w,h = quad.actual:getViewport()
	self.width = w 
	self.height = h 
	self:makeColision()
	local texture = textureMapper:new(self,img,quad.actual)
	self.image_source = {
		sheet = sheet.name,
		quad = quad.name, 
		img = img,
	}
	self._OnUpdate = function(dt)
		if self.texture then self.texture:update(dt) end 
	end 
	self._OnDraw = function()
		setColor(self.color)
		if self.texture then self.texture:draw() end 
		setColor()
	end
	self.texture = texture
end 
local function removeTexture(self) 
	if self.texture then self.texture:remove()  self.texture = nil end 
	if self.image_source then self.image_source = nil end
end
function station:setTexture(sheet,quad)
	setTexture(self,sheet,quad)
end  
function station:OnRightClick(menu,ui)
	local ed = Gamestate.current()
	menu:addChoice("Set Texture",function()
		removeTexture(self)
		ed:pickImage(function(sheet,quad)
			self:setTexture(sheet,quad)
		end)
	end)
	local frame 
	menu:addChoice("Set Dimensions",function()
		if frame then 
			frame:remove()
		end
		local fh = ui:getFont():getHeight() 		
		frame = ui:addFrame("Set Dimensions",nil,nil,210,fh + 20)
		frame:addCloseButton()
		local t = ui:addTextinput(frame,5,20,200,fh)
		t.OnChange = function()
			local data = parseTextVariables(t.text)
			local w,h = data[1] or 100,data[2] or 100
			self.width = math.max(1,w) 
			self.height = math.max(1,h)
			self:makeColision()
		end
		frame:setHeader()
		
	end)
end

return station 