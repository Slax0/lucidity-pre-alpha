local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/anim.png")
local w,h = RCToWH(image,4,8)
local mob = {
		info = {
			name = "Tremble",	-- Name on card
			maxHp = 150,	--	100 or higher
			maxMp = 5, -- 100 or higher
			-- Below all stats cam go up to 100
			def = 2,
			atk = 20,
			luck = 2, 
			level = 4,
			--- STOP
			skills = {
			},
			story = "The spirit of murder", -- The story that shows up 
			rarity = 6, --super common 1 to 10 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 7%,6 = 5%, 7 = 3%, 8 = 2.5% , 9 = 1.5%, 10 = 1% 
			element = "dark", -- The element that shows up on the card when you capture it/buy it.
			weak = {
				"divine"
			},
			strong = {
				"attack",
			},
			invin = { 		--Invincibility, you don't lose a heart with this
				"dark",
			},
		},
		stillFrame =1,
		w = w,
		h = h,
		scale = 1.5,
		mirror = true,
		states = {
			{
				name = "run",
				anim = {w,h,0.14,1,8},
				func = function() end,
			},
			{
				name = "attack",
				anim = {w,h,0.09,6,13},
				func = function() end,
			},
			{
				name = "death",
				anim = {w,h,0.09,25,32},
				func = function() end,
			},
			{
				name = "cast",
				anim = {w,h,0.14,6,13},
				func = function() end 
			},
			{
				name = "idle",
				anim = {w,h,0.14,17,21},
				func = function() end,
			},
		},
		state = "idle",
		image = image, -- defining own image, for manipulation
	}
return mob