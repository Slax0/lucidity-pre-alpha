local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/anim.png")
local w,h = RCToWH(image,5,8)
local mob = {
		info = {
			name = "Avictus",	-- Name on card
			maxHp = 100,	--	100 or higher
			maxMp = 100, -- 100 or higher
			-- Below all stats cam go up to 100
			def = 5,
			atk = 10,
			luck = 10, 
			level = 3,
			sourceCrystal = "player",
			--- STOP
			stillFrame = 1,
			skills = {
			{
				name = "Saint of the living",
				desc = "Guards the party, except himself.",
				cost = 90,
				damage = 0,
				-- if you want to heal do heal = no, damage = 0
				func = function(target,stack) -- Target is enemy -- tables is the damage meter -- stack: {self, team1, team2, finished}
					local damage = 6
					local mob = stack[1]
					local x,y = mob:getPos()
					mob:cast()
					mob.OnCast = function()
						for i,v in ipairs(target) do
							if v ~= mob then 
								v:setGuard(true)
							end
						end 
						stack[4] = true
						mob.OnCast = nil
					end
				end,
				type = "divine",
				target = "fTeam", -- targets: fTeam, team, self, friend, foe  
				image = nil
			},
			{
				name = "Heal",
				desc = "Heals [50%] of current HP of the target.",
				cost = 45,
				damage = 0,
				type = "heal",
				target = "friend",
				
				func = function(target,stack)
					local mob = stack[1]
					mob:cast()
					mob.OnCast = function()
						target:applyDamage((target.hp/100)*50,"heal")
						stack[4] = true
						mob.OnCast = nil
					end 
				end,
				
			},
			{
				name = "Curse",
				desc = "Curse[10 ATK] a target for 3 turns",
				cost = 25,
				damage = 0,
				type = "dark",
				target = "foe",
				
				func = function(target,stack)
					local mob = stack[1]
					mob:cast()
					mob.OnCast = function()
						local no = 3
						local i = 0
						target.effect = "curse"
						target.OnChangeTurn = function() 
							local dmg = 10
							local type = "dark"
							if i >= 3 then 
								target.effect = nil
								target.OnChangeTurn = nil
							end
							i = i + 1
							return dmg,type
						end 
						stack[4] = true
						mob.OnCast = nil
					end 
				end,
				
			}
			},
			story = "A guardian of sorts", -- The story that shows up 
			rarity = 4, --super common 1 to 10 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 7%,6 = 5%, 7 = 3%, 8 = 2.5% , 9 = 1.5%, 10 = 1% 
			element = "divine", -- The element that shows up on the card when you capture it/buy it.
			weak = {
				"fire",
				"attack",
			},
			strong = {
				"divine",
			},
			invin = { 		--Invincibility, you don't lose a heart with this
				"dark",
			},
		},
		w = w,
		h = h,
		states = {
			{
				name = "run",
				anim = {w,h,0.14,13,15},
			},
			{
				name = "attack",
				anim = {w,h,0.09,20,26},
			},
			{
				name = "death",
				anim = {w,h,0.09,33,40},
			},
			{
				name = "cast",
				anim = {w,h,0.14,6,12},
			},
			{
				name = "idle",
				anim = {w,h,0.14,1,6},
			},
		},
		state = "idle",
		image = image, -- defining own image, for manipulation
	}
return mob