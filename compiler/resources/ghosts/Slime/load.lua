local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/slime.png")
local w,h = 49,28
local mob = {
		info = {
			name = "Slime",	-- Name on card
			maxHp = 25,	--	10 or higher
			maxMp = 20, -- 10 or higher
			-- Below all stats cam go up to 100
			def = 2,
			atk = 12,
			luck = 1, 
			level = 1,
			sourceCrystal = "basic",
			--- STOP
			stillFrame = 1, -- The still frame found in the "Idle" state
			skills = { 
			{
				name = "Vomit",
				cost = 1,
				damage = 6,
				-- if you want to heal do heal = no, damage = 0
				func = function(target,stack) -- Target is enemy -- tables is the damage meter -- stack: {self, team1, team2, finished}
					local damage = 6
					local mob = stack[1]
					mob:cast() -- you can put an state here on default its "cast"
					mob.OnCast = function()
						local dmg,modif = target:applyDamage(damage)
						stack[4] = true
						mob.OnCast = nil
					end
					-- Alternatively to Cast:
					-- local frames = 3
					-- local v = target
					-- local w = 49
					-- local h = 28
					-- local anim = newAnimation(slime1.image,w,h,0.10,12,0,h*4) -- the animation used when doing the attack, default is "cast"
					-- mob.OnUpdate = function(dt) 
					-- if anim.position >= #anim.frames then 	-- Attack when animation is finished
							-- local dmg,modif = target:applyDamage(damage)
							-- local tables2 = {
								-- x = x2 - target.width/2,
								-- y = y2 - target.height,
								-- sx = 1,
								-- sy = 1,
								-- dmg = dmg,
								-- modif = modif,
								-- mob = target,
							-- }
							-- table.insert(tables,tables2)
							-- stack[4] = true
							-- mob:setAnimation(nil)
							-- mob.OnUpdate = nil
						-- end 
					-- end
				end,
				type = "water",
				target = "foe", -- targets: fTeam, team, self, friend, foe  
				image = nil
			}
			},
			story = "With every step that the slime steps it leaves green sludge behind, to later reform itself.", -- The story that shows up 
			rarity = 1, --super common 1,9 is rareness (how hard it is to capture) try to keep a good economy here, eg 1 = 30%, 2 = 25%.3 = 15%,4 = 10%,5 = 5%,6 = 2.5%, 7 = 2%, 8 = 1.8% , 9 = 1.5%, 10 = 1% 
			element = "water", -- The element that shows up on the card when you capture it/buy it.
			weak = {
				"fire",
				"ice",
				"divine",	-- Weaknesses, do double damage vs you and the attacks/skills do 2x the damage
			},
			strong = {
				"divine", 	-- increases defense and attack for selected, keep this low to avoid super powers 
			},
			invin = { 		--Invincibility, you dont lose a heart with this
				"stone",
			},
		},
		w = w,
		h = h,
		states = {
			{
				name = "run",
				anim = {w,h,0.09,1,13},
				func = function() end,
			},
			{
				name = "attack",
				anim = {w,h,0.10,30,38},
				func = function() end,
			},
			{
				name = "death",
				anim = {w,h,0.10,39,42},
				func = function() end,
			},
			{
				name = "cast",
				anim = {w,h,0.10,39,42},
				func = function() end 
			},
			{
				name = "idle",
				anim = {w,h,0.10,14,28},
				func = function() end,
			},
		},
		state = "idle",
		image = image, -- defining own image, for manipulation
	}
return mob