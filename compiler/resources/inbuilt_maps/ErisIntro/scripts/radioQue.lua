local game = Gamestate.current()
radioQue = function()
	if not game.SaveData["Intro"] then 
		Timer.add(2,function()
			Radio.Active = true 
			Radio:add("Oh how long I waited for this moment!")
			Radio:add("Finally, you are here and lucid.")
			Radio:add("It must have been just minutes for you.. but a millennia for me.")
			Radio:add("Since you are here, you made the right choice.")
			Radio:add("Go to your left and find a statue of wings, use it to preserve your image.")
			Radio:add("I must warn you that you have left your world.")
			Radio:add("The master of this new world may not be as keen as you.")
			Radio:add("They, just like you, have vicious cards and ghosts of their past.")
			Radio:add("You can view your current TEAM and CARDS at the top left.")
			Radio:add("if you encounter one of the Ghosts it will drag you into its space.")
			Radio:add("Beware that you cannot leave this space until either your team or its team is defeated.")
			Radio:add("If you lose you'll be trapped, forever.")
			Radio:add("For now lets focus on the protagonist of this world.")
			game.SaveData["Intro"] = true 
		end)
	end 
	if game.SaveData["endOfTheWorld"] then 
		table.insert(game.afterLoad,function()
			_G["UnlockDoors"]()
		end)
	end
end
_G["UnlockDoors"] = function()
	for i,v in ipairs(doors) do 
		v.Can_Use = true 
	end 
end

_G["radioPhase"] = {
	function()
		if not game.SaveData["endOfTheWorld"] then 
			game.SaveData["endOfTheWorld"] = true 
			Radio.Active = true 
			Radio:clearQue()
			Radio:add("The worlds end, and the end of her memory.")
			Radio:add("She is the source of this world, it is made of her memories and once they run out..")
			Radio:add("We still exist, so its safe to assume she's here somewhere.")
			Radio:add("One of the houses perhaps?")
		end 
	end,
}
_G["EriQue"] = function(self)
	local game = Gamestate.current()
	if not game.SaveData["eris"] then 
		game.SaveData["eris"] = true 
		local Cam = game:clampCamera(true)
		local Eri = self 
		local finished = false 
		Cam.modx = 0 
		Cam.mody = 0 
		game.tweenGroup:to(Cam,2,{modx = 100})
		Que = msgQue:new(0,0,Eri.width,Eri.height + 40)
		Que.Active = true 
		Que:add("Father.. What has befallen me?!")
		Que:add("Why have you left me to such a hideous fate!")
		Que:add("Our lands lie in peril but you still refuse to return.")
		Que:add("Have you abandoned us?!")
		Que:add("...",function()
			Eri:turnAround()
		end)
		Que:add("Who.. Who are you?")
		Que:add("...")
		Que:add("A mute?!",function()
			Eri:executeWaypoint(1,function()
				Que:add("Did the cat bite your tongue?")
				Que:add("If you naught to say then I must leave.")
				Que:add("That which cannot speak doesn't deserve my ears.",function()
					Eri:turnAround(function() 
						Eri:executeWaypoint(2,function()
							Eri.dead = true 
							Eri:remove()
						end)
						Eri.OnFocusLost = function()
							print("LOST FOCUS")
							Eri.dead = true 
							Eri:remove()
						end
						game.tweenGroup:to(Cam,2,{modx = 0}):oncomplete(function()
							game:releaseCamera()
						end)
						finished = true 
					end)
				end)
			end)
		end)
		function Cam:updateCamera(dt)
			local x,y = self.x + self.modx,self.y + self.mody 
			if not finished then 
				Que:setPosition(game:camPos(Eri.colision:center()))
				Que:update(dt)
			end 
			return x,y 
		end	
	end 
end
_G["radioQue"] = radioQue

doors = {} 
_G["AddDoor"] = function(door)
	table.insert(doors,door)
	door.Can_Use = false 
end

