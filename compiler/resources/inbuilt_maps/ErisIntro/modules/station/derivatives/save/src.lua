local _ = {} 
local font = love.graphics.newFont(uniBody,20)
local fontS = love.graphics.newFont(uniBody,12)
function _:new(module,dir,editor,colider,phyWorld,x,y)
	local img = love.graphics.newImage(dir.."/img.png")
	local w,h = img:getDimensions()
	w = w -2 
	h = h -2 
	
	local a = newAnimation(unpack(FloatingAnimation))
	local animation = {
		x = w/2,
		y = h -30,
		transp = 0,
	}
	local tweens = {}
	
	local station = module:new(editor,colider,phyWorld,x,y,w,h)
	local ui = editor.ui
	local frame 
	local canvas = love.graphics.newCanvas()
	local lg = love.graphics
	
	station.OnUse = function()
		if frame then 
			frame:remove()
			frame = nil 
		else 
			local w,h = love.graphics.getDimensions()
			local f = ui:addFrame(nil,w/2,h/2,204,326)
			frame = f 
			local list = ui:addList(f,2,2,200,300)
			local saves = getSaves()
			
			local saveName = ui:addTextinput(f,2,304,130,20)
			saveName:setText(playerData.saveName)
			
			for i,v in ipairs(saves) do 
				local time = v.playTime 
				local name = v.name 
				local mode = v.mapMode 
				local map = v.map
				local canvas = love.graphics.newCanvas(200,70)
				local b = ui:addButton(list,0,0,200,70) 
				if string.find(name,".troll") then 
					b.color = colors.red 
				else 
					b.color = colors.green
				end 
				b.OnDraw = function()
					love.graphics.draw(canvas,b.x - b.width/2,b.y - b.height/2)
					b.colision:draw("line")
				end 
				local date = "XX/XX/XX"
				local time = "XX:XX:XX"
				if v.date then 
					date = v.date[1] 
					time = v.date[2]
				end 
				local lg = love.graphics
				local text = {
					"Name: "..name, 
					"Map: "..(map or ""), 
					time.." "..date,
				}
				for i,v in ipairs(text) do 
					text[i] = v.."\n"
				end 
				love.graphics.setCanvas(canvas)
					local l = lg.getFont()
					lg.setFont(fontS)
					love.graphics.print(table.concat(text))
					lg.setFont(l)
				love.graphics.setCanvas()
				b.OnClick = function()	
					saveName:setText(name)
				end
				b.Name = name 
			end 

			local save = ui:addButton(f,132,304,70,20)
			save:setText("Save")
			save.OnClick = function()
				saveGame(saveName:getText())
				f:remove()
			end 
			for i,v in ipairs(list.items) do 
				if string.find(v.Name,".troll") then 
					list:removeItem(v,true)
					list:addItem(v,1)
				end 			
			end 
		end 
	end 
	
	local saved 
	
	function station.OnEnter()
		if not saved then print("Saving") saveGame("Autosave") saved = true end 
		station.Activated = true
		local Tween = Gamestate.current():getTween()
			Timer.add(0.5,function()
			RaysOn = true		
			Tween:to(animation,1,{transp = 225})	
		end)
	end
	
	function station.OnExit()
		if frame then 
			frame:remove()
			frame = nil 
		end
		saved = false
	end 	
	
	function station:OnMenu(menu,ui)
		local b = ui:addButton(nil,0,0,2,2)
		b:setText("Use")
		b.OnClick = function()
			station.OnUse() 
		end 
		menu:addItem(b)
	end
	
	local lg = love.graphics
	function station.OnUpdate(dt)
		canvas:clear()
		
		local item = animation
		upDown(item,-30,item.y,"y",3)	
		local _,_,w,h = a:getViewport()
		if station.Activated then 
			a:update(dt)
			local function fn() 
				
				lg.draw(img,-1,1)
				
				if RaysOn then 
					local _,_,w,h = a:getViewport()
					local v = animation
					setColor(173,173,173,v.transp)
					a:draw(v.x,v.y,0,1,1,w/2,h/2) 
				end 
				setColor()
			
				
			end 
			canvas:renderTo(fn)
		else 
			local function fn()
				lg.draw(img,-1,1)
			end 
			canvas:renderTo(fn)
		end 
	end 
	station.OnDraw = function()
		local x,y = station.colision:bbox()
		lg.draw(canvas,x,y) 
	end 
	return station 
end 
return _ 