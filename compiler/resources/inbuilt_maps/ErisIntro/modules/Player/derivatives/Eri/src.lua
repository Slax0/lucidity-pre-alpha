local eri = {}
function eri:new(module,dir,mode,colider,phyWorld,x,y)

	local scalex = 1 
	local scaley = 1 
	
	local image = love.graphics.newImage(dir.."/eri.png")
	local width,height = RCToWH(image,2,10)
	
	local eri = module:new(mode,colider,phyWorld,x,y,width,height,scalex,scaley)
	eri.bodyWidth = 18
	eri.bodyHeight = 45 
	eri:makeColision()
	-- newAnimation(image,fw, fh, delay,startFrame,finalFrame)
	local w,h = width,height
	
	local runing = newAnimation(image,w,h,0.09,1,8)
	local jumping = newAnimation(image,w,h,0.7,9,9)
	local falling = newAnimation(image,w,h,0.7,10,10)
	local still = newAnimation(image,w,h,0.5,11,18)
	local xchange = newAnimation(image,w,h,1,19,19)
	
	local states = {{name = "falling",anim = falling, },{name = "jumping",anim = jumping},{name = "idle",anim = still},
	{name = "moving",anim = runing},
	{name = "x_change", anim = xchange}}
	eri:setStates(states)
	eri.name = "Eri"
	
	local anim = stock_effects.jump.anim:addInstance()
	eri:setJumpEffect(anim)
	return eri
end 
return eri