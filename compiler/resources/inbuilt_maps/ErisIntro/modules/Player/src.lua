local player = {}
function player:new(mode,colider,phyWorld,x,y,width,height,scalex,scaley)
	local s = {
		colider = colider,
		phyWorld = phyWorld,
		x = x,
		y = y,
		scalex = scalex or 1,
		scaley = scaley or 1,
		width = width,
		height = height,
		type = "player",
		state = "idle",
		states = {},
	}
	setmetatable(s,{__index = player})
	return s:add() 
end 
local function checkPlayerColision(col,player)
	local player,part
	local data = col:getUserData()
	if data then 
		if data.source then 
			if data.source.type == "player" then 
				player = data.source
				part = data.part 
			end 
		end 
	end 
	return player,part
end 
function player:setStates(states)
	self.states = states
end 
function player:checkCol(a,b)
	local other 
	local part 
	local ad = a:getUserData()
	local bd = b:getUserData()
	if ad and ad.source == self then
		part = ad.part 
		other = b 
	elseif bd and bd.source == self then 
		part = bd.part
		other = a 
	end 
	if other and other:isSensor() then 
		other = nil 
	end 
	return other,part
end 
function player:add()
	local s = self 
	s.ctrl = {}
	s.direction = {}
	s.effects = {}
	s.timestamp = {}
	s.clamps = {}
	s.controlColisions = {}
	
	
	s.moving = false
	s.jumping = false
	s.clamps.yvel = false 
	s.Active = true 
	
	s.cWidth = 0 
	s.cHeight = 0 
	s.speed_limit = 450
	
	s.timestamp.a = 0
	s.timestamp.b = 0
	s.timestamp.clamp = 0
	
	s.yvel = 0
	s.xvel = 0
	
	s.colide = 0
	s.bodyColide = 0
	s.onGround = 0
	s.speed = 2000
	s.gravity = 2000
	
	return s 
end 
function player:makeColision()
	local body 
	if self.body then 
		self.body:destroy()
		self.body = nil 
	end 
	local phyWorld = self.phyWorld
	local colider = self.colider 
	if self.renderRange then colider:remove(self.renderRange) end  
	for i,v in pairs(self.controlColisions) do 
		colider:remove(v)
	end 
	
	self.renderRange = colider:addRectangle(self.x or 0,self.y or 0,self.width*self.scalex,self.height*self.scaley)
		function self.renderRange.getUserData()
			return {source = self,part = "body"}
		end 
	colider:addToGroup(tostring(self),self.renderRange)
	if not self.body and not self.defined_Body and phyWorld then
			local body = love.physics.newBody(phyWorld,self.x,self.y,"dynamic")
			body:isSleepingAllowed(false)
			local sx,sy = 1,1
			local ow = self.bodyWidth or self.width
			local oh = self.bodyHeight or self.height
			local w = (ow *sx)
			local h = (oh *sy)
			
			local wa = 0
			local w2 = ((ow/2)*sx)/1.5
			local h2 = ((oh/2)*sy)/4 -(2.5*sx)
			if w2 <= 0 then
				w2 = 15
			end
			if h2 <= 0 then
				h2 = 5
			end
						-- Make the ellipse
			local diameter = h/2
			local oh = h - diameter
			if diameter > w/2 then
				diameter = w/2
				wa = w/2 - (diameter)
				oh = h - diameter
			end
			local fixtures = {}
			local shapes = {}
			
			

						-- Make the ellipse
			
			shapes[1] = love.physics.newRectangleShape(0,-h/2 + oh/2,w,oh)
			fixtures[1] = love.physics.newFixture(body,shapes[1])
			fixtures[1]:setUserData({source = self,part = "Body"})
			
			shapes[2] = love.physics.newRectangleShape(0,h/2,w-6,5)
			fixtures[2] = love.physics.newFixture(body,shapes[2])
			fixtures[2]:setUserData({source = self,part = "Indicator"})
			fixtures[2]:setSensor(true)

			shapes[3] = love.physics.newCircleShape(w/2 - (diameter) - wa,h/2 - diameter,diameter)
			fixtures[3] = love.physics.newFixture(body,shapes[3])
			fixtures[3]:setUserData({source = self, part = "Ground"})
			
		self.shapes = shapes 
		self.fixtures = fixtures
		body:setFixedRotation(true)
		body:setMassData(0,0,0,0)
		body:setLinearDamping(0,0)
		self.body = body
		for i,v in ipairs(fixtures) do 
			v:setFriction(0)
			v:setCategory(16)
			v:setMask(16)
			v:setGroupIndex(-137)
		end 
		local x,y = 0,h/2
		if self.indicatorCol then colider:remove(self.indicatorCol) end 
		self.indicatorCol = colider:addRectangle(x,y,w-6,5)
		self.indicatorCol.ofx = x
		self.indicatorCol.ofy = y - (self.height - 4)
		function self.indicatorCol.getUserData()
			return {source = self,part = "Alt_Indicator"}
		end 
		colider:addToGroup(tostring(self),self.indicatorCol)
	end
	
	if self.OnMakeColision then 
		self.OnMakeColision(self.renderRange)
	end 
end 
function player:update(dt)
	self:navi()
	if not self.ctrl then 
		return 
	end 
	local left,right,jump = self.ctrl["left"],self.ctrl["right"],self.ctrl["jump"]
	local scalex,scaley = self.scalex,self.scaley 
		local _clamp = true
		if self.timestamp.clamp > 3 then
			_clamp = false
		end
	if self.Active then
			local m
			if left then
				m = true 
				self.direction["still"] = false
				if self.direction["right"] then 
					
					self.direction["right"] = false 
					self.direction["x_change"] = true 
				
				else 
					self.MClamp = (self.MClamp or 0) + 1 
					if self.MClamp > 15 then 
						self.direction["x_change"] = false
						self.MClamp = 0 
					end 
				end 
				self.direction["left"] = true
			elseif right then
				m = true
				self.direction["still"] = false
				if self.direction["left"] then 
	
					self.direction["left"]  = false 
					self.direction["x_change"] = true 
				else 
					self.MClamp = (self.MClamp or 0) + 1 
					if self.MClamp > 15 then 
						self.direction["x_change"] = false
						self.MClamp = 0 
					end 
				end 
				self.direction["right"] = true
			else 
				self.direction["still"] = true
			end
			if m and self.bodyColide > 0 and self.onGround > 0 then 
				self.direction["wall"] = true
				self.scalex = -1
			elseif  self.direction["wall"] then 
				self.scalex = 1
				self.direction["wall"] = false
			end 
		if self.body and not self.body:isDestroyed() then
			self:movement(dt)
			self.x,self.y = self.body:getPosition()
			self.renderRange:moveTo(self.x,self.y)
		else 
			local width = self.width
			local height = self.height
			local w,h = (width)/2,(height)/2
			self.topX,self.topY = incrementPos(self.x,self.y,w,h)
			self.dx = self.topX + (self.modx or 0) 
			self.dy = self.topY + (self.mody or 0)
			self.renderRange:moveTo(self.topX + (self.modx or 0),self.topY + (self.mody or 0))
		end
		if self.onGround > 0 then
			self.direction["jump"] = false
		elseif self.onGround == 0 then 
			self.direction["still"] = false
			self.direction["jump"] = true
		end
		local state 
		if self.direction["still"] then 
			state = "idle"
		elseif self.direction["x_change"] then 
			state = "x_change"
		elseif self.direction["wall"] then 
			state = "wall"
		elseif  (self.direction["left"] or self.direction["right"]) and not _clamp then  
			state = "moving"
		end 
		if not state then 
			state = "idle"
		end 
		if self.onGround == 0  then 
			if self.yvel > 0 then
				state = "falling"
			elseif self.yvel < 0 then
				state = "jumping"
			end
		end 
		self.state = state
		for i,v in pairs(self.states) do 
			if state == v.name then 
				v.anim:update(dt)
			elseif v.restFrame then 
				v.anim:seek(v.restFrame)

			end 
		end
		self:updateEffects(dt)
	end 	
		if self.indicatorCol then self.indicatorCol:moveTo(self.x - self.indicatorCol.ofx,self.y - self.indicatorCol.ofy) end 
end 
function player:getColision()
	if not self.renderRange then 
		self:makeColision()
	end 
	return self.renderRange 
end 
function player:navi()
	local mLeft,mRight,mJump = keys.game[1].f(),keys.game[2].f(),keys.game[5].f() 
	if self.ctrl then 
		self.ctrl["left"] = mLeft
		self.ctrl["right"] = mRight
		self.ctrl["jump"] = mJump
	end 
end 
function player:updateCamera(dt,cameraSlide)

		
		local cameraSettings = self.cameraSettings
		local c = true
		-- misc.cameraSlide
		if not self.body or self.body:isDestroyed() then c = false end  
		if c then 

			local x,y = self.x,self.y
			local cfx = (self.cameraFactorX or 70)*math.abs(self.scalex)
			local cfy = (self.cameraFactorY or 50)*math.abs(self.scaley)
			cfy = 1
			local cameraBX = (self.cameraBoundX or 50)*math.abs(self.scalex)
			local cameraBY = (self.cameraBoundY or 200)*math.abs(self.scaley)
			local maxFrontView = cfx
			local maxJumpView = cfy
			local direction = self.direction
			local x = self.x - self.width/2
			local y = -(self.y - self.height/2)
			local y = -y
			local cWidth = self.cWidth
			local cHeight = self.cHeight
			local body = self.body
			
			local dir = self.direction
			
			local left = dir["left"]
			local right = dir["right"]
			local jumping = dir["jump"]
			local cfx = cfx*2
			local movRange = 2
			if not cameraSlide then 
				cWidth = 0 
				cHeight = 0 
			else 
			
			if left then
				if cWidth >= -(cameraBX + movRange*self.scalex ) then
					cWidth = cWidth -(cfx*dt)
				end
				if cWidth == -cameraBX then
					cWidth = -cameraBX
				end
			elseif right then
				local nw = cameraBX 
				if cWidth <= nw then
					cWidth = cWidth + (cfx*dt)
				end
				if cWidth == nw then
					cWidth = nw
				end
			end 
			if jumping then
				local sc = cameraBY
				if self.yvel > 0 then
					cHeight = cHeight - (cfy*dt)
				elseif self.yvel < 0 then 
					cHeight = cHeight + (cfy*dt)
				end 
				if math.abs(cHeight) >  sc then
					if cHeight < 0 then 
						cHeight = -sc 
					else 
						cHeight = sc
					end 
				end
			end
			if self.onGround ~= 0 then
				local cfy = cfy*2
				if cHeight ~= 0 then
					if cHeight <= 0  then
						cHeight = cHeight + (cfy*dt)
					end
					if cHeight >= 0 then
						cHeight = cHeight - (cfy*dt)
					end
				end
			end
		end 
		local x = x + cWidth + 0.00001
		local y = -y + cHeight + 0.00001
		self.cWidth = cWidth
		self.cHeight = cHeight
		return x + (self.cModX or 0 ),-y + (self.cModY or 0 )
	end
end 
function player:movement(dt)
	self:navi()
	local body = self.body
	if not body:isDestroyed() then 
		local jump = false
		local _moveClamp
		local _clamp = true
		local timestamp = self.timestamp
		local timeJump = self.timestamp.a
		local timeKey = self.timestamp.b
		local brake = false
		local speed_limit = self.speed_limit
		
		local c_left,c_right,c_jump = self.ctrl["left"],self.ctrl["right"],self.ctrl["jump"]
		
		if not self.speed then
			self.speed = 3000
		end

		if c_left or c_right then 
			self.timestamp.clamp = self.timestamp.clamp + 10*dt
			self.inactive = false
		else
			self.inactive = true
			self.timestamp.clamp = 0
		end
		if self.timestamp.clamp > 3 then
			_clamp = false
		end

		local xvel,yvel = body:getLinearVelocity()
		body:setAngularVelocity(0)
		body:setLinearVelocity(math.round(xvel,3),math.round(yvel,3))
		
		local groundColision
		if self.colide > 0 then
			groundColision = true
		else
			groundColision = false
		end
		
		local bodyColision
		if self.bodyColide > 0 then
			bodyColision = true
		else
			bodyColision = false
		end
		
		local groundConfirm = 0 
		if self.onGround > 0 then
			groundConfirm = true 
		else
			groundConfirm = false 
		end
		if groundColision then
			groundConfirm = true
		end
		
		self.directionChanged = false
		
		if self.direction["x_change"] then 
			local x,y = body:getLinearVelocity()
			body:setLinearVelocity(0,y)
		end 

		if not self.inactive then
			if xvel < speed_limit and  xvel > -speed_limit then
				if not  _clamp then
					local xvl,yvl = body:getLinearVelocity()
					if c_left then
						xvl = xvl - (self.speed*dt)
						body:setLinearVelocity(xvl,yvl)
					elseif c_right then
						xvl = xvl + (self.speed*dt)
						body:setLinearVelocity(xvl,yvl)		
					end
				end
			else
				local xvel,yvel = body:getLinearVelocity()
				if xvel > 0 then
					xvel = speed_limit
				elseif xvel < 0 then
					xvel = -speed_limit
				end
				
				body:setLinearVelocity(xvel,yvel)
			end
		else
			local xvel,yvel = body:getLinearVelocity()
			if xvel > 0 then
				xvel = xvel - (self.speed*4)*dt
			end
			if xvel < 0 then
				xvel = xvel + (self.speed*4)*dt
			end
			body:setLinearVelocity(xvel,yvel)
		end
		if self.onGround ~= 0 and groundColision and  c_jump then
			jump = true 
			local xvl,yvl = body:getLinearVelocity()
			local yvl = yvl - 700
			body:setLinearVelocity(xvl,-700)
		end
		
		local _ = false
		local xvl,yvl = body:getLinearVelocity()
		if self.inactive or _clamp then
			_ = true
		else
			_ = false
		end
		
		yvl = yvl + ((self.gravity)*dt)
		
		if self.onGround ~= 0 and groundColision and groundConfirm and _ then
			if math.abs(math.floor(yvl)) == 1 or  math.abs(math.floor(yvl)) < (3000*dt) then
				yvl = 0
				body:setLinearVelocity(xvl,yvl)
			end
			if math.abs(math.floor(xvl)) == 1 or  math.abs(math.floor(xvl)) < (7000*dt) then
				xvl = 0
			end
		end
		
		body:setLinearVelocity(math.floor(xvl),math.floor(yvl))
		if self.OnMovement then 
			self.OnMovement(dt)
		end 
		local xvl,yvl = body:getLinearVelocity()
		self.yvel = yvl
		self.xvel = xvl
	end 
end 
function player:save()
	local t = {}
	t.x = self.x 
	t.y = self.y 
	t.scalex = self.scalex 
	t.scaley = self.scaley 
	t.zmap = self.zmap or 1
	t.expectedOneLoad = self._oneLoad
	return t
end 
function player:load(table)
	self.x = table.x 
	self.y = table.y 
	self.zmap = table.zmap
	self.scalex = table.scalex
	self.scaley = table.scaley 
	self.expectedOneLoad = table.expectedOneLoad
	self:makeColision()
end 
function player:beginContact(a,b,coll)
	local other,part = self:checkCol(a,b)
	if other then 
		if part == "Ground" then 
			if coll:isTouching() then
				self.colide = self.colide + 1
			end 
		elseif part == "Body" then 
			self.bodyColide = self.bodyColide + 1
			local x,y = coll:getNormal()
			local _,f = math.modf(y)
			local _,d = math.modf(x)
			if f ~= 0 then 
				self:onClip(true)
			elseif d ~= 0 then 
				self:onClip(true)
			end 
		elseif part == "Indicator" then 
			self.onGround = self.onGround + 1
		end 
	end 
end 
function player:onClip(c)
	if c then 
		self.cliping = (self.cliping or 0) + 1
	else 
		self.cliping = (self.cliping or 0) - 1
	end
end 
function player:endContact(a,b,coll)
	local other,part = self:checkCol(a,b)
	if other then 
		if part == "Ground" then 
			if coll:isTouching() then
				self.colide = self.colide - 1
			end 
		elseif part == "Body" then 
			self.bodyColide = self.bodyColide - 1
			local x,y = coll:getNormal()
			local _,f = math.modf(y)
			local _,d = math.modf(x)
			if f ~= 0 then 
				self:onClip(false)
			elseif d ~= 0 then 
				self:onClip(false)
			end 
		elseif part == "Indicator" then 	
			self.onGround = self.onGround - 1
			if self.onGround < 0 then 
				self.onGround = 0
			end 
			if self.onGround == 0 then 
				local x = self.x
				local y = self.y 
				local sx = 1
				if self.direction["right"] then 
					sx = -1
				elseif self.direction["left"] then 
					sx = sx 
				end
				if not self.OnMovement then 
					self:jumpEffect(x,y,0,self.scalex,self.scaley,sx)
				end 
			end 
		end 
	end 
end 
function player:drawEffects()
	for i,v in pairs(self.effects) do 
		v.draw()
	end 
end 
function player:updateEffects(dt)
	for i,v in pairs(self.effects) do 
		v.update(dt)
	end 
end 
function player:jumpEffect(x,y,r,sx,sy,dir)
	if self.jump_effect then 
		local anim = deepCopy2(self.jump_effect.anim)
		local w,h = anim.img:getDimensions()
		local w = w/#anim.frames
		setmetatable(anim,{__index = self.jump_effect.anim.__index})
		anim:setMode("once")
		local effect = {
			draw = function()
				anim:draw(x + (2*sx),y + 2,r,sx*dir,sy,w/2,h/2)
			end,

		}
		effect.update = function(dt)
			anim:update(dt)
			if not anim.playing then 
				removeFromTable(self.effects,effect)
			end 
		end 
		table.insert(self.effects,effect)
	end 
end 
function player:setJumpEffect(anim)
	self.jump_effect = {anim = anim}
end 
function player:draw(debug)
	local sx,sy = self.scalex,self.scaley
	local x,y = self.x,self.y 
	if not self.body then 
		x,y = self.dx,self.dy
	end 
	if self.direction["left"] then 
		sx = -sx 
	end 
	local state = self.state
	for i,v in pairs(self.states) do 
		if state == v.name then 
			v.anim:draw(x,y,self.r or 0,sx,sy,self.width/2,self.height/2)
		end 
	end
	self:drawEffects()
	setColor(colors.green)
	if self.body and not self.body:isDestroyed() and debug then 
		for i,v in pairs(self.shapes) do 
			if v.getPoints then 
				love.graphics.polygon("line",self.body:getWorldPoints(v:getPoints()))
			else 
				local x,y = self.body:getWorldPoints(v:getPoint())
				love.graphics.circle("line",x,y, v:getRadius())
			end 
		end 
	end 
	if debug  then 
		setColor(colors.white)
		if self.indicatorCol then self.indicatorCol:draw("fill") end 
		if self.renderRange then self.renderRange:draw("line") end 
	end 
	setColor(colors.white)
end 
function player:getFixtures()
	return self.fixtures 
end 
function player:remove()
	
	if self.body and not self.body:isDestroyed() then 
		self.body:destroy()
	end 
	if self.colider then 
		self.colider:remove(self.renderRange)
		for i,v in pairs(self.controlColisions) do 
			self.colider:remove(v)
		end 
		for i,v in pairs(self) do 
			if type(v) ~= "function" then 
				self[i] = nil
			end 
		end
	end 
end 
function player:moveTo(x,y)
	self.x = x 
	self.y = y
	if self.body and not self.body:isDestroyed() then 
		self.body:setPosition(x,y)
	end 
end 
return player