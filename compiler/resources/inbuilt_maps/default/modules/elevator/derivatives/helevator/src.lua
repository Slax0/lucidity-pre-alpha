local der = {}
function der:new(module,dir,mode,colider,phyWorld,x,y)
	print("phyWorld = ",phyWorld)
	local door_der = self.dep[1].derivative
	local door_mod = self.dep[1].module
	local door_dir = self.dep[1].der_dir 
	local img = love.graphics.newImage(dir.."/image.png")
	local image = crop(img,0,0,192,124) 
	local rail = crop(img,193,0,42,32) 
	local endRail = crop(img,193,32,41,39)  
	local smallFont = makeFont(5)
	local medFont = makeFont(6)
	
	local tind = crop(img,86,13,19,20)
	local w,h = 14,23
	local pyind = {
		off = crop(img,194,72,w,h),
		on = crop(img,209,72,w,h),
		there = crop(img,194,96,w,h),
	}
	
	local b = module:new(mode,colider,phyWorld,x,y,200,6,1,1)
	b:setImage(image,nil,true,false,0,-1)
	b:setRail(rail)
	b:setRailEnd(endRail)
	b:setSpeed(100)
	b:setPause(50)
	b:setSmooth(true)
	b:makeColision()
	
	local door = door_der:new(door_mod,door_dir,mode,colider,phyWorld)
	door.parent = b
	local MAINDOOR = door
	local door2 = door_der:new(door_mod,door_dir,mode,colider,phyWorld)
	door2.parent = b
	if phyWorld then 
		b:addFrame(function(self)
			local shape = love.physics.newRectangleShape(0,-(self.items[1].image:getHeight())*self.scaley,self.width*self.scalex,self.height*self.scaley)
			local x = 0
			local y = (self.items[1].image:getHeight()*self.scaley)
			local types = "dynamic"
			return shape,x,y,types
		end)
		b:addFrame(function(self)
			local h = (self.items[1].image:getHeight()*self.scaley) - door.height
			local w = door.width
			local x = (self.items[1].image:getWidth()/2)*self.scalex
			local y = -(self.items[1].image:getHeight()*self.scaley)
			local shape = love.physics.newRectangleShape(x - w/2 +1,y + h/2 +1,w,h)
			local types = "static"
			return shape,x,y,types
		end)
		b:addFrame(function(self)
			local h =(self.items[1].image:getHeight()*self.scaley) - door.height
			local w = door.width
			local x = -(self.items[1].image:getWidth()/2)*self.scalex
			local y = -(self.items[1].image:getHeight()*self.scaley)
			local shape = love.physics.newRectangleShape(x + w/2 -1,y + h/2 +1,w,h)
			local types = "static"
			return shape,x,y,types
		end)
	end 
	b:setSpeed(100)
	b:setPause(50)
	
	door:setMirror(true)
	
	b.current = 2 
	
	local ox = -((b.width/2)*b.scalex)
	local oy = ((b.height/2)*b.scaley)
	local Elevator = b 
	local time = 0
	local mainFrame 
	local ui = Gamestate.current():getUi()
	local forceClose = false
	local buttons = {}
	b.onUpdate = function(...)
		local body = b.body
		local x,y = b:getColision():center()
		if body then 
			x = body:getX()
			y = body:getY()
		else 
			x = ((b.dx or b.x))
			y = ((b.dy or b.y) + b._height/2)
		end
		b._Ux = x 
		b._Uy = y 
		if forceClose then 
			b.active = true
			b.timer = 0
			forceClose = false
		end 
		if b._HasPlayer then 
			local gs = Gamestate.current() 
			queryKey("Use",gs.play_keyControler,function()
				if mainFrame then 
					mainFrame:remove()
				end 
				local w,h = love.graphics.getDimensions()
				mainFrame = ui:addFrame("Select floor",w/2,h/2,140,240)
				mainFrame.dragEnabled = true
				mainFrame.OnRemove = function()
					clearTable(buttons)
				end 
				mainFrame:setHeader()
				mainFrame.color = basicFrameColor
				local closeDoor = ui:addButton(mainFrame,5,210,30,30)
				closeDoor:setText(">|<")
				closeDoor.OnClick = function()
					Elevator._Called = false 
					if string.find(door.status,"close") and Elevator.des == Elevator.current then 
						door2:open(true)
						door:open(true)
						forceClose = false 
						Elevator.active = false
					else 
						forceClose = true 
					end 
				end
				local _ = ui:addButton(mainFrame,35,210,100,30)
				_:setText("Exit")
				_.OnClick = function()
					Elevator._Called = false 
					mainFrame:remove()
					mainFrame = nil
				end
				local list = ui:addList(mainFrame,5,20,120,165)
				local floors = b.floor
				for i = #floors,1,-1 do 
					local b = ui:addButton(nil,0,0,115,30)
					b:setText(i)
					if Elevator.des == i then 
						b.color = colors.blue
					elseif Elevator.current == i then 
						b:setInactive(true)
					end 
					b.OnClick = function()
						Elevator._Called = false
						Elevator:goTo(i)
						mainFrame:remove()
						mainFrame = nil 
					end 
					list:addItem(b)
					table.insert(buttons,b)
				end 
			end)
		end 
		local args = {...}
		local dt = args[1]
		local self = args[2]
		local body = self.body
		local x,y = self:getColision():center()
		local w,h = door.width,door.height
		if body then 
			x = body:getX() - w/2
			y = body:getY() - h/2
		else 
			x = ((self.dx or self.x))- w/2
			y = ((self.dy or self.y) + self._height/2 )- (h/2) +1
		end 
		local x = x 
		local y = y 
		local _clamp
		local anim = door.animation
		b.IsBlocked = false
		if door:collidesWithPlayer() or  door2:collidesWithPlayer() then 
			if anim:getCurrentFrame() == door.image_o then
				_clamp = true 
				b.IsBlocked = true
			else 
				b.IsBlocked = false
			end 
		end
		if not _clamp then
			if not self.active and string.find(door.status,"closed") and b.des == b.current then
				self.free = false
				door:open(true)
				door2:open(true)
			end 
			if string.find(door.status,"open")  and self.active then
				door:close(true)
				door2:close(true)
			end
			if anim:getCurrentFrame() == door.image_o then
				b.clamp = true
			elseif anim:getCurrentFrame() == door.image_c then
				time = time + 0.1
			end 
			if time > 30 then
				b.clamp = false
				time = 0
			end
		end 
		if Elevator._Called then 
			Elevator:goTo(Elevator._Called)
		end 
		door:moveTo(x - ox+1,y - oy)
		door2:moveTo(x + ox + door.width -1,y - oy)
		if buttons then 
			local des = b.des 
			local cur = b.current
			for i,v in ipairs(buttons) do 
				if b.floor[i] then 
					v.color = colors.gray
					v:setInactive(false)
					if i == des then 
						v.color = colors.blue 
					elseif i == cur then 
						v:setInactive(true)
					end 
				else 
					v:remove()
				end 
			end 
		end 
	end
	b.children = {
		door,
		door2,
	}
	b.onRemove = function() 
		door:remove()
		door2:remove()
	end

	function b.OnPlayerExit()
		if mainFrame then mainFrame:remove() end 
	end
	
	local of = b.newFloor
	function b:newFloor(...)
		local f = of(b,...)
		local od = f.draw
		function f:draw()
			od(self)
			if self.ControlP then 
				local des,cur = b.des,b.current
				for i,v in ipairs(self.ControlP.doors) do 
					local x,y = v.x,v.y
					if v._isLeft then 
						x = x + v.width - tind:getWidth()/2
					else
						x = x - (v.width + tind:getWidth()/2)
					end 
					y = y - (v.height/2)
					local f = love.graphics.getFont()
					love.graphics.setFont(smallFont)
					love.graphics.draw(tind,x,y)
					local t = ""
					if des < cur then 
						t = "-"..cur
					elseif des > cur then 
						t = "+"..cur
					else 
						t = cur 
					end 
					love.graphics.printf(t,x,y + tind:getHeight()/4,tind:getWidth(),"center")
					love.graphics.setFont(f)
					local img = pyind.off
					if b._Called then 
						if b.floor[cur] ~= self and b._Called == self.index then 
							img = pyind.on
						elseif b._Called == self.index then 
							img = pyind.there
						end 
					end 
					love.graphics.draw(img,x,y + tind:getHeight() + 10,0,1,1)
				end 
			end 
		end
		local oo = f.update
		function f:update(dt)
			oo(self,dt)
			if self.ControlP  then 
				local des = b.des
				local cur = b.current
				for i,v in ipairs(self.ControlP.doors) do 
					local door = v
					if des == cur and b.floor[cur] == self then 
						if string.find(door.status,"closed") and not b.active then 
							door:open(true)
						end 
					else 
						if not string.find(door.status,"closed") and b.active then 
							door:close(true)
						end 
					end 
					local x,y = self:getColision():bbox()
					if not v._isLeft then 
						door:moveTo(x -(door.width/2 -1),y-door.height/2+2)
					else
						local w,h = findDimensions(self:getColision())
						local x = x + w 
						door:moveTo(x + (door.width/2 - 1),y-door.height/2+2)
					end
					if door:playerIsInRange()  then 
							if (door.Direction == 1 and v._isLeft) or door.Direction == 1 then 
							queryKey("Use",Gamestate.current().play_keyControler,function()
								Elevator._Called = self.index
							end)
						end 
					end 
				end
			end 
		end 
			local function createDoor(self,mode)
				if not self.ControlP then 
					self.ControlP = {save = {},doors = {}}  
				end
				if not self.ControlP.save[mode] then 
					self.ControlP.save[mode] = true 
					local door = door_der:new(door_mod,door_dir,mode,colider,phyWorld)
					door.parent = self
					if mode == "left" then 
						door._isLeft = true
						door:setMirror(true)
					end 
					table.insert(b.children,door)
					table.insert(self.ControlP.doors,door)
				else 
					for i,v in ipairs(self.ControlP.doors) do 
						if mode == "left" and v._isLeft then 
							v:remove()
						elseif mode == "right" then 
							v:remove()
						end 
					end 
					self.ControlP.save[mode] = nil 
				end 
			end 
		function f:OnMenu(m,ui,e) 
			local c = ui:addButton(nil,0,0,20,20)
			c:setText("Add Door[right]")
			m:addItem(c)
			c.OnClick = function()
				createDoor(self,"right")
			end
			local c = ui:addButton(nil,0,0,20,20)
			c:setText("Add Door[left]")
			c.OnClick = function() 
				createDoor(self,"left")
			end 
			m:addItem(c)
		end 
		function f:OnSave()
			if self.ControlP then 
				return self.ControlP.save
			end 
		end 
		function f:OnLoad(t)
			if t then 
				self.ControlP = {save = {},doors = {}} 
			end 
			if t.right then
				local door = door_der:new(door_mod,door_dir,mode,colider,phyWorld)
				door.parent = self
				self.ControlP.save.right = true
				table.insert(b.children,door)
				table.insert(self.ControlP.doors,door)
			end
			if t.left then 
				local door = door_der:new(door_mod,door_dir,mode,colider,phyWorld)
				door.parent = self
				door._isLeft = true
				self.ControlP.save.left = true
				door:setMirror(true)
				table.insert(b.children,door)
				table.insert(self.ControlP.doors,door)				
			end 
		end 
		return f 
	end 
	
	local pa = {
		x = 28,
		y = 40,
		w = 52,
		h = 59,
	}
	local sig = {
		x = 88,
		y = 15,
		w = 15,
		h = 16,
	}
	local timer = 0 
	local tpos = 1
	local at = ""
	local olt = ""
	function b:OnDraw()
		local des,cur = b.des,b.current
		local dt = love.timer.getDelta()
		local lg = love.graphics
		local F = lg.getFont()
		local x = self._Ux
		local y = self._Uy
		x,y = x - self.width/2,y - image:getHeight()+5
		x,y = math.floor(x),math.floor(y)
		local t = "Welcome to ?#~?;a@w dorms!"
		if self.IsBlocked then 
			timer = timer + 5*dt
			if timer > 10 then 
				lg.setFont(medFont)
				t = "Please step away from the doors!"
			else 
				lg.setFont(medFont)
			end
		else 
			timer = 0 
			lg.setFont(medFont)
		end 
		if des > cur then 
			t = "Going up to "..des
		elseif des < cur then 
			t = "Going down to "..des 
		end 
			tpos = tpos + 10*dt
			local apos = math.abs(tpos)
			local tl = string.len(t)
			if t ~= olt then 
				tpos = 0
				apos = 0 
				olt = t
			end 
			if apos > tl then 
				apos = tl
				if tpos > apos + 20 then 
					tpos = 0 
				end 
			end 
			at = string.sub(t,1,apos)
			if math.floor(math.abs(self.timer)) > 0 then 
				at = "Moving in: "..math.floor(findDistance(self.timer,self.pause))
			end 
		lg.printf(at,x + pa.x,y + pa.y,pa.w,"center")
		local t = cur
		lg.printf(t,x + sig.x,y + sig.y,sig.w,"center")
		lg.setFont(F)
	end 
	return b  
end 
function der:getDependencies()
	local d = {}
	local dep = {module = "door",derivative = "elevator_door"}
	table.insert(d,dep)
	return d 
end 
function der:setDependencies(d)
	self.dep = d  
end 
return der 