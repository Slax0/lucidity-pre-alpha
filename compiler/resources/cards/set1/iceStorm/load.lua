local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE.."/image.png")
local icePick = love.graphics.newImage(_PACKAGE.."/icePick.png")
local w,h = RCToWH(icePick,1,8)
icePick = newAnimation(icePick,w,h,0.07,1,8)
icePick:setMode("once")
local card = {
	name = "Ice Storm",
	desc = "Does 20 damage to whole team.",
	cost = 30,
	rarity = 2, -- out of 10
	target = "team", -- team, none, single
	onUse = function(target,stack,item,area1,area2) -- Target is the target ::  stack: {team1, team2, finished} :: item is the item that you can draw with, it will allow you to draw things to screen
		-- Area polygon is only there if target = "team". ITs the target area for  the selected team in points eg [x,y,x1,y1,x2,y2]
		-- the draw item has the following func: drawBefore() draw() update()
		local element = "ice"
		-- for i ==1,10 do 
			-- local x,y = math.random(0,)
		-- end 
		local x,y = area1:bbox()
		local aw,ah = findDimensions(area1)
		local anims = {} 
		local grid = {} 
		for x=1,aw,w do 
			for y=1,ah,h do 
				local block = {x,y} 
				table.insert(grid,block)
			end 
		end 
		local no = 0 
		local bno = 0 
		for i,v in ipairs(grid) do 
			local c = love.math.random(1,3)
			if c == 1 and not v[3] then 
				bno = bno + 1 
				local anim = icePick:addInstance()
				anim:setMode("once")
				anim._px = v[1]
				anim._py = v[2]
				local dx,dy = x + v[1],y + v[2]
				local speed = 1000
				local distance =  math.sqrt((dx + i)^2 + dy^2) 
				Timer.tween(distance/speed,anim,{_px = dx,_py = dy},"in-quad",function()
					no = no + 1 
					if no >= #anims then 
						for i,v in ipairs(target) do 
							v:applyDamage(20,element)
						end 
						stack[4] = true	
					end 
				end)
				anim._resG = grid[i]
				grid[i][3] = true 
				table.insert(anims,anim)
			end 
		end 
		item.drawBefore = function()
			for i,v in ipairs(anims) do 
				if (v._px ~= x  + v._resG[1] or v._py ~= y  + v._resG[2])  then 
					v:draw(v._px,v._py)
				end 
			end 
		end 
		item.update = function(dt)
			for i,v in ipairs(anims) do 
				v:update(dt)
			end 
		end
	end,
	qualifier = function(stack) -- stack is same as above
		return true
	end,
	image = image,
	anim = {}, -- if animation then place here, else will display image without anim. Manage states using functions above.
}

return card