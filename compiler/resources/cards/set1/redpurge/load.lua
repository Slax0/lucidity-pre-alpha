local _PACKAGE = getPackageAlt(...)
local image = love.graphics.newImage(_PACKAGE .."/image.png")
local fireAnim = love.graphics.newImage(_PACKAGE .."/fire.png")
local w,h = RCToWH(fireAnim,1,14)
fireAnim = newAnimation(fireAnim,w,h,0.07,1,14)
local card = {
	name = "The Red Purge",
	desc = "Dust to dust \n targets team \n 50% chance \n sets on fire or 20 ATK",
	cost = 30,
	rarity = 2, -- out of 10
	target = "team", -- team, none, single
	onUse = function(target,stack,item,area1,area2) -- Target is the target ::  stack: {team1, team2, finished} :: item is the item that you can draw with, it will allow you to draw things to screen
		-- Area polygon is only there if target = "team". ITs the target area for  the selected team in points eg [x,y,x1,y1,x2,y2]
		-- the draw item has the following func: drawBefore() draw() update()
		local element = "fire"
		-- for i ==1,10 do 
			-- local x,y = math.random(0,)
		-- end 
		local x,y = area1:bbox()
		local aw,ah = findDimensions(area1)
		local anims = {} 
		local grid = {} 
		for x=1,aw,w do 
			for y=1,ah,h do 
				local block = {x,y} 
				table.insert(grid,block)
			end 
		end 
		for i,v in ipairs(grid) do 
			local c = love.math.random(1,3)
			if c == 1 and not v[3] then 
				local anim = fireAnim:addInstance()
				anim._px = v[1]
				anim._py = v[2]
				grid[i][3] = true 
				table.insert(anims,anim)
			end 
		end 
		item.drawBefore = function()
			for i,v in ipairs(anims) do 
				v:draw(x + v._px,y + v._py)
			end 
		end 
		item.update = function(dt)
			for i,v in ipairs(anims) do 
				v:update(dt)
			end 
		end 
		for i,v in ipairs(target) do 
			local chance = math.random(1,2)
			if chance == 1 then 
				v:applyDamage(20,element)
			elseif chance == 2 then 
				v:setEffect(element)
			end 
		end
		stack[4] = true 
	end,
	qualifier = function(stack) -- stack is same as above
		return true
	end,
	image = image,
	anim = {}, -- if animation then place here, else will display image without anim. Manage states using functions above.
}

return card