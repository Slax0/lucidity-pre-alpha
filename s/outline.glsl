vec4 resultCol;
extern vec2 stepSize;
extern vec3 color;
extern number alpha2;
vec4 effect( vec4 col, Image texture, vec2 texturePos, vec2 screenPos )
{
    number alpha = 4*texture2D( texture, texturePos ).a;
    alpha += texture2D( texture, texturePos - vec2( stepSize.x, stepSize.y ) ).a;
    alpha += texture2D( texture, texturePos - vec2( -stepSize.x, stepSize.y ) ).a;
    alpha += texture2D( texture, texturePos - vec2( stepSize.x, stepSize.y ) ).a;
    alpha += texture2D( texture, texturePos - vec2( stepSize.x, -stepSize.y ) ).a;
    // calculate resulting color
    resultCol = vec4( color.x, color.y, color.z, alpha);
    // return color for current pixel
    return resultCol;
}
